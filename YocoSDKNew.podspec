#
# Be sure to run `pod lib lint YocoSDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'YocoSDKNew'
  s.version          = '4.3.1-rc2'
  s.summary          = 'Yoco\'s payment sdk with UI interface'
  
  s.description      = <<-DESC
  The Yoco SDK will allow your app to pair with Yoco card machines and accept payments. It provides a built-in user interface to take the user through the payment flow.
  DESC
  
  s.homepage         = 'https://www.yoco.co.za'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Jacques Questiaux' => 'jacques@yoco.com' }
  s.source           = { :git => 'https://Fonix@bitbucket.org/Fonix/yocosdk.git', :tag => s.version }
  s.social_media_url = 'https://www.facebook.com/YocoZA/'
  
  s.ios.deployment_target = '9.0'
  s.swift_version = "5.2.1"
  
  s.vendored_frameworks = s.name + '.xcframework'
  s.dependency 'ReSwift', '~> 5.0'
  s.dependency 'lottie-ios', '~> 3.1'
  s.dependency 'OnoSDK', '~> 1.4.0'
  s.dependency 'SwiftSignatureView', '~> 2.2' # requires deployment target 8.3
  s.dependency 'Gloss', '~> 3.2'
  s.dependency 'Alamofire', '~> 4.9'
  
end
