# YocoSDK

[![CI Status](https://img.shields.io/gitlab/pipeline/yoco/yoco-payment-sdk-ios-ui)](https://gitlab.com/yoco/yoco-payment-sdk-ios-ui/)
[![Version](https://img.shields.io/cocoapods/v/YocoSDK.svg?style=flat)](https://cocoapods.org/pods/YocoSDK)
[![License](https://img.shields.io/cocoapods/l/YocoSDK.svg?style=flat)](https://gitlab.com/yoco/yoco-payment-sdk-ios-ui/-/raw/develop/LICENSE)
[![Platform](https://img.shields.io/cocoapods/p/YocoSDK.svg?style=flat)](https://cocoapods.org/pods/YocoSDK)

## Requirements

iOS 9.0 and above, and a secret key acquired from Yoco.

## Installation

Please see our installation guide at https://developer.yoco.com/

## Author

Jacques Questiaux, support@yoco.com

## License

YocoSDK is available under the MIT license. See the LICENSE file for more info.
