//
//  MiuraSDKService.swift
//  YocoPOS
//
//  Created by Shadrack Taeli on 2020/04/02.
//  Copyright © 2020 Facebook. All rights reserved.
//

import Foundation
import Ono

class MiuraSDKService: IMiuraSDKService {
  
  var tag: String = OnoMiuraUtils().loggingTag(className: String(describing: MiuraSDKService.self))
  
  private let listener: OnoMiuraListenerProtocol
  private let miuraManager: MiuraManager
  private let miuraDelegate: OnoMiuraManagerDelegate
  
  private var rkiAvailableUpdate: AvailableUpdate? = nil
  private let initialDelayInSeconds = 10.0
  private let waitForResetInSeconds = 60.0
  private let betweenAttemptDelayInSeconds = 5.0
  private let totalConnectionAttempts = 20
  
  private var rkiRequestData: [String: String]? = nil
  
  private let waitForConnectedTerminalInSeconds = 1.0
  private let totalOpenSessionAttempts = 3
  
  init(listener: OnoMiuraListenerProtocol) {
    self.listener = listener
    miuraManager = MiuraManager.sharedInstance()
    miuraManager.targetDevice = TargetDevice.PED
    miuraDelegate = OnoMiuraManagerDelegate(listener: listener)
    miuraManager.delegate = miuraDelegate
  }
  
  func abortTransaction(errorDetails: ErrorDetails, finalizeTransaction: Bool) {
    if finalizeTransaction {
      miuraManager.continueTransaction(
        MPITLVObject(tag: TLVTag.TLVTag_Command_Data, value: "".data(using: String.Encoding.utf8)),
        completion: { (_, result: MPIResponseData) in
          if result.isSuccess() {
            self.miuraManager.displayText("", completion: {_ in self.listener.onAbortTransaction() })
          } else {
            self.listener.onUnableToAbortTransaction()
          }
      }
      )
    } else {
      miuraManager.abortTransaction { (success) in
        if success {
          self.listener.onAbortTransaction()
        }else{
          self.listener.onUnableToAbortTransaction()
        }
      }
    }
  }
  
  func askForPin(transactionAmount: Int64, transactionCurrency: Currency, onoMiuraCardData: OnoMiuraCardData, topLineLabel: String) {
    if let miuraTrack2String = onoMiuraCardData.maskedTrack2Data?.asString() {
      miuraManager.onlinePin(
        withAmount: UInt(transactionAmount),
        currencyCode:  UInt(transactionCurrency.code),
        track2Data: miuraTrack2String,
        labelText: topLineLabel
      ) {(result: OnlinePinResponse?) in
        if result?.pinData == nil {
          self.listener.onPinEntryCancelledOrTimeout()
        } else if result?.pinKSN == nil {
          self.listener.onPinEntryFailed(reason: Messages().NO_PIN_KSN)
        } else {
          self.listener.onPinDataProvided(
            pinData: result?.pinData?.toSensitiveString(),
            pinKsn: result?.pinKSN?.toSensitiveString()
          )
        }
      }
    } else {
      self.listener.onPinEntryFailed(reason: Messages().NO_TRACK_2_DATA)
    }
  }
  
  func cardStatus(status: Bool) {
    miuraManager.cardStatus(status, enableAtr: status, enableTrack1: false, enableTrack2: status, enableTrack3: status) { (result) in
      Logger().i(tag: self.tag, message: "cardStatus: success = \(result)")
    }
  }
  
  func unregisterDeviceConnectionListeners() {
    miuraDelegate.listenToConnectionHandlers = false
  }
  
  func closeSession() {
    miuraManager.closeSession()
  }
  
  func displayText(miuraText: OnoMiuraText, clearCommandAfterExecution: MiuraCommandType) {
    miuraManager.displayText(miuraText.toCenteredText()) { (success) in
      if success {
        self.listener.onTextDisplayed(clearCommandAfterExecution: clearCommandAfterExecution, shownMessage: miuraText)
      } else {
        self.listener.onTextDisplayedFailed(clearCommandAfterExecution: clearCommandAfterExecution, shownMessage: miuraText)
      }
    }
  }
  
  func getBatteryStatus() {
    miuraManager.getBatteryStatus { (charg, batteryPercentage) in
      self.listener.onGetBatteryStatusInfo(chargingStatus: Int32(MPIBinaryUtil.int(withByte: UInt8(charg.rawValue))), batteryLevel: Int32(batteryPercentage))
    }
  }
  
  func getBluetoothInfo() {
    miuraManager.getBluetoothInfo { (bluetoothInfo) in
      if let bluetoothInfo = bluetoothInfo {
        self.listener.onGetBluetoothInfo(bluetoothInfo: bluetoothInfo)
      } else {
        self.listener.onGetBluetoothInfoFailed(reason: "we got Null BluetoothInfo")
      }
    }
  }
  
  func getDeviceCapabilities() {
    miuraManager.getDeviceCapabilities { (capabilities) in
      if let capabilities = capabilities {
        self.listener.onGetDeviceCapabilities(deviceCapabilities: capabilities.map{ OnoMiuraDeviceCapability(name: $0.key, value: $0.value) })
      } else {
        self.listener.onGetDeviceCapabilitiesFailed(reason: "we got Null device capabilities")
      }
    }
  }
  
  func getDeviceName() -> String {
    return UIDevice.current.model
  }
  
  func getDeviceP2PEFiles() {
    let command = MiuraCommandType.terminalGetP2peFiles
    miuraManager.p2peInitialise(withCompletion: true) { (rkiInitStatus: RkiInitStatus) in
      // RkiInitStatus seems to be missing a NoError class, 1 is an invalid value but is what is returned when it was succesfull
      if rkiInitStatus.rawValue == 1 {
        self.miuraManager.p2peStatus { (status) in
          if status?.isInitialised ?? false {
            let requestRkiDataIdValues = [
              RequestRkiDataId.prodSigningCertificate,
              RequestRkiDataId.terminalCertificate,
              RequestRkiDataId.tempKeyloadCertificate,
              RequestRkiDataId.suggestedIksn
            ]
            self.rkiRequestData = [String: String]()
            self.getNextFileFromPED(command: command, requestRkiDataIdValues: requestRkiDataIdValues, index: 0)
          } else {
            self.throwException(commandType: command, exceptionDescription: OnoMiuraStatusMessages().P2PE_INITIALIZE_IS_FALSE)
          }
        }
      } else {
        self.throwException(commandType: command, exceptionDescription: OnoMiuraStatusMessages().P2PE_INITIALIZE_FAILED)
      }
    }
  }
  
  private func getNextFileFromPED(command: MiuraCommandType, requestRkiDataIdValues: [RequestRkiDataId], index: Int) {
    let dataCount = requestRkiDataIdValues.count
    if index < dataCount {
      for (i, dataId) in requestRkiDataIdValues.enumerated() {
        if i == index {
          self.miuraManager.downloadBinary(withFileName: dataId.fileName) { (fileData) in
            if let fileData = fileData {
              if dataId == RequestRkiDataId.suggestedIksn {
                if let value = String(bytes: fileData, encoding: String.Encoding.utf8) {
                  self.rkiRequestData?.updateValue(
                    // Miura's return the suggested IKSN like `IKSN: FFFFF123....\n` but Ono expects it without the IKSN, \n and leading spaces
                    value.replacingOccurrences(of: "IKSN: ", with: "").replacingOccurrences(of: "\n", with: ""),
                    forKey: dataId.jsonKey
                  )
                }
              } else {
                self.rkiRequestData?.updateValue(
                  fileData.base64EncodedString().replacingOccurrences(of: "\n", with: ""),
                  forKey: dataId.jsonKey
                )
              }
              self.getNextFileFromPED(command: command, requestRkiDataIdValues: requestRkiDataIdValues, index: index + 1)
            } else {
              self.throwException(
                commandType: command,
                exceptionDescription: OnoMiuraStatusMessages().GET_PED_FILE_FAILED(fileName: dataId.fileName)
              )
            }
          }
        }
      }
    } else {
      listener.onRkiPedFiles(fileDataMap: rkiRequestData ?? [String: String]())
      rkiRequestData = nil
    }
  }
  
  func getP2PEStatus(isKeyInjectionVerification: Bool) {
    miuraManager.p2peStatus { (status: P2PEStatus?) in
      if let status = status {
        self.listener.onGetP2PEStatusInfo(isInitialized: status.isInitialised.toKotlinBoolean(), isPinReady: status.isPINReady.toKotlinBoolean(), isSREDReady: status.isSREDReady.toKotlinBoolean(), isKeyInjectionVerification: isKeyInjectionVerification)
      } else {
        self.listener.onGetP2PEStatusInfoFailed(reason: "we got Null P2PEStatus", isKeyInjectionVerification: isKeyInjectionVerification)
      }
    }
  }
  
  func getPEDConfig() {
    miuraManager.getConfigurationWithCompletion { (config) in
      if let config = config {
        self.listener.onGetPEDConfigInfo(versionMap: config)
      } else  {
        self.listener.onGetPEDConfigInfoFailed(reason: "we got Null PEDConfig")
      }
    }
  }
  
  func getSoftwareInfo() {
    miuraManager.getSoftwareInfo { (softwareInfo) in
      if let softwareInfo = softwareInfo {
        let os = OnoMiuraOS(type: softwareInfo.osType!, version: softwareInfo.osVersion!)
        let mpi = OnoMiuraMPI(type: softwareInfo.mpiType!, version: softwareInfo.mpiVersion!)
        self.listener.onGetSoftwareInfo(miuraOS: os, miuraMPI: mpi)
      } else  {
        self.listener.onGetSoftwareInfoFailed(reason: "we got Null info from the device")
      }
    }
  }
  
  func injectKeys(keyInjectionDataMap: SensitiveMap) {
    let reponseRkiDataIdValues = [
      ResponseRkiDataId.hsmCert,
      ResponseRkiDataId.kbpk,
      ResponseRkiDataId.kbpkSig,
      ResponseRkiDataId.sredIksn,
      ResponseRkiDataId.sredTr31,
      ResponseRkiDataId.pinIksn,
      ResponseRkiDataId.pinTr31
    ]
    
    writeNextFileToPED(reponseRkiDataIdValues: reponseRkiDataIdValues, keyInjectionDataMap: keyInjectionDataMap, index: 0)
  }
  
  private func writeNextFileToPED(reponseRkiDataIdValues: [ResponseRkiDataId], keyInjectionDataMap: SensitiveMap, index: Int) {
    let command = MiuraCommandType.terminalKeyInjection
    let dataCount = reponseRkiDataIdValues.count
    if index < dataCount {
      for (i, dataId) in reponseRkiDataIdValues.enumerated() {
        if i == index {
          Logger().i(tag: tag, message: "Start writing file \(index) \(dataId)")
          if let base64Data = keyInjectionDataMap.valueMap[dataId.jsonName] {
            var fileData: Data?
            switch dataId {
              case .kbpkSig, .kbpk:
                fileData = Data(base64Encoded: base64Data)
              case .sredIksn, .pinIksn:
                fileData = Data([UInt8]("IKSN: \(base64Data)".utf8))
              default:
                fileData = Data(base64Encoded: base64Data)
            }
            
            if let fileData = fileData {
              Logger().i(tag: tag, message: "\(dataId), size \(String(describing: fileData.count))")
              listener.onWritingPedFile(
                fileName: dataId.pedFileName,
                fileSize: Int32(fileData.count),
                fileNumber: Int32(index) + 1,
                totalNumberOfFiles: Int32(dataCount)
              )
              miuraManager.uploadBinary(fileData, forName: dataId.pedFileName) { (_, response) in
                if response.isSuccess() {
                  Logger().i(tag: self.tag, message: OnoMiuraStatusMessages().WRITE_PED_FILE_SUCCESS(dataId: dataId))
                  self.writeNextFileToPED(
                    reponseRkiDataIdValues: reponseRkiDataIdValues,
                    keyInjectionDataMap: keyInjectionDataMap,
                    index: index + 1
                  )
                } else {
                  self.throwException(
                    commandType: command,
                    exceptionDescription: OnoMiuraStatusMessages().WRITE_PED_FILE_FAILED(dataId: dataId)
                  )
                }
              }
            } else {
              throwException(commandType: command, exceptionDescription: OnoMiuraStatusMessages().NULL_DATA_FOR_PED_FILE(dataId: dataId))
            }
          } else {
            throwException(commandType: command, exceptionDescription: OnoMiuraStatusMessages().NULL_DATA_FOR_PED_FILE(dataId: dataId))
          }
        }
      }
    } else {
      miuraManager.p2peImport(withCompletion: true) { (rkiImportStatus) in
        if rkiImportStatus == RkiImportStatus.noError {
          self.listener.onSuccessfulKeyInjection()
        } else {
          self.throwException(commandType: command, exceptionDescription: OnoMiuraStatusMessages().KEY_INJECTION_PED_ERROR(error: ""))
        }
      }
    }
  }
  
  func openSession(serialNumber: String, address: String) {
    Logger().i(tag: tag, message: "openSession: \(serialNumber) - \(address)")
    registerDeviceConnectionHandlers()
    getConnectedTerminal(address: address, openSessionCount: 0)
  }
  
  private func getConnectedTerminal(address: String, openSessionCount: Int) {
    Logger().i(tag: tag, message: "getConnectedTerminal: \(address)")
    if openSessionCount < totalOpenSessionAttempts {
      Logger().i(tag: tag, message: "Attempt: \(openSessionCount + 1) of \(totalOpenSessionAttempts)")
      if let accessory = miuraManager.connectedAccessories()?.first(where: { ($0 as? EAAccessory)?.name == address }) as? EAAccessory {
        if let firstProtocol = accessory.protocolStrings.first {
          Logger().i(tag: self.tag, message: "Got terminal after \(openSessionCount + 1) attempts, get connected terminal successful")
          miuraManager.openSession(accessory, protocolName: (firstProtocol))
          listener.onOpenSessionComplete(isConnected: miuraManager.isConnected, errorMessage: nil)
        } else {
          listener.onOpenSessionComplete(
            isConnected: false,
            errorMessage: "Device protols had a nil first protocol. available device protocols: \(accessory.protocolStrings)"
          )
        }
      } else {
        Logger().i(tag: self.tag, message: "Attempting to get a connected terminal again")
        DispatchQueue.main.asyncAfter(deadline: .now() + waitForConnectedTerminalInSeconds) {
          self.getConnectedTerminal(address: address, openSessionCount: openSessionCount + 1)
        }
      }
    } else {
      listener.onOpenSessionComplete(isConnected: false, errorMessage: "Card machine not found, please check it is awake and nearby")
    }
  }
  
  private func streamNextFile(availableUpdate: AvailableUpdate, downloadedFiles: [String : KotlinByteArray], os: OnoMiuraOS?, index: Int) {
    let fileCount = downloadedFiles.count
    if (fileCount > index) {
      for (i, key) in downloadedFiles.keys.enumerated() {
        if (i == index) {
          let printIndex = index + 1
          let terminalText = OnoMiuraText(
            topLineMaxOf21Chars: "Copying file \(printIndex) of \(fileCount)",
            secondLineMaxOf21Chars: "Please wait...",
            thirdLineMaxOf21Chars: nil,
            forthLineMaxOf21Chars: nil,
            fifthLineMaxOf21Chars: nil,
            capitalizeLines: false,
            clearAfterMillis: 0
          )
          
          if let byteArray: KotlinByteArray = downloadedFiles[key] {
            let filename = UpdateUtils().mapUpdateTypeToFilename(
              updateType: key,
              os: os,
              version: availableUpdate.files.last(where: { $0.type == key } )?.version
            )
            Logger().i(tag: tag, message: "Start: Copying file \(filename), file \(printIndex) of \(fileCount)")
            miuraManager.displayText(terminalText.toCenteredText(), completion: nil)
            miuraManager.uploadBinary(byteArray.toData(), forName: filename) { (_, response: MPIResponseData) in
              if response.isSuccess() {
                Logger().i(tag: self.tag, message: "Finished: Copying file \(printIndex) / \(fileCount)")
                self.streamNextFile(availableUpdate: availableUpdate, downloadedFiles: downloadedFiles, os: os, index: index + 1)
              } else {
                Logger().i(tag: self.tag, message: "failed to upload file: \(filename)")
                self.listener.onUpdateFinished(
                  availableUpdate: availableUpdate,
                  wasSuccessful: false,
                  errorMessage: "Error uploading file \(key)"
                )
                return
              }
            }
          }
        }
      }
    } else {
      miuraManager.displayText(OnoMiuraText.Companion().REBOOTING.toCenteredText(), completion: nil)
      listener.terminalConnectingMessageUpdated(connectingMessage: TerminalStatusMessage.UpdateMessages().INSTALLING_UPDATES)
      self.hardResetAndReconnectDevice(onReconnect: {
        self.miuraManager.displayText(OnoMiuraText.Companion().UPDATE_COMPLETE.toCenteredText(), completion: nil)
        self.listener.onUpdateFinished(availableUpdate: availableUpdate, wasSuccessful: true, errorMessage: nil)
      }) {
        self.listener.onUpdateFinished(availableUpdate: availableUpdate, wasSuccessful: false, errorMessage: "device never reconnected")
      }
    }
  }
  
  func performUpdate(availableUpdate: AvailableUpdate, downloadedFiles: [String : KotlinByteArray], os: OnoMiuraOS?) {
    Logger().i(tag: tag, message: "performUpdate")
    listener.terminalConnectingMessageUpdated(connectingMessage: TerminalStatusMessage.UpdateMessages().COPYING_UPDATES)
    streamNextFile(availableUpdate: availableUpdate, downloadedFiles: downloadedFiles, os: os, index: 0)
  }
  
  func registerEventHandlers() {
    // This is not needed for iOS
  }
  
  private func registerDeviceConnectionHandlers() {
    miuraDelegate.listenToConnectionHandlers = true
  }
  
  func setConnector(terminalAddress: String) {
    // This is not needed for iOS
  }
  
  func startContactlessTransaction(transactionAmount: Int32, currencyCode: Int32, cardPresentationMode: CardPromptMode) {
    listener.onAwaitingCard(cardPresentationMode: cardPresentationMode)
    miuraManager.startContactlessTransaction(with: MPITransactionType.purchase, amount: UInt(transactionAmount), currencyCode: UInt(currencyCode)) { (_, response: MPIResponseData) in
      if response.isSuccess() {
        self.listener.onCardTapped(nfcAuthorizationTLV: response.body.toKotlinByteArray())
      } else {
        self.listener.onStartTransactionFailed(cardPresentationMode: cardPresentationMode, transactionError: self.mappedResponseError(response: response))
      }
    }
  }
  
  func startContactTransaction(displayMessage: OnoMiuraText, cardPresentationMode: CardPromptMode) {
    miuraManager.displayText(displayMessage.toCenteredText()) { (success) in
      if success {
        self.cardStatus(status: true)
        self.listener.onAwaitingCard(cardPresentationMode: cardPresentationMode)
      } else {
        self.listener.onStartTransactionFailed(cardPresentationMode: cardPresentationMode, transactionError: nil)
      }
    }
  }
  
  func doEMV(transactionAmount: Int64, transactionCurrency: Currency) {
    self.miuraManager.startTransaction(with: MPITransactionType.purchase, amount: UInt(transactionAmount), currencyCode: UInt(transactionCurrency.code)) {(_, response: MPIResponseData) in
      if response.isSuccess() {
        self.listener.onCardInserted(emvAuthorizationTLV: response.body.toKotlinByteArray())
      } else {
        let error = self.mappedResponseError(response: response)
        if (error != OnoMiuraTransactionError.contactlessAbortByCardInsert) {
          // if we get contactlessAbortByCardInsert, this could mean the card was already inserted when an NFC transaction started,
          // and we will get a follow up cardData event to continue the transaction
          self.listener.onEMVFailed(transactionError: self.mappedResponseError(response: response))
        }
      }
    }
  }
  
  func processARPC(arpc: SensitiveString) {
    miuraManager.continueTransaction(arpc.toMPITLVObject()) { (_, response) in
      if response.isSuccess() {
        self.listener.onProcessARPC(finalizationTLV: response.body.toKotlinByteArray())
      } else {
        self.listener.onProcessARPCFailed(transactionError: self.mappedResponseError(response: response))
      }
    }
  }
  
  func transactionCompleted() {
    cardStatus(status: false)
  }
  
  func askForTipAmount(
    topLinePromptIndex: Int32,
    secondLinePromptIndex: Int32,
    thirdLinePromptIndex: Int32,
    maxNumIntDigits: Int32,
    numFractalDigits: Int32,
    numberPlaceHolder: String
  ) {
    do{
      try miuraManager.getNumericData(
        automaticEnter: false,
        backlightOn: true,
        firstLineIndex: topLinePromptIndex,
        secondLineIndex: secondLinePromptIndex,
        thirdLineIndex: thirdLinePromptIndex,
        numIntDigits: maxNumIntDigits,
        numFracDigits: numFractalDigits,
        numberToEditAscii: numberPlaceHolder,
        onSuccess: {self.listener.tipAmountEntered(amount: Int32(100.0 * $0))},
        onFailure: {self.listener.onAsyncProcessException(commandType: MiuraCommandType.tipAmountEntry, exceptionDescription: $0, exceptionCause: $1)}
      )
    } catch let error {
      self.listener.onAsyncProcessException(commandType: MiuraCommandType.tipAmountEntry, exceptionDescription: error.localizedDescription, exceptionCause: nil)
    }
  }
  
  func doYouWantToTipPrompt(promptMessage: OnoMiuraText) {
    displayText(miuraText: promptMessage, clearCommandAfterExecution: MiuraCommandType.doYouWantToTip)
  }
  
  func enableKeypad() {
    miuraManager.keyboardStatus(KeyPadStatusSettings.enable, backlightSetting: BacklightSettings.no_Change) { (_, response: MPIResponseData) in
      self.listener.keypadStatus(isEnabled: response.isSuccess())
    }
  }
  
  func hardResetAndReconnectDevice(onReconnect: @escaping () -> Void, onDisconnected: @escaping () -> Void) {
    Logger().i(tag: tag, message: "Start device reset")
    self.unregisterDeviceConnectionListeners()
    var reconnectingAttemptsStarted = false
    miuraManager.resetDevice(withResetType: ResetDeviceType.hard_Reset) { (_, _) in
      if (!reconnectingAttemptsStarted) {
        reconnectingAttemptsStarted = true
        Logger().i(tag: self.tag, message: "Device finished reseting, starting reconnect attempts")
        DispatchQueue.main.asyncAfter(deadline: .now() + self.initialDelayInSeconds) {
          self.delayAndAttemptReconnect(reconnectCount: 0, onReconnect: onReconnect, onDisconnected: onDisconnected)
        }
      }
    }
    
    DispatchQueue.main.asyncAfter(deadline: .now() + self.waitForResetInSeconds) {
      if (!reconnectingAttemptsStarted) {
        reconnectingAttemptsStarted = true
        Logger().i(tag: self.tag, message: "Device never reporting reset finishing, starting reconnect attempts")
        self.delayAndAttemptReconnect(reconnectCount: 0, onReconnect: onReconnect, onDisconnected: onDisconnected)
      }
    }
  }
  
  private func delayAndAttemptReconnect(reconnectCount: Int, onReconnect: @escaping () -> Void, onDisconnected: @escaping () -> Void) {
    if (reconnectCount < totalConnectionAttempts) {
      Logger().i(tag: tag, message: "Attempt: \(reconnectCount + 1) of \(totalConnectionAttempts)")
      Logger().i(tag: self.tag, message: "Attempting to connect again")
      let deviceReconnected = self.miuraManager.openSession()
      if (!deviceReconnected) {
        Logger().i(tag: tag, message: "Device is still disconnected, wait \(betweenAttemptDelayInSeconds) seconds and then try connect again")
        DispatchQueue.main.asyncAfter(deadline: .now() + betweenAttemptDelayInSeconds) {
          self.delayAndAttemptReconnect(reconnectCount: reconnectCount + 1, onReconnect: onReconnect, onDisconnected: onDisconnected)
        }
      } else {
        Logger().i(tag: tag, message: "Device says reconnected, wait 1 second and test if true")
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
          self.miuraManager.getDeviceCapabilities { (deviceCapabilities) in
            if deviceCapabilities != nil {
              Logger().i(tag: self.tag, message: "Device capabilities not null, means connection is solid")
              Logger().i(tag: self.tag, message: "Device reconnected after \(reconnectCount + 1) attempts, update completed successfully")
              self.registerDeviceConnectionHandlers()
              onReconnect()
            } else {
              Logger().w(tag: self.tag, message: "Device capabilities null, treat as still disconnected")
              DispatchQueue.main.asyncAfter(deadline: .now() + self.betweenAttemptDelayInSeconds) {
                self.delayAndAttemptReconnect(reconnectCount: reconnectCount + 1, onReconnect: onReconnect, onDisconnected: onDisconnected)
              }
            }
          }
        }
      }
    } else {
      Logger().e(tag: self.tag, message: "Device never reconnected after \(reconnectCount + 1) attempts, update failed")
      self.registerDeviceConnectionHandlers()
      self.listener.terminalDisconnected()
      onDisconnected()
    }
  }
  
  private func mappedResponseError(response: MPIResponseData) -> OnoMiuraTransactionError {
    switch response.sw2[0] {
      case 33:
        return OnoMiuraTransactionError.invalidData
      case 34:
        return OnoMiuraTransactionError.terminalNotReady
      case 35:
        return OnoMiuraTransactionError.noSmartcardInSlot
      case 37:
        return OnoMiuraTransactionError.invalidCardNoMsrAllowed
      case 38:
        return OnoMiuraTransactionError.commandNotAllowed
      case 39:
        return OnoMiuraTransactionError.dataMissing
      case 40:
        return OnoMiuraTransactionError.unsupportedCardMsrAllowed
      case 48:
        return OnoMiuraTransactionError.cardReadErrorMsrAllowed
      case 64:
        return OnoMiuraTransactionError.invalidIssuer
      case 65:
        return OnoMiuraTransactionError.userCancelled
      case 66:
        return OnoMiuraTransactionError.contactlessTimeout
      case 67:
        return OnoMiuraTransactionError.contactlessAbortByCardInsert
      case 68:
        return OnoMiuraTransactionError.contactlessAbortBySwipe
      case 193:
        return OnoMiuraTransactionError.contactlessInsertOrSwipe
      case 194:
        return OnoMiuraTransactionError.contactlessInsertSwipeOrOtherCard
      case 195:
        return OnoMiuraTransactionError.contactlessInsertCard
      case 207:
        return OnoMiuraTransactionError.contactlessHardwareError
      default:
        return OnoMiuraTransactionError.unknown
    }
  }
  
  private func throwException(commandType: MiuraCommandType, exceptionDescription: String) {
    listener.onAsyncProcessException(
      commandType: commandType,
      exceptionDescription: exceptionDescription,
      exceptionCause: nil
    )
  }
}



