//
//  OnoMiuraExtensios.swift
//  YocoPOS
//
//  Created by Nkokhelo Mhlongo on 2020/05/21.
//  Copyright © 2020 Facebook. All rights reserved.
//

import Foundation
import Ono

// Miura Extensions
extension CardData {
  /**
   * Convert CardData to OnoMiuraCardData
   */
  func toOnoMiuraCardData() -> OnoMiuraCardData {
    return OnoMiuraCardData(
      sensitizedRaw: self.raw.toSensitiveByteArray(),
      answerToReset: self.answerToReset,
      pinData: nil, // Not required
      trackData: self.sredData?.toSensitiveString(),
      pinDataKsn: nil, // Not required
      cardStatus: self.cardStatus.toOnoMiuraCardStatus(),
      trackDataKSN: self.sredKSN?.toSensitiveString(),
      plainTrack1Data: nil, // TODO CardData doesn't have this
      plainTrack2Data: self.track2Data?.toOnoMiuraTrack2Data(), // TODO CardData doesn't have this
      maskedTrack2Data: self.track2Data?.toOnoMiuraTrack2Data()
    )
  }
}

extension Track2Data {
  /**
   * Convert Track2Data to OnoMiuraCardData
   */
  func toOnoMiuraTrack2Data() -> OnoMiuraCardData.OnoMiuraTrack2Data {
    return OnoMiuraCardData.OnoMiuraTrack2Data(
      sensitizedRaw: self.data.map{ SensitiveByteArray(byteArray: ($0.toByteArray())!) },
      isMasked: true, // Defaulting to true as the docs say track 2 data is masked
      pan: self.pan?.toSensitiveString(),
      expirationDate: self.expirationDate?.toSensitiveString(),
      serviceCode: self.serviceCode?.stringValue
    )
  }
}

extension CardStatus {
  /**
   * Convert CardStatus to OnoMiuraCardData.OnoMiuraCardStatus
   */
  func toOnoMiuraCardStatus() -> OnoMiuraCardData.OnoMiuraCardStatus {
    return OnoMiuraCardData.OnoMiuraCardStatus(
      cardPresent: self.isCardPresent,
      emvCompatible: self.isEMVCompatible,
      msrDataAvailable: self.isMSRDataAvailable,
      track1DataAvailable: self.isTrack1DataAvailable,
      track2DataAvailable: self.isTrack2DataAvailable,
      track3DataAvailable: self.isTrack3DataAvailable
    )
  }
}

extension DeviceStatus {
  /**
   * Convert DeviceStatus to MiuraDeviceStatus
   */
  func toOnoMiuraDeviceStatus() -> MiuraDeviceStatus {
    switch self {
      case .devicePoweredOn: return .devicepoweredon
      case .pinEntryEvent: return .pinentryevent
      case .applicationSelection: return .applicationselection
      case .deviceRebooting: return .devicerebooting
      case .devicePoweringOff: return .devicepoweringoff
      case .mpiRestarting: return .mpirestarting
      case .seePhone: return .seephone
      case .successBeep: return .successbeep
      case .errorBeep: return .errorbeep
      default: return .unknown
    }
  }

}

extension OnoMiuraCardData.OnoMiuraTrack2Data {
  /**
   * Convert the track 2 data from the byte array to the String
   */
  func asString() -> String? {
    return (self.sensitizedRaw?.byteArray).map{OnoSdkUtils().byteArrayToAsciiString(byteArray: $0)}
  }
}

extension MiuraManager {
  class GetNumericDataResult {
    let isSuccess: Bool
    let data: String
    
    init(isSuccessful: Bool, resultData: String) {
      isSuccess = isSuccessful
      data = resultData
    }
  }
  
  func getNumericData(
    automaticEnter: Bool,
    backlightOn: Bool,
    firstLineIndex: Int32,
    secondLineIndex: Int32,
    thirdLineIndex: Int32,
    numIntDigits: Int32,
    numFracDigits: Int32,
    numberToEditAscii: String?,
    onSuccess: @escaping (Double) -> Void,
    onFailure: @escaping (String, KotlinThrowable) -> Void
  ) throws {
    if (firstLineIndex < 0 || firstLineIndex > 65535) {
      onFailure(
        Messages().GENERIC_TIP_ERROR,
        KotlinThrowable(message: "getNumericData condition failed: 0 <= FirstLineIndex(\(firstLineIndex)) <= 65535")
      )
    }
    else if (secondLineIndex < 0 || secondLineIndex > 65535) {
      onFailure(
        Messages().GENERIC_TIP_ERROR,
        KotlinThrowable(message: "getNumericData condition failed: 0 <= SecondLineIndex(\(secondLineIndex)) <= 65535")
      )
    }
    else if (thirdLineIndex < 0 || thirdLineIndex > 65535) {
      onFailure(
        Messages().GENERIC_TIP_ERROR,
        KotlinThrowable(message: "getNumericData condition failed: 0 <= ThirdLineIndex(\(thirdLineIndex)) <= 65535")
      )
    }
    else if (numIntDigits < 0 || numIntDigits > 12) {
      onFailure(
        Messages().GENERIC_TIP_ERROR,
        KotlinThrowable(message: "getNumericData condition failed: 0 <= NumIntDigits(\(numIntDigits)) <= 12")
      )
    } else  if (numFracDigits < 0 && numFracDigits > 11) {
      onFailure(
        Messages().GENERIC_TIP_ERROR,
        KotlinThrowable(message: "getNumericData condition failed: 0 <= NumFracDigits(\(numFracDigits)) <= 11")
      )
    }else if (numIntDigits + numFracDigits <= 0 || numIntDigits + numFracDigits > 12) {
      onFailure(
        Messages().GENERIC_TIP_ERROR,
        KotlinThrowable(message: "getNumericData condition failed: 0 < NumIntDigits(\(numIntDigits)) + NumFracDigits(\(numFracDigits)) <= 12")
      )
    } else if (numberToEditAscii != nil && numberToEditAscii.map{Double($0)} == nil) {
      onFailure(
        Messages().GENERIC_TIP_ERROR,
        KotlinThrowable(message: "getNumericData condition failed: NumberToEditAscii(\(String(describing: numberToEditAscii))) must a valid number made up of ASCII charachers only")
      )
    } else if (numberToEditAscii?.count ?? 0 > 12) {
      onFailure(
        Messages().GENERIC_TIP_ERROR,
        KotlinThrowable(message: "getNumericData condition failed: NumberToEditAscii(\(String(describing: numberToEditAscii))) is longer than 12 chars")
      )
    } else if ((numberToEditAscii?.filter({$0.isWholeNumber}))?.count ?? 0 > (numIntDigits + numFracDigits)) {
      let maxDigitsDescription = "MaxDigits(\((numIntDigits + numFracDigits))) = (NumIntDigits(\(numIntDigits)) + NumFracDigits(\(numFracDigits))"
      let errorMsg = "getNumericData condition failed: NumberToEditAscii(\(String(describing: numberToEditAscii))) contains too many digit characters. \(maxDigitsDescription)"
      onFailure(Messages().GENERIC_TIP_ERROR, KotlinThrowable(message: errorMsg))
    } else {
      MPICommandExecutor.getNumericData(
        automaticEnter,
        securePrompts: Data( [
            UInt8(firstLineIndex >> 8 & 255),
            UInt8(firstLineIndex & 255),
            UInt8(secondLineIndex >> 8 & 255),
            UInt8(secondLineIndex & 255),
            UInt8(thirdLineIndex >> 8 & 255),
            UInt8(thirdLineIndex & 255)
          ]
        ),
        numericFormat: Data(
          [
            UInt8(numIntDigits),
            UInt8(numFracDigits)
          ]
        ),
        numericTimeout: 120,
        isBacklightOn: true) { (numericDataResult: Numeric_Data_Result, amount: String) in
          if numericDataResult == Numeric_Data_Result.selected {
            let amountSelected = Double(amount as String)
            if let amountSelected = amountSelected {
              onSuccess(amountSelected)
            } else {
              onFailure(Messages().GENERIC_TIP_ERROR, KotlinThrowable(message: "The data(\(amount)) from the card machine is not a invalid number."))
            }
          } else {
            onFailure(Messages().GENERIC_TIP_ERROR, KotlinThrowable(message: "getNumericData response was not successful, result = \(String(describing: numericDataResult))"))
          }
        }
    }
  }
}

// --
