//
//  MiuraService.swift
//  YocoPOS
//
//  Created by Shadrack Taeli on 2020/04/03.
//  Copyright © 2020 Facebook. All rights reserved.
//

import Foundation
import Ono

class MiuraSDKServiceProvider {
  private static var miuraService: MiuraService?
  
  var enviroment: Environment
  
  init(enviroment: Environment) {
    self.enviroment = enviroment
  }
  
  func getMiuraService() -> OnoMiuraServiceProtocol {
    // Avoid creating multiple `MiuraService` instances to avoid issues
    // where two clients are using the same device but have separate
    // command queues and listeners.
    if let miuraService = MiuraSDKServiceProvider.miuraService {
      return miuraService
    }
    
    if (enviroment == Environment.development && NSNumber(integerLiteral: Int(TARGET_OS_SIMULATOR)).boolValue) {
      return MockOnoMiuraService(
        dispatch: OnoSDK().getDispatch(dispatcherName: "MockOnoMiuraService"),
        isUnitTesting: false,
        deviceName: UIDevice.current.model,
        dateTimeString: {OnoSDKUtils.currentDateTime(format: $0)}
      
      )
    }
    
    let onoMiuraListener = OnoMiuraListener(dispatch: OnoSDK().getDispatch(dispatcherName: "OnoMiuraListener"))
    let onoMiuraService = MiuraSDKService(listener: onoMiuraListener)
    let miuraCommandQueue = TerminalCommandQueue(sdkService: onoMiuraService, dispatch: OnoSDK().getDispatch(dispatcherName: "MiuraCommandQueue"))
    
    onoMiuraListener.miuraCommandQueue = miuraCommandQueue
    return MiuraService(miuraCommandQueue: miuraCommandQueue, dispatch: OnoSDK().getDispatch(dispatcherName: "MiuraService"), updateService: UpdateService())
  }
}
