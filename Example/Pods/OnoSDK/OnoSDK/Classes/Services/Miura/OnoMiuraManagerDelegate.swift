//
//  MiuraDelegate.swift
//  YocoPOS
//
//  Created by Shadrack Taeli on 2020/04/07.
//  Copyright © 2020 Facebook. All rights reserved.
//

import Foundation
import Ono

class OnoMiuraManagerDelegate: MiuraManagerDelegate {
  func miuraManager(_ manager: MiuraManager, usbStatusChange usbStatusChanged: UsbStatus) {
    Logger().i(tag: tag, message: "usbStatusChange: \(usbStatusChanged)")
  }
  
  private let tag: String = OnoMiuraUtils().loggingTag(className: String(describing: OnoMiuraManagerDelegate.self))
  private let listener: OnoMiuraListenerProtocol
  internal var listenToConnectionHandlers: Bool = true

  init(listener: OnoMiuraListenerProtocol) {
    self.listener = listener
  }

  func miuraManager(_ manager: MiuraManager, accessoryDidDisconnect accessory: EAAccessory) {
    if (listenToConnectionHandlers) {
      Logger().i(tag: tag, message: "accessoryDidDisconnect: \(accessory)")
      listener.dispatch(MiuraActions.TerminalDisconnected())
    }
  }

  func miuraManager(_ manager: MiuraManager, keyPressed keyCode: UInt) {
    Logger().i(tag: tag, message: "keyPressed: \(keyCode)")
    
    let pressedButtonAction = OnoMiuraButton.Companion.init().fromKeyIndex(keyIndex: Int32(keyCode)).map {
      MiuraActions.KeyPressed(button: $0)
    }
    
    listener.dispatch(pressedButtonAction ?? MiuraActions.UnknownKeyPressed(pressedKeyIndex: Int32(keyCode)))
  }
  
  func miuraManager(_ manager: MiuraManager, cardStatusChange cardData: CardData) {
    Logger().i(tag: tag, message: "cardStatusChange")
    listener.dispatch(MiuraActions.CardData(onoMiuraCardData: cardData.toOnoMiuraCardData()))
  }

  func miuraManager(_ manager: MiuraManager, deviceStatusChange deviceStatus: DeviceStatus, text statusText: String) {
    Logger().i(tag: tag, message: "batteryStatusChange: deviceStatus: \(deviceStatus); statusText: \(statusText)")
    listener.onDeviceStatusChanged(miuraDeviceStatus: deviceStatus.toOnoMiuraDeviceStatus(), statusText: statusText)
  }

  func miuraManager(_ manager: MiuraManager, batteryStatusChange: ChargingStatus) {
    Logger().i(tag: tag, message: "batteryStatusChange: \(batteryStatusChange)")
  }

  func miuraManager(_ manager: MiuraManager, barCodeScan barCodeSrting: String) {
    Logger().i(tag: tag, message: "barCodeScan: \(barCodeSrting)")
  }

  func miuraManager(_ manager: MiuraManager, printerSledStatus printerStatus: PrinterSledStatus) {
    Logger().i(tag: tag, message: "printerM12status: \(printerStatus)")
  }

  func isEqual(_ object: Any?) -> Bool {
    return false
  }

  var hash: Int = 0

  var superclass: AnyClass? = nil

  func `self`() -> Self {
    return self
  }

  func perform(_ aSelector: Selector!) -> Unmanaged<AnyObject>! {
    return nil
  }

  func perform(_ aSelector: Selector!, with object: Any!) -> Unmanaged<AnyObject>! {
    return nil
  }

  func perform(_ aSelector: Selector!, with object1: Any!, with object2: Any!) -> Unmanaged<AnyObject>! {
    return nil
  }

  func isProxy() -> Bool {
    return false
  }

  func isKind(of aClass: AnyClass) -> Bool {
    return false
  }

  func isMember(of aClass: AnyClass) -> Bool {
    return false
  }

  func conforms(to aProtocol: Protocol) -> Bool {
    return false
  }

  func responds(to aSelector: Selector!) -> Bool {
    return false
  }

  var description: String = ""
}


