//
//  OnoWebRequestExtensions.swift
//  YocoPOS
//
//  Created by Shadrack Taeli on 2020/06/23.
//  Copyright © 2020 Yoco Technologies PTY LTD. All rights reserved.
//

import Foundation
import Gloss
import Ono

extension OnoWebRequest {
  /* Makes a POST api request with callbacks */
  func doPost<T: JSONDecodable>(
    onSuccess: @escaping (_ code: Int, _ data: T) -> Void,
    onFailure: @escaping (_ error: ApiError) -> Void
  ) {
    self.post({ (statusCode, response) in
      self.handleSuccessResponse(code: statusCode, response: response, onSuccess: onSuccess, onFailure: onFailure)
    }, failure: { (statusCode, error, response) in
      self.handleFailureResponse(code: statusCode, error: error as NSError?, response: response, onFailure: onFailure)
    })
  }
  
  /* Makes a GET api request with callbacks */
  func doGet<T: JSONDecodable>(
    onSuccess: @escaping (_ code: Int, _ data: T) -> Void,
    onFailure: @escaping (_ error: ApiError) -> Void
  ) {
    self.get({ (statusCode, response) in
      self.handleSuccessResponse(code: statusCode, response: response, onSuccess: onSuccess, onFailure: onFailure)
    }) { (statusCode, error, response) in
      self.handleFailureResponse(code: statusCode, error: error as NSError?, response: response, onFailure: onFailure)
    }
  }
  
  static func getDefaultHeaderString(key:String, defaultValue: String) -> String {
    if let headerValue = OnoWebRequest.defaultHeaders()[key] as? String {
      return headerValue
    } else {
      Logger().d(tag: OnoSDK().loggingTag(moduleName: "api", className: "OnoWebRequest"), message: "api service default header didn't have a value for \(key)")
      return defaultValue
    }
  }
  
  private func handleSuccessResponse<T: JSONDecodable>(
    code: Int,
    response: [AnyHashable : Any]?,
    onSuccess: (_ code: Int, _ data: T) -> Void,
    onFailure: (_ error: ApiError) -> Void
  ) {
    if let response = response as? JSON, let dto = T(json: response) {
      onSuccess(code, dto)
    } else {
      onFailure(ApiError(code: code, message: "the response had missing fields or a NULL body", type: ApiErrorType.Server))
    }
  }
  
  private func handleFailureResponse(
    code: Int,
    error: NSError?,
    response: [AnyHashable : Any]?,
    onFailure: (_ error: ApiError) -> Void
  ) {
    if let response = response as? JSON, let apiError = ApiError(json: response) {
      (apiError.code != nil) ? onFailure(apiError) : onFailure(ApiError(code: code, message: apiError.message, type: apiError.type, transactionId: apiError.transactionId, errorMessage: apiError.errorMessage))
    } else {
      onFailure(ApiError(code: code, message: error?.localizedDescription, type: getErrorType(error: error)))
    }
  }
  
  private func getErrorType(error: NSError?) -> ApiErrorType {
    switch error?.code {
    case NSURLErrorTimedOut:
      return ApiErrorType.Timeout
    case NSURLErrorNotConnectedToInternet, NSURLErrorNetworkConnectionLost, NSURLErrorCannotConnectToHost:
      return ApiErrorType.Connection
    default:
      return ApiErrorType.Error
    }
  }
  
}
