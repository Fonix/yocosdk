//
//  ApiError.swift
//  YocoPOS
//
//  Created by Shadrack Taeli on 2020/06/23.
//  Copyright © 2020 Yoco Technologies PTY LTD. All rights reserved.
//

import Foundation
import Gloss

enum ApiErrorType {
  case Server
  case Connection
  case Timeout
  case Error
}

struct ApiError: JSONDecodable, CustomStringConvertible {
  let code: Int?
  let message: String?
  let errorMessage: String?
  let transactionId: String?
  let type: ApiErrorType
  
  init?(json: JSON) {
    code = "status" <~~ json
    message = "message" <~~ json
    errorMessage = "errorMessage" <~~ json
    transactionId = "transactionId" <~~ json
    type = ApiErrorType.Server
  }
  
  init(code: Int?, message: String?, type: ApiErrorType, transactionId: String? = nil, errorMessage: String? = nil) {
    self.code = code
    self.message = message
    self.transactionId = transactionId
    self.type = type
    self.errorMessage = errorMessage
  }
  
  var description: String {
    return "\(self.code.map{String($0)} ?? "nil"): \(self.message ?? "nil")"
  }
}
