//
//  OnoV2ChargeResponseDTO.swift
//  OnoSDK
//
//  Created by Nkokhelo Mhlongo on 2020/10/22.
//

import Foundation
import Gloss
import Ono

struct ChargeApiV2ResponseDTO : JSONDecodable {
  let chargeResult: String
  let transactionId: String
  let errorMessage: String?
  let isoResponseCode: String?
  let paymentInfo: PaymentInfo
  let chargeResultMessage: String?
  
  init?(json: JSON) {
    guard
      let chargeResult: String = "chargeResult" <~~ json,
      let transactionId: String = "transactionId" <~~ json,
      let paymentInfo: PaymentInfo = "paymentInfo" <~~ json
      else {
      return nil
    }
    
    self.paymentInfo = paymentInfo
    self.chargeResult = chargeResult
    self.transactionId = transactionId
    self.errorMessage = "errorMessage" <~~ json
    self.isoResponseCode = "isoResponseCode" <~~ json
    self.chargeResultMessage = "chargeResultMessage" <~~ json
  }
}

struct PaymentInfo : JSONDecodable {
  let masterpassInfo: MasterpassInfo
  
  init?(json: JSON) {
    guard let masterpassInfo: MasterpassInfo = "masterpassInfo" <~~ json else {
      return nil
    }
    self.masterpassInfo = masterpassInfo
  }
}

struct MasterpassInfo : JSONDecodable {
  let rrn: String?
  let code: String
  let wallet: String?
  let msisdn: String?
  let cardType: String?
  let binNumber: String?
  let accountType: String?
  let panLast4Digits: String?
  let cardHolderName: String?
  let authorizationId: String?
  
  init?(json: JSON) {
    guard let code: String = "code" <~~ json else {
      return nil
    }
    
    self.code = code
    self.rrn = "rrn" <~~ json
    self.wallet = "wallet" <~~ json
    self.msisdn = "msisdn" <~~ json
    self.cardType = "cardType" <~~ json
    self.binNumber = "binNumber" <~~ json
    self.accountType = "accountType" <~~ json
    self.panLast4Digits = "panLast4Digits" <~~ json
    self.cardHolderName = "cardHolderName" <~~ json
    self.authorizationId = "authorizationId" <~~ json
  }
}
