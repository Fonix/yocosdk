//
//  RKIResponseDTO.swift
//  Analytics
//
//  Created by Shadrack Taeli on 2020/08/24.
//  Copyright © 2020 Yoco Technologies PTY LTD. All rights reserved.
//

import Foundation
import Gloss
import Ono

struct RKIResponseDTO: JSONDecodable {
  let rkiResponseData: SensitiveMap
  
  init?(json: JSON) {
    rkiResponseData = SensitiveMap(
      valueMap: json.compactMapValues { $0 as? String }
    )
  }
}
