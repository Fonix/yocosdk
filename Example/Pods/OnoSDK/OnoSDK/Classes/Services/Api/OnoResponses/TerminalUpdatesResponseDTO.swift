//
//  TerminalUpdatesResponseDTO.swift
//  YocoPOS
//
//  Created by Shadrack Taeli on 2020/06/24.
//  Copyright © 2020 Yoco Technologies PTY LTD. All rights reserved.
//

import Foundation
import Ono
import Gloss

struct TerminalUpdatesResponseDTO: JSONDecodable {
  let updatesAvailable: [AvailableUpdate]
  
  init?(json: JSON) {
    let updatesAvailable: [AvailableUpdateDTO]? = "updatesAvailable" <~~ json
    self.updatesAvailable = updatesAvailable?.map { $0.toAvailableUpdate() } ?? []
  }
}

struct AvailableUpdateDTO: JSONDecodable {
  let files: [UpdateFileDTO]
  let installation: String
  let message: String?
  
  init?(json: JSON) {
    guard let files: [UpdateFileDTO] = "files" <~~ json,
      let installation: String = "installation" <~~ json else {
        return nil
    }
    
    self.files = files
    self.installation = installation
    self.message = "message" <~~ json
  }
}

struct UpdateFileDTO: JSONDecodable {
  let url: String
  let md5sum: String
  let size: Int
  let type: String
  let version: String
  
  init?(json: JSON) {
    guard let url: String = "url" <~~ json,
      let md5sum: String = "md5sum" <~~ json,
      let size: Int = "size" <~~ json,
      let type: String = "type" <~~ json,
      let version: String = "version" <~~ json else {
        return nil
    }
    
    self.url = url
    self.md5sum = md5sum
    self.size = size
    self.type = type
    self.version = version
  }
}


