//
//  ApiResponseDTO.swift
//  YocoPOS
//
//  Created by Shadrack Taeli on 2020/06/24.
//  Copyright © 2020 Yoco Technologies PTY LTD. All rights reserved.
//

import Foundation
import Gloss

struct ApiResponseDTO: JSONDecodable {
  let status: Int?
  let message: String?
  
  init?(json: JSON) {
    status = "status" <~~ json
    message = "message" <~~ json
  }
}
