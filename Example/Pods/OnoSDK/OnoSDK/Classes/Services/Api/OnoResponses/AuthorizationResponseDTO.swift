//
//  AuthorizeResponseDTO.swift
//  YocoPOS
//
//  Created by Shadrack Taeli on 2020/06/23.
//  Copyright © 2020 Yoco Technologies PTY LTD. All rights reserved.
//

import Foundation
import Gloss
import Ono

struct AuthorizationResponseDTO: JSONDecodable {
  let transactionId: String
  let isoResponseCode: String
  let chargeResult: String
  let signatureRequired: Bool?
  let chargeResultMessage: String?
  let arpc: String?
  
  init?(json: JSON) {
    guard let transactionId: String = "transactionId" <~~ json,
      let isoResponseCode: String = "isoResponseCode" <~~ json,
      let chargeResult: String = "chargeResult" <~~ json else {
      return nil
    }
    
    self.transactionId = transactionId
    self.chargeResult = chargeResult
    self.isoResponseCode = isoResponseCode
    self.signatureRequired = "signatureRequired" <~~ json
    self.chargeResultMessage = "chargeResultMessage" <~~ json
    self.arpc = "arpc" <~~ json
  }
}

