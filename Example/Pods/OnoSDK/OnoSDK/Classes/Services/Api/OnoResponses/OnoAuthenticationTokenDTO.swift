//
//  OnoAthenticationTokenDTO.swift
//  Alamofire
//
//  Created by Nkokhelo Mhlongo on 2020/10/27.
//

import Foundation
import Gloss
import Ono

struct OnoAuthenticationTokenDTO: JSONDecodable {
  let data: AuthenticationTokenData
  init?(json: JSON) {
    guard
      let data: AuthenticationTokenData = "data" <~~ json else {
      return nil
    }
    self.data = data
  }
}

struct AuthenticationTokenData: JSONDecodable {
  let token: String?
  init?(json: JSON) {
    self.token = "token" <~~ json
  }
}

