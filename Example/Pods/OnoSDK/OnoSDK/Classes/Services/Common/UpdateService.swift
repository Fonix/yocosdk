//
//  UpdateService.swift
//  YocoPOS
//
//  Created by Mandisa Mjamba on 2019/07/18.
//  Copyright © 2019 Yoco. All rights reserved.
//

import Foundation
import Ono

class UpdateService: IOnoUpdateService {
  let TAG = "OnoSDK> UpdateService>"
  
  let defaults = UserDefaults.standard
  
  func downloadAndVerifyUpdate(availableUpdate: AvailableUpdate, onComplete: @escaping ([String : KotlinByteArray]) -> Void, onFailure: @escaping (String) -> Void) {
    Logger().i(tag: TAG, message: "Download update with \(availableUpdate.files.count) files")
    
    var completedDownloads: [String : KotlinByteArray] = [:]
    
    for file in availableUpdate.files {
      if let url = URL(string: file.url) {
        Logger().d(tag: TAG, message: "Start downloading \(file.type) from: \(file.url)")
        DownloadService.load(
          url: url,
          onComplete: { data in
            let md5sum = data.md5
            
            if (md5sum == file.md5sum) {
              Logger().i(tag: self.TAG, message: "Finished downloading \(file.type)")
              completedDownloads[file.type] = data.toKotlinByteArray()
              if (completedDownloads.count == availableUpdate.files.count) {
                onComplete(completedDownloads)
              }
            } else {
              Logger().e(tag: self.TAG, message: "\(file.type) md5s didn't match expected: \(file.md5sum) got \(md5sum)")
              onFailure("Download is corrupted")
            }
          },
          onError: { error in
            Logger().e(tag: self.TAG, message: "\(file.type) onError: \(String(describing: error))")
            onFailure(error ?? "Download error")
          }
        )
      }
    }
  }
  
  func dataToHexString(data: Data) -> String {
    return data.map { String(format: "%02hhx", $0) }.joined()
  }
}
