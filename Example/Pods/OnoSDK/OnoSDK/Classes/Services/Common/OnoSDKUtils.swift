//
//  OnoUtils.swift
//  YocoPOS
//
//  Created by Nkokhelo Mhlongo on 2019/10/21.
//  Copyright © 2019 Facebook. All rights reserved.
//

import Foundation
import Ono

class OnoSDKUtils {

   /// - Parameters:
   ///     - format: The format specifying how the current date time string should be formatted, default is yyyyMMddHHmmss.
   /// - Returns: The string representing the current date time formatted as specified by the provided dateTimeFormat.
  static func currentDateTime(format: String = "yyyyMMddHHmmss") -> String {
    let formatter = DateFormatter()
    formatter.dateFormat = format
    formatter.locale = Locale(identifier: "en_US_POSIX")
    return formatter.string(from: Date())
  }
}
