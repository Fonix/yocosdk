//
//  CoreApiService.swift
//  YocoPOS
//
//  Created by Altus van der Merwe on 2019/06/04.
//  Copyright © 2019 Yoco. All rights reserved.
//

import Foundation
import Ono

class CoreApiService: CoreApiServiceProtocol {
  var TAG = "OnoSDK> CoreApiService> "
  var baseURL = "https://core.yoco.co.za";
  
  private let CACHE_KEY_PREFIX = "ono:"
  
  override func updateClientTransactionId(clientTransactionId: String?) {
    DispatchQueue.main.async {
      OnoWebRequest.setDefaultHeader(clientTransactionId, forKey: ApiHeaderKey.xCorrelationId.value)
    }
  }
  
  override func clearAuthentication() {
    Logger().i(tag: TAG, message: "clearAuthenticationToken")
    OnoWebRequest.setDefaultHeader(nil, forKey: ApiHeaderKey.coreAuthToken.value)
    OnoWebRequest.setDefaultHeader(nil, forKey: ApiHeaderKey.sdkIntegrationSecret.value)
    OnoWebRequest.setDefaultHeader(nil, forKey: ApiHeaderKey.sdkIntegrationToken.value)
  }
  
  override func baseURLUpdated(baseUrl: String) {
    Logger().i(tag: TAG, message: "baseURLUpdated baseUrl: \(baseUrl)")
    self.baseURL = baseUrl
    OnoWebRequest.setBaseURL(baseUrl)
  }
  
  override func authenticationUpdated(authenticationToken: SensitiveString?, integrationAuthentication: IntegrationAuthentication?) {
    DispatchQueue.main.async {
      Logger().i(tag: self.TAG, message: "authenticationTokenUpdated: \(String(describing: authenticationToken)) \(String(describing: integrationAuthentication))")
      if let authenticationToken = authenticationToken {
        OnoWebRequest.setDefaultHeader(authenticationToken.value, forKey: ApiHeaderKey.coreAuthToken.value)
      }
      if let integrationAuthentication = integrationAuthentication {
        OnoWebRequest.setDefaultHeader(integrationAuthentication.secret.value, forKey: ApiHeaderKey.sdkIntegrationSecret.value)
        OnoWebRequest.setDefaultHeader(integrationAuthentication.token.value, forKey: ApiHeaderKey.sdkIntegrationToken.value)
      }
    }
  }
  
  override func transactionApproved(clientTransactionId: String, debuggingInfo: DebuggingInfo) {
    Logger().i(tag: TAG, message: "transactionApproved: \(clientTransactionId)")
    
    let debugInfoJson = debuggingInfo.getMappedDebugData()
    
    OnoWebRequest.init(path: "/transactions/\(clientTransactionId)/approved")
      .withEndPoint("client")
      .withJSONBody(debugInfoJson)
      .withRetries(true)
      .post({(statusCode, response) in
        self.dispatch(ActivityTimelineActions.ClearAllActivityTiming(message: "All data successfully was sent to core"))
      }, failure: { (statusCode, error, json) in
        Logger().i(tag: self.TAG, message: String("Unable to tell core that the client saw the transaction as approved: \(statusCode) \(error) \(json)"))
      })
  }
  
  override func transactionFailed(clientTransactionId: String, debuggingInfo: DebuggingInfo) {
    Logger().i(tag: TAG, message: "transactionFailed: \(clientTransactionId)")
    
    let debugInfoJson = debuggingInfo.getMappedDebugData()
    
    OnoWebRequest.init(path: "/transactions/\(clientTransactionId)/failure")
      .withEndPoint("client")
      .withJSONBody(debugInfoJson)
      .post({(statusCode, response) in
        self.dispatch(ActivityTimelineActions.ClearAllActivityTiming(message: "All data successfully was sent to core"))
      }, failure: {(statusCode, error, json) in
        Logger().i(tag: self.TAG, message: String("Unable to tell core that the client saw the transaction as failed: \(statusCode) \(error) \(json)"))
      })
  }
  
  override func fetchOnoAuthenticationToken(onoMerchantId: String, coreUserAuthenticationToken: SensitiveString?, coreIntegrationAuthentication: IntegrationAuthentication?) {
    if let cachedToken = fetchCacheForKey(onoMerchantId: onoMerchantId) {
      Logger().i(tag: self.TAG, message: "Cached transaction token found")
      self.dispatch(OnoActions.FetchedMerchantSecret(secret: SensitiveString(value: cachedToken)))
    } else {
      if let coreUserAuthToken = coreUserAuthenticationToken {
        OnoWebRequest.setDefaultHeader(
          OnoWebRequest.getDefaultHeaderString(key: ApiHeaderKey.coreAuthToken.value, defaultValue: coreUserAuthToken.value),
          forKey: ApiHeaderKey.coreAuthToken.value
        )
      }
      if let coreIntergrationSecret = coreIntegrationAuthentication?.secret {
        OnoWebRequest.setDefaultHeader(
          OnoWebRequest.getDefaultHeaderString(key: ApiHeaderKey.sdkIntegrationSecret.value, defaultValue: coreIntergrationSecret.value),
          forKey: ApiHeaderKey.sdkIntegrationSecret.value
        )
      }
      if let coreIntergrationToken = coreIntegrationAuthentication?.token {
        OnoWebRequest.setDefaultHeader(
          OnoWebRequest.getDefaultHeaderString(key: ApiHeaderKey.sdkIntegrationToken.value, defaultValue: coreIntergrationToken.value),
          forKey: ApiHeaderKey.sdkIntegrationToken.value
        )
      }
      
      OnoWebRequest.init(path:"/ono/transactionToken")
        .withJSONBody([String: String]())
        .withEndPoint("client")
        .doPost(
          onSuccess: {(statusCode: Int, responseData: OnoAuthenticationTokenDTO) in
            if let token = responseData.data.token {
              self.saveCacheForKey(onoMerchantId: onoMerchantId, value: token)
              self.dispatch(OnoActions.FetchedMerchantSecret(secret: SensitiveString(value: token)))
            } else {
              self.dispatch(CoreActions.RetryFetchOnoAuthToken(
                onoMerchantId: onoMerchantId,
                errorDetails: Http.NullOnoAuthenticationToken(statusCode: statusCode.toKotlinInt()))
              )
            }
        },
          onFailure: { (error) in self.dispatch(CoreActions.RetryFetchOnoAuthToken(onoMerchantId: onoMerchantId, errorDetails: self.getErrorDetails(apiError: error))) }
      )
    }
  }
  
  private func fetchCacheForKey(onoMerchantId: String) -> String? {
    let token = UserDefaults.standard.string(forKey: "\(CACHE_KEY_PREFIX)\(onoMerchantId)")
    let wasFound = (token == nil) ? "Token NOT FOUND from the cache" : "Token FOUND from the cache"
    Logger().d(tag: TAG, message: "\(wasFound) & the standard userDefaults count WHEN saveCacheForKey was \(UserDefaults.standard.dictionaryRepresentation().count)")
    return token
  }
  
  private func saveCacheForKey(onoMerchantId: String, value: String) {
    Logger().d(tag: TAG, message: "Standard userDefaults count BEFORE saveCacheForKey was \(UserDefaults.standard.dictionaryRepresentation().count)")
    UserDefaults.standard.set(value, forKey: "\(CACHE_KEY_PREFIX)\(onoMerchantId)")
    Logger().d(tag: TAG, message: "Standard userDefaults count AFTER saveCacheForKey was \(UserDefaults.standard.dictionaryRepresentation().count)")
  }
  
  override func clearCachedTransactionToken() {
    let maybeOnoMerchantId = (OnoWebRequest.defaultHeaders()?[ApiHeaderKey.onoMerchantId] as? String)
    
    if let onoMerchantId = maybeOnoMerchantId {
      Logger().d(tag: TAG, message: "Standard userDefaults count BEFORE clearCacheForKey was \(UserDefaults.standard.dictionaryRepresentation().count)")
      UserDefaults.standard.removeObject(forKey: "\(CACHE_KEY_PREFIX)\(onoMerchantId)")
      Logger().d(tag: TAG, message: "Standard userDefaults count BEFORE clearCacheForKey was \(UserDefaults.standard.dictionaryRepresentation().count)")
      
      // Fetch a new token after clearing the currently cached token.
      dispatch(CoreActions.FetchOnoAuthenticationToken(onoMerchantId: onoMerchantId))
    } else {
      Logger().e(tag: TAG, message: "clearCacheForKey failed because the cache Key was NIL")
    }
  }
  
  public func submitAnalyticsEvent(eventName: String, data: [String : String]) {
    var event: [String: Any] = [:]
    event["name"] = eventName
    event["meta"] = data
    event["category"] = "OnoSDK"
    OnoWebRequest.init(path: "/tracking/event/")
      .withEndPoint("common")
      .withJSONBody(event)
      .post({(statusCode, response) in
        
      }, failure: {(statusCode, error, json) in
        
      })
  }
  
  override func linkThisDeviceWithBusiness(deviceMap: [String: String]) {
    OnoWebRequest.init(path:"/hardwareDevices/")
      .withJSONBody(deviceMap)
      .withEndPoint("client")
      .withRetries(false)
      .doPost(
        onSuccess: { (_ , _ : ApiResponseDTO) in self.dispatch(CoreActions.DeviceAndBusinessAssociatedSuccessfully()) },
        onFailure: { (error) in self.dispatch(CoreActions.DeviceAndBusinessAssociationFailed(coreErrorDetails: self.getErrorDetails(apiError: error))) }
    )
  }
  
  override func linkThisDeviceWithTerminal(deviceTerminalMap: [String: String]) {
    OnoWebRequest.init(path: "/readers/connectedToReader/")
      .withJSONBody(deviceTerminalMap)
      .withEndPoint("client")
      .withRetries(false)
      .doPost(
        onSuccess: { (_ , _ : ApiResponseDTO) in self.dispatch(CoreActions.TerminalAndBusinessAssociatedSuccessfully()) },
        onFailure: { (error) in self.dispatch(CoreActions.TerminalAndDeviceAssociationFailed(coreErrorDetails: self.getErrorDetails(apiError: error))) }
    )
  }
  
  override func rkiSuccessful(serialNumber: String) {
    OnoWebRequest.init(path: "/terminal/rkiSuccess")
      .withJSONBody(["serialNumber": serialNumber])
      .withEndPoint("client")
      .withRetries(false)
      .doPost(
        onSuccess: { (_ , _ : ApiResponseDTO) in self.dispatch(CoreActions.RkiSuccessReported(serialNumber: serialNumber, coreErrorDetails: nil)) },
        onFailure: { (error) in self.dispatch(CoreActions.RkiSuccessReported(serialNumber: serialNumber, coreErrorDetails: self.getErrorDetails(apiError: error))) }
    )
  }
  
  private func getErrorDetails(apiError: ApiError) -> Http {
    if apiError.type == ApiErrorType.Connection {
      return Http.NoConnection(statusCode: apiError.code?.toKotlinInt(), message: apiError.message)
    } else {
      return Http.Companion().otherNetworkError(statusCode: apiError.code?.toKotlinInt(), message: apiError.message)
    }
  }
}
