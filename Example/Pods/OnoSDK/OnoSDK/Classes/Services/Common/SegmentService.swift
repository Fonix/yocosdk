//
//  SegmentService.swift
//  YocoPOS
//
//  Created by Altus van der Merwe on 2019/07/08.
//  Copyright © 2019 Yoco. All rights reserved.
//

import Foundation
import Ono

public class SegmentService : ISegmentService {
  
  let core: CoreApiService
  
  internal init(coreApi: CoreApiService) {
    core = coreApi
  }
  
  public func trackEvent(eventName: String, data: [String : String]) {
    core.submitAnalyticsEvent(eventName: eventName, data: data)
  }
}

