//
//  OnoApiService.swift
//  YocoPOS
//
//  Created by Altus van der Merwe on 2019/06/04.
//  Copyright © 2019 Yoco. All rights reserved.
//

import Foundation
import Ono

class OnoApiService: OnoApiServiceProtocol {
  let TAG = "OnoSDK> OnoApiService> "
  func RequestTAG(requestID: Int32) -> String {
    return "\(TAG) \(requestID)"
  }
  
  var baseURL = "https://api.onoconnect.com";
  var merchantId: String? = nil
  var merchantSecret: String? = nil
  
  override func updateClientTransactionId(clientTransactionId: String?) {
    DispatchQueue.main.async {
      OnoWebRequest.setDefaultHeader(clientTransactionId, forKey: ApiHeaderKey.xCorrelationId.value)
    }
  }
  
  override func baseURLUpdated(baseUrl: String) {
    Logger().i(tag: TAG, message: "baseURLUpdated \(baseUrl)")
    baseURL = baseUrl
  }
  
  override func merchantIdentityUpdated(id: String) {
    DispatchQueue.main.async {
      Logger().i(tag:self.TAG, message: "merchantIdentityUpdated identifier: \(id)")
      self.merchantId = id
      
      OnoWebRequest.setDefaultHeader(id, forKey: ApiHeaderKey.onoMerchantId.value)
    }
  }
  
  override func merchantSecretUpdated(secret: SensitiveString) {
    DispatchQueue.main.async {
      Logger().i(tag: self.TAG, message: "merchantSecretedUpdated")
      
      OnoWebRequest.setDefaultHeader(secret.value, forKey: ApiHeaderKey.onoMerchantToken.value)
    }
  }
  
  override func clearMerchant() {
    Logger().i(tag: TAG, message: "clearMerchant")
    merchantSecret = nil
    merchantId = nil
  }
  
  override func clearMerchantSecret() {
    Logger().i(tag: TAG, message: "clearMerchantSecret")
    merchantSecret = nil
  }
  
  override func checkForUpdates(terminal: Terminal, requestID: Int32) {
    Logger().i(tag: TAG, message: "checkForUpdates terminal: \(terminal) requestID: \(requestID) NOT IMPLEMENTED")
    
    OnoWebRequest().withBaseURLAndPath(self.baseURL, path: "/terminal/fetchUpdateInfo")
      .withJSONBody(TerminalUpdatesBody(inTerminal: terminal).getMappedData())
      .withRetries(false)
      .doPost(onSuccess: { (code, data: TerminalUpdatesResponseDTO) in
        // TODO: This currently only handles emvConfig and automatic updates
        //        if let emvConfigMap = (response?["updatesAvailable"] as? [AnyHashable: Any])?["emvConfig"] as? [AnyHashable: Any] {
        //          let files = (emvConfigMap["files"] as! [[AnyHashable: Any]]).map({
        //            UpdateFile.init(
        //              url: $0["url"] as! String,
        //              md5sum: $0["md5sum"] as! String,
        //              size: $0["size"] as! Int64,
        //              type: $0["type"] as! String,
        //              version: $0["version"] as! String
        //            )
        //          })
        //          let emvConfig = AvailableUpdate.init(
        //            updateFiles: files,
        //            installation: UpdateInstallationType.automatic,
        //            message: emvConfigMap["message"] as? String,
        //            version: emvConfigMap["version"] as! String
        //          )
        //
        //          self.dispatch(TerminalActions.UpdatesAvailable(
        //            availableUpdates: [TerminalUpdateType.emvConfig : emvConfig]
        //          ))
        //        } else {
        //          self.dispatch(TerminalActions.UpdatesAvailable(
        //            availableUpdates: [:]
        //          ))
        //        }
        
        Logger().i(tag: self.TAG, message: data.updatesAvailable.description)
        
        self.dispatch(TerminalActions.UpdatesAvailable(
          availableUpdates: data.updatesAvailable
        ))
      }, onFailure: {(error) in
        self.dispatch(OnoActions.CheckForUpdatesFailed(
          errorDetails: self.getErrorDetails(apiError: error),
          httpStatusCode: error.code?.toKotlinInt()
        ))
      })
  }
  
  override func authorizeTransaction(authorizationBody: OnoAuthorizationBody, bin: String?, requestID: Int32) {
    let request: OnoWebRequest = OnoWebRequest().withBaseURLAndPath(self.baseURL, path: "/transactions")
      .withJSONBody(authorizationBody.toMap() as [AnyHashable : Any])
      .withRetries(false)
    
    let finalRequest: OnoWebRequest = bin.map { actualBin in request.withCustomHeaders(["BIN": actualBin]) } ?? request
    
    finalRequest.doPost(onSuccess: { (code, data: AuthorizationResponseDTO) in
      self.handleTransactionAuthorizationSuccess(
        clientTransactionId: authorizationBody.clientTransactionId,
        data: data,
        responseCode: code
      )
    }, onFailure: { (error) in
      switch error.type {
      case .Connection, .Timeout:
        self.dispatch(TransactionCommands.RetryTransactionAuthorization(clientTransactionId: authorizationBody.clientTransactionId, attempt: 0))
      default:
        self.handleTransactionAuthorizationFailure(
          apiError: error,
          maybeOnoTransactionId: error.transactionId
        )
      }
    })
  }
  
  override func finalizeTransaction(transactionId: String, transactionState: TransactionState, finalizeState: FinalizeState, finalizationBody: OnoFinalizationBody, timeoutSeconds: Int32, requestID: Int32) {
    
    DispatchQueue.main.asyncAfter(wallDeadline: .now() + .seconds(Int(timeoutSeconds))) {
      self.dispatch(
        OnoActions.FinalizationTimeoutReached(
          transactionId: transactionId,
          transactionState: transactionState,
          finalizeState: finalizeState,
          finalizationBody: finalizationBody
        )
      )
    }
    
    let path = "/transactions/\(transactionId)/\(finalizeState.name.lowercased())"
    
    OnoWebRequest().withBaseURLAndPath(self.baseURL, path: path)
      .withJSONBody(finalizationBody.getMappedData())
      .withRetries(false)
      .doPost(onSuccess: { (_ , _ : ApiResponseDTO) in
        self.dispatch(
          OnoActions.FinalizationResponse(
            transactionState: transactionState,
            message: nil,
            transactionId: transactionId
          )
        )
      }, onFailure: { (error) in
        self.dispatch(
          OnoActions.FinalizationFailed(
            clientTransactionID: transactionId,
            finalizeState: finalizeState,
            finalizationBody: finalizationBody,
            errorDetails: self.getErrorDetails(apiError: error),
            httpStatusCode: error.code?.toKotlinInt()
          )
        )
      })
  }
  
  override func submitTransactionSignature(transactionId: String, signatureBody: OnoSignatureBody, timeoutSeconds: Int32, requestID: Int32) {
    
    DispatchQueue.main.asyncAfter(wallDeadline: .now() + .seconds(Int(timeoutSeconds))) {
      self.dispatch(OnoActions.SignatureSubmissionTimeoutReached(transactionId: transactionId, signatureBody: signatureBody))
    }
    
    Logger().i(tag: TAG, message: "About to attempt signature submission for TransactionId: \(transactionId)")
    
    let path = "/transactions/\(transactionId)/signature"
    
    OnoWebRequest().withBaseURLAndPath(self.baseURL, path: path)
      .withJSONBody(signatureBody.getMappedData())
      .withRetries(false)
      .doPost(onSuccess: { (_, _: ApiResponseDTO) in
        self.dispatch(
          OnoActions.SignatureSubmittedSuccessfully(transactionId: transactionId)
        )
      }, onFailure: { (error) in
        self.dispatch(
          OnoActions.SubmitSignatureFailed(
            transactionId: transactionId,
            signatureBody: signatureBody,
            errorDetails: self.getErrorDetails(apiError: error),
            httpStatusCode: error.code?.toKotlinInt()
          )
        )
      })
  }
  
  override func silentRejectTransaction(transaction: Transaction, attempt: Int32) {
    let (path, body) = transaction.switchTransactionId.map { tranId in
      let finalizationPath = "/transactions/\(tranId)/reject"
      let finalizationBody = OnoFinalizationBody(tlv: nil, transactionId: tranId, sdkTransactionId: transaction.sdkTransactionId, attempt: attempt).getMappedData()
      return (finalizationPath, finalizationBody)
      } ?? { () -> (String, [String : Any]) in
        let finalizationPath = "/transactions/by/clientTransactionId/\(transaction.clientTransactionId)/reject"
        let finalizationBody = OnoRejectByClientIdBody(transaction: transaction).getMappedData()
        return (finalizationPath, finalizationBody)
      }()
    
    OnoWebRequest().withBaseURLAndPath(self.baseURL, path: path)
      .withJSONBody(body)
      .withRetries(false)
      .doPost(onSuccess: { (code, _: ApiResponseDTO) in
        self.dispatch(
          OnoActions.BackgroundRejectResult(
            transaction: transaction,
            attempt: attempt,
            httpStatusCode:
            code.toKotlinInt(),
            errorDetails: nil
          )
        )
      }, onFailure: { (error) in
        self.dispatch(
          OnoActions.BackgroundRejectResult(
            transaction: transaction,
            attempt: attempt,
            httpStatusCode: error.code?.toKotlinInt(),
            errorDetails: self.getErrorDetails(apiError: error)
          )
        )
      })
  }
  
  override func delaySilentRejectCall(transaction: Transaction, attempt: Int32) {
    let delay: Int64 = (Int64(exactly: OnoSdkUtils().rejectAttemptDelayInSecond(attempt: attempt)) ?? 128) * 1000
    OnoSDK().timerFactory.startTimer(
      runAfter: delay,
      block: {
        self.dispatch(TransactionCommands.BackgroundRejectTransaction(transaction: transaction, attempt: attempt+1))
    }
    )
  }
  
  override func retryTransactionAuthorization(clientTransactionId: String, attempt: Int32, requestID: Int32) {
    let path = "/transactions/by/clientTransactionId/\(clientTransactionId)/lastResponse"
    OnoSDK().timerFactory.startTimer(runAfter: Int64(attempt) * 1000) {
      OnoWebRequest().withBaseURLAndPath(self.baseURL, path: path)
        .withRetries(false)
        .doGet(onSuccess: { (code , data : AuthorizationResponseDTO) in
          self.handleTransactionAuthorizationSuccess(
            clientTransactionId: clientTransactionId,
            data: data,
            responseCode: code
          )
        }, onFailure: { (error) in
          self.handleTransactionAuthorizationFailure(
            apiError: error,
            maybeOnoTransactionId: error.transactionId
          )
        })
    }
  }
  
  override func postRkiRequest(terminal: Terminal, rkiRequestMap: SensitiveMap, requestID: Int32, attempt: Int32) {
    let path = "/v1/terminals/\(terminal.manufacturer.name.lowercased())/rki"
    
    OnoWebRequest().withBaseURLAndPath(self.baseURL, path: path)
      .withJSONBody(rkiRequestMap.valueMap)
      .withRetries(false)
      .doPost(onSuccess: { (code, data: RKIResponseDTO) in
        self.dispatch(
          OnoActions.RkiServerResponse(
            terminal: terminal,
            rkiResponseData: data.rkiResponseData,
            requestID: requestID,
            httpStatusCode: code.toKotlinInt()
          )
        )
      }, onFailure: { (error) in
        self.dispatch(
          OnoActions.RkiServerRequestFailed(
            terminal: terminal,
            rkiRequestMap: rkiRequestMap,
            errorDetails: self.getErrorDetails(apiError: error),
            requestID: requestID,
            attempt: attempt,
            httpStatusCode: error.code?.toKotlinInt()
          )
        )
      })
  }
  
  override func retryRkiServerRequest(terminal: Terminal, rkiRequestMap: SensitiveMap, requestID: Int32, attempt: Int32) {
    OnoSDK().timerFactory.startTimer(runAfter: Int64(attempt) * 1000) {
      self.postRkiRequest(terminal: terminal, rkiRequestMap: rkiRequestMap, requestID: requestID, attempt: attempt)
    }
  }
  
  private func handleTransactionAuthorizationSuccess(
    clientTransactionId: String,
    data: AuthorizationResponseDTO,
    responseCode: Int,
    attempt: Int? = nil
  ) {
    self.dispatch(
      OnoActions.AuthorizationResponse(
        clientTransactionId: clientTransactionId,
        transactionId: data.transactionId,
        chargeResult: data.chargeResult,
        isoResponse: data.isoResponseCode,
        chargeResultMessage: data.chargeResultMessage,
        arpc: data.arpc?.toSensitiveString(),
        signatureRequired: data.signatureRequired?.toKotlinBoolean(),
        errorMessage: nil,
        attempt: attempt?.toKotlinInt(),
        httpStatusCode: responseCode.toKotlinInt()
      )
    )
  }
  
  private func handleTransactionAuthorizationFailure(apiError: ApiError, maybeOnoTransactionId: String?) {
    self.dispatch(
      OnoActions.AuthorizationFailed(
        httpStatusCode: apiError.code?.toKotlinInt(),
        errorDetails: getErrorDetails(apiError: apiError),
        maybeOnoTransactionId: maybeOnoTransactionId
      )
    )
  }
  
  private func getErrorDetails(apiError: ApiError) -> ErrorDetails {
    let statusCode = apiError.code?.toKotlinInt()
    let message = apiError.message ?? apiError.errorMessage
    
    switch apiError.type {
    case .Connection:
      return Http.NoConnection(statusCode: statusCode, message: message)
    case .Timeout:
      return Http.ConnectionTimeout(statusCode: statusCode, message: message)
    default:
      return Http.Companion().otherNetworkError(statusCode: statusCode, message: message)
    }
  }
  
  override func chargeTransaction(chargeTransactionBody: OnoChargeTransactionBody, requestID: Int32, onoAuthToken: SensitiveString?) {
    if let onoAuthenticationToken = onoAuthToken {
      OnoWebRequest.setDefaultHeader(
        OnoWebRequest.getDefaultHeaderString(key: ApiHeaderKey.onoMerchantToken.value, defaultValue: onoAuthenticationToken.value),
        forKey: ApiHeaderKey.onoMerchantToken.value
      )
    }
    
    OnoWebRequest().withBaseURLAndPath(self.baseURL, path: "/v2/charges")
      .withJSONBody(chargeTransactionBody.getMappedData())
      .withRetries(false)
      .doPost(onSuccess: { (_, data: ChargeApiV2ResponseDTO) in
        self.dispatch(
          QRTransactionActions.ChargeTransactionResponse(
            switchTransactionId: data.transactionId,
            chargeResult: data.chargeResult,
            qrCode: data.paymentInfo.masterpassInfo.code
          )
        )
      }, onFailure: { (error) in
        self.dispatch(
          QRTransactionActions.ChargeTransactionFailed(
            errorDetails: self.getErrorDetails(apiError: error),
            maybeOnoTransactionId: chargeTransactionBody.clientTransactionId
          )
        )
      })
  }
  
  override func pollChargeStatus(clientTransactionId: String, requestID: Int32) {
    OnoWebRequest().withBaseURLAndPath(self.baseURL, path: "/v2/charges/\(clientTransactionId)/lastResponse")
      .withRetries(false)
      .doGet(onSuccess: { (_, data: ChargeApiV2ResponseDTO) in
        self.dispatch(
          QRTransactionActions.PollTransactionResponse(
            switchTransactionId: data.transactionId,
            chargeResult: data.chargeResult,
            qrCode: data.paymentInfo.masterpassInfo.code,
            isoResponse: data.isoResponseCode,
            chargeResultMessage: data.chargeResultMessage,
            rrn: data.paymentInfo.masterpassInfo.rrn,
            authorizationId: data.paymentInfo.masterpassInfo.authorizationId,
            msisdn: data.paymentInfo.masterpassInfo.msisdn,
            accountType: data.paymentInfo.masterpassInfo.accountType,
            cardType: data.paymentInfo.masterpassInfo.cardType,
            binNumber: data.paymentInfo.masterpassInfo.binNumber,
            panLast4Digits: data.paymentInfo.masterpassInfo.panLast4Digits,
            cardHolderName: data.paymentInfo.masterpassInfo.cardHolderName,
            wallet: data.paymentInfo.masterpassInfo.wallet
          )
        )
      }, onFailure: { (error) in
        self.dispatch(
          QRTransactionActions.PollTransactionFailed(
            errorDetails: self.getErrorDetails(apiError: error),
            maybeOnoTransactionId: clientTransactionId
          )
        )
      })
  }
  
  override func confirmTransaction(transactionId: String, confirmationBody: OnoConfirmationBody, requestID: Int32) {
    OnoWebRequest().withBaseURLAndPath(self.baseURL, path: "/v2/charges/\(transactionId)/confirm")
      .withJSONBody(confirmationBody.getMappedData())
      .withRetries(false)
      .doPost(onSuccess: { (_, _: ApiResponseDTO) in
        self.dispatch(QRTransactionActions.QrTransactionConfirmed())
      }, onFailure: { (error) in
        self.dispatch(
          QRTransactionActions.TransactionConfirmationFailed(
            errorDetails: self.getErrorDetails(apiError: error),
            statusCode: error.code?.toKotlinInt()
          )
        )
      })
  }
  
  override func rejectTransaction(transactionId: String, rejectionBody: OnoConfirmationBody, requestID: Int32) {
    OnoWebRequest().withBaseURLAndPath(self.baseURL, path: "/v2/charges/\(transactionId)/reject")
      .withJSONBody(rejectionBody.getMappedData())
      .withRetries(false)
      .doPost(onSuccess: { (_, data: ApiResponseDTO) in
        self.dispatch(QRTransactionActions.QrTransactionRejected())
      }, onFailure: { (error) in
        self.dispatch(
          QRTransactionActions.TransactionRejectionFailed(
            errorDetails: self.getErrorDetails(apiError: error),
            statusCode: error.code?.toKotlinInt()
          )
        )
      })
  }
  
  override func rejectTransactionByClientTransactionId(clientTransactionId: String, rejectionBody: OnoRejectByClientIdBody, requestID: Int32) {
    OnoWebRequest().withBaseURLAndPath(self.baseURL, path: "/v2/charges/by/clientTransactionId/\(clientTransactionId)/reject")
      .withJSONBody(rejectionBody.getMappedData())
      .withRetries(false)
      .doPost(onSuccess: { (_, data: ApiResponseDTO) in
        self.dispatch(QRTransactionActions.QrTransactionRejected())
      }, onFailure: { (error) in
        self.dispatch(
          QRTransactionActions.TransactionRejectionByIdFailed(
            errorDetails: self.getErrorDetails(apiError: error),
            statusCode: error.code?.toKotlinInt()
          )
        )
      })
  }
  
}
