//
//  BluetoothService.swift
//  YocoPOS
//
//  Created by Altus van der Merwe on 2019/06/29.
//  Copyright © 2019 Yoco. All rights reserved.
//

import CoreBluetooth
import Foundation
import Ono

@objc class IOSBluetoothService : BluetoothServiceProtocol, CBCentralManagerDelegate {
  let TAG = "OnoSDK> BluetoothService> "
  
  var centralBluetoothManager: CBCentralManager?
  var enviroment: Environment!
  
  init(enviroment: Environment, dispatch: @escaping (Action) -> Void) {
    super.init(dispatch: dispatch)
    self.enviroment = enviroment
    self.givenIsBluetoothPermissionAllowed(loggingTag: TAG) {
      setupCoreBluetoothCentralManager()
    }
    EAAccessoryManager.shared().registerForLocalNotifications()
    NotificationCenter.default.addObserver(
      self,
      selector: #selector(self.accessoryDidConnect),
      name: NSNotification.Name.EAAccessoryDidConnect,
      object: nil
    )
    
    NotificationCenter.default.addObserver(
      self,
      selector: #selector(self.accessoryDidDisconnect),
      name: NSNotification.Name.EAAccessoryDidDisconnect,
      object: nil
    )
  }
  
  @objc func accessoryDidConnect(notication: NSNotification) {
    let accessory = notication.userInfo?.values.first as? EAAccessory
    Logger().i(tag: self.TAG, message: "Accessory did Connect: \(String(describing: accessory))")
    if let accessory = accessory {
      self.dispatch(BluetoothActions.DeviceDiscovered(discoveredDevice: BluetoothDevice(address: accessory.name, name: accessory.name, bondState: BluetoothBondState.statePaired)))
    }
  }
  
  @objc func accessoryDidDisconnect(notication: NSNotification) {
    Logger().i(tag: self.TAG, message: "Accessory did Disconnnect \(String(describing: notication))")
  }
  
  func getBluetoothService() -> BluetoothServiceProtocol {
    if (enviroment == Environment.development && NSNumber(integerLiteral: Int(TARGET_OS_SIMULATOR)).boolValue) {
      return MockBluetoothService(dispatch: OnoSDK().getDispatch(dispatcherName: "MockBluetoothService"))
    }
    return self
  }
  
  override func enableBluetooth() {
    Logger().i(tag: TAG, message: "enableBluetooth - let iOS show the popup for user to enable bluetooth.")
    setupCoreBluetoothCentralManager()
  }
  
  override func startSearching() {
    if(self.centralBluetoothManager == nil) {
      // Setup the bluetooth central manager when we start scanning for bluetooth devices,
      // if the permission is not granted or bluetooth is OFF, then iOS will prompt for it.
      setupCoreBluetoothCentralManager()
      return;
    }
    
    self.givenIsBluetoothPermissionAllowed(loggingTag: TAG) {
      Logger().i(tag: TAG, message: "startSearching")
      self.dispatch(BluetoothActions.DiscoveryStarted())
      self.centralBluetoothManager?.scanForPeripherals(withServices: nil)
    }
  }
  
  override func stopSearching() {
    Logger().i(tag: TAG, message: "stopSearching")
    if(centralBluetoothManager?.state == .poweredOn) {
      centralBluetoothManager?.stopScan()
    }
    self.dispatch(BluetoothActions.DiscoveryFinished())
  }
  
  override func doInitState() {
    Logger().i(tag: TAG, message: "doInitState")
    centralBluetoothManager.map { self.dispatchBluetoothState(bluetoothManager: $0) }
  }
  
  override func fetchPairedDevices() {
    Logger().i(tag: TAG, message: "fetchPairedDevices")
    EAAccessoryManager.shared().connectedAccessories.forEach { (accessory) in
      Logger().i(tag: TAG, message: "fetchPairedDevices: address: \(accessory.protocolStrings.first ?? accessory.name)")
      self.dispatch(BluetoothActions.DeviceDiscovered(discoveredDevice: BluetoothDevice(address: accessory.name, name: accessory.name, bondState: BluetoothBondState.statePaired)))
    }
  }
  
  func centralManagerDidUpdateState(_ central: CBCentralManager) {
    self.setCentralBluetoothManager(centralBluetoothManager:central)
    self.dispatchBluetoothState(bluetoothManager: central)
  }
  
  func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
    self.setCentralBluetoothManager(centralBluetoothManager: central)
    if let deviceName = peripheral.name {
      self.dispatch(BluetoothActions.DeviceDiscovered(discoveredDevice: BluetoothDevice(address: deviceName, name: deviceName, bondState: BluetoothBondState.stateNotPaired)))
    }
  }
  
  private func dispatchBluetoothState(bluetoothManager: CBCentralManager) {
    switch bluetoothManager.state {
    case .poweredOff:
      self.dispatch(BluetoothActions.StateChanged(state: BluetoothState.stateOff))
    case .poweredOn:
      self.dispatch(BluetoothActions.StateChanged(state: BluetoothState.stateOn))
    default:
      self.dispatch(BluetoothActions.StateChanged(state: BluetoothState.unknown))
    }
  }
  
  override func showPairingDialog() {
    let predicate = NSPredicate.init(format: "(self CONTAINS 'Miura') OR (self CONTAINS 'Yoco')", argumentArray: nil)
    EAAccessoryManager.shared().showBluetoothAccessoryPicker(withNameFilter: predicate) { (error) in
      if let error = error {
        if let eaError = error as? EABluetoothAccessoryPickerError {
          switch eaError.code {
          case .alreadyConnected:
            Logger().e(tag: self.TAG, message: "Pairing device failed: \(error.localizedDescription)")
            self.dispatch(TerminalCommands.PairingFailed(errorMessage: "Already connected, press red X to stop pairing mode on card machine"))
          case .resultNotFound:
            Logger().e(tag: self.TAG, message: "Pairing device failed: \(error.localizedDescription)")
            self.dispatch(TerminalCommands.PairingFailed(errorMessage: "Not found"))
          case .resultCancelled:
            Logger().e(tag: self.TAG, message: "Pairing device failed: \(error.localizedDescription)")
            self.dispatch(TerminalCommands.PairingFailed(errorMessage: "Pairing cancelled"))
          case .resultFailed:
            Logger().e(tag: self.TAG, message: "Pairing device failed: \(error.localizedDescription)")
            self.dispatch(TerminalCommands.PairingFailed(errorMessage: "Failed"))
          default:
            Logger().e(tag: self.TAG, message: "Pairing device failed: \(error.localizedDescription)")
            self.dispatch(TerminalCommands.PairingFailed(errorMessage: "Unknown"))
          }
        } else {
          Logger().e(tag: self.TAG, message: "Pairing device failed: \(error.localizedDescription)")
          self.dispatch(TerminalCommands.PairingFailed(errorMessage: error.localizedDescription))
        }
      } else {
        Logger().i(tag: self.TAG, message: "Paired new device succesfully!")
        self.dispatch(TerminalActions.NewTerminalPaired())
      }
      
      self.fetchPairedDevices()
    }
  }
  
  private func setupCoreBluetoothCentralManager() {
    self.setCentralBluetoothManager(centralBluetoothManager: CBCentralManager(delegate: self, queue: nil, options: [CBCentralManagerOptionShowPowerAlertKey:true]))
  }
  
  private func setCentralBluetoothManager(centralBluetoothManager: CBCentralManager) {
    self.centralBluetoothManager = centralBluetoothManager
  }
  
  private func givenIsBluetoothPermissionAllowed<T>(loggingTag:String, performBluetoothFunction: () -> T?) -> T? {
    if #available(iOS 13.1, *) {
      let authorization = CBCentralManager.authorization
      if(authorization == .allowedAlways) {
        return performBluetoothFunction()
      } else {
        self.logUnexpectedBluetoothAuthorizationState(loggingTag: loggingTag, currentAuthorizationState: authorization, checkedUsingConvensionForIOS: "13.1+")
        return nil
      }
    }
    else if #available(iOS 13.0, *) {
      let authorization = CBCentralManager().authorization
      if(authorization == .allowedAlways) {
        return performBluetoothFunction()
      } else {
        self.logUnexpectedBluetoothAuthorizationState(loggingTag: loggingTag, currentAuthorizationState: authorization, checkedUsingConvensionForIOS: "13.0+")
        return nil
      }
    }
    else {
      Logger().d(tag: loggingTag, message: "There's no Bluetooth permission authorization for iOS < 13.0")
      return performBluetoothFunction()
    }
  }
  
  @available(iOS 13.0, *)
  private func logUnexpectedBluetoothAuthorizationState (loggingTag:String, currentAuthorizationState: CBManagerAuthorization, checkedUsingConvensionForIOS: String) {
    func describeAuthorization(authorization: CBManagerAuthorization) -> String {
      switch authorization {
      case .denied:
        return "denied"
      case .restricted:
        return "restricted"
      case .notDetermined:
        return "notDetermined"
      case .allowedAlways:
        return "allowedAlways"
      default:
        return "unknownAuthorization(value = \(authorization))"
      }
    }
    
    let authStateStr = describeAuthorization(authorization: currentAuthorizationState)
    Logger().d(
      tag: loggingTag,
      message: "Bluetooth permission authorization state \(authStateStr), checked using convension for \(checkedUsingConvensionForIOS)"
    )
  }
}
