//
//  OnoLogger.swift
//  YocoPOS
//
//  Created by Altus van der Merwe on 2019/06/10.
//  Copyright © 2019 Yoco. All rights reserved.
//

import Foundation
import Ono
import UIKit

public class IOSLogger : ILogger {

  public var clientTransactionUUID: String?
  var globalMetaData: [String : Any]
  
  let deviceDetails: String = String("[MANUFACTURER: Apple, PRODUCT: \(UIDevice.current.localizedModel), MODEL: \(UIDevice.current.model), DEVICE_IDENTIFIER: \(UIDevice.current.identifierForVendor?.uuidString ?? "")]")
  
  let APP_COMPONENT = "app"
  let SDK_COMPONENT = "oldSDK"
  let ONO_SDK_COMPONENT = "onoSDK"
  
  public init() {
    
    globalMetaData = [String : Any](minimumCapacity: 11)
    
    var appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"]
    if let exAppVersion = appVersion {
      if let exBuildNumber = Bundle.main.infoDictionary?["CFBundleVersion"] {
        appVersion = "\(exAppVersion) build \(exBuildNumber)"
      }
    }
    
    globalMetaData[Logger().KEY_APP_VERSION] = appVersion
    globalMetaData[Logger().KEY_APP_PACKAGE_NAME] = Bundle.main.bundleIdentifier
    globalMetaData[Logger().KEY_YOCO_SDK_VERSION] = Bundle(identifier: "org.cocoapods.YocoSDKNew")?.infoDictionary?["CFBundleShortVersionString"] as? String
    globalMetaData[Logger().KEY_ONO_SDK_VERSION] = Bundle(identifier: "org.cocoapods.OnoSDK")?.infoDictionary?["CFBundleShortVersionString"] as? String
    globalMetaData[Logger().KEY_DEVICE_MODEL] = UIDevice.current.model
    globalMetaData[Logger().KEY_OPERATING_SYSTEM] = UIDevice.current.systemName
    globalMetaData[Logger().KEY_OS_NAME] = UIDevice.current.systemName
    globalMetaData[Logger().KEY_OS_VERSION] = UIDevice.current.systemVersion
    globalMetaData[Logger().KEY_BUILD_NUMBER] = Bundle.main.infoDictionary?["CFBundleVersion"]
    globalMetaData[Logger().KEY_DEVICE_DETAILS] = deviceDetails
    globalMetaData[Logger().KEY_DEVICE_MANUFACTURER] = "Apple"
    globalMetaData[Logger().KEY_DEVICE_IDENTIFIER] = UIDevice.current.identifierForVendor?.uuidString
    globalMetaData[Logger().KEY_DEVICE_TYPE] = UIDevice.current.userInterfaceIdiom == .pad ? "tablet" : "phone" // assuming it won't run on TV or CarPlay
    globalMetaData[Logger().KEY_DEVICE_NAME] = UIDevice.current.name
  }

  public func logMessage(logLevel: LogLevel, message: String, metadata: [AnyHashable : Any]?, component: String?) {
    let tag = metadata?["tag"] as! String?

    #if DEBUG
      doInternalLog(logLevel: logLevel, tag: tag ?? "UNKNOWN", message: message)
    #endif
    
    var actualComponent = APP_COMPONENT
    if let existingComponent = component {
      actualComponent = existingComponent
    } else {
      if let existingTag = tag {
        actualComponent = existingTag.lowercased().starts(with: "onosdk") ? ONO_SDK_COMPONENT : APP_COMPONENT
      }
    }
    
    OnoIOSLogger.shared().logMessage(Int(logLevel.value), message: message, metadata: metadata, component: actualComponent)
  }
  
  public func uploadLogFile(clientBillIdentifier: String, minLogLevel: Int32, excludeComponents: KotlinArray?, excludeRegex: String?) -> Bool {
    var excludes = Array<String?>() // excludeComponents needs to be of type NSArray
    if let iterator = excludeComponents?.iterator() {
      while iterator.hasNext() {
        excludes.append(iterator.next() as? String)
      }
    }
    return OnoIOSLogger.shared().uploadLogFile(clientBillIdentifier, logLevel: Int(minLogLevel), excludeComponents: excludes as [Any])
  }
  
  public func uploadLogsInRange(startDate: Int64, endDate: Int64, logLevel: Int32) {
    OnoIOSLogger.shared().uploadLogs(
      inRange: Date(timeIntervalSince1970: Double(integerLiteral: startDate)),
      end: Date(timeIntervalSince1970: Double(integerLiteral: endDate)),
      logLevel: Int(logLevel)
    )
  }
  
  public func setMetaValue(key: String, value: Any) -> Any {
    OnoIOSLogger.shared().setMetaValue(value, forKey: key)
    return value
  }
  
  public func setClientBillIdentifier(clientBillIdentifier: String) {
    OnoIOSLogger.shared().setClientBillIdentifier(clientBillIdentifier)
  }
  
  public func checkExists(clientBillIdentifier: String) -> Bool {
    return false//YCLogger.shared().checkLogExists(clientBillIdentifier)
  }
  
  public func doInternalLog(logLevel: LogLevel, tag: String, message: String) {
    switch logLevel {
      case LogLevel.trace:
        NSLog("[TRACE] \(tag) \(message)")
      case LogLevel.debug:
        NSLog("[DEBUG] \(tag) \(message)")
      case LogLevel.info:
        NSLog("[INFO] \(tag) \(message)")
      case LogLevel.warn:
        NSLog("[WARN] \(tag) \(message)")
      case LogLevel.error:
        NSLog("[ERROR] \(tag) \(message)")
      default:
        NSLog("\(tag)) \(message)")
    }
  }
  
  public func getGlobalMetadata() -> [String : Any] {
    return globalMetaData
  }
  
  public func getCurrentTimeMillis() -> Int64 {
    return Int64(Date().timeIntervalSince1970 * 1000)
  }
  
  public func getConnectionInfo() -> OnoNetworkConnectionInfo {
    let signalStrength = Int32(-1) // TODO: find a way to get the signal strength
    let connectionName = YCTelephony.connectionProviderName()
    switch(YCTelephony.getConnectionType()) {
    case "WiFi":
      return OnoNetworkConnectionInfo(
        networkType: NetworkConnectionType.wifi,
        carrierName: connectionName,
        signalStrength: signalStrength,
        connectionType: NetworkConnectionType.wifi.name
      );
    case "3G":
      return OnoNetworkConnectionInfo(
        networkType: NetworkConnectionType.mobile,
        carrierName: connectionName,
        signalStrength: signalStrength,
        connectionType: YCTelephony.carrierNetworkType()
      );
    default:
      return OnoNetworkConnectionInfo(
        networkType: NetworkConnectionType.none,
        carrierName: connectionName,
        signalStrength: signalStrength,
        connectionType: NetworkConnectionType.none.name
      );
    }
  }

}
