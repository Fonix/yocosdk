//
//  DownloadService.swift
//  YocoPOS
//
//  Created by Altus van der Merwe on 2019/08/15.
//  Copyright © 2019 Yoco. All rights reserved.
//

import Foundation
import Ono

class DownloadService {
  class func load(url: URL, onComplete: @escaping (Data) -> (), onError: @escaping (String?) -> ()) {
    let sessionConfig = URLSessionConfiguration.default
    let session = URLSession(configuration: sessionConfig)
    let request = URLRequest(url: url)
    
    let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
      if let tempLocalUrl = tempLocalUrl, error == nil {
        // Success
        if let statusCode = (response as? HTTPURLResponse)?.statusCode {
          Logger().i(tag: "OnoSDK> DownloadService>", message: "Success: \(statusCode)")
        }
        
        do {
          let data = try Data.init(contentsOf: tempLocalUrl)
          DispatchQueue.main.async {
            let innerData = Data.init(data)
            onComplete(innerData)
          }
        } catch (let readError) {
          Logger().i(tag: "OnoSDK> DownloadService>", message: "error reading file \(tempLocalUrl) : \(readError)")
          DispatchQueue.main.async {
            onError(readError.localizedDescription)
          }
        }
      } else {
        Logger().i(tag: "OnoSDK> DownloadService>", message: "Failure: \(String(describing: error?.localizedDescription))")
        DispatchQueue.main.async {
          onError(error?.localizedDescription)
        }
      }
    }
    task.resume()
  }
}
