//
//  OnoExtensions.swift
//  YocoPOS
//
//  Created by Shadrack Taeli on 2020/04/08.
//  Copyright © 2020 Facebook. All rights reserved.
//

import Foundation
import CommonCrypto
import Ono

extension Data {
  /**
   * Convert Data to KotlinByteArray
   */
  func toKotlinByteArray() -> KotlinByteArray {
    let intArray : [Int8] = Array(self).map { Int8(bitPattern: $0) }
    let kotlinByteArray = KotlinByteArray.init(size: Int32(intArray.count))
    for (index, element) in intArray.enumerated() {
      kotlinByteArray.set(index: Int32(index), value: element)
    }
    return kotlinByteArray
  }
  
  /**
   * Convert KotlinByteArray to SensitiveByteArray
   */
  func toSensitiveByteArray() -> SensitiveByteArray {
    return SensitiveByteArray(byteArray: self.toKotlinByteArray())
  }
  
  var md5: String {
    let hash = self.withUnsafeBytes { (bytes: UnsafeRawBufferPointer) -> [UInt8] in
      var hash = [UInt8](repeating: 0, count: Int(CC_MD5_DIGEST_LENGTH))
      CC_MD5(bytes.baseAddress, CC_LONG(self.count), &hash)
      return hash
    }
    return hash.map { String(format: "%02x", $0) }.joined()
  }
}

extension KotlinByteArray {
  /**
   * Convert KotlinByteArray to SensitiveByteArray
   */
  func toSensitiveByteArray() -> SensitiveByteArray {
    return SensitiveByteArray(byteArray: self)
  }
}

extension Bool {
  /**
   * Convert Bool to KoltinBoolean
   */
  func toKotlinBoolean() -> KotlinBoolean {
    return KotlinBoolean(value: self)
  }
}

extension String {
  /**
   * Convert String to SensitiveString
   */
  func toSensitiveString() -> SensitiveString {
    return SensitiveString(value: self)
  }
  
  /**
   * Convert String to KotlinByteArray
   */
  func toByteArray() -> KotlinByteArray? {
    return self.data(using: .utf8)?.toKotlinByteArray()
  }
  
  /**
   * Convert Hex String to Data
   */
  func hexStringToData() -> Data {
    var hex = self
    var data = Data()
    while(hex.count > 0) {
      let subIndex = hex.index(hex.startIndex, offsetBy: 2)
      let c = String(hex[..<subIndex])
      hex = String(hex[subIndex...])
      var ch: UInt32 = 0
      Scanner(string: c).scanHexInt32(&ch)
      var char = UInt8(ch)
      data.append(&char, count: 1)
    }
    return data
  }
}

extension SensitiveString {
  /**
   * Convert SensitiveString to MPITLVObject
   */
  func toMPITLVObject() -> MPITLVObject {
    return MPITLVObject(
      tag: TLVTag.TLVTag_Command_Data,
      construct: MPITLVParser.decode(withBytes: MPIBinaryUtil.bytes(withHexString: self.value))
    )
  }
}

extension Int {
  /**
   * Convert Int to KotlinInt
   */
  func toKotlinInt() -> KotlinInt {
    return KotlinInt(int: Int32(self))
  }
}

extension KotlinByteArray {
  /**
   * Convert KotlinByteArray to NSData
   */
  func toData() -> Data {
    var intArray: [Int8] = Array()
    let _iter = self.iterator()
    for _ in 0..<self.size {
      intArray.append(_iter.nextByte())
    }
    return intArray.withUnsafeBufferPointer { Data(buffer: $0) }
  }
}

extension AvailableUpdateDTO {
  /**
   * Convert AvailableUpdateDTO to AvailableUpdate
   */
  func toAvailableUpdate() -> AvailableUpdate {
    return AvailableUpdate.init(
      files: self.files.map { $0.toUpdateFile() },
      installation: self.installation,
      message: self.message,
      updateIndex: nil,
      totalUpdates: nil
    )
  }
}

extension UpdateFileDTO {
  /**
   * Convert UpdateFileDTO to UpdateFile
   */
  func toUpdateFile() -> UpdateFile {
    return UpdateFile.init(
      url: self.url,
      md5sum: self.md5sum,
      size: Int32(self.size),
      type: self.type,
      version: self.version
    )
  }
}

extension OnoAuthorizationBody {
  /**
   * Convert OnoAuthorizationBody to Map<String, String>
   */
  func toMap() -> [String: Any?] {
    return [
      "merchantId": self.merchantId,
      "clientTransactionId": self.clientTransactionId,
      "sdkTransactionId": self.sdkTransactionId,
      "amountInCents": self.amountInCents,
      "currency": self.currency,
      "terminal": self.terminal,
      "appInfo": self.appInfo,
      "deviceInfo": self.deviceInfo,
      "tlv": self.tlv?.value,
      "transactionData": self.transactionData?.valueMap,
      "metadata": self.metadata,
      "attemptedInputModes": self.attemptedInputModes
    ]
  }
}
// --
