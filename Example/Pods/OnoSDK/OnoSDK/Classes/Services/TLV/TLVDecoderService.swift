//
//  TLVDecoderService.swift
//  YocoPOS
//

import Foundation
import Ono

extension String {
    typealias Byte = UInt8
    var hexaToBytes: [Byte] {
        var start = startIndex
        return stride(from: 0, to: count, by: 2).compactMap { _ in   // use flatMap for older Swift versions
            let end = index(after: start)
            defer { start = index(after: end) }
            return Byte(self[start...end], radix: 16)
        }
    }
    var hexaToBinary: String {
        return hexaToBytes.map {
            let binary = String($0, radix: 2)
            return repeatElement("0", count: 8-binary.count) + binary
        }.joined()
    }
}

public class TLVDecoderService : TLVDecoderServiceProtocol {
  
  // SGTLV taken from: https://github.com/saturngod/SGTLVDecode
  
  public override func extractHexStringFromTLV(tlv: String, tag: String) -> String? {
    let dic = SGTLVDecode.decode(with: tlv)
    let tagValue = dic?[tag] as? [AnyHashable: Any]
    let valueValue = tagValue?["value"]
    return valueValue as? String
  }
  
  public override func extractBinaryStringListFromTLV(tlv: String, tag: String) -> [String]? {
    let hexString = extractHexStringFromTLV(tlv: tlv, tag: tag)
    return (hexString?.hexaToBinary).map {
      chunkString(s: $0, splitSize: 8)
    }
  }
  
  func chunkString(s: String, splitSize: Int) -> [String] {
    if s.count <= splitSize {
      return [s]
    } else {
      return [String(s.prefix(splitSize))] + chunkString(s: String(s.suffix(from: s.index(s.startIndex, offsetBy: splitSize))), splitSize: splitSize)
    }
  }
}
