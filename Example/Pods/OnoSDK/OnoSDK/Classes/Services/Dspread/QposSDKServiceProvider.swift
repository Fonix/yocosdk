//
//  DspreadService.swift
//  YocoPOS
//
//  Created by Mandisa Mjamba on 2019/07/18.
//  Copyright © 2019 Yoco. All rights reserved.
//

import Foundation
import Ono

class QposSDKServiceProvider {
  private static var dspreadService: DspreadService?

  var enviroment: Environment
  
  init(enviroment: Environment) {
    self.enviroment = enviroment
  }
  
  func getDspreadService() -> DspreadServiceProtocol {
    // Avoid creating multiple `DspreadService` instances to avoid issues
    // where two clients are using the same device but have separate
    // command queues and listeners.
    if let dspreadService = QposSDKServiceProvider.dspreadService {
      return dspreadService
    }

    if (enviroment == Environment.development && NSNumber(integerLiteral: Int(TARGET_OS_SIMULATOR)).boolValue) {
      return MockDspreadService(dispatch: OnoSDK().getDispatch(dispatcherName: "MockDspreadService"), isUnitTesting: false, dateTimeString: {OnoSDKUtils.currentDateTime(format: $0)})
    }
    
    let onoQposListener = OnoQposListener(dispatch: OnoSDK().getDispatch(dispatcherName: "OnoQposListener"))
    let onoQposService = QposSDKService(listener: onoQposListener)
    let qposCommandQueue = TerminalCommandQueue(sdkService: onoQposService, dispatch: OnoSDK().getDispatch(dispatcherName: "QposCommandQueue"))
    
    onoQposListener.qposCommandQueue = qposCommandQueue
    QposSDKServiceProvider.dspreadService = DspreadService(qposCommandQueue: qposCommandQueue, updateService: UpdateService(), dispatch: OnoSDK().getDispatch(dispatcherName: "DspreadService"))
    return QposSDKServiceProvider.dspreadService!
  }
}
