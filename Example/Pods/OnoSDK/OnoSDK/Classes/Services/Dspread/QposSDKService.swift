//
//  OnoQposService.swift
//  YocoPOS
//
//  Created by Mandisa Mjamba on 2019/07/17.
//  Copyright © 2019 Yoco. All rights reserved.
//

import Foundation
import Ono

class QposSDKService: IQposSDKService {
  var tag: String = "OnoSDK> QposSDKService> "
  
  // We need the listener to callback the setBuzzerStatus method
  private let listener: OnoQposListenerProtocol
  private lazy var qposService: QPOSService = setupQposService()
  
  init(listener: OnoQposListenerProtocol) {
    self.listener = listener
  }
  
  func quickReset() {
    self.qposService.resetPosStatus()
  }
  
  
  func getSdkVersion() -> String {
    return self.qposService.getSdkVersion()
  }
  
  func connectBT(address: String) {
    DispatchQueue.global(qos: .background).async {
      self.qposService.connectBluetoothNoScan(address)
    }
  }
  
  func getQposInfo() {
    self.qposService.getQPosInfo()
  }
  
  func getQposId() {
    self.qposService.getQPosId()
  }
  
  func disconnectBT() {
    self.qposService.disconnectBT()
  }
  
  func setBuzzerStatus(buzzerStatus: Int32) {
    self.qposService.setBuzzerStatus(Int(buzzerStatus))
  }
  
  func setShutdownTime(shutdownTime: Int32) {
    self.qposService.doSetShutDownTime(String(shutdownTime))
  }
  
  func doTrade(transaction: Transaction, transactionConfig: TransactionConfig, cardPromptMode: CardPromptMode, keyIndex: Int32) {
    Logger().d(tag: tag, message: "doTrade: transaction = \(transaction), transactionConfig = \(transactionConfig)")
    let cardTradeMode = convertPromptToTradeMode(cardPromptMode: cardPromptMode)
    let timeout = Int(transactionConfig.transactionTimeout)
    
    // We add a delay here, because if we had just connected
    // it exhibits weird behaviour where it shows TAP/INSERT CARD, and then goes back to Hello without any callbacks
    // 1000 seems to be the sweet spot, 500 tends to be too short, 750 works 80% of the time
    OnoSDK().timerFactory.startTimer(runAfter: 1000) {
      self.qposService.setCardTradeMode(cardTradeMode)
      self.qposService.doCheckCard(timeout, keyIndex: Int(keyIndex))
    }
  }
  
  func requestPin(maskedPan: SensitiveString, timeout: Int32, keyIndex: Int32) {
    self.qposService.getPin(1, keyIndex: Int(keyIndex), maxLen: 6, typeFace: Messages().ENTER_PIN, cardNo: maskedPan.value, data: "", delay: Int(timeout), withResultBlock: { (_, tradeData: [AnyHashable : Any]? ) in
      self.listener.onReturnGetPinResult(p0: [
        "pinKsn": tradeData?["ksn"] as! String,
        "pinBlock": tradeData?["pin"] as! String,
        "encryptMode": tradeData?["encryptMode"] as! String
      ])
    })
  }
  
  func setAmountIcon(symbol: String) {
    self.qposService.setAmountIcon(symbol)
  }
  
  func setAmount(amount: String, cashbackAmount: String, currencyCode: String, transactionType: OnoQposTransactionType, shouldPosDisplayAmount: Bool) {
    if (transactionType == OnoQposTransactionType.goods) {
      self.qposService.setAmount(amount, aAmountDescribe: cashbackAmount, currency: currencyCode, transactionType: TransactionType.GOODS, posDisplayAmount: shouldPosDisplayAmount)
    } else {
      NSException(name:NSExceptionName(rawValue: "QPOSServiceException"), reason:"We are not mapping to other transaction types", userInfo:nil).raise()
    }
  }
  
  func doEmvApp(emvOption: OnoQposEmvOption) {
    if (emvOption == OnoQposEmvOption.startWithForceOnline) {
      self.qposService.doEmvApp(EmvOption.START_WITH_FORCE_ONLINE)
    } else {
      NSException(name:NSExceptionName(rawValue: "QPOSServiceException"), reason:"We are not mapping to other EMV options", userInfo:nil).raise()
    }
  }
  
  func sendTime() {
    self.qposService.sendTime(OnoSDKUtils.currentDateTime(format: "yyyyMMddHHmmss"))
  }
  
  func nfcBatchData() -> [String : String] {
    return qposService.getNFCBatchData() as! [String : String]
  }
  
  func selectEmvApp(emvOption: Int32) {
    self.qposService.selectEmvApp(Int(emvOption))
  }
  
  func sendOnlineProcessResult(arpc: String) {
    self.qposService.sendOnlineProcessResult(arpc)
  }
  
  func isCardExist(timeout: Int32) {
    self.qposService.isCardExist(Int(timeout)) { (cardExists) in
      DispatchQueue.main.async {
        self.listener.onQposIsCardExist(exists: cardExists)
      }
    }
  }
  
  func cancelTrade() {
    DispatchQueue.global(qos: .background).async {
      self.qposService.cancelTrade()
    }
  }
  
  func lcdShowCustomDisplay(lcdModeAlign: OnoQposLcdModeAlign, customDisplayString: String, timeout: Int32) {
    if (lcdModeAlign == OnoQposLcdModeAlign.lcdModeAligncenter) {
      let mappedString = QPOSUtil.byteArray2Hex(customDisplayString.data(using:.utf8))
      self.qposService.lcdShowCustomDisplay(LcdModeAlign.LCD_MODE_ALIGNCENTER, lcdFont: mappedString, delay: Int(timeout))
    } else {
      NSException(name:NSExceptionName(rawValue: "QPOSServiceException"), reason:"We are not mapping to other lcd mode alignments", userInfo:nil).raise()
    }
  }
  
  func closeDevice() {
    self.qposService.closeDevice()
  }
  
  func lcdShowCloseDisplay() {
    self.qposService.lcdShowCloseDisplay()
  }
  
  func resetPosStatus() {
    self.qposService.closeDevice()
    self.qposService.resetPosStatus()
  }
  
  func cancelSelectEmvApp() {
    self.qposService.cancelSelectEmvApp()
  }
  
  func updateEmvConfig(emvApp: String, emvCapk: String) {
    self.qposService.updateEmvConfig(emvApp, emvCapk: emvCapk)
  }
  
  func retrieveEMVConfig(tlv: String) {
    self.qposService.updateEmvAPP(EMVOperation.getList.rawValue, data: [tlv], block: { (success, result) in
      DispatchQueue.main.async {
        self.listener.onReturnGetEMVListResult(p0: result)
      }
    })
  }
  
  private func convertPromptToTradeMode(cardPromptMode: CardPromptMode) -> CardTradeMode {
    switch (cardPromptMode) {
    case CardPromptMode.insertOnly:
      return CardTradeMode.ONLY_INSERT_CARD
    case CardPromptMode.insertOrSwipe:
      // SWIPE_TAP_INSERT_CARD_UNALLOWED_LOW_TRADE is named poorly,
      // but is the same as SWIPE_TAP_INSERT_CARD_NOTUP_UNALLOWED_LOW_TRADE without NFC support
      return CardTradeMode.SWIPE_TAP_INSERT_CARD_UNALLOWED_LOW_TRADE
    case CardPromptMode.swipeOnly:
      return CardTradeMode.ONLY_SWIPE_CARD
    case CardPromptMode.tapInsertSwipe:
      return CardTradeMode.SWIPE_TAP_INSERT_CARD_NOTUP_UNALLOWED_LOW_TRADE
    default:
      // Switch statement needs to be exhaustive, we add a default clause here
      return CardTradeMode.ONLY_INSERT_CARD
    }
  }
  
  private func convertBuzzerStatus(buzzerStatus: Int32) -> Int {
    // We need to swap the buzzer status value that we receive from the state machine
    // On Android, buzzerStatus of 1 will turn off the buzzer on the qpos
    // But on iOS, we need a buzzerStatus of 0 to turn off the buzzer
    if (buzzerStatus > 0) {
      return 0
    }
    return 1
  }
  
  private func setupQposService() -> QPOSService {
    let service: QPOSService = QPOSService.sharedInstance()
    service.setDelegate(IOSQposListener(listener: listener))
    service.setPosType(PosType.bluetooth_2mode)
    service.setBTAutoDetecting(true)
    service.setQueue(nil)
    return service
  }
}
