//
//  OnoSDKExtensions.swift
//  OnoSDK
//
//  Created by Altus van der Merwe on 2020/06/09.
//

import Foundation
import Ono

extension OnoSDK {
  public func defaultInitialze(doTransactionLogging: Bool = true) {
    if (OnoSDK().isInitialized()) {
        // We don't want to make multiple instances of the services below, so we check if the OnoSDK
        // is already initialized, and if it is it means we have already created these things
        OnoSDK().logInitError(maybeError: nil, tag: OnoSDK().loggingTag(moduleName: "iOS", className: "OnoSdkDefaultInitialze"))
        return
    }
    
    var environment = Environment.production
    #if DEBUG
    environment = Environment.development
    #endif
    
    let core = CoreApiService(dispatch: OnoSDK().getDispatch(dispatcherName: "CoreApiService"))
    
    OnoSDK().instantiate(environment: environment,
                         logger: IOSLogger(),
                         onoApiService: OnoApiService(dispatch: OnoSDK().getDispatch(dispatcherName: "OnoApiService")),
                         coreApiService: core,
                         segmentService: SegmentService(coreApi: core),
                         timerFactory: IOSTimerFactory(),
                         tlvDecoderService: TLVDecoderService(),
                         onoSDKModules: [
                          BluetoothModule(bluetoothService: IOSBluetoothService(enviroment: environment, dispatch: OnoSDK().getDispatch(dispatcherName: "BluetoothModule")).getBluetoothService()),
                          DspreadModule(dspreadService: QposSDKServiceProvider(enviroment: environment).getDspreadService()),
                          MiuraModule(miuraService: MiuraSDKServiceProvider(enviroment: environment).getMiuraService())
                         ],
                         doTransactionLogging: doTransactionLogging)
  }
}
