//
//  YCLogger.m
//  Yoco
//
//  Created by Andrew Snowden on 2015/01/07.
//  Copyright (c) 2015 Yoco Technologies (PTY) LTD. All rights reserved.
//

#import "OnoIOSLogger.h"
#import "OnoWebRequest.h"
#import "Snappy+Init.h"
#import <UIKit/UIKit.h>
#import "YCDebugInfo.h"
#import "NSObject+Utils.h"

#define MAX_LOG_SIZE 10485760

@interface OnoIOSLogger ()

@property (nonatomic, retain) NSFileHandle* fileHandle;
@property (nonatomic, retain) NSDateFormatter* dateFormatter;
@property (nonatomic, retain) NSTimer* cleanupTimer;
@property (nonatomic, retain) NSString* clientBillIdentifier;
@property (nonatomic, retain) NSMutableDictionary* metadata;

// Used for debouncing duplicate logs
@property (nonatomic, retain) NSMutableDictionary* lastCategoryLogs;

@end

@implementation OnoIOSLogger

+(OnoIOSLogger*) sharedLogger {
    static OnoIOSLogger *sharedLogger = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedLogger = [[OnoIOSLogger alloc] init];
    });
    return sharedLogger;
}

-(id) init {
    if (self = [super init]) {
        self.logRetentionInterval = 60 * 60 * 24 * 7;
        self.dateFormatter = [[NSDateFormatter alloc] init];
        NSLocale *enUSPOSIXLocale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
        [self.dateFormatter setLocale:enUSPOSIXLocale];
        [self.dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"]; // NSISO8601DateFormatter only in iOS 10
        
        self.lastCategoryLogs = [[NSMutableDictionary alloc] init];
        self.metadata = [[NSMutableDictionary alloc] init];
        
        [self setMetaValue:debugInfo.appVersion forKey:@"appVersion"];
        [self setMetaValue:debugInfo.appName forKey:@"appName"];
        [self setMetaValue:debugInfo.sdkVersion forKey:@"sdkVersion"];
        [self setMetaValue:debugInfo.deviceModel forKey:@"deviceModel"];
        [self setMetaValue:debugInfo.deviceMake forKey:@"make"];
        [self setMetaValue:debugInfo.osName forKey:@"osName"];
        [self setMetaValue:debugInfo.osVersion forKey:@"osVersion"];
        [self setMetaValue:debugInfo.deviceIdentifier forKey:@"deviceIdentifier"];
        
        self.automaticallyUploadLogs = true;
        
        if (!self.cleanupTimer) {
            self.cleanupTimer = [NSTimer scheduledTimerWithTimeInterval:60*60
                                                                  target:self
                                                                selector:@selector(cleanupLogs)
                                                                userInfo:nil
                                                                 repeats:NO];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(60 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self cleanupLogs];
            });
        }

    }
    
    return self;
}

-(void) setMetaValue:(nonnull id)value forKey:(nonnull NSString*)key {
    [self.metadata setObject:value forKey:key];
}

-(NSMutableDictionary*) globalMetaData {
    return self.metadata;
}

-(NSString*) pathForBillIdentifier:(NSString*)clientBillIdentifier {
    return [NSString stringWithFormat:@"%@OnoSDK-%@.log", NSTemporaryDirectory(), clientBillIdentifier];
}

-(void) cleanupLogs {
    NSString* logsPath = NSTemporaryDirectory();
    NSError* error;
    NSArray* directoryContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:logsPath error:&error];
    
    if (error) {
        NSLog(@"Error listing directory contents for '%@' - %@", logsPath, error);
    } else {
        NSDate* deleteBefore = [[NSDate date] dateByAddingTimeInterval:(-1 * self.logRetentionInterval)];
        for (NSString* file in directoryContents) {
            if ([file hasSuffix:@".log"]) {
                NSError* attributesError;
                NSString* filePath = [NSString stringWithFormat:@"%@%@", logsPath, file];
                NSDictionary *attributes = [[NSFileManager defaultManager] attributesOfItemAtPath:filePath error:&attributesError];
                
                if (attributesError) {
                    NSLog(@"Error fetching attributes for file '%@' - %@", filePath, attributesError);
                } else {
                    NSDate* lastModified = attributes.fileModificationDate;
                    if ([lastModified compare:deleteBefore] == NSOrderedAscending) {
                        // Delete this file
                        NSLog(@"Removing log file '%@' with last modified date of %@", filePath, lastModified);
                        NSError* deleteError;
                        [[NSFileManager defaultManager] removeItemAtPath:filePath error:&deleteError];
                        
                        if (deleteError) {
                            NSLog(@"Error deleting file at '%@' - %@", filePath, deleteError);
                        }
                    }
                }
            }
        }
    }
}

-(void) setClientBillIdentifier:(nonnull NSString*)clientBillIdentifier {
    @synchronized (self) {
        if ([NSObject isNull:self.clientBillIdentifier] || ![clientBillIdentifier isEqualToString:self.clientBillIdentifier]) {
            if (self.fileHandle) {
                [self logMessage:YCLogLevelInfo message:[NSString stringWithFormat:@"Switching bill to `%@`", clientBillIdentifier] metadata:nil component:@"app"];
                [self.fileHandle closeFile];
                
                if (self.automaticallyUploadLogs) {
                    NSString* oldClientBillIdentifier = self.clientBillIdentifier;
                    __weak OnoIOSLogger* weakSelf = self;
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [weakSelf uploadLogFile:oldClientBillIdentifier logLevel:YCLogLevelInfo excludeComponents:nil];
                    });
                }
            }
            
            _clientBillIdentifier = clientBillIdentifier;
            NSString* path = [self pathForBillIdentifier:clientBillIdentifier];
            
            
            // Open our file
            self.fileHandle = [NSFileHandle fileHandleForWritingAtPath:path];
            if (!self.fileHandle) {
                
                BOOL fileCreated = [[NSFileManager defaultManager] createFileAtPath:path contents:nil attributes:nil];
                if (fileCreated) {
                    NSLog(@"Created log file at: %@", path);
                    self.fileHandle = [NSFileHandle fileHandleForWritingAtPath:path];
                } else {
                    NSLog(@"Failed to create file at: %@", path);
                }
                
            } else {
                NSLog(@"Opened log file at: %@", path);
            }
            
            if (self.fileHandle) {
                [self.fileHandle seekToEndOfFile];
            }
        }
    }
}

-(void) logMessage:(NSInteger)logLevel message:(nonnull NSString*)message metadata:(nullable NSDictionary*)metadata component:(nullable NSString*)component {
    @synchronized (self) {
        if (self.fileHandle) {
            @try {                
                // This is from Bugsnag
                if ([message hasPrefix:@"Could not serialize breadcrumb data for"]) {
                    return;
                } else if ([message hasPrefix:@"Warning: isMounted(...) is deprecated"]) {
                    return;
                }
                
                NSMutableDictionary* lineData = [@{
                                                   @"logLevel": @(logLevel),
                                                   @"component": component ? component : @"app",
                                                   @"message": message,
                                                   @"timestamp": [self.dateFormatter stringFromDate:[[NSDate alloc] init]]
                                                   } mutableCopy];
                if (metadata && metadata.count > 0) {
                    [lineData setValue:metadata forKey:@"metadata"];
                }
                
                NSError* error;
                NSData* tmp = [NSJSONSerialization dataWithJSONObject:lineData options:0 error:&error];
                [self.fileHandle writeData:tmp];
                [self.fileHandle writeData:[@"\n" dataUsingEncoding:NSUTF8StringEncoding]];
                [self.fileHandle synchronizeFile];
            }
            @catch (NSException *exception) {
                NSLog(@"Could not serialize JSON for log: %@ -> %@", message, exception);
            }
        }
    }
}

/**
 * Only log a message if it has changed in this category. This can be used to log distinct payment state updates
 */
-(void) logMessageIfChanged:(nonnull NSString*)category logLevel:(NSInteger)logLevel message:(nonnull NSString*)message metadata:(nullable NSDictionary*)metadata component:(nullable NSString*)component {

    if (![[self.lastCategoryLogs valueForKey:category] isEqualToString:message]) {
        [self.lastCategoryLogs setValue:message forKey:category];
        [self logMessage:logLevel message:message metadata:metadata component:component];
    }
}

-(BOOL) checkLogExists:(NSString *)clientBillIdentifier {
    NSString* path = [self pathForBillIdentifier:clientBillIdentifier];
    // Attempt to read the file
    // If we cannot read it does not exist
    self.fileHandle = [NSFileHandle fileHandleForReadingAtPath:path];
    if (self.fileHandle)
        return true;
    return false;
}

-(BOOL) uploadCurrentLogFile {
    return [self uploadLogFile:self.clientBillIdentifier logLevel:YCLogLevelInfo excludeComponents:nil];
}

-(BOOL) uploadCurrentLogFile:(YCLogLevel)logLevel {
    return [self uploadLogFile:self.clientBillIdentifier logLevel:logLevel excludeComponents:nil];
}

-(BOOL) uploadCurrentLogFileExcludeComponents:(nullable NSArray*)excludeComponents {
    return [self uploadLogFile:self.clientBillIdentifier logLevel:YCLogLevelInfo excludeComponents:excludeComponents];
}

/*
 Upload a log file at the given log level, calls back with a boolean indicating if the log file exists
 */
-(BOOL) uploadLogFile:(nonnull NSString*)clientBillIdentifier logLevel:(NSInteger)logLevel excludeComponents:(nullable NSArray*)excludeComponents {
    return [self uploadLogFile:clientBillIdentifier logLevel:logLevel excludeComponents:excludeComponents completeCallback:nil];
}

-(BOOL)uploadLogFile:(NSString *)clientBillIdentifier logLevel:(NSInteger)logLevel excludeComponents:(NSArray *)excludeComponents completeCallback:(void (^)(void))completeCallback {
    // Try and load this file
    NSString* path = [self pathForBillIdentifier:clientBillIdentifier];
    NSFileHandle* fileHandle = [NSFileHandle fileHandleForReadingAtPath:path];
    
    NSSet* excludeSet = [[NSSet alloc] initWithArray:excludeComponents];
    
    if (fileHandle) {
        unsigned long long fileSize = [fileHandle seekToEndOfFile];
        [fileHandle seekToFileOffset:0];
        
        if (fileSize > MAX_LOG_SIZE) {
            // Only send the last part of our log if it is huge
            [fileHandle seekToFileOffset:(fileSize - MAX_LOG_SIZE)];
        }
        
        NSData* data = [fileHandle readDataToEndOfFile];
        NSString* dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        if (fileSize > MAX_LOG_SIZE) {
            // If we started in the middle of our file then we need to skip to the first newline
            
            NSRange range = [dataString rangeOfString:@"\n"];
            if (range.location == NSNotFound) {
                dataString = @"{}";
            } else {
                dataString = [dataString substringFromIndex:range.location + 1];
            }
        }
        
        NSArray* logLines = [dataString componentsSeparatedByString:@"\n"];
        NSMutableArray* includeLines = [[NSMutableArray alloc] init];
        
        NSString* startDate = NULL;
        NSString* endDate = NULL;
        
        for (NSString* line in logLines) {
            NSError* error;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:[line dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];
            
            if (!error) {
                if (!startDate) {
                    startDate = [json valueForKey:@"timestamp"];
                }
                endDate = [json valueForKey:@"timestamp"];
                
                if ([[json valueForKey:@"logLevel"] integerValue] >= logLevel && ![excludeSet containsObject:[json valueForKey:@"component"]]) {
                    [includeLines addObject:line];
                }
            }
        }
        
        if (includeLines.count > 0) {
            NSString* sendString = [includeLines componentsJoinedByString:@"\n"];
            @try {
                NSData* compressedData = [[sendString dataUsingEncoding:NSUTF8StringEncoding] safeCompressString];
                
                if (!startDate) {
                  startDate = nil;
                }
                if (!endDate) {
                  endDate = nil;
                }
                
                
                NSError* error;
                NSData* meta = [NSJSONSerialization dataWithJSONObject:self.metadata options:0 error:&error];
                NSString* metadata = nil;
                
                if (!error) {
                    metadata = [[NSString alloc] initWithData:meta encoding:NSUTF8StringEncoding];
                }
                
                NSDictionary* queryParameters = @{
                                                  @"sourceUUID": clientBillIdentifier,
                                                  @"logType": @"transaction",
                                                  @"deviceIdentifier": debugInfo.deviceIdentifier,
                                                  @"isStructured": @"true",
                                                  @"canFetchDetailed": logLevel > YCLogLevelDebug ? @"true" : @"false",
                                                  @"startDate": startDate,
                                                  @"endDate": endDate,
                                                  @"metadata": metadata
                                                  };
                
                [[[[[OnoWebRequest requestWithPath:@"/logs/"] withQueryParameters:queryParameters] withFile:@"compressedLog" filename:@"logFile.snappy" andData:compressedData contentType:@"application/octet-stream"] withRetries:true] post:^(NSInteger statusCode, NSDictionary *json) {
                    NSLog(@"Successfully posted log file for clientBillIdentifier: %@", clientBillIdentifier);
                    if (completeCallback) {
                        completeCallback();
                    }
                } failure:^(NSInteger statusCode, NSError *error, NSDictionary *json) {
                    NSLog(@"Unable to post log file for clientBillIdentifier: %@\n%@", clientBillIdentifier, error);
                    if (completeCallback) {
                        completeCallback();
                    }
                }];
            } @catch (NSException *exception) {
                NSLog(@"Unable to compress or upload log files: %@", exception);
                if (completeCallback) {
                    completeCallback();
                }
                
            }
            
        }
        
        return true;
    } else {
        // The file does not exist
        NSLog(@"Unable to post log file for clientBillIdentifier %@ because it doesn't exist", clientBillIdentifier);
        return false;
    }
}

-(void) uploadLogsInRange:(NSDate*)startDate endDate:(NSDate*)endDate logLevel:(NSInteger)logLevel {
    NSString* logsPath = NSTemporaryDirectory();
    NSError* error;
    NSArray* directoryContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:logsPath error:&error];
    
    NSMutableArray* clientBillIdentifiers = [[NSMutableArray alloc] init];
    
    if (error) {
        NSLog(@"Error listing directory contents for '%@' - %@", logsPath, error);
    } else {
        for (NSString* file in directoryContents) {
            if ([file hasSuffix:@".log"]) {
                NSError* attributesError;
                NSString* filePath = [NSString stringWithFormat:@"%@%@", logsPath, file];
                NSDictionary *attributes = [[NSFileManager defaultManager] attributesOfItemAtPath:filePath error:&attributesError];
                
                if (attributesError) {
                    NSLog(@"Error fetching attributes for file '%@' - %@", filePath, attributesError);
                } else {
                    NSDate* lastModified = attributes.fileModificationDate;
                    NSDate* created = attributes.fileCreationDate;
                    
                    if (([lastModified compare:endDate] == NSOrderedAscending && [lastModified compare:startDate] == NSOrderedDescending)
                        || ([created compare:endDate] == NSOrderedAscending && [created compare:startDate] == NSOrderedDescending)) {
                        NSString* clientBillIdentifier = [file substringToIndex:[file rangeOfString:@"." options:NSBackwardsSearch].location];
                        [clientBillIdentifiers addObject:clientBillIdentifier];
                    }
                }
            }
        }
    }
    
    [self uploadLogsForIdentifiers:clientBillIdentifiers logLevel:logLevel];
}

-(void) uploadLogsForIdentifiers:(NSArray*)clientBillIdentifiers logLevel:(NSInteger)logLevel {
    __weak typeof(self) weakSelf = self;
    
    if (clientBillIdentifiers.count > 0) {
        NSString* clientBillIdentifier = [clientBillIdentifiers firstObject];
        NSArray* remainingIdentifiers = [clientBillIdentifiers subarrayWithRange:NSMakeRange(1, clientBillIdentifiers.count - 1)];
        
        [self uploadLogFile:clientBillIdentifier logLevel:logLevel excludeComponents:nil completeCallback:^{
            [weakSelf uploadLogsForIdentifiers:remainingIdentifiers logLevel:logLevel];
        }];
    }
}


@end
