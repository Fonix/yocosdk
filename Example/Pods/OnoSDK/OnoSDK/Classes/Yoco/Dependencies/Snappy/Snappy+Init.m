//
//  Snappy+Init.m
//  Yoco
//
//  Created by admin on 2015/12/07.
//  Copyright © 2015 Yoco Technologies (PTY) LTD. All rights reserved.
//

#import "Snappy+Init.h"
#import "Snappy-ObjC.h"
#import "snappy.hh"

@implementation NSString (SnappyInit)

-(NSData *) safeCompressString {
    return [[self dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO] safeCompressString];
}

@end

@implementation NSData (SnappyInit)


- (NSData *) safeCompressString {
    // Attempt to fix a crash where data is mutated while we are compressing it
    // https://app.bugsnag.com/yoco-technologies-pty-ltd/yoco-register/errors/59a7c85edc951c0019ac1859?event_id=59cbd4f3241d5c0018927012&i=sk&m=oc
    // NSGenericException · *** Collection <__NSArrayM: 0x14f903f0> was mutated while being enumerated.
    NSData* dataCopy = [self copy];
    
    struct snappy_env env;
    snappy_init_env(&env);
    size_t clen = snappy_max_compressed_length(self.length);
    NSAssert(clen >= 0, @"The length of the compressed buffer shouldn't be zero");
    char *buffer = malloc(clen);
    snappy_compress(&env, dataCopy.bytes, dataCopy.length, buffer, &clen);
    snappy_free_env(&env);
    return [NSData dataWithBytesNoCopy:buffer length:clen];
}

@end
