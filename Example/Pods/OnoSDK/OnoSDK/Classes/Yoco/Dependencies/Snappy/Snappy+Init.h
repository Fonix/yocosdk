//
//  Snappy+Init.h
//  Yoco
//
//  Created by admin on 2015/12/07.
//  Copyright © 2015 Yoco Technologies (PTY) LTD. All rights reserved.
//

@interface NSString (SnappyInit)

- (NSData *) safeCompressString;

@end


@interface NSData (SnappyInit)

- (NSData *) safeCompressString;

@end
