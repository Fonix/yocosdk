//
//  YCDebugInfo.m
//  YocoSDK
//
//  Created by Andrew Snowden on 2018/06/03.
//  Copyright © 2018 Yoco. All rights reserved.
//

#import "YCDebugInfo.h"
#import "OnoDeviceUID.h"
#import <UIKit/UIKit.h>
#import "sys/utsname.h"

//#import "YCPaymentHandler.h"
#import "YCUtils.h"
//#import <mpos_core/mpos.h>

NSString* deviceModelName()
{
    struct utsname systemInfo;
    uname(&systemInfo);
    
    return [NSString stringWithCString:systemInfo.machine
                              encoding:NSUTF8StringEncoding];
}

@implementation YCDebugInfo

+(YCDebugInfo*) sharedDebugInfo {
    static YCDebugInfo *sharedDebugInfo = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedDebugInfo = [[YCDebugInfo alloc] init];
    });
    return sharedDebugInfo;
}

-(NSString *)appVersion {
    if (_appVersion) {
        return _appVersion;
    }
    return self.hostAppVersion;
}

-(NSString*) hostAppVersion {
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    
    NSString* appVersion = [infoDict objectForKey:@"CFBundleShortVersionString"];
    NSNumber* buildNumber = [infoDict objectForKey:@"CFBundleVersion"];
    
    return [NSString stringWithFormat:@"%@ build %@", appVersion, buildNumber];
}

-(NSString*) appName {
    return [NSString emptyIfNil:[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"]];
}

-(NSString*) sdkVersion {
    return [[NSBundle bundleForClass:self.class] objectForInfoDictionaryKey: @"CFBundleShortVersionString"]; //[YCPaymentHandler sdkVersion];
}

-(NSString*) mposVersion {
    return @"MPOS REMOVED"; //[MPMpos version];
}

-(NSString*) deviceMake {
    return @"Apple";
}

-(NSString*) deviceModel {
    return deviceModelName();
}

-(NSString*) deviceName {
    return UIDevice.currentDevice.name;
}

-(NSString*) osName {
    return UIDevice.currentDevice.systemName;
}

-(NSString*) osVersion {
    return UIDevice.currentDevice.systemVersion;
}

-(NSString*) deviceIdentifier {
    return [DeviceUID uid];
}

@end
