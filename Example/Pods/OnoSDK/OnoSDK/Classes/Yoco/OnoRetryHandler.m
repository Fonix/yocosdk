//
//  OnoRetryHandler.m
//  Yoco
//
//  Created by Kurt Kruger on 2015/04/22.
//  Copyright (c) 2015 Yoco Technologies (PTY) LTD. All rights reserved.
//

#import "OnoRetryHandler.h"
#import <objc/objc.h>
#import "OnoWebRequest.h"
#import "OnoConstants.h"
#import "YCUtils.h"

#define DISABLE_DEBUG_RETRIES true

#define OnoUnprocessedRequests @"OnoUnprocessedRequests"

static NSNumber* retriesEnabled;

@interface OnoRetryHandler()

@property (nonatomic, assign) NSTimeInterval retryInterval;
@property (nonatomic, strong) NSTimer *timer;

@end

@implementation OnoRetryHandler

-(id) init {
    self = [super init];
    if (self) {
        self.retryInterval = 60;
        self.shouldPerformRetries = true;
        [self scheduleRetries];
    }
    
    return self;
}

+(OnoRetryHandler*) instance {
    static OnoRetryHandler* instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[OnoRetryHandler alloc] init];
    });
    
    return instance;
}

-(void) addRequest:(OnoWebRequest *)request {
    [self addRequest:request success:nil failure:nil];
}

-(void)addRequest:(OnoWebRequest*)request success:(void (^)(NSInteger statusCode, NSDictionary* json))success failure:(void (^)(NSInteger statusCode, NSError *error, NSDictionary* json))failure {
    [self add:request];
    [self executeRequest:request success:success failure:failure];
}

-(void) scheduleRetries {
    if (!self.timer){
        self.timer = [NSTimer scheduledTimerWithTimeInterval:self.retryInterval target:self selector:@selector(process) userInfo:nil repeats:YES];
    }
}

-(void)process {
    if ([self hasUnprocessed]) {
        
        NSArray *unprocessed = [self listOfUnprocessed];
        
        for (int i = 0; i < MIN([unprocessed count], 5); i++) {
            NSData *encodedObject = [unprocessed objectAtIndex:i];
            OnoWebRequest* toExecute = (OnoWebRequest *)[NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
            if ((toExecute.lastExecuted.timeIntervalSinceNow * -1) >= self.retryInterval) {
                if (toExecute.retryCount <= toExecute.maxRetries && self.shouldPerformRetries) { // Disable retries on the simulator
                    [self executeRequest:toExecute success:nil failure:nil];
                } else {
                    [self removeAtIndex:[self indexOfObject:toExecute]];
                }
            }
        }
    }
}


-(void)executeRequest:(OnoWebRequest*)request success:(void (^)(NSInteger statusCode, NSDictionary* json))success failure:(void (^)(NSInteger statusCode, NSError *error, NSDictionary* json))failure {
    request.retryCount++;
    request.lastExecuted = [NSDate date];
    
    [request executeAttempt:^(NSInteger statusCode, NSDictionary *json) {
        [self removeAtIndex:[self indexOfObject:request]];
        if (request.successNotificationId) {
            [[NSNotificationCenter defaultCenter] postNotificationName:OnoWebRequestDidUpdate object:request];
        }
        if (success) {
            success(statusCode, json);
        }
    } failure:^(NSInteger statusCode, NSError *error, NSDictionary *json) {
        if (request.failureNotificationId) {
            [[NSNotificationCenter defaultCenter] postNotificationName:OnoWebRequestDidUpdate object:request];
        }
        if (failure) {
            failure(statusCode, error, json);
        }

        if (DISABLE_DEBUG_RETRIES) {
            #ifdef DEBUG
                [self removeAtIndex:[self indexOfObject:request]];
            #endif
        } else {
            [self updateAndRotate:request];
        }
        
    }];
}

-(OnoWebRequest *)objectAtIndex:(NSInteger)index {
    NSData *encodedObject = [self atIndex:index];
    OnoWebRequest *object = (OnoWebRequest *)[NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
    return object;
}

-(BOOL)hasUnprocessed {
    NSArray* array = [self listOfUnprocessed];
    return [NSArray isNotEmpty:array];
}

-(NSArray*)listOfUnprocessed {
    return [NSArray arrayFromDefaultsWithKey:OnoUnprocessedRequests];
}

-(NSInteger) indexOfObject:(OnoWebRequest*)request {
    
    int count = -1;
    
    for(id encodedObject in [self listOfUnprocessed]){
        count++;
        OnoWebRequest* unprocessed = (OnoWebRequest *)[NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
        if([request.webRequestUUID isEqualToString:unprocessed.webRequestUUID]) {
            return count;
        }
    }
    
    return count;
}

-(void)add:(OnoWebRequest *)object {
    @synchronized(self.class) {
        NSArray* original = [self listOfUnprocessed];
        if (!original) {
            original = [[NSArray alloc] init];
        }
        
        NSMutableArray* updatedArray = [NSMutableArray arrayWithArray:original];
        [updatedArray addObject:[self encodeObject:object]];
        
        [updatedArray saveToDefaultsWithKey:OnoUnprocessedRequests];
    }
}

-(void)updateAndRotate:(OnoWebRequest*)request {
    @synchronized(self.class) {
        NSMutableArray* list = [[self listOfUnprocessed] mutableCopy];
    
        NSInteger objectIndex = [self indexOfObject:request];
    
        if(objectIndex == -1) return;
    
        OnoWebRequest* object = (OnoWebRequest *)[self objectAtIndex:objectIndex];
    
        [list removeObjectAtIndex:objectIndex];
        [list addObject:[self encodeObject:object]];
        
        [list saveToDefaultsWithKey:OnoUnprocessedRequests];
    };
}

-(id)atIndex:(NSInteger)index {
    NSArray* list = [self listOfUnprocessed];
    if (index < [list count])
        return [list objectAtIndex:index];
    else
        return nil;
}

-(id)removeAtIndex:(NSInteger)index {
    @synchronized(self.class) {
        id object = [self atIndex:index];
        if ([NSObject isNull:object]) return nil;
        NSArray* original = [NSArray arrayFromDefaultsWithKey:OnoUnprocessedRequests];
        NSMutableArray* updatedArray = [NSMutableArray arrayWithArray:original];
        if (index < updatedArray.count) {
            [updatedArray removeObjectAtIndex: index];
            [updatedArray saveToDefaultsWithKey:OnoUnprocessedRequests];
        }
        return object;
    }
}

-(NSData *) encodeObject:(OnoWebRequest *)request {
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:request];
    return encodedObject;
}

@end

