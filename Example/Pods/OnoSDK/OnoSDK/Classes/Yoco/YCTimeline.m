//
//  YCTimeline.m
//  YocoSDK
//
//  Created by Andrew Snowden on 2018/10/07.
//  Copyright © 2018 Yoco. All rights reserved.
//

#import "YCTimeline.h"
#import "NSArray+Utils.h"
#import "NSDictionary+Utils.h"
#import "NSNull+IfEmpty.h"

#define CURRENT_TIME_MILLIS() ((unsigned long)round([[NSDate date] timeIntervalSince1970] * 1000))

@implementation YCTimelineTask

-(id) init {
    self = [super init];
    if (self) {
        self.startMillis = CURRENT_TIME_MILLIS();
    }
    
    return self;
}

-(id)initWithTaskName:(NSString *)taskName {
    self = [self init];
    self.taskName = taskName;
    return self;
}

-(id)initWithTaskName:(NSString *)taskName withMetadata:(NSDictionary *)metadata {
    self = [self initWithTaskName:taskName];
    self.metadata = metadata;
    return self;
}

-(void) stopTask {
    self.endMillis = CURRENT_TIME_MILLIS();
}

-(NSDictionary *)toDictionary {
    unsigned long endMillis = self.endMillis;
    if (endMillis == 0) {
        endMillis = CURRENT_TIME_MILLIS();
    }
    
    NSDictionary* d = @{
             @"taskName": self.taskName ? self.taskName : @"unknown",
             @"startMillis": @(self.startMillis),
             @"endMillis": @(endMillis),
             };
    
    if (self.metadata) {
        return [d dictionaryByMergingWith:@{@"metadata": self.metadata}];
    }
    
    return d;
}

@end

@implementation YCTimelineEvent

-(id) initWithTitle:(NSString*)title eventType:(YCTimelineEventType)eventType {
    self = [self init];
    if (self) {
        self.title = title;
        self.eventType = eventType;
        self.atMillis = CURRENT_TIME_MILLIS();
    }
    return self;
}

-(id) initWithTitle:(NSString*)title eventType:(YCTimelineEventType)eventType metadata:(NSDictionary*)metadata {
    self = [self initWithTitle:title eventType:eventType];
    if (self) {
        self.metadata = metadata;
    }
    return self;
}

-(NSString*) stringEventType {
    switch (self.eventType) {
        case YCTimelineEventTypeInfo:
            return @"info";
        case YCTimelineEventTypeWarn:
            return @"warn";
        case YCTimelineEventTypeError:
            return @"error";
        case YCTimelineEventTypeSuccess:
            return @"success";
            
    }
}

-(NSDictionary*) toDictionary {
    NSDictionary* d = @{
             @"title": self.title,
             @"eventType": [self stringEventType],
             @"atMillis": @(self.atMillis)
             };
    
    if (self.desc != nil) {
        d = [d dictionaryByMergingWith:@{@"description": self.desc}];
    }
    
    if (self.metadata) {
        d = [d dictionaryByMergingWith:@{@"metadata": self.metadata}];
    }
    
    return d;
}

@end

@interface YCTimeline ()

// When important events we want to add it to any running timelines
+(NSPointerArray*)runningTimelines;
+(void) timelineCreated:(YCTimeline*)timeline;
+(void) timelineDealloced:(YCTimeline*)timeline;

+(NSMutableDictionary*)keyedTasks;

@end

@implementation YCTimeline

-(id) init {
    self = [super init];
    if (self) {
        self.tasks = [[NSMutableArray alloc] init];
        self.events = [[NSMutableArray alloc] init];
        
        [YCTimeline timelineCreated:self];
    }
    return self;
}

-(void)dealloc {
    [YCTimeline timelineDealloced:self];
}

-(void)addTimelineEvent:(YCTimelineEvent *)event {
    [self.events addObject:event];
}

-(YCTimelineTask *)startTask:(NSString *)taskName {
    return [self addTask:[[YCTimelineTask alloc] initWithTaskName:taskName]];
}

-(YCTimelineTask *)startTask:(NSString *)taskName withMetadata:(nonnull NSDictionary *)metadata {
    return [self addTask:[[YCTimelineTask alloc] initWithTaskName:taskName withMetadata:metadata]];
}

-(YCTimelineTask*) addTask:(YCTimelineTask *)task {
    [self.tasks addObject:task];
    return task;
}

-(YCTimelineTask*)stopTask:(NSString *)taskName {
    for (int i = 0; i < self.tasks.count; i++) {
        YCTimelineTask* task = [self.tasks objectAtIndex:self.tasks.count - i - 1];
        if ([task.taskName isEqualToString:taskName]) {
            [task stopTask];
            return task;
        }
    }
    
    return nil;
}

-(NSDictionary *)toDictionary {
    return @{
             @"tasks": [self.tasks map:^id(YCTimelineTask* task, NSUInteger idx) {
                 return [task toDictionary];
             }],
             @"events": [self.events map:^id(YCTimelineEvent* event, NSUInteger idx) {
                 return [event toDictionary];
             }]
    };
}

+(NSPointerArray *)runningTimelines {
    static NSPointerArray* runningTimelines = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        runningTimelines = [NSPointerArray weakObjectsPointerArray];
    });

    return runningTimelines;
}

+(void) timelineCreated:(YCTimeline*)timeline {
    [[YCTimeline runningTimelines] addPointer:(__bridge void *)timeline];
}

+(void) timelineDealloced:(YCTimeline *)timeline {
    NSPointerArray* arr = [YCTimeline runningTimelines];
    
    @synchronized (arr) {
        NSMutableSet *indexesToRemove = [NSMutableSet new];
        for (NSUInteger i = 0; i < [arr count]; i++) {
            if (![arr pointerAtIndex:i]) { // is the pointer null? then remove it
                [indexesToRemove addObject:@(i)];
            }
        }
        
        for (NSNumber *indexToRemove in indexesToRemove) {
            [arr removePointerAtIndex:[indexToRemove unsignedIntegerValue]];
        }
    }
}

+(void)addEventToRunningTimelines:(YCTimelineEvent *)event {
    for (YCTimeline* timeline in [YCTimeline runningTimelines]) {
        [timeline addTimelineEvent:event];
    }
}

+(void) addTaskToRunningTimelines:(YCTimelineTask *)task {
    for (YCTimeline* timeline in [YCTimeline runningTimelines]) {
        [timeline addTask:task];
    }
    
}

+(NSMutableDictionary *)keyedTasks {
    static NSMutableDictionary* keyedTasks = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        keyedTasks = [[NSMutableDictionary alloc] init];
    });
    
    return keyedTasks;
}

+(YCTimelineTask*) addKeyedTask:(NSString*)taskName key:(NSString*)key {
    YCTimelineTask* task = [[YCTimelineTask alloc] initWithTaskName:taskName];
    
    NSArray* tasksForKey = [[YCTimeline keyedTasks] valueForKey:key];
    if (!tasksForKey) {
        tasksForKey = @[task];
    } else {
        tasksForKey = [tasksForKey arrayByAddingObject:task];
    }
    
    [[YCTimeline keyedTasks] setValue:tasksForKey forKey:key];
    return task;
}

+(YCTimelineTask*) stopKeyedTask:(NSString *)taskName key:(NSString *)key {
    NSMutableDictionary* keyedTasks = [YCTimeline keyedTasks];
    NSArray* tasksForKey = [keyedTasks valueForKey:key];
    if (tasksForKey.count > 0) {
        YCTimelineTask* timelineTask = tasksForKey.firstObject;
        [timelineTask stopTask];
        
        if (tasksForKey.count == 1) {
            [keyedTasks setValue:nil forKey:key];
        } else {
            [keyedTasks setValue:[tasksForKey subarrayWithRange:NSMakeRange(1, tasksForKey.count - 1)] forKey:key];
        }
        
        return timelineTask;
    } else {
        return nil;
    }
}

@end
