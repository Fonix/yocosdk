//
//  OnoWebRequest.m
//  Yoco
//
//  Created by Andrew Snowden on 2015/08/08.
//  Copyright (c) 2015 Yoco Technologies (PTY) LTD. All rights reserved.
//

#import "OnoWebRequest.h"
#import "NSURL+PathParameters.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "NSError+LocalizedDescription.h"
#import "OnoRetryHandler.h"
#import <UIKit/UIKit.h>
#import "YCDebugInfo.h"
#import "OnoRetryHandler.h"
#import "YCTimeline.h"
#import "YCUtils.h"
#import "NSError+PrettyError.h"

@interface OnoWebRequest ()

@property (nonatomic, retain) YCTimelineTask* timelineTask;

@end

@implementation OnoWebRequest

#if DEBUG
static NSString* baseURL = @"https://core.fish.yocobeta.co.za";
//static NSString* baseURL = @"http://192.168.1.15:9090";
//static NSString* baseURL = @"https://core.yoco.co.za";
#else
static NSString* baseURL = @"https://core.yoco.co.za";
#endif


+(void) setBaseURL:(NSString*)newBaseURL {
    baseURL = newBaseURL;
}

-(id) init {
    self = [super init];
    if (self) {
        self.baseURL = baseURL;

        self.webRequestUUID =  [[NSUUID UUID] UUIDString];
        self.lastExecuted =  [NSDate date];
        self.retryCount = 0;
        self.maxRetries = 5;        
        self.endPoint = @"client";
    }
    return self;
}

-(id) initWithPath:(NSString*)path {
    self = [self init];
    self.requestPath = path;
    return self;
}

-(OnoWebRequest*) withQueryParameters:(NSDictionary*)queryParameters {
    self.queryParameters = queryParameters;
    return self;
}

-(OnoWebRequest*)withJSONBody:(id)jsonBody {
    self.jsonBody = jsonBody;
    return self;
}

-(OnoWebRequest*)withCustomHeaders:(NSDictionary*)customHeaders {
    self.customHeaders = customHeaders;
    return self;
}

-(OnoWebRequest*) withRetries:(BOOL)shouldRetry {
    self.shouldRetry = shouldRetry;
    return self;
}

-(OnoWebRequest*) withEndPoint:(NSString *)endPoint {
    self.endPoint = endPoint;
    return self;
}

-(OnoWebRequest*) withSuccessNotification:(NSString *)sId {
    self.successNotificationId = sId;
    return self;
}

-(OnoWebRequest*) withFailureNotification:(NSString *)sId {
    self.failureNotificationId = sId;
    return self;
}


-(OnoWebRequest*) withFile:(NSString*)fieldName filename:(NSString*)filename andData:(NSData*)data contentType:(NSString*)contentType {
    if (!self.multipartFiles) {
        self.multipartFiles = [[NSMutableArray alloc] init];
    }
    
    [self.multipartFiles addObject:@{
                                    @"fieldName": fieldName,
                                    @"filename": filename,
                                    @"data": data,
                                    @"contentType": contentType
                                    }];

    self.jsonBody = nil;
    return self;
}

-(OnoWebRequest*) withBaseURLAndPath:(NSString*) baseURL path:(NSString*)path {
    self.baseURL = baseURL;
    self.requestPath = path;
    self.endPoint = nil;
    return self;
}

+(OnoWebRequest*) requestWithPath:(NSString*)path {
    OnoWebRequest* request = [[OnoWebRequest alloc] initWithPath:path];
    
    return request;
}

-(OnoWebRequest*)get:(void (^)(NSInteger statusCode, NSDictionary* json))success failure:(void (^)(NSInteger statusCode, NSError *error, NSDictionary* json))failure {
    self.method = @"GET";
    return [self execute:success failure:failure];
}

-(OnoWebRequest*)post:(void (^)(NSInteger statusCode, NSDictionary* json))success failure:(void (^)(NSInteger statusCode, NSError *error, NSDictionary* json))failure {
    self.method = @"POST";
    return [self execute:success failure:failure];}

-(OnoWebRequest*)del:(void (^)(NSInteger statusCode, NSDictionary* json))success failure:(void (^)(NSInteger statusCode, NSError *error, NSDictionary* json))failure {
    self.method = @"DELETE";
    return [self execute:success failure:failure];
}

-(id) initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if(self) {
        self.webRequestUUID = [decoder decodeObjectForKey:@"webRequestUUID"];
        self.successNotificationId = [decoder decodeObjectForKey:@"successNotificationId"];
        self.failureNotificationId = [decoder decodeObjectForKey:@"failureNotificationId"];
        self.baseURL = [decoder decodeObjectForKey:@"baseURL"];
        self.endPoint = [decoder decodeObjectForKey:@"endPoint"];
        self.requestPath = [decoder decodeObjectForKey:@"requestPath"];
        self.method = [decoder decodeObjectForKey:@"method"];
        self.queryParameters = [decoder decodeObjectForKey:@"queryParameters"];
        self.jsonBody = [decoder decodeObjectForKey:@"jsonBody"];
        
        // Previous versions stored filename and filedata instead of a list of files
        NSString* filename = [decoder decodeObjectForKey:@"filename"];
        NSData* fileData = [decoder decodeObjectForKey:@"fileData"];
        NSArray* files = [decoder decodeObjectForKey:@"multipartFiles"];
        
        if (files) {
            self.multipartFiles = [files mutableCopy];
        } else if (filename && fileData) {
            // Legacy support
            [self withFile:@"picture" filename:filename andData:fileData contentType:@"image/png"];
        }
        self.firstExecuted = [decoder decodeObjectForKey:@"firstExecuted"];
        self.lastExecuted = [decoder decodeObjectForKey:@"lastExecuted"];
        self.retryCount = [[decoder decodeObjectForKey:@"retryCount"] integerValue];
        self.maxRetries = [[decoder decodeObjectForKey:@"maxRetries"] integerValue];
        self.method = [decoder decodeObjectForKey:@"method"];
    }
    
    return self;
}

-(void) encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.webRequestUUID forKey:@"webRequestUUID"];
    [encoder encodeObject:self.successNotificationId forKey:@"successNotificationId"];
    [encoder encodeObject:self.failureNotificationId forKey:@"failureNotificationId"];
    [encoder encodeObject:self.baseURL forKey:@"baseURL"];
    [encoder encodeObject:self.endPoint forKey:@"endPoint"];
    [encoder encodeObject:self.requestPath forKey:@"requestPath"];
    [encoder encodeObject:self.method forKey:@"method"];
    [encoder encodeObject:self.queryParameters forKey:@"queryParameters"];
    [encoder encodeObject:self.jsonBody forKey:@"jsonBody"];
    [encoder encodeObject:self.multipartFiles forKey:@"multipartFiles"];
    [encoder encodeObject:self.firstExecuted forKey:@"firstExecuted"];
    [encoder encodeObject:self.lastExecuted forKey:@"lastExecuted"];
    [encoder encodeObject:@(self.retryCount) forKey:@"retryCount"];
    [encoder encodeObject:@(self.maxRetries) forKey:@"maxRetries"];
    [encoder encodeObject:self.method forKey:@"method"];
}

-(NSString*) url {
    if (self.endPoint != nil) {
        if ([self.requestPath hasPrefix:@"/"]) {
            return [NSString stringWithFormat:@"%@/api/%@/v2%@", self.baseURL, self.endPoint, self.requestPath];
        } else {
            return [NSString stringWithFormat:@"%@/api/%@/v2/%@", self.baseURL, self.endPoint, self.requestPath];
        }
    } else {
        if ([self.requestPath hasPrefix:@"/"]) {
            return [NSString stringWithFormat:@"%@%@", self.baseURL, self.requestPath];
        } else {
            return [NSString stringWithFormat:@"%@/%@", self.baseURL, self.requestPath];
        }
    }
}

- (NSString *)mimeTypeForPath:(NSString *)path
{
    // get a mime type for an extension using MobileCoreServices.framework
    
    CFStringRef extension = (__bridge CFStringRef)[path pathExtension];
    CFStringRef UTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, extension, NULL);
    assert(UTI != NULL);
    
    NSString *mimetype = CFBridgingRelease(UTTypeCopyPreferredTagWithClass(UTI, kUTTagClassMIMEType));
    assert(mimetype != NULL);
    
    CFRelease(UTI);
    
    return mimetype;
}

-(NSMutableURLRequest*) urlRequest {
    if (_urlRequest) {
        return _urlRequest;
    }
    else {
        NSMutableURLRequest* request = nil;
        
        if (self.multipartFiles.count == 0) {
            request = [NSMutableURLRequest requestWithURL:[[NSURL URLWithString:self.url] URLByAppendingParameters:self.queryParameters]];
            [request setHTTPMethod:self.method];
        } else {
            request = [NSMutableURLRequest requestWithURL:[[NSURL URLWithString:self.url] URLByAppendingParameters:self.queryParameters]];
            [request setHTTPMethod:@"POST"];
            
            NSString *boundary = @"---------------------------14737809831466499882746641449";
            NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
            [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
            
            NSMutableData *body = [NSMutableData data];
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            for (id file in self.multipartFiles) {
                [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", [file valueForKey:@"fieldName"], [file valueForKey:@"filename"]] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", [file valueForKey:@"contentType"]] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[NSData dataWithData:[file valueForKey:@"data"]]];
                [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            }
            
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [request setHTTPBody:body];
        }
        
        NSDictionary* headers = self.headers;
        for (NSString* key in headers) {
            [request addValue:[headers valueForKey:key] forHTTPHeaderField:key];
        }
        if (self.customHeaders) {
            for (NSString* key in self.customHeaders) {
                [request addValue:[self.customHeaders valueForKey:key] forHTTPHeaderField:key];
            }
        }
        
        if (self.jsonBody) {
            NSError* error;
            
            @try {
                NSData* tmp = [NSJSONSerialization dataWithJSONObject:self.jsonBody options:0 error:&error];
                [request setHTTPBody:tmp];
                [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            }
            @catch (NSException *exception) {
                OnoLogInfo(@"Could not serialize JSON for request: %@ -> %@", self.url, exception);
            }
        }
        
        return request;
    }
}

-(NSMutableDictionary*) headers {
    if (_headers) {
        return _headers;
    }
    else {
        NSMutableDictionary* headers = [[NSMutableDictionary alloc] init];
        NSDictionary* defaultHeaders = [OnoWebRequest defaultHeaders];
        
        for (NSString* key in defaultHeaders) {
            [headers setValue:[defaultHeaders valueForKey:key] forKey:key];
        }
        
        // add some data about the system that is making this call (this will allow us to track this data with the session and with the request)
        [headers setValue:debugInfo.appVersion forKey:HEADER_APP_VERSION];
        [headers setValue:debugInfo.hostAppVersion forKey:HEADER_HOST_APP_VERSION];
        [headers setValue:debugInfo.sdkVersion forKey:HEADER_SDK_VERSION];
        [headers setValue:debugInfo.mposVersion forKey:HEADER_MPOS_VERSION];
        [headers setValue:debugInfo.deviceModel forKey:HEADER_MODEL];
        [headers setValue:debugInfo.deviceMake forKey:HEADER_MAKE];
        [headers setValue:debugInfo.osName forKey:HEADER_OS_NAME];
        [headers setValue:debugInfo.osVersion forKey:HEADER_OS_VERSION];
        [headers setValue:debugInfo.deviceIdentifier forKey:HEADER_DEVICE_IDENTIFIER];
        
        return headers;
    }
}

+(NSDictionary*) defaultHeaders {
    static NSMutableDictionary* defaultHeaders;
    static dispatch_once_t singleton;
    
    dispatch_once(&singleton, ^{
        defaultHeaders = [[NSMutableDictionary alloc] init];
    });
    
    return defaultHeaders;
}

+(NSURLSession*) session {
    static NSURLSession* session;
    static dispatch_once_t singleton;
    
    dispatch_once(&singleton, ^{
        NSURLSessionConfiguration* configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        configuration.HTTPMaximumConnectionsPerHost = 10;
        configuration.timeoutIntervalForRequest = 60;
        configuration.timeoutIntervalForResource = 60;
        session = [NSURLSession sessionWithConfiguration:configuration];
    });
    
    return session;
}

+(void) setDefaultHeader:(NSString *)value forKey:(NSString *)key {
    [[OnoWebRequest defaultHeaders] setValue:value forKey:key];
}

-(OnoWebRequest *)execute:(void (^)(NSInteger statusCode, NSDictionary* json))success failure:(void (^)(NSInteger statusCode, NSError *error, NSDictionary* json))failure {
    if (self.shouldRetry) {
        // A request that fails will retry, but will not call the success callback when it successfully completes
        [[OnoRetryHandler instance] addRequest:self success:success failure:failure];
        return self;
    }
    else {
        return [self executeAttempt:success failure:failure];
    }
}

-(NSString *)description {
    return [NSString stringWithFormat:@"%@ %@", self.method, self.urlRequest.URL];
}
    
-(OnoWebRequest *)executeAttempt:(void (^)(NSInteger statusCode, NSDictionary* json))success failure:(void (^)(NSInteger statusCode, NSError *error, NSDictionary* json))failure {
    OnoLogInfo(@"OnoWebRequest> executeAttempt: %@", self);

    NSURLRequest* urlRequest = self.urlRequest;
    NSString* urlString = [NSObject valueOrNull:urlRequest.URL.absoluteString];
    
    self.timelineTask = [[YCTimelineTask alloc] initWithTaskName:@"web request" withMetadata:@{@"url": urlString, @"method": self.method}];
    [YCTimeline addTaskToRunningTimelines:self.timelineTask];
    
    NSURLSession* session = [OnoWebRequest session];
    
    [[session dataTaskWithRequest:urlRequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        // OnoSDK uses this method, and requires all the data objects it uses to come from the main thread
        // therefore we handle the response on the main thread, and make sure we make a copy of the error,
        // instead of using the one that came from the request.
        dispatch_async(dispatch_get_main_queue(), ^{
            OnoLogInfo(@"OnoWebRequest> finished: %@", self);
            [self.timelineTask stopTask];
            
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            __block NSDictionary* json;
            
            // If we got an error from the request, we should make a copy to send to the failure callback,
            // Otherwise try serialize the JSON, and check for errors there
            NSError *newError;
            if (error != nil) {
                newError = [error copy];
            } else if ([NSData isNotEmpty:data]) {
                json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&newError];
            }
            
            __block NSInteger status = -1;
            if (httpResponse) {
                status = [[NSNumber numberWithLong:httpResponse.statusCode] intValue];
            }
            
            if (status >= 200 && status < 300) {
                success(status, json);
            } else {
                if (!json) {
                    //We have an unknown error of an unknown type, we need
                    //to show the user an unexpected error message
                    
                    if (data) {
                        @try {
                            OnoLogWarn(@"OnoWebRequest> Could not parse JSON response: '%@'", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
                        }
                        @catch (NSException *exception) {
                            
                        }
                    }
                    
                    NSString* message = [newError localizedDescription];
                    
                    if (newError.code == -1011) {
                        message = [NSString stringWithFormat:@"An unexpected error occurred (%ld)", (long)newError.code];
                    }
                    else if (httpResponse && httpResponse.statusCode == 404) {
                        message = @"Not found";
                    }
                    
                    if ([NSString isEmpty:message]) {
                        message = @"An unexpected error occurred";
                    }
                    
                    json = @{
                             @"status" : @(status),
                             @"message" : message
                             };
                }
            
                // Ono does not send status as part of the JSON body
                if ([json valueForKey:@"status"] != NULL) {
                    status = [[json valueForKey:@"status"] intValue];
                }
            
                OnoLogInfo(@"OnoWebRequest> %@ %@ (%@)", [json valueForKey:@"status"], [json valueForKey:@"message"], urlString);
                NSDictionary* metadata = @{
                                           @"url": [NSNull valueOrNull:urlString],
                                           @"status": [NSNull valueOrNull:[json valueForKey:@"status"]],
                                           @"message": [NSNull valueOrNull:[json valueForKey:@"message"]],
                                           @"error": [NSNull valueOrNull:newError.infoString]
                                           };
                [YCTimeline addEventToRunningTimelines:[[YCTimelineEvent alloc] initWithTitle:@"web request failed" eventType:YCTimelineEventTypeError metadata:metadata]];
            
                if (status == 401) {
                    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
                    if ([userDefaults boolForKey:@"yoco-logged-in"]) {
                        [userDefaults setBool:NO forKey:@"yoco-logged-in"];
                        [userDefaults synchronize];
                    }
                    
                    failure(status, [newError replaceLocalizedDescription:[json valueForKey:@"message"]], json);
                } else if (status == 403) {
                    NSString *message = [json valueForKey:@"message"];
                    if ([NSString isEmpty:message]) {
                        message = @"Permission denied, contact supervisor";
                    }
                    
                    failure(status, newError, json);
                }
                else {
                    OnoLogError(@"Error for request to: %@: %@ -> %@", self.urlRequest, error, json);
                    failure(status, newError, json);
                }
            }
        });
    }] resume];
    
    return self;
}

+(void) setup {
    [OnoRetryHandler instance];
}

@end
