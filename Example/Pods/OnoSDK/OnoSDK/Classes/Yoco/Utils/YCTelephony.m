//
//  YCTelephony.m
//  YCSdkTest
//
//  Created by Rohan Jansen on 2015/11/20.
//  Copyright © 2015 Rohan Jansen. All rights reserved.
//

#import "YCTelephony.h"
#import "Reachability.h"

@implementation YCTelephony

+(NSString*) wifiSSID {
    // Does not work on the simulator.
    NSString *ssid = nil;
    NSArray *ifs = (__bridge_transfer id)CNCopySupportedInterfaces();
    for (NSString *ifnam in ifs) {
        NSDictionary *info = (__bridge_transfer id)CNCopyCurrentNetworkInfo((__bridge CFStringRef)ifnam);
        if (info[@"SSID"]) {
            ssid = info[@"SSID"];
        }
    }
    return ssid;
}

+(NSString*) getConnectionType {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reachability currentReachabilityStatus];
    NSString* type = [NSString stringWithFormat:@"Undetermined: %ld", (long)status];
    
    switch (status) {
        case NotReachable:
            type = @"No internet";
            break;
            
        case ReachableViaWiFi:
            type = @"WiFi";
            break;
            
        case ReachableViaWWAN:
            type = @"3G";
            break;
    }
    
    [reachability stopNotifier];
    
    return type;
}

+(NSString*) carrierName {
    CTTelephonyNetworkInfo* info = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier* carrier = [info subscriberCellularProvider];
    NSString* carrierName = [carrier carrierName];
    return carrierName;
}

+(NSString*) connectionProviderName {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    switch (status) {
        case ReachableViaWiFi:
            return [YCTelephony wifiSSID];
        case ReachableViaWWAN:
            return [YCTelephony carrierName];
        case NotReachable:
            return @"Not Connected";
    }
}

+(NSString*) carrierNetworkType {
    NSString* carrierType = [[CTTelephonyNetworkInfo alloc] init].currentRadioAccessTechnology;
    return [carrierType stringByReplacingOccurrencesOfString:@"CTRadioAccessTechnology" withString:@""];
}

-(id)init {
    self = [super init];
    if (self) {
        [self clearCounters];
    }
    return self;
}

+(YCTelephony*) instance {
    static YCTelephony *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[YCTelephony alloc] init];
    });
    return instance;
}

-(void) clearCounters {
    self.taskDataCounter = [[NSMutableDictionary alloc] init];
    self.previousDataCounters = [[NSMutableDictionary alloc] init];
    self.totalDeltaCounters = [[NSMutableDictionary alloc] init];
}

-(NSArray*) dataCounters
{
    BOOL   success;
    struct ifaddrs *addrs;
    const struct ifaddrs *cursor;
    const struct if_data *networkStatisc;
    
    int WiFiSent = 0;
    int WiFiReceived = 0;
    int WWANSent = 0;
    int WWANReceived = 0;
    
    NSString* name = nil;
    
    success = getifaddrs(&addrs) == 0;
    if (success)
    {
        cursor = addrs;
        while (cursor != NULL)
        {
            name=[NSString stringWithFormat:@"%s",cursor->ifa_name];
            //            NSLog(@"ifa_name %s == %@\n", cursor->ifa_name,name);
            // names of interfaces: en0 is WiFi ,pdp_ip0 is WWAN
            
            if (cursor->ifa_addr->sa_family == AF_LINK)
            {
                if ([name hasPrefix:@"en"])
                {
                    networkStatisc = (const struct if_data *) cursor->ifa_data;
                    WiFiSent+=networkStatisc->ifi_obytes;
                    WiFiReceived+=networkStatisc->ifi_ibytes;
                    //                    NSLog(@"WiFiSent %d ==%d",WiFiSent,networkStatisc->ifi_obytes);
                    //                    NSLog(@"WiFiReceived %d ==%d",WiFiReceived,networkStatisc->ifi_ibytes);
                }
                
                if ([name hasPrefix:@"pdp_ip"])
                {
                    networkStatisc = (const struct if_data *) cursor->ifa_data;
                    WWANSent+=networkStatisc->ifi_obytes;
                    WWANReceived+=networkStatisc->ifi_ibytes;
                    //                    NSLog(@"WWANSent %d ==%d",WWANSent,networkStatisc->ifi_obytes);
                    //                    NSLog(@"WWANReceived %d ==%d",WWANReceived,networkStatisc->ifi_ibytes);
                }
            }
            
            cursor = cursor->ifa_next;
        }
        
        freeifaddrs(addrs);
    }
    
    NSArray* currentDataCounters = [NSArray arrayWithObjects:[NSNumber numberWithInt:WiFiSent], [NSNumber numberWithInt:WiFiReceived],[NSNumber numberWithInt:WWANSent],[NSNumber numberWithInt:WWANReceived], nil];
    
    return currentDataCounters;
}

-(void)startDataCounter:(NSString*)task {
    NSArray* currentDataCounters = [self dataCounters];
    [self.previousDataCounters setValue:[currentDataCounters copy] forKey:task];
}

-(NSArray*)stopDataCounter:(NSString*)task {
    NSArray* currentDataCounters = [self dataCounters];
    NSArray* previousDataCounters = [self.previousDataCounters objectForKey:task];
    
    NSArray* delta = [NSArray arrayWithObjects:@([[currentDataCounters objectAtIndex:0] intValue]-[[previousDataCounters objectAtIndex:0] intValue]),
                      @([[currentDataCounters objectAtIndex:1] intValue]-[[previousDataCounters objectAtIndex:1] intValue]),
                      @([[currentDataCounters objectAtIndex:2] intValue]-[[previousDataCounters objectAtIndex:2] intValue]),
                      @([[currentDataCounters objectAtIndex:3] intValue]-[[previousDataCounters objectAtIndex:3] intValue]),
                      nil];
    
    [self.previousDataCounters setValue:[currentDataCounters copy] forKey:task];
    
    if (![self.totalDeltaCounters objectForKey:task]) {
        [self.totalDeltaCounters setValue:[NSMutableArray arrayWithArray:delta] forKey:task];
    } else {
        NSMutableArray* totalDelta = [self.totalDeltaCounters objectForKey:task];
        
        for (int i=0; i<4; i++) {
            [totalDelta setObject:@([[totalDelta objectAtIndex:i] intValue] + [[delta objectAtIndex:i] intValue]) atIndexedSubscript:i];
        }
        
        [self.totalDeltaCounters setValue:totalDelta forKey:task];
    }
    
    return delta;
}

-(int)calculateTotalData:(NSString*)task {
    if (task) {
        NSArray* delta = [self.totalDeltaCounters objectForKey:task];
        
        if (delta) {
            return [[delta objectAtIndex:0] intValue] +
            [[delta objectAtIndex:1] intValue] +
            [[delta objectAtIndex:2] intValue] +
            [[delta objectAtIndex:3] intValue];
        }
        
        return -1;
    } else {
        int totalData = 0;
        
        for (NSString* key in self.totalDeltaCounters) {
            totalData += [self calculateTotalData:key];
        }
        
        return totalData;
    }
}

-(int)calculateTotalDataSent:(NSString*)task {
    if (task) {
        NSArray* delta = [self.totalDeltaCounters objectForKey:task];
        
        if (delta) {
            return [[delta objectAtIndex:0] intValue] +
            [[delta objectAtIndex:2] intValue];
        }
        
        return -1;
    } else {
        int totalDataSent = 0;
        
        for (NSString* key in self.totalDeltaCounters) {
            totalDataSent += [self calculateTotalDataSent:key];
        }
        
        return totalDataSent;
    }
}

-(int)calculateTotalDataReceived:(NSString*)task {
    if (task) {
        NSArray* delta = [self.totalDeltaCounters objectForKey:task];
        
        if (delta) {
            return [[delta objectAtIndex:1] intValue] +
            [[delta objectAtIndex:3] intValue];
        }
        
        return -1;
    } else {
        int totalDataReceived = 0;
        
        for (NSString* key in self.totalDeltaCounters) {
            totalDataReceived += [self calculateTotalDataReceived:key];
        }
        
        return totalDataReceived;
    }
}

@end
