//
//  YCDDLogger.h
//  Yoco
//
//  Created by Andrew Snowden on 2015/07/20.
//  Copyright (c) 2015 Yoco Technologies (PTY) LTD. All rights reserved.
//

@import CocoaLumberjack;

@interface YCDDLogger : DDAbstractLogger <DDLogger>

@end
