//
//  YCDate.m
//  Yoco
//
//  Created by Kurt Kruger on 2014/03/07.
//  Copyright (c) 2014 Yoco Technologies (PTY) LTD. All rights reserved.
//

#import "NSDate+String.h"

@implementation NSDate (Extensions)

-(NSString*)toString {
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"SAST"]];
    return [formatter stringFromDate:self];
}

-(NSString*)toUTCString {
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    return [formatter stringFromDate:self];
}

+(NSDate*)stringToDate:(NSString*)date {
    if (!date) return nil;
    
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    
    return [formatter dateFromString: date];
}

-(NSString*)toStringWithFormat:(NSString*)format {
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:format];
    return [formatter stringFromDate:self];
}

-(BOOL)isToday{
  NSDateComponents *otherDay = [[NSCalendar currentCalendar] components:NSCalendarUnitEra|NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:self];
  NSDateComponents *today = [[NSCalendar currentCalendar] components:NSCalendarUnitEra|NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:[NSDate date]];
    if([today day] == [otherDay day] &&
       [today month] == [otherDay month] &&
       [today year] == [otherDay year] &&
       [today era] == [otherDay era]) {
        return YES;
    }
    return NO;
}


+(NSDate*)relativeEarliest {
    return [NSDate stringToDate:@"1980-01-01T00:00:00.000Z"];
}

-(NSString*)pretty{
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"EEEE dd MMMM yyyy"];
    return [formatter stringFromDate:self];
}

-(NSString*)prettyTime{
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm:ss"];
    return [formatter stringFromDate: self];
}

-(NSString*)prettyDateTime{
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"d MMMM yyyy, hh:mm a"];
    return [formatter stringFromDate:self];
}

-(NSInteger)daysBetween:(NSDate*)date {
  NSUInteger unitFlags = NSCalendarUnitDay;
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:unitFlags fromDate:self toDate:date options:0];
  NSInteger days = labs([components day]);
    return days;
}

@end
