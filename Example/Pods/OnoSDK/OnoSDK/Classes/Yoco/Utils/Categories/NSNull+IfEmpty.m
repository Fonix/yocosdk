//
//  NSNull+IfEmpty.m
//  YocoSDK
//
//  Created by Andrew Snowden on 2016/03/03.
//  Copyright © 2016 Yoco. All rights reserved.
//

#import "NSNull+IfEmpty.h"

@implementation NSNull (IfEmpty)

+(id) valueOrNull:(id)val {
    if (val) {
        return val;
    } else {
        return [NSNull null];
    }
}
@end
