//
//  NSDecimalNumber+Conversions.m
//  YOCO
//
//  Created by Andrew Snowden on 2013/10/30.
//  Copyright (c) 2013 JellyWhipStudios. All rights reserved.
//

#import "NSDecimalNumber+Conversions.h"

@implementation NSDecimalNumber (Conversions)

+(NSDecimalNumber*) decimalNumberWithInteger:(NSInteger)integer {
    return [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%d", (int)integer]];
}

-(NSDecimalNumber*) decimalNumberByMultiplyingByInteger:(NSInteger)integer {
    return [self decimalNumberByMultiplyingBy:[NSDecimalNumber decimalNumberWithInteger:integer]];
}

@end
