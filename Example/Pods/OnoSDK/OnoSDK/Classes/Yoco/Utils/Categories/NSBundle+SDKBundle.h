//
//  NSBundle+SDKBundle.h
//  YocoSDK
//
//  Created by Andrew Snowden on 2016/02/17.
//  Copyright © 2016 Yoco. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSBundle (SDKBundle)

+ (NSBundle *)yocoSDKBundle;

@end
