//
//  NSObject+Utils.m
//  YocoSDK
//
//  Created by Nkokhelo Mhlongo on 7/20/18.
//  Copyright © 2018 Yoco. All rights reserved.
//

#import "NSObject+Utils.h"

@implementation NSObject (Utils)

+(BOOL) isNull:(id)object {
    return object == nil || object == [NSNull null];
}

+(BOOL) isNotNull:(id)object {
    return ![NSObject isNull:object];
}

+(id) valueOrNull:(id)val {
    if (val) {
        return val;
    } else {
        return [NSNull null];
    }
}

@end
