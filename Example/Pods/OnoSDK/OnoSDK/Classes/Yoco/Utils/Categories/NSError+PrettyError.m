//
//  NSError+PrettyError.m
//  Yoco
//
//  Created by Lungisa Matshoba on 2014/08/12.
//  Copyright (c) 2014 Yoco Technologies (PTY) LTD. All rights reserved.
//

#import "NSError+PrettyError.h"
#import "NSError+YCErrorCodes.h"
#import "NSObject+Utils.h"

@implementation NSError (PrettyError)

-(NSError*) withLocalizedDescription:(NSString*)localizedDescription {
    NSMutableDictionary* userInfo = [NSMutableDictionary dictionaryWithDictionary:self.userInfo];
    if (self.localizedDescription != localizedDescription) {
        [userInfo setObject:self.localizedDescription forKey:@"originalErrorMessage"];
    }
    [userInfo setObject:localizedDescription forKey:NSLocalizedDescriptionKey];
    return [NSError errorWithDomain:self.domain code:self.code userInfo:userInfo];
    
}

-(NSError *)withLocalizedDescription:(NSString *)localizedDescription andCode:(NSInteger)code {
    return [self withLocalizedDescription:[NSString stringWithFormat:@"%@ (%@)", localizedDescription, @(code)]];
}

-(NSError*)prettyError {
    return self;
}

-(NSString*)errorInfo {
    NSString* info = [self.userInfo valueForKey:@"MPErrorInfo"];
    
    if (info)
        return [NSString stringWithFormat:@"%@%@", [[info substringToIndex:1] capitalizedString], [info substringFromIndex: 1]];
    
    return nil;
}

-(NSString*)prettyLocalizedDescriptionAndInfo {
    NSString* info = [self errorInfo];
    
    if (info) {
        return [NSString stringWithFormat:@"%@ (%@)", [[self prettyError] localizedDescription], info];
    } else {
        return [[self prettyError] localizedDescription];
    }
}


-(NSString*)prettyLocalizedDescription{
    return [[self prettyError] localizedDescription];
}

-(BOOL) isPayworksError {
    return ([self.domain isEqualToString:@"mpos.MPErrorDomain"]);
}

-(NSString*) errorCodeString {
    if ([self.domain isEqualToString:YOCO_ERROR_DOMAIN]) {
        return [NSError yocoErrorCode:(YCErrorReason)self.code];
    } else {
        return [NSString stringWithFormat:@"%@ %ld", self.domain, (long)self.code];
    }
}

//-(NSError *)payworksPrettyError {
//    if ([self.domain isEqualToString:@"mpos.MPErrorDomain"]) {
//        MPErrorType errorCode = self.code;
//        switch ((NSInteger)errorCode) {
//            case MPErrorTypeUnknown:
//                return [self withLocalizedDescription:@"Something went wrong. Please contact us at support@yoco.co.za."];
//                break;
//            case MPErrorTypeTransactionError:
//                return [self withLocalizedDescription:@"We could not process this transaction. Please contact us at support@yoco.co.za"];
//                break;
//            case MPErrorTypeSDKConfigurationMissing:
//            case MPErrorTypeParameterInvalid:
//            case MPErrorTypeParameterMissing:
//                return [self withLocalizedDescription:@"The transaction has some missing information. Please contact support@yoco.co.za for assistance."];
//                break;
//            case MPErrorTypeInternalInconsistency:
//            case MPErrorTypeSDKResourcesModified:
//            case MPErrorTypeSDKResourcesNotFound:
//                return [self withLocalizedDescription:@"The software is not working correctly. Please contact us at support@yoco.co.za for assistance."];
//                break;
//            case MPErrorTypeServerAuthenticationFailed:
//                return [self withLocalizedDescription:@"The server returned an error (Unauthorized). Please contact support@yoco.co.za for assistance."];
//            case MPErrorTypeServerError:
//            case MPErrorTypeServerUnavailable:
//            case MPErrorTypeServerPinningWithRemoteFailed:
//                return [self withLocalizedDescription:@"The server returned an error. Please try to process the transaction again."];
//                break;
//            case MPErrorTypeTransactionAborted:
//                return [self withLocalizedDescription:@"The transaction was cancelled."];
//                break;
//            case MPErrorTypeTransactionDeclined:
//                return [self withLocalizedDescription:@"The transaction was declined."];
//                break;
//            case MPErrorTypeTransactionNoLongerAbortable:
//                return [self withLocalizedDescription:@"The transaction is is processing and cannot be cancelled."];
//                break;
//            case MPErrorTypeTransactionBusy:
//                return [self withLocalizedDescription:@"Your previous transaction is still processing. Please retry this transaction in 1 minute."];
//                break;
//            case MPErrorTypeTransactionInvalid:
//                return [self withLocalizedDescription:@"This is not a supported transaction. Please consult www.yoco.co.za/support for supported transaction types."];
//                break;
//            case MPErrorTypeTransactionReferenceNotFound:
//                return [self withLocalizedDescription:@"The transaction reference could not be found. Please contact us at support@yoco.co.za"];
//                break;
//            case MPErrorTypeAccessoryError:
//                return [self withLocalizedDescription:@"An error occurred while communicating with your reader. Please make sure your reader is turned on and try to process the transaction again."];
//                break;
//            case MPErrorTypeAccessoryTampered:
//                return [self withLocalizedDescription:@"Your reader has been tampered with and cannot be used. Please contact support@yoco.co.za immediately."];
//                break;
//            case MPErrorTypeAccessoryBatteryLow:
//                return [self withLocalizedDescription:@"Your reader’s battery is too low to process transactions. Please retry once the reader is charged."];
//                break;
//            case MPErrorTypeAccessoryRequiresUpdate:
//                return [self withLocalizedDescription:@"Your reader requires an update before you can transact. We will now attempt to update it."];
//                break;
//            case MPErrorTypeAccessoryDeactivated:
//                return [self withLocalizedDescription:@"The reader has been deactivated. Please contact us at support@yoco.co.za"];
//                break;
//            case MPErrorTypeAccessoryNotWhitelisted:
//                return [self withLocalizedDescription:@"It seems this card reader is not paired with your merchant account. Please contact us support@yoco.co.za"];
//                break;
//            case MPErrorTypeAccessoryBusy:
//                return [self withLocalizedDescription:@"Your reader is currently busy. Please try again or try reconnecting your reader."];
//                break;
//            case MPErrorTypeAccessoryAlreadyDisconnected:
//                return [self withLocalizedDescription:@"Your reader is no longer connected."];
//                break;
//            case MPErrorTypeAccessoryAlreadyConnected:
//                return [self withLocalizedDescription:@"Your reader is already connected."];
//                break;
//            case MPErrorTypeAccessoryNotConnected:
//                return [self withLocalizedDescription:@"Your reader is not currently connected or could not be detected. Please make sure your reader is turned on and is in range."];
//                break;
//            case MPErrorTypeAccessoryNotFound:
//                return [self withLocalizedDescription:@"We could not detect your reader. Please try again."];
//                break;
//        }
//    }
//
//    return self;
//}
//
//+(NSString*) mposErrorCode:(MPErrorType)code {
//    switch (code) {
//        case MPErrorTypeUnknown:
//            return @"MPErrorTypeUnknown";
//        case MPErrorTypeTransactionError:
//            return @"MPErrorTypeTransactionError";
//        case MPErrorTypeSDKConfigurationMissing:
//            return @"MPErrorTypeConfigurationMissing";
//        case MPErrorTypeParameterInvalid:
//            return @"MPErrorTypeParameterInvalid";
//        case MPErrorTypeParameterMissing:
//            return @"MPErrorTypeParameterMissing";
//        case MPErrorTypeInternalInconsistency:
//            return @"MPErrorTypeInternalInconistency";
//        case MPErrorTypeSDKResourcesModified:
//            return @"MPErrorTypeSDKResourcesModified";
//        case MPErrorTypeSDKResourcesNotFound:
//            return @"MPErrorTypeSDKResourcesNotFound";
//        case MPErrorTypeServerError:
//            return @"MPErrorTypeServerError";
//        case MPErrorTypeServerUnavailable:
//            return @"MPErrorTypeServerUnavailable";
//        case MPErrorTypeServerPinningWithRemoteFailed:
//            return @"MPErrorTypeServerPinningWithRemoteFailed";
//        case MPErrorTypeServerAuthenticationFailed:
//            return @"MPErrorTypeServerAuthenticationFailed";
//        case MPErrorTypeTransactionAborted:
//            return @"MPErrorTypeTransactionAborted";
//        case MPErrorTypeTransactionDeclined:
//            return @"MPErrorTypeTransactionDeclined";
//        case MPErrorTypeTransactionNoLongerAbortable:
//            return @"MPErrorTypeTransactionNoLongerAbortable";
//        case MPErrorTypeTransactionBusy:
//            return @"MPErrorTypeTransactionBusy";
//        case MPErrorTypeTransactionInvalid:
//            return @"MPErrorTypeTransactionInvalid";
//        case MPErrorTypeTransactionReferenceNotFound:
//            return @"MPErrorTypeTransactionReferenceNotFound";
//        case MPErrorTypeAccessoryError:
//            return @"MPErrorTypeAccessoryError";
//        case MPErrorTypeAccessoryTampered:
//            return @"MPErrorTypeAccessoryTampered";
//        case MPErrorTypeAccessoryBatteryLow:
//            return @"MPErrorTypeAccessoryBatteryLow";
//        case MPErrorTypeAccessoryRequiresUpdate:
//            return @"MPErrorTypeAccessoryRequiresUpdate";
//        case MPErrorTypeAccessoryDeactivated:
//            return @"MPErrorTypeAccessoryDeactivated";
//        case MPErrorTypeAccessoryNotWhitelisted:
//            return @"MPErrorTypeAccessoryNotWhitelisted";
//        case MPErrorTypeAccessoryBusy:
//            return @"MPErrorTypeAccessoryBusy";
//        case MPErrorTypeAccessoryAlreadyDisconnected:
//            return @"MPErrorTypeAccessoryAlreadyDisconnected";
//        case MPErrorTypeAccessoryAlreadyConnected:
//            return @"MPErrorTypeAccessoryAlreadyConnected";
//        case MPErrorTypeAccessoryNotConnected:
//            return @"MPErrorTypeAccessoryNotConnected";
//        case MPErrorTypeAccessoryNotFound:
//            return @"MPErrorTypeAccessoryNotFound";
//        case MPErrorTypeServerTimeout:
//            return @"MPErrorTypeServerTimeout";
//        case MPErrorTypeSDKFeatureNotEnabled:
//            return @"MPErrorTypeSDKFeatureNotEnabled";
//        case MPErrorTypeServerInvalidResponse:
//            return @"MPErrorTypeServerInvalidResponse";
//        case MPErrorTypeServerUnknownUsername:
//            return @"MPErrorTypeServerUnknownUsername";
//        case MPErrorTypeTransactionActionError:
//            return @"MPErrorTypeTransactionActionError";
//        case MPErrorTypeAccessoryComponentNotFound:
//            return @"MPErrorTypeAccessoryComponentNotFound";
//        case MPErrorTypeTransactionSessionNotFound:
//            return @"MPErrorTypeTransactionSessionNotFound";
//        case MPErrorTypeAccessoryComponentPrinterBusy:
//            return @"MPErrorTypeAccessoryComponentPrinterBusy";
//        case MPErrorTypeAccessoryComponentPrinterCoverOpen:
//            return @"MPErrorTypeAccessoryComponentPrinterCoverOpen";
//        case MPErrorTypeAccessoryComponentPrinterPaperLowOrOut:
//            return @"MPErrorTypeAccessoryComponentPrinterPaperLowOrOut";
//        case MPErrorTypeOfflineBatchMalformedPendingManualReview:
//            return @"MPErrorTypeOfflineBatchMalformedPendingManualReview";
//        default:
//            return @"Unknown";
//    }
//
//    return [NSString stringWithFormat:@"%ld", (long)code];
//}

-(NSString*) originalErrorMessage {
    NSString* original = [self.userInfo valueForKey:@"originalErrorMessage"];
    if ([NSObject isNotNull:original]) {
        return original;
    }
    return self.localizedDescription;
}

-(NSString*) infoString {
//    NSString* errorCode = [NSError mposErrorCode:self.code];
//
    NSString* extra = @"";
//    if ([NSObject isNotNull:self.developerInfo]) {
//        extra = self.developerInfo;
//    }
//    else if ([NSObject isNotNull:self.info]) {
//        extra = self.info;
//    }
    
    return [NSString stringWithFormat:@"Error: %@ [reason: %@] [domain: %@] [code: %ld] [extra: %@]", [self originalErrorMessage], self.localizedFailureReason, self.domain, self.code, extra];
}

@end
