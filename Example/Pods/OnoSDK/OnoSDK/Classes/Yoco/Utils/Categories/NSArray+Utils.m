//
//  NSArray+Utils.m
//  YocoSDK
//
//  Created by Andrew Snowden on 2016/02/15.
//  Copyright © 2016 Yoco. All rights reserved.
//

#import "NSArray+Utils.h"
#import "NSObject+Utils.h"

@implementation NSArray (Utils)

+(NSUserDefaults*) getDefaults {
  return [[NSUserDefaults alloc] initWithSuiteName:@"OnoSDK"];
}

-(void) saveToDefaultsWithKey:(NSString*)key {
    NSUserDefaults *defaults = [NSArray getDefaults];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self];
    [defaults setObject:data forKey:key];
    [defaults synchronize];
}

+(NSArray*) arrayFromDefaultsWithKey:(NSString*)key {
    NSUserDefaults *defaults = [NSArray getDefaults];
    NSData *data = [defaults objectForKey:key];
    NSArray *array = nil;
    if (data) {
        array = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    }
    return array;
}

- (NSArray *)map:(id (^)(id obj, NSUInteger idx))block {
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:[self count]];

    [self enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [result addObject:block(obj, idx)];
    }];
    return result;
}

- (NSArray *)filter:(BOOL (^)(id obj, NSUInteger idx))block {
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:[self count]];
    
    [self enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if (block(obj, idx)) {
            [result addObject:obj];
        }
    }];
    return result;
}

+(BOOL)isEmpty:(id)array {
    return [NSObject isNull:array] || [array count] == 0;
}

+(BOOL)isNotEmpty:(id)array {
    return ![NSArray isEmpty:array];
}

+(NSArray*) emptyIfNull:(NSArray*)array {
    if (array) {
        return array;
    } else {
        return @[];
    }
}

@end
