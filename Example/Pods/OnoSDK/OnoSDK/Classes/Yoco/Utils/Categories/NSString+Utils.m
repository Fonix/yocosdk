//
//  NSString+Utils.m
//  YCSdkTest
//
//  Created by Andrew Snowden on 2016/01/18.
//  Copyright © 2016 Rohan Jansen. All rights reserved.
//

#import "NSString+Utils.h"
#import "NSObject+Utils.h"

@implementation NSString (Utils)

+(NSString*) stringWithInt:(NSInteger)integer {
    return [NSString stringWithFormat:@"%ld", (long)integer];
}

+(NSString*)toBase64:(NSString*)text {
    NSData *data = [text dataUsingEncoding:NSUTF8StringEncoding];
    return [data base64EncodedStringWithOptions:0];
}

+(NSString*)fromBase64:(NSString*)text {
    NSData *data = [[NSData alloc] initWithBase64EncodedString:text options:0];
    return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
}


+(NSString*)emptyIfNil:(NSString*)text {
    if (![NSString isEmpty:text]) {
        return text;
    }
    else {
        return @"";
    }
}

+(NSString*)dictSafeString:(NSString*)text {
    if (![NSString isEmpty:text]) {
        return text;
    } else {
      return nil;
    }
}

+(BOOL)isEmpty:(NSString*)text {
    return [NSObject isNull:text] || text.length == 0;
}

+(BOOL)isNotEmpty:(NSString *)text {
    return ![NSString isEmpty:text];
}


+ (NSString *)stringWithUUID {
    CFUUIDRef uuid = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, uuid);
    CFRelease(uuid);
    return (__bridge_transfer NSString *)string;
}

- (NSString *)URLEncodedString {
    static CFStringRef toEscape = CFSTR(":/=,!$&'()*+;[]@#?%");
    return (__bridge_transfer NSString *)CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                                                 (__bridge CFStringRef)self,
                                                                                 NULL,
                                                                                 toEscape,
                                                                                 kCFStringEncodingUTF8);
}

- (NSString *)stringByTrimmingLeadingAndTrailingCharactersInSet:(NSCharacterSet *)characterSet {
    return [[self stringByTrimmingLeadingCharactersInSet:characterSet]
            stringByTrimmingTrailingCharactersInSet:characterSet];
}


- (NSString *)stringByTrimmingLeadingAndTrailingWhitespaceAndNewlineCharacters {
    return [[self stringByTrimmingLeadingWhitespaceAndNewlineCharacters]
            stringByTrimmingTrailingWhitespaceAndNewlineCharacters];
}


- (NSString *)stringByTrimmingLeadingCharactersInSet:(NSCharacterSet *)characterSet {
    NSRange rangeOfFirstWantedCharacter = [self rangeOfCharacterFromSet:[characterSet invertedSet]];
    if (rangeOfFirstWantedCharacter.location == NSNotFound) {
        return @"";
    }
    return [self substringFromIndex:rangeOfFirstWantedCharacter.location];
}


- (NSString *)stringByTrimmingLeadingWhitespaceAndNewlineCharacters {
    return [self stringByTrimmingLeadingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}


- (NSString *)stringByTrimmingTrailingCharactersInSet:(NSCharacterSet *)characterSet {
    NSRange rangeOfLastWantedCharacter = [self rangeOfCharacterFromSet:[characterSet invertedSet]
                                                               options:NSBackwardsSearch];
    if (rangeOfLastWantedCharacter.location == NSNotFound) {
        return @"";
    }
    return [self substringToIndex:rangeOfLastWantedCharacter.location + 1]; // Non-inclusive
}


- (NSString *)stringByTrimmingTrailingWhitespaceAndNewlineCharacters {
    return [self stringByTrimmingTrailingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

-(NSString*)stringByTrimmingLines {
    NSMutableArray* newLines = [[NSMutableArray alloc] init];
    NSArray* lines = [self componentsSeparatedByString:@"\n"];
    for (NSString* line in lines) {
        [newLines addObject:[line stringByTrimmingLeadingAndTrailingWhitespaceAndNewlineCharacters]];
    }
    return [[newLines componentsJoinedByString:@"\n"] stringByTrimmingLeadingAndTrailingWhitespaceAndNewlineCharacters];
}

+(NSString*)stringFromDefaultsWithKey:(NSString*)key {
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    return [userDefaults valueForKey:key];
}

-(void) saveToDefaultsWithKey:(NSString*)key {
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:self forKey:key];
    [userDefaults synchronize];
}

-(BOOL) isNotEqualToString:(NSString*) thatString {
    return ![self isEqualToString: thatString];
}

@end
