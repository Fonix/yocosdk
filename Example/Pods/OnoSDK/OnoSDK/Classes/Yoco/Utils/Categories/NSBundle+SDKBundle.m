//
//  NSBundle+SDKBundle.m
//  YocoSDK
//
//  Created by Andrew Snowden on 2016/02/17.
//  Copyright © 2016 Yoco. All rights reserved.
//

#import "NSBundle+SDKBundle.h"

@implementation NSBundle (SDKBundle)

+ (NSBundle *)yocoSDKBundle {
    static NSBundle* frameworkBundle = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        NSString* mainBundlePath = [[NSBundle mainBundle] resourcePath];
        NSString* frameworkBundlePath = [mainBundlePath stringByAppendingPathComponent:@"YocoSDK.bundle"];
        frameworkBundle = [NSBundle bundleWithPath:frameworkBundlePath];
    });
    return frameworkBundle;
}

@end
