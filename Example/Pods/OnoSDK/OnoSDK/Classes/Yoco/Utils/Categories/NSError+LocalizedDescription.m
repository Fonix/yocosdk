//
//  NSError+LocalizedDescription.m
//  Yoco
//
//  Created by Kurt Kruger on 2014/11/06.
//  Copyright (c) 2014 Yoco Technologies (PTY) LTD. All rights reserved.
//

#import "NSError+LocalizedDescription.h"

@implementation NSError (LocalizedDescription)

-(NSError*)replaceLocalizedDescription:(NSString*)message {
    NSMutableDictionary *details = [[NSMutableDictionary alloc] initWithDictionary:self.userInfo];
    [details setObject:message forKey:NSLocalizedDescriptionKey];
    return [NSError errorWithDomain:self.domain code:self.code userInfo:details];
}

@end
