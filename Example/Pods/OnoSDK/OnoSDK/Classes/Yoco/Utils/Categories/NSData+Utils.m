//
//  NSData+Utils.m
//  YocoSDK
//
//  Created by Nkokhelo Mhlongo on 7/24/18.
//  Copyright © 2018 Yoco. All rights reserved.
//

#import "NSData+Utils.h"
#import "NSObject+Utils.h"

@implementation NSData (Utils)

+(BOOL)isEmpty:(NSData*)data {
  return [NSObject isNull:data] || data.length == 0;
}

+(BOOL)isNotEmpty:(NSData*)data {
  return ![NSData isEmpty:data];
}

@end
