//
//  NSDate+TimeZones.m
//  Yoco
//
//  Created by Andrew Snowden on 2014/06/02.
//  Copyright (c) 2014 Yoco Technologies (PTY) LTD. All rights reserved.
//

#import "NSDate+TimeZones.h"

@implementation NSDate (TimeZones)

-(NSDate *) toLocalTime
{
    NSTimeZone *tz = [NSTimeZone localTimeZone];
    NSInteger seconds = [tz secondsFromGMTForDate: self];
    return [NSDate dateWithTimeInterval: seconds sinceDate: self];
}

-(NSDate *) toUTCTime
{
    NSTimeZone *tz = [NSTimeZone localTimeZone];
    NSInteger seconds = -[tz secondsFromGMTForDate: self];
    return [NSDate dateWithTimeInterval: seconds sinceDate: self];
}

@end
