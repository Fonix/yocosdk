//
//  NSError+YCErrorCodes.m
//  YCSdkTest
//
//  Created by Rohan Jansen on 2015/11/26.
//  Copyright © 2015 Rohan Jansen. All rights reserved.
//

#import "NSError+YCErrorCodes.h"
//#import "NSError+PrettyError.h"

@implementation NSError (YCErrorCodes)

+(NSString*) errorFromReason:(YCErrorReason)reason {
    switch (reason) {
        case YCErrorReaderBatteryTooLow:
            return @"Your battery is too low to process this payment. Please charge your reader and try again.";
        case YCErrorReaderNotAvailable:
            return @"Waiting for reader to connect"; // @"Your reader is not connected";
        case YCErrorReaderNotEnabled:
            return @"You do not have this type of reader added to your business, please contact support@yoco.co.za";
        case YCErrorMicrophonePermissionDenied:
            return @"Yoco requires permission to use your microphone in order to be able to connect to your reader.  Please enable this in 'Settings -> Privacy -> Microphone'.";
        case YCErrorTransactionCancelled:
            return @"Payment cancelled";
        case YCErrorTransactionDeclined:
            return @"Payment declined";
        case YCErrorNotAllowedCardPayments:
            return @"You have not been approved to accept card payments";
        case YCErrorTutorialsBlocking:
            return @"Please complete the tutorials before attempting to connect your card reader";
        case YCErrorReaderBusy:
            return @"Your reader has experienced an error we cannot recover from. The application must restart";
        case YCErrorReaderBusyUpdatingDisconnectFailed:
            return @"The reader is busy updating and cannot be disconnected at this moment";
        case YCErrorReaderBusyUpdating:
            return @"The reader is busy updating";
        case YCErrorNoReaders:
            return @"No readers have been loaded yet. Please contact support@yoco.co.za";
        case YCErrorAmountTooSmall:
            return @"Payment amount must be more than 1.00";
        case YCErrorSwipeDisabled:
            return @"Payment Declined. You’ve reached your limit for swipe and sign transactions. For assistance please contact support@yoco.co.za";
        case YCErrorNoResponse:
            return @"No response from server";
        case YCErrorTest:
            return @"Test error";

    }
    
    return @"An unexpected error has occurred, please restart the application";
}


+(NSString*) yocoErrorCode:(YCErrorReason)code {
    switch (code - YOCO_ERROR_CODE_OFFSET) {
        case YCErrorReaderNotAvailable:
            return @"YCErrorReaderNotAvailable";
        case YCErrorReaderNotEnabled:
            return @"YCErrorReaderNotEnabled";
        case YCErrorTransactionDeclined:
            return @"YCErrorTransactionDeclined";
        case YCErrorTransactionCancelled:
            return @"YCErrorTransactionCancelled";
        case YCErrorNotAllowedCardPayments:
            return @"YCErrorNotAllowedCardPayments";
        case YCErrorTutorialsBlocking:
            return @"YCErrorTutorialsBlocking";
        case YCErrorReaderBusy:
            return @"YCErrorReaderBusy";
        case YCErrorReaderBusyUpdatingDisconnectFailed:
            return @"YCErrorReaderBusyUpdatingDisconnectFailed";
        case YCErrorReaderBusyUpdating:
            return @"YCErrorReaderBusyUpdating";
        case YCErrorMicrophonePermissionDenied:
            return @"YCErrorMicrophonePermissionDenied";
        case YCErrorNoReaders:
            return @"YCErrorNoReaders";
        case YCErrorReaderBatteryTooLow:
            return @"YCErrorReaderBatteryTooLow";
        case YCErrorAmountTooSmall:
            return @"YCErrorAmountTooSmall";
        case YCErrorSwipeDisabled:
            return @"YCErrorSwipeDisabled";
        case YCErrorNoResponse:
            return @"YCErrorNoResponse";
        case YCErrorTest:
            return @"YCErrorTest";
        default:
            return [NSString stringWithFormat:@"%@ %ld", YOCO_ERROR_DOMAIN, (long)code];
    }
}

+(NSError *)errorWithReason:(YCErrorReason)reason {
    NSString* message = [NSError errorFromReason:reason];
    return [NSError errorWithDomain:YOCO_ERROR_DOMAIN code:(YOCO_ERROR_CODE_OFFSET + reason) userInfo:@{NSLocalizedDescriptionKey: message}];
}

+(NSError *)errorWithDescription:(NSString*)description andReason:(YCErrorReason)reason {
    return [NSError errorWithDomain:YOCO_ERROR_DOMAIN code:(YOCO_ERROR_CODE_OFFSET + reason) userInfo:@{NSLocalizedDescriptionKey: description}];
}

-(BOOL)isErrorWithReason:(YCErrorReason)reason {
    return ([self.domain isEqualToString:YOCO_ERROR_DOMAIN] && self.code == YOCO_ERROR_CODE_OFFSET + reason);
}

-(NSString*)internalDescription {
    if ([self.domain isEqualToString:YOCO_ERROR_DOMAIN]) {
        switch (self.code - YOCO_ERROR_CODE_OFFSET) {
            case YCErrorReaderBusy:
                return @"Reader busy";
                break;
            default:
                break;
        }
    }
    /*else if (self.isPayworksError && self.userInfo) {
        NSString* errorInfo = [self.userInfo valueForKey:MPErrorInfoKey];
        NSString* developerInfo = [self.userInfo valueForKey:MPErrorDeveloperInfoKey];

        // Try and get a better error when we get a server error
        if (self.code == MPErrorTypeServerError) {
            if (developerInfo.length < 50) {
                if ([developerInfo hasPrefix:@"response was: "]) {
                    return [developerInfo stringByReplacingOccurrencesOfString:@"response was: " withString:@""];
                }
            }
        }

        if (errorInfo) {
            // Payworks puts a . at the end of their errors which looks weird with our error : phase approach
            return [NSString stringWithFormat:@"%@: %@", self.localizedDescription, [errorInfo stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"."]]];
        }
    }*/
    
    return self.localizedDescription;
}

@end
