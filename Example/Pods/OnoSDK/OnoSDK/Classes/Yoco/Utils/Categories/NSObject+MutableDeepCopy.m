@implementation NSObject (MutableDeepCopy)

- (id)mutableDeepCopy
{
    return [self copy];
}

@end

#pragma mark - NSDictionary

@implementation NSDictionary (MutableDeepCopy)

- (id)mutableDeepCopy
{
    return [NSMutableDictionary dictionaryWithObjects:self.allValues.mutableDeepCopy
                                              forKeys:self.allKeys.mutableDeepCopy];
}

@end

#pragma mark - NSArray

@implementation NSArray (MutableDeepCopy)

- (id)mutableDeepCopy
{
    NSMutableArray *const mutableDeepCopy = [NSMutableArray new];
    for (id object in self) {
        [mutableDeepCopy addObject:[object mutableDeepCopy]];
    }
    
    return mutableDeepCopy;
}

@end

#pragma mark - NSNull

@implementation NSNull (MutableDeepCopy)

- (id)mutableDeepCopy
{
    return self;
}

@end
