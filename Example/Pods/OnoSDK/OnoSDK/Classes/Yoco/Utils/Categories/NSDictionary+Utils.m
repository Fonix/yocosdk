//
//  NSDictionary+Utils.m
//  Yoco
//
//  Created by Lungisa Matshoba on 2014/03/17.
//  Copyright (c) 2014 Yoco Technologies (PTY) LTD. All rights reserved.
//

#import "NSDictionary+Utils.h"
#import "NSArray+Utils.h"
#import "NSObject+MutableDeepCopy.h"

@implementation NSObject (WithoutNulls)

- (id) withoutNulls {
    return self;
}

@end

@implementation NSArray (WithoutNulls)

- (id)withoutNulls {
    NSMutableArray* copy = [self mutableCopy];
    [copy removeObject:[NSNull null]];
    return [copy map:^id(id obj, NSUInteger idx) {
        return [obj withoutNulls];
    }];
}

@end

@implementation NSDictionary (Utils)

- (id)withoutNulls {
    NSMutableDictionary* copy = [self mutableCopy];
    NSArray *keysForNullValues = [copy allKeysForObject:[NSNull null]];
    [copy removeObjectsForKeys:keysForNullValues];
    
    for (NSString* key in copy.allKeys) {
        NSObject * value = copy[key];
        [copy setValue:[value withoutNulls] forKey:key];
    }
    
    return copy;
}

-(void) saveToDefaultsWithKey:(NSString*)key {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self];
    [defaults setObject:data forKey:key];
    [defaults synchronize];
}

+(NSDictionary*) dictionaryFromDefaultsWithKey:(NSString*)key {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:key];
    NSDictionary *userDict = nil;
    if (data) {
        userDict = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    }
    return userDict;
}

-(NSData*) serialize {
    return [NSKeyedArchiver archivedDataWithRootObject:self];
}

+(NSDictionary*) fromData:(NSData*)data {
    return [NSKeyedUnarchiver unarchiveObjectWithData:data];
}

-(NSInteger) centsFromDecimalKeyPath:(NSString*)keyPath {
    NSNumber* val = [self valueForKeyPath:keyPath];
    if ([NSObject isNotNull:val]) {
        return [[[NSDecimalNumber decimalNumberWithDecimal:[val decimalValue]] decimalNumberByMultiplyingBy:[NSDecimalNumber decimalNumberWithString:@"100"]] integerValue];
    }
    else {
        return 0;
    }
    
}

-(BOOL) boolForKey:(NSString*)key withDefault:(BOOL)defaultValue {
    if ([self valueForKey:key]) {
        return [self boolForKey:key];
    } else {
        return defaultValue;
    }
}

-(BOOL) boolForKey:(NSString*)key {
    NSNumber* number = [self valueForKey:key];
    return [number boolValue];
}

-(NSDecimalNumber*)decimalNumberForKey:(NSString*)key {
    NSNumber* number = [self valueForKey:key];
    if ([NSObject isNotNull:number] && [number respondsToSelector:@selector(decimalValue)]) {
        return [NSDecimalNumber decimalNumberWithDecimal:[number decimalValue]];
    } else {
        return nil;
    }
}

-(NSDecimalNumber*)decimalNumberForKeyPath:(NSString*)keyPath {
    NSNumber* number = [self valueForKeyPath:keyPath];
    if ([NSObject isNotNull:number] && [number respondsToSelector:@selector(decimalValue)]) {
        return [NSDecimalNumber decimalNumberWithDecimal:[number decimalValue]];
    } else {
        return nil;
    }
}

-(BOOL) boolForKeyPath:(NSString*)keyPath withDefault:(BOOL)defaultValue {
    id value = [self valueForKeyPath:keyPath];
    if (value == [NSNull null]) {
        return defaultValue;
    } else {
        return [value boolValue];
    }
}

-(BOOL) boolForKeyPath:(NSString*)keyPath {
    return [self boolForKeyPath:keyPath withDefault:NO];
}

+(NSDictionary*) emptyIfNull:(NSDictionary*)dictionary {
    if (dictionary) {
        return dictionary;
    } else {
        return @{};
    }
}

-(NSString*)toJSON {
    return [NSDictionary toJSON:self];
}

+(NSString*)toJSON:(NSDictionary*)dict {
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    
    if (!jsonData) {
        [NSException raise:@"Could not convert dictionary to JSON." format:@"Error converting dictionary to JSON: %@", error];
    } else {
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    return nil;
}

+(NSDictionary*)fromJSON:(NSString*)json {
    NSError *error;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:[json dataUsingEncoding:NSUTF8StringEncoding]
                                                         options:NSJSONReadingMutableContainers
                                                           error:&error];
    
    if (!dict) {
        [NSException raise:@"Could not convert JSON to dictionary." format:@"Error converting JSON to dictionary: %@", error];
    }
    
    return dict;
}

+(NSDictionary*) dictionaryByMerging:(NSDictionary *)dict1 with:(NSDictionary *)dict2 {
    NSMutableDictionary* result = [NSMutableDictionary dictionaryWithDictionary:dict1];
    
    [dict2 enumerateKeysAndObjectsUsingBlock: ^(id key, id obj, BOOL *stop) {
        if (![dict1 objectForKey:key]) {
            if ([obj isKindOfClass:[NSDictionary class]]) {
                NSDictionary* existingDictKey = [dict1 objectForKey:key];
                if ([NSObject isNull:existingDictKey]) {
                    [result setObject:obj forKey:key];
                } else {
                    NSDictionary * newVal = [[dict1 objectForKey:key] dictionaryByMergingWith:(NSDictionary*)obj];
                    [result setObject:newVal forKey:key];
                }
            } else {
                [result setObject:obj forKey:key];
            }
        }
    }];
    
    return result;
}
-(NSDictionary*) dictionaryByMergingWith:(NSDictionary*)dict {
    return [[self class] dictionaryByMerging: self with:dict];
}

-(void) removeObjectForKeyPath:(NSString*)keyPath {
    // Separate the key path
    NSArray * keyPathElements = [keyPath componentsSeparatedByString:@"."];
    // Drop the last element and rejoin the path
    NSUInteger numElements = [keyPathElements count];
    NSString * keyPathHead = [[keyPathElements subarrayWithRange:(NSRange){0, numElements - 1}] componentsJoinedByString:@"."];
    // Get the mutable dictionary represented by the path minus that last element
    NSMutableDictionary * tailContainer = [self valueForKeyPath:keyPathHead];
    // Remove the object represented by the last element
    [tailContainer removeObjectForKey:[keyPathElements lastObject]];
}

/** Set a value in a dictionary, but use NSNUll if it is nil, or make a mutable copy
 if it is a dictionary. This makes sure we don't throw any exceptions, and that the
 entire dictionary remains mutable within all levels
 */
- (void)safeSetValue:(nullable id)value forKey:(NSString *)key {
    if ([NSObject isNull:value]) {
        [self setValue:[NSNull null] forKey:key];
    } else if ([value isKindOfClass:[NSDictionary class]] && ![value isKindOfClass:[NSMutableDictionary class]]) {
        [self setValue:[value mutableDeepCopy] forKey:key];
    }
    else {
        [self setValue:value forKey:key];
    }
}

/**
 * Set a value deep in a dictionary and create any intermediate containers that do not yet exist
 */
-(void) setNestedValue:(id)value forKeyPath:(NSString*)keyPath {
    NSArray* keyPathElements = [keyPath componentsSeparatedByString:@"."];
    
    if (keyPathElements.count == 1) {
        [self setValue:value forKey:keyPath];
        return;
    }
    
    id currentNode = self;
    NSArray* containerPath = [keyPathElements subarrayWithRange:(NSRange){0, keyPathElements.count - 1}];
    
    for (NSString* key in containerPath) {
        if ([NSObject isNull:[currentNode valueForKey:key]]) {

            [currentNode setObject:[[NSMutableDictionary alloc] init] forKey:key];
        }
        currentNode = [currentNode valueForKey:key];
    }
    
    [currentNode safeSetValue:value forKey:keyPathElements.lastObject];
}

@end
