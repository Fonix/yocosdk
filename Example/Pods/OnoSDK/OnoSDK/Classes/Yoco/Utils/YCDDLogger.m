//
//  YCDDLogger.m
//  Yoco
//
//  Created by Andrew Snowden on 2015/07/20.
//  Copyright (c) 2015 Yoco Technologies (PTY) LTD. All rights reserved.
//

#import "YCDDLogger.h"
#import "OnoIOSLogger.h"
#import "YCTimeline.h"
#import "YCUtils.h"
#import "NSError+PrettyError.h"

@interface YCDDLogger()

// Prefixes that will cause a message to be logged as TRACE
@property (nonatomic, retain) NSArray* tracePrefixes;

// Prefixes that will cause a message to be logged as INFO
@property (nonatomic, retain) NSArray* infoPrefixes;

@property (atomic, retain) NSMutableArray* logMessages;

@property (nonatomic, retain) NSRegularExpression* webRequest;

@property (nonatomic, retain) NSMutableDictionary* webRequestTasks;

@end

@implementation YCDDLogger

-(id)init {
    self = [super init];
    if (self) {
        NSError* error = NULL;
        // Matching Payworks web requests like "GET 'https://api.payworks.io/foo' (200)"
        self.webRequest = [NSRegularExpression regularExpressionWithPattern:@"^(GET|POST|PUT|DELETE) '([^']+)'( \\([0-9]+\\))?" options:0 error:&error];
        
        if (error) {
            OnoLogError(@"Unable to construct web request regex: %@", error.infoString);
        }
        
        self.webRequestTasks = [[NSMutableDictionary alloc] init];
        
        self.infoPrefixes = @[
                     @"POST",
                     @"GET",
                     @"about to start transaction",
                     @"starting transaction",
                     @"got a customer verification response",
                     @"checking if transaction is",
                     @"starting update check",
                     @"signature",
                     @"no signature",
                     @"no identification",
                     @"request to dispatch text",
                     @"step2",
                     @"whitelist available",
                     @"sending get battery status",
                     @"enabling feature",
                     @"added requirement"
                     ];
        
        self.tracePrefixes = @[
                               @"check",
                               @"constructing request",
                               @"stop iterating",
                               @"data send",
                               @"data received",
                               @"send ",
                               @"debug",
                               @"parseDataForPackets",
                               @"#no more data",
                               @"creating request",
                               @"cancel",
                               @"info"
                               ];
    }
    return self;
}

- (void)logMessage:(DDLogMessage *)logMessage {
    YCLogLevel logLevel = YCLogLevelDebug;
    
    // DDLog seems to deallocate these aggressively, so we work with copies
    NSString* message = [logMessage.message copy];
    NSString* function = [logMessage.function copy];
    
    NSDictionary* metadata = @{
                               @"fileName": [logMessage.fileName copy],
                               @"function": function,
                               @"line": @(logMessage.line),
                               @"queue": [logMessage.queueLabel copy]
                               };
    
    
    // Payworks seems to use flag
    switch (logMessage.flag) {
        case DDLogFlagDebug:
            break;
        case DDLogFlagInfo:
            logLevel = YCLogLevelInfo;
            break;
        case DDLogFlagError:
            logLevel = YCLogLevelError;
            break;
        case DDLogFlagVerbose:
            logLevel = YCLogLevelError;
            break;
        case DDLogFlagWarning:
            logLevel = YCLogLevelError;
            break;
    }
    
    // Payworks seems really bad at considering anything not debug, there are some things we want to always send
    if (logLevel == YCLogLevelDebug) {
        for (NSString* prefix in self.infoPrefixes) {
            if ([message hasPrefix:prefix]) {
                logLevel = YCLogLevelInfo;
            }
        }
    }
    
    if (logLevel == YCLogLevelDebug) {
        for (NSString* prefix in self.tracePrefixes) {
            if ([message hasPrefix:prefix]) {
                logLevel = YCLogLevelTrace;
            }
        }
    }
    
    // Payworks logs a connection change as an error :(
    if ([message hasPrefix:@"connectionState changed from"]) {
        logLevel = YCLogLevelInfo;
    }
    
    NSTextCheckingResult* match = [self.webRequest firstMatchInString:message options:0 range:NSMakeRange(0, message.length)];

    if (match) {
        // This was a web request
        NSString* method = [message substringWithRange:[match rangeAtIndex:1]];
        NSString* url = [message substringWithRange:[match rangeAtIndex:2]];
      
        NSRange statusRange = [match rangeAtIndex:3];
        
        NSString* webRequestTaskName = @"payworks web request";
    
        if (statusRange.location == NSNotFound) {
            YCTimelineTask* timelineTask = [YCTimeline addKeyedTask:webRequestTaskName key:url];
            [timelineTask setMetadata:@{@"method": method, @"url": url}];
            [YCTimeline addTaskToRunningTimelines:timelineTask];
        } else {
            [YCTimeline stopKeyedTask:webRequestTaskName key:url];
            
            NSString* statusCode = [message substringWithRange:statusRange];
            
            if (statusCode.intValue >= 300) {
                // This was a failed web request
                logLevel = YCLogLevelError;
                NSDictionary* metadata = @{
                                           @"method": method,
                                           @"url": url,
                                           @"statusCode": @(statusCode.intValue)
                                           };
                [YCTimeline addEventToRunningTimelines:[[YCTimelineEvent alloc] initWithTitle:@"payworks web request failed" eventType:YCTimelineEventTypeError metadata:metadata]];
            }
        }
    }

    [[OnoIOSLogger sharedLogger] logMessage:logLevel message:message metadata:metadata component:@"payworks"];
}

@end
