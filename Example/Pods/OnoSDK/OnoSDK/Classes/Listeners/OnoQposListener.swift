//
//  OnoQposListener.swift
//  YocoPOS
//
//  Created by Mandisa Mjamba on 2019/07/17.
//  Copyright © 2019 Yoco. All rights reserved.
//

import Foundation
import Ono

@objc(IOSQposListener)
class IOSQposListener: NSObject, QPOSServiceListener {
  let TAG = "OnoSDK> Dspread>IOSQposListener> "
  var listener: OnoQposListenerProtocol
  
  init (listener: OnoQposListenerProtocol) {
    self.listener = listener
  }
  
  func onRequestQposConnected() {
    DispatchQueue.main.async {
      self.listener.onRequestQposConnected()
    }
  }
  
  func onReturnSetSleepTimeResult(_ p0: Bool) {
    DispatchQueue.main.async {
      self.listener.onReturnSetSleepTimeResult(p0: p0)
    }
  }
  
  func onQposIdResult(_ posId: [AnyHashable : Any]!) {
    DispatchQueue.main.async {
      self.listener.onQposIdResult(terminalData: posId as? [String : String])
    }
  }
  
  func onQposInfoResult(_ posInfoData: [AnyHashable : Any]!) {
    DispatchQueue.main.async {
      self.listener.onQposInfoResult(terminalInfo: posInfoData as? [String : String])
    }
  }
  
  func onRequestQposDisconnected() {
    DispatchQueue.main.async {
      self.listener.onRequestQposDisconnected()
    }
  }
  
  func onRequestNoQposDetected() {
    DispatchQueue.main.async {
      self.listener.onRequestNoQposDetected()
    }
  }
  
  func onRequestSetAmount() {
    DispatchQueue.main.async {
      self.listener.onRequestSetAmount()
    }
  }
  
  func onRequestWaitingUser() {
    DispatchQueue.main.async {
      self.listener.onRequestWaitingUser()
    }
  }
  
  func onDoTradeResult(_ result: DoTradeResult, decodeData: [AnyHashable : Any]!) {
    DispatchQueue.main.async {
      self.listener.onDoTradeResult(
        cardReadMode: self.convertCardReadToTradeResult(cardReadMode: result),
        tradeData: decodeData as? [String : String]
      )
    }
  }
  
  func onRequestSelectEmvApp(_ appList: [Any]!) {
    DispatchQueue.main.async {
      let array = NSMutableArray(array: appList)
      self.listener.onRequestSelectEmvApp(emvApplicationsList: array)
    }
  }
  
  func onRequestTime() {
    DispatchQueue.main.async {
      self.listener.onRequestTime()
    }
  }
  
  func onRequest(_ displayMsg: Display) {
    DispatchQueue.main.async {
      let onoDisplay = self.convertDisplayToOnoDisplay(display: displayMsg)
      self.listener.onRequestDisplay(display: onoDisplay)
    }
  }
  
  func onTradeCancelled() {
    DispatchQueue.main.async {
      self.listener.onTradeCancelled()
    }
  }
  
  func onRequestOnlineProcess(_ authorizationTLV: String?) {
    DispatchQueue.main.async {
      self.listener.onRequestOnlineProcess(authorizationTLV: authorizationTLV)
    }
  }
  
  func onReturnReversalData(_ declinedTLV: String?) {
    DispatchQueue.main.async {
      self.listener.onReturnReversalData(declinedTLV: declinedTLV)
    }
  }
  
  func onRequestBatchData(_ acceptedTLV: String?) {
    DispatchQueue.main.async {
      self.listener.onRequestBatchData(acceptedTLV: acceptedTLV)
    }
  }
  
  func onQposIsCardExist(exists: Bool) {
    DispatchQueue.main.async {
      self.listener.onQposIsCardExist(exists: exists)
    }
  }
  
  func onLcdShowCustomDisplay(_ isMessageDisplayed: Bool) {
    DispatchQueue.main.async {
      self.listener.onLcdShowCustomDisplay(isMessageDisplayed: isMessageDisplayed)
    }
  }
  
  func onError(_ errorState: Error) {
    Logger().d(tag: TAG, message: "onError: errorState \(errorState)")
    DispatchQueue.main.async {
      let onoError = self.convertErrorToQposError(error: errorState)
      self.listener.onError(error: onoError)
    }
  }
  
  func onDHError(_ errorState: DHError) {
    Logger().d(tag: TAG, message: "onDHError: errorState \(errorState)")
    DispatchQueue.main.async {
      let onoError = self.convertDHErrorToQposError(error: errorState)
      self.listener.onError(error: onoError)
    }
  }
  
  func onEmvICCExceptionData(_ iccExceptionTlv: String?) {
    DispatchQueue.main.async {
      self.listener.onEmvICCExceptionData(iccExceptionTlv: iccExceptionTlv)
    }
  }
  
  func onReturnPower(onIccResult isSuccess: Bool, ksn: String!, atr: String!, atrLen: Int) {
    DispatchQueue.main.async {
      self.listener.onReturnPowerOnNFCResult(p0: isSuccess, p1: ksn, p2: atr, p3: Int32(atrLen))
    }
  }
  
  func onReturnSetMasterKeyResult(_ p0: Bool) {
    DispatchQueue.main.async {
      self.listener.onReturnSetMasterKeyResult(p0: p0)
    }
  }
  
  func onReturnPowerOnIccResult(p0: Bool, p1: String?, p2: String?, p3: Int) {
    DispatchQueue.main.async {
      self.listener.onReturnPowerOnIccResult(p0: p0, p1: p1, p2: p2, p3: Int32(p3))
    }
  }
  
  func onSetBuzzerResult(p0: Bool) {
    DispatchQueue.main.async {
      self.listener.onSetBuzzerResult(p0: p0)
    }
  }
  
  func onBatchWriteMifareCardResult(p0: String?, p1: [String : NSMutableArray]?) {
    DispatchQueue.main.async {
      self.listener.onBatchWriteMifareCardResult(p0: p0, p1: p1)
    }
  }
  
  func onQposDoTradeLog(p0: Bool) {
    DispatchQueue.main.async {
      self.listener.onQposDoTradeLog(p0: p0)
    }
  }
  
  func onVerifyMifareCardResult(_ p0: Bool) {
    DispatchQueue.main.async {
      self.listener.onVerifyMifareCardResult(p0: p0)
    }
  }
  
  func onGetSleepModeTime(p0: String?) {
    DispatchQueue.main.async {
      self.listener.onGetSleepModeTime(p0: p0)
    }
  }
  
  func onSetBuzzerStatusResult(p0: Bool) {
    DispatchQueue.main.async {
      self.listener.onSetBuzzerStatusResult(status: p0)
    }
  }
  
  func onReturnBuzzerStatusResult(_ p0: Bool) {
    DispatchQueue.main.async {
      self.listener.onSetBuzzerStatusResult(status: p0)
    }
  }
  
  func onRequest(_ transactionResult: TransactionResult) {
    DispatchQueue.main.async {
      let onoTransactionResult = self.convertTransactionResultToOnoTransactionResult(transactionResult: transactionResult)
      self.listener.onRequestTransactionResult(transactionResult: onoTransactionResult)
    }
  }
  
  func onRequestTransactionLog(_ p0: String?) {
    DispatchQueue.main.async {
      self.listener.onRequestTransactionLog(p0: p0)
    }
  }
  
  func onReturnGetQuickEmvResult(p0: Bool) {
    DispatchQueue.main.async {
      self.listener.onReturnGetQuickEmvResult(p0: p0)
    }
  }
  
  func onSetSleepModeTime(p0: Bool) {
    DispatchQueue.main.async {
      self.listener.onSetSleepModeTime(p0: p0)
    }
  }
  
  func onEncryptData(p0: String?) {
    DispatchQueue.main.async {
      self.listener.onEncryptData(p0: p0)
    }
  }
  
  func onRequestGetCardNoResult(_ result: String!) {
    DispatchQueue.main.async {
      self.listener.onGetCardNoResult(p0: result)
    }
  }
  
  func onAsyncResetPosStatus(_ isReset: String!) {
    Logger().d(tag: TAG, message: "onAsyncResetPosStatus: We do not have a listener method for this callback: reset \(String(describing: isReset))")
  }
  
  func onRequestPinEntry() {
    Logger().d(tag: TAG, message: "onRequestPinEntry: We do not have a listener method for this callback")
  }
  
  func onDownloadRsaPublicKeyResult(_ result: [AnyHashable : Any]!) {
    Logger().d(tag: TAG, message: "onDownloadRsaPublicKeyResult: We do not have a listener method for this callback: result \(String(describing: result))")
  }
  
  func onSetManagementKey(p0: Bool) {
    DispatchQueue.main.async {
      self.listener.onSetManagementKey(p0: p0)
    }
  }
  
  func transferMifareData(p0: String?) {
    DispatchQueue.main.async {
      self.listener.transferMifareData(p0: p0)
    }
  }
  
  func onUpdatePosFirmwareResult(_ p0: UpdateInformationResult) {
    DispatchQueue.main.async {
      self.listener.onUpdatePosFirmwareResult(p0: self.mapUpdateInformationResult(infoResult: p0))
    }
  }
  
  func onReadBusinessCardResult(p0: Bool, p1: String?) {
    DispatchQueue.main.async {
      self.listener.onReadBusinessCardResult(p0: p0, p1: p1)
    }
  }
  
  func onReturnPowerOffIccResult(_ p0: Bool) {
    DispatchQueue.main.async {
      self.listener.onReturnPowerOffNFCResult(p0: p0)
    }
  }
  
  func onRequestCalculateMac(_ p0: String?) {
    DispatchQueue.main.async {
      self.listener.onRequestCalculateMac(p0: p0)
    }
  }
  
  func onDeviceFound(p0: BluetoothDevice?) {
    DispatchQueue.main.async {
      if (p0 != nil) {
        let device = BluetoothDevice(address: p0!.address, name: p0!.name, bondState: BluetoothBondState.stateNotPaired)
        self.listener.onDeviceFound(p0: device)
      }
    }
  }
  
  func onRequestFinalConfirm() {
    DispatchQueue.main.async {
      self.listener.onRequestFinalConfirm()
    }
  }
  
  func onBluetoothBondTimeout() {
    DispatchQueue.main.async {
      self.listener.onBluetoothBondTimeout()
    }
  }
  
  func onRequestNoQposDetectedUnbond() {
    DispatchQueue.main.async {
      self.listener.onRequestNoQposDetectedUnbond()
    }
  }
  
  func batchReadMifareCardResult(_ result: [AnyHashable : Any]!) {
    DispatchQueue.main.async {
      self.listener.onBatchReadMifareCardResult(p0: "", p1: result as? [String : NSMutableArray])
    }
  }
  
  func batchWriteMifareCardResult(_ result: [AnyHashable : Any]!) {
    DispatchQueue.main.async {
      self.listener.onBatchWriteMifareCardResult(p0: "", p1: result as? [String : NSMutableArray])
    }
  }
  
  func onConfirmAmountResult(p0: Bool) {
    DispatchQueue.main.async {
      self.listener.onConfirmAmountResult(p0: p0)
    }
  }
  
  func onReturniccCashBack(_ result: [AnyHashable : Any]!) {
    DispatchQueue.main.async {
      self.listener.onReturniccCashBack(p0: result as? [String : String])
    }
  }
  
  func writeMifareULData(_ p0: String?) {
    DispatchQueue.main.async {
      self.listener.writeMifareULData(p0: p0)
    }
  }
  
  func verifyMifareULData(_ result: [AnyHashable : Any]!) {
    DispatchQueue.main.async {
      self.listener.verifyMifareULData(p0: result as? [String : String])
    }
  }
  
  func onGetKeyCheckValue(_ checkValueResult: [AnyHashable : Any]!) {
    DispatchQueue.main.async {
      // checkValueResult needs to be of type NSMutableArray
      let resultArray = NSMutableArray()
      for result in checkValueResult.reversed() {
        resultArray.add(NSMutableArray(object: result))
      }
      self.listener.onGetKeyCheckValue(p0: resultArray)
    }
  }
  
  func onReturnGetEMVListResult(p0: String?) {
    DispatchQueue.main.async {
      self.listener.onReturnGetEMVListResult(p0: p0)
    }
  }
  
  func getMifareReadData(_ result: [AnyHashable : Any]!) {
    DispatchQueue.main.async {
      self.listener.getMifareReadData(p0: result as? [String : String])
    }
  }
  
  func onUpdateMasterKeyResult(_ isSuccess: Bool, aDic resultDic: [AnyHashable : Any]!) {
    DispatchQueue.main.async {
      self.listener.onUpdateMasterKeyResult(p0: isSuccess, p1: resultDic as? [String : String])
    }
  }
  
  func onCbcMacResult(p0: String?) {
    DispatchQueue.main.async {
      self.listener.onCbcMacResult(p0: p0)
    }
  }
  
  func onGetPosComm(_ mod: Int, amount: String?, posId posid: String?) {
    DispatchQueue.main.async {
      self.listener.onGetPosComm(mod: Int32(mod), amount: amount, posid: posid)
    }
  }
  
  func onFinishMifareCardResult(_ p0: Bool) {
    DispatchQueue.main.async {
      self.listener.onFinishMifareCardResult(p0: p0)
    }
  }
  
  func onBluetoothBonded() {
    DispatchQueue.main.async {
      self.listener.onBluetoothBonded()
    }
  }
  
  func onReadMifareCardResult(_ result: [AnyHashable : Any]!) {
    DispatchQueue.main.async {
      self.listener.onReadMifareCardResult(p0: result as? [String : String])
    }
  }
  
  func onReturnPowerOffIccResult(p0: Bool) {
    DispatchQueue.main.async {
      self.listener.onReturnPowerOffIccResult(p0: p0)
    }
  }
  
  func onBluetoothBondFailed() {
    DispatchQueue.main.async {
      self.listener.onBluetoothBondFailed()
    }
  }
  
  func onRequestIsServerConnected() {
    DispatchQueue.main.async {
      self.listener.onRequestIsServerConnected()
    }
  }
  
  func onBluetoothBoardStateResult(p0: Bool) {
    DispatchQueue.main.async {
      self.listener.onBluetoothBoardStateResult(p0: p0)
    }
  }
  
  func onWriteMifareCardResult(_ p0: Bool) {
    DispatchQueue.main.async {
      self.listener.onWriteMifareCardResult(p0: p0)
    }
  }
  
  func onReturnApduResult(_ p0: Bool, apdu p1: String?, apdu_Len p2: Int) {
    DispatchQueue.main.async {
      self.listener.onReturnApduResult(p0: p0, p1: p1, p2: Int32(p2))
    }
  }
  
  func onReturnUpdateEMVRIDResult(p0: Bool) {
    DispatchQueue.main.async {
      self.listener.onReturnUpdateEMVRIDResult(p0: p0)
    }
  }
  
  func onQposKsnResult(p0: [String : String]?) {
    DispatchQueue.main.async {
      self.listener.onQposKsnResult(p0: p0)
    }
  }
  
  func onReturnNFCApduResult(p0: Bool, p1: String?, p2: Int) {
    DispatchQueue.main.async {
      self.listener.onReturnNFCApduResult(p0: p0, p1: p1, p2: Int32(p2))
    }
  }
  
  func onWriteBusinessCardResult(p0: Bool) {
    DispatchQueue.main.async {
      self.listener.onWriteBusinessCardResult(p0: p0)
    }
  }
  
  func onSetBuzzerTimeResult(p0: Bool) {
    DispatchQueue.main.async {
      self.listener.onSetBuzzerTimeResult(status: p0)
    }
  }
  
  func onPinKeyTDESResult(_ p0: String?) {
    DispatchQueue.main.async {
      self.listener.onPinKey_TDES_Result(p0: p0)
    }
  }
  
  func onBatchReadMifareCardResult(p0: String?, p1: [String : NSMutableArray]?) {
    DispatchQueue.main.async {
      self.listener.onBatchReadMifareCardResult(p0: p0, p1: p1)
    }
  }
  
  func onReturnRSAResult(p0: String?) {
    DispatchQueue.main.async {
      self.listener.onReturnRSAResult(p0: p0)
    }
  }
  
  func onReturnUpdateIPEKResult(p0: Bool) {
    DispatchQueue.main.async {
      self.listener.onReturnUpdateIPEKResult(p0: p0)
    }
  }
  
  func onWaitingforData(p0: String?) {
    DispatchQueue.main.async {
      self.listener.onWaitingforData(p0: p0)
    }
  }
  
  func onQposDoGetTradeLogNum(p0: String?) {
    DispatchQueue.main.async {
      self.listener.onQposDoGetTradeLogNum(p0: p0)
    }
  }
  
  func onSetParamsResult(p0: Bool, p1: [String : Any]?) {
    DispatchQueue.main.async {
      self.listener.onSetParamsResult(p0: p0, p1: p1)
    }
  }
  
  func onOperateMifareCardResult(_ result: [AnyHashable : Any]!) {
    DispatchQueue.main.async {
      self.listener.onOperateMifareCardResult(p0: result as? [String : String])
    }
  }
  
  func getMifareFastReadData(_ result: [AnyHashable : Any]!) {
    DispatchQueue.main.async {
      self.listener.getMifareFastReadData(p0: result as? [String : String])
    }
  }
  
  func onSearchMifareCardResult(_ result: [AnyHashable : Any]!) {
    DispatchQueue.main.async {
      self.listener.onSearchMifareCardResult(p0: result as? [String : String])
    }
  }
  
  func onRequestDevice() {
    DispatchQueue.main.async {
      self.listener.onRequestDevice()
    }
  }

  func onRequestUpdateKey(p0: String?) {
    DispatchQueue.main.async {
      self.listener.onRequestUpdateKey(p0: p0)
    }
  }
  
  func onReturnCustomConfigResult(_ wasSuccessful: Bool, config result: String?) {
    DispatchQueue.main.async {
      self.listener.onReturnCustomConfigResult(wasSuccessful: wasSuccessful, result: result)
    }
  }
  
  func getMifareCardVersion(_ result: [AnyHashable : Any]!) {
    DispatchQueue.main.async {
      self.listener.getMifareCardVersion(p0: result as? [String : String])
    }
  }
  
  func onRequestSignatureResult(_ result: Data!) {
    DispatchQueue.main.async {
      self.listener.onRequestSignatureResult(p0: result.toKotlinByteArray())
    }
  }
  
  func onRequestSetPin() {
    DispatchQueue.main.async {
      self.listener.onRequestSetPin()
    }
  }
  
  func onBluetoothBonding() {
    DispatchQueue.main.async {
      self.listener.onBluetoothBonding()
    }
  }
  
  func onAddKey(p0: Bool) {
    DispatchQueue.main.async {
      self.listener.onAddKey(p0: p0)
    }
  }
  
  func onGetShutDownTime(p0: String?) {
    DispatchQueue.main.async {
      self.listener.onGetShutDownTime(p0: p0)
    }
  }
  
  func onQposDoSetRsaPublicKey(p0: Bool) {
    DispatchQueue.main.async {
      self.listener.onQposDoSetRsaPublicKey(p0: p0)
    }
  }
  
  func onReturnBatchSendAPDUResult(_ apduResponses: [AnyHashable : Any]!) {
    DispatchQueue.main.async {
      self.listener.onReturnBatchSendAPDUResult(p0: apduResponses as? KotlinMutableDictionary<KotlinInt, NSString>)
    }
  }
  
  func onReturnUpdateEMVResult(wasSuccessful: Bool) {
    DispatchQueue.main.async {
      self.listener.onReturnUpdateEMVResult(wasSuccessful: wasSuccessful)
    }
  }
  
  func onRequestDeviceScanFinished() {
    DispatchQueue.main.async {
      self.listener.onRequestDeviceScanFinished()
    }
  }
  
  func onQposGenerateSessionKeysResult(p0: [String : String]?) {
    DispatchQueue.main.async {
      self.listener.onQposGenerateSessionKeysResult(p0: p0)
    }
  }
  
  func onRequestUpdateWorkKeyResult(_ p0: UpdateInformationResult) {
    DispatchQueue.main.async {
      self.listener.onRequestUpdateWorkKeyResult(p0: self.mapUpdateInformationResult(infoResult: p0))
    }
  }
  
  func onQposDoGetTradeLog(p0: String?, p1: String?) {
    DispatchQueue.main.async {
      self.listener.onQposDoGetTradeLog(p0: p0, p1: p1)
    }
  }
  
  func onGetInputAmountResult(isInputEntered: Bool, inputAmount: String?) {
    DispatchQueue.main.async {
      self.listener.onGetInputAmountResult(isInputEntered: isInputEntered, inputAmount: inputAmount)
    }
  }
  
  func onGetBuzzerStatusResult(p0: String?) {
    DispatchQueue.main.async {
      Logger().i(tag: "OnoSDK> OnoQposListener", message: "onGetBuzzerStatusResult \(String(describing: p0))")
      self.listener.onGetBuzzerStatusResult(p0: p0)
    }
  }
  
  func onReturnDownloadRsaPublicKey(p0: KotlinMutableDictionary<NSString, NSString>?) {
    DispatchQueue.main.async {
      self.listener.onReturnDownloadRsaPublicKey(p0: p0)
    }
  }
  
  func onGetDevicePubKey(key: String?) {
    DispatchQueue.main.async {
      self.listener.onGetDevicePubKey(key: key)
    }
  }
  
  func onSetPosBlePinCode(p0: Bool) {
    DispatchQueue.main.async {
      self.listener.onSetPosBlePinCode(p0: p0)
    }
  }
  
  private func mapUpdateInformationResult(infoResult: UpdateInformationResult) -> OnoQposUpdateInformationResult? {
    switch (infoResult) {
      case .UPDATE_SUCCESS:
        return OnoQposUpdateInformationResult.updateSuccess
      case .UPDATE_FAIL:
        return OnoQposUpdateInformationResult.updateFail
      case .UPDATE_PACKET_VEFIRY_ERROR:
        return OnoQposUpdateInformationResult.updatePacketVefiryError
      case .UPDATE_PACKET_LEN_ERROR:
        return OnoQposUpdateInformationResult.updatePacketLenError
      case .UPDATE_LOWPOWER:
        return OnoQposUpdateInformationResult.updateLowpower
      case .UPDATING:
        return OnoQposUpdateInformationResult.updating
      @unknown default:
        return nil
      }
  }
  
  private func convertCardReadToTradeResult(cardReadMode: DoTradeResult) -> OnoQposDoTradeResult {
    switch (cardReadMode) {
      case .NONE:
        return OnoQposDoTradeResult.none
      case .MCR:
        return OnoQposDoTradeResult.mcr
      case .ICC:
        return OnoQposDoTradeResult.icc
      case .NOT_ICC:
        return OnoQposDoTradeResult.notIcc
      case .BAD_SWIPE:
        return OnoQposDoTradeResult.badSwipe
      case .NO_RESPONSE:
        return OnoQposDoTradeResult.noResponse
      case .NO_UPDATE_WORK_KEY:
        return OnoQposDoTradeResult.noUpdateWorkKey
      case .NFC_ONLINE:
        return OnoQposDoTradeResult.nfcOnline
      case .NFC_OFFLINE:
        return OnoQposDoTradeResult.nfcOffline
      case .NFC_DECLINED:
        return OnoQposDoTradeResult.nfcDeclined
      default:
        Logger().e(tag: TAG, message: "Could not map card read mode returning none")
        return OnoQposDoTradeResult.none
    }
  }
  
  private func convertDisplayToOnoDisplay(display: Display) -> OnoQposDisplay {
    switch (display) {
      case .TRY_ANOTHER_INTERFACE:
        return OnoQposDisplay.tryAnotherInterface
      case .PLEASE_WAIT:
        return OnoQposDisplay.pleaseWait
      case .REMOVE_CARD:
        return OnoQposDisplay.removeCard
      case .CLEAR_DISPLAY_MSG:
        return OnoQposDisplay.clearDisplayMsg
      case .PROCESSING:
        return OnoQposDisplay.processing
      case .PIN_OK:
        return OnoQposDisplay.pinOk
      case .TRANSACTION_TERMINATED:
        return OnoQposDisplay.transactionTerminated
      case .INPUT_PIN_ING:
        return OnoQposDisplay.inputPinIng
      case .MAG_TO_ICC_TRADE:
        return OnoQposDisplay.magToIccTrade
      case .INPUT_OFFLINE_PIN_ONLY:
        return OnoQposDisplay.inputOfflinePinOnly
      case .CARD_REMOVED:
        return OnoQposDisplay.cardRemoved
      case .INPUT_LAST_OFFLINE_PIN:
        return OnoQposDisplay.inputLastOfflinePin
      case .MSR_DATA_READY:
        return OnoQposDisplay.msrDataReady
      @unknown default:
        fatalError("convertDisplayToOnoDisplay display \(display) does not match any OnoQposDisplay")
      }
  }
  
  private func convertErrorToQposError(error: Error) -> OnoQposError {
    switch (error) {
      case .TIMEOUT:
        return OnoQposError.timeout
      case .MAC_ERROR:
        return OnoQposError.macError
      case .CMD_TIMEOUT:
        return OnoQposError.cmdTimeout
      case .CMD_NOT_AVAILABLE:
        return OnoQposError.cmdNotAvailable
      case .DEVICE_RESET:
        return OnoQposError.deviceReset
      case .UNKNOWN:
        return OnoQposError.unknown
      case .DEVICE_BUSY:
        return OnoQposError.deviceBusy
      case .INPUT_OUT_OF_RANGE:
        return OnoQposError.inputOutOfRange
      case .INPUT_INVALID_FORMAT:
        return OnoQposError.inputInvalidFormat
      case .INPUT_ZERO_VALUES:
        return OnoQposError.inputZeroValues
      case .INPUT_INVALID:
        return OnoQposError.inputInvalid
      case .CASHBACK_NOT_SUPPORTED:
        return OnoQposError.cashbackNotSupported
      case .CRC_ERROR:
        return OnoQposError.crcError
      case .COMM_ERROR:
        return OnoQposError.commError
      case .WR_DATA_ERROR:
        return OnoQposError.wrDataError
      case .EMV_APP_CFG_ERROR:
        return OnoQposError.emvAppCfgError
      case .EMV_CAPK_CFG_ERROR:
        return OnoQposError.emvCapkCfgError
      case .APDU_ERROR:
        return OnoQposError.apduError
      // This exists in Android but not in iOS
      // case Error.APP_SELECT_TIMEOUT:
      //   return OnoQposError.appSelectTimeout
      case .ICC_ONLINE_TIMEOUT:
        return OnoQposError.iccOnlineTimeout
      case .AMOUNT_OUT_OF_LIMIT:
        return OnoQposError.amountOutOfLimit
      case .DIGITS_UNAVAILABLE, .QPOS_MEMORY_OVERFLOW:
        Logger().e(tag: TAG, message: "We are not handling this error: \(error)")
        return OnoQposError.unknown
      default:
        Logger().e(tag: TAG, message: "We are not handling this error: \(error)")
        return OnoQposError.unknown
    }
  }
  
  private func convertDHErrorToQposError(error: DHError) -> OnoQposError {
    switch (error) {
      case .TIMEOUT:
        return OnoQposError.timeout
      case .MAC_ERROR:
        return OnoQposError.macError
      case .CMD_TIMEOUT:
        return OnoQposError.cmdTimeout
      case .CMD_NOT_AVAILABLE:
        return OnoQposError.cmdNotAvailable
      case .DEVICE_RESET:
        return OnoQposError.deviceReset
      case .UNKNOWN:
        return OnoQposError.unknown
      case .DEVICE_BUSY:
        return OnoQposError.deviceBusy
      case .INPUT_OUT_OF_RANGE:
        return OnoQposError.inputOutOfRange
      case .INPUT_INVALID_FORMAT:
        return OnoQposError.inputInvalidFormat
      case .INPUT_ZERO_VALUES:
        return OnoQposError.inputZeroValues
      case .INPUT_INVALID:
        return OnoQposError.inputInvalid
      case .CASHBACK_NOT_SUPPORTED:
        return OnoQposError.cashbackNotSupported
      case .CRC_ERROR:
        return OnoQposError.crcError
      case .COMM_ERROR:
        return OnoQposError.commError
      case .WR_DATA_ERROR:
        return OnoQposError.wrDataError
      case .EMV_APP_CFG_ERROR:
        return OnoQposError.emvAppCfgError
      case .EMV_CAPK_CFG_ERROR:
        return OnoQposError.emvCapkCfgError
      case .APDU_ERROR:
        return OnoQposError.apduError
      // This exists in Android but not in iOS
      // case Error.APP_SELECT_TIMEOUT:
      //   return OnoQposError.appSelectTimeout
      case .ICC_ONLINE_TIMEOUT:
        return OnoQposError.iccOnlineTimeout
      case .AMOUNT_OUT_OF_LIMIT:
        return OnoQposError.amountOutOfLimit
      case .DIGITS_UNAVAILABLE, .QPOS_MEMORY_OVERFLOW:
        Logger().d(tag: TAG, message: "We are not handling this error: \(error)")
        return OnoQposError.unknown
      default:
        Logger().e(tag: TAG, message: "We are not handling this error: \(error)")
        return OnoQposError.unknown
    }
  }
  
  private func convertTransactionResultToOnoTransactionResult(transactionResult: TransactionResult) -> OnoQposTransactionResult {
    switch (transactionResult) {
      case .APPROVED:
        return OnoQposTransactionResult.approved
      case .TERMINATED:
        return OnoQposTransactionResult.terminated
      case .DECLINED:
        return OnoQposTransactionResult.declined
      case .CANCEL:
        return OnoQposTransactionResult.cancel
      case .CAPK_FAIL:
        return OnoQposTransactionResult.capkFail
      case .NOT_ICC:
        return OnoQposTransactionResult.notIcc
      case .SELECT_APP_FAIL:
        return OnoQposTransactionResult.selectAppFail
      case .DEVICE_ERROR:
        return OnoQposTransactionResult.deviceError
      case .CARD_NOT_SUPPORTED:
        return OnoQposTransactionResult.cardNotSupported
      case .MISSING_MANDATORY_DATA:
        return OnoQposTransactionResult.missingMandatoryData
      case .CARD_BLOCKED_OR_NO_EMV_APPS:
        return OnoQposTransactionResult.cardBlockedOrNoEmvApps
      case .INVALID_ICC_DATA:
        return OnoQposTransactionResult.invalidIccData
      case .FALLBACK:
        return OnoQposTransactionResult.fallback
      case .NFC_TERMINATED:
        return OnoQposTransactionResult.nfcTerminated
      // This exists in Android but not in iOS
      // case TransactionResult.CARD_REMOVED:
      //   return OnoQposTransactionResult.cardRemoved
      case .TRADE_LOG_FULL:
        return OnoQposTransactionResult.tradeLogFull
      @unknown default:
        fatalError("convertTransactionResultToOnoTransactionResult \(transactionResult) does not match any OnoQposTransactionResult")
      }
  }
}
