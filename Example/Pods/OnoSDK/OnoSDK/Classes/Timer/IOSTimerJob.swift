//
//  IOSTimerJob.swift
//  YocoPOS
//
//  Created by Altus van der Merwe on 2019/07/25.
//  Copyright © 2019 Yoco. All rights reserved.
//

import Foundation
import Ono

public class IOSTimerJob: ITimerJob {
  var timer: Timer? = nil
  var block: (() -> Void)? = nil
  
  func setupTimer(runAfter: Int64, block: @escaping () -> Void) {
    self.block = block
    timer = Timer.scheduledTimer(timeInterval: TimeInterval(runAfter/1000),
                         target: self,
                         selector: #selector(self.runBlock),
                         userInfo: nil,
                         repeats: false)
  }
  
  public func cancel() {
    self.timer?.invalidate()
  }
  
  @objc func runBlock() {
    if let codeToRun = block {
      codeToRun()
    }
  }
}
