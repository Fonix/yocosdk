//
//  IOSTimerFactory.swift
//  YocoPOS
//
//  Created by Altus van der Merwe on 2019/07/25.
//  Copyright © 2019 Yoco. All rights reserved.
//

import Foundation
import Ono

public class IOSTimerFactory: ITimerFactory {
  
  public init() {
  }
  
  public func startTimer(runAfter: Int64, block: @escaping () -> Void) -> ITimerJob {
    let job = IOSTimerJob()
    job.setupTimer(runAfter: runAfter, block: block)
    return job
  }
}
