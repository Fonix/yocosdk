#import <Foundation/NSArray.h>
#import <Foundation/NSDictionary.h>
#import <Foundation/NSError.h>
#import <Foundation/NSObject.h>
#import <Foundation/NSSet.h>
#import <Foundation/NSString.h>
#import <Foundation/NSValue.h>

@class OnoCommandDriver, OnoStoreListener, OnoOnoApiServiceProtocol, OnoCoreApiServiceProtocol, OnoManufacturer, OnoAction, OnoEnvironment, OnoTLVDecoderServiceProtocol, OnoKotlinThrowable, OnoSensitiveString, OnoTerminalConfig, OnoTransactionInitiator, OnoLogLevel, OnoKotlinEnum, OnoApiHeaderKey, OnoSensitiveMap, OnoOnoAuthorizationBody, OnoRetrievedBody, OnoTerminal, OnoTransaction, OnoOnoChargeTransactionBody, OnoATransaction, OnoQRPaymentInfo, OnoOnoFinalizationBody, OnoOnoRejectByClientIdBody, OnoSignature, OnoOnoSignatureBody, OnoTerminalUpdatesBody, OnoTrackingEventBody, OnoAppInfo, OnoDeviceInfo, OnoOnoMetadata, OnoTerminalModel, OnoTerminalDetails, OnoCardPromptMode, OnoIsoResponseCode, OnoKotlinByteArray, OnoOnoNetworkConnectionInfo, OnoKotlinArray, OnoTransactionState, OnoMockLoggerLogEntry, OnoActionGroup, OnoCommand, OnoStateMachineResult, OnoStore, OnoStateUpdateEvent, OnoKotlinPair, OnoStoreUnitTestHelperTracksLists, OnoActivityTimelineActionsTaskBegan, OnoActivityTimelineActionsEventOccurred, OnoActivityTimelineActionsTaskEnded, OnoSegmentCommandsTrackEvent, OnoActivityTimelineActionsEndAllRunningTasks, OnoMockOnoSdkModuleStateMachineCallData, OnoMockOnoSdkModuleStateMachineActionTypeResponse, OnoMockOnoSdkModuleStateMachineSpecificActionResponse, OnoNetworkConnectionType, OnoSensitiveByteArray, OnoOnoMiuraServiceProtocol, OnoResponseRkiDataId, OnoBluetoothDevice, OnoErrorDetails, OnoOnoMiuraTransactionStep, OnoMiuraActionsAbortTransaction, OnoOnoMiuraTransactionError, OnoMiuraActionsArpcFailed, OnoMiuraActionsArpcProcessResult, OnoMiuraCommandType, OnoMiuraActionsAsyncProcessException, OnoMiuraActionsAwaitingCard, OnoOnoMiuraBatteryStatus, OnoMiuraActionsBatteryStatusInfo, OnoMiuraActionsBluetoothInfo, OnoOnoMiuraCardData, OnoMiuraActionsCardData, OnoMiuraActionsCardInserted, OnoMiuraActionsCardSwiped, OnoMiuraActionsCardTapped, OnoOnoMiuraDeviceCapability, OnoMiuraActionsDeviceCapabilities, OnoMiuraDeviceStatus, OnoMiuraActionsDeviceStatusChange, OnoMiuraActionsEmvFailed, OnoMiuraActionsGetBatteryStatusFailed, OnoMiuraActionsGetBluetoothInfoFailed, OnoMiuraActionsGetDeviceCapabilitiesFailed, OnoMiuraActionsGetFileProgress, OnoMiuraActionsGetP2PEStatusFailed, OnoMiuraActionsGetPEDConfigFailed, OnoMiuraActionsGetSoftwareInfoFailed, OnoMiuraActionsGettingFileFromPed, OnoOnoMiuraButton, OnoMiuraActionsKeyPressed, OnoMiuraActionsKeypadStatus, OnoMiuraActionsMiuraExecutionTimeout, OnoMiuraActionsMiuraServiceBusy, OnoMiuraActionsMiuraStartTimeout, OnoOnoMiuraP2PEStatus, OnoMiuraActionsP2PEStatusInfo, OnoMiuraActionsPEDConfigInfo, OnoMiuraActionsPinDataProvided, OnoMiuraActionsPinEntryFailed, OnoMiuraActionsRkiFiles, OnoOnoMiuraOS, OnoOnoMiuraMPI, OnoMiuraActionsSoftwareInfo, OnoCurrency, OnoMiuraActionsStartTransaction, OnoMiuraActionsStartTransactionFailed, OnoMiuraActionsTerminalConnectionFailed, OnoOnoMiuraText, OnoMiuraActionsTextDisplayFailed, OnoMiuraActionsTextDisplayedSuccessfully, OnoMiuraActionsTipAmountEntered, OnoMiuraActionsTipRequestAborted, OnoMiuraActionsUnknownKeyPressed, OnoMiuraActionsWritingFileToPed, OnoCommandGroup, OnoMiuraCommandsAbortTransaction, OnoPinEnforcedBy, OnoMiuraCommandsAskForPin, OnoMiuraCommandsAskForTipAmount, OnoMiuraCommandsClearAllMiuraCommands, OnoQueueCommand, OnoMiuraCommandsClearMiuraCommand, OnoMiuraCommandsConnectTerminal, OnoMiuraCommandsDelayGetBatteryStatusInfo, OnoMiuraCommandsDelaySetTerminalAvailable, OnoMiuraCommandsDisplayText, OnoMiuraCommandsDoYouWantToTip, OnoMiuraCommandsGetP2PEStatusInfo, OnoMiuraCommandsHardReset, OnoMiuraCommandsInjectKeys, OnoAvailableUpdate, OnoMiuraCommandsPerformUpdate, OnoMiuraCommandsProcessArpc, OnoMiuraCommandsProcessEmvTransaction, OnoMiuraCommandsShowIdleTextOnSchedule, OnoMiuraCommandsStartTransaction, OnoMiuraStateMachineMiuraData, OnoMiuraStateMachineAbortMode, OnoMiuraConnectionStatus, OnoOnoMiuraAccessory, OnoMiuraStateMachineConnectionData, OnoMiuraStateMachineTransactionData, OnoMiuraStateMachineTipPromptSettings, OnoTransactionType, OnoDispatcher, OnoTerminalCommandQueue, OnoMockOnoMiuraServiceFailConnectionMode, OnoMockOnoMiuraServiceMockingPinEntry, OnoMockOnoMiuraServiceMockingPinEntryPinEntryMode, OnoOnoMiuraListenerProtocol, OnoRequestRkiDataId, OnoOnoMiuraCardDataOnoMiuraCardStatus, OnoOnoMiuraCardDataOnoMiuraTrack2Data, OnoTippingSettings, OnoOnoMiuraPinEntryError, OnoTerminalActionsAskForTip, OnoTerminalActionsCancelTransaction, OnoTerminalActionsCheckForUpdates, OnoTerminalActionsConnectTerminal, OnoTerminalActionsConnectingMessageUpdated, OnoTerminalActionsConnectionError, OnoTerminalActionsEMVConfigVersionRetrieved, OnoTerminalActionsFailTransaction, OnoTerminalActionsModelUpdated, OnoTerminalActionsPairTerminal, OnoTerminalActionsRkiFailed, OnoTerminalActionsRkiServerRequest, OnoTerminalActionsRkiServerResponse, OnoTerminalActionsSerialNumberUpdated, OnoTerminalActionsSetTerminalConfig, OnoTerminalActionsStartTransaction, OnoTerminalActionsTerminalAvailable, OnoTerminalActionsTerminalBusyError, OnoConnectionType, OnoTerminalActionsTerminalConnectionTypeCleared, OnoTerminalActionsTerminalDisconnected, OnoTerminalInfo, OnoTerminalActionsTerminalInfoUpdated, OnoTerminalActionsTipAmountValue, OnoTerminalActionsTipRequestFailed, OnoTerminalActionsUpdateFinished, OnoTerminalActionsUpdateTerminalInfo, OnoTerminalActionsUpdatesAvailable, OnoTerminalActionsUpdatesFailed, OnoTerminalCommandsAbortTipEntry, OnoTerminalCommandsActiveTerminalDisconnected, OnoTerminalCommandsConnectTerminal, OnoTerminalCommandsConnectionFailed, OnoTerminalCommandsDisconnectTerminal, OnoTerminalCommandsGetRkiData, OnoTerminalCommandsInitializeTransaction, OnoTerminalCommandsPairTerminal, OnoTerminalCommandsPairingFailed, OnoTerminalCommandsPerformUpdate, OnoTerminalCommandsProcessRkiServerRequest, OnoTerminalCommandsProcessRkiServerResponse, OnoTerminalCommandsRequestTipAmount, OnoTerminalCommandsRkiError, OnoTerminalCommandsRkiSuccessful, OnoTerminalCommandsStartingConnectionFailed, OnoTerminalCommandsTerminalConnected, OnoTerminalCommandsTerminalDisconnected, OnoTerminalCommandsTerminalWithAddressNotFound, OnoTerminalCommandsTipEntered, OnoTerminalCommandsTipEntryFailed, OnoTerminalCommandsUpdateFinished, OnoTransactionInitializationRequest, OnoTerminalStateData, OnoUpdateFile, OnoUpdateInstallationType, OnoTerminalState, OnoTransactionInitializationRequestTipData, OnoActivityTask, OnoActivityEvent, OnoActivityTimeline, OnoActivityTimelineActionsClearAllActivityTiming, OnoActivityTimelineActionsGetCompletedTransactionDebugData, OnoActivityTaskName, OnoActivityEventName, OnoHttp, OnoCoreActionsDeviceAndBusinessAssociationFailed, OnoCoreActionsFetchOnoAuthenticationToken, OnoCoreActionsNullApiInterface, OnoCoreActionsPushTransactionCompleteData, OnoCoreActionsRetryFetchOnoAuthToken, OnoCoreActionsRkiSuccessReported, OnoIntegrationAuthentication, OnoCoreActionsSetAuthentication, OnoCoreActionsSetBaseCoreURL, OnoCoreActionsTerminalAndDeviceAssociationFailed, OnoCoreCommandsAssociateTerminalWithThisDevice, OnoCoreCommandsAssociateThisDeviceWithBusiness, OnoCoreCommandsFetchOnoAuthenticationToken, OnoDebuggingInfo, OnoCoreCommandsTransactionApproved, OnoCoreCommandsTransactionFailed, OnoCoreCommandsUpdateAuthentication, OnoCoreCommandsUpdateBaseURL, OnoCoreStateData, OnoPaymentProcessor, OnoTransactionError, OnoLoggingCommandsSetLogUUID, OnoLoggingCommandsUploadLogs, OnoLoggingCommandsUploadLogsInRange, OnoTLV_TAGS, OnoOnoApiResultAction, OnoOnoActionsAuthorizationFailed, OnoOnoActionsAuthorizationResponse, OnoOnoActionsBackgroundRejectResult, OnoOnoActionsCheckForUpdatesFailed, OnoOnoActionsFetchOnoAuthenticationTokenFailed, OnoOnoActionsFetchedMerchantSecret, OnoFinalizeState, OnoOnoActionsFinalizationFailed, OnoOnoActionsFinalizationResponse, OnoOnoActionsFinalizationTimeoutReached, OnoOnoActionsIdentifyMerchant, OnoOnoActionsNullApiInterface, OnoOnoActionsRkiServerRequestFailed, OnoOnoActionsRkiServerResponse, OnoOnoActionsSetOnoURL, OnoOnoActionsSignatureSubmissionTimeoutReached, OnoOnoActionsSignatureSubmittedSuccessfully, OnoOnoActionsSubmitSignatureFailed, OnoOnoCommandsAuthorizeTransaction, OnoOnoCommandsBackgroundRejectTransaction, OnoOnoCommandsChargeTransaction, OnoOnoCommandsCheckForUpdates, OnoOnoConfirmationBody, OnoOnoCommandsConfirmTransaction, OnoOnoCommandsDelayBackgroundRejectTransaction, OnoOnoCommandsFinalizeTransaction, OnoOnoCommandsPollChargeStatus, OnoOnoCommandsProcessRkiServerRequest, OnoOnoCommandsRejectTransaction, OnoOnoCommandsRejectTransactionByClientId, OnoOnoCommandsRetryRkiServerRequest, OnoOnoCommandsRetryTransactionAuthorization, OnoOnoCommandsServiceNotReady, OnoOnoCommandsSubmitSignature, OnoOnoCommandsUpdateBaseURL, OnoOnoCommandsUpdateMerchantIdentity, OnoOnoCommandsUpdateMerchantSecret, OnoOnoStateData, OnoOnoMetaDataKeys, OnoTransactionActionsAuthorizationResponse, OnoMockOnoApiServiceCompanionMockNetworkOrServerError, OnoTransactionCommandsFinalizeTransaction, OnoTransactionCommandsSubmitSignature, OnoChargeResult, OnoTransactionActionsAbortTransaction, OnoTransactionActionsAuthorizationData, OnoTransactionActionsAuthorizationFailed, OnoAuthorizationResponseData, OnoTransactionActionsAwaitingEMVSelection, OnoTransactionActionsAwaitingPinEntry, OnoTransactionActionsCardPresented, OnoTransactionActionsCheckSwitchFallbackRequired, OnoTransactionActionsEMVAppSelectedOnApp, OnoTransactionActionsFinalizationFailed, OnoTransactionActionsFinalizationResponse, OnoTransactionActionsFinalizationTLV, OnoTransactionActionsFinalizeTransaction, OnoFallbackEntry, OnoTransactionActionsInitializeTransaction, OnoTransactionActionsPinDigitEntered, OnoTransactionActionsReadingCard, OnoTransactionActionsSignatureProvided, OnoTransactionActionsSignatureSubmissionFailed, OnoTransactionActionsTransactionCancelled, OnoTransactionActionsTransactionCompleted, OnoTransactionActionsTransactionDeclinedBySwitch, OnoTransactionActionsTransactionDeclinedByTerminalOrCard, OnoTransactionActionsTransactionErrorOccurred, OnoTransactionActionsTransactionFallback, OnoTransactionActionsTransactionProcessing, OnoTransactionCommandsAbortTransaction, OnoTransactionCommandsAuthorizeTransaction, OnoTransactionCommandsBackgroundRejectTransaction, OnoFinalizationData, OnoTransactionCommandsRetryTransactionAuthorization, OnoTransactionCommandsSelectEMVApplication, OnoTransactionCommandsSignatureRequired, OnoTransactionCommandsStartTransaction, OnoTransactionCommandsStartTransactionFailed, OnoTransactionCommandsSubmitArpc, OnoTransactionCommandsSubmitEMVOption, OnoTransactionCommandsTransactionCompleted, OnoTransactionCommandsTransactionError, OnoTransactionStateData, OnoTLV, OnoPaymentType, OnoCancelled, OnoPinEnforcedByCardServiceCode, OnoPinEnforcedByOnoCardPromptMode, OnoRKIError, OnoSwitchReject, OnoTerminalError, OnoTerminalErrorQueue, OnoTimedOut, OnoTransactionErrorDetails, OnoUnclassified, OnoQRTransactionActionsChargeTransactionFailed, OnoQRTransactionActionsChargeTransactionResponse, OnoQRTransactionActionsFailTransaction, OnoQRTransactionActionsPollTransactionFailed, OnoQRTransactionActionsPollTransactionResponse, OnoQRTransactionActionsStartTransaction, OnoQRTransactionActionsTransactionConfirmationFailed, OnoQRTransactionActionsTransactionRejectionByIdFailed, OnoQRTransactionActionsTransactionRejectionFailed, OnoQRTransaction, OnoQRTransactionCommandsChargeTransaction, OnoQRTransactionCommandsConfirmQrTransaction, OnoQRTransactionCommandsPollChargeStatus, OnoQRTransactionCommandsPresentQrCode, OnoQRTransactionCommandsRejectQrTransaction, OnoQRTransactionCommandsRejectQrTransactionByClientId, OnoQRTransactionCommandsTransactionCompleted, OnoQRTransactionCommandsTransactionStartFailed, OnoQRTransactionData, OnoQRTransactionState, OnoDspreadServiceProtocol, OnoOnoQposDoTradeResult, OnoTransactionConfig, OnoMockDspreadServicePinPrompt, OnoKotlinNothing, OnoOnoQposEmvOption, OnoOnoQposLcdModeAlign, OnoOnoQposTransactionType, OnoOnoQposError, OnoOnoQposDisplay, OnoOnoQposTransactionResult, OnoOnoQposUpdateInformationResult, OnoOnoQposListenerProtocol, OnoQposCommandType, OnoConnectionStatus, OnoTradeMode, OnoTransactionStep, OnoDspreadActionsAuthorizationTLV, OnoDspreadActionsBuzzerStatusSet, OnoDspreadActionsCardPresented, OnoDspreadActionsCardStillExists, OnoDspreadActionsCheckTerminalStatus, OnoDspreadActionsChipFallback, OnoDspreadActionsEmvIccExceptionData, OnoDspreadActionsEnterPin, OnoDspreadActionsFinalizationTLV, OnoDspreadActionsMessageShown, OnoDspreadActionsMessageTimeout, OnoDspreadActionsNfcFallback, OnoDspreadActionsQposError, OnoDspreadActionsQposExecutionTimeout, OnoDspreadActionsQposIdRetrieved, OnoDspreadActionsQposInfoRetrieved, OnoDspreadActionsQposServiceBusy, OnoDspreadActionsQposStartTimeout, OnoDspreadActionsQposTerminalInfoRetrieved, OnoDspreadActionsQposTransactionResult, OnoDspreadActionsRequestEMVApplication, OnoDspreadActionsSwipeRequestPin, OnoDspreadActionsUpdateFinished, OnoDspreadCommandsCancelTrade, OnoDspreadCommandsCheckCardExists, OnoDspreadCommandsClearQposCommand, OnoDspreadCommandsConnectQpos, OnoDspreadCommandsDelayCheckTerminalStatus, OnoDspreadCommandsDiscardNFCBatchData, OnoDspreadCommandsDoEMV, OnoDspreadCommandsDoTrade, OnoDspreadCommandsPerformEMVAppUpdate, OnoDspreadCommandsPerformUpdate, OnoDspreadCommandsReconnectAfterDelay, OnoDspreadCommandsRequestPin, OnoDspreadCommandsResetQpos, OnoDspreadCommandsSetAmount, OnoDspreadCommandsSetBuzzerStatus, OnoDspreadCommandsSetShutdownTime, OnoDspreadCommandsShowMessage, OnoDspreadCommandsSubmitARPC, OnoDspreadCommandsSubmitEMVOption, OnoDspreadData, OnoBluetoothServiceProtocol, OnoBluetoothListenerHelper, OnoBluetoothActionsDeviceConnected, OnoBluetoothActionsDeviceDiscovered, OnoBluetoothActionsDeviceIsPairing, OnoBluetoothActionsDevicePaired, OnoBluetoothActionsDevicePairingFailed, OnoBluetoothState, OnoBluetoothActionsStateChanged, OnoBluetoothActionsUnpairingDevice, OnoBluetoothCommandsAbortDevicePairing, OnoBluetoothCommandsPairDevice, OnoBluetoothCommandsUnpairDevice, OnoBluetoothStateData, OnoBluetoothStatus, OnoBluetoothBondState, OnoBluetoothErrorDetails, OnoKotlinByteIterator, OnoKotlinx_coroutines_coreAtomicDesc, OnoKotlinx_coroutines_corePrepareOp, OnoKotlinx_coroutines_coreAtomicOp, OnoKotlinx_coroutines_coreOpDescriptor, OnoKotlinx_coroutines_coreLinkedListNode, OnoKotlinx_coroutines_coreAbstractAtomicDesc;

@protocol OnoStateMachine, OnoOnoSDKModule, OnoISegmentService, OnoTerminalListener, OnoTransactionListener, OnoQRTransactionListener, OnoKotlinx_coroutines_coreCoroutineExceptionHandler, OnoILogger, OnoITimerFactory, OnoKotlinComparable, OnoKotlinKClass, OnoStoreData, OnoIOnoUpdateService, OnoITimerJob, OnoISDKService, OnoIQueueCommandType, OnoATransactionState, OnoBluetoothListener, OnoKotlinx_coroutines_coreMutex, OnoKotlinCoroutineContext, OnoKotlinCoroutineContextKey, OnoKotlinCoroutineContextElement, OnoKotlinIterator, OnoKotlinKDeclarationContainer, OnoKotlinKAnnotatedElement, OnoKotlinKClassifier, OnoKotlinx_coroutines_coreSelectClause2, OnoKotlinx_coroutines_coreSelectInstance, OnoKotlinSuspendFunction1, OnoKotlinx_coroutines_coreDisposableHandle, OnoKotlinContinuation, OnoKotlinFunction;

NS_ASSUME_NONNULL_BEGIN
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunknown-warning-option"
#pragma clang diagnostic ignored "-Wnullability"

__attribute__((swift_name("KotlinBase")))
@interface OnoBase : NSObject
- (instancetype)init __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
+ (void)initialize __attribute__((objc_requires_super));
@end;

@interface OnoBase (OnoBaseCopying) <NSCopying>
@end;

__attribute__((swift_name("KotlinMutableSet")))
@interface OnoMutableSet<ObjectType> : NSMutableSet<ObjectType>
@end;

__attribute__((swift_name("KotlinMutableDictionary")))
@interface OnoMutableDictionary<KeyType, ObjectType> : NSMutableDictionary<KeyType, ObjectType>
@end;

@interface NSError (NSErrorOnoKotlinException)
@property (readonly) id _Nullable kotlinException;
@end;

__attribute__((swift_name("KotlinNumber")))
@interface OnoNumber : NSNumber
- (instancetype)initWithChar:(char)value __attribute__((unavailable));
- (instancetype)initWithUnsignedChar:(unsigned char)value __attribute__((unavailable));
- (instancetype)initWithShort:(short)value __attribute__((unavailable));
- (instancetype)initWithUnsignedShort:(unsigned short)value __attribute__((unavailable));
- (instancetype)initWithInt:(int)value __attribute__((unavailable));
- (instancetype)initWithUnsignedInt:(unsigned int)value __attribute__((unavailable));
- (instancetype)initWithLong:(long)value __attribute__((unavailable));
- (instancetype)initWithUnsignedLong:(unsigned long)value __attribute__((unavailable));
- (instancetype)initWithLongLong:(long long)value __attribute__((unavailable));
- (instancetype)initWithUnsignedLongLong:(unsigned long long)value __attribute__((unavailable));
- (instancetype)initWithFloat:(float)value __attribute__((unavailable));
- (instancetype)initWithDouble:(double)value __attribute__((unavailable));
- (instancetype)initWithBool:(BOOL)value __attribute__((unavailable));
- (instancetype)initWithInteger:(NSInteger)value __attribute__((unavailable));
- (instancetype)initWithUnsignedInteger:(NSUInteger)value __attribute__((unavailable));
+ (instancetype)numberWithChar:(char)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedChar:(unsigned char)value __attribute__((unavailable));
+ (instancetype)numberWithShort:(short)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedShort:(unsigned short)value __attribute__((unavailable));
+ (instancetype)numberWithInt:(int)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedInt:(unsigned int)value __attribute__((unavailable));
+ (instancetype)numberWithLong:(long)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedLong:(unsigned long)value __attribute__((unavailable));
+ (instancetype)numberWithLongLong:(long long)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedLongLong:(unsigned long long)value __attribute__((unavailable));
+ (instancetype)numberWithFloat:(float)value __attribute__((unavailable));
+ (instancetype)numberWithDouble:(double)value __attribute__((unavailable));
+ (instancetype)numberWithBool:(BOOL)value __attribute__((unavailable));
+ (instancetype)numberWithInteger:(NSInteger)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedInteger:(NSUInteger)value __attribute__((unavailable));
@end;

__attribute__((swift_name("KotlinByte")))
@interface OnoByte : OnoNumber
- (instancetype)initWithChar:(char)value;
+ (instancetype)numberWithChar:(char)value;
@end;

__attribute__((swift_name("KotlinUByte")))
@interface OnoUByte : OnoNumber
- (instancetype)initWithUnsignedChar:(unsigned char)value;
+ (instancetype)numberWithUnsignedChar:(unsigned char)value;
@end;

__attribute__((swift_name("KotlinShort")))
@interface OnoShort : OnoNumber
- (instancetype)initWithShort:(short)value;
+ (instancetype)numberWithShort:(short)value;
@end;

__attribute__((swift_name("KotlinUShort")))
@interface OnoUShort : OnoNumber
- (instancetype)initWithUnsignedShort:(unsigned short)value;
+ (instancetype)numberWithUnsignedShort:(unsigned short)value;
@end;

__attribute__((swift_name("KotlinInt")))
@interface OnoInt : OnoNumber
- (instancetype)initWithInt:(int)value;
+ (instancetype)numberWithInt:(int)value;
@end;

__attribute__((swift_name("KotlinUInt")))
@interface OnoUInt : OnoNumber
- (instancetype)initWithUnsignedInt:(unsigned int)value;
+ (instancetype)numberWithUnsignedInt:(unsigned int)value;
@end;

__attribute__((swift_name("KotlinLong")))
@interface OnoLong : OnoNumber
- (instancetype)initWithLongLong:(long long)value;
+ (instancetype)numberWithLongLong:(long long)value;
@end;

__attribute__((swift_name("KotlinULong")))
@interface OnoULong : OnoNumber
- (instancetype)initWithUnsignedLongLong:(unsigned long long)value;
+ (instancetype)numberWithUnsignedLongLong:(unsigned long long)value;
@end;

__attribute__((swift_name("KotlinFloat")))
@interface OnoFloat : OnoNumber
- (instancetype)initWithFloat:(float)value;
+ (instancetype)numberWithFloat:(float)value;
@end;

__attribute__((swift_name("KotlinDouble")))
@interface OnoDouble : OnoNumber
- (instancetype)initWithDouble:(double)value;
+ (instancetype)numberWithDouble:(double)value;
@end;

__attribute__((swift_name("KotlinBoolean")))
@interface OnoBoolean : OnoNumber
- (instancetype)initWithBool:(BOOL)value;
+ (instancetype)numberWithBool:(BOOL)value;
@end;

__attribute__((swift_name("OnoSDKModule")))
@protocol OnoOnoSDKModule
@required
@property (readonly) NSArray<OnoCommandDriver *> *commandDrivers __attribute__((swift_name("commandDrivers")));
@property (readonly) NSArray<id<OnoStateMachine>> *stateMachines __attribute__((swift_name("stateMachines")));
@property (readonly) NSArray<OnoStoreListener *> *storeListeners __attribute__((swift_name("storeListeners")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoCommonModule")))
@interface OnoOnoCommonModule : OnoBase <OnoOnoSDKModule>
- (instancetype)initWithOnoApiService:(OnoOnoApiServiceProtocol *)onoApiService coreApiService:(OnoCoreApiServiceProtocol *)coreApiService segmentService:(id<OnoISegmentService>)segmentService terminalListener:(id<OnoTerminalListener>)terminalListener transactionListener:(id<OnoTransactionListener>)transactionListener qrTransactionListener:(id<OnoQRTransactionListener>)qrTransactionListener __attribute__((swift_name("init(onoApiService:coreApiService:segmentService:terminalListener:transactionListener:qrTransactionListener:)"))) __attribute__((objc_designated_initializer));
@property (readonly) NSArray<OnoCommandDriver *> *commandDrivers __attribute__((swift_name("commandDrivers")));
@property (readonly) NSArray<id<OnoStateMachine>> *stateMachines __attribute__((swift_name("stateMachines")));
@property (readonly) NSArray<OnoStoreListener *> *storeListeners __attribute__((swift_name("storeListeners")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoSDK")))
@interface OnoOnoSDK : OnoBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)onoSDK __attribute__((swift_name("init()")));
- (void)askForTipTransactionAmountInCents:(int64_t)transactionAmountInCents transactionCurrency:(NSString *)transactionCurrency __attribute__((swift_name("askForTip(transactionAmountInCents:transactionCurrency:)")));
- (void)asyncProcessAsyncExceptionHandler:(id<OnoKotlinx_coroutines_coreCoroutineExceptionHandler>)asyncExceptionHandler codeBlock:(void (^)(void))codeBlock __attribute__((swift_name("asyncProcess(asyncExceptionHandler:codeBlock:)")));
- (void)cancelTransactionMessage:(NSString *)message __attribute__((swift_name("cancelTransaction(message:)")));
- (void)clearCoreAuthenticationToken __attribute__((swift_name("clearCoreAuthenticationToken()")));
- (void)clearOnoMerchantDetails __attribute__((swift_name("clearOnoMerchantDetails()")));
- (void)clearStaleState __attribute__((swift_name("clearStaleState()")));
- (void)connectAnyAvailableTerminal __attribute__((swift_name("connectAnyAvailableTerminal()")));
- (void)connectTerminalAddress:(NSString *)address manufacturer:(OnoManufacturer *)manufacturer __attribute__((swift_name("connectTerminal(address:manufacturer:)")));
- (void)disconnectTerminal __attribute__((swift_name("disconnectTerminal()")));
- (void)emvApplicationSelectedApplicationIdentifier:(int32_t)applicationIdentifier __attribute__((swift_name("emvApplicationSelected(applicationIdentifier:)")));
- (void)failTransactionFailureDescription:(NSString *)failureDescription __attribute__((swift_name("failTransaction(failureDescription:)")));
- (void (^)(OnoAction *))getDispatchDispatcherName:(NSString *)dispatcherName __attribute__((swift_name("getDispatch(dispatcherName:)")));
- (void)initiateRemoteKeyInjection __attribute__((swift_name("initiateRemoteKeyInjection()")));
- (void)instantiateEnvironment:(OnoEnvironment *)environment logger:(id<OnoILogger>)logger onoApiService:(OnoOnoApiServiceProtocol *)onoApiService coreApiService:(OnoCoreApiServiceProtocol *)coreApiService segmentService:(id<OnoISegmentService>)segmentService timerFactory:(id<OnoITimerFactory>)timerFactory tlvDecoderService:(OnoTLVDecoderServiceProtocol *)tlvDecoderService onoSDKModules:(NSArray<id<OnoOnoSDKModule>> *)onoSDKModules doTransactionLogging:(BOOL)doTransactionLogging __attribute__((swift_name("instantiate(environment:logger:onoApiService:coreApiService:segmentService:timerFactory:tlvDecoderService:onoSDKModules:doTransactionLogging:)")));
- (BOOL)isInitialized __attribute__((swift_name("isInitialized()")));
- (void)logInitErrorMaybeError:(OnoKotlinThrowable * _Nullable)maybeError tag:(NSString *)tag __attribute__((swift_name("logInitError(maybeError:tag:)")));
- (NSString *)loggingTagModuleName:(NSString *)moduleName className:(NSString *)className __attribute__((swift_name("loggingTag(moduleName:className:)")));
- (void)pairTerminalAddress:(NSString *)address manufacturer:(OnoManufacturer *)manufacturer __attribute__((swift_name("pairTerminal(address:manufacturer:)")));
- (void)searchForTerminals __attribute__((swift_name("searchForTerminals()")));
- (void)setCoreAuthenticationTokenAuthenticationToken:(OnoSensitiveString *)authenticationToken __attribute__((swift_name("setCoreAuthenticationToken(authenticationToken:)")));
- (void)setCoreBaseURLCoreBaseURL:(NSString *)coreBaseURL __attribute__((swift_name("setCoreBaseURL(coreBaseURL:)")));
- (void)setIntegrationAuthenticationIntegrationSecret:(NSString *)integrationSecret integrationToken:(NSString *)integrationToken __attribute__((swift_name("setIntegrationAuthentication(integrationSecret:integrationToken:)")));
- (void)setListenersTerminalListener:(id<OnoTerminalListener> _Nullable)terminalListener transactionListener:(id<OnoTransactionListener> _Nullable)transactionListener qrTransactionListener:(id<OnoQRTransactionListener> _Nullable)qrTransactionListener __attribute__((swift_name("setListeners(terminalListener:transactionListener:qrTransactionListener:)")));
- (void)setOnoBaseUrlBaseURL:(NSString *)baseURL __attribute__((swift_name("setOnoBaseUrl(baseURL:)")));
- (void)setOnoMerchantDetailsMerchantID:(NSString *)merchantID __attribute__((swift_name("setOnoMerchantDetails(merchantID:)")));
- (void)setQRTransactionListenerQrTransactionListener:(id<OnoQRTransactionListener> _Nullable)qrTransactionListener __attribute__((swift_name("setQRTransactionListener(qrTransactionListener:)")));
- (void)setTerminalConfigManufacturer:(OnoManufacturer *)manufacturer terminalConfig:(OnoTerminalConfig *)terminalConfig __attribute__((swift_name("setTerminalConfig(manufacturer:terminalConfig:)")));
- (void)setTerminalListenerTerminalListener:(id<OnoTerminalListener> _Nullable)terminalListener __attribute__((swift_name("setTerminalListener(terminalListener:)")));
- (void)setTransactionListenerTransactionListener:(id<OnoTransactionListener> _Nullable)transactionListener __attribute__((swift_name("setTransactionListener(transactionListener:)")));
- (void)showPairingDialog __attribute__((swift_name("showPairingDialog()")));
- (void)startQRTransactionTransactionUUID:(NSString *)transactionUUID amountInCents:(int64_t)amountInCents currency:(NSString *)currency initiatedBy:(OnoTransactionInitiator *)initiatedBy __attribute__((swift_name("startQRTransaction(transactionUUID:amountInCents:currency:initiatedBy:)")));
- (void)startTransactionAmountInCents:(int64_t)amountInCents coreTransactionUUID:(NSString *)coreTransactionUUID currency:(NSString *)currency initiatedBy:(OnoTransactionInitiator *)initiatedBy askForTip:(BOOL)askForTip __attribute__((swift_name("startTransaction(amountInCents:coreTransactionUUID:currency:initiatedBy:askForTip:)")));
- (void)stopSearchingForTerminals __attribute__((swift_name("stopSearchingForTerminals()")));
- (void)transactionSignatureSignature:(NSString *)signature mediaType:(NSString *)mediaType encoding:(NSString *)encoding __attribute__((swift_name("transactionSignature(signature:mediaType:encoding:)")));
- (void)unregisterAllListeners __attribute__((swift_name("unregisterAllListeners()")));
- (void)updateAllListenersWithCurrentState __attribute__((swift_name("updateAllListenersWithCurrentState()")));
- (void)uploadLogsLogUUID:(NSString *)logUUID logLevel:(OnoLogLevel *)logLevel __attribute__((swift_name("uploadLogs(logUUID:logLevel:)")));
- (void)uploadLogsInRangeStartDate:(int64_t)startDate endDate:(int64_t)endDate logLevel:(OnoLogLevel *)logLevel __attribute__((swift_name("uploadLogsInRange(startDate:endDate:logLevel:)")));
@property (readonly) NSString *SDK_NAME __attribute__((swift_name("SDK_NAME")));
@property (readonly) NSString *TAG __attribute__((swift_name("TAG")));
@property (readonly) OnoCoreApiServiceProtocol *coreApiService __attribute__((swift_name("coreApiService")));
@property (readonly) OnoEnvironment * _Nullable environment __attribute__((swift_name("environment")));
@property (readonly) OnoOnoApiServiceProtocol *onoApiService __attribute__((swift_name("onoApiService")));
@property (readonly) id<OnoITimerFactory> timerFactory __attribute__((swift_name("timerFactory")));
@property (readonly) OnoTLVDecoderServiceProtocol * _Nullable tlvDecoderService __attribute__((swift_name("tlvDecoderService")));
@end;

__attribute__((swift_name("StoreActions")))
@interface OnoStoreActions : OnoBase
@end;

__attribute__((swift_name("Action")))
@interface OnoAction : OnoBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (NSString *)fullToString __attribute__((swift_name("fullToString()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable enclosingClassName __attribute__((swift_name("enclosingClassName")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StoreActions.ClearStaleState")))
@interface OnoStoreActionsClearStaleState : OnoAction
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StoreActions.InitialState")))
@interface OnoStoreActionsInitialState : OnoAction
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((swift_name("KotlinComparable")))
@protocol OnoKotlinComparable
@required
- (int32_t)compareToOther:(id _Nullable)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((swift_name("KotlinEnum")))
@interface OnoKotlinEnum : OnoBase <OnoKotlinComparable>
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer));
- (int32_t)compareToOther:(OnoKotlinEnum *)other __attribute__((swift_name("compareTo(other:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly) int32_t ordinal __attribute__((swift_name("ordinal")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiHeaderKey")))
@interface OnoApiHeaderKey : OnoKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) OnoApiHeaderKey *coreAuthToken __attribute__((swift_name("coreAuthToken")));
@property (class, readonly) OnoApiHeaderKey *sdkIntegrationSecret __attribute__((swift_name("sdkIntegrationSecret")));
@property (class, readonly) OnoApiHeaderKey *sdkIntegrationToken __attribute__((swift_name("sdkIntegrationToken")));
@property (class, readonly) OnoApiHeaderKey *onoMerchantId __attribute__((swift_name("onoMerchantId")));
@property (class, readonly) OnoApiHeaderKey *onoMerchantToken __attribute__((swift_name("onoMerchantToken")));
@property (class, readonly) OnoApiHeaderKey *xCorrelationId __attribute__((swift_name("xCorrelationId")));
- (int32_t)compareToOther:(OnoApiHeaderKey *)other __attribute__((swift_name("compareTo(other:)")));
@property (readonly) BOOL isSensitive __attribute__((swift_name("isSensitive")));
@property (readonly) NSString *value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoAuthorizationBody")))
@interface OnoOnoAuthorizationBody : OnoBase
- (instancetype)initWithMerchantId:(NSString *)merchantId clientTransactionId:(NSString *)clientTransactionId sdkTransactionId:(NSString *)sdkTransactionId amountInCents:(int64_t)amountInCents currency:(NSString *)currency terminal:(NSDictionary<NSString *, id> *)terminal appInfo:(NSDictionary<NSString *, NSString *> *)appInfo deviceInfo:(NSDictionary<NSString *, NSString *> *)deviceInfo tlv:(OnoSensitiveString * _Nullable)tlv transactionData:(OnoSensitiveMap * _Nullable)transactionData metadata:(NSDictionary<NSString *, id> * _Nullable)metadata attemptedInputModes:(NSArray<NSString *> * _Nullable)attemptedInputModes __attribute__((swift_name("init(merchantId:clientTransactionId:sdkTransactionId:amountInCents:currency:terminal:appInfo:deviceInfo:tlv:transactionData:metadata:attemptedInputModes:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoSensitiveMap * _Nullable)component10 __attribute__((swift_name("component10()")));
- (NSDictionary<NSString *, id> * _Nullable)component11 __attribute__((swift_name("component11()")));
- (NSArray<NSString *> * _Nullable)component12 __attribute__((swift_name("component12()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (int64_t)component4 __attribute__((swift_name("component4()")));
- (NSString *)component5 __attribute__((swift_name("component5()")));
- (NSDictionary<NSString *, id> *)component6 __attribute__((swift_name("component6()")));
- (NSDictionary<NSString *, NSString *> *)component7 __attribute__((swift_name("component7()")));
- (NSDictionary<NSString *, NSString *> *)component8 __attribute__((swift_name("component8()")));
- (OnoSensitiveString * _Nullable)component9 __attribute__((swift_name("component9()")));
- (OnoOnoAuthorizationBody *)doCopyMerchantId:(NSString *)merchantId clientTransactionId:(NSString *)clientTransactionId sdkTransactionId:(NSString *)sdkTransactionId amountInCents:(int64_t)amountInCents currency:(NSString *)currency terminal:(NSDictionary<NSString *, id> *)terminal appInfo:(NSDictionary<NSString *, NSString *> *)appInfo deviceInfo:(NSDictionary<NSString *, NSString *> *)deviceInfo tlv:(OnoSensitiveString * _Nullable)tlv transactionData:(OnoSensitiveMap * _Nullable)transactionData metadata:(NSDictionary<NSString *, id> * _Nullable)metadata attemptedInputModes:(NSArray<NSString *> * _Nullable)attemptedInputModes __attribute__((swift_name("doCopy(merchantId:clientTransactionId:sdkTransactionId:amountInCents:currency:terminal:appInfo:deviceInfo:tlv:transactionData:metadata:attemptedInputModes:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int64_t amountInCents __attribute__((swift_name("amountInCents")));
@property (readonly) NSDictionary<NSString *, NSString *> *appInfo __attribute__((swift_name("appInfo")));
@property (readonly) NSArray<NSString *> * _Nullable attemptedInputModes __attribute__((swift_name("attemptedInputModes")));
@property (readonly) NSString *clientTransactionId __attribute__((swift_name("clientTransactionId")));
@property (readonly) NSString *currency __attribute__((swift_name("currency")));
@property (readonly) NSDictionary<NSString *, NSString *> *deviceInfo __attribute__((swift_name("deviceInfo")));
@property (readonly) NSString *merchantId __attribute__((swift_name("merchantId")));
@property (readonly) NSDictionary<NSString *, id> * _Nullable metadata __attribute__((swift_name("metadata")));
@property (readonly) NSString *sdkTransactionId __attribute__((swift_name("sdkTransactionId")));
@property (readonly) NSDictionary<NSString *, id> *terminal __attribute__((swift_name("terminal")));
@property (readonly) OnoSensitiveString * _Nullable tlv __attribute__((swift_name("tlv")));
@property (readonly) OnoSensitiveMap * _Nullable transactionData __attribute__((swift_name("transactionData")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoAuthorizationBody.Companion")))
@interface OnoOnoAuthorizationBodyCompanion : OnoBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (OnoRetrievedBody *)bodyFromTerminalAndTransactionMerchantId:(NSString *)merchantId terminal:(OnoTerminal *)terminal transaction:(OnoTransaction *)transaction __attribute__((swift_name("bodyFromTerminalAndTransaction(merchantId:terminal:transaction:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoChargeTransactionBody")))
@interface OnoOnoChargeTransactionBody : OnoBase
- (instancetype)initWithMerchantId:(NSString *)merchantId clientTransactionId:(NSString *)clientTransactionId amountInCents:(int64_t)amountInCents currency:(NSString *)currency paymentMethodInfo:(NSDictionary<NSString *, NSString *> *)paymentMethodInfo appInfo:(NSDictionary<NSString *, NSString *> *)appInfo deviceInfo:(NSDictionary<NSString *, NSString *> *)deviceInfo metadata:(NSDictionary<NSString *, id> * _Nullable)metadata __attribute__((swift_name("init(merchantId:clientTransactionId:amountInCents:currency:paymentMethodInfo:appInfo:deviceInfo:metadata:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (int64_t)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (NSDictionary<NSString *, NSString *> *)component5 __attribute__((swift_name("component5()")));
- (NSDictionary<NSString *, NSString *> *)component6 __attribute__((swift_name("component6()")));
- (NSDictionary<NSString *, NSString *> *)component7 __attribute__((swift_name("component7()")));
- (NSDictionary<NSString *, id> * _Nullable)component8 __attribute__((swift_name("component8()")));
- (OnoOnoChargeTransactionBody *)doCopyMerchantId:(NSString *)merchantId clientTransactionId:(NSString *)clientTransactionId amountInCents:(int64_t)amountInCents currency:(NSString *)currency paymentMethodInfo:(NSDictionary<NSString *, NSString *> *)paymentMethodInfo appInfo:(NSDictionary<NSString *, NSString *> *)appInfo deviceInfo:(NSDictionary<NSString *, NSString *> *)deviceInfo metadata:(NSDictionary<NSString *, id> * _Nullable)metadata __attribute__((swift_name("doCopy(merchantId:clientTransactionId:amountInCents:currency:paymentMethodInfo:appInfo:deviceInfo:metadata:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSDictionary<NSString *, id> *)getMappedData __attribute__((swift_name("getMappedData()")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int64_t amountInCents __attribute__((swift_name("amountInCents")));
@property (readonly) NSDictionary<NSString *, NSString *> *appInfo __attribute__((swift_name("appInfo")));
@property (readonly) NSString *clientTransactionId __attribute__((swift_name("clientTransactionId")));
@property (readonly) NSString *currency __attribute__((swift_name("currency")));
@property (readonly) NSDictionary<NSString *, NSString *> *deviceInfo __attribute__((swift_name("deviceInfo")));
@property (readonly) NSString *merchantId __attribute__((swift_name("merchantId")));
@property (readonly) NSDictionary<NSString *, id> * _Nullable metadata __attribute__((swift_name("metadata")));
@property (readonly) NSDictionary<NSString *, NSString *> *paymentMethodInfo __attribute__((swift_name("paymentMethodInfo")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoChargeTransactionBody.Companion")))
@interface OnoOnoChargeTransactionBodyCompanion : OnoBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (OnoOnoChargeTransactionBody *)bodyFromTransactionMerchantId:(NSString *)merchantId transaction:(OnoATransaction *)transaction __attribute__((swift_name("bodyFromTransaction(merchantId:transaction:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoConfirmationBody")))
@interface OnoOnoConfirmationBody : OnoBase
- (instancetype)initWithTransactionId:(NSString *)transactionId sdkTransactionId:(NSString * _Nullable)sdkTransactionId qrPaymentInfo:(OnoQRPaymentInfo *)qrPaymentInfo __attribute__((swift_name("init(transactionId:sdkTransactionId:qrPaymentInfo:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithTransactionId:(NSString *)transactionId sdkTransactionId:(NSString * _Nullable)sdkTransactionId confirmationTLV:(OnoSensitiveString *)confirmationTLV __attribute__((swift_name("init(transactionId:sdkTransactionId:confirmationTLV:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithTransactionId:(NSString *)transactionId sdkTransactionId:(NSString * _Nullable)sdkTransactionId paymentMethodInfo:(NSDictionary<NSString *, NSDictionary<NSString *, NSString *> *> *)paymentMethodInfo __attribute__((swift_name("init(transactionId:sdkTransactionId:paymentMethodInfo:)"))) __attribute__((objc_designated_initializer));
- (NSDictionary<NSString *, id> *)getMappedData __attribute__((swift_name("getMappedData()")));
@property (readonly) NSDictionary<NSString *, NSDictionary<NSString *, NSString *> *> *paymentMethodInfo __attribute__((swift_name("paymentMethodInfo")));
@property (readonly) NSString * _Nullable sdkTransactionId __attribute__((swift_name("sdkTransactionId")));
@property (readonly) NSString *transactionId __attribute__((swift_name("transactionId")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoFinalizationBody")))
@interface OnoOnoFinalizationBody : OnoBase
- (instancetype)initWithTlv:(OnoSensitiveString * _Nullable)tlv transactionId:(NSString *)transactionId sdkTransactionId:(NSString *)sdkTransactionId attempt:(int32_t)attempt __attribute__((swift_name("init(tlv:transactionId:sdkTransactionId:attempt:)"))) __attribute__((objc_designated_initializer));
- (OnoSensitiveString * _Nullable)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (int32_t)component4 __attribute__((swift_name("component4()")));
- (OnoOnoFinalizationBody *)doCopyTlv:(OnoSensitiveString * _Nullable)tlv transactionId:(NSString *)transactionId sdkTransactionId:(NSString *)sdkTransactionId attempt:(int32_t)attempt __attribute__((swift_name("doCopy(tlv:transactionId:sdkTransactionId:attempt:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSDictionary<NSString *, id> *)getMappedData __attribute__((swift_name("getMappedData()")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t attempt __attribute__((swift_name("attempt")));
@property (readonly) NSString *sdkTransactionId __attribute__((swift_name("sdkTransactionId")));
@property (readonly) OnoSensitiveString * _Nullable tlv __attribute__((swift_name("tlv")));
@property (readonly) NSString *transactionId __attribute__((swift_name("transactionId")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoRejectByClientIdBody")))
@interface OnoOnoRejectByClientIdBody : OnoBase
- (instancetype)initWithTransaction:(OnoTransaction *)transaction __attribute__((swift_name("init(transaction:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithSdkTransactionId:(NSString *)sdkTransactionId paymentMethodInfo:(NSDictionary<NSString *, NSString *> *)paymentMethodInfo __attribute__((swift_name("init(sdkTransactionId:paymentMethodInfo:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSDictionary<NSString *, NSString *> *)component2 __attribute__((swift_name("component2()")));
- (OnoOnoRejectByClientIdBody *)doCopySdkTransactionId:(NSString *)sdkTransactionId paymentMethodInfo:(NSDictionary<NSString *, NSString *> *)paymentMethodInfo __attribute__((swift_name("doCopy(sdkTransactionId:paymentMethodInfo:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSDictionary<NSString *, id> *)getMappedData __attribute__((swift_name("getMappedData()")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSDictionary<NSString *, NSString *> *paymentMethodInfo __attribute__((swift_name("paymentMethodInfo")));
@property (readonly) NSString *sdkTransactionId __attribute__((swift_name("sdkTransactionId")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoSignatureBody")))
@interface OnoOnoSignatureBody : OnoBase
- (instancetype)initWithMerchantId:(NSString *)merchantId transactionId:(NSString *)transactionId sdkTransactionId:(NSString *)sdkTransactionId signature:(OnoSignature *)signature attempt:(int32_t)attempt __attribute__((swift_name("init(merchantId:transactionId:sdkTransactionId:signature:attempt:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (OnoSignature *)component4 __attribute__((swift_name("component4()")));
- (int32_t)component5 __attribute__((swift_name("component5()")));
- (OnoOnoSignatureBody *)doCopyMerchantId:(NSString *)merchantId transactionId:(NSString *)transactionId sdkTransactionId:(NSString *)sdkTransactionId signature:(OnoSignature *)signature attempt:(int32_t)attempt __attribute__((swift_name("doCopy(merchantId:transactionId:sdkTransactionId:signature:attempt:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSDictionary<NSString *, id> *)getMappedData __attribute__((swift_name("getMappedData()")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t attempt __attribute__((swift_name("attempt")));
@property (readonly) NSString *merchantId __attribute__((swift_name("merchantId")));
@property (readonly) NSString *sdkTransactionId __attribute__((swift_name("sdkTransactionId")));
@property (readonly) OnoSignature *signature __attribute__((swift_name("signature")));
@property (readonly) NSString *transactionId __attribute__((swift_name("transactionId")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("RetrievedBody")))
@interface OnoRetrievedBody : OnoBase
- (instancetype)initWithBody:(id _Nullable)body error:(NSString * _Nullable)error __attribute__((swift_name("init(body:error:)"))) __attribute__((objc_designated_initializer));
- (id _Nullable)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (OnoRetrievedBody *)doCopyBody:(id _Nullable)body error:(NSString * _Nullable)error __attribute__((swift_name("doCopy(body:error:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) id _Nullable body __attribute__((swift_name("body")));
@property (readonly) NSString * _Nullable error __attribute__((swift_name("error")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalUpdatesBody")))
@interface OnoTerminalUpdatesBody : OnoBase
- (instancetype)initWithInTerminal:(OnoTerminal *)inTerminal __attribute__((swift_name("init(inTerminal:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithTerminal:(NSDictionary<NSString *, id> *)terminal appInfo:(NSDictionary<NSString *, NSString *> *)appInfo __attribute__((swift_name("init(terminal:appInfo:)"))) __attribute__((objc_designated_initializer));
- (NSDictionary<NSString *, id> *)component1 __attribute__((swift_name("component1()")));
- (NSDictionary<NSString *, NSString *> *)component2 __attribute__((swift_name("component2()")));
- (OnoTerminalUpdatesBody *)doCopyTerminal:(NSDictionary<NSString *, id> *)terminal appInfo:(NSDictionary<NSString *, NSString *> *)appInfo __attribute__((swift_name("doCopy(terminal:appInfo:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSDictionary<NSString *, id> *)getMappedData __attribute__((swift_name("getMappedData()")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSDictionary<NSString *, NSString *> *appInfo __attribute__((swift_name("appInfo")));
@property (readonly) NSDictionary<NSString *, id> *terminal __attribute__((swift_name("terminal")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TrackingEventBody")))
@interface OnoTrackingEventBody : OnoBase
- (instancetype)initWithName:(NSString *)name meta:(NSDictionary<NSString *, NSString *> *)meta category:(NSString *)category __attribute__((swift_name("init(name:meta:category:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSDictionary<NSString *, NSString *> *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (OnoTrackingEventBody *)doCopyName:(NSString *)name meta:(NSDictionary<NSString *, NSString *> *)meta category:(NSString *)category __attribute__((swift_name("doCopy(name:meta:category:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *category __attribute__((swift_name("category")));
@property (readonly) NSDictionary<NSString *, NSString *> *meta __attribute__((swift_name("meta")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AppInfo")))
@interface OnoAppInfo : OnoBase
- (instancetype)initWithAppName:(NSString *)appName appVersion:(NSString *)appVersion sdkName:(NSString *)sdkName sdkVersion:(NSString *)sdkVersion __attribute__((swift_name("init(appName:appVersion:sdkName:sdkVersion:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (OnoAppInfo *)doCopyAppName:(NSString *)appName appVersion:(NSString *)appVersion sdkName:(NSString *)sdkName sdkVersion:(NSString *)sdkVersion __attribute__((swift_name("doCopy(appName:appVersion:sdkName:sdkVersion:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSDictionary<NSString *, NSString *> *)getMappedData __attribute__((swift_name("getMappedData()")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *appName __attribute__((swift_name("appName")));
@property (readonly) NSString *appVersion __attribute__((swift_name("appVersion")));
@property (readonly) NSString *sdkName __attribute__((swift_name("sdkName")));
@property (readonly) NSString *sdkVersion __attribute__((swift_name("sdkVersion")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AppInfo.Companion")))
@interface OnoAppInfoCompanion : OnoBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (OnoAppInfo *)construct __attribute__((swift_name("construct()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DeviceInfo")))
@interface OnoDeviceInfo : OnoBase
- (instancetype)initWithManufacturer:(NSString *)manufacturer model:(NSString *)model os:(NSString *)os osVersion:(NSString *)osVersion __attribute__((swift_name("init(manufacturer:model:os:osVersion:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (OnoDeviceInfo *)doCopyManufacturer:(NSString *)manufacturer model:(NSString *)model os:(NSString *)os osVersion:(NSString *)osVersion __attribute__((swift_name("doCopy(manufacturer:model:os:osVersion:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSDictionary<NSString *, NSString *> *)getMappedData __attribute__((swift_name("getMappedData()")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *manufacturer __attribute__((swift_name("manufacturer")));
@property (readonly) NSString *model __attribute__((swift_name("model")));
@property (readonly) NSString *os __attribute__((swift_name("os")));
@property (readonly) NSString *osVersion __attribute__((swift_name("osVersion")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DeviceInfo.Companion")))
@interface OnoDeviceInfoCompanion : OnoBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (OnoDeviceInfo *)construct __attribute__((swift_name("construct()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoMetadata")))
@interface OnoOnoMetadata : OnoBase
- (instancetype)initWithTransaction:(OnoATransaction *)transaction __attribute__((swift_name("init(transaction:)"))) __attribute__((objc_designated_initializer));
- (OnoATransaction *)component1 __attribute__((swift_name("component1()")));
- (OnoOnoMetadata *)doCopyTransaction:(OnoATransaction *)transaction __attribute__((swift_name("doCopy(transaction:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSDictionary<NSString *, id> *)getMappedData __attribute__((swift_name("getMappedData()")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoATransaction *transaction __attribute__((swift_name("transaction")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalDetails")))
@interface OnoTerminalDetails : OnoBase
- (instancetype)initWithTerminal:(OnoTerminal *)terminal __attribute__((swift_name("init(terminal:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithSerialNumber:(NSString *)serialNumber terminalModel:(OnoTerminalModel *)terminalModel terminalSdkVersion:(NSString * _Nullable)terminalSdkVersion firmwareVersion:(NSString * _Nullable)firmwareVersion bootloaderVersion:(NSString * _Nullable)bootloaderVersion emvConfigVersion:(NSString * _Nullable)emvConfigVersion versions:(NSDictionary<NSString *, id> * _Nullable)versions batteryPercentage:(OnoInt * _Nullable)batteryPercentage __attribute__((swift_name("init(serialNumber:terminalModel:terminalSdkVersion:firmwareVersion:bootloaderVersion:emvConfigVersion:versions:batteryPercentage:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoTerminalModel *)component2 __attribute__((swift_name("component2()")));
- (NSString * _Nullable)component3 __attribute__((swift_name("component3()")));
- (NSString * _Nullable)component4 __attribute__((swift_name("component4()")));
- (NSString * _Nullable)component5 __attribute__((swift_name("component5()")));
- (NSString * _Nullable)component6 __attribute__((swift_name("component6()")));
- (NSDictionary<NSString *, id> * _Nullable)component7 __attribute__((swift_name("component7()")));
- (OnoInt * _Nullable)component8 __attribute__((swift_name("component8()")));
- (OnoTerminalDetails *)doCopySerialNumber:(NSString *)serialNumber terminalModel:(OnoTerminalModel *)terminalModel terminalSdkVersion:(NSString * _Nullable)terminalSdkVersion firmwareVersion:(NSString * _Nullable)firmwareVersion bootloaderVersion:(NSString * _Nullable)bootloaderVersion emvConfigVersion:(NSString * _Nullable)emvConfigVersion versions:(NSDictionary<NSString *, id> * _Nullable)versions batteryPercentage:(OnoInt * _Nullable)batteryPercentage __attribute__((swift_name("doCopy(serialNumber:terminalModel:terminalSdkVersion:firmwareVersion:bootloaderVersion:emvConfigVersion:versions:batteryPercentage:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSDictionary<NSString *, id> *)getMappedDataIncludeVersions:(BOOL)includeVersions __attribute__((swift_name("getMappedData(includeVersions:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoInt * _Nullable batteryPercentage __attribute__((swift_name("batteryPercentage")));
@property (readonly) NSString * _Nullable bootloaderVersion __attribute__((swift_name("bootloaderVersion")));
@property (readonly) NSString * _Nullable emvConfigVersion __attribute__((swift_name("emvConfigVersion")));
@property (readonly) NSString * _Nullable firmwareVersion __attribute__((swift_name("firmwareVersion")));
@property (readonly) NSString *serialNumber __attribute__((swift_name("serialNumber")));
@property (readonly) OnoTerminalModel *terminalModel __attribute__((swift_name("terminalModel")));
@property (readonly) NSString * _Nullable terminalSdkVersion __attribute__((swift_name("terminalSdkVersion")));
@property (readonly) NSDictionary<NSString *, id> * _Nullable versions __attribute__((swift_name("versions")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Messages")))
@interface OnoMessages : OnoBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)messages __attribute__((swift_name("init()")));
- (NSString *)authorizationFailedIsoCode:(NSString * _Nullable)isoCode __attribute__((swift_name("authorizationFailed(isoCode:)")));
- (NSString *)chipInsertNotAllowedCardPromptMode:(OnoCardPromptMode *)cardPromptMode __attribute__((swift_name("chipInsertNotAllowed(cardPromptMode:)")));
- (NSString *)confirmationResponseForUnapprovedChargeChargeResult:(NSString * _Nullable)chargeResult __attribute__((swift_name("confirmationResponseForUnapprovedCharge(chargeResult:)")));
- (NSString *)forcedUpdateFailedError:(NSString *)error __attribute__((swift_name("forcedUpdateFailed(error:)")));
- (NSString *)internalServerErrorErrorMessage:(NSString * _Nullable)errorMessage __attribute__((swift_name("internalServerError(errorMessage:)")));
- (NSString *)invalidAmountValueAmountInCents:(int64_t)amountInCents __attribute__((swift_name("invalidAmountValue(amountInCents:)")));
- (NSString *)invalidCurrencyProvidedCurrency:(NSString *)currency __attribute__((swift_name("invalidCurrencyProvided(currency:)")));
- (NSString *)networkErrorErrorMessage:(NSString * _Nullable)errorMessage __attribute__((swift_name("networkError(errorMessage:)")));
- (NSString *)onoChargeErrorChargeResultMessage:(NSString * _Nullable)chargeResultMessage __attribute__((swift_name("onoChargeError(chargeResultMessage:)")));
- (NSString *)onoSwitchFallbackCodeReoccurringIsoResponseCode:(OnoIsoResponseCode *)isoResponseCode __attribute__((swift_name("onoSwitchFallbackCodeReoccurring(isoResponseCode:)")));
- (NSString *)onoSwitchFallbackNotAllowedFromChipIsoCode:(OnoIsoResponseCode * _Nullable)isoCode __attribute__((swift_name("onoSwitchFallbackNotAllowedFromChip(isoCode:)")));
- (NSString *)removeCardAndTryAgainUsingCardPromptMode:(OnoCardPromptMode *)cardPromptMode __attribute__((swift_name("removeCardAndTryAgainUsing(cardPromptMode:)")));
- (NSString *)rkiFailedErrorError:(NSString *)error __attribute__((swift_name("rkiFailedError(error:)")));
- (NSString *)sdkInitLockFailedThrowable:(OnoKotlinThrowable *)throwable __attribute__((swift_name("sdkInitLockFailed(throwable:)")));
- (NSString *)sdkInitializedSuccessfullyStartInitTime:(int64_t)startInitTime startInLock:(int64_t)startInLock endInitTime:(int64_t)endInitTime __attribute__((swift_name("sdkInitializedSuccessfully(startInitTime:startInLock:endInitTime:)")));
- (NSString *)swipeNotAllowedCardPromptMode:(OnoCardPromptMode *)cardPromptMode __attribute__((swift_name("swipeNotAllowed(cardPromptMode:)")));
- (NSString *)terminalNotWhitelistedSerialNumber:(NSString * _Nullable)serialNumber __attribute__((swift_name("terminalNotWhitelisted(serialNumber:)")));
- (NSString *)unknownChargeResultChargeResult:(NSString *)chargeResult chargeResultMessage:(NSString * _Nullable)chargeResultMessage __attribute__((swift_name("unknownChargeResult(chargeResult:chargeResultMessage:)")));
- (NSString *)unsupportedCardPromptModeCardPromptMode:(OnoCardPromptMode *)cardPromptMode model:(OnoTerminalModel *)model __attribute__((swift_name("unsupportedCardPromptMode(cardPromptMode:model:)")));
@property (readonly) NSString *ABORTING_TIP_REQUEST __attribute__((swift_name("ABORTING_TIP_REQUEST")));
@property (readonly) NSString *APP_SELECTION_FAILED __attribute__((swift_name("APP_SELECTION_FAILED")));
@property (readonly) NSString *APP_SELECTION_TERMINATED __attribute__((swift_name("APP_SELECTION_TERMINATED")));
@property (readonly) NSString *APP_SELECTION_TIMEOUT_ERROR __attribute__((swift_name("APP_SELECTION_TIMEOUT_ERROR")));
@property (readonly) NSString *ARPC_REQUIRED_BUT_MISSING __attribute__((swift_name("ARPC_REQUIRED_BUT_MISSING")));
@property (readonly) NSString *AUTHORIZATION_FAILED __attribute__((swift_name("AUTHORIZATION_FAILED")));
@property (readonly) NSString *AWAITING_CARD_TIMEOUT_PLEASE_RETRY __attribute__((swift_name("AWAITING_CARD_TIMEOUT_PLEASE_RETRY")));
@property (readonly) NSString *BLANK_SIGNATURE_PROVIDED __attribute__((swift_name("BLANK_SIGNATURE_PROVIDED")));
@property (readonly) NSString *BLUETOOTH_DISABLED __attribute__((swift_name("BLUETOOTH_DISABLED")));
@property (readonly) NSString *CARDHOLDER_CANCELLED_TIPPING __attribute__((swift_name("CARDHOLDER_CANCELLED_TIPPING")));
@property (readonly) NSString *CARDHOLDER_DENIED_TIPPING __attribute__((swift_name("CARDHOLDER_DENIED_TIPPING")));
@property (readonly) NSString *CARD_NOT_PRESENTED_IN_TIME __attribute__((swift_name("CARD_NOT_PRESENTED_IN_TIME")));
@property (readonly) NSString *CARD_NOT_SUPPORTED __attribute__((swift_name("CARD_NOT_SUPPORTED")));
@property (readonly) NSString *CARD_REMOVED_ERROR __attribute__((swift_name("CARD_REMOVED_ERROR")));
@property (readonly) NSString *CARD_SWIPED_PIN_PROMPT __attribute__((swift_name("CARD_SWIPED_PIN_PROMPT")));
@property (readonly) NSString *CARD_WAS_REMOVED __attribute__((swift_name("CARD_WAS_REMOVED")));
@property (readonly) NSString *CHIP_CARD_SWIPED __attribute__((swift_name("CHIP_CARD_SWIPED")));
@property (readonly) NSString *CHIP_READ_FAILED __attribute__((swift_name("CHIP_READ_FAILED")));
@property (readonly) NSString *CONFIRMATION_FAILED __attribute__((swift_name("CONFIRMATION_FAILED")));
@property (readonly) NSString *CONNECTION_REFUSED __attribute__((swift_name("CONNECTION_REFUSED")));
@property (readonly) NSString *CONNECT_FAILED __attribute__((swift_name("CONNECT_FAILED")));
@property (readonly) NSString *COULD_NOT_CONNECT_TO_SERVER __attribute__((swift_name("COULD_NOT_CONNECT_TO_SERVER")));
@property (readonly) NSString *COULD_NOT_READ_CARD __attribute__((swift_name("COULD_NOT_READ_CARD")));
@property (readonly) NSString *DEVICE_NOT_PAIRED __attribute__((swift_name("DEVICE_NOT_PAIRED")));
@property (readonly) NSString *DO_CHIP_NOT_NFC __attribute__((swift_name("DO_CHIP_NOT_NFC")));
@property (readonly) NSString *ENTER_PIN __attribute__((swift_name("ENTER_PIN")));
@property (readonly) NSString *FAILED_TO_ENABLE_KEYPAD __attribute__((swift_name("FAILED_TO_ENABLE_KEYPAD")));
@property (readonly) NSString *FAIL_TRANSACTION_ACTION __attribute__((swift_name("FAIL_TRANSACTION_ACTION")));
@property (readonly) NSString *GENERIC_TIP_ERROR __attribute__((swift_name("GENERIC_TIP_ERROR")));
@property (readonly) NSString *GOT_QR_FOR_CANCELLED_TRANSACTION __attribute__((swift_name("GOT_QR_FOR_CANCELLED_TRANSACTION")));
@property (readonly) NSString *INVALID_CLIENT_TRANSACTION_ID __attribute__((swift_name("INVALID_CLIENT_TRANSACTION_ID")));
@property (readonly) NSString *ISO_CODE_AND_CHARGE_RESULT_MISMATCH __attribute__((swift_name("ISO_CODE_AND_CHARGE_RESULT_MISMATCH")));
@property (readonly) NSString *MASKED_PAN_NOT_RECEIVED __attribute__((swift_name("MASKED_PAN_NOT_RECEIVED")));
@property (readonly) NSString *MERCHANT_CANCELLED_TIPPING __attribute__((swift_name("MERCHANT_CANCELLED_TIPPING")));
@property (readonly) NSString *MERCHANT_NOT_AUTHORIZED __attribute__((swift_name("MERCHANT_NOT_AUTHORIZED")));
@property (readonly) NSString *NEGATIVE_AMOUNT_FORBIDDEN __attribute__((swift_name("NEGATIVE_AMOUNT_FORBIDDEN")));
@property (readonly) NSString *NEGATIVE_TIP_AMOUNT __attribute__((swift_name("NEGATIVE_TIP_AMOUNT")));
@property (readonly) NSString *NFC_DECLINED_TRY_CHIP __attribute__((swift_name("NFC_DECLINED_TRY_CHIP")));
@property (readonly) NSString *NFC_READ_FAILED __attribute__((swift_name("NFC_READ_FAILED")));
@property (readonly) NSString *NFC_TRY_ANOTHER_CARD __attribute__((swift_name("NFC_TRY_ANOTHER_CARD")));
@property (readonly) NSString *NO_AUTHORIZATION_RESPONSE __attribute__((swift_name("NO_AUTHORIZATION_RESPONSE")));
@property (readonly) NSString *NO_CARD_DATA __attribute__((swift_name("NO_CARD_DATA")));
@property (readonly) NSString *NO_FINALIZATION_DATA __attribute__((swift_name("NO_FINALIZATION_DATA")));
@property (readonly) NSString *NO_INTERNET_CONNECTION __attribute__((swift_name("NO_INTERNET_CONNECTION")));
@property (readonly) NSString *NO_PIN_DATA __attribute__((swift_name("NO_PIN_DATA")));
@property (readonly) NSString *NO_PIN_KSN __attribute__((swift_name("NO_PIN_KSN")));
@property (readonly) NSString *NO_QPOS_DETECTED __attribute__((swift_name("NO_QPOS_DETECTED")));
@property (readonly) NSString *NO_RESPONSE_FROM_SERVER __attribute__((swift_name("NO_RESPONSE_FROM_SERVER")));
@property (readonly) NSString *NO_SIGNATURE_PROVIDED __attribute__((swift_name("NO_SIGNATURE_PROVIDED")));
@property (readonly) NSString *NO_SWITCH_TRANSACTION_ID __attribute__((swift_name("NO_SWITCH_TRANSACTION_ID")));
@property (readonly) NSString *NO_TERMINAL_ASSOCIATED_WITH_TRANSACTION __attribute__((swift_name("NO_TERMINAL_ASSOCIATED_WITH_TRANSACTION")));
@property (readonly) NSString *NO_TRACK_2_DATA __attribute__((swift_name("NO_TRACK_2_DATA")));
@property (readonly) NSString *NO_TRANSACTION __attribute__((swift_name("NO_TRANSACTION")));
@property (readonly) NSString *NO_TRANSACTION_DATA __attribute__((swift_name("NO_TRANSACTION_DATA")));
@property (readonly) NSString *NULL_API_INTERFACE __attribute__((swift_name("NULL_API_INTERFACE")));
@property (readonly) NSString *NULL_ONO_AUTH_TOKEN_FROM_CORE __attribute__((swift_name("NULL_ONO_AUTH_TOKEN_FROM_CORE")));
@property (readonly) NSString *NULL_ONO_MERCHANT_ID __attribute__((swift_name("NULL_ONO_MERCHANT_ID")));
@property (readonly) NSString *NULL_TERMINAL __attribute__((swift_name("NULL_TERMINAL")));
@property (readonly) NSString *PIN_TIMEOUT_ERROR __attribute__((swift_name("PIN_TIMEOUT_ERROR")));
@property (readonly) NSString *POLL_RESPONSE_FOR_UNKNOWN_TRANSACTION __attribute__((swift_name("POLL_RESPONSE_FOR_UNKNOWN_TRANSACTION")));
@property (readonly) NSString *PROCESSING_PAYMENT __attribute__((swift_name("PROCESSING_PAYMENT")));
@property (readonly) NSString *QPOS_TRADE_DATA_HAS_NULL_MASKED_PAN __attribute__((swift_name("QPOS_TRADE_DATA_HAS_NULL_MASKED_PAN")));
@property (readonly) NSString *QPOS_TRADE_DATA_WAS_NULL __attribute__((swift_name("QPOS_TRADE_DATA_WAS_NULL")));
@property (readonly) NSString *QPOS_TRADE_RESULT_IS_NULL __attribute__((swift_name("QPOS_TRADE_RESULT_IS_NULL")));
@property (readonly) NSString *QR_TRANSACTION_REJECTED __attribute__((swift_name("QR_TRANSACTION_REJECTED")));
@property (readonly) NSString *REJECTION_FAILED __attribute__((swift_name("REJECTION_FAILED")));
@property (readonly) NSString *REMOVE_CARD __attribute__((swift_name("REMOVE_CARD")));
@property (readonly) NSString *REPAIR_TERMINAL __attribute__((swift_name("REPAIR_TERMINAL")));
@property (readonly) NSString *REQUESTING_TIP __attribute__((swift_name("REQUESTING_TIP")));
@property (readonly) NSString *REVERSE_TRANSACTION_SIGNATURE_CAPTURE_CANCELLED __attribute__((swift_name("REVERSE_TRANSACTION_SIGNATURE_CAPTURE_CANCELLED")));
@property (readonly) NSString *SDK_ALREADY_INSTANTIATED __attribute__((swift_name("SDK_ALREADY_INSTANTIATED")));
@property (readonly) NSString *TERMINAL_BAD_STATE __attribute__((swift_name("TERMINAL_BAD_STATE")));
@property (readonly) NSString *TERMINAL_DISCONNECTED __attribute__((swift_name("TERMINAL_DISCONNECTED")));
@property (readonly) NSString *TERMINAL_PROCESSING __attribute__((swift_name("TERMINAL_PROCESSING")));
@property (readonly) NSString *TERMINAL_SIGNATURE_REQUIRED __attribute__((swift_name("TERMINAL_SIGNATURE_REQUIRED")));
@property (readonly) NSString *TERMINAL_TIMEOUT_OCCURRED __attribute__((swift_name("TERMINAL_TIMEOUT_OCCURRED")));
@property (readonly) NSString *TERMINAL_UPDATE_FAILED __attribute__((swift_name("TERMINAL_UPDATE_FAILED")));
@property (readonly) NSString *TIP_ENTRY_ON_TRAN_FLOW __attribute__((swift_name("TIP_ENTRY_ON_TRAN_FLOW")));
@property (readonly) NSString *TRANSACTION_ALREADY_COMPLETED __attribute__((swift_name("TRANSACTION_ALREADY_COMPLETED")));
@property (readonly) NSString *TRANSACTION_CANCELLED __attribute__((swift_name("TRANSACTION_CANCELLED")));
@property (readonly) NSString *TRANSACTION_CANCELLED_FROM_APP __attribute__((swift_name("TRANSACTION_CANCELLED_FROM_APP")));
@property (readonly) NSString *TRANSACTION_CANCELLED_ON_TERMINAL __attribute__((swift_name("TRANSACTION_CANCELLED_ON_TERMINAL")));
@property (readonly) NSString *TRANSACTION_CHARGE_ERROR __attribute__((swift_name("TRANSACTION_CHARGE_ERROR")));
@property (readonly) NSString *TRANSACTION_DECLINED __attribute__((swift_name("TRANSACTION_DECLINED")));
@property (readonly) NSString *TRANSACTION_DECLINED_BY_CARD_OR_TERMINAL __attribute__((swift_name("TRANSACTION_DECLINED_BY_CARD_OR_TERMINAL")));
@property (readonly) NSString *TRANSACTION_INFLIGHT __attribute__((swift_name("TRANSACTION_INFLIGHT")));
@property (readonly) NSString *TRANSACTION_INFO_MISSING __attribute__((swift_name("TRANSACTION_INFO_MISSING")));
@property (readonly) NSString *TRANSACTION_INITIALIZED __attribute__((swift_name("TRANSACTION_INITIALIZED")));
@property (readonly) NSString *TRANSACTION_IS_FINALIZING __attribute__((swift_name("TRANSACTION_IS_FINALIZING")));
@property (readonly) NSString *TRANSACTION_NOT_IN_EXPECTED_STATE __attribute__((swift_name("TRANSACTION_NOT_IN_EXPECTED_STATE")));
@property (readonly) NSString *TRANSACTION_STUCK_IN_PENDING_STATE __attribute__((swift_name("TRANSACTION_STUCK_IN_PENDING_STATE")));
@property (readonly) NSString *TRANSACTION_TERMINATED __attribute__((swift_name("TRANSACTION_TERMINATED")));
@property (readonly) NSString *UNABLE_TO_REVERSE_TRANSACTION_SIGNATURE_CAPTURE_CANCELLED __attribute__((swift_name("UNABLE_TO_REVERSE_TRANSACTION_SIGNATURE_CAPTURE_CANCELLED")));
@property (readonly) NSString *UNKNOWN_CONNECTION_ERROR_OCCURRED __attribute__((swift_name("UNKNOWN_CONNECTION_ERROR_OCCURRED")));
@property (readonly) NSString *UNKNOWN_ERROR_OCCURRED __attribute__((swift_name("UNKNOWN_ERROR_OCCURRED")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoSdkUtils")))
@interface OnoOnoSdkUtils : OnoBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)onoSdkUtils __attribute__((swift_name("init()")));
- (OnoKotlinByteArray *)asciiStringToByteArrayString:(NSString *)string __attribute__((swift_name("asciiStringToByteArray(string:)")));
- (NSString *)byteArrayToAsciiStringByteArray:(OnoKotlinByteArray *)byteArray __attribute__((swift_name("byteArrayToAsciiString(byteArray:)")));
- (NSString *)byteArrayToHexStringByteArray:(OnoKotlinByteArray *)byteArray __attribute__((swift_name("byteArrayToHexString(byteArray:)")));
- (NSString *)getNetworkRequestErrorMessageStatusCode:(OnoInt * _Nullable)statusCode errorMessage:(NSString * _Nullable)errorMessage __attribute__((swift_name("getNetworkRequestErrorMessage(statusCode:errorMessage:)")));
- (OnoKotlinByteArray *)hexStringToByteArrayHexString:(NSString *)hexString __attribute__((swift_name("hexStringToByteArray(hexString:)")));
- (int32_t)rejectAttemptDelayInSecondAttempt:(int32_t)attempt __attribute__((swift_name("rejectAttemptDelayInSecond(attempt:)")));
@end;

__attribute__((swift_name("ILogger")))
@protocol OnoILogger
@required
- (BOOL)checkExistsClientBillIdentifier:(NSString *)clientBillIdentifier __attribute__((swift_name("checkExists(clientBillIdentifier:)")));
- (void)doInternalLogLogLevel:(OnoLogLevel *)logLevel tag:(NSString *)tag message:(NSString *)message __attribute__((swift_name("doInternalLog(logLevel:tag:message:)")));
- (OnoOnoNetworkConnectionInfo *)getConnectionInfo __attribute__((swift_name("getConnectionInfo()")));
- (int64_t)getCurrentTimeMillis __attribute__((swift_name("getCurrentTimeMillis()")));
- (NSDictionary<NSString *, id> *)getGlobalMetadata __attribute__((swift_name("getGlobalMetadata()")));
- (void)logMessageLogLevel:(OnoLogLevel *)logLevel message:(NSString *)message metadata:(NSDictionary<id, id> * _Nullable)metadata component:(NSString * _Nullable)component __attribute__((swift_name("logMessage(logLevel:message:metadata:component:)")));
- (void)setClientBillIdentifierClientBillIdentifier:(NSString *)clientBillIdentifier __attribute__((swift_name("setClientBillIdentifier(clientBillIdentifier:)")));
- (id)setMetaValueKey:(NSString *)key value:(id)value __attribute__((swift_name("setMetaValue(key:value:)")));
- (BOOL)uploadLogFileClientBillIdentifier:(NSString *)clientBillIdentifier minLogLevel:(int32_t)minLogLevel excludeComponents:(OnoKotlinArray * _Nullable)excludeComponents excludeRegex:(NSString * _Nullable)excludeRegex __attribute__((swift_name("uploadLogFile(clientBillIdentifier:minLogLevel:excludeComponents:excludeRegex:)")));
- (void)uploadLogsInRangeStartDate:(int64_t)startDate endDate:(int64_t)endDate logLevel:(int32_t)logLevel __attribute__((swift_name("uploadLogsInRange(startDate:endDate:logLevel:)")));
@property NSString * _Nullable clientTransactionUUID __attribute__((swift_name("clientTransactionUUID")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ILoggerMetaValue")))
@interface OnoILoggerMetaValue : OnoBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)metaValue __attribute__((swift_name("init()")));
@property (readonly) NSString *KEY_APP_VERSION __attribute__((swift_name("KEY_APP_VERSION")));
@property (readonly) NSString *KEY_DEVICE_IDENTIFIER __attribute__((swift_name("KEY_DEVICE_IDENTIFIER")));
@property (readonly) NSString *KEY_DEVICE_MODEL __attribute__((swift_name("KEY_DEVICE_MODEL")));
@property (readonly) NSString *KEY_MAKE __attribute__((swift_name("KEY_MAKE")));
@property (readonly) NSString *KEY_OS_NAME __attribute__((swift_name("KEY_OS_NAME")));
@property (readonly) NSString *KEY_OS_VERSION __attribute__((swift_name("KEY_OS_VERSION")));
@property (readonly) NSString *KEY_SDK_VERSION __attribute__((swift_name("KEY_SDK_VERSION")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LogLevel")))
@interface OnoLogLevel : OnoKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) OnoLogLevel *trace __attribute__((swift_name("trace")));
@property (class, readonly) OnoLogLevel *debug __attribute__((swift_name("debug")));
@property (class, readonly) OnoLogLevel *info __attribute__((swift_name("info")));
@property (class, readonly) OnoLogLevel *warn __attribute__((swift_name("warn")));
@property (class, readonly) OnoLogLevel *error __attribute__((swift_name("error")));
- (int32_t)compareToOther:(OnoLogLevel *)other __attribute__((swift_name("compareTo(other:)")));
@property (readonly) int32_t value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LogLevel.Companion")))
@interface OnoLogLevelCompanion : OnoBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (OnoLogLevel *)logLevelForTransactionStateTransactionState:(OnoTransactionState *)transactionState __attribute__((swift_name("logLevelForTransactionState(transactionState:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Logger")))
@interface OnoLogger : OnoBase <OnoILogger>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)logger __attribute__((swift_name("init()")));
- (BOOL)checkExistsClientBillIdentifier:(NSString *)clientBillIdentifier __attribute__((swift_name("checkExists(clientBillIdentifier:)")));
- (void)dTag:(NSString *)tag message:(NSString *)message __attribute__((swift_name("d(tag:message:)")));
- (void)dTag:(NSString *)tag message:(NSString *)message metadata:(NSDictionary<id, id> * _Nullable)metadata __attribute__((swift_name("d(tag:message:metadata:)")));
- (void)doInternalLogLogLevel:(OnoLogLevel *)logLevel tag:(NSString *)tag message:(NSString *)message __attribute__((swift_name("doInternalLog(logLevel:tag:message:)")));
- (void)eTag:(NSString *)tag message:(NSString *)message __attribute__((swift_name("e(tag:message:)")));
- (void)eTag:(NSString *)tag message:(NSString *)message metadata:(NSDictionary<id, id> * _Nullable)metadata __attribute__((swift_name("e(tag:message:metadata:)")));
- (OnoOnoNetworkConnectionInfo *)getConnectionInfo __attribute__((swift_name("getConnectionInfo()")));
- (int64_t)getCurrentTimeMillis __attribute__((swift_name("getCurrentTimeMillis()")));
- (NSDictionary<NSString *, id> *)getGlobalMetadata __attribute__((swift_name("getGlobalMetadata()")));
- (void)iTag:(NSString *)tag message:(NSString *)message __attribute__((swift_name("i(tag:message:)")));
- (void)iTag:(NSString *)tag message:(NSString *)message metadata:(NSDictionary<id, id> * _Nullable)metadata __attribute__((swift_name("i(tag:message:metadata:)")));
- (void)instantiateLogger:(id<OnoILogger>)logger __attribute__((swift_name("instantiate(logger:)")));
- (void)logMessageLogLevel:(OnoLogLevel *)logLevel message:(NSString *)message metadata:(NSDictionary<id, id> * _Nullable)metadata component:(NSString * _Nullable)component __attribute__((swift_name("logMessage(logLevel:message:metadata:component:)")));
- (void)setClientBillIdentifierClientBillIdentifier:(NSString *)clientBillIdentifier __attribute__((swift_name("setClientBillIdentifier(clientBillIdentifier:)")));
- (id)setMetaValueKey:(NSString *)key value:(id)value __attribute__((swift_name("setMetaValue(key:value:)")));
- (void)tTag:(NSString *)tag message:(NSString *)message __attribute__((swift_name("t(tag:message:)")));
- (void)tTag:(NSString *)tag message:(NSString *)message metadata:(NSDictionary<id, id> * _Nullable)metadata __attribute__((swift_name("t(tag:message:metadata:)")));
- (BOOL)uploadLogFileClientBillIdentifier:(NSString *)clientBillIdentifier minLogLevel:(int32_t)minLogLevel excludeComponents:(OnoKotlinArray * _Nullable)excludeComponents excludeRegex:(NSString * _Nullable)excludeRegex __attribute__((swift_name("uploadLogFile(clientBillIdentifier:minLogLevel:excludeComponents:excludeRegex:)")));
- (void)uploadLogsInRangeStartDate:(int64_t)startDate endDate:(int64_t)endDate logLevel:(int32_t)logLevel __attribute__((swift_name("uploadLogsInRange(startDate:endDate:logLevel:)")));
- (void)wTag:(NSString *)tag message:(NSString *)message __attribute__((swift_name("w(tag:message:)")));
- (void)wTag:(NSString *)tag message:(NSString *)message metadata:(NSDictionary<id, id> * _Nullable)metadata __attribute__((swift_name("w(tag:message:metadata:)")));
@property (readonly) NSString *KEY_APP_PACKAGE_NAME __attribute__((swift_name("KEY_APP_PACKAGE_NAME")));
@property (readonly) NSString *KEY_APP_VERSION __attribute__((swift_name("KEY_APP_VERSION")));
@property (readonly) NSString *KEY_BUILD_NUMBER __attribute__((swift_name("KEY_BUILD_NUMBER")));
@property (readonly) NSString *KEY_DEVICE_DETAILS __attribute__((swift_name("KEY_DEVICE_DETAILS")));
@property (readonly) NSString *KEY_DEVICE_IDENTIFIER __attribute__((swift_name("KEY_DEVICE_IDENTIFIER")));
@property (readonly) NSString *KEY_DEVICE_MANUFACTURER __attribute__((swift_name("KEY_DEVICE_MANUFACTURER")));
@property (readonly) NSString *KEY_DEVICE_MODEL __attribute__((swift_name("KEY_DEVICE_MODEL")));
@property (readonly) NSString *KEY_DEVICE_NAME __attribute__((swift_name("KEY_DEVICE_NAME")));
@property (readonly) NSString *KEY_DEVICE_TYPE __attribute__((swift_name("KEY_DEVICE_TYPE")));
@property (readonly) NSString *KEY_MEMORY_USED_BYTES __attribute__((swift_name("KEY_MEMORY_USED_BYTES")));
@property (readonly) NSString *KEY_ONO_SDK_VERSION __attribute__((swift_name("KEY_ONO_SDK_VERSION")));
@property (readonly) NSString *KEY_OPERATING_SYSTEM __attribute__((swift_name("KEY_OPERATING_SYSTEM")));
@property (readonly) NSString *KEY_OS_NAME __attribute__((swift_name("KEY_OS_NAME")));
@property (readonly) NSString *KEY_OS_VERSION __attribute__((swift_name("KEY_OS_VERSION")));
@property (readonly) NSString *KEY_TRANSACTION_UUID __attribute__((swift_name("KEY_TRANSACTION_UUID")));
@property (readonly) NSString *KEY_YOCO_SDK_VERSION __attribute__((swift_name("KEY_YOCO_SDK_VERSION")));
@property NSString * _Nullable clientTransactionUUID __attribute__((swift_name("clientTransactionUUID")));
@property id<OnoILogger> _Nullable sharedLogger __attribute__((swift_name("sharedLogger")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MockLogger")))
@interface OnoMockLogger : OnoBase <OnoILogger>
- (instancetype)initWithClientTransactionUUID:(NSString * _Nullable)clientTransactionUUID __attribute__((swift_name("init(clientTransactionUUID:)"))) __attribute__((objc_designated_initializer));
- (BOOL)checkExistsClientBillIdentifier:(NSString *)clientBillIdentifier __attribute__((swift_name("checkExists(clientBillIdentifier:)")));
- (void)clearLoggedEntries __attribute__((swift_name("clearLoggedEntries()")));
- (void)doInternalLogLogLevel:(OnoLogLevel *)logLevel tag:(NSString *)tag message:(NSString *)message __attribute__((swift_name("doInternalLog(logLevel:tag:message:)")));
- (OnoOnoNetworkConnectionInfo *)getConnectionInfo __attribute__((swift_name("getConnectionInfo()")));
- (int64_t)getCurrentTimeMillis __attribute__((swift_name("getCurrentTimeMillis()")));
- (NSArray<OnoMockLoggerLogEntry *> *)getEntriesByTagTag:(NSString *)tag __attribute__((swift_name("getEntriesByTag(tag:)")));
- (NSDictionary<NSString *, id> *)getGlobalMetadata __attribute__((swift_name("getGlobalMetadata()")));
- (void)logMessageLogLevel:(OnoLogLevel *)logLevel message:(NSString *)message metadata:(NSDictionary<id, id> * _Nullable)metadata component:(NSString * _Nullable)component __attribute__((swift_name("logMessage(logLevel:message:metadata:component:)")));
- (void)setClientBillIdentifierClientBillIdentifier:(NSString *)clientBillIdentifier __attribute__((swift_name("setClientBillIdentifier(clientBillIdentifier:)")));
- (id)setMetaValueKey:(NSString *)key value:(id)value __attribute__((swift_name("setMetaValue(key:value:)")));
- (BOOL)uploadLogFileClientBillIdentifier:(NSString *)clientBillIdentifier minLogLevel:(int32_t)minLogLevel excludeComponents:(OnoKotlinArray * _Nullable)excludeComponents excludeRegex:(NSString * _Nullable)excludeRegex __attribute__((swift_name("uploadLogFile(clientBillIdentifier:minLogLevel:excludeComponents:excludeRegex:)")));
- (void)uploadLogsInRangeStartDate:(int64_t)startDate endDate:(int64_t)endDate logLevel:(int32_t)logLevel __attribute__((swift_name("uploadLogsInRange(startDate:endDate:logLevel:)")));
@property NSString * _Nullable clientTransactionUUID __attribute__((swift_name("clientTransactionUUID")));
@property (readonly) NSArray<OnoMockLoggerLogEntry *> *loggingEntries __attribute__((swift_name("loggingEntries")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MockLogger.LogEntry")))
@interface OnoMockLoggerLogEntry : OnoBase
- (instancetype)initWithClientTransactionUUID:(NSString * _Nullable)clientTransactionUUID logLevel:(OnoLogLevel *)logLevel message:(NSString *)message metadata:(NSDictionary<id, id> * _Nullable)metadata component:(NSString * _Nullable)component __attribute__((swift_name("init(clientTransactionUUID:logLevel:message:metadata:component:)"))) __attribute__((objc_designated_initializer));
- (NSString * _Nullable)component1 __attribute__((swift_name("component1()")));
- (OnoLogLevel *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (NSDictionary<id, id> * _Nullable)component4 __attribute__((swift_name("component4()")));
- (NSString * _Nullable)component5 __attribute__((swift_name("component5()")));
- (OnoMockLoggerLogEntry *)doCopyClientTransactionUUID:(NSString * _Nullable)clientTransactionUUID logLevel:(OnoLogLevel *)logLevel message:(NSString *)message metadata:(NSDictionary<id, id> * _Nullable)metadata component:(NSString * _Nullable)component __attribute__((swift_name("doCopy(clientTransactionUUID:logLevel:message:metadata:component:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable clientTransactionUUID __attribute__((swift_name("clientTransactionUUID")));
@property (readonly) NSString * _Nullable component __attribute__((swift_name("component")));
@property (readonly) OnoLogLevel *logLevel __attribute__((swift_name("logLevel")));
@property (readonly) NSString *message __attribute__((swift_name("message")));
@property (readonly) NSDictionary<id, id> * _Nullable metadata __attribute__((swift_name("metadata")));
@end;

__attribute__((swift_name("Action.Group")))
@interface OnoActionGroup : OnoBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((swift_name("Command")))
@interface OnoCommand : OnoAction
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((swift_name("Command.Group")))
@interface OnoCommandGroup : OnoActionGroup
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((swift_name("CommandDriver")))
@interface OnoCommandDriver : OnoBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)receiveCommandCommand:(OnoCommand *)command __attribute__((swift_name("receiveCommand(command:)")));
- (void)unhandledOrIgnoredCommand:(OnoCommand *)command mustHandleCommandInGroup:(id<OnoKotlinKClass>)mustHandleCommandInGroup __attribute__((swift_name("unhandledOrIgnored(command:mustHandleCommandInGroup:)")));
@end;

__attribute__((swift_name("Dispatcher")))
@interface OnoDispatcher : OnoBase
- (instancetype)initWithDispatch:(void (^)(OnoAction *))dispatch __attribute__((swift_name("init(dispatch:)"))) __attribute__((objc_designated_initializer));
@property (readonly) void (^dispatch)(OnoAction *) __attribute__((swift_name("dispatch")));
@end;

__attribute__((swift_name("StateMachine")))
@protocol OnoStateMachine
@required
- (id<OnoStoreData>)castStoreDataStoreData:(id<OnoStoreData>)storeData __attribute__((swift_name("castStoreData(storeData:)")));
- (OnoStateMachineResult *)consumeActionStoreData:(id<OnoStoreData>)storeData action:(OnoAction *)action __attribute__((swift_name("consumeAction(storeData:action:)")));
- (id<OnoStoreData>)getCurrentStateStore:(OnoStore *)store __attribute__((swift_name("getCurrentState(store:)")));
- (id<OnoStoreData>)getCurrentStateState:(NSDictionary<NSString *, id<OnoStoreData>> *)state __attribute__((swift_name("getCurrentState(state:)")));
- (id<OnoStoreData>)getStoreDataState:(NSDictionary<NSString *, id<OnoStoreData>> *)state __attribute__((swift_name("getStoreData(state:)")));
- (NSString *)tag __attribute__((swift_name("tag()")));
- (OnoStateMachineResult *)unhandledOrIgnoredState:(id<OnoStoreData>)state action:(OnoAction *)action mustHandleActionInGroup:(id<OnoKotlinKClass>)mustHandleActionInGroup __attribute__((swift_name("unhandledOrIgnored(state:action:mustHandleActionInGroup:)")));
@property (readonly) id<OnoStoreData> initialState __attribute__((swift_name("initialState")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly) NSString *simpleName __attribute__((swift_name("simpleName")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StateMachineResult")))
@interface OnoStateMachineResult : OnoBase
- (instancetype)initWithState:(id _Nullable)state actions:(NSArray<OnoAction *> * _Nullable)actions __attribute__((swift_name("init(state:actions:)"))) __attribute__((objc_designated_initializer));
@property (readonly) NSArray<OnoAction *> * _Nullable actions __attribute__((swift_name("actions")));
@property (readonly) id _Nullable state __attribute__((swift_name("state")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Store")))
@interface OnoStore : OnoBase
- (instancetype)initWithStateMachines:(NSArray<id<OnoStateMachine>> *)stateMachines commandDrivers:(NSArray<OnoCommandDriver *> *)commandDrivers environment:(OnoEnvironment *)environment __attribute__((swift_name("init(stateMachines:commandDrivers:environment:)"))) __attribute__((objc_designated_initializer));
- (void)addListenerStoreListener:(OnoStoreListener *)storeListener __attribute__((swift_name("addListener(storeListener:)")));
- (void)dispatchActionAction:(OnoAction *)action dispatcherName:(NSString *)dispatcherName __attribute__((swift_name("dispatchAction(action:dispatcherName:)")));
- (id<OnoStoreData>)stateForMachineName:(NSString *)name __attribute__((swift_name("stateForMachine(name:)")));
- (void)updateAllListenersWithCurrentState __attribute__((swift_name("updateAllListenersWithCurrentState()")));
@property (readonly) NSDictionary<NSString *, id<OnoStoreData>> *currentState __attribute__((swift_name("currentState")));
@property (readonly) NSArray<OnoStateUpdateEvent *> *stateHistory __attribute__((swift_name("stateHistory")));
@end;

__attribute__((swift_name("StoreData")))
@protocol OnoStoreData
@required
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StoreListener")))
@interface OnoStoreListener : OnoBase
- (instancetype)initWithFetchData:(id _Nullable (^)(NSDictionary<NSString *, id<OnoStoreData>> *))fetchData callback:(void (^)(id _Nullable))callback __attribute__((swift_name("init(fetchData:callback:)"))) __attribute__((objc_designated_initializer));
@property (readonly) void (^callback)(id _Nullable) __attribute__((swift_name("callback")));
@property (readonly) id _Nullable (^fetchData)(NSDictionary<NSString *, id<OnoStoreData>> *) __attribute__((swift_name("fetchData")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StoreUnitTestHelper")))
@interface OnoStoreUnitTestHelper : OnoBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)storeUnitTestHelper __attribute__((swift_name("init()")));
- (NSArray<id<OnoKotlinKClass>> *)actionClassHistory:(OnoStore *)receiver __attribute__((swift_name("actionClassHistory(_:)")));
- (NSArray<OnoKotlinPair *> *)actionDispatchHistory:(OnoStore *)receiver __attribute__((swift_name("actionDispatchHistory(_:)")));
- (NSArray<OnoAction *> *)actionHistory:(OnoStore *)receiver __attribute__((swift_name("actionHistory(_:)")));
- (BOOL)actionWasDispatched:(OnoStore *)receiver actionKClass:(id<OnoKotlinKClass>)actionKClass __attribute__((swift_name("actionWasDispatched(_:actionKClass:)")));
- (BOOL)assetActionsOccurredInOrder:(OnoStore *)receiver orderedActions:(NSArray<id<OnoKotlinKClass>> *)orderedActions __attribute__((swift_name("assetActionsOccurredInOrder(_:orderedActions:)")));
- (int32_t)countOfAction:(OnoStore *)receiver actionKClass:(id<OnoKotlinKClass>)actionKClass __attribute__((swift_name("countOfAction(_:actionKClass:)")));
- (int32_t)countOfExactAction:(OnoStore *)receiver action:(OnoAction *)action __attribute__((swift_name("countOfExactAction(_:action:)")));
- (void)dispatchAction:(OnoStore *)receiver action:(OnoAction *)action __attribute__((swift_name("dispatchAction(_:action:)")));
- (OnoInt * _Nullable)indexOfLastAction:(OnoStore *)receiver actionKClass:(id<OnoKotlinKClass>)actionKClass __attribute__((swift_name("indexOfLastAction(_:actionKClass:)")));
- (OnoAction * _Nullable)lastActionOfType:(OnoStore *)receiver actionKClass:(id<OnoKotlinKClass>)actionKClass __attribute__((swift_name("lastActionOfType(_:actionKClass:)")));
- (OnoTransaction *)lastCompletedTransaction:(OnoStore *)receiver __attribute__((swift_name("lastCompletedTransaction(_:)")));
- (NSArray<OnoAction *> *)listActionsOfType:(OnoStore *)receiver actionKClass:(id<OnoKotlinKClass>)actionKClass __attribute__((swift_name("listActionsOfType(_:actionKClass:)")));
- (void)setCurrentStateForStateMachine:(OnoStore *)receiver machinePairData:(OnoKotlinArray *)machinePairData __attribute__((swift_name("setCurrentStateForStateMachine(_:machinePairData:)")));
- (void)showActionHistoryOnAssertionFailure:(OnoStore *)receiver showFullActionName:(BOOL)showFullActionName function:(void (^)(void))function __attribute__((swift_name("showActionHistoryOnAssertionFailure(_:showFullActionName:function:)")));
- (OnoStoreUnitTestHelperTracksLists *)trackedEventsLists:(OnoStore *)receiver __attribute__((swift_name("trackedEventsLists(_:)")));
- (NSArray<id> *)transactionHistory:(OnoStore *)receiver __attribute__((swift_name("transactionHistory(_:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StoreUnitTestHelper.TracksLists")))
@interface OnoStoreUnitTestHelperTracksLists : OnoBase
- (instancetype)initWithTaskBeginTracks:(NSArray<OnoActivityTimelineActionsTaskBegan *> *)taskBeginTracks taskEventTracks:(NSArray<OnoActivityTimelineActionsEventOccurred *> *)taskEventTracks taskEndTracks:(NSArray<OnoActivityTimelineActionsTaskEnded *> *)taskEndTracks segmentEventTracks:(NSArray<OnoSegmentCommandsTrackEvent *> *)segmentEventTracks trackEndAllActivities:(NSArray<OnoActivityTimelineActionsEndAllRunningTasks *> *)trackEndAllActivities __attribute__((swift_name("init(taskBeginTracks:taskEventTracks:taskEndTracks:segmentEventTracks:trackEndAllActivities:)"))) __attribute__((objc_designated_initializer));
- (NSArray<OnoActivityTimelineActionsTaskBegan *> *)component1 __attribute__((swift_name("component1()")));
- (NSArray<OnoActivityTimelineActionsEventOccurred *> *)component2 __attribute__((swift_name("component2()")));
- (NSArray<OnoActivityTimelineActionsTaskEnded *> *)component3 __attribute__((swift_name("component3()")));
- (NSArray<OnoSegmentCommandsTrackEvent *> *)component4 __attribute__((swift_name("component4()")));
- (NSArray<OnoActivityTimelineActionsEndAllRunningTasks *> *)component5 __attribute__((swift_name("component5()")));
- (OnoStoreUnitTestHelperTracksLists *)doCopyTaskBeginTracks:(NSArray<OnoActivityTimelineActionsTaskBegan *> *)taskBeginTracks taskEventTracks:(NSArray<OnoActivityTimelineActionsEventOccurred *> *)taskEventTracks taskEndTracks:(NSArray<OnoActivityTimelineActionsTaskEnded *> *)taskEndTracks segmentEventTracks:(NSArray<OnoSegmentCommandsTrackEvent *> *)segmentEventTracks trackEndAllActivities:(NSArray<OnoActivityTimelineActionsEndAllRunningTasks *> *)trackEndAllActivities __attribute__((swift_name("doCopy(taskBeginTracks:taskEventTracks:taskEndTracks:segmentEventTracks:trackEndAllActivities:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<OnoSegmentCommandsTrackEvent *> *segmentEventTracks __attribute__((swift_name("segmentEventTracks")));
@property (readonly) NSArray<OnoActivityTimelineActionsTaskBegan *> *taskBeginTracks __attribute__((swift_name("taskBeginTracks")));
@property (readonly) NSArray<OnoActivityTimelineActionsTaskEnded *> *taskEndTracks __attribute__((swift_name("taskEndTracks")));
@property (readonly) NSArray<OnoActivityTimelineActionsEventOccurred *> *taskEventTracks __attribute__((swift_name("taskEventTracks")));
@property (readonly) NSArray<OnoActivityTimelineActionsEndAllRunningTasks *> *trackEndAllActivities __attribute__((swift_name("trackEndAllActivities")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StateUpdateEvent")))
@interface OnoStateUpdateEvent : OnoBase
- (instancetype)initWithAction:(OnoAction *)action newState:(NSDictionary<NSString *, id<OnoStoreData>> *)newState dispatcherName:(NSString *)dispatcherName __attribute__((swift_name("init(action:newState:dispatcherName:)"))) __attribute__((objc_designated_initializer));
- (OnoAction *)component1 __attribute__((swift_name("component1()")));
- (NSDictionary<NSString *, id<OnoStoreData>> *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (OnoStateUpdateEvent *)doCopyAction:(OnoAction *)action newState:(NSDictionary<NSString *, id<OnoStoreData>> *)newState dispatcherName:(NSString *)dispatcherName __attribute__((swift_name("doCopy(action:newState:dispatcherName:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoAction *action __attribute__((swift_name("action")));
@property (readonly) NSString *dispatcherName __attribute__((swift_name("dispatcherName")));
@property (readonly, getter=doNewState) NSDictionary<NSString *, id<OnoStoreData>> *newState __attribute__((swift_name("newState")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MockOnoSdkModuleStateMachine")))
@interface OnoMockOnoSdkModuleStateMachine : OnoBase <OnoStateMachine>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (OnoMockOnoSdkModuleStateMachineCallData *)castStoreDataStoreData:(id<OnoStoreData>)storeData __attribute__((swift_name("castStoreData(storeData:)")));
- (OnoStateMachineResult *)consumeActionStoreData:(id<OnoStoreData>)storeData action:(OnoAction *)action __attribute__((swift_name("consumeAction(storeData:action:)")));
- (OnoMockOnoSdkModuleStateMachineCallData *)getCurrentStateStore:(OnoStore *)store __attribute__((swift_name("getCurrentState(store:)")));
- (OnoMockOnoSdkModuleStateMachineCallData *)getCurrentStateState:(NSDictionary<NSString *, id<OnoStoreData>> *)state __attribute__((swift_name("getCurrentState(state:)")));
- (OnoMockOnoSdkModuleStateMachineCallData *)getStoreDataState:(NSDictionary<NSString *, id<OnoStoreData>> *)state __attribute__((swift_name("getStoreData(state:)")));
- (void)setReactionForActionTypesActionResponse:(OnoKotlinArray *)actionResponse __attribute__((swift_name("setReactionForActionTypes(actionResponse:)")));
- (void)setReactionForActionTypesActionResponse_:(OnoKotlinArray *)actionResponse __attribute__((swift_name("setReactionForActionTypes(actionResponse_:)")));
@property (readonly) OnoMockOnoSdkModuleStateMachineCallData *initialState __attribute__((swift_name("initialState")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MockOnoSdkModuleStateMachine.ActionTypeResponse")))
@interface OnoMockOnoSdkModuleStateMachineActionTypeResponse : OnoBase
- (instancetype)initWithActionClass:(id<OnoKotlinKClass>)actionClass sideEffectAction:(OnoAction *)sideEffectAction __attribute__((swift_name("init(actionClass:sideEffectAction:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithActionClass:(id<OnoKotlinKClass>)actionClass sideEffectActions:(NSArray<OnoAction *> *)sideEffectActions __attribute__((swift_name("init(actionClass:sideEffectActions:)"))) __attribute__((objc_designated_initializer));
- (id<OnoKotlinKClass>)component1 __attribute__((swift_name("component1()")));
- (NSArray<OnoAction *> *)component2 __attribute__((swift_name("component2()")));
- (OnoMockOnoSdkModuleStateMachineActionTypeResponse *)doCopyActionClass:(id<OnoKotlinKClass>)actionClass sideEffectActions:(NSArray<OnoAction *> *)sideEffectActions __attribute__((swift_name("doCopy(actionClass:sideEffectActions:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) id<OnoKotlinKClass> actionClass __attribute__((swift_name("actionClass")));
@property (readonly) NSArray<OnoAction *> *sideEffectActions __attribute__((swift_name("sideEffectActions")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MockOnoSdkModuleStateMachine.CallData")))
@interface OnoMockOnoSdkModuleStateMachineCallData : OnoBase <OnoStoreData>
- (instancetype)initWithCallIndex:(int32_t)callIndex callAction:(OnoAction * _Nullable)callAction callSideEffect:(NSArray<OnoAction *> * _Nullable)callSideEffect __attribute__((swift_name("init(callIndex:callAction:callSideEffect:)"))) __attribute__((objc_designated_initializer));
- (int32_t)component1 __attribute__((swift_name("component1()")));
- (OnoAction * _Nullable)component2 __attribute__((swift_name("component2()")));
- (NSArray<OnoAction *> * _Nullable)component3 __attribute__((swift_name("component3()")));
- (OnoMockOnoSdkModuleStateMachineCallData *)doCopyCallIndex:(int32_t)callIndex callAction:(OnoAction * _Nullable)callAction callSideEffect:(NSArray<OnoAction *> * _Nullable)callSideEffect __attribute__((swift_name("doCopy(callIndex:callAction:callSideEffect:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoAction * _Nullable callAction __attribute__((swift_name("callAction")));
@property (readonly) int32_t callIndex __attribute__((swift_name("callIndex")));
@property (readonly) NSArray<OnoAction *> * _Nullable callSideEffect __attribute__((swift_name("callSideEffect")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MockOnoSdkModuleStateMachine.SpecificActionResponse")))
@interface OnoMockOnoSdkModuleStateMachineSpecificActionResponse : OnoBase
- (instancetype)initWithAction:(OnoAction *)action sideEffectAction:(OnoAction *)sideEffectAction __attribute__((swift_name("init(action:sideEffectAction:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithAction:(OnoAction *)action sideEffectActions:(NSArray<OnoAction *> *)sideEffectActions __attribute__((swift_name("init(action:sideEffectActions:)"))) __attribute__((objc_designated_initializer));
- (OnoAction *)component1 __attribute__((swift_name("component1()")));
- (NSArray<OnoAction *> *)component2 __attribute__((swift_name("component2()")));
- (OnoMockOnoSdkModuleStateMachineSpecificActionResponse *)doCopyAction:(OnoAction *)action sideEffectActions:(NSArray<OnoAction *> *)sideEffectActions __attribute__((swift_name("doCopy(action:sideEffectActions:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoAction *action __attribute__((swift_name("action")));
@property (readonly) NSArray<OnoAction *> *sideEffectActions __attribute__((swift_name("sideEffectActions")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoNetworkConnectionInfo")))
@interface OnoOnoNetworkConnectionInfo : OnoBase
- (instancetype)initWithNetworkType:(OnoNetworkConnectionType *)networkType carrierName:(NSString * _Nullable)carrierName signalStrength:(int32_t)signalStrength connectionType:(NSString *)connectionType __attribute__((swift_name("init(networkType:carrierName:signalStrength:connectionType:)"))) __attribute__((objc_designated_initializer));
- (OnoNetworkConnectionType *)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (int32_t)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (OnoOnoNetworkConnectionInfo *)doCopyNetworkType:(OnoNetworkConnectionType *)networkType carrierName:(NSString * _Nullable)carrierName signalStrength:(int32_t)signalStrength connectionType:(NSString *)connectionType __attribute__((swift_name("doCopy(networkType:carrierName:signalStrength:connectionType:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSDictionary<NSString *, NSString *> *)getMappedData __attribute__((swift_name("getMappedData()")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable carrierName __attribute__((swift_name("carrierName")));
@property (readonly) NSString *connectionType __attribute__((swift_name("connectionType")));
@property (readonly) OnoNetworkConnectionType *networkType __attribute__((swift_name("networkType")));
@property (readonly) int32_t signalStrength __attribute__((swift_name("signalStrength")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SensitiveByteArray")))
@interface OnoSensitiveByteArray : OnoBase
- (instancetype)initWithByteArray:(OnoKotlinByteArray *)byteArray __attribute__((swift_name("init(byteArray:)"))) __attribute__((objc_designated_initializer));
- (OnoKotlinByteArray *)component1 __attribute__((swift_name("component1()")));
- (OnoSensitiveByteArray *)doCopyByteArray:(OnoKotlinByteArray *)byteArray __attribute__((swift_name("doCopy(byteArray:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoKotlinByteArray *byteArray __attribute__((swift_name("byteArray")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SensitiveMap")))
@interface OnoSensitiveMap : OnoBase
- (instancetype)initWithValueMap:(NSDictionary<NSString *, NSString *> *)valueMap __attribute__((swift_name("init(valueMap:)"))) __attribute__((objc_designated_initializer));
- (NSDictionary<NSString *, NSString *> *)component1 __attribute__((swift_name("component1()")));
- (OnoSensitiveMap *)doCopyValueMap:(NSDictionary<NSString *, NSString *> *)valueMap __attribute__((swift_name("doCopy(valueMap:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSDictionary<NSString *, NSString *> *valueMap __attribute__((swift_name("valueMap")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SensitiveMap.Companion")))
@interface OnoSensitiveMapCompanion : OnoBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (OnoSensitiveMap * _Nullable)combineMapA:(OnoSensitiveMap * _Nullable)mapA mapB:(OnoSensitiveMap * _Nullable)mapB __attribute__((swift_name("combine(mapA:mapB:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SensitiveString")))
@interface OnoSensitiveString : OnoBase
- (instancetype)initWithValue:(NSString *)value __attribute__((swift_name("init(value:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoSensitiveString *)doCopyValue:(NSString *)value __attribute__((swift_name("doCopy(value:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (BOOL)isBlank __attribute__((swift_name("isBlank()")));
- (int32_t)length __attribute__((swift_name("length()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionInitiator")))
@interface OnoTransactionInitiator : OnoBase
- (instancetype)initWithIntegrator:(NSString *)integrator integratorUUID:(NSString * _Nullable)integratorUUID __attribute__((swift_name("init(integrator:integratorUUID:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (OnoTransactionInitiator *)doCopyIntegrator:(NSString *)integrator integratorUUID:(NSString * _Nullable)integratorUUID __attribute__((swift_name("doCopy(integrator:integratorUUID:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSDictionary<NSString *, id> *)toMap __attribute__((swift_name("toMap()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *integrator __attribute__((swift_name("integrator")));
@property (readonly) NSString * _Nullable integratorUUID __attribute__((swift_name("integratorUUID")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraModule")))
@interface OnoMiuraModule : OnoBase <OnoOnoSDKModule>
- (instancetype)initWithMiuraService:(OnoOnoMiuraServiceProtocol *)miuraService __attribute__((swift_name("init(miuraService:)"))) __attribute__((objc_designated_initializer));
@property (readonly) NSArray<OnoCommandDriver *> *commandDrivers __attribute__((swift_name("commandDrivers")));
@property (readonly) NSArray<id<OnoStateMachine>> *stateMachines __attribute__((swift_name("stateMachines")));
@property (readonly) NSArray<OnoStoreListener *> *storeListeners __attribute__((swift_name("storeListeners")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoMiuraStatusMessages")))
@interface OnoOnoMiuraStatusMessages : OnoBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)onoMiuraStatusMessages __attribute__((swift_name("init()")));
- (NSString *)GET_PED_FILE_FAILEDFileName:(NSString *)fileName __attribute__((swift_name("GET_PED_FILE_FAILED(fileName:)")));
- (NSString *)KEY_INJECTION_PED_ERRORError:(NSString *)error __attribute__((swift_name("KEY_INJECTION_PED_ERROR(error:)")));
- (NSString *)NEGATIVE_PED_FILE_SIZEPedFileName:(NSString *)pedFileName __attribute__((swift_name("NEGATIVE_PED_FILE_SIZE(pedFileName:)")));
- (NSString *)NULL_DATA_FOR_PED_FILEDataId:(OnoResponseRkiDataId *)dataId __attribute__((swift_name("NULL_DATA_FOR_PED_FILE(dataId:)")));
- (NSString *)SELECT_PED_FILE_FAILEDDataId:(OnoResponseRkiDataId *)dataId __attribute__((swift_name("SELECT_PED_FILE_FAILED(dataId:)")));
- (NSString *)WRITE_PED_FILE_FAILEDDataId:(OnoResponseRkiDataId *)dataId __attribute__((swift_name("WRITE_PED_FILE_FAILED(dataId:)")));
- (NSString *)WRITE_PED_FILE_SUCCESSDataId:(OnoResponseRkiDataId *)dataId __attribute__((swift_name("WRITE_PED_FILE_SUCCESS(dataId:)")));
@property (readonly) NSString *CARD_STATUS_LISTENER_REGISTERED_SUCCESSFULLY __attribute__((swift_name("CARD_STATUS_LISTENER_REGISTERED_SUCCESSFULLY")));
@property (readonly) NSString *E5_TLV_RESPONSE_FROM_CHIP __attribute__((swift_name("E5_TLV_RESPONSE_FROM_CHIP")));
@property (readonly) NSString *NULL_MPI_CLIENT __attribute__((swift_name("NULL_MPI_CLIENT")));
@property (readonly) NSString *P2PE_INITIALIZE_FAILED __attribute__((swift_name("P2PE_INITIALIZE_FAILED")));
@property (readonly) NSString *P2PE_INITIALIZE_IS_FALSE __attribute__((swift_name("P2PE_INITIALIZE_IS_FALSE")));
@property (readonly) NSString *P2PE_INITIALIZE_SHOULD_BE_FALSE __attribute__((swift_name("P2PE_INITIALIZE_SHOULD_BE_FALSE")));
@property (readonly) NSString *P2PE_IS_READY __attribute__((swift_name("P2PE_IS_READY")));
@property (readonly) NSString *P2PE_STATUS_NOT_CARD_READY __attribute__((swift_name("P2PE_STATUS_NOT_CARD_READY")));
@property (readonly) NSString *P2PE_STATUS_NOT_PIN_READY __attribute__((swift_name("P2PE_STATUS_NOT_PIN_READY")));
@property (readonly) NSString *STARTED_TRAN_WITH_CARD_INSERTED __attribute__((swift_name("STARTED_TRAN_WITH_CARD_INSERTED")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoMiuraUtils")))
@interface OnoOnoMiuraUtils : OnoBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)onoMiuraUtils __attribute__((swift_name("init()")));
- (NSString *)deviceSerialNumberBtDevice:(OnoBluetoothDevice *)btDevice __attribute__((swift_name("deviceSerialNumber(btDevice:)")));
- (OnoSensitiveString *)fromBytesToTlvData:(OnoKotlinByteArray *)data __attribute__((swift_name("fromBytesToTlv(data:)")));
- (BOOL)hasRequiredOnoEmvConfigVersionEmvConfigVersion:(NSString * _Nullable)emvConfigVersion __attribute__((swift_name("hasRequiredOnoEmvConfigVersion(emvConfigVersion:)")));
- (BOOL)isMiuraDeviceBtDevice:(OnoBluetoothDevice *)btDevice __attribute__((swift_name("isMiuraDevice(btDevice:)")));
- (NSString *)loggingTagClassName:(NSString *)className __attribute__((swift_name("loggingTag(className:)")));
@property (readonly) NSString *INVALID_SERIAL_NUMBER __attribute__((swift_name("INVALID_SERIAL_NUMBER")));
@end;

__attribute__((swift_name("MiuraActions")))
@interface OnoMiuraActions : OnoActionGroup
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.AbortTransaction")))
@interface OnoMiuraActionsAbortTransaction : OnoAction
- (instancetype)initWithErrorDetails:(OnoErrorDetails *)errorDetails transactionStep:(OnoOnoMiuraTransactionStep * _Nullable)transactionStep __attribute__((swift_name("init(errorDetails:transactionStep:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoErrorDetails *)component1 __attribute__((swift_name("component1()")));
- (OnoOnoMiuraTransactionStep * _Nullable)component2 __attribute__((swift_name("component2()")));
- (OnoMiuraActionsAbortTransaction *)doCopyErrorDetails:(OnoErrorDetails *)errorDetails transactionStep:(OnoOnoMiuraTransactionStep * _Nullable)transactionStep __attribute__((swift_name("doCopy(errorDetails:transactionStep:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoErrorDetails *errorDetails __attribute__((swift_name("errorDetails")));
@property (readonly) OnoOnoMiuraTransactionStep * _Nullable transactionStep __attribute__((swift_name("transactionStep")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.ArpcFailed")))
@interface OnoMiuraActionsArpcFailed : OnoAction
- (instancetype)initWithTransactionError:(OnoOnoMiuraTransactionError * _Nullable)transactionError __attribute__((swift_name("init(transactionError:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoOnoMiuraTransactionError * _Nullable)component1 __attribute__((swift_name("component1()")));
- (OnoMiuraActionsArpcFailed *)doCopyTransactionError:(OnoOnoMiuraTransactionError * _Nullable)transactionError __attribute__((swift_name("doCopy(transactionError:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoOnoMiuraTransactionError * _Nullable transactionError __attribute__((swift_name("transactionError")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.ArpcProcessResult")))
@interface OnoMiuraActionsArpcProcessResult : OnoAction
- (instancetype)initWithFinalizationTlv:(OnoSensitiveString * _Nullable)finalizationTlv __attribute__((swift_name("init(finalizationTlv:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoSensitiveString * _Nullable)component1 __attribute__((swift_name("component1()")));
- (OnoMiuraActionsArpcProcessResult *)doCopyFinalizationTlv:(OnoSensitiveString * _Nullable)finalizationTlv __attribute__((swift_name("doCopy(finalizationTlv:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoSensitiveString * _Nullable finalizationTlv __attribute__((swift_name("finalizationTlv")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.AsyncProcessException")))
@interface OnoMiuraActionsAsyncProcessException : OnoAction
- (instancetype)initWithFailedProcessID:(OnoMiuraCommandType *)failedProcessID failureReason:(NSString *)failureReason cause:(OnoKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(failedProcessID:failureReason:cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoMiuraCommandType *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (OnoKotlinThrowable * _Nullable)component3 __attribute__((swift_name("component3()")));
- (OnoMiuraActionsAsyncProcessException *)doCopyFailedProcessID:(OnoMiuraCommandType *)failedProcessID failureReason:(NSString *)failureReason cause:(OnoKotlinThrowable * _Nullable)cause __attribute__((swift_name("doCopy(failedProcessID:failureReason:cause:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoKotlinThrowable * _Nullable cause __attribute__((swift_name("cause")));
@property (readonly) OnoMiuraCommandType *failedProcessID __attribute__((swift_name("failedProcessID")));
@property (readonly) NSString *failureReason __attribute__((swift_name("failureReason")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.AwaitingCard")))
@interface OnoMiuraActionsAwaitingCard : OnoAction
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString * _Nullable)component1 __attribute__((swift_name("component1()")));
- (OnoMiuraActionsAwaitingCard *)doCopyMessage:(NSString * _Nullable)message __attribute__((swift_name("doCopy(message:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable message __attribute__((swift_name("message")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.BatteryStatusInfo")))
@interface OnoMiuraActionsBatteryStatusInfo : OnoAction
- (instancetype)initWithBatteryStatus:(OnoOnoMiuraBatteryStatus *)batteryStatus __attribute__((swift_name("init(batteryStatus:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoOnoMiuraBatteryStatus *)component1 __attribute__((swift_name("component1()")));
- (OnoMiuraActionsBatteryStatusInfo *)doCopyBatteryStatus:(OnoOnoMiuraBatteryStatus *)batteryStatus __attribute__((swift_name("doCopy(batteryStatus:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoOnoMiuraBatteryStatus *batteryStatus __attribute__((swift_name("batteryStatus")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.BluetoothInfo")))
@interface OnoMiuraActionsBluetoothInfo : OnoAction
- (instancetype)initWithBluetoothInfo:(NSDictionary<NSString *, NSString *> *)bluetoothInfo __attribute__((swift_name("init(bluetoothInfo:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSDictionary<NSString *, NSString *> *)component1 __attribute__((swift_name("component1()")));
- (OnoMiuraActionsBluetoothInfo *)doCopyBluetoothInfo:(NSDictionary<NSString *, NSString *> *)bluetoothInfo __attribute__((swift_name("doCopy(bluetoothInfo:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSDictionary<NSString *, NSString *> *bluetoothInfo __attribute__((swift_name("bluetoothInfo")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.BypassPinEntry")))
@interface OnoMiuraActionsBypassPinEntry : OnoAction
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.CardData")))
@interface OnoMiuraActionsCardData : OnoAction
- (instancetype)initWithOnoMiuraCardData:(OnoOnoMiuraCardData *)onoMiuraCardData __attribute__((swift_name("init(onoMiuraCardData:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoOnoMiuraCardData *)component1 __attribute__((swift_name("component1()")));
- (OnoMiuraActionsCardData *)doCopyOnoMiuraCardData:(OnoOnoMiuraCardData *)onoMiuraCardData __attribute__((swift_name("doCopy(onoMiuraCardData:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoOnoMiuraCardData *onoMiuraCardData __attribute__((swift_name("onoMiuraCardData")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.CardInserted")))
@interface OnoMiuraActionsCardInserted : OnoAction
- (instancetype)initWithTlv:(OnoSensitiveString *)tlv __attribute__((swift_name("init(tlv:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoSensitiveString *)component1 __attribute__((swift_name("component1()")));
- (OnoMiuraActionsCardInserted *)doCopyTlv:(OnoSensitiveString *)tlv __attribute__((swift_name("doCopy(tlv:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoSensitiveString *tlv __attribute__((swift_name("tlv")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.CardSwiped")))
@interface OnoMiuraActionsCardSwiped : OnoAction
- (instancetype)initWithOnoMiuraCardData:(OnoOnoMiuraCardData *)onoMiuraCardData __attribute__((swift_name("init(onoMiuraCardData:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoOnoMiuraCardData *)component1 __attribute__((swift_name("component1()")));
- (OnoMiuraActionsCardSwiped *)doCopyOnoMiuraCardData:(OnoOnoMiuraCardData *)onoMiuraCardData __attribute__((swift_name("doCopy(onoMiuraCardData:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoOnoMiuraCardData *onoMiuraCardData __attribute__((swift_name("onoMiuraCardData")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.CardTapped")))
@interface OnoMiuraActionsCardTapped : OnoAction
- (instancetype)initWithTlv:(OnoSensitiveString *)tlv __attribute__((swift_name("init(tlv:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoSensitiveString *)component1 __attribute__((swift_name("component1()")));
- (OnoMiuraActionsCardTapped *)doCopyTlv:(OnoSensitiveString *)tlv __attribute__((swift_name("doCopy(tlv:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoSensitiveString *tlv __attribute__((swift_name("tlv")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.ContinueInitialization")))
@interface OnoMiuraActionsContinueInitialization : OnoAction
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.DeviceCapabilities")))
@interface OnoMiuraActionsDeviceCapabilities : OnoAction
- (instancetype)initWithDeviceCapabilities:(NSArray<OnoOnoMiuraDeviceCapability *> *)deviceCapabilities __attribute__((swift_name("init(deviceCapabilities:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSArray<OnoOnoMiuraDeviceCapability *> *)component1 __attribute__((swift_name("component1()")));
- (OnoMiuraActionsDeviceCapabilities *)doCopyDeviceCapabilities:(NSArray<OnoOnoMiuraDeviceCapability *> *)deviceCapabilities __attribute__((swift_name("doCopy(deviceCapabilities:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<OnoOnoMiuraDeviceCapability *> *deviceCapabilities __attribute__((swift_name("deviceCapabilities")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.DeviceStatusChange")))
@interface OnoMiuraActionsDeviceStatusChange : OnoAction
- (instancetype)initWithDeviceStatus:(OnoMiuraDeviceStatus *)deviceStatus statusText:(NSString *)statusText __attribute__((swift_name("init(deviceStatus:statusText:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoMiuraDeviceStatus *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (OnoMiuraActionsDeviceStatusChange *)doCopyDeviceStatus:(OnoMiuraDeviceStatus *)deviceStatus statusText:(NSString *)statusText __attribute__((swift_name("doCopy(deviceStatus:statusText:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoMiuraDeviceStatus *deviceStatus __attribute__((swift_name("deviceStatus")));
@property (readonly) NSString *statusText __attribute__((swift_name("statusText")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.DispatchTerminalInfo")))
@interface OnoMiuraActionsDispatchTerminalInfo : OnoAction
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.EmvFailed")))
@interface OnoMiuraActionsEmvFailed : OnoAction
- (instancetype)initWithTransactionError:(OnoOnoMiuraTransactionError * _Nullable)transactionError __attribute__((swift_name("init(transactionError:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoOnoMiuraTransactionError * _Nullable)component1 __attribute__((swift_name("component1()")));
- (OnoMiuraActionsEmvFailed *)doCopyTransactionError:(OnoOnoMiuraTransactionError * _Nullable)transactionError __attribute__((swift_name("doCopy(transactionError:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoOnoMiuraTransactionError * _Nullable transactionError __attribute__((swift_name("transactionError")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.GetBatteryStatusFailed")))
@interface OnoMiuraActionsGetBatteryStatusFailed : OnoAction
- (instancetype)initWithReason:(NSString *)reason __attribute__((swift_name("init(reason:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoMiuraActionsGetBatteryStatusFailed *)doCopyReason:(NSString *)reason __attribute__((swift_name("doCopy(reason:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *reason __attribute__((swift_name("reason")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.GetBluetoothInfoFailed")))
@interface OnoMiuraActionsGetBluetoothInfoFailed : OnoAction
- (instancetype)initWithReason:(NSString *)reason __attribute__((swift_name("init(reason:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoMiuraActionsGetBluetoothInfoFailed *)doCopyReason:(NSString *)reason __attribute__((swift_name("doCopy(reason:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *reason __attribute__((swift_name("reason")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.GetDeviceCapabilitiesFailed")))
@interface OnoMiuraActionsGetDeviceCapabilitiesFailed : OnoAction
- (instancetype)initWithReason:(NSString *)reason __attribute__((swift_name("init(reason:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoMiuraActionsGetDeviceCapabilitiesFailed *)doCopyReason:(NSString *)reason __attribute__((swift_name("doCopy(reason:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *reason __attribute__((swift_name("reason")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.GetFileProgress")))
@interface OnoMiuraActionsGetFileProgress : OnoAction
- (instancetype)initWithFileName:(NSString *)fileName progress:(float)progress fileNumber:(int32_t)fileNumber totalNumberOfFiles:(int32_t)totalNumberOfFiles __attribute__((swift_name("init(fileName:progress:fileNumber:totalNumberOfFiles:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (float)component2 __attribute__((swift_name("component2()")));
- (int32_t)component3 __attribute__((swift_name("component3()")));
- (int32_t)component4 __attribute__((swift_name("component4()")));
- (OnoMiuraActionsGetFileProgress *)doCopyFileName:(NSString *)fileName progress:(float)progress fileNumber:(int32_t)fileNumber totalNumberOfFiles:(int32_t)totalNumberOfFiles __attribute__((swift_name("doCopy(fileName:progress:fileNumber:totalNumberOfFiles:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *fileName __attribute__((swift_name("fileName")));
@property (readonly) int32_t fileNumber __attribute__((swift_name("fileNumber")));
@property (readonly) float progress __attribute__((swift_name("progress")));
@property (readonly) int32_t totalNumberOfFiles __attribute__((swift_name("totalNumberOfFiles")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.GetP2PEStatusFailed")))
@interface OnoMiuraActionsGetP2PEStatusFailed : OnoAction
- (instancetype)initWithReason:(NSString *)reason isKeyInjectionVerification:(BOOL)isKeyInjectionVerification __attribute__((swift_name("init(reason:isKeyInjectionVerification:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (BOOL)component2 __attribute__((swift_name("component2()")));
- (OnoMiuraActionsGetP2PEStatusFailed *)doCopyReason:(NSString *)reason isKeyInjectionVerification:(BOOL)isKeyInjectionVerification __attribute__((swift_name("doCopy(reason:isKeyInjectionVerification:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL isKeyInjectionVerification __attribute__((swift_name("isKeyInjectionVerification")));
@property (readonly) NSString *reason __attribute__((swift_name("reason")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.GetPEDConfigFailed")))
@interface OnoMiuraActionsGetPEDConfigFailed : OnoAction
- (instancetype)initWithReason:(NSString *)reason __attribute__((swift_name("init(reason:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoMiuraActionsGetPEDConfigFailed *)doCopyReason:(NSString *)reason __attribute__((swift_name("doCopy(reason:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *reason __attribute__((swift_name("reason")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.GetSoftwareInfoFailed")))
@interface OnoMiuraActionsGetSoftwareInfoFailed : OnoAction
- (instancetype)initWithReason:(NSString *)reason __attribute__((swift_name("init(reason:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoMiuraActionsGetSoftwareInfoFailed *)doCopyReason:(NSString *)reason __attribute__((swift_name("doCopy(reason:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *reason __attribute__((swift_name("reason")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.GettingFileFromPed")))
@interface OnoMiuraActionsGettingFileFromPed : OnoAction
- (instancetype)initWithFileName:(NSString *)fileName fileNumber:(int32_t)fileNumber totalNumberOfFiles:(int32_t)totalNumberOfFiles __attribute__((swift_name("init(fileName:fileNumber:totalNumberOfFiles:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (int32_t)component2 __attribute__((swift_name("component2()")));
- (int32_t)component3 __attribute__((swift_name("component3()")));
- (OnoMiuraActionsGettingFileFromPed *)doCopyFileName:(NSString *)fileName fileNumber:(int32_t)fileNumber totalNumberOfFiles:(int32_t)totalNumberOfFiles __attribute__((swift_name("doCopy(fileName:fileNumber:totalNumberOfFiles:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *fileName __attribute__((swift_name("fileName")));
@property (readonly) int32_t fileNumber __attribute__((swift_name("fileNumber")));
@property (readonly) int32_t totalNumberOfFiles __attribute__((swift_name("totalNumberOfFiles")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.KeyInjectionSuccessful")))
@interface OnoMiuraActionsKeyInjectionSuccessful : OnoAction
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.KeyPressed")))
@interface OnoMiuraActionsKeyPressed : OnoAction
- (instancetype)initWithButton:(OnoOnoMiuraButton *)button __attribute__((swift_name("init(button:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoOnoMiuraButton *)component1 __attribute__((swift_name("component1()")));
- (OnoMiuraActionsKeyPressed *)doCopyButton:(OnoOnoMiuraButton *)button __attribute__((swift_name("doCopy(button:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoOnoMiuraButton *button __attribute__((swift_name("button")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.KeypadStatus")))
@interface OnoMiuraActionsKeypadStatus : OnoAction
- (instancetype)initWithIsEnabled:(BOOL)isEnabled __attribute__((swift_name("init(isEnabled:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (BOOL)component1 __attribute__((swift_name("component1()")));
- (OnoMiuraActionsKeypadStatus *)doCopyIsEnabled:(BOOL)isEnabled __attribute__((swift_name("doCopy(isEnabled:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL isEnabled __attribute__((swift_name("isEnabled")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.MiuraExecutionTimeout")))
@interface OnoMiuraActionsMiuraExecutionTimeout : OnoAction
- (instancetype)initWithCommand:(OnoMiuraCommandType *)command __attribute__((swift_name("init(command:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoMiuraCommandType *)component1 __attribute__((swift_name("component1()")));
- (OnoMiuraActionsMiuraExecutionTimeout *)doCopyCommand:(OnoMiuraCommandType *)command __attribute__((swift_name("doCopy(command:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoMiuraCommandType *command __attribute__((swift_name("command")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.MiuraServiceBusy")))
@interface OnoMiuraActionsMiuraServiceBusy : OnoAction
- (instancetype)initWithAttemptedCommand:(OnoMiuraCommandType *)attemptedCommand currentCommand:(OnoMiuraCommandType *)currentCommand __attribute__((swift_name("init(attemptedCommand:currentCommand:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoMiuraCommandType *)component1 __attribute__((swift_name("component1()")));
- (OnoMiuraCommandType *)component2 __attribute__((swift_name("component2()")));
- (OnoMiuraActionsMiuraServiceBusy *)doCopyAttemptedCommand:(OnoMiuraCommandType *)attemptedCommand currentCommand:(OnoMiuraCommandType *)currentCommand __attribute__((swift_name("doCopy(attemptedCommand:currentCommand:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoMiuraCommandType *attemptedCommand __attribute__((swift_name("attemptedCommand")));
@property (readonly) OnoMiuraCommandType *currentCommand __attribute__((swift_name("currentCommand")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.MiuraStartTimeout")))
@interface OnoMiuraActionsMiuraStartTimeout : OnoAction
- (instancetype)initWithCommand:(OnoMiuraCommandType *)command __attribute__((swift_name("init(command:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoMiuraCommandType *)component1 __attribute__((swift_name("component1()")));
- (OnoMiuraActionsMiuraStartTimeout *)doCopyCommand:(OnoMiuraCommandType *)command __attribute__((swift_name("doCopy(command:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoMiuraCommandType *command __attribute__((swift_name("command")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.P2PEStatusInfo")))
@interface OnoMiuraActionsP2PEStatusInfo : OnoAction
- (instancetype)initWithP2PEStatus:(OnoOnoMiuraP2PEStatus *)P2PEStatus isKeyInjectionVerification:(BOOL)isKeyInjectionVerification __attribute__((swift_name("init(P2PEStatus:isKeyInjectionVerification:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoOnoMiuraP2PEStatus *)component1 __attribute__((swift_name("component1()")));
- (BOOL)component2 __attribute__((swift_name("component2()")));
- (OnoMiuraActionsP2PEStatusInfo *)doCopyP2PEStatus:(OnoOnoMiuraP2PEStatus *)P2PEStatus isKeyInjectionVerification:(BOOL)isKeyInjectionVerification __attribute__((swift_name("doCopy(P2PEStatus:isKeyInjectionVerification:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoOnoMiuraP2PEStatus *P2PEStatus __attribute__((swift_name("P2PEStatus")));
@property (readonly) BOOL isKeyInjectionVerification __attribute__((swift_name("isKeyInjectionVerification")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.PEDConfigInfo")))
@interface OnoMiuraActionsPEDConfigInfo : OnoAction
- (instancetype)initWithPedConfig:(NSDictionary<NSString *, NSString *> *)pedConfig __attribute__((swift_name("init(pedConfig:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSDictionary<NSString *, NSString *> *)component1 __attribute__((swift_name("component1()")));
- (OnoMiuraActionsPEDConfigInfo *)doCopyPedConfig:(NSDictionary<NSString *, NSString *> *)pedConfig __attribute__((swift_name("doCopy(pedConfig:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSDictionary<NSString *, NSString *> *pedConfig __attribute__((swift_name("pedConfig")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.PinDataProvided")))
@interface OnoMiuraActionsPinDataProvided : OnoAction
- (instancetype)initWithPinData:(OnoSensitiveString * _Nullable)pinData pinKsn:(OnoSensitiveString * _Nullable)pinKsn __attribute__((swift_name("init(pinData:pinKsn:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoSensitiveString * _Nullable)component1 __attribute__((swift_name("component1()")));
- (OnoSensitiveString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (OnoMiuraActionsPinDataProvided *)doCopyPinData:(OnoSensitiveString * _Nullable)pinData pinKsn:(OnoSensitiveString * _Nullable)pinKsn __attribute__((swift_name("doCopy(pinData:pinKsn:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoSensitiveString * _Nullable pinData __attribute__((swift_name("pinData")));
@property (readonly) OnoSensitiveString * _Nullable pinKsn __attribute__((swift_name("pinKsn")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.PinEntryCancelledOrTimeout")))
@interface OnoMiuraActionsPinEntryCancelledOrTimeout : OnoAction
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.PinEntryFailed")))
@interface OnoMiuraActionsPinEntryFailed : OnoAction
- (instancetype)initWithReason:(NSString *)reason __attribute__((swift_name("init(reason:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoMiuraActionsPinEntryFailed *)doCopyReason:(NSString *)reason __attribute__((swift_name("doCopy(reason:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *reason __attribute__((swift_name("reason")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.RestartTerminalStatusChecks")))
@interface OnoMiuraActionsRestartTerminalStatusChecks : OnoAction
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.RkiFiles")))
@interface OnoMiuraActionsRkiFiles : OnoAction
- (instancetype)initWithRkiRequestData:(OnoSensitiveMap *)rkiRequestData __attribute__((swift_name("init(rkiRequestData:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoSensitiveMap *)component1 __attribute__((swift_name("component1()")));
- (OnoMiuraActionsRkiFiles *)doCopyRkiRequestData:(OnoSensitiveMap *)rkiRequestData __attribute__((swift_name("doCopy(rkiRequestData:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoSensitiveMap *rkiRequestData __attribute__((swift_name("rkiRequestData")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.SoftwareInfo")))
@interface OnoMiuraActionsSoftwareInfo : OnoAction
- (instancetype)initWithMiuraOS:(OnoOnoMiuraOS *)miuraOS miuraMPI:(OnoOnoMiuraMPI *)miuraMPI __attribute__((swift_name("init(miuraOS:miuraMPI:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoOnoMiuraOS *)component1 __attribute__((swift_name("component1()")));
- (OnoOnoMiuraMPI *)component2 __attribute__((swift_name("component2()")));
- (OnoMiuraActionsSoftwareInfo *)doCopyMiuraOS:(OnoOnoMiuraOS *)miuraOS miuraMPI:(OnoOnoMiuraMPI *)miuraMPI __attribute__((swift_name("doCopy(miuraOS:miuraMPI:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoOnoMiuraMPI *miuraMPI __attribute__((swift_name("miuraMPI")));
@property (readonly) OnoOnoMiuraOS *miuraOS __attribute__((swift_name("miuraOS")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.StartTransaction")))
@interface OnoMiuraActionsStartTransaction : OnoAction
- (instancetype)initWithTransaction:(OnoTransaction *)transaction cardPresentationMode:(OnoCardPromptMode *)cardPresentationMode __attribute__((swift_name("init(transaction:cardPresentationMode:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithTransactionAmount:(int64_t)transactionAmount transactionCurrency:(OnoCurrency *)transactionCurrency cardPresentationMode:(OnoCardPromptMode *)cardPresentationMode __attribute__((swift_name("init(transactionAmount:transactionCurrency:cardPresentationMode:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (int64_t)component1 __attribute__((swift_name("component1()")));
- (OnoCurrency *)component2 __attribute__((swift_name("component2()")));
- (OnoCardPromptMode *)component3 __attribute__((swift_name("component3()")));
- (OnoMiuraActionsStartTransaction *)doCopyTransactionAmount:(int64_t)transactionAmount transactionCurrency:(OnoCurrency *)transactionCurrency cardPresentationMode:(OnoCardPromptMode *)cardPresentationMode __attribute__((swift_name("doCopy(transactionAmount:transactionCurrency:cardPresentationMode:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoCardPromptMode *cardPresentationMode __attribute__((swift_name("cardPresentationMode")));
@property (readonly) int64_t transactionAmount __attribute__((swift_name("transactionAmount")));
@property (readonly) OnoCurrency *transactionCurrency __attribute__((swift_name("transactionCurrency")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.StartTransactionFailed")))
@interface OnoMiuraActionsStartTransactionFailed : OnoAction
- (instancetype)initWithCardPresentationMode:(OnoCardPromptMode *)cardPresentationMode transactionError:(OnoOnoMiuraTransactionError * _Nullable)transactionError __attribute__((swift_name("init(cardPresentationMode:transactionError:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoCardPromptMode *)component1 __attribute__((swift_name("component1()")));
- (OnoOnoMiuraTransactionError * _Nullable)component2 __attribute__((swift_name("component2()")));
- (OnoMiuraActionsStartTransactionFailed *)doCopyCardPresentationMode:(OnoCardPromptMode *)cardPresentationMode transactionError:(OnoOnoMiuraTransactionError * _Nullable)transactionError __attribute__((swift_name("doCopy(cardPresentationMode:transactionError:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoCardPromptMode *cardPresentationMode __attribute__((swift_name("cardPresentationMode")));
@property (readonly) OnoOnoMiuraTransactionError * _Nullable transactionError __attribute__((swift_name("transactionError")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.TerminalConnected")))
@interface OnoMiuraActionsTerminalConnected : OnoAction
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.TerminalConnectionFailed")))
@interface OnoMiuraActionsTerminalConnectionFailed : OnoAction
- (instancetype)initWithErrorMessage:(NSString * _Nullable)errorMessage isStillPaired:(BOOL)isStillPaired __attribute__((swift_name("init(errorMessage:isStillPaired:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString * _Nullable)component1 __attribute__((swift_name("component1()")));
- (BOOL)component2 __attribute__((swift_name("component2()")));
- (OnoMiuraActionsTerminalConnectionFailed *)doCopyErrorMessage:(NSString * _Nullable)errorMessage isStillPaired:(BOOL)isStillPaired __attribute__((swift_name("doCopy(errorMessage:isStillPaired:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable errorMessage __attribute__((swift_name("errorMessage")));
@property (readonly) BOOL isStillPaired __attribute__((swift_name("isStillPaired")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.TerminalDisconnected")))
@interface OnoMiuraActionsTerminalDisconnected : OnoAction
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.TextDisplayFailed")))
@interface OnoMiuraActionsTextDisplayFailed : OnoAction
- (instancetype)initWithText:(OnoOnoMiuraText *)text __attribute__((swift_name("init(text:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoOnoMiuraText *)component1 __attribute__((swift_name("component1()")));
- (OnoMiuraActionsTextDisplayFailed *)doCopyText:(OnoOnoMiuraText *)text __attribute__((swift_name("doCopy(text:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoOnoMiuraText *text __attribute__((swift_name("text")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.TextDisplayedSuccessfully")))
@interface OnoMiuraActionsTextDisplayedSuccessfully : OnoAction
- (instancetype)initWithShownText:(OnoOnoMiuraText *)shownText __attribute__((swift_name("init(shownText:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoOnoMiuraText *)component1 __attribute__((swift_name("component1()")));
- (OnoMiuraActionsTextDisplayedSuccessfully *)doCopyShownText:(OnoOnoMiuraText *)shownText __attribute__((swift_name("doCopy(shownText:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoOnoMiuraText *shownText __attribute__((swift_name("shownText")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.TipAmountEntered")))
@interface OnoMiuraActionsTipAmountEntered : OnoAction
- (instancetype)initWithTipAmount:(int32_t)tipAmount __attribute__((swift_name("init(tipAmount:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (int32_t)component1 __attribute__((swift_name("component1()")));
- (OnoMiuraActionsTipAmountEntered *)doCopyTipAmount:(int32_t)tipAmount __attribute__((swift_name("doCopy(tipAmount:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t tipAmount __attribute__((swift_name("tipAmount")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.TipRequestAborted")))
@interface OnoMiuraActionsTipRequestAborted : OnoAction
- (instancetype)initWithReason:(NSString *)reason __attribute__((swift_name("init(reason:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoMiuraActionsTipRequestAborted *)doCopyReason:(NSString *)reason __attribute__((swift_name("doCopy(reason:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *reason __attribute__((swift_name("reason")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.TransactionAborted")))
@interface OnoMiuraActionsTransactionAborted : OnoAction
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.UnableToAbortTransaction")))
@interface OnoMiuraActionsUnableToAbortTransaction : OnoAction
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.UnknownKeyPressed")))
@interface OnoMiuraActionsUnknownKeyPressed : OnoAction
- (instancetype)initWithPressedKeyIndex:(int32_t)pressedKeyIndex __attribute__((swift_name("init(pressedKeyIndex:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (int32_t)component1 __attribute__((swift_name("component1()")));
- (OnoMiuraActionsUnknownKeyPressed *)doCopyPressedKeyIndex:(int32_t)pressedKeyIndex __attribute__((swift_name("doCopy(pressedKeyIndex:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t pressedKeyIndex __attribute__((swift_name("pressedKeyIndex")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraActions.WritingFileToPed")))
@interface OnoMiuraActionsWritingFileToPed : OnoAction
- (instancetype)initWithFileName:(NSString *)fileName fileSize:(int32_t)fileSize fileNumber:(int32_t)fileNumber totalNumberOfFiles:(int32_t)totalNumberOfFiles __attribute__((swift_name("init(fileName:fileSize:fileNumber:totalNumberOfFiles:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (int32_t)component2 __attribute__((swift_name("component2()")));
- (int32_t)component3 __attribute__((swift_name("component3()")));
- (int32_t)component4 __attribute__((swift_name("component4()")));
- (OnoMiuraActionsWritingFileToPed *)doCopyFileName:(NSString *)fileName fileSize:(int32_t)fileSize fileNumber:(int32_t)fileNumber totalNumberOfFiles:(int32_t)totalNumberOfFiles __attribute__((swift_name("doCopy(fileName:fileSize:fileNumber:totalNumberOfFiles:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *fileName __attribute__((swift_name("fileName")));
@property (readonly) int32_t fileNumber __attribute__((swift_name("fileNumber")));
@property (readonly) int32_t fileSize __attribute__((swift_name("fileSize")));
@property (readonly) int32_t totalNumberOfFiles __attribute__((swift_name("totalNumberOfFiles")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraCommandDriver")))
@interface OnoMiuraCommandDriver : OnoCommandDriver
- (instancetype)initWithMiuraService:(OnoOnoMiuraServiceProtocol *)miuraService __attribute__((swift_name("init(miuraService:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (void)receiveCommandCommand:(OnoCommand *)command __attribute__((swift_name("receiveCommand(command:)")));
@end;

__attribute__((swift_name("MiuraCommands")))
@interface OnoMiuraCommands : OnoCommandGroup
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraCommands.AbortTransaction")))
@interface OnoMiuraCommandsAbortTransaction : OnoCommand
- (instancetype)initWithMessage:(NSString *)message finalizeTransaction:(BOOL)finalizeTransaction __attribute__((swift_name("init(message:finalizeTransaction:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithErrorDetails:(OnoErrorDetails *)errorDetails finalizeTransaction:(BOOL)finalizeTransaction __attribute__((swift_name("init(errorDetails:finalizeTransaction:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoErrorDetails *)component1 __attribute__((swift_name("component1()")));
- (BOOL)component2 __attribute__((swift_name("component2()")));
- (OnoMiuraCommandsAbortTransaction *)doCopyErrorDetails:(OnoErrorDetails *)errorDetails finalizeTransaction:(BOOL)finalizeTransaction __attribute__((swift_name("doCopy(errorDetails:finalizeTransaction:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoErrorDetails *errorDetails __attribute__((swift_name("errorDetails")));
@property (readonly) BOOL finalizeTransaction __attribute__((swift_name("finalizeTransaction")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraCommands.AskForPin")))
@interface OnoMiuraCommandsAskForPin : OnoCommand
- (instancetype)initWithTransactionAmount:(int64_t)transactionAmount transactionCurrency:(OnoCurrency *)transactionCurrency onoMiuraCardData:(OnoOnoMiuraCardData *)onoMiuraCardData pinEnforcedBy:(OnoPinEnforcedBy *)pinEnforcedBy topLineLabel:(NSString *)topLineLabel __attribute__((swift_name("init(transactionAmount:transactionCurrency:onoMiuraCardData:pinEnforcedBy:topLineLabel:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (int64_t)component1 __attribute__((swift_name("component1()")));
- (OnoCurrency *)component2 __attribute__((swift_name("component2()")));
- (OnoOnoMiuraCardData *)component3 __attribute__((swift_name("component3()")));
- (OnoPinEnforcedBy *)component4 __attribute__((swift_name("component4()")));
- (NSString *)component5 __attribute__((swift_name("component5()")));
- (OnoMiuraCommandsAskForPin *)doCopyTransactionAmount:(int64_t)transactionAmount transactionCurrency:(OnoCurrency *)transactionCurrency onoMiuraCardData:(OnoOnoMiuraCardData *)onoMiuraCardData pinEnforcedBy:(OnoPinEnforcedBy *)pinEnforcedBy topLineLabel:(NSString *)topLineLabel __attribute__((swift_name("doCopy(transactionAmount:transactionCurrency:onoMiuraCardData:pinEnforcedBy:topLineLabel:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoOnoMiuraCardData *onoMiuraCardData __attribute__((swift_name("onoMiuraCardData")));
@property (readonly) OnoPinEnforcedBy *pinEnforcedBy __attribute__((swift_name("pinEnforcedBy")));
@property (readonly) NSString *topLineLabel __attribute__((swift_name("topLineLabel")));
@property (readonly) int64_t transactionAmount __attribute__((swift_name("transactionAmount")));
@property (readonly) OnoCurrency *transactionCurrency __attribute__((swift_name("transactionCurrency")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraCommands.AskForTipAmount")))
@interface OnoMiuraCommandsAskForTipAmount : OnoCommand
- (instancetype)initWithTopLinePromptIndex:(int32_t)topLinePromptIndex secondLinePromptIndex:(int32_t)secondLinePromptIndex thirdLinePromptIndex:(int32_t)thirdLinePromptIndex maxNumIntDigits:(int32_t)maxNumIntDigits numFractalDigits:(int32_t)numFractalDigits numberPlaceHolder:(NSString *)numberPlaceHolder __attribute__((swift_name("init(topLinePromptIndex:secondLinePromptIndex:thirdLinePromptIndex:maxNumIntDigits:numFractalDigits:numberPlaceHolder:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (int32_t)component1 __attribute__((swift_name("component1()")));
- (int32_t)component2 __attribute__((swift_name("component2()")));
- (int32_t)component3 __attribute__((swift_name("component3()")));
- (int32_t)component4 __attribute__((swift_name("component4()")));
- (int32_t)component5 __attribute__((swift_name("component5()")));
- (NSString *)component6 __attribute__((swift_name("component6()")));
- (OnoMiuraCommandsAskForTipAmount *)doCopyTopLinePromptIndex:(int32_t)topLinePromptIndex secondLinePromptIndex:(int32_t)secondLinePromptIndex thirdLinePromptIndex:(int32_t)thirdLinePromptIndex maxNumIntDigits:(int32_t)maxNumIntDigits numFractalDigits:(int32_t)numFractalDigits numberPlaceHolder:(NSString *)numberPlaceHolder __attribute__((swift_name("doCopy(topLinePromptIndex:secondLinePromptIndex:thirdLinePromptIndex:maxNumIntDigits:numFractalDigits:numberPlaceHolder:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t maxNumIntDigits __attribute__((swift_name("maxNumIntDigits")));
@property (readonly) int32_t numFractalDigits __attribute__((swift_name("numFractalDigits")));
@property (readonly) NSString *numberPlaceHolder __attribute__((swift_name("numberPlaceHolder")));
@property (readonly) int32_t secondLinePromptIndex __attribute__((swift_name("secondLinePromptIndex")));
@property (readonly) int32_t thirdLinePromptIndex __attribute__((swift_name("thirdLinePromptIndex")));
@property (readonly) int32_t topLinePromptIndex __attribute__((swift_name("topLinePromptIndex")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraCommands.ClearAllMiuraCommands")))
@interface OnoMiuraCommandsClearAllMiuraCommands : OnoCommand
- (instancetype)initWithReason:(NSString *)reason __attribute__((swift_name("init(reason:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoMiuraCommandsClearAllMiuraCommands *)doCopyReason:(NSString *)reason __attribute__((swift_name("doCopy(reason:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *reason __attribute__((swift_name("reason")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraCommands.ClearMiuraCommand")))
@interface OnoMiuraCommandsClearMiuraCommand : OnoCommand
- (instancetype)initWithCommand:(OnoQueueCommand *)command __attribute__((swift_name("init(command:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoQueueCommand *)component1 __attribute__((swift_name("component1()")));
- (OnoMiuraCommandsClearMiuraCommand *)doCopyCommand:(OnoQueueCommand *)command __attribute__((swift_name("doCopy(command:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoQueueCommand *command __attribute__((swift_name("command")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraCommands.ConnectTerminal")))
@interface OnoMiuraCommandsConnectTerminal : OnoCommand
- (instancetype)initWithTerminal:(OnoTerminal *)terminal __attribute__((swift_name("init(terminal:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoTerminal *)component1 __attribute__((swift_name("component1()")));
- (OnoMiuraCommandsConnectTerminal *)doCopyTerminal:(OnoTerminal *)terminal __attribute__((swift_name("doCopy(terminal:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoTerminal *terminal __attribute__((swift_name("terminal")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraCommands.DelayGetBatteryStatusInfo")))
@interface OnoMiuraCommandsDelayGetBatteryStatusInfo : OnoCommand
- (instancetype)initWithDelayInSeconds:(int32_t)delayInSeconds __attribute__((swift_name("init(delayInSeconds:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (int32_t)component1 __attribute__((swift_name("component1()")));
- (OnoMiuraCommandsDelayGetBatteryStatusInfo *)doCopyDelayInSeconds:(int32_t)delayInSeconds __attribute__((swift_name("doCopy(delayInSeconds:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t delayInSeconds __attribute__((swift_name("delayInSeconds")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraCommands.DelaySetTerminalAvailable")))
@interface OnoMiuraCommandsDelaySetTerminalAvailable : OnoCommand
- (instancetype)initWithTerminal:(OnoTerminal *)terminal __attribute__((swift_name("init(terminal:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoTerminal *)component1 __attribute__((swift_name("component1()")));
- (OnoMiuraCommandsDelaySetTerminalAvailable *)doCopyTerminal:(OnoTerminal *)terminal __attribute__((swift_name("doCopy(terminal:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoTerminal *terminal __attribute__((swift_name("terminal")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraCommands.DisconnectTerminal")))
@interface OnoMiuraCommandsDisconnectTerminal : OnoCommand
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraCommands.DisplayText")))
@interface OnoMiuraCommandsDisplayText : OnoCommand
- (instancetype)initWithMiuraDisplayText:(OnoOnoMiuraText *)miuraDisplayText __attribute__((swift_name("init(miuraDisplayText:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoOnoMiuraText *)component1 __attribute__((swift_name("component1()")));
- (OnoMiuraCommandsDisplayText *)doCopyMiuraDisplayText:(OnoOnoMiuraText *)miuraDisplayText __attribute__((swift_name("doCopy(miuraDisplayText:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoOnoMiuraText *miuraDisplayText __attribute__((swift_name("miuraDisplayText")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraCommands.DoYouWantToTip")))
@interface OnoMiuraCommandsDoYouWantToTip : OnoCommand
- (instancetype)initWithPromptMessage:(OnoOnoMiuraText *)promptMessage __attribute__((swift_name("init(promptMessage:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoOnoMiuraText *)component1 __attribute__((swift_name("component1()")));
- (OnoMiuraCommandsDoYouWantToTip *)doCopyPromptMessage:(OnoOnoMiuraText *)promptMessage __attribute__((swift_name("doCopy(promptMessage:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoOnoMiuraText *promptMessage __attribute__((swift_name("promptMessage")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraCommands.EnableKeypad")))
@interface OnoMiuraCommandsEnableKeypad : OnoCommand
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraCommands.GetBatteryStatusInfo")))
@interface OnoMiuraCommandsGetBatteryStatusInfo : OnoCommand
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraCommands.GetBluetoothInfo")))
@interface OnoMiuraCommandsGetBluetoothInfo : OnoCommand
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraCommands.GetDeviceCapabilities")))
@interface OnoMiuraCommandsGetDeviceCapabilities : OnoCommand
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraCommands.GetP2PEFiles")))
@interface OnoMiuraCommandsGetP2PEFiles : OnoCommand
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraCommands.GetP2PEStatusInfo")))
@interface OnoMiuraCommandsGetP2PEStatusInfo : OnoCommand
- (instancetype)initWithIsKeyInjectionVerification:(BOOL)isKeyInjectionVerification __attribute__((swift_name("init(isKeyInjectionVerification:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (BOOL)component1 __attribute__((swift_name("component1()")));
- (OnoMiuraCommandsGetP2PEStatusInfo *)doCopyIsKeyInjectionVerification:(BOOL)isKeyInjectionVerification __attribute__((swift_name("doCopy(isKeyInjectionVerification:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL isKeyInjectionVerification __attribute__((swift_name("isKeyInjectionVerification")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraCommands.GetPEDConfigInfo")))
@interface OnoMiuraCommandsGetPEDConfigInfo : OnoCommand
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraCommands.GetTerminalSoftwareInfo")))
@interface OnoMiuraCommandsGetTerminalSoftwareInfo : OnoCommand
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraCommands.HardReset")))
@interface OnoMiuraCommandsHardReset : OnoCommand
- (instancetype)initWithDispatchOnReconnect:(NSArray<OnoAction *> *)dispatchOnReconnect dispatchOnDisconnected:(NSArray<OnoAction *> *)dispatchOnDisconnected __attribute__((swift_name("init(dispatchOnReconnect:dispatchOnDisconnected:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSArray<OnoAction *> *)component1 __attribute__((swift_name("component1()")));
- (NSArray<OnoAction *> *)component2 __attribute__((swift_name("component2()")));
- (OnoMiuraCommandsHardReset *)doCopyDispatchOnReconnect:(NSArray<OnoAction *> *)dispatchOnReconnect dispatchOnDisconnected:(NSArray<OnoAction *> *)dispatchOnDisconnected __attribute__((swift_name("doCopy(dispatchOnReconnect:dispatchOnDisconnected:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<OnoAction *> *dispatchOnDisconnected __attribute__((swift_name("dispatchOnDisconnected")));
@property (readonly) NSArray<OnoAction *> *dispatchOnReconnect __attribute__((swift_name("dispatchOnReconnect")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraCommands.IdleSession")))
@interface OnoMiuraCommandsIdleSession : OnoCommand
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraCommands.InjectKeys")))
@interface OnoMiuraCommandsInjectKeys : OnoCommand
- (instancetype)initWithKeysData:(OnoSensitiveMap *)keysData __attribute__((swift_name("init(keysData:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoSensitiveMap *)component1 __attribute__((swift_name("component1()")));
- (OnoMiuraCommandsInjectKeys *)doCopyKeysData:(OnoSensitiveMap *)keysData __attribute__((swift_name("doCopy(keysData:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoSensitiveMap *keysData __attribute__((swift_name("keysData")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraCommands.PerformUpdate")))
@interface OnoMiuraCommandsPerformUpdate : OnoCommand
- (instancetype)initWithAvailableUpdate:(OnoAvailableUpdate *)availableUpdate os:(OnoOnoMiuraOS * _Nullable)os __attribute__((swift_name("init(availableUpdate:os:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoAvailableUpdate *)component1 __attribute__((swift_name("component1()")));
- (OnoOnoMiuraOS * _Nullable)component2 __attribute__((swift_name("component2()")));
- (OnoMiuraCommandsPerformUpdate *)doCopyAvailableUpdate:(OnoAvailableUpdate *)availableUpdate os:(OnoOnoMiuraOS * _Nullable)os __attribute__((swift_name("doCopy(availableUpdate:os:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoAvailableUpdate *availableUpdate __attribute__((swift_name("availableUpdate")));
@property (readonly) OnoOnoMiuraOS * _Nullable os __attribute__((swift_name("os")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraCommands.ProcessArpc")))
@interface OnoMiuraCommandsProcessArpc : OnoCommand
- (instancetype)initWithAuthorizationARPC:(OnoSensitiveString *)authorizationARPC __attribute__((swift_name("init(authorizationARPC:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoSensitiveString *)component1 __attribute__((swift_name("component1()")));
- (OnoMiuraCommandsProcessArpc *)doCopyAuthorizationARPC:(OnoSensitiveString *)authorizationARPC __attribute__((swift_name("doCopy(authorizationARPC:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoSensitiveString *authorizationARPC __attribute__((swift_name("authorizationARPC")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraCommands.ProcessEmvTransaction")))
@interface OnoMiuraCommandsProcessEmvTransaction : OnoCommand
- (instancetype)initWithTransactionAmount:(int64_t)transactionAmount transactionCurrency:(OnoCurrency *)transactionCurrency __attribute__((swift_name("init(transactionAmount:transactionCurrency:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (int64_t)component1 __attribute__((swift_name("component1()")));
- (OnoCurrency *)component2 __attribute__((swift_name("component2()")));
- (OnoMiuraCommandsProcessEmvTransaction *)doCopyTransactionAmount:(int64_t)transactionAmount transactionCurrency:(OnoCurrency *)transactionCurrency __attribute__((swift_name("doCopy(transactionAmount:transactionCurrency:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int64_t transactionAmount __attribute__((swift_name("transactionAmount")));
@property (readonly) OnoCurrency *transactionCurrency __attribute__((swift_name("transactionCurrency")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraCommands.ShowIdleTextOnSchedule")))
@interface OnoMiuraCommandsShowIdleTextOnSchedule : OnoCommand
- (instancetype)initWithShowAfterMillis:(int64_t)showAfterMillis __attribute__((swift_name("init(showAfterMillis:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (int64_t)component1 __attribute__((swift_name("component1()")));
- (OnoMiuraCommandsShowIdleTextOnSchedule *)doCopyShowAfterMillis:(int64_t)showAfterMillis __attribute__((swift_name("doCopy(showAfterMillis:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int64_t showAfterMillis __attribute__((swift_name("showAfterMillis")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraCommands.StartTransaction")))
@interface OnoMiuraCommandsStartTransaction : OnoCommand
- (instancetype)initWithTransactionAmount:(int64_t)transactionAmount transactionCurrency:(OnoCurrency *)transactionCurrency cardPresentationMode:(OnoCardPromptMode *)cardPresentationMode __attribute__((swift_name("init(transactionAmount:transactionCurrency:cardPresentationMode:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (int64_t)component1 __attribute__((swift_name("component1()")));
- (OnoCurrency *)component2 __attribute__((swift_name("component2()")));
- (OnoCardPromptMode *)component3 __attribute__((swift_name("component3()")));
- (OnoMiuraCommandsStartTransaction *)doCopyTransactionAmount:(int64_t)transactionAmount transactionCurrency:(OnoCurrency *)transactionCurrency cardPresentationMode:(OnoCardPromptMode *)cardPresentationMode __attribute__((swift_name("doCopy(transactionAmount:transactionCurrency:cardPresentationMode:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoCardPromptMode *cardPresentationMode __attribute__((swift_name("cardPresentationMode")));
@property (readonly) int64_t transactionAmount __attribute__((swift_name("transactionAmount")));
@property (readonly) OnoCurrency *transactionCurrency __attribute__((swift_name("transactionCurrency")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraCommands.TransactionCompleted")))
@interface OnoMiuraCommandsTransactionCompleted : OnoCommand
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraCommands.UnregisterDeviceConnectionListeners")))
@interface OnoMiuraCommandsUnregisterDeviceConnectionListeners : OnoCommand
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraStateMachine")))
@interface OnoMiuraStateMachine : OnoBase <OnoStateMachine>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)miuraStateMachine __attribute__((swift_name("init()")));
- (OnoMiuraStateMachineMiuraData *)castStoreDataStoreData:(id<OnoStoreData>)storeData __attribute__((swift_name("castStoreData(storeData:)")));
- (OnoStateMachineResult *)consumeActionStoreData:(id<OnoStoreData>)storeData action:(OnoAction *)action __attribute__((swift_name("consumeAction(storeData:action:)")));
- (OnoMiuraStateMachineMiuraData *)getCurrentStateStore:(OnoStore *)store __attribute__((swift_name("getCurrentState(store:)")));
- (OnoMiuraStateMachineMiuraData *)getCurrentStateState:(NSDictionary<NSString *, id<OnoStoreData>> *)state __attribute__((swift_name("getCurrentState(state:)")));
- (OnoMiuraStateMachineMiuraData *)getStoreDataState:(NSDictionary<NSString *, id<OnoStoreData>> *)state __attribute__((swift_name("getStoreData(state:)")));
@property (readonly) OnoMiuraStateMachineMiuraData *initialState __attribute__((swift_name("initialState")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly, getter=tag_) NSString *tag __attribute__((swift_name("tag")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraStateMachine.AbortMode")))
@interface OnoMiuraStateMachineAbortMode : OnoBase
- (instancetype)initWithIsAbortForNewTransaction:(BOOL)isAbortForNewTransaction isAbortTipping:(BOOL)isAbortTipping __attribute__((swift_name("init(isAbortForNewTransaction:isAbortTipping:)"))) __attribute__((objc_designated_initializer));
- (BOOL)component1 __attribute__((swift_name("component1()")));
- (BOOL)component2 __attribute__((swift_name("component2()")));
- (OnoMiuraStateMachineAbortMode *)doCopyIsAbortForNewTransaction:(BOOL)isAbortForNewTransaction isAbortTipping:(BOOL)isAbortTipping __attribute__((swift_name("doCopy(isAbortForNewTransaction:isAbortTipping:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL isAbortForNewTransaction __attribute__((swift_name("isAbortForNewTransaction")));
@property (readonly) BOOL isAbortTipping __attribute__((swift_name("isAbortTipping")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraStateMachine.ConnectionData")))
@interface OnoMiuraStateMachineConnectionData : OnoBase
- (instancetype)initWithConnectionStatus:(OnoMiuraConnectionStatus * _Nullable)connectionStatus connectingTerminal:(OnoTerminal * _Nullable)connectingTerminal miuraOS:(OnoOnoMiuraOS * _Nullable)miuraOS miuraMPI:(OnoOnoMiuraMPI * _Nullable)miuraMPI p2peStatus:(OnoOnoMiuraP2PEStatus * _Nullable)p2peStatus pedConfig:(NSDictionary<NSString *, NSString *> * _Nullable)pedConfig accessory:(OnoOnoMiuraAccessory * _Nullable)accessory batteryStatus:(OnoOnoMiuraBatteryStatus * _Nullable)batteryStatus bluetoothInfo:(NSDictionary<NSString *, NSString *> * _Nullable)bluetoothInfo deviceCapabilities:(NSArray<OnoOnoMiuraDeviceCapability *> * _Nullable)deviceCapabilities bluetoothDeviceConnected:(OnoBoolean * _Nullable)bluetoothDeviceConnected statusUpdateChecks:(OnoInt * _Nullable)statusUpdateChecks __attribute__((swift_name("init(connectionStatus:connectingTerminal:miuraOS:miuraMPI:p2peStatus:pedConfig:accessory:batteryStatus:bluetoothInfo:deviceCapabilities:bluetoothDeviceConnected:statusUpdateChecks:)"))) __attribute__((objc_designated_initializer));
- (OnoMiuraConnectionStatus * _Nullable)component1 __attribute__((swift_name("component1()")));
- (NSArray<OnoOnoMiuraDeviceCapability *> * _Nullable)component10 __attribute__((swift_name("component10()")));
- (OnoBoolean * _Nullable)component11 __attribute__((swift_name("component11()")));
- (OnoInt * _Nullable)component12 __attribute__((swift_name("component12()")));
- (OnoTerminal * _Nullable)component2 __attribute__((swift_name("component2()")));
- (OnoOnoMiuraOS * _Nullable)component3 __attribute__((swift_name("component3()")));
- (OnoOnoMiuraMPI * _Nullable)component4 __attribute__((swift_name("component4()")));
- (OnoOnoMiuraP2PEStatus * _Nullable)component5 __attribute__((swift_name("component5()")));
- (NSDictionary<NSString *, NSString *> * _Nullable)component6 __attribute__((swift_name("component6()")));
- (OnoOnoMiuraAccessory * _Nullable)component7 __attribute__((swift_name("component7()")));
- (OnoOnoMiuraBatteryStatus * _Nullable)component8 __attribute__((swift_name("component8()")));
- (NSDictionary<NSString *, NSString *> * _Nullable)component9 __attribute__((swift_name("component9()")));
- (OnoMiuraStateMachineConnectionData *)doCopyConnectionStatus:(OnoMiuraConnectionStatus * _Nullable)connectionStatus connectingTerminal:(OnoTerminal * _Nullable)connectingTerminal miuraOS:(OnoOnoMiuraOS * _Nullable)miuraOS miuraMPI:(OnoOnoMiuraMPI * _Nullable)miuraMPI p2peStatus:(OnoOnoMiuraP2PEStatus * _Nullable)p2peStatus pedConfig:(NSDictionary<NSString *, NSString *> * _Nullable)pedConfig accessory:(OnoOnoMiuraAccessory * _Nullable)accessory batteryStatus:(OnoOnoMiuraBatteryStatus * _Nullable)batteryStatus bluetoothInfo:(NSDictionary<NSString *, NSString *> * _Nullable)bluetoothInfo deviceCapabilities:(NSArray<OnoOnoMiuraDeviceCapability *> * _Nullable)deviceCapabilities bluetoothDeviceConnected:(OnoBoolean * _Nullable)bluetoothDeviceConnected statusUpdateChecks:(OnoInt * _Nullable)statusUpdateChecks __attribute__((swift_name("doCopy(connectionStatus:connectingTerminal:miuraOS:miuraMPI:p2peStatus:pedConfig:accessory:batteryStatus:bluetoothInfo:deviceCapabilities:bluetoothDeviceConnected:statusUpdateChecks:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoOnoMiuraAccessory * _Nullable accessory __attribute__((swift_name("accessory")));
@property (readonly) OnoOnoMiuraBatteryStatus * _Nullable batteryStatus __attribute__((swift_name("batteryStatus")));
@property (readonly) OnoBoolean * _Nullable bluetoothDeviceConnected __attribute__((swift_name("bluetoothDeviceConnected")));
@property (readonly) NSDictionary<NSString *, NSString *> * _Nullable bluetoothInfo __attribute__((swift_name("bluetoothInfo")));
@property (readonly) OnoTerminal * _Nullable connectingTerminal __attribute__((swift_name("connectingTerminal")));
@property (readonly) OnoMiuraConnectionStatus * _Nullable connectionStatus __attribute__((swift_name("connectionStatus")));
@property (readonly) NSArray<OnoOnoMiuraDeviceCapability *> * _Nullable deviceCapabilities __attribute__((swift_name("deviceCapabilities")));
@property (readonly) OnoOnoMiuraMPI * _Nullable miuraMPI __attribute__((swift_name("miuraMPI")));
@property (readonly) OnoOnoMiuraOS * _Nullable miuraOS __attribute__((swift_name("miuraOS")));
@property (readonly) OnoOnoMiuraP2PEStatus * _Nullable p2peStatus __attribute__((swift_name("p2peStatus")));
@property (readonly) NSDictionary<NSString *, NSString *> * _Nullable pedConfig __attribute__((swift_name("pedConfig")));
@property (readonly) OnoInt * _Nullable statusUpdateChecks __attribute__((swift_name("statusUpdateChecks")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraStateMachine.MiuraData")))
@interface OnoMiuraStateMachineMiuraData : OnoBase <OnoStoreData>
- (instancetype)initWithConnectionData:(OnoMiuraStateMachineConnectionData *)connectionData transactionData:(OnoMiuraStateMachineTransactionData * _Nullable)transactionData tipPromptSettings:(OnoMiuraStateMachineTipPromptSettings *)tipPromptSettings totalStatusUpdateChecksRequired:(int32_t)totalStatusUpdateChecksRequired statusUpdateCheckDelay:(int32_t)statusUpdateCheckDelay updateCompleteLengthSeconds:(int32_t)updateCompleteLengthSeconds __attribute__((swift_name("init(connectionData:transactionData:tipPromptSettings:totalStatusUpdateChecksRequired:statusUpdateCheckDelay:updateCompleteLengthSeconds:)"))) __attribute__((objc_designated_initializer));
- (OnoMiuraStateMachineConnectionData *)component1 __attribute__((swift_name("component1()")));
- (OnoMiuraStateMachineTransactionData * _Nullable)component2 __attribute__((swift_name("component2()")));
- (OnoMiuraStateMachineTipPromptSettings *)component3 __attribute__((swift_name("component3()")));
- (int32_t)component4 __attribute__((swift_name("component4()")));
- (int32_t)component5 __attribute__((swift_name("component5()")));
- (int32_t)component6 __attribute__((swift_name("component6()")));
- (OnoMiuraStateMachineMiuraData *)doCopyConnectionData:(OnoMiuraStateMachineConnectionData *)connectionData transactionData:(OnoMiuraStateMachineTransactionData * _Nullable)transactionData tipPromptSettings:(OnoMiuraStateMachineTipPromptSettings *)tipPromptSettings totalStatusUpdateChecksRequired:(int32_t)totalStatusUpdateChecksRequired statusUpdateCheckDelay:(int32_t)statusUpdateCheckDelay updateCompleteLengthSeconds:(int32_t)updateCompleteLengthSeconds __attribute__((swift_name("doCopy(connectionData:transactionData:tipPromptSettings:totalStatusUpdateChecksRequired:statusUpdateCheckDelay:updateCompleteLengthSeconds:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoMiuraStateMachineConnectionData *connectionData __attribute__((swift_name("connectionData")));
@property (readonly) int32_t statusUpdateCheckDelay __attribute__((swift_name("statusUpdateCheckDelay")));
@property (readonly) OnoMiuraStateMachineTipPromptSettings *tipPromptSettings __attribute__((swift_name("tipPromptSettings")));
@property (readonly) int32_t totalStatusUpdateChecksRequired __attribute__((swift_name("totalStatusUpdateChecksRequired")));
@property (readonly) OnoMiuraStateMachineTransactionData * _Nullable transactionData __attribute__((swift_name("transactionData")));
@property (readonly) int32_t updateCompleteLengthSeconds __attribute__((swift_name("updateCompleteLengthSeconds")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraStateMachine.TipPromptSettings")))
@interface OnoMiuraStateMachineTipPromptSettings : OnoBase
- (instancetype)initWithTopLinePromptIndex:(int32_t)topLinePromptIndex secondLinePromptIndex:(int32_t)secondLinePromptIndex thirdLinePromptIndex:(int32_t)thirdLinePromptIndex maxNumIntDigits:(int32_t)maxNumIntDigits numFractalDigits:(int32_t)numFractalDigits numberPlaceHolder:(NSString *)numberPlaceHolder __attribute__((swift_name("init(topLinePromptIndex:secondLinePromptIndex:thirdLinePromptIndex:maxNumIntDigits:numFractalDigits:numberPlaceHolder:)"))) __attribute__((objc_designated_initializer));
- (int32_t)component1 __attribute__((swift_name("component1()")));
- (int32_t)component2 __attribute__((swift_name("component2()")));
- (int32_t)component3 __attribute__((swift_name("component3()")));
- (int32_t)component4 __attribute__((swift_name("component4()")));
- (int32_t)component5 __attribute__((swift_name("component5()")));
- (NSString *)component6 __attribute__((swift_name("component6()")));
- (OnoMiuraStateMachineTipPromptSettings *)doCopyTopLinePromptIndex:(int32_t)topLinePromptIndex secondLinePromptIndex:(int32_t)secondLinePromptIndex thirdLinePromptIndex:(int32_t)thirdLinePromptIndex maxNumIntDigits:(int32_t)maxNumIntDigits numFractalDigits:(int32_t)numFractalDigits numberPlaceHolder:(NSString *)numberPlaceHolder __attribute__((swift_name("doCopy(topLinePromptIndex:secondLinePromptIndex:thirdLinePromptIndex:maxNumIntDigits:numFractalDigits:numberPlaceHolder:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
- (OnoMiuraStateMachineTipPromptSettings *)updatedFromMapMap:(NSDictionary<NSString *, id> *)map __attribute__((swift_name("updatedFromMap(map:)")));
@property (readonly) int32_t maxNumIntDigits __attribute__((swift_name("maxNumIntDigits")));
@property (readonly) int32_t numFractalDigits __attribute__((swift_name("numFractalDigits")));
@property (readonly) NSString *numberPlaceHolder __attribute__((swift_name("numberPlaceHolder")));
@property (readonly) int32_t secondLinePromptIndex __attribute__((swift_name("secondLinePromptIndex")));
@property (readonly) int32_t thirdLinePromptIndex __attribute__((swift_name("thirdLinePromptIndex")));
@property (readonly) int32_t topLinePromptIndex __attribute__((swift_name("topLinePromptIndex")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraStateMachine.TransactionData")))
@interface OnoMiuraStateMachineTransactionData : OnoBase
- (instancetype)initWithTransactionAmount:(int64_t)transactionAmount transactionCurrency:(OnoCurrency *)transactionCurrency transactionStep:(OnoOnoMiuraTransactionStep *)transactionStep cardAskingMode:(OnoCardPromptMode *)cardAskingMode cardProvidedMode:(OnoTransactionType * _Nullable)cardProvidedMode pinEventCount:(int32_t)pinEventCount askForPin:(BOOL)askForPin askedAppSelection:(BOOL)askedAppSelection abortMode:(OnoMiuraStateMachineAbortMode *)abortMode cardData:(OnoOnoMiuraCardData * _Nullable)cardData __attribute__((swift_name("init(transactionAmount:transactionCurrency:transactionStep:cardAskingMode:cardProvidedMode:pinEventCount:askForPin:askedAppSelection:abortMode:cardData:)"))) __attribute__((objc_designated_initializer));
- (int64_t)component1 __attribute__((swift_name("component1()")));
- (OnoOnoMiuraCardData * _Nullable)component10 __attribute__((swift_name("component10()")));
- (OnoCurrency *)component2 __attribute__((swift_name("component2()")));
- (OnoOnoMiuraTransactionStep *)component3 __attribute__((swift_name("component3()")));
- (OnoCardPromptMode *)component4 __attribute__((swift_name("component4()")));
- (OnoTransactionType * _Nullable)component5 __attribute__((swift_name("component5()")));
- (int32_t)component6 __attribute__((swift_name("component6()")));
- (BOOL)component7 __attribute__((swift_name("component7()")));
- (BOOL)component8 __attribute__((swift_name("component8()")));
- (OnoMiuraStateMachineAbortMode *)component9 __attribute__((swift_name("component9()")));
- (OnoMiuraStateMachineTransactionData *)doCopyTransactionAmount:(int64_t)transactionAmount transactionCurrency:(OnoCurrency *)transactionCurrency transactionStep:(OnoOnoMiuraTransactionStep *)transactionStep cardAskingMode:(OnoCardPromptMode *)cardAskingMode cardProvidedMode:(OnoTransactionType * _Nullable)cardProvidedMode pinEventCount:(int32_t)pinEventCount askForPin:(BOOL)askForPin askedAppSelection:(BOOL)askedAppSelection abortMode:(OnoMiuraStateMachineAbortMode *)abortMode cardData:(OnoOnoMiuraCardData * _Nullable)cardData __attribute__((swift_name("doCopy(transactionAmount:transactionCurrency:transactionStep:cardAskingMode:cardProvidedMode:pinEventCount:askForPin:askedAppSelection:abortMode:cardData:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoMiuraStateMachineAbortMode *abortMode __attribute__((swift_name("abortMode")));
@property (readonly) BOOL askForPin __attribute__((swift_name("askForPin")));
@property (readonly) BOOL askedAppSelection __attribute__((swift_name("askedAppSelection")));
@property (readonly) OnoCardPromptMode *cardAskingMode __attribute__((swift_name("cardAskingMode")));
@property (readonly) OnoOnoMiuraCardData * _Nullable cardData __attribute__((swift_name("cardData")));
@property (readonly) OnoTransactionType * _Nullable cardProvidedMode __attribute__((swift_name("cardProvidedMode")));
@property (readonly) int32_t pinEventCount __attribute__((swift_name("pinEventCount")));
@property (readonly) int64_t transactionAmount __attribute__((swift_name("transactionAmount")));
@property (readonly) OnoCurrency *transactionCurrency __attribute__((swift_name("transactionCurrency")));
@property (readonly) OnoOnoMiuraTransactionStep *transactionStep __attribute__((swift_name("transactionStep")));
@end;

__attribute__((swift_name("OnoMiuraServiceProtocol")))
@interface OnoOnoMiuraServiceProtocol : OnoDispatcher
- (instancetype)initWithDispatch:(void (^)(OnoAction *))dispatch __attribute__((swift_name("init(dispatch:)"))) __attribute__((objc_designated_initializer));
- (void)abortTransactionErrorDetails:(OnoErrorDetails *)errorDetails finalizeTransaction:(BOOL)finalizeTransaction __attribute__((swift_name("abortTransaction(errorDetails:finalizeTransaction:)")));
- (void)askForPinTransactionAmount:(int64_t)transactionAmount transactionCurrency:(OnoCurrency *)transactionCurrency onoMiuraCardData:(OnoOnoMiuraCardData *)onoMiuraCardData topLineLabel:(NSString *)topLineLabel __attribute__((swift_name("askForPin(transactionAmount:transactionCurrency:onoMiuraCardData:topLineLabel:)")));
- (void)askForTipAmountTopLinePromptIndex:(int32_t)topLinePromptIndex secondLinePromptIndex:(int32_t)secondLinePromptIndex thirdLinePromptIndex:(int32_t)thirdLinePromptIndex maxNumIntDigits:(int32_t)maxNumIntDigits numFractalDigits:(int32_t)numFractalDigits numberPlaceHolder:(NSString *)numberPlaceHolder __attribute__((swift_name("askForTipAmount(topLinePromptIndex:secondLinePromptIndex:thirdLinePromptIndex:maxNumIntDigits:numFractalDigits:numberPlaceHolder:)")));
- (void)clearAllCommandFromQueueReason:(NSString *)reason __attribute__((swift_name("clearAllCommandFromQueue(reason:)")));
- (void)clearCommandFromQueueCommand:(OnoQueueCommand *)command __attribute__((swift_name("clearCommandFromQueue(command:)")));
- (void)connectTerminal:(OnoTerminal *)terminal __attribute__((swift_name("connect(terminal:)")));
- (void)delayGetDeviceBatteryStatusDelayInSeconds:(int32_t)delayInSeconds __attribute__((swift_name("delayGetDeviceBatteryStatus(delayInSeconds:)")));
- (void)delaySetTerminalAvailableTerminal:(OnoTerminal *)terminal __attribute__((swift_name("delaySetTerminalAvailable(terminal:)")));
- (void)disconnect __attribute__((swift_name("disconnect()")));
- (void)displayTextMiuraText:(OnoOnoMiuraText *)miuraText __attribute__((swift_name("displayText(miuraText:)")));
- (void)doEMVTransactionAmount:(int64_t)transactionAmount transactionCurrency:(OnoCurrency *)transactionCurrency __attribute__((swift_name("doEMV(transactionAmount:transactionCurrency:)")));
- (void)doYouWantToTipPromptPromptMessage:(OnoOnoMiuraText *)promptMessage __attribute__((swift_name("doYouWantToTipPrompt(promptMessage:)")));
- (void)enableKeypad __attribute__((swift_name("enableKeypad()")));
- (void)getBatteryStatus __attribute__((swift_name("getBatteryStatus()")));
- (void)getBluetoothInfo __attribute__((swift_name("getBluetoothInfo()")));
- (void)getDeviceCapabilities __attribute__((swift_name("getDeviceCapabilities()")));
- (void)getDeviceP2PEFiles __attribute__((swift_name("getDeviceP2PEFiles()")));
- (void)getP2PEStatusIsKeyInjectionVerification:(BOOL)isKeyInjectionVerification __attribute__((swift_name("getP2PEStatus(isKeyInjectionVerification:)")));
- (void)getPEDConfig __attribute__((swift_name("getPEDConfig()")));
- (void)getSoftwareInfo __attribute__((swift_name("getSoftwareInfo()")));
- (void)hardResetAndReconnectDeviceDispatchOnReconnect:(NSArray<OnoAction *> *)dispatchOnReconnect dispatchOnDisconnected:(NSArray<OnoAction *> *)dispatchOnDisconnected __attribute__((swift_name("hardResetAndReconnectDevice(dispatchOnReconnect:dispatchOnDisconnected:)")));
- (void)idleSessionMiuraText:(OnoOnoMiuraText *)miuraText __attribute__((swift_name("idleSession(miuraText:)")));
- (void)injectKeysKeyInjectionDataMap:(OnoSensitiveMap *)keyInjectionDataMap __attribute__((swift_name("injectKeys(keyInjectionDataMap:)")));
- (void)performUpdateAvailableUpdate:(OnoAvailableUpdate *)availableUpdate os:(OnoOnoMiuraOS * _Nullable)os __attribute__((swift_name("performUpdate(availableUpdate:os:)")));
- (void)processARPCArpc:(OnoSensitiveString *)arpc __attribute__((swift_name("processARPC(arpc:)")));
- (void)rkiFinishedWasSuccessful:(BOOL)wasSuccessful errorMessage:(NSString * _Nullable)errorMessage __attribute__((swift_name("rkiFinished(wasSuccessful:errorMessage:)")));
- (void)showIdleTextAfterMillisTimeMillis:(int64_t)timeMillis __attribute__((swift_name("showIdleTextAfterMillis(timeMillis:)")));
- (void)startTransactionTransactionAmount:(int64_t)transactionAmount transactionCurrency:(OnoCurrency *)transactionCurrency cardPresentationMode:(OnoCardPromptMode *)cardPresentationMode __attribute__((swift_name("startTransaction(transactionAmount:transactionCurrency:cardPresentationMode:)")));
- (void)transactionCompleted __attribute__((swift_name("transactionCompleted()")));
- (void)unregisterDeviceConnectionListeners __attribute__((swift_name("unregisterDeviceConnectionListeners()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraService")))
@interface OnoMiuraService : OnoOnoMiuraServiceProtocol
- (instancetype)initWithMiuraCommandQueue:(OnoTerminalCommandQueue *)miuraCommandQueue dispatch:(void (^)(OnoAction *))dispatch updateService:(id<OnoIOnoUpdateService>)updateService __attribute__((swift_name("init(miuraCommandQueue:dispatch:updateService:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithDispatch:(void (^)(OnoAction *))dispatch __attribute__((swift_name("init(dispatch:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (void)abortTransactionErrorDetails:(OnoErrorDetails *)errorDetails finalizeTransaction:(BOOL)finalizeTransaction __attribute__((swift_name("abortTransaction(errorDetails:finalizeTransaction:)")));
- (void)askForPinTransactionAmount:(int64_t)transactionAmount transactionCurrency:(OnoCurrency *)transactionCurrency onoMiuraCardData:(OnoOnoMiuraCardData *)onoMiuraCardData topLineLabel:(NSString *)topLineLabel __attribute__((swift_name("askForPin(transactionAmount:transactionCurrency:onoMiuraCardData:topLineLabel:)")));
- (void)askForTipAmountTopLinePromptIndex:(int32_t)topLinePromptIndex secondLinePromptIndex:(int32_t)secondLinePromptIndex thirdLinePromptIndex:(int32_t)thirdLinePromptIndex maxNumIntDigits:(int32_t)maxNumIntDigits numFractalDigits:(int32_t)numFractalDigits numberPlaceHolder:(NSString *)numberPlaceHolder __attribute__((swift_name("askForTipAmount(topLinePromptIndex:secondLinePromptIndex:thirdLinePromptIndex:maxNumIntDigits:numFractalDigits:numberPlaceHolder:)")));
- (void)clearAllCommandFromQueueReason:(NSString *)reason __attribute__((swift_name("clearAllCommandFromQueue(reason:)")));
- (void)clearCommandFromQueueCommand:(OnoQueueCommand *)command __attribute__((swift_name("clearCommandFromQueue(command:)")));
- (void)connectTerminal:(OnoTerminal *)terminal __attribute__((swift_name("connect(terminal:)")));
- (void)delayGetDeviceBatteryStatusDelayInSeconds:(int32_t)delayInSeconds __attribute__((swift_name("delayGetDeviceBatteryStatus(delayInSeconds:)")));
- (void)delaySetTerminalAvailableTerminal:(OnoTerminal *)terminal __attribute__((swift_name("delaySetTerminalAvailable(terminal:)")));
- (void)disconnect __attribute__((swift_name("disconnect()")));
- (void)displayTextMiuraText:(OnoOnoMiuraText *)miuraText __attribute__((swift_name("displayText(miuraText:)")));
- (void)doEMVTransactionAmount:(int64_t)transactionAmount transactionCurrency:(OnoCurrency *)transactionCurrency __attribute__((swift_name("doEMV(transactionAmount:transactionCurrency:)")));
- (void)doYouWantToTipPromptPromptMessage:(OnoOnoMiuraText *)promptMessage __attribute__((swift_name("doYouWantToTipPrompt(promptMessage:)")));
- (void)enableKeypad __attribute__((swift_name("enableKeypad()")));
- (void)getBatteryStatus __attribute__((swift_name("getBatteryStatus()")));
- (void)getBluetoothInfo __attribute__((swift_name("getBluetoothInfo()")));
- (void)getDeviceCapabilities __attribute__((swift_name("getDeviceCapabilities()")));
- (void)getDeviceP2PEFiles __attribute__((swift_name("getDeviceP2PEFiles()")));
- (void)getP2PEStatusIsKeyInjectionVerification:(BOOL)isKeyInjectionVerification __attribute__((swift_name("getP2PEStatus(isKeyInjectionVerification:)")));
- (void)getPEDConfig __attribute__((swift_name("getPEDConfig()")));
- (void)getSoftwareInfo __attribute__((swift_name("getSoftwareInfo()")));
- (void)hardResetAndReconnectDeviceDispatchOnReconnect:(NSArray<OnoAction *> *)dispatchOnReconnect dispatchOnDisconnected:(NSArray<OnoAction *> *)dispatchOnDisconnected __attribute__((swift_name("hardResetAndReconnectDevice(dispatchOnReconnect:dispatchOnDisconnected:)")));
- (void)idleSessionMiuraText:(OnoOnoMiuraText *)miuraText __attribute__((swift_name("idleSession(miuraText:)")));
- (void)injectKeysKeyInjectionDataMap:(OnoSensitiveMap *)keyInjectionDataMap __attribute__((swift_name("injectKeys(keyInjectionDataMap:)")));
- (void)performUpdateAvailableUpdate:(OnoAvailableUpdate *)availableUpdate os:(OnoOnoMiuraOS * _Nullable)os __attribute__((swift_name("performUpdate(availableUpdate:os:)")));
- (void)processARPCArpc:(OnoSensitiveString *)arpc __attribute__((swift_name("processARPC(arpc:)")));
- (void)rkiFinishedWasSuccessful:(BOOL)wasSuccessful errorMessage:(NSString * _Nullable)errorMessage __attribute__((swift_name("rkiFinished(wasSuccessful:errorMessage:)")));
- (void)showIdleTextAfterMillisTimeMillis:(int64_t)timeMillis __attribute__((swift_name("showIdleTextAfterMillis(timeMillis:)")));
- (void)startTransactionTransactionAmount:(int64_t)transactionAmount transactionCurrency:(OnoCurrency *)transactionCurrency cardPresentationMode:(OnoCardPromptMode *)cardPresentationMode __attribute__((swift_name("startTransaction(transactionAmount:transactionCurrency:cardPresentationMode:)")));
- (void)transactionCompleted __attribute__((swift_name("transactionCompleted()")));
- (void)unregisterDeviceConnectionListeners __attribute__((swift_name("unregisterDeviceConnectionListeners()")));
@property (readonly) id<OnoITimerJob> _Nullable autoShowIdleScreenTimer __attribute__((swift_name("autoShowIdleScreenTimer")));
@property id<OnoITimerJob> _Nullable delayCheckStatusTimer __attribute__((swift_name("delayCheckStatusTimer")));
@property (readonly) OnoAvailableUpdate * _Nullable rkiAvailableUpdate __attribute__((swift_name("rkiAvailableUpdate")));
@property (readonly) NSString *tag __attribute__((swift_name("tag")));
@property (readonly) id<OnoIOnoUpdateService> updateService __attribute__((swift_name("updateService")));
@end;

__attribute__((swift_name("MockOnoMiuraService")))
@interface OnoMockOnoMiuraService : OnoOnoMiuraServiceProtocol
- (instancetype)initWithDispatch:(void (^)(OnoAction *))dispatch isUnitTesting:(BOOL)isUnitTesting deviceName:(NSString *)deviceName dateTimeString:(NSString *(^ _Nullable)(NSString *))dateTimeString __attribute__((swift_name("init(dispatch:isUnitTesting:deviceName:dateTimeString:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithDispatch:(void (^)(OnoAction *))dispatch __attribute__((swift_name("init(dispatch:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (void)abortTransactionErrorDetails:(OnoErrorDetails *)errorDetails finalizeTransaction:(BOOL)finalizeTransaction __attribute__((swift_name("abortTransaction(errorDetails:finalizeTransaction:)")));
- (void)askForPinTransactionAmount:(int64_t)transactionAmount transactionCurrency:(OnoCurrency *)transactionCurrency onoMiuraCardData:(OnoOnoMiuraCardData *)onoMiuraCardData topLineLabel:(NSString *)topLineLabel __attribute__((swift_name("askForPin(transactionAmount:transactionCurrency:onoMiuraCardData:topLineLabel:)")));
- (void)askForTipAmountTopLinePromptIndex:(int32_t)topLinePromptIndex secondLinePromptIndex:(int32_t)secondLinePromptIndex thirdLinePromptIndex:(int32_t)thirdLinePromptIndex maxNumIntDigits:(int32_t)maxNumIntDigits numFractalDigits:(int32_t)numFractalDigits numberPlaceHolder:(NSString *)numberPlaceHolder __attribute__((swift_name("askForTipAmount(topLinePromptIndex:secondLinePromptIndex:thirdLinePromptIndex:maxNumIntDigits:numFractalDigits:numberPlaceHolder:)")));
- (void)clearAllCommandFromQueueReason:(NSString *)reason __attribute__((swift_name("clearAllCommandFromQueue(reason:)")));
- (void)clearCommandFromQueueCommand:(OnoQueueCommand *)command __attribute__((swift_name("clearCommandFromQueue(command:)")));
- (void)connectTerminal:(OnoTerminal *)terminal __attribute__((swift_name("connect(terminal:)")));
- (void)delayGetDeviceBatteryStatusDelayInSeconds:(int32_t)delayInSeconds __attribute__((swift_name("delayGetDeviceBatteryStatus(delayInSeconds:)")));
- (void)delaySetTerminalAvailableTerminal:(OnoTerminal *)terminal __attribute__((swift_name("delaySetTerminalAvailable(terminal:)")));
- (void)disconnect __attribute__((swift_name("disconnect()")));
- (void)displayTextMiuraText:(OnoOnoMiuraText *)miuraText __attribute__((swift_name("displayText(miuraText:)")));
- (void)doEMVTransactionAmount:(int64_t)transactionAmount transactionCurrency:(OnoCurrency *)transactionCurrency __attribute__((swift_name("doEMV(transactionAmount:transactionCurrency:)")));
- (void)doYouWantToTipPromptPromptMessage:(OnoOnoMiuraText *)promptMessage __attribute__((swift_name("doYouWantToTipPrompt(promptMessage:)")));
- (void)enableKeypad __attribute__((swift_name("enableKeypad()")));
- (void)getBatteryStatus __attribute__((swift_name("getBatteryStatus()")));
- (void)getBluetoothInfo __attribute__((swift_name("getBluetoothInfo()")));
- (void)getDeviceCapabilities __attribute__((swift_name("getDeviceCapabilities()")));
- (void)getDeviceP2PEFiles __attribute__((swift_name("getDeviceP2PEFiles()")));
- (void)getP2PEStatusIsKeyInjectionVerification:(BOOL)isKeyInjectionVerification __attribute__((swift_name("getP2PEStatus(isKeyInjectionVerification:)")));
- (void)getPEDConfig __attribute__((swift_name("getPEDConfig()")));
- (void)getSoftwareInfo __attribute__((swift_name("getSoftwareInfo()")));
- (void)hardResetAndReconnectDeviceDispatchOnReconnect:(NSArray<OnoAction *> *)dispatchOnReconnect dispatchOnDisconnected:(NSArray<OnoAction *> *)dispatchOnDisconnected __attribute__((swift_name("hardResetAndReconnectDevice(dispatchOnReconnect:dispatchOnDisconnected:)")));
- (void)idleSessionMiuraText:(OnoOnoMiuraText *)miuraText __attribute__((swift_name("idleSession(miuraText:)")));
- (void)injectKeysKeyInjectionDataMap:(OnoSensitiveMap *)rkiResponseData __attribute__((swift_name("injectKeys(keyInjectionDataMap:)")));
- (void)performUpdateAvailableUpdate:(OnoAvailableUpdate *)availableUpdate os:(OnoOnoMiuraOS * _Nullable)os __attribute__((swift_name("performUpdate(availableUpdate:os:)")));
- (void)processARPCArpc:(OnoSensitiveString *)arpc __attribute__((swift_name("processARPC(arpc:)")));
- (void)rkiFinishedWasSuccessful:(BOOL)wasSuccessful errorMessage:(NSString * _Nullable)errorMessage __attribute__((swift_name("rkiFinished(wasSuccessful:errorMessage:)")));
- (void)showIdleTextAfterMillisTimeMillis:(int64_t)timeMillis __attribute__((swift_name("showIdleTextAfterMillis(timeMillis:)")));
- (void)startTransactionTransactionAmount:(int64_t)transactionAmount transactionCurrency:(OnoCurrency *)transactionCurrency cardPresentationMode:(OnoCardPromptMode *)cardPresentationMode __attribute__((swift_name("startTransaction(transactionAmount:transactionCurrency:cardPresentationMode:)")));
- (void)transactionCompleted __attribute__((swift_name("transactionCompleted()")));
- (void)unregisterDeviceConnectionListeners __attribute__((swift_name("unregisterDeviceConnectionListeners()")));
@property BOOL abortBeforeCardPresentation __attribute__((swift_name("abortBeforeCardPresentation")));
@property OnoOnoMiuraBatteryStatus *batteryStatus __attribute__((swift_name("batteryStatus")));
@property BOOL cardInserted __attribute__((swift_name("cardInserted")));
@property BOOL cardSwiped __attribute__((swift_name("cardSwiped")));
@property BOOL cardTapped __attribute__((swift_name("cardTapped")));
@property int32_t delayCheckBatteryStatusResponses __attribute__((swift_name("delayCheckBatteryStatusResponses")));
@property OnoSensitiveString *emvAuthorizationTLV __attribute__((swift_name("emvAuthorizationTLV")));
@property BOOL failAbortingTransaction __attribute__((swift_name("failAbortingTransaction")));
@property OnoMockOnoMiuraServiceFailConnectionMode * _Nullable failConnectionMode __attribute__((swift_name("failConnectionMode")));
@property BOOL failEmv __attribute__((swift_name("failEmv")));
@property BOOL failGetBatteryStatus __attribute__((swift_name("failGetBatteryStatus")));
@property BOOL failGetBluetoothInfo __attribute__((swift_name("failGetBluetoothInfo")));
@property BOOL failGetDeviceCapabilities __attribute__((swift_name("failGetDeviceCapabilities")));
@property BOOL failGetPEDConfig __attribute__((swift_name("failGetPEDConfig")));
@property BOOL failGetSoftwareInfo __attribute__((swift_name("failGetSoftwareInfo")));
@property BOOL failP2peStatus __attribute__((swift_name("failP2peStatus")));
@property BOOL failProcessArpc __attribute__((swift_name("failProcessArpc")));
@property BOOL failTextDisplayMessage __attribute__((swift_name("failTextDisplayMessage")));
@property BOOL failUpdate __attribute__((swift_name("failUpdate")));
@property OnoSensitiveString *finalizationTlv __attribute__((swift_name("finalizationTlv")));
@property BOOL finishUpdateAutomatically __attribute__((swift_name("finishUpdateAutomatically")));
@property NSString * _Nullable getP2peFilesErrorMessage __attribute__((swift_name("getP2peFilesErrorMessage")));
@property BOOL isKeypadEnabled __attribute__((swift_name("isKeypadEnabled")));
@property NSString * _Nullable keyInjectionFailureMessage __attribute__((swift_name("keyInjectionFailureMessage")));
@property OnoSensitiveString *nfcAuthorizationTLV __attribute__((swift_name("nfcAuthorizationTLV")));
@property void (^ _Nullable onAskForTipCalled)(OnoInt *, OnoInt *, OnoInt *, OnoInt *, OnoInt *, NSString *) __attribute__((swift_name("onAskForTipCalled")));
@property OnoOnoMiuraP2PEStatus *p2peStatus __attribute__((swift_name("p2peStatus")));
@property NSDictionary<NSString *, NSString *> *pedConfig __attribute__((swift_name("pedConfig")));
@property OnoMockOnoMiuraServiceMockingPinEntry * _Nullable pinEntry __attribute__((swift_name("pinEntry")));
@property BOOL reconnectAfterHardReset __attribute__((swift_name("reconnectAfterHardReset")));
@property OnoAvailableUpdate * _Nullable rkiUpdate __attribute__((swift_name("rkiUpdate")));
@property (readonly, getter=tag_) NSString *tag __attribute__((swift_name("tag")));
@property OnoTerminalModel *terminalModel __attribute__((swift_name("terminalModel")));
@property NSString *terminalSerialNo __attribute__((swift_name("terminalSerialNo")));
@property OnoKotlinPair *tippingPair __attribute__((swift_name("tippingPair")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MockOnoMiuraService.Companion")))
@interface OnoMockOnoMiuraServiceCompanion : OnoBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (readonly) OnoOnoMiuraCardData *insertedCardData __attribute__((swift_name("insertedCardData")));
@property (readonly) OnoOnoMiuraCardData *swipedCardData __attribute__((swift_name("swipedCardData")));
@property (readonly) OnoTerminal *terminal __attribute__((swift_name("terminal")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MockOnoMiuraService.FailConnectionMode")))
@interface OnoMockOnoMiuraServiceFailConnectionMode : OnoKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) OnoMockOnoMiuraServiceFailConnectionMode *connectionFailed __attribute__((swift_name("connectionFailed")));
@property (class, readonly) OnoMockOnoMiuraServiceFailConnectionMode *terminalHalfPaired __attribute__((swift_name("terminalHalfPaired")));
- (int32_t)compareToOther:(OnoMockOnoMiuraServiceFailConnectionMode *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MockOnoMiuraService.MockingPinEntry")))
@interface OnoMockOnoMiuraServiceMockingPinEntry : OnoBase
- (instancetype)initWithAskForPin:(BOOL)askForPin pinEntryMode:(OnoMockOnoMiuraServiceMockingPinEntryPinEntryMode * _Nullable)pinEntryMode __attribute__((swift_name("init(askForPin:pinEntryMode:)"))) __attribute__((objc_designated_initializer));
@property (readonly) BOOL askForPin __attribute__((swift_name("askForPin")));
@property (readonly) OnoMockOnoMiuraServiceMockingPinEntryPinEntryMode * _Nullable pinEntryMode __attribute__((swift_name("pinEntryMode")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MockOnoMiuraService.MockingPinEntryPinEntryMode")))
@interface OnoMockOnoMiuraServiceMockingPinEntryPinEntryMode : OnoKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) OnoMockOnoMiuraServiceMockingPinEntryPinEntryMode *pinEntered __attribute__((swift_name("pinEntered")));
@property (class, readonly) OnoMockOnoMiuraServiceMockingPinEntryPinEntryMode *byPassed __attribute__((swift_name("byPassed")));
@property (class, readonly) OnoMockOnoMiuraServiceMockingPinEntryPinEntryMode *cancelled __attribute__((swift_name("cancelled")));
@property (class, readonly) OnoMockOnoMiuraServiceMockingPinEntryPinEntryMode *timesOut __attribute__((swift_name("timesOut")));
@property (class, readonly) OnoMockOnoMiuraServiceMockingPinEntryPinEntryMode *machineError __attribute__((swift_name("machineError")));
@property (class, readonly) OnoMockOnoMiuraServiceMockingPinEntryPinEntryMode *ignore __attribute__((swift_name("ignore")));
- (int32_t)compareToOther:(OnoMockOnoMiuraServiceMockingPinEntryPinEntryMode *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((swift_name("ISDKService")))
@protocol OnoISDKService
@required
@property (readonly, getter=tag_) NSString *tag __attribute__((swift_name("tag")));
@end;

__attribute__((swift_name("IMiuraSDKService")))
@protocol OnoIMiuraSDKService <OnoISDKService>
@required
- (void)abortTransactionErrorDetails:(OnoErrorDetails *)errorDetails finalizeTransaction:(BOOL)finalizeTransaction __attribute__((swift_name("abortTransaction(errorDetails:finalizeTransaction:)")));
- (void)askForPinTransactionAmount:(int64_t)transactionAmount transactionCurrency:(OnoCurrency *)transactionCurrency onoMiuraCardData:(OnoOnoMiuraCardData *)onoMiuraCardData topLineLabel:(NSString *)topLineLabel __attribute__((swift_name("askForPin(transactionAmount:transactionCurrency:onoMiuraCardData:topLineLabel:)")));
- (void)askForTipAmountTopLinePromptIndex:(int32_t)topLinePromptIndex secondLinePromptIndex:(int32_t)secondLinePromptIndex thirdLinePromptIndex:(int32_t)thirdLinePromptIndex maxNumIntDigits:(int32_t)maxNumIntDigits numFractalDigits:(int32_t)numFractalDigits numberPlaceHolder:(NSString *)numberPlaceHolder __attribute__((swift_name("askForTipAmount(topLinePromptIndex:secondLinePromptIndex:thirdLinePromptIndex:maxNumIntDigits:numFractalDigits:numberPlaceHolder:)")));
- (void)cardStatusStatus:(BOOL)status __attribute__((swift_name("cardStatus(status:)")));
- (void)closeSession __attribute__((swift_name("closeSession()")));
- (void)displayTextMiuraText:(OnoOnoMiuraText *)miuraText clearCommandAfterExecution:(OnoMiuraCommandType *)clearCommandAfterExecution __attribute__((swift_name("displayText(miuraText:clearCommandAfterExecution:)")));
- (void)doEMVTransactionAmount:(int64_t)transactionAmount transactionCurrency:(OnoCurrency *)transactionCurrency __attribute__((swift_name("doEMV(transactionAmount:transactionCurrency:)")));
- (void)doYouWantToTipPromptPromptMessage:(OnoOnoMiuraText *)promptMessage __attribute__((swift_name("doYouWantToTipPrompt(promptMessage:)")));
- (void)enableKeypad __attribute__((swift_name("enableKeypad()")));
- (void)getBatteryStatus __attribute__((swift_name("getBatteryStatus()")));
- (void)getBluetoothInfo __attribute__((swift_name("getBluetoothInfo()")));
- (void)getDeviceCapabilities __attribute__((swift_name("getDeviceCapabilities()")));
- (NSString *)getDeviceName __attribute__((swift_name("getDeviceName()")));
- (void)getDeviceP2PEFiles __attribute__((swift_name("getDeviceP2PEFiles()")));
- (void)getP2PEStatusIsKeyInjectionVerification:(BOOL)isKeyInjectionVerification __attribute__((swift_name("getP2PEStatus(isKeyInjectionVerification:)")));
- (void)getPEDConfig __attribute__((swift_name("getPEDConfig()")));
- (void)getSoftwareInfo __attribute__((swift_name("getSoftwareInfo()")));
- (void)hardResetAndReconnectDeviceOnReconnect:(void (^)(void))onReconnect onDisconnected:(void (^)(void))onDisconnected __attribute__((swift_name("hardResetAndReconnectDevice(onReconnect:onDisconnected:)")));
- (void)injectKeysKeyInjectionDataMap:(OnoSensitiveMap *)keyInjectionDataMap __attribute__((swift_name("injectKeys(keyInjectionDataMap:)")));
- (void)openSessionSerialNumber:(NSString *)serialNumber address:(NSString *)address __attribute__((swift_name("openSession(serialNumber:address:)")));
- (void)performUpdateAvailableUpdate:(OnoAvailableUpdate *)availableUpdate downloadedFiles:(NSDictionary<NSString *, OnoKotlinByteArray *> *)downloadedFiles os:(OnoOnoMiuraOS * _Nullable)os __attribute__((swift_name("performUpdate(availableUpdate:downloadedFiles:os:)")));
- (void)processARPCArpc:(OnoSensitiveString *)arpc __attribute__((swift_name("processARPC(arpc:)")));
- (void)registerEventHandlers __attribute__((swift_name("registerEventHandlers()")));
- (void)setConnectorTerminalAddress:(NSString *)terminalAddress __attribute__((swift_name("setConnector(terminalAddress:)")));
- (void)startContactTransactionDisplayMessage:(OnoOnoMiuraText *)displayMessage cardPresentationMode:(OnoCardPromptMode *)cardPresentationMode __attribute__((swift_name("startContactTransaction(displayMessage:cardPresentationMode:)")));
- (void)startContactlessTransactionTransactionAmount:(int32_t)transactionAmount currencyCode:(int32_t)currencyCode cardPresentationMode:(OnoCardPromptMode *)cardPresentationMode __attribute__((swift_name("startContactlessTransaction(transactionAmount:currencyCode:cardPresentationMode:)")));
- (void)transactionCompleted __attribute__((swift_name("transactionCompleted()")));
- (void)unregisterDeviceConnectionListeners __attribute__((swift_name("unregisterDeviceConnectionListeners()")));
@end;

__attribute__((swift_name("IQueueCommandType")))
@protocol OnoIQueueCommandType
@required
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraCommandType")))
@interface OnoMiuraCommandType : OnoKotlinEnum <OnoIQueueCommandType>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) OnoMiuraCommandType *connectTerminal __attribute__((swift_name("connectTerminal")));
@property (class, readonly) OnoMiuraCommandType *disconnectTerminal __attribute__((swift_name("disconnectTerminal")));
@property (class, readonly) OnoMiuraCommandType *delaySetTerminalAvailable __attribute__((swift_name("delaySetTerminalAvailable")));
@property (class, readonly) OnoMiuraCommandType *startTransaction __attribute__((swift_name("startTransaction")));
@property (class, readonly) OnoMiuraCommandType *doEmv __attribute__((swift_name("doEmv")));
@property (class, readonly) OnoMiuraCommandType *processArpc __attribute__((swift_name("processArpc")));
@property (class, readonly) OnoMiuraCommandType *abortTransaction __attribute__((swift_name("abortTransaction")));
@property (class, readonly) OnoMiuraCommandType *askForPin __attribute__((swift_name("askForPin")));
@property (class, readonly) OnoMiuraCommandType *getBatteryStatus __attribute__((swift_name("getBatteryStatus")));
@property (class, readonly) OnoMiuraCommandType *getBluetoothInfo __attribute__((swift_name("getBluetoothInfo")));
@property (class, readonly) OnoMiuraCommandType *getDeviceCapabilities __attribute__((swift_name("getDeviceCapabilities")));
@property (class, readonly) OnoMiuraCommandType *getP2peStatus __attribute__((swift_name("getP2peStatus")));
@property (class, readonly) OnoMiuraCommandType *getPedConfig __attribute__((swift_name("getPedConfig")));
@property (class, readonly) OnoMiuraCommandType *getSoftwareInfo __attribute__((swift_name("getSoftwareInfo")));
@property (class, readonly) OnoMiuraCommandType *displayText __attribute__((swift_name("displayText")));
@property (class, readonly) OnoMiuraCommandType *idleSession __attribute__((swift_name("idleSession")));
@property (class, readonly) OnoMiuraCommandType *transactionCompleted __attribute__((swift_name("transactionCompleted")));
@property (class, readonly) OnoMiuraCommandType *performUpdate __attribute__((swift_name("performUpdate")));
@property (class, readonly) OnoMiuraCommandType *terminalKeyInjection __attribute__((swift_name("terminalKeyInjection")));
@property (class, readonly) OnoMiuraCommandType *terminalGetP2peFiles __attribute__((swift_name("terminalGetP2peFiles")));
@property (class, readonly) OnoMiuraCommandType *doYouWantToTip __attribute__((swift_name("doYouWantToTip")));
@property (class, readonly) OnoMiuraCommandType *tipAmountEntry __attribute__((swift_name("tipAmountEntry")));
@property (class, readonly) OnoMiuraCommandType *enableKeypad __attribute__((swift_name("enableKeypad")));
@property (class, readonly) OnoMiuraCommandType *unregisterListeners __attribute__((swift_name("unregisterListeners")));
@property (class, readonly) OnoMiuraCommandType *hardReset __attribute__((swift_name("hardReset")));
- (int32_t)compareToOther:(OnoMiuraCommandType *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((swift_name("OnoMiuraListenerProtocol")))
@interface OnoOnoMiuraListenerProtocol : OnoDispatcher
- (instancetype)initWithDispatch:(void (^)(OnoAction *))dispatch __attribute__((swift_name("init(dispatch:)"))) __attribute__((objc_designated_initializer));
- (void)keypadStatusIsEnabled:(BOOL)isEnabled __attribute__((swift_name("keypadStatus(isEnabled:)")));
- (void)onAbortTransaction __attribute__((swift_name("onAbortTransaction()")));
- (void)onAsyncProcessExceptionCommandType:(OnoMiuraCommandType *)commandType exceptionDescription:(NSString *)exceptionDescription exceptionCause:(OnoKotlinThrowable * _Nullable)exceptionCause __attribute__((swift_name("onAsyncProcessException(commandType:exceptionDescription:exceptionCause:)")));
- (void)onAwaitingCardCardPresentationMode:(OnoCardPromptMode *)cardPresentationMode __attribute__((swift_name("onAwaitingCard(cardPresentationMode:)")));
- (void)onByPassPinEntry __attribute__((swift_name("onByPassPinEntry()")));
- (void)onCardDataProvidedOnoMiuraCardData:(OnoOnoMiuraCardData *)onoMiuraCardData __attribute__((swift_name("onCardDataProvided(onoMiuraCardData:)")));
- (void)onCardInsertedEmvAuthorizationTLV:(OnoKotlinByteArray *)emvAuthorizationTLV __attribute__((swift_name("onCardInserted(emvAuthorizationTLV:)")));
- (void)onCardTappedNfcAuthorizationTLV:(OnoKotlinByteArray *)nfcAuthorizationTLV __attribute__((swift_name("onCardTapped(nfcAuthorizationTLV:)")));
- (void)onDeviceStatusChangedMiuraDeviceStatus:(OnoMiuraDeviceStatus *)miuraDeviceStatus statusText:(NSString *)statusText __attribute__((swift_name("onDeviceStatusChanged(miuraDeviceStatus:statusText:)")));
- (void)onEMVFailedTransactionError:(OnoOnoMiuraTransactionError * _Nullable)transactionError __attribute__((swift_name("onEMVFailed(transactionError:)")));
- (void)onGetBatteryStatusInfoChargingStatus:(int32_t)chargingStatus batteryLevel:(int32_t)batteryLevel __attribute__((swift_name("onGetBatteryStatusInfo(chargingStatus:batteryLevel:)")));
- (void)onGetBatteryStatusInfoFailedReason:(NSString *)reason __attribute__((swift_name("onGetBatteryStatusInfoFailed(reason:)")));
- (void)onGetBluetoothInfoBluetoothInfo:(NSDictionary<NSString *, NSString *> * _Nullable)bluetoothInfo __attribute__((swift_name("onGetBluetoothInfo(bluetoothInfo:)")));
- (void)onGetBluetoothInfoFailedReason:(NSString *)reason __attribute__((swift_name("onGetBluetoothInfoFailed(reason:)")));
- (void)onGetDeviceCapabilitiesDeviceCapabilities:(NSArray<OnoOnoMiuraDeviceCapability *> *)deviceCapabilities __attribute__((swift_name("onGetDeviceCapabilities(deviceCapabilities:)")));
- (void)onGetDeviceCapabilitiesFailedReason:(NSString *)reason __attribute__((swift_name("onGetDeviceCapabilitiesFailed(reason:)")));
- (void)onGetP2PEStatusInfoIsInitialized:(OnoBoolean * _Nullable)isInitialized isPinReady:(OnoBoolean * _Nullable)isPinReady isSREDReady:(OnoBoolean * _Nullable)isSREDReady isKeyInjectionVerification:(BOOL)isKeyInjectionVerification __attribute__((swift_name("onGetP2PEStatusInfo(isInitialized:isPinReady:isSREDReady:isKeyInjectionVerification:)")));
- (void)onGetP2PEStatusInfoFailedReason:(NSString *)reason isKeyInjectionVerification:(BOOL)isKeyInjectionVerification __attribute__((swift_name("onGetP2PEStatusInfoFailed(reason:isKeyInjectionVerification:)")));
- (void)onGetPEDConfigInfoVersionMap:(NSDictionary<NSString *, NSString *> * _Nullable)versionMap __attribute__((swift_name("onGetPEDConfigInfo(versionMap:)")));
- (void)onGetPEDConfigInfoFailedReason:(NSString *)reason __attribute__((swift_name("onGetPEDConfigInfoFailed(reason:)")));
- (void)onGetPedFileProgressFileName:(NSString *)fileName progress:(float)progress fileNumber:(int32_t)fileNumber totalNumberOfFiles:(int32_t)totalNumberOfFiles __attribute__((swift_name("onGetPedFileProgress(fileName:progress:fileNumber:totalNumberOfFiles:)")));
- (void)onGetSoftwareInfoMiuraOS:(OnoOnoMiuraOS *)miuraOS miuraMPI:(OnoOnoMiuraMPI *)miuraMPI __attribute__((swift_name("onGetSoftwareInfo(miuraOS:miuraMPI:)")));
- (void)onGetSoftwareInfoFailedReason:(NSString *)reason __attribute__((swift_name("onGetSoftwareInfoFailed(reason:)")));
- (void)onGettingPedFileFileName:(NSString *)fileName fileNumber:(int32_t)fileNumber totalNumberOfFiles:(int32_t)totalNumberOfFiles __attribute__((swift_name("onGettingPedFile(fileName:fileNumber:totalNumberOfFiles:)")));
- (void)onKeyPressedPressedButton:(OnoOnoMiuraButton * _Nullable)pressedButton keyIndex:(int32_t)keyIndex __attribute__((swift_name("onKeyPressed(pressedButton:keyIndex:)")));
- (void)onOpenSessionCompleteIsConnected:(BOOL)isConnected errorMessage:(NSString * _Nullable)errorMessage __attribute__((swift_name("onOpenSessionComplete(isConnected:errorMessage:)")));
- (void)onPinDataProvidedPinData:(OnoSensitiveString * _Nullable)pinData pinKsn:(OnoSensitiveString * _Nullable)pinKsn __attribute__((swift_name("onPinDataProvided(pinData:pinKsn:)")));
- (void)onPinEntryCancelledOrTimeout __attribute__((swift_name("onPinEntryCancelledOrTimeout()")));
- (void)onPinEntryFailedReason:(NSString *)reason __attribute__((swift_name("onPinEntryFailed(reason:)")));
- (void)onProcessARPCFinalizationTLV:(OnoKotlinByteArray * _Nullable)finalizationTLV __attribute__((swift_name("onProcessARPC(finalizationTLV:)")));
- (void)onProcessARPCFailedTransactionError:(OnoOnoMiuraTransactionError * _Nullable)transactionError __attribute__((swift_name("onProcessARPCFailed(transactionError:)")));
- (void)onRkiPedFilesFileDataMap:(NSDictionary<NSString *, NSString *> *)fileDataMap __attribute__((swift_name("onRkiPedFiles(fileDataMap:)")));
- (void)onStartTransactionFailedCardPresentationMode:(OnoCardPromptMode *)cardPresentationMode transactionError:(OnoOnoMiuraTransactionError * _Nullable)transactionError __attribute__((swift_name("onStartTransactionFailed(cardPresentationMode:transactionError:)")));
- (void)onSuccessfulKeyInjection __attribute__((swift_name("onSuccessfulKeyInjection()")));
- (void)onTextDisplayedClearCommandAfterExecution:(OnoMiuraCommandType *)clearCommandAfterExecution shownMessage:(OnoOnoMiuraText *)shownMessage __attribute__((swift_name("onTextDisplayed(clearCommandAfterExecution:shownMessage:)")));
- (void)onTextDisplayedFailedClearCommandAfterExecution:(OnoMiuraCommandType *)clearCommandAfterExecution shownMessage:(OnoOnoMiuraText *)shownMessage __attribute__((swift_name("onTextDisplayedFailed(clearCommandAfterExecution:shownMessage:)")));
- (void)onUnableToAbortTransaction __attribute__((swift_name("onUnableToAbortTransaction()")));
- (void)onUpdateFinishedAvailableUpdate:(OnoAvailableUpdate *)availableUpdate wasSuccessful:(BOOL)wasSuccessful errorMessage:(NSString * _Nullable)errorMessage __attribute__((swift_name("onUpdateFinished(availableUpdate:wasSuccessful:errorMessage:)")));
- (void)onWritingPedFileFileName:(NSString *)fileName fileSize:(int32_t)fileSize fileNumber:(int32_t)fileNumber totalNumberOfFiles:(int32_t)totalNumberOfFiles __attribute__((swift_name("onWritingPedFile(fileName:fileSize:fileNumber:totalNumberOfFiles:)")));
- (void)terminalConnected __attribute__((swift_name("terminalConnected()")));
- (void)terminalConnectingMessageUpdatedConnectingMessage:(NSString *)connectingMessage __attribute__((swift_name("terminalConnectingMessageUpdated(connectingMessage:)")));
- (void)terminalDisconnected __attribute__((swift_name("terminalDisconnected()")));
- (void)tipAmountEnteredAmount:(int32_t)amount __attribute__((swift_name("tipAmountEntered(amount:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoMiuraListener")))
@interface OnoOnoMiuraListener : OnoOnoMiuraListenerProtocol
- (instancetype)initWithDispatch:(void (^)(OnoAction *))dispatch __attribute__((swift_name("init(dispatch:)"))) __attribute__((objc_designated_initializer));
- (void)keypadStatusIsEnabled:(BOOL)isEnabled __attribute__((swift_name("keypadStatus(isEnabled:)")));
- (void)onAbortTransaction __attribute__((swift_name("onAbortTransaction()")));
- (void)onAsyncProcessExceptionCommandType:(OnoMiuraCommandType *)commandType exceptionDescription:(NSString *)exceptionDescription exceptionCause:(OnoKotlinThrowable * _Nullable)exceptionCause __attribute__((swift_name("onAsyncProcessException(commandType:exceptionDescription:exceptionCause:)")));
- (void)onAwaitingCardCardPresentationMode:(OnoCardPromptMode *)cardPresentationMode __attribute__((swift_name("onAwaitingCard(cardPresentationMode:)")));
- (void)onByPassPinEntry __attribute__((swift_name("onByPassPinEntry()")));
- (void)onCardDataProvidedOnoMiuraCardData:(OnoOnoMiuraCardData *)onoMiuraCardData __attribute__((swift_name("onCardDataProvided(onoMiuraCardData:)")));
- (void)onCardInsertedEmvAuthorizationTLV:(OnoKotlinByteArray *)emvAuthorizationTLV __attribute__((swift_name("onCardInserted(emvAuthorizationTLV:)")));
- (void)onCardTappedNfcAuthorizationTLV:(OnoKotlinByteArray *)nfcAuthorizationTLV __attribute__((swift_name("onCardTapped(nfcAuthorizationTLV:)")));
- (void)onDeviceStatusChangedMiuraDeviceStatus:(OnoMiuraDeviceStatus *)miuraDeviceStatus statusText:(NSString *)statusText __attribute__((swift_name("onDeviceStatusChanged(miuraDeviceStatus:statusText:)")));
- (void)onEMVFailedTransactionError:(OnoOnoMiuraTransactionError * _Nullable)transactionError __attribute__((swift_name("onEMVFailed(transactionError:)")));
- (void)onGetBatteryStatusInfoChargingStatus:(int32_t)chargingStatus batteryLevel:(int32_t)batteryLevel __attribute__((swift_name("onGetBatteryStatusInfo(chargingStatus:batteryLevel:)")));
- (void)onGetBatteryStatusInfoFailedReason:(NSString *)reason __attribute__((swift_name("onGetBatteryStatusInfoFailed(reason:)")));
- (void)onGetBluetoothInfoBluetoothInfo:(NSDictionary<NSString *, NSString *> * _Nullable)bluetoothInfo __attribute__((swift_name("onGetBluetoothInfo(bluetoothInfo:)")));
- (void)onGetBluetoothInfoFailedReason:(NSString *)reason __attribute__((swift_name("onGetBluetoothInfoFailed(reason:)")));
- (void)onGetDeviceCapabilitiesDeviceCapabilities:(NSArray<OnoOnoMiuraDeviceCapability *> *)deviceCapabilities __attribute__((swift_name("onGetDeviceCapabilities(deviceCapabilities:)")));
- (void)onGetDeviceCapabilitiesFailedReason:(NSString *)reason __attribute__((swift_name("onGetDeviceCapabilitiesFailed(reason:)")));
- (void)onGetP2PEStatusInfoIsInitialized:(OnoBoolean * _Nullable)isInitialized isPinReady:(OnoBoolean * _Nullable)isPinReady isSREDReady:(OnoBoolean * _Nullable)isSREDReady isKeyInjectionVerification:(BOOL)isKeyInjectionVerification __attribute__((swift_name("onGetP2PEStatusInfo(isInitialized:isPinReady:isSREDReady:isKeyInjectionVerification:)")));
- (void)onGetP2PEStatusInfoFailedReason:(NSString *)reason isKeyInjectionVerification:(BOOL)isKeyInjectionVerification __attribute__((swift_name("onGetP2PEStatusInfoFailed(reason:isKeyInjectionVerification:)")));
- (void)onGetPEDConfigInfoVersionMap:(NSDictionary<NSString *, NSString *> * _Nullable)versionMap __attribute__((swift_name("onGetPEDConfigInfo(versionMap:)")));
- (void)onGetPEDConfigInfoFailedReason:(NSString *)reason __attribute__((swift_name("onGetPEDConfigInfoFailed(reason:)")));
- (void)onGetPedFileProgressFileName:(NSString *)fileName progress:(float)progress fileNumber:(int32_t)fileNumber totalNumberOfFiles:(int32_t)totalNumberOfFiles __attribute__((swift_name("onGetPedFileProgress(fileName:progress:fileNumber:totalNumberOfFiles:)")));
- (void)onGetSoftwareInfoMiuraOS:(OnoOnoMiuraOS *)miuraOS miuraMPI:(OnoOnoMiuraMPI *)miuraMPI __attribute__((swift_name("onGetSoftwareInfo(miuraOS:miuraMPI:)")));
- (void)onGetSoftwareInfoFailedReason:(NSString *)reason __attribute__((swift_name("onGetSoftwareInfoFailed(reason:)")));
- (void)onGettingPedFileFileName:(NSString *)fileName fileNumber:(int32_t)fileNumber totalNumberOfFiles:(int32_t)totalNumberOfFiles __attribute__((swift_name("onGettingPedFile(fileName:fileNumber:totalNumberOfFiles:)")));
- (void)onKeyPressedPressedButton:(OnoOnoMiuraButton * _Nullable)pressedButton keyIndex:(int32_t)keyIndex __attribute__((swift_name("onKeyPressed(pressedButton:keyIndex:)")));
- (void)onOpenSessionCompleteIsConnected:(BOOL)isConnected errorMessage:(NSString * _Nullable)errorMessage __attribute__((swift_name("onOpenSessionComplete(isConnected:errorMessage:)")));
- (void)onPinDataProvidedPinData:(OnoSensitiveString * _Nullable)pinData pinKsn:(OnoSensitiveString * _Nullable)pinKsn __attribute__((swift_name("onPinDataProvided(pinData:pinKsn:)")));
- (void)onPinEntryCancelledOrTimeout __attribute__((swift_name("onPinEntryCancelledOrTimeout()")));
- (void)onPinEntryFailedReason:(NSString *)reason __attribute__((swift_name("onPinEntryFailed(reason:)")));
- (void)onProcessARPCFinalizationTLV:(OnoKotlinByteArray * _Nullable)finalizationTLV __attribute__((swift_name("onProcessARPC(finalizationTLV:)")));
- (void)onProcessARPCFailedTransactionError:(OnoOnoMiuraTransactionError * _Nullable)transactionError __attribute__((swift_name("onProcessARPCFailed(transactionError:)")));
- (void)onRkiPedFilesFileDataMap:(NSDictionary<NSString *, NSString *> *)fileDataMap __attribute__((swift_name("onRkiPedFiles(fileDataMap:)")));
- (void)onStartTransactionFailedCardPresentationMode:(OnoCardPromptMode *)cardPresentationMode transactionError:(OnoOnoMiuraTransactionError * _Nullable)transactionError __attribute__((swift_name("onStartTransactionFailed(cardPresentationMode:transactionError:)")));
- (void)onSuccessfulKeyInjection __attribute__((swift_name("onSuccessfulKeyInjection()")));
- (void)onTextDisplayedClearCommandAfterExecution:(OnoMiuraCommandType *)clearCommandAfterExecution shownMessage:(OnoOnoMiuraText *)shownText __attribute__((swift_name("onTextDisplayed(clearCommandAfterExecution:shownMessage:)")));
- (void)onTextDisplayedFailedClearCommandAfterExecution:(OnoMiuraCommandType *)clearCommandAfterExecution shownMessage:(OnoOnoMiuraText *)failedText __attribute__((swift_name("onTextDisplayedFailed(clearCommandAfterExecution:shownMessage:)")));
- (void)onUnableToAbortTransaction __attribute__((swift_name("onUnableToAbortTransaction()")));
- (void)onUpdateFinishedAvailableUpdate:(OnoAvailableUpdate *)availableUpdate wasSuccessful:(BOOL)wasSuccessful errorMessage:(NSString * _Nullable)errorMessage __attribute__((swift_name("onUpdateFinished(availableUpdate:wasSuccessful:errorMessage:)")));
- (void)onWritingPedFileFileName:(NSString *)fileName fileSize:(int32_t)fileSize fileNumber:(int32_t)fileNumber totalNumberOfFiles:(int32_t)totalNumberOfFiles __attribute__((swift_name("onWritingPedFile(fileName:fileSize:fileNumber:totalNumberOfFiles:)")));
- (void)terminalConnected __attribute__((swift_name("terminalConnected()")));
- (void)terminalConnectingMessageUpdatedConnectingMessage:(NSString *)connectingMessage __attribute__((swift_name("terminalConnectingMessageUpdated(connectingMessage:)")));
- (void)terminalDisconnected __attribute__((swift_name("terminalDisconnected()")));
- (void)tipAmountEnteredAmount:(int32_t)amount __attribute__((swift_name("tipAmountEntered(amount:)")));
@property OnoTerminalCommandQueue * _Nullable miuraCommandQueue __attribute__((swift_name("miuraCommandQueue")));
@property (readonly) NSString *tag __attribute__((swift_name("tag")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("RequestRkiDataId")))
@interface OnoRequestRkiDataId : OnoKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) OnoRequestRkiDataId *prodSigningCertificate __attribute__((swift_name("prodSigningCertificate")));
@property (class, readonly) OnoRequestRkiDataId *terminalCertificate __attribute__((swift_name("terminalCertificate")));
@property (class, readonly) OnoRequestRkiDataId *tempKeyloadCertificate __attribute__((swift_name("tempKeyloadCertificate")));
@property (class, readonly) OnoRequestRkiDataId *suggestedIksn __attribute__((swift_name("suggestedIksn")));
- (int32_t)compareToOther:(OnoRequestRkiDataId *)other __attribute__((swift_name("compareTo(other:)")));
@property (readonly) NSString *fileName __attribute__((swift_name("fileName")));
@property (readonly) NSString *jsonKey __attribute__((swift_name("jsonKey")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ResponseRkiDataId")))
@interface OnoResponseRkiDataId : OnoKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) OnoResponseRkiDataId *hsmCert __attribute__((swift_name("hsmCert")));
@property (class, readonly) OnoResponseRkiDataId *kbpk __attribute__((swift_name("kbpk")));
@property (class, readonly) OnoResponseRkiDataId *kbpkSig __attribute__((swift_name("kbpkSig")));
@property (class, readonly) OnoResponseRkiDataId *sredIksn __attribute__((swift_name("sredIksn")));
@property (class, readonly) OnoResponseRkiDataId *sredTr31 __attribute__((swift_name("sredTr31")));
@property (class, readonly) OnoResponseRkiDataId *pinIksn __attribute__((swift_name("pinIksn")));
@property (class, readonly) OnoResponseRkiDataId *pinTr31 __attribute__((swift_name("pinTr31")));
- (int32_t)compareToOther:(OnoResponseRkiDataId *)other __attribute__((swift_name("compareTo(other:)")));
@property (readonly) NSString *jsonName __attribute__((swift_name("jsonName")));
@property (readonly) NSString *pedFileName __attribute__((swift_name("pedFileName")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UpdateUtils")))
@interface OnoUpdateUtils : OnoBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)updateUtils __attribute__((swift_name("init()")));
- (NSDictionary<NSString *, id> *)getVersionsMapMiuraOS:(OnoOnoMiuraOS * _Nullable)miuraOS miuraMPI:(OnoOnoMiuraMPI * _Nullable)miuraMPI accessory:(OnoOnoMiuraAccessory * _Nullable)accessory pedConfig:(NSDictionary<NSString *, NSString *> * _Nullable)pedConfig __attribute__((swift_name("getVersionsMap(miuraOS:miuraMPI:accessory:pedConfig:)")));
- (NSString *)mapUpdateTypeToFilenameUpdateType:(NSString *)updateType os:(OnoOnoMiuraOS * _Nullable)os version:(NSString * _Nullable)version __attribute__((swift_name("mapUpdateTypeToFilename(updateType:os:version:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoMiuraAccessory")))
@interface OnoOnoMiuraAccessory : OnoBase
- (instancetype)initWithConnected:(BOOL)connected serialNumber:(NSString *)serialNumber connectionID:(NSString * _Nullable)connectionID name:(NSString * _Nullable)name manufacturer:(OnoManufacturer * _Nullable)manufacturer modelNumber:(NSString * _Nullable)modelNumber firmwareRevisionActive:(NSString * _Nullable)firmwareRevisionActive hardwareRevision:(NSString * _Nullable)hardwareRevision protocols:(NSArray<NSString *> * _Nullable)protocols __attribute__((swift_name("init(connected:serialNumber:connectionID:name:manufacturer:modelNumber:firmwareRevisionActive:hardwareRevision:protocols:)"))) __attribute__((objc_designated_initializer));
- (NSDictionary<NSString *, id> *)asMap __attribute__((swift_name("asMap()")));
- (BOOL)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString * _Nullable)component3 __attribute__((swift_name("component3()")));
- (NSString * _Nullable)component4 __attribute__((swift_name("component4()")));
- (OnoManufacturer * _Nullable)component5 __attribute__((swift_name("component5()")));
- (NSString * _Nullable)component6 __attribute__((swift_name("component6()")));
- (NSString * _Nullable)component7 __attribute__((swift_name("component7()")));
- (NSString * _Nullable)component8 __attribute__((swift_name("component8()")));
- (NSArray<NSString *> * _Nullable)component9 __attribute__((swift_name("component9()")));
- (OnoOnoMiuraAccessory *)doCopyConnected:(BOOL)connected serialNumber:(NSString *)serialNumber connectionID:(NSString * _Nullable)connectionID name:(NSString * _Nullable)name manufacturer:(OnoManufacturer * _Nullable)manufacturer modelNumber:(NSString * _Nullable)modelNumber firmwareRevisionActive:(NSString * _Nullable)firmwareRevisionActive hardwareRevision:(NSString * _Nullable)hardwareRevision protocols:(NSArray<NSString *> * _Nullable)protocols __attribute__((swift_name("doCopy(connected:serialNumber:connectionID:name:manufacturer:modelNumber:firmwareRevisionActive:hardwareRevision:protocols:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL connected __attribute__((swift_name("connected")));
@property (readonly) NSString * _Nullable connectionID __attribute__((swift_name("connectionID")));
@property (readonly) NSString * _Nullable firmwareRevisionActive __attribute__((swift_name("firmwareRevisionActive")));
@property (readonly) NSString * _Nullable hardwareRevision __attribute__((swift_name("hardwareRevision")));
@property (readonly) OnoManufacturer * _Nullable manufacturer __attribute__((swift_name("manufacturer")));
@property (readonly) NSString * _Nullable modelNumber __attribute__((swift_name("modelNumber")));
@property (readonly) NSString * _Nullable name __attribute__((swift_name("name")));
@property (readonly) NSArray<NSString *> * _Nullable protocols __attribute__((swift_name("protocols")));
@property (readonly) NSString *serialNumber __attribute__((swift_name("serialNumber")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoMiuraBatteryStatus")))
@interface OnoOnoMiuraBatteryStatus : OnoBase
- (instancetype)initWithChargingStatus:(int32_t)chargingStatus batteryLevel:(int32_t)batteryLevel __attribute__((swift_name("init(chargingStatus:batteryLevel:)"))) __attribute__((objc_designated_initializer));
- (NSDictionary<NSString *, id> *)asMap __attribute__((swift_name("asMap()")));
- (int32_t)component1 __attribute__((swift_name("component1()")));
- (int32_t)component2 __attribute__((swift_name("component2()")));
- (OnoOnoMiuraBatteryStatus *)doCopyChargingStatus:(int32_t)chargingStatus batteryLevel:(int32_t)batteryLevel __attribute__((swift_name("doCopy(chargingStatus:batteryLevel:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t batteryLevel __attribute__((swift_name("batteryLevel")));
@property (readonly) int32_t chargingStatus __attribute__((swift_name("chargingStatus")));
@property (readonly) BOOL isCharging __attribute__((swift_name("isCharging")));
@property (readonly) BOOL shouldStayConnected __attribute__((swift_name("shouldStayConnected")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoMiuraCardData")))
@interface OnoOnoMiuraCardData : OnoBase
- (instancetype)initWithSensitizedRaw:(OnoSensitiveByteArray * _Nullable)sensitizedRaw answerToReset:(NSString * _Nullable)answerToReset pinData:(OnoSensitiveString * _Nullable)pinData trackData:(OnoSensitiveString * _Nullable)trackData pinDataKsn:(OnoSensitiveString * _Nullable)pinDataKsn cardStatus:(OnoOnoMiuraCardDataOnoMiuraCardStatus * _Nullable)cardStatus trackDataKSN:(OnoSensitiveString * _Nullable)trackDataKSN plainTrack1Data:(OnoSensitiveString * _Nullable)plainTrack1Data plainTrack2Data:(OnoOnoMiuraCardDataOnoMiuraTrack2Data * _Nullable)plainTrack2Data maskedTrack2Data:(OnoOnoMiuraCardDataOnoMiuraTrack2Data * _Nullable)maskedTrack2Data __attribute__((swift_name("init(sensitizedRaw:answerToReset:pinData:trackData:pinDataKsn:cardStatus:trackDataKSN:plainTrack1Data:plainTrack2Data:maskedTrack2Data:)"))) __attribute__((objc_designated_initializer));
- (OnoSensitiveMap *)asMap __attribute__((swift_name("asMap()")));
- (OnoSensitiveByteArray * _Nullable)component1 __attribute__((swift_name("component1()")));
- (OnoOnoMiuraCardDataOnoMiuraTrack2Data * _Nullable)component10 __attribute__((swift_name("component10()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (OnoSensitiveString * _Nullable)component3 __attribute__((swift_name("component3()")));
- (OnoSensitiveString * _Nullable)component4 __attribute__((swift_name("component4()")));
- (OnoSensitiveString * _Nullable)component5 __attribute__((swift_name("component5()")));
- (OnoOnoMiuraCardDataOnoMiuraCardStatus * _Nullable)component6 __attribute__((swift_name("component6()")));
- (OnoSensitiveString * _Nullable)component7 __attribute__((swift_name("component7()")));
- (OnoSensitiveString * _Nullable)component8 __attribute__((swift_name("component8()")));
- (OnoOnoMiuraCardDataOnoMiuraTrack2Data * _Nullable)component9 __attribute__((swift_name("component9()")));
- (OnoOnoMiuraCardData *)doCopySensitizedRaw:(OnoSensitiveByteArray * _Nullable)sensitizedRaw answerToReset:(NSString * _Nullable)answerToReset pinData:(OnoSensitiveString * _Nullable)pinData trackData:(OnoSensitiveString * _Nullable)trackData pinDataKsn:(OnoSensitiveString * _Nullable)pinDataKsn cardStatus:(OnoOnoMiuraCardDataOnoMiuraCardStatus * _Nullable)cardStatus trackDataKSN:(OnoSensitiveString * _Nullable)trackDataKSN plainTrack1Data:(OnoSensitiveString * _Nullable)plainTrack1Data plainTrack2Data:(OnoOnoMiuraCardDataOnoMiuraTrack2Data * _Nullable)plainTrack2Data maskedTrack2Data:(OnoOnoMiuraCardDataOnoMiuraTrack2Data * _Nullable)maskedTrack2Data __attribute__((swift_name("doCopy(sensitizedRaw:answerToReset:pinData:trackData:pinDataKsn:cardStatus:trackDataKSN:plainTrack1Data:plainTrack2Data:maskedTrack2Data:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (OnoPinEnforcedBy * _Nullable)getPinEnforcerCardPromptMode:(OnoCardPromptMode * _Nullable)cardPromptMode __attribute__((swift_name("getPinEnforcer(cardPromptMode:)")));
- (BOOL)hasPinData __attribute__((swift_name("hasPinData()")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (OnoBoolean * _Nullable)isIccCardData __attribute__((swift_name("isIccCardData()")));
- (BOOL)shouldFirstTryChip __attribute__((swift_name("shouldFirstTryChip()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable answerToReset __attribute__((swift_name("answerToReset")));
@property (readonly) OnoOnoMiuraCardDataOnoMiuraCardStatus * _Nullable cardStatus __attribute__((swift_name("cardStatus")));
@property (readonly) OnoOnoMiuraCardDataOnoMiuraTrack2Data * _Nullable maskedTrack2Data __attribute__((swift_name("maskedTrack2Data")));
@property (readonly) OnoSensitiveString * _Nullable pinData __attribute__((swift_name("pinData")));
@property (readonly) OnoSensitiveString * _Nullable pinDataKsn __attribute__((swift_name("pinDataKsn")));
@property (readonly) OnoSensitiveString * _Nullable plainTrack1Data __attribute__((swift_name("plainTrack1Data")));
@property (readonly) OnoOnoMiuraCardDataOnoMiuraTrack2Data * _Nullable plainTrack2Data __attribute__((swift_name("plainTrack2Data")));
@property (readonly) OnoSensitiveByteArray * _Nullable sensitizedRaw __attribute__((swift_name("sensitizedRaw")));
@property (readonly) OnoSensitiveString * _Nullable trackData __attribute__((swift_name("trackData")));
@property (readonly) OnoSensitiveString * _Nullable trackDataKSN __attribute__((swift_name("trackDataKSN")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoMiuraCardData.OnoMiuraCardStatus")))
@interface OnoOnoMiuraCardDataOnoMiuraCardStatus : OnoBase
- (instancetype)initWithCardPresent:(BOOL)cardPresent emvCompatible:(BOOL)emvCompatible msrDataAvailable:(BOOL)msrDataAvailable track1DataAvailable:(BOOL)track1DataAvailable track2DataAvailable:(BOOL)track2DataAvailable track3DataAvailable:(BOOL)track3DataAvailable __attribute__((swift_name("init(cardPresent:emvCompatible:msrDataAvailable:track1DataAvailable:track2DataAvailable:track3DataAvailable:)"))) __attribute__((objc_designated_initializer));
- (BOOL)component1 __attribute__((swift_name("component1()")));
- (BOOL)component2 __attribute__((swift_name("component2()")));
- (BOOL)component3 __attribute__((swift_name("component3()")));
- (BOOL)component4 __attribute__((swift_name("component4()")));
- (BOOL)component5 __attribute__((swift_name("component5()")));
- (BOOL)component6 __attribute__((swift_name("component6()")));
- (OnoOnoMiuraCardDataOnoMiuraCardStatus *)doCopyCardPresent:(BOOL)cardPresent emvCompatible:(BOOL)emvCompatible msrDataAvailable:(BOOL)msrDataAvailable track1DataAvailable:(BOOL)track1DataAvailable track2DataAvailable:(BOOL)track2DataAvailable track3DataAvailable:(BOOL)track3DataAvailable __attribute__((swift_name("doCopy(cardPresent:emvCompatible:msrDataAvailable:track1DataAvailable:track2DataAvailable:track3DataAvailable:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL cardPresent __attribute__((swift_name("cardPresent")));
@property (readonly) BOOL emvCompatible __attribute__((swift_name("emvCompatible")));
@property (readonly) BOOL msrDataAvailable __attribute__((swift_name("msrDataAvailable")));
@property (readonly) BOOL track1DataAvailable __attribute__((swift_name("track1DataAvailable")));
@property (readonly) BOOL track2DataAvailable __attribute__((swift_name("track2DataAvailable")));
@property (readonly) BOOL track3DataAvailable __attribute__((swift_name("track3DataAvailable")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoMiuraCardData.OnoMiuraTrack2Data")))
@interface OnoOnoMiuraCardDataOnoMiuraTrack2Data : OnoBase
- (instancetype)initWithSensitizedRaw:(OnoSensitiveByteArray * _Nullable)sensitizedRaw isMasked:(BOOL)isMasked pan:(OnoSensitiveString * _Nullable)pan expirationDate:(OnoSensitiveString * _Nullable)expirationDate serviceCode:(NSString * _Nullable)serviceCode __attribute__((swift_name("init(sensitizedRaw:isMasked:pan:expirationDate:serviceCode:)"))) __attribute__((objc_designated_initializer));
- (OnoSensitiveByteArray * _Nullable)component1 __attribute__((swift_name("component1()")));
- (BOOL)component2 __attribute__((swift_name("component2()")));
- (OnoSensitiveString * _Nullable)component3 __attribute__((swift_name("component3()")));
- (OnoSensitiveString * _Nullable)component4 __attribute__((swift_name("component4()")));
- (NSString * _Nullable)component5 __attribute__((swift_name("component5()")));
- (OnoOnoMiuraCardDataOnoMiuraTrack2Data *)doCopySensitizedRaw:(OnoSensitiveByteArray * _Nullable)sensitizedRaw isMasked:(BOOL)isMasked pan:(OnoSensitiveString * _Nullable)pan expirationDate:(OnoSensitiveString * _Nullable)expirationDate serviceCode:(NSString * _Nullable)serviceCode __attribute__((swift_name("doCopy(sensitizedRaw:isMasked:pan:expirationDate:serviceCode:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable bin __attribute__((swift_name("bin")));
@property (readonly) OnoSensitiveString * _Nullable expirationDate __attribute__((swift_name("expirationDate")));
@property (readonly) BOOL isMasked __attribute__((swift_name("isMasked")));
@property (readonly) OnoSensitiveString * _Nullable pan __attribute__((swift_name("pan")));
@property (readonly) OnoSensitiveByteArray * _Nullable sensitizedRaw __attribute__((swift_name("sensitizedRaw")));
@property (readonly) NSString * _Nullable serviceCode __attribute__((swift_name("serviceCode")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoMiuraDeviceCapability")))
@interface OnoOnoMiuraDeviceCapability : OnoBase
- (instancetype)initWithName:(NSString *)name value:(NSString * _Nullable)value __attribute__((swift_name("init(name:value:)"))) __attribute__((objc_designated_initializer));
- (NSDictionary<NSString *, NSString *> *)asMap __attribute__((swift_name("asMap()")));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (OnoOnoMiuraDeviceCapability *)doCopyName:(NSString *)name value:(NSString * _Nullable)value __attribute__((swift_name("doCopy(name:value:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly) NSString * _Nullable value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoMiuraMPI")))
@interface OnoOnoMiuraMPI : OnoBase
- (instancetype)initWithType:(NSString *)type version:(NSString *)version __attribute__((swift_name("init(type:version:)"))) __attribute__((objc_designated_initializer));
- (NSDictionary<NSString *, NSString *> *)asMap __attribute__((swift_name("asMap()")));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (OnoOnoMiuraMPI *)doCopyType:(NSString *)type version:(NSString *)version __attribute__((swift_name("doCopy(type:version:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *type __attribute__((swift_name("type")));
@property (readonly) NSString *version __attribute__((swift_name("version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoMiuraOS")))
@interface OnoOnoMiuraOS : OnoBase
- (instancetype)initWithType:(NSString *)type version:(NSString *)version __attribute__((swift_name("init(type:version:)"))) __attribute__((objc_designated_initializer));
- (NSDictionary<NSString *, NSString *> *)asMap __attribute__((swift_name("asMap()")));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (OnoOnoMiuraOS *)doCopyType:(NSString *)type version:(NSString *)version __attribute__((swift_name("doCopy(type:version:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (BOOL)isTest __attribute__((swift_name("isTest()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *type __attribute__((swift_name("type")));
@property (readonly) NSString *version __attribute__((swift_name("version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoMiuraP2PEStatus")))
@interface OnoOnoMiuraP2PEStatus : OnoBase
- (instancetype)initWithIsInitialised:(BOOL)isInitialised isPINReady:(BOOL)isPINReady isCardDataReady:(BOOL)isCardDataReady __attribute__((swift_name("init(isInitialised:isPINReady:isCardDataReady:)"))) __attribute__((objc_designated_initializer));
- (NSDictionary<NSString *, OnoBoolean *> *)asMap __attribute__((swift_name("asMap()")));
- (BOOL)component1 __attribute__((swift_name("component1()")));
- (BOOL)component2 __attribute__((swift_name("component2()")));
- (BOOL)component3 __attribute__((swift_name("component3()")));
- (OnoOnoMiuraP2PEStatus *)doCopyIsInitialised:(BOOL)isInitialised isPINReady:(BOOL)isPINReady isCardDataReady:(BOOL)isCardDataReady __attribute__((swift_name("doCopy(isInitialised:isPINReady:isCardDataReady:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL isCardDataReady __attribute__((swift_name("isCardDataReady")));
@property (readonly) BOOL isInitialised __attribute__((swift_name("isInitialised")));
@property (readonly) BOOL isPINReady __attribute__((swift_name("isPINReady")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoMiuraText")))
@interface OnoOnoMiuraText : OnoBase
- (instancetype)initWithTopLineMaxOf21Chars:(NSString * _Nullable)topLineMaxOf21Chars secondLineMaxOf21Chars:(NSString * _Nullable)secondLineMaxOf21Chars thirdLineMaxOf21Chars:(NSString * _Nullable)thirdLineMaxOf21Chars forthLineMaxOf21Chars:(NSString * _Nullable)forthLineMaxOf21Chars fifthLineMaxOf21Chars:(NSString * _Nullable)fifthLineMaxOf21Chars capitalizeLines:(BOOL)capitalizeLines clearAfterMillis:(int64_t)clearAfterMillis __attribute__((swift_name("init(topLineMaxOf21Chars:secondLineMaxOf21Chars:thirdLineMaxOf21Chars:forthLineMaxOf21Chars:fifthLineMaxOf21Chars:capitalizeLines:clearAfterMillis:)"))) __attribute__((objc_designated_initializer));
- (NSString * _Nullable)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (NSString * _Nullable)component3 __attribute__((swift_name("component3()")));
- (NSString * _Nullable)component4 __attribute__((swift_name("component4()")));
- (NSString * _Nullable)component5 __attribute__((swift_name("component5()")));
- (BOOL)component6 __attribute__((swift_name("component6()")));
- (int64_t)component7 __attribute__((swift_name("component7()")));
- (OnoOnoMiuraText *)doCopyTopLineMaxOf21Chars:(NSString * _Nullable)topLineMaxOf21Chars secondLineMaxOf21Chars:(NSString * _Nullable)secondLineMaxOf21Chars thirdLineMaxOf21Chars:(NSString * _Nullable)thirdLineMaxOf21Chars forthLineMaxOf21Chars:(NSString * _Nullable)forthLineMaxOf21Chars fifthLineMaxOf21Chars:(NSString * _Nullable)fifthLineMaxOf21Chars capitalizeLines:(BOOL)capitalizeLines clearAfterMillis:(int64_t)clearAfterMillis __attribute__((swift_name("doCopy(topLineMaxOf21Chars:secondLineMaxOf21Chars:thirdLineMaxOf21Chars:forthLineMaxOf21Chars:fifthLineMaxOf21Chars:capitalizeLines:clearAfterMillis:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)toCenteredText __attribute__((swift_name("toCenteredText()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL capitalizeLines __attribute__((swift_name("capitalizeLines")));
@property (readonly) int64_t clearAfterMillis __attribute__((swift_name("clearAfterMillis")));
@property (readonly) NSString * _Nullable fifthLineMaxOf21Chars __attribute__((swift_name("fifthLineMaxOf21Chars")));
@property (readonly) NSString * _Nullable forthLineMaxOf21Chars __attribute__((swift_name("forthLineMaxOf21Chars")));
@property (readonly) NSString * _Nullable secondLineMaxOf21Chars __attribute__((swift_name("secondLineMaxOf21Chars")));
@property (readonly) NSString * _Nullable thirdLineMaxOf21Chars __attribute__((swift_name("thirdLineMaxOf21Chars")));
@property (readonly) NSString * _Nullable topLineMaxOf21Chars __attribute__((swift_name("topLineMaxOf21Chars")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoMiuraText.Companion")))
@interface OnoOnoMiuraTextCompanion : OnoBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (OnoOnoMiuraText *)DO_YOU_WANT_TO_TIPAmount:(int64_t)amount currency:(OnoCurrency *)currency __attribute__((swift_name("DO_YOU_WANT_TO_TIP(amount:currency:)")));
- (OnoOnoMiuraText *)TRANSACTION_IN_STATEState:(OnoTransactionState *)state additionalMessage:(NSString * _Nullable)additionalMessage __attribute__((swift_name("TRANSACTION_IN_STATE(state:additionalMessage:)")));
- (OnoOnoMiuraText *)presentCardPromptCardPresentationMode:(OnoCardPromptMode *)cardPresentationMode transactionCurrency:(OnoCurrency *)transactionCurrency amountInCents:(int64_t)amountInCents __attribute__((swift_name("presentCardPrompt(cardPresentationMode:transactionCurrency:amountInCents:)")));
@property (readonly) OnoOnoMiuraText *ERROR_READING_CHIP_FALLBACK_SWIPE __attribute__((swift_name("ERROR_READING_CHIP_FALLBACK_SWIPE")));
@property (readonly) OnoOnoMiuraText *IDLE_DISPLAY_TEXT __attribute__((swift_name("IDLE_DISPLAY_TEXT")));
@property (readonly) int32_t MAX_CHARS_PER_LINE __attribute__((swift_name("MAX_CHARS_PER_LINE")));
@property (readonly) int32_t MAX_NUMBER_OF_LINES __attribute__((swift_name("MAX_NUMBER_OF_LINES")));
@property (readonly) OnoOnoMiuraText *PROCESSING __attribute__((swift_name("PROCESSING")));
@property (readonly) OnoOnoMiuraText *REBOOTING __attribute__((swift_name("REBOOTING")));
@property (readonly) OnoOnoMiuraText *RKI_GET_P2PE_FILES __attribute__((swift_name("RKI_GET_P2PE_FILES")));
@property (readonly) OnoOnoMiuraText *RKI_GET_P2PE_FILES_FAILED __attribute__((swift_name("RKI_GET_P2PE_FILES_FAILED")));
@property (readonly) OnoOnoMiuraText *RKI_KEY_INJECTION_FAILED __attribute__((swift_name("RKI_KEY_INJECTION_FAILED")));
@property (readonly) OnoOnoMiuraText *RKI_KEY_INJECTION_SUCCESS __attribute__((swift_name("RKI_KEY_INJECTION_SUCCESS")));
@property (readonly) OnoOnoMiuraText *RKI_KEY_INJECTION_VERIFICATION __attribute__((swift_name("RKI_KEY_INJECTION_VERIFICATION")));
@property (readonly) OnoOnoMiuraText *RKI_KEY_INJECTION_VERIFICATION_FAILED __attribute__((swift_name("RKI_KEY_INJECTION_VERIFICATION_FAILED")));
@property (readonly) OnoOnoMiuraText *RKI_PROCESSING_INJECTING_KEYS __attribute__((swift_name("RKI_PROCESSING_INJECTING_KEYS")));
@property (readonly) OnoOnoMiuraText *RKI_REQUEST_KEYS __attribute__((swift_name("RKI_REQUEST_KEYS")));
@property (readonly) OnoOnoMiuraText *SIGN_ON_APP __attribute__((swift_name("SIGN_ON_APP")));
@property (readonly) OnoOnoMiuraText *SUBMITTING_SIGNATURE __attribute__((swift_name("SUBMITTING_SIGNATURE")));
@property (readonly) OnoOnoMiuraText *UPDATE_COMPLETE __attribute__((swift_name("UPDATE_COMPLETE")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TippingSettings")))
@interface OnoTippingSettings : OnoKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) OnoTippingSettings *tippingSettings __attribute__((swift_name("tippingSettings")));
@property (class, readonly) OnoTippingSettings *topLinePromptIndex __attribute__((swift_name("topLinePromptIndex")));
@property (class, readonly) OnoTippingSettings *secondLinePromptIndex __attribute__((swift_name("secondLinePromptIndex")));
@property (class, readonly) OnoTippingSettings *thirdLinePromptIndex __attribute__((swift_name("thirdLinePromptIndex")));
@property (class, readonly) OnoTippingSettings *maxNumIntDigits __attribute__((swift_name("maxNumIntDigits")));
@property (class, readonly) OnoTippingSettings *numFractalDigits __attribute__((swift_name("numFractalDigits")));
@property (class, readonly) OnoTippingSettings *numberPlaceholder __attribute__((swift_name("numberPlaceholder")));
- (int32_t)compareToOther:(OnoTippingSettings *)other __attribute__((swift_name("compareTo(other:)")));
@property (readonly) NSString *dataKey __attribute__((swift_name("dataKey")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraConnectionStatus")))
@interface OnoMiuraConnectionStatus : OnoKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) OnoMiuraConnectionStatus *initializing __attribute__((swift_name("initializing")));
@property (class, readonly) OnoMiuraConnectionStatus *connected __attribute__((swift_name("connected")));
@property (class, readonly) OnoMiuraConnectionStatus *disconnecting __attribute__((swift_name("disconnecting")));
@property (class, readonly) OnoMiuraConnectionStatus *connectionFailed __attribute__((swift_name("connectionFailed")));
- (int32_t)compareToOther:(OnoMiuraConnectionStatus *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MiuraDeviceStatus")))
@interface OnoMiuraDeviceStatus : OnoKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) OnoMiuraDeviceStatus *devicepoweredon __attribute__((swift_name("devicepoweredon")));
@property (class, readonly) OnoMiuraDeviceStatus *applicationselection __attribute__((swift_name("applicationselection")));
@property (class, readonly) OnoMiuraDeviceStatus *devicepoweringoff __attribute__((swift_name("devicepoweringoff")));
@property (class, readonly) OnoMiuraDeviceStatus *devicerebooting __attribute__((swift_name("devicerebooting")));
@property (class, readonly) OnoMiuraDeviceStatus *pinentryevent __attribute__((swift_name("pinentryevent")));
@property (class, readonly) OnoMiuraDeviceStatus *mpirestarting __attribute__((swift_name("mpirestarting")));
@property (class, readonly) OnoMiuraDeviceStatus *successbeep __attribute__((swift_name("successbeep")));
@property (class, readonly) OnoMiuraDeviceStatus *errorbeep __attribute__((swift_name("errorbeep")));
@property (class, readonly) OnoMiuraDeviceStatus *seephone __attribute__((swift_name("seephone")));
@property (class, readonly) OnoMiuraDeviceStatus *unknown __attribute__((swift_name("unknown")));
- (int32_t)compareToOther:(OnoMiuraDeviceStatus *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoMiuraButton")))
@interface OnoOnoMiuraButton : OnoKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) OnoOnoMiuraButton *anyNumber __attribute__((swift_name("anyNumber")));
@property (class, readonly) OnoOnoMiuraButton *acceptButton __attribute__((swift_name("acceptButton")));
@property (class, readonly) OnoOnoMiuraButton *cancelButton __attribute__((swift_name("cancelButton")));
@property (class, readonly) OnoOnoMiuraButton *deleteButton __attribute__((swift_name("deleteButton")));
@property (class, readonly) OnoOnoMiuraButton *upArrow __attribute__((swift_name("upArrow")));
@property (class, readonly) OnoOnoMiuraButton *downArrow __attribute__((swift_name("downArrow")));
@property (class, readonly) OnoOnoMiuraButton *m010BluetoothButton __attribute__((swift_name("m010BluetoothButton")));
- (int32_t)compareToOther:(OnoOnoMiuraButton *)other __attribute__((swift_name("compareTo(other:)")));
@property (readonly) int32_t keyIndex __attribute__((swift_name("keyIndex")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoMiuraButton.Companion")))
@interface OnoOnoMiuraButtonCompanion : OnoBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (OnoOnoMiuraButton * _Nullable)fromKeyIndexKeyIndex:(int32_t)keyIndex __attribute__((swift_name("fromKeyIndex(keyIndex:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoMiuraPinEntryError")))
@interface OnoOnoMiuraPinEntryError : OnoKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) OnoOnoMiuraPinEntryError *invalidParam __attribute__((swift_name("invalidParam")));
@property (class, readonly) OnoOnoMiuraPinEntryError *noPinKey __attribute__((swift_name("noPinKey")));
@property (class, readonly) OnoOnoMiuraPinEntryError *internalError __attribute__((swift_name("internalError")));
@property (class, readonly) OnoOnoMiuraPinEntryError *unknown __attribute__((swift_name("unknown")));
- (int32_t)compareToOther:(OnoOnoMiuraPinEntryError *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoMiuraTransactionError")))
@interface OnoOnoMiuraTransactionError : OnoKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) OnoOnoMiuraTransactionError *invalidData __attribute__((swift_name("invalidData")));
@property (class, readonly) OnoOnoMiuraTransactionError *terminalNotReady __attribute__((swift_name("terminalNotReady")));
@property (class, readonly) OnoOnoMiuraTransactionError *noSmartcardInSlot __attribute__((swift_name("noSmartcardInSlot")));
@property (class, readonly) OnoOnoMiuraTransactionError *invalidCardNoMsrAllowed __attribute__((swift_name("invalidCardNoMsrAllowed")));
@property (class, readonly) OnoOnoMiuraTransactionError *commandNotAllowed __attribute__((swift_name("commandNotAllowed")));
@property (class, readonly) OnoOnoMiuraTransactionError *dataMissing __attribute__((swift_name("dataMissing")));
@property (class, readonly) OnoOnoMiuraTransactionError *unsupportedCardMsrAllowed __attribute__((swift_name("unsupportedCardMsrAllowed")));
@property (class, readonly) OnoOnoMiuraTransactionError *cardReadErrorMsrAllowed __attribute__((swift_name("cardReadErrorMsrAllowed")));
@property (class, readonly) OnoOnoMiuraTransactionError *invalidIssuer __attribute__((swift_name("invalidIssuer")));
@property (class, readonly) OnoOnoMiuraTransactionError *userCancelled __attribute__((swift_name("userCancelled")));
@property (class, readonly) OnoOnoMiuraTransactionError *contactlessTimeout __attribute__((swift_name("contactlessTimeout")));
@property (class, readonly) OnoOnoMiuraTransactionError *contactlessAbortByCardInsert __attribute__((swift_name("contactlessAbortByCardInsert")));
@property (class, readonly) OnoOnoMiuraTransactionError *contactlessAbortBySwipe __attribute__((swift_name("contactlessAbortBySwipe")));
@property (class, readonly) OnoOnoMiuraTransactionError *contactlessInsertOrSwipe __attribute__((swift_name("contactlessInsertOrSwipe")));
@property (class, readonly) OnoOnoMiuraTransactionError *contactlessInsertSwipeOrOtherCard __attribute__((swift_name("contactlessInsertSwipeOrOtherCard")));
@property (class, readonly) OnoOnoMiuraTransactionError *contactlessInsertCard __attribute__((swift_name("contactlessInsertCard")));
@property (class, readonly) OnoOnoMiuraTransactionError *contactlessHardwareError __attribute__((swift_name("contactlessHardwareError")));
@property (class, readonly) OnoOnoMiuraTransactionError *unknown __attribute__((swift_name("unknown")));
- (int32_t)compareToOther:(OnoOnoMiuraTransactionError *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoMiuraTransactionStep")))
@interface OnoOnoMiuraTransactionStep : OnoKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) OnoOnoMiuraTransactionStep *initializing __attribute__((swift_name("initializing")));
@property (class, readonly) OnoOnoMiuraTransactionStep *authorizing __attribute__((swift_name("authorizing")));
@property (class, readonly) OnoOnoMiuraTransactionStep *aborting __attribute__((swift_name("aborting")));
@property (class, readonly) OnoOnoMiuraTransactionStep *pinEntry __attribute__((swift_name("pinEntry")));
@property (class, readonly) OnoOnoMiuraTransactionStep *doYouWantToTip __attribute__((swift_name("doYouWantToTip")));
@property (class, readonly) OnoOnoMiuraTransactionStep *tipAmountEntry __attribute__((swift_name("tipAmountEntry")));
- (int32_t)compareToOther:(OnoOnoMiuraTransactionStep *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((swift_name("ITimerFactory")))
@protocol OnoITimerFactory
@required
- (id<OnoITimerJob>)startTimerRunAfter:(int64_t)runAfter block:(void (^)(void))block __attribute__((swift_name("startTimer(runAfter:block:)")));
@end;

__attribute__((swift_name("ITimerJob")))
@protocol OnoITimerJob
@required
- (void)cancel __attribute__((swift_name("cancel()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MockTimerFactory")))
@interface OnoMockTimerFactory : OnoBase <OnoITimerFactory>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (id<OnoITimerJob>)startTimerRunAfter:(int64_t)runAfter block:(void (^)(void))block __attribute__((swift_name("startTimer(runAfter:block:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MockTimerJob")))
@interface OnoMockTimerJob : OnoBase <OnoITimerJob>
- (instancetype)initWithRunAfter:(int64_t)runAfter block:(void (^)(void))block __attribute__((swift_name("init(runAfter:block:)"))) __attribute__((objc_designated_initializer));
- (void)cancel __attribute__((swift_name("cancel()")));
@property (readonly) void (^block)(void) __attribute__((swift_name("block")));
@property (readonly) int64_t runAfter __attribute__((swift_name("runAfter")));
@end;

__attribute__((swift_name("QueueActions")))
@interface OnoQueueActions : OnoBase
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("QueueActions.ExecutionTimeout")))
@interface OnoQueueActionsExecutionTimeout : OnoAction
- (instancetype)initWithQueueCommand:(id<OnoIQueueCommandType>)queueCommand actionReference:(OnoQueueCommand *)actionReference __attribute__((swift_name("init(queueCommand:actionReference:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
@property (readonly) OnoQueueCommand *actionReference __attribute__((swift_name("actionReference")));
@property (readonly) id<OnoIQueueCommandType> queueCommand __attribute__((swift_name("queueCommand")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("QueueActions.ServiceBusy")))
@interface OnoQueueActionsServiceBusy : OnoAction
- (instancetype)initWithAttemptedCommand:(id<OnoIQueueCommandType>)attemptedCommand currentCommand:(id<OnoIQueueCommandType>)currentCommand __attribute__((swift_name("init(attemptedCommand:currentCommand:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
@property (readonly) id<OnoIQueueCommandType> attemptedCommand __attribute__((swift_name("attemptedCommand")));
@property (readonly) id<OnoIQueueCommandType> currentCommand __attribute__((swift_name("currentCommand")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("QueueActions.StartTimeout")))
@interface OnoQueueActionsStartTimeout : OnoAction
- (instancetype)initWithQueueCommand:(id<OnoIQueueCommandType>)queueCommand actionReference:(OnoQueueCommand *)actionReference __attribute__((swift_name("init(queueCommand:actionReference:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
@property (readonly) OnoQueueCommand *actionReference __attribute__((swift_name("actionReference")));
@property (readonly) id<OnoIQueueCommandType> queueCommand __attribute__((swift_name("queueCommand")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("QueueCommand")))
@interface OnoQueueCommand : OnoDispatcher
- (instancetype)initWithDispatch:(void (^)(OnoAction *))dispatch sdkServiceTag:(NSString *)sdkServiceTag commandType:(id<OnoIQueueCommandType>)commandType actionExecution:(void (^)(id<OnoISDKService>))actionExecution executionTimeoutMillis:(OnoLong * _Nullable)executionTimeoutMillis startTimeoutMillis:(int64_t)startTimeoutMillis __attribute__((swift_name("init(dispatch:sdkServiceTag:commandType:actionExecution:executionTimeoutMillis:startTimeoutMillis:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithDispatch:(void (^)(OnoAction *))dispatch __attribute__((swift_name("init(dispatch:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (void)executionFinished __attribute__((swift_name("executionFinished()")));
- (void)executionStarted __attribute__((swift_name("executionStarted()")));
- (void)startExecutionTimeout __attribute__((swift_name("startExecutionTimeout()")));
- (void)startStartTimeout __attribute__((swift_name("startStartTimeout()")));
- (void)terminateCommand __attribute__((swift_name("terminateCommand()")));
@property (readonly) void (^actionExecution)(id<OnoISDKService>) __attribute__((swift_name("actionExecution")));
@property (readonly) id<OnoIQueueCommandType> commandType __attribute__((swift_name("commandType")));
@property (readonly) OnoLong * _Nullable executionTimeoutMillis __attribute__((swift_name("executionTimeoutMillis")));
@property (readonly) int64_t startTimeoutMillis __attribute__((swift_name("startTimeoutMillis")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalCommandQueue")))
@interface OnoTerminalCommandQueue : OnoDispatcher
- (instancetype)initWithSdkService:(id<OnoISDKService>)sdkService dispatch:(void (^)(OnoAction *))dispatch __attribute__((swift_name("init(sdkService:dispatch:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithDispatch:(void (^)(OnoAction *))dispatch __attribute__((swift_name("init(dispatch:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (void)clearAllEnqueuedCommands __attribute__((swift_name("clearAllEnqueuedCommands()")));
- (void)clearIfActionActionType:(id<OnoIQueueCommandType>)actionType __attribute__((swift_name("clearIfAction(actionType:)")));
- (void)clearIfOneOfActionsActions:(NSSet<id<OnoIQueueCommandType>> *)actions __attribute__((swift_name("clearIfOneOfActions(actions:)")));
- (void)clearSpecificActionQueueCommand:(OnoQueueCommand *)queueCommand __attribute__((swift_name("clearSpecificAction(queueCommand:)")));
- (id<OnoIQueueCommandType> _Nullable)getCurrentlyBusyAction __attribute__((swift_name("getCurrentlyBusyAction()")));
- (void)performActionOnServiceIfNotBusyQueueCommand:(OnoQueueCommand *)queueCommand __attribute__((swift_name("performActionOnServiceIfNotBusy(queueCommand:)")));
- (void)performActionOnServiceNowQueueCommand:(OnoQueueCommand *)queueCommand __attribute__((swift_name("performActionOnServiceNow(queueCommand:)")));
- (void)performActionOnServiceWhenReadyQueueCommand:(OnoQueueCommand *)queueCommand __attribute__((swift_name("performActionOnServiceWhenReady(queueCommand:)")));
- (id<OnoISDKService>)performNonBlockingActionOnService __attribute__((swift_name("performNonBlockingActionOnService()")));
@property (readonly) NSString *sdkServiceTag __attribute__((swift_name("sdkServiceTag")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Environment")))
@interface OnoEnvironment : OnoKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) OnoEnvironment *production __attribute__((swift_name("production")));
@property (class, readonly) OnoEnvironment *staging __attribute__((swift_name("staging")));
@property (class, readonly) OnoEnvironment *development __attribute__((swift_name("development")));
@property (class, readonly) OnoEnvironment *testing __attribute__((swift_name("testing")));
- (int32_t)compareToOther:(OnoEnvironment *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("NetworkConnectionType")))
@interface OnoNetworkConnectionType : OnoKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) OnoNetworkConnectionType *none __attribute__((swift_name("none")));
@property (class, readonly) OnoNetworkConnectionType *mobile __attribute__((swift_name("mobile")));
@property (class, readonly) OnoNetworkConnectionType *wifi __attribute__((swift_name("wifi")));
- (int32_t)compareToOther:(OnoNetworkConnectionType *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((swift_name("TerminalActions")))
@interface OnoTerminalActions : OnoBase
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalActions.AskForTip")))
@interface OnoTerminalActionsAskForTip : OnoAction
- (instancetype)initWithTransactionAmountInCents:(int64_t)transactionAmountInCents currency:(NSString *)currency __attribute__((swift_name("init(transactionAmountInCents:currency:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (int64_t)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (OnoTerminalActionsAskForTip *)doCopyTransactionAmountInCents:(int64_t)transactionAmountInCents currency:(NSString *)currency __attribute__((swift_name("doCopy(transactionAmountInCents:currency:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *currency __attribute__((swift_name("currency")));
@property (readonly) int64_t transactionAmountInCents __attribute__((swift_name("transactionAmountInCents")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalActions.CancelTransaction")))
@interface OnoTerminalActionsCancelTransaction : OnoAction
- (instancetype)initWithMessage:(NSString *)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoTerminalActionsCancelTransaction *)doCopyMessage:(NSString *)message __attribute__((swift_name("doCopy(message:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *message __attribute__((swift_name("message")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalActions.CheckForUpdates")))
@interface OnoTerminalActionsCheckForUpdates : OnoAction
- (instancetype)initWithTerminal:(OnoTerminal *)terminal __attribute__((swift_name("init(terminal:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoTerminal *)component1 __attribute__((swift_name("component1()")));
- (OnoTerminalActionsCheckForUpdates *)doCopyTerminal:(OnoTerminal *)terminal __attribute__((swift_name("doCopy(terminal:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoTerminal *terminal __attribute__((swift_name("terminal")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalActions.ConnectAnyAvailableTerminal")))
@interface OnoTerminalActionsConnectAnyAvailableTerminal : OnoAction
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalActions.ConnectTerminal")))
@interface OnoTerminalActionsConnectTerminal : OnoAction
- (instancetype)initWithAddress:(NSString *)address manufacturer:(OnoManufacturer *)manufacturer __attribute__((swift_name("init(address:manufacturer:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoManufacturer *)component2 __attribute__((swift_name("component2()")));
- (OnoTerminalActionsConnectTerminal *)doCopyAddress:(NSString *)address manufacturer:(OnoManufacturer *)manufacturer __attribute__((swift_name("doCopy(address:manufacturer:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *address __attribute__((swift_name("address")));
@property (readonly) OnoManufacturer *manufacturer __attribute__((swift_name("manufacturer")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalActions.ConnectingMessageUpdated")))
@interface OnoTerminalActionsConnectingMessageUpdated : OnoAction
- (instancetype)initWithMessage:(NSString *)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoTerminalActionsConnectingMessageUpdated *)doCopyMessage:(NSString *)message __attribute__((swift_name("doCopy(message:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *message __attribute__((swift_name("message")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalActions.ConnectionError")))
@interface OnoTerminalActionsConnectionError : OnoAction
- (instancetype)initWithErrorMessage:(NSString *)errorMessage isStillPaired:(BOOL)isStillPaired __attribute__((swift_name("init(errorMessage:isStillPaired:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (BOOL)component2 __attribute__((swift_name("component2()")));
- (OnoTerminalActionsConnectionError *)doCopyErrorMessage:(NSString *)errorMessage isStillPaired:(BOOL)isStillPaired __attribute__((swift_name("doCopy(errorMessage:isStillPaired:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *errorMessage __attribute__((swift_name("errorMessage")));
@property (readonly) BOOL isStillPaired __attribute__((swift_name("isStillPaired")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalActions.DisconnectTerminal")))
@interface OnoTerminalActionsDisconnectTerminal : OnoAction
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalActions.EMVConfigVersionRetrieved")))
@interface OnoTerminalActionsEMVConfigVersionRetrieved : OnoAction
- (instancetype)initWithVersion:(NSString * _Nullable)version __attribute__((swift_name("init(version:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString * _Nullable)component1 __attribute__((swift_name("component1()")));
- (OnoTerminalActionsEMVConfigVersionRetrieved *)doCopyVersion:(NSString * _Nullable)version __attribute__((swift_name("doCopy(version:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable version __attribute__((swift_name("version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalActions.FailTransaction")))
@interface OnoTerminalActionsFailTransaction : OnoAction
- (instancetype)initWithMessage:(NSString *)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoTerminalActionsFailTransaction *)doCopyMessage:(NSString *)message __attribute__((swift_name("doCopy(message:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *message __attribute__((swift_name("message")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalActions.InitiateRki")))
@interface OnoTerminalActionsInitiateRki : OnoAction
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalActions.ModelUpdated")))
@interface OnoTerminalActionsModelUpdated : OnoAction
- (instancetype)initWithTerminalModel:(OnoTerminalModel *)terminalModel __attribute__((swift_name("init(terminalModel:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoTerminalModel *)component1 __attribute__((swift_name("component1()")));
- (OnoTerminalActionsModelUpdated *)doCopyTerminalModel:(OnoTerminalModel *)terminalModel __attribute__((swift_name("doCopy(terminalModel:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoTerminalModel *terminalModel __attribute__((swift_name("terminalModel")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalActions.NewTerminalPaired")))
@interface OnoTerminalActionsNewTerminalPaired : OnoAction
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalActions.PairTerminal")))
@interface OnoTerminalActionsPairTerminal : OnoAction
- (instancetype)initWithAddress:(NSString *)address manufacturer:(OnoManufacturer *)manufacturer __attribute__((swift_name("init(address:manufacturer:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoManufacturer *)component2 __attribute__((swift_name("component2()")));
- (OnoTerminalActionsPairTerminal *)doCopyAddress:(NSString *)address manufacturer:(OnoManufacturer *)manufacturer __attribute__((swift_name("doCopy(address:manufacturer:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *address __attribute__((swift_name("address")));
@property (readonly) OnoManufacturer *manufacturer __attribute__((swift_name("manufacturer")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalActions.RkiFailed")))
@interface OnoTerminalActionsRkiFailed : OnoAction
- (instancetype)initWithReason:(NSString *)reason __attribute__((swift_name("init(reason:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithErrorDetails:(OnoErrorDetails *)errorDetails __attribute__((swift_name("init(errorDetails:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoErrorDetails *)component1 __attribute__((swift_name("component1()")));
- (OnoTerminalActionsRkiFailed *)doCopyErrorDetails:(OnoErrorDetails *)errorDetails __attribute__((swift_name("doCopy(errorDetails:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoErrorDetails *errorDetails __attribute__((swift_name("errorDetails")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalActions.RkiServerRequest")))
@interface OnoTerminalActionsRkiServerRequest : OnoAction
- (instancetype)initWithRkiRequestData:(OnoSensitiveMap *)rkiRequestData __attribute__((swift_name("init(rkiRequestData:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoSensitiveMap *)component1 __attribute__((swift_name("component1()")));
- (OnoTerminalActionsRkiServerRequest *)doCopyRkiRequestData:(OnoSensitiveMap *)rkiRequestData __attribute__((swift_name("doCopy(rkiRequestData:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoSensitiveMap *rkiRequestData __attribute__((swift_name("rkiRequestData")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalActions.RkiServerResponse")))
@interface OnoTerminalActionsRkiServerResponse : OnoAction
- (instancetype)initWithTerminal:(OnoTerminal *)terminal rkiResponseData:(OnoSensitiveMap *)rkiResponseData __attribute__((swift_name("init(terminal:rkiResponseData:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoTerminal *)component1 __attribute__((swift_name("component1()")));
- (OnoSensitiveMap *)component2 __attribute__((swift_name("component2()")));
- (OnoTerminalActionsRkiServerResponse *)doCopyTerminal:(OnoTerminal *)terminal rkiResponseData:(OnoSensitiveMap *)rkiResponseData __attribute__((swift_name("doCopy(terminal:rkiResponseData:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoSensitiveMap *rkiResponseData __attribute__((swift_name("rkiResponseData")));
@property (readonly) OnoTerminal *terminal __attribute__((swift_name("terminal")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalActions.RkiSuccessful")))
@interface OnoTerminalActionsRkiSuccessful : OnoAction
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalActions.SerialNumberUpdated")))
@interface OnoTerminalActionsSerialNumberUpdated : OnoAction
- (instancetype)initWithSerialNumber:(NSString *)serialNumber __attribute__((swift_name("init(serialNumber:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoTerminalActionsSerialNumberUpdated *)doCopySerialNumber:(NSString *)serialNumber __attribute__((swift_name("doCopy(serialNumber:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *serialNumber __attribute__((swift_name("serialNumber")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalActions.SetTerminalConfig")))
@interface OnoTerminalActionsSetTerminalConfig : OnoAction
- (instancetype)initWithManufacturer:(OnoManufacturer *)manufacturer config:(OnoTerminalConfig *)config __attribute__((swift_name("init(manufacturer:config:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoManufacturer *)component1 __attribute__((swift_name("component1()")));
- (OnoTerminalConfig *)component2 __attribute__((swift_name("component2()")));
- (OnoTerminalActionsSetTerminalConfig *)doCopyManufacturer:(OnoManufacturer *)manufacturer config:(OnoTerminalConfig *)config __attribute__((swift_name("doCopy(manufacturer:config:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoTerminalConfig *config __attribute__((swift_name("config")));
@property (readonly) OnoManufacturer *manufacturer __attribute__((swift_name("manufacturer")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalActions.StartSearching")))
@interface OnoTerminalActionsStartSearching : OnoAction
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalActions.StartTransaction")))
@interface OnoTerminalActionsStartTransaction : OnoAction
- (instancetype)initWithAmountInCents:(int64_t)amountInCents clientTransactionUUID:(NSString *)clientTransactionUUID currency:(NSString *)currency initiatedBy:(OnoTransactionInitiator *)initiatedBy alsoAskForTip:(BOOL)alsoAskForTip __attribute__((swift_name("init(amountInCents:clientTransactionUUID:currency:initiatedBy:alsoAskForTip:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (int64_t)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (OnoTransactionInitiator *)component4 __attribute__((swift_name("component4()")));
- (BOOL)component5 __attribute__((swift_name("component5()")));
- (OnoTerminalActionsStartTransaction *)doCopyAmountInCents:(int64_t)amountInCents clientTransactionUUID:(NSString *)clientTransactionUUID currency:(NSString *)currency initiatedBy:(OnoTransactionInitiator *)initiatedBy alsoAskForTip:(BOOL)alsoAskForTip __attribute__((swift_name("doCopy(amountInCents:clientTransactionUUID:currency:initiatedBy:alsoAskForTip:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL alsoAskForTip __attribute__((swift_name("alsoAskForTip")));
@property (readonly) int64_t amountInCents __attribute__((swift_name("amountInCents")));
@property (readonly) NSString *clientTransactionUUID __attribute__((swift_name("clientTransactionUUID")));
@property (readonly) NSString *currency __attribute__((swift_name("currency")));
@property (readonly) OnoTransactionInitiator *initiatedBy __attribute__((swift_name("initiatedBy")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalActions.StopSearching")))
@interface OnoTerminalActionsStopSearching : OnoAction
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalActions.TerminalAvailable")))
@interface OnoTerminalActionsTerminalAvailable : OnoAction
- (instancetype)initWithTerminal:(OnoTerminal *)terminal __attribute__((swift_name("init(terminal:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoTerminal *)component1 __attribute__((swift_name("component1()")));
- (OnoTerminalActionsTerminalAvailable *)doCopyTerminal:(OnoTerminal *)terminal __attribute__((swift_name("doCopy(terminal:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoTerminal *terminal __attribute__((swift_name("terminal")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalActions.TerminalBusyError")))
@interface OnoTerminalActionsTerminalBusyError : OnoAction
- (instancetype)initWithMessage:(NSString *)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoTerminalActionsTerminalBusyError *)doCopyMessage:(NSString *)message __attribute__((swift_name("doCopy(message:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *message __attribute__((swift_name("message")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalActions.TerminalConnected")))
@interface OnoTerminalActionsTerminalConnected : OnoAction
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalActions.TerminalConnectionTypeCleared")))
@interface OnoTerminalActionsTerminalConnectionTypeCleared : OnoAction
- (instancetype)initWithConnectionType:(OnoConnectionType *)connectionType errorDetails:(OnoErrorDetails *)errorDetails __attribute__((swift_name("init(connectionType:errorDetails:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoConnectionType *)component1 __attribute__((swift_name("component1()")));
- (OnoErrorDetails *)component2 __attribute__((swift_name("component2()")));
- (OnoTerminalActionsTerminalConnectionTypeCleared *)doCopyConnectionType:(OnoConnectionType *)connectionType errorDetails:(OnoErrorDetails *)errorDetails __attribute__((swift_name("doCopy(connectionType:errorDetails:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoConnectionType *connectionType __attribute__((swift_name("connectionType")));
@property (readonly) OnoErrorDetails *errorDetails __attribute__((swift_name("errorDetails")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalActions.TerminalDisconnected")))
@interface OnoTerminalActionsTerminalDisconnected : OnoAction
- (instancetype)initWithErrorDetails:(OnoErrorDetails *)errorDetails __attribute__((swift_name("init(errorDetails:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoErrorDetails *)component1 __attribute__((swift_name("component1()")));
- (OnoTerminalActionsTerminalDisconnected *)doCopyErrorDetails:(OnoErrorDetails *)errorDetails __attribute__((swift_name("doCopy(errorDetails:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoErrorDetails *errorDetails __attribute__((swift_name("errorDetails")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalActions.TerminalInfoUpdated")))
@interface OnoTerminalActionsTerminalInfoUpdated : OnoAction
- (instancetype)initWithInfo:(OnoTerminalInfo *)info __attribute__((swift_name("init(info:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoTerminalInfo *)component1 __attribute__((swift_name("component1()")));
- (OnoTerminalActionsTerminalInfoUpdated *)doCopyInfo:(OnoTerminalInfo *)info __attribute__((swift_name("doCopy(info:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoTerminalInfo *info __attribute__((swift_name("info")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalActions.TipAmountValue")))
@interface OnoTerminalActionsTipAmountValue : OnoAction
- (instancetype)initWithTipAmountInCents:(int32_t)tipAmountInCents __attribute__((swift_name("init(tipAmountInCents:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (int32_t)component1 __attribute__((swift_name("component1()")));
- (OnoTerminalActionsTipAmountValue *)doCopyTipAmountInCents:(int32_t)tipAmountInCents __attribute__((swift_name("doCopy(tipAmountInCents:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t tipAmountInCents __attribute__((swift_name("tipAmountInCents")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalActions.TipRequestFailed")))
@interface OnoTerminalActionsTipRequestFailed : OnoAction
- (instancetype)initWithReason:(NSString *)reason __attribute__((swift_name("init(reason:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoTerminalActionsTipRequestFailed *)doCopyReason:(NSString *)reason __attribute__((swift_name("doCopy(reason:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *reason __attribute__((swift_name("reason")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalActions.UpdateFinished")))
@interface OnoTerminalActionsUpdateFinished : OnoAction
- (instancetype)initWithAvailableUpdate:(OnoAvailableUpdate *)availableUpdate wasSuccessful:(BOOL)wasSuccessful errorMessage:(NSString * _Nullable)errorMessage manufacturer:(OnoManufacturer *)manufacturer __attribute__((swift_name("init(availableUpdate:wasSuccessful:errorMessage:manufacturer:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoAvailableUpdate *)component1 __attribute__((swift_name("component1()")));
- (BOOL)component2 __attribute__((swift_name("component2()")));
- (NSString * _Nullable)component3 __attribute__((swift_name("component3()")));
- (OnoManufacturer *)component4 __attribute__((swift_name("component4()")));
- (OnoTerminalActionsUpdateFinished *)doCopyAvailableUpdate:(OnoAvailableUpdate *)availableUpdate wasSuccessful:(BOOL)wasSuccessful errorMessage:(NSString * _Nullable)errorMessage manufacturer:(OnoManufacturer *)manufacturer __attribute__((swift_name("doCopy(availableUpdate:wasSuccessful:errorMessage:manufacturer:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoAvailableUpdate *availableUpdate __attribute__((swift_name("availableUpdate")));
@property (readonly) NSString * _Nullable errorMessage __attribute__((swift_name("errorMessage")));
@property (readonly) OnoManufacturer *manufacturer __attribute__((swift_name("manufacturer")));
@property (readonly) BOOL wasSuccessful __attribute__((swift_name("wasSuccessful")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalActions.UpdateTerminalInfo")))
@interface OnoTerminalActionsUpdateTerminalInfo : OnoAction
- (instancetype)initWithManufacturer:(OnoManufacturer *)manufacturer __attribute__((swift_name("init(manufacturer:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoManufacturer *)component1 __attribute__((swift_name("component1()")));
- (OnoTerminalActionsUpdateTerminalInfo *)doCopyManufacturer:(OnoManufacturer *)manufacturer __attribute__((swift_name("doCopy(manufacturer:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoManufacturer *manufacturer __attribute__((swift_name("manufacturer")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalActions.UpdatesAvailable")))
@interface OnoTerminalActionsUpdatesAvailable : OnoAction
- (instancetype)initWithAvailableUpdates:(NSArray<OnoAvailableUpdate *> *)availableUpdates __attribute__((swift_name("init(availableUpdates:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSArray<OnoAvailableUpdate *> *)component1 __attribute__((swift_name("component1()")));
- (OnoTerminalActionsUpdatesAvailable *)doCopyAvailableUpdates:(NSArray<OnoAvailableUpdate *> *)availableUpdates __attribute__((swift_name("doCopy(availableUpdates:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<OnoAvailableUpdate *> *availableUpdates __attribute__((swift_name("availableUpdates")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalActions.UpdatesFailed")))
@interface OnoTerminalActionsUpdatesFailed : OnoAction
- (instancetype)initWithErrorMessage:(NSString *)errorMessage __attribute__((swift_name("init(errorMessage:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoTerminalActionsUpdatesFailed *)doCopyErrorMessage:(NSString *)errorMessage __attribute__((swift_name("doCopy(errorMessage:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *errorMessage __attribute__((swift_name("errorMessage")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalCommandDriver")))
@interface OnoTerminalCommandDriver : OnoCommandDriver
- (instancetype)initWithTerminalListener:(id<OnoTerminalListener>)terminalListener __attribute__((swift_name("init(terminalListener:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (void)receiveCommandCommand:(OnoCommand *)command __attribute__((swift_name("receiveCommand(command:)")));
@property (readonly) id<OnoTerminalListener> terminalListener __attribute__((swift_name("terminalListener")));
@end;

__attribute__((swift_name("TerminalCommands")))
@interface OnoTerminalCommands : OnoBase
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalCommands.AbortTipEntry")))
@interface OnoTerminalCommandsAbortTipEntry : OnoAction
- (instancetype)initWithReason:(NSString *)reason __attribute__((swift_name("init(reason:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoTerminalCommandsAbortTipEntry *)doCopyReason:(NSString *)reason __attribute__((swift_name("doCopy(reason:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *reason __attribute__((swift_name("reason")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalCommands.ActiveTerminalDisconnected")))
@interface OnoTerminalCommandsActiveTerminalDisconnected : OnoCommand
- (instancetype)initWithTerminal:(OnoTerminal *)terminal errorDetails:(OnoErrorDetails *)errorDetails __attribute__((swift_name("init(terminal:errorDetails:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoTerminal *)component1 __attribute__((swift_name("component1()")));
- (OnoErrorDetails *)component2 __attribute__((swift_name("component2()")));
- (OnoTerminalCommandsActiveTerminalDisconnected *)doCopyTerminal:(OnoTerminal *)terminal errorDetails:(OnoErrorDetails *)errorDetails __attribute__((swift_name("doCopy(terminal:errorDetails:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoErrorDetails *errorDetails __attribute__((swift_name("errorDetails")));
@property (readonly) OnoTerminal *terminal __attribute__((swift_name("terminal")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalCommands.ConnectTerminal")))
@interface OnoTerminalCommandsConnectTerminal : OnoCommand
- (instancetype)initWithTerminal:(OnoTerminal *)terminal __attribute__((swift_name("init(terminal:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoTerminal *)component1 __attribute__((swift_name("component1()")));
- (OnoTerminalCommandsConnectTerminal *)doCopyTerminal:(OnoTerminal *)terminal __attribute__((swift_name("doCopy(terminal:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoTerminal *terminal __attribute__((swift_name("terminal")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalCommands.ConnectionFailed")))
@interface OnoTerminalCommandsConnectionFailed : OnoCommand
- (instancetype)initWithError:(NSString *)error __attribute__((swift_name("init(error:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoTerminalCommandsConnectionFailed *)doCopyError:(NSString *)error __attribute__((swift_name("doCopy(error:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *error __attribute__((swift_name("error")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalCommands.DisconnectTerminal")))
@interface OnoTerminalCommandsDisconnectTerminal : OnoCommand
- (instancetype)initWithTerminal:(OnoTerminal *)terminal __attribute__((swift_name("init(terminal:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoTerminal *)component1 __attribute__((swift_name("component1()")));
- (OnoTerminalCommandsDisconnectTerminal *)doCopyTerminal:(OnoTerminal *)terminal __attribute__((swift_name("doCopy(terminal:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoTerminal *terminal __attribute__((swift_name("terminal")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalCommands.GetRkiData")))
@interface OnoTerminalCommandsGetRkiData : OnoCommand
- (instancetype)initWithTerminal:(OnoTerminal *)terminal __attribute__((swift_name("init(terminal:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoTerminal *)component1 __attribute__((swift_name("component1()")));
- (OnoTerminalCommandsGetRkiData *)doCopyTerminal:(OnoTerminal *)terminal __attribute__((swift_name("doCopy(terminal:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoTerminal *terminal __attribute__((swift_name("terminal")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalCommands.InitializeTransaction")))
@interface OnoTerminalCommandsInitializeTransaction : OnoCommand
- (instancetype)initWithSerialNumber:(NSString *)serialNumber amount:(int64_t)amount __attribute__((swift_name("init(serialNumber:amount:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (int64_t)component2 __attribute__((swift_name("component2()")));
- (OnoTerminalCommandsInitializeTransaction *)doCopySerialNumber:(NSString *)serialNumber amount:(int64_t)amount __attribute__((swift_name("doCopy(serialNumber:amount:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int64_t amount __attribute__((swift_name("amount")));
@property (readonly) NSString *serialNumber __attribute__((swift_name("serialNumber")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalCommands.PairTerminal")))
@interface OnoTerminalCommandsPairTerminal : OnoAction
- (instancetype)initWithTerminal:(OnoTerminal *)terminal __attribute__((swift_name("init(terminal:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoTerminal *)component1 __attribute__((swift_name("component1()")));
- (OnoTerminalCommandsPairTerminal *)doCopyTerminal:(OnoTerminal *)terminal __attribute__((swift_name("doCopy(terminal:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoTerminal *terminal __attribute__((swift_name("terminal")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalCommands.PairingFailed")))
@interface OnoTerminalCommandsPairingFailed : OnoCommand
- (instancetype)initWithErrorMessage:(NSString *)errorMessage __attribute__((swift_name("init(errorMessage:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoTerminalCommandsPairingFailed *)doCopyErrorMessage:(NSString *)errorMessage __attribute__((swift_name("doCopy(errorMessage:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *errorMessage __attribute__((swift_name("errorMessage")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalCommands.PerformUpdate")))
@interface OnoTerminalCommandsPerformUpdate : OnoCommand
- (instancetype)initWithAvailableUpdate:(OnoAvailableUpdate *)availableUpdate terminal:(OnoTerminal *)terminal __attribute__((swift_name("init(availableUpdate:terminal:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoAvailableUpdate *)component1 __attribute__((swift_name("component1()")));
- (OnoTerminal *)component2 __attribute__((swift_name("component2()")));
- (OnoTerminalCommandsPerformUpdate *)doCopyAvailableUpdate:(OnoAvailableUpdate *)availableUpdate terminal:(OnoTerminal *)terminal __attribute__((swift_name("doCopy(availableUpdate:terminal:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoAvailableUpdate *availableUpdate __attribute__((swift_name("availableUpdate")));
@property (readonly) OnoTerminal *terminal __attribute__((swift_name("terminal")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalCommands.ProcessRkiServerRequest")))
@interface OnoTerminalCommandsProcessRkiServerRequest : OnoCommand
- (instancetype)initWithTerminal:(OnoTerminal *)terminal rkiRequestData:(OnoSensitiveMap *)rkiRequestData __attribute__((swift_name("init(terminal:rkiRequestData:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoTerminal *)component1 __attribute__((swift_name("component1()")));
- (OnoSensitiveMap *)component2 __attribute__((swift_name("component2()")));
- (OnoTerminalCommandsProcessRkiServerRequest *)doCopyTerminal:(OnoTerminal *)terminal rkiRequestData:(OnoSensitiveMap *)rkiRequestData __attribute__((swift_name("doCopy(terminal:rkiRequestData:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoSensitiveMap *rkiRequestData __attribute__((swift_name("rkiRequestData")));
@property (readonly) OnoTerminal *terminal __attribute__((swift_name("terminal")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalCommands.ProcessRkiServerResponse")))
@interface OnoTerminalCommandsProcessRkiServerResponse : OnoCommand
- (instancetype)initWithTerminal:(OnoTerminal *)terminal rkiResponseData:(OnoSensitiveMap *)rkiResponseData __attribute__((swift_name("init(terminal:rkiResponseData:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoTerminal *)component1 __attribute__((swift_name("component1()")));
- (OnoSensitiveMap *)component2 __attribute__((swift_name("component2()")));
- (OnoTerminalCommandsProcessRkiServerResponse *)doCopyTerminal:(OnoTerminal *)terminal rkiResponseData:(OnoSensitiveMap *)rkiResponseData __attribute__((swift_name("doCopy(terminal:rkiResponseData:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoSensitiveMap *rkiResponseData __attribute__((swift_name("rkiResponseData")));
@property (readonly) OnoTerminal *terminal __attribute__((swift_name("terminal")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalCommands.RequestTipAmount")))
@interface OnoTerminalCommandsRequestTipAmount : OnoCommand
- (instancetype)initWithTransactionAmount:(int64_t)transactionAmount transactionCurrency:(OnoCurrency *)transactionCurrency terminal:(OnoTerminal *)terminal __attribute__((swift_name("init(transactionAmount:transactionCurrency:terminal:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (int64_t)component1 __attribute__((swift_name("component1()")));
- (OnoCurrency *)component2 __attribute__((swift_name("component2()")));
- (OnoTerminal *)component3 __attribute__((swift_name("component3()")));
- (OnoTerminalCommandsRequestTipAmount *)doCopyTransactionAmount:(int64_t)transactionAmount transactionCurrency:(OnoCurrency *)transactionCurrency terminal:(OnoTerminal *)terminal __attribute__((swift_name("doCopy(transactionAmount:transactionCurrency:terminal:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoTerminal *terminal __attribute__((swift_name("terminal")));
@property (readonly) int64_t transactionAmount __attribute__((swift_name("transactionAmount")));
@property (readonly) OnoCurrency *transactionCurrency __attribute__((swift_name("transactionCurrency")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalCommands.RkiError")))
@interface OnoTerminalCommandsRkiError : OnoCommand
- (instancetype)initWithTerminal:(OnoTerminal *)terminal errorDetails:(OnoErrorDetails *)errorDetails __attribute__((swift_name("init(terminal:errorDetails:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoTerminal *)component1 __attribute__((swift_name("component1()")));
- (OnoErrorDetails *)component2 __attribute__((swift_name("component2()")));
- (OnoTerminalCommandsRkiError *)doCopyTerminal:(OnoTerminal *)terminal errorDetails:(OnoErrorDetails *)errorDetails __attribute__((swift_name("doCopy(terminal:errorDetails:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoErrorDetails *errorDetails __attribute__((swift_name("errorDetails")));
@property (readonly) OnoTerminal *terminal __attribute__((swift_name("terminal")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalCommands.RkiSuccessful")))
@interface OnoTerminalCommandsRkiSuccessful : OnoCommand
- (instancetype)initWithTerminal:(OnoTerminal *)terminal __attribute__((swift_name("init(terminal:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoTerminal *)component1 __attribute__((swift_name("component1()")));
- (OnoTerminalCommandsRkiSuccessful *)doCopyTerminal:(OnoTerminal *)terminal __attribute__((swift_name("doCopy(terminal:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoTerminal *terminal __attribute__((swift_name("terminal")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalCommands.ShowPairingDialog")))
@interface OnoTerminalCommandsShowPairingDialog : OnoCommand
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalCommands.StartingConnectionFailed")))
@interface OnoTerminalCommandsStartingConnectionFailed : OnoCommand
- (instancetype)initWithErrorMessage:(NSString *)errorMessage __attribute__((swift_name("init(errorMessage:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoTerminalCommandsStartingConnectionFailed *)doCopyErrorMessage:(NSString *)errorMessage __attribute__((swift_name("doCopy(errorMessage:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *errorMessage __attribute__((swift_name("errorMessage")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalCommands.TerminalConnected")))
@interface OnoTerminalCommandsTerminalConnected : OnoCommand
- (instancetype)initWithTerminal:(OnoTerminal *)terminal __attribute__((swift_name("init(terminal:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoTerminal *)component1 __attribute__((swift_name("component1()")));
- (OnoTerminalCommandsTerminalConnected *)doCopyTerminal:(OnoTerminal *)terminal __attribute__((swift_name("doCopy(terminal:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoTerminal *terminal __attribute__((swift_name("terminal")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalCommands.TerminalConsideredIdle")))
@interface OnoTerminalCommandsTerminalConsideredIdle : OnoCommand
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalCommands.TerminalDisconnected")))
@interface OnoTerminalCommandsTerminalDisconnected : OnoCommand
- (instancetype)initWithSerialNumber:(NSString * _Nullable)serialNumber __attribute__((swift_name("init(serialNumber:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString * _Nullable)component1 __attribute__((swift_name("component1()")));
- (OnoTerminalCommandsTerminalDisconnected *)doCopySerialNumber:(NSString * _Nullable)serialNumber __attribute__((swift_name("doCopy(serialNumber:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable serialNumber __attribute__((swift_name("serialNumber")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalCommands.TerminalWithAddressNotFound")))
@interface OnoTerminalCommandsTerminalWithAddressNotFound : OnoCommand
- (instancetype)initWithAddress:(NSString *)address __attribute__((swift_name("init(address:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoTerminalCommandsTerminalWithAddressNotFound *)doCopyAddress:(NSString *)address __attribute__((swift_name("doCopy(address:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *address __attribute__((swift_name("address")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalCommands.TipEntered")))
@interface OnoTerminalCommandsTipEntered : OnoCommand
- (instancetype)initWithTipAmountInCents:(int32_t)tipAmountInCents __attribute__((swift_name("init(tipAmountInCents:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (int32_t)component1 __attribute__((swift_name("component1()")));
- (OnoTerminalCommandsTipEntered *)doCopyTipAmountInCents:(int32_t)tipAmountInCents __attribute__((swift_name("doCopy(tipAmountInCents:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t tipAmountInCents __attribute__((swift_name("tipAmountInCents")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalCommands.TipEntryFailed")))
@interface OnoTerminalCommandsTipEntryFailed : OnoCommand
- (instancetype)initWithErrorMessage:(NSString *)errorMessage __attribute__((swift_name("init(errorMessage:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoTerminalCommandsTipEntryFailed *)doCopyErrorMessage:(NSString *)errorMessage __attribute__((swift_name("doCopy(errorMessage:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *errorMessage __attribute__((swift_name("errorMessage")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalCommands.UpdateFinished")))
@interface OnoTerminalCommandsUpdateFinished : OnoCommand
- (instancetype)initWithAvailableUpdate:(OnoAvailableUpdate *)availableUpdate wasSuccessful:(BOOL)wasSuccessful errorMessage:(NSString * _Nullable)errorMessage __attribute__((swift_name("init(availableUpdate:wasSuccessful:errorMessage:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoAvailableUpdate *)component1 __attribute__((swift_name("component1()")));
- (BOOL)component2 __attribute__((swift_name("component2()")));
- (NSString * _Nullable)component3 __attribute__((swift_name("component3()")));
- (OnoTerminalCommandsUpdateFinished *)doCopyAvailableUpdate:(OnoAvailableUpdate *)availableUpdate wasSuccessful:(BOOL)wasSuccessful errorMessage:(NSString * _Nullable)errorMessage __attribute__((swift_name("doCopy(availableUpdate:wasSuccessful:errorMessage:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoAvailableUpdate *availableUpdate __attribute__((swift_name("availableUpdate")));
@property (readonly) NSString * _Nullable errorMessage __attribute__((swift_name("errorMessage")));
@property (readonly) BOOL wasSuccessful __attribute__((swift_name("wasSuccessful")));
@end;

__attribute__((swift_name("TerminalListener")))
@protocol OnoTerminalListener
@required
- (void)currentTerminalUpdatedTerminal:(OnoTerminal * _Nullable)terminal __attribute__((swift_name("currentTerminalUpdated(terminal:)")));
- (void)foundTerminalsUpdatedFoundTerminals:(NSDictionary<NSString *, OnoTerminal *> *)foundTerminals __attribute__((swift_name("foundTerminalsUpdated(foundTerminals:)")));
- (void)pairingFailedErrorMessage:(NSString *)errorMessage __attribute__((swift_name("pairingFailed(errorMessage:)")));
- (void)startingConnectionErrorOccurredErrorMessage:(NSString *)errorMessage __attribute__((swift_name("startingConnectionErrorOccurred(errorMessage:)")));
- (void)terminalConnectedTerminal:(OnoTerminal *)terminal __attribute__((swift_name("terminalConnected(terminal:)")));
- (void)terminalDisconnectedSerialNumber:(NSString * _Nullable)serialNumber __attribute__((swift_name("terminalDisconnected(serialNumber:)")));
- (void)terminalWithAddressNotFoundAddress:(NSString *)address __attribute__((swift_name("terminalWithAddressNotFound(address:)")));
- (void)tipEnteredTipAmountInCents:(int32_t)tipAmountInCents __attribute__((swift_name("tipEntered(tipAmountInCents:)")));
- (void)tipEntryFailedErrorMessage:(NSString *)errorMessage __attribute__((swift_name("tipEntryFailed(errorMessage:)")));
- (void)updateConnectingToAnyAvailableTerminalConnecting:(BOOL)connecting __attribute__((swift_name("updateConnectingToAnyAvailableTerminal(connecting:)")));
- (void)updateFinishedAvailableUpdate:(OnoAvailableUpdate *)availableUpdate wasSuccessful:(BOOL)wasSuccessful errorMessage:(NSString * _Nullable)errorMessage __attribute__((swift_name("updateFinished(availableUpdate:wasSuccessful:errorMessage:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalListenerHandler")))
@interface OnoTerminalListenerHandler : OnoBase <OnoTerminalListener>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)currentTerminalUpdatedTerminal:(OnoTerminal * _Nullable)terminal __attribute__((swift_name("currentTerminalUpdated(terminal:)")));
- (void)foundTerminalsUpdatedFoundTerminals:(NSDictionary<NSString *, OnoTerminal *> *)foundTerminals __attribute__((swift_name("foundTerminalsUpdated(foundTerminals:)")));
- (void)pairingFailedErrorMessage:(NSString *)errorMessage __attribute__((swift_name("pairingFailed(errorMessage:)")));
- (void)startingConnectionErrorOccurredErrorMessage:(NSString *)errorMessage __attribute__((swift_name("startingConnectionErrorOccurred(errorMessage:)")));
- (void)terminalConnectedTerminal:(OnoTerminal *)terminal __attribute__((swift_name("terminalConnected(terminal:)")));
- (void)terminalDisconnectedSerialNumber:(NSString * _Nullable)serialNumber __attribute__((swift_name("terminalDisconnected(serialNumber:)")));
- (void)terminalWithAddressNotFoundAddress:(NSString *)address __attribute__((swift_name("terminalWithAddressNotFound(address:)")));
- (void)tipEnteredTipAmountInCents:(int32_t)tipAmountInCents __attribute__((swift_name("tipEntered(tipAmountInCents:)")));
- (void)tipEntryFailedErrorMessage:(NSString *)errorMessage __attribute__((swift_name("tipEntryFailed(errorMessage:)")));
- (void)updateConnectingToAnyAvailableTerminalConnecting:(BOOL)connecting __attribute__((swift_name("updateConnectingToAnyAvailableTerminal(connecting:)")));
- (void)updateFinishedAvailableUpdate:(OnoAvailableUpdate *)availableUpdate wasSuccessful:(BOOL)wasSuccessful errorMessage:(NSString * _Nullable)errorMessage __attribute__((swift_name("updateFinished(availableUpdate:wasSuccessful:errorMessage:)")));
@property id<OnoTerminalListener> _Nullable listener __attribute__((swift_name("listener")));
@property (readonly) NSArray<OnoStoreListener *> *storeListeners __attribute__((swift_name("storeListeners")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalStateData")))
@interface OnoTerminalStateData : OnoBase <OnoStoreData>
- (instancetype)initWithAvailableTerminals:(NSDictionary<NSString *, OnoTerminal *> *)availableTerminals currentTerminal:(OnoTerminal * _Nullable)currentTerminal connectingAnyAvailableTerminal:(BOOL)connectingAnyAvailableTerminal connectionError:(NSString * _Nullable)connectionError shouldTransactWithRequest:(OnoTransactionInitializationRequest * _Nullable)shouldTransactWithRequest connectNextAvailableTerminal:(BOOL)connectNextAvailableTerminal __attribute__((swift_name("init(availableTerminals:currentTerminal:connectingAnyAvailableTerminal:connectionError:shouldTransactWithRequest:connectNextAvailableTerminal:)"))) __attribute__((objc_designated_initializer));
- (NSDictionary<NSString *, OnoTerminal *> *)component1 __attribute__((swift_name("component1()")));
- (OnoTerminal * _Nullable)component2 __attribute__((swift_name("component2()")));
- (BOOL)component3 __attribute__((swift_name("component3()")));
- (NSString * _Nullable)component4 __attribute__((swift_name("component4()")));
- (OnoTransactionInitializationRequest * _Nullable)component5 __attribute__((swift_name("component5()")));
- (BOOL)component6 __attribute__((swift_name("component6()")));
- (OnoTerminalStateData *)doCopyAvailableTerminals:(NSDictionary<NSString *, OnoTerminal *> *)availableTerminals currentTerminal:(OnoTerminal * _Nullable)currentTerminal connectingAnyAvailableTerminal:(BOOL)connectingAnyAvailableTerminal connectionError:(NSString * _Nullable)connectionError shouldTransactWithRequest:(OnoTransactionInitializationRequest * _Nullable)shouldTransactWithRequest connectNextAvailableTerminal:(BOOL)connectNextAvailableTerminal __attribute__((swift_name("doCopy(availableTerminals:currentTerminal:connectingAnyAvailableTerminal:connectionError:shouldTransactWithRequest:connectNextAvailableTerminal:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSDictionary<NSString *, OnoTerminal *> *availableTerminals __attribute__((swift_name("availableTerminals")));
@property (readonly) BOOL connectNextAvailableTerminal __attribute__((swift_name("connectNextAvailableTerminal")));
@property (readonly) BOOL connectingAnyAvailableTerminal __attribute__((swift_name("connectingAnyAvailableTerminal")));
@property (readonly) NSString * _Nullable connectionError __attribute__((swift_name("connectionError")));
@property (readonly) OnoTerminal * _Nullable currentTerminal __attribute__((swift_name("currentTerminal")));
@property (readonly) OnoTransactionInitializationRequest * _Nullable shouldTransactWithRequest __attribute__((swift_name("shouldTransactWithRequest")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalStateMachine")))
@interface OnoTerminalStateMachine : OnoBase <OnoStateMachine>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)terminalStateMachine __attribute__((swift_name("init()")));
- (OnoTerminalStateData *)castStoreDataStoreData:(id<OnoStoreData>)storeData __attribute__((swift_name("castStoreData(storeData:)")));
- (OnoStateMachineResult *)consumeActionStoreData:(id<OnoStoreData>)storeData action:(OnoAction *)action __attribute__((swift_name("consumeAction(storeData:action:)")));
- (NSDictionary<NSString *, OnoTerminal *> *)getAvailableTerminalsState:(NSDictionary<NSString *, id<OnoStoreData>> *)state __attribute__((swift_name("getAvailableTerminals(state:)")));
- (OnoTerminalStateData *)getCurrentStateStore:(OnoStore *)store __attribute__((swift_name("getCurrentState(store:)")));
- (OnoTerminalStateData *)getCurrentStateState:(NSDictionary<NSString *, id<OnoStoreData>> *)state __attribute__((swift_name("getCurrentState(state:)")));
- (OnoTerminal * _Nullable)getCurrentTerminalState:(NSDictionary<NSString *, id<OnoStoreData>> *)state __attribute__((swift_name("getCurrentTerminal(state:)")));
- (OnoTerminalStateData *)getStoreDataState:(NSDictionary<NSString *, id<OnoStoreData>> *)state __attribute__((swift_name("getStoreData(state:)")));
@property (readonly) NSString *TAG __attribute__((swift_name("TAG")));
@property (readonly) OnoTerminalStateData *initialState __attribute__((swift_name("initialState")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AvailableUpdate")))
@interface OnoAvailableUpdate : OnoBase
- (instancetype)initWithFiles:(NSArray<OnoUpdateFile *> *)files installation:(NSString *)installation message:(NSString * _Nullable)message updateIndex:(OnoInt * _Nullable)updateIndex totalUpdates:(OnoInt * _Nullable)totalUpdates __attribute__((swift_name("init(files:installation:message:updateIndex:totalUpdates:)"))) __attribute__((objc_designated_initializer));
- (NSArray<OnoUpdateFile *> *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString * _Nullable)component3 __attribute__((swift_name("component3()")));
- (OnoInt * _Nullable)component4 __attribute__((swift_name("component4()")));
- (OnoInt * _Nullable)component5 __attribute__((swift_name("component5()")));
- (OnoAvailableUpdate *)doCopyFiles:(NSArray<OnoUpdateFile *> *)files installation:(NSString *)installation message:(NSString * _Nullable)message updateIndex:(OnoInt * _Nullable)updateIndex totalUpdates:(OnoInt * _Nullable)totalUpdates __attribute__((swift_name("doCopy(files:installation:message:updateIndex:totalUpdates:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (OnoUpdateInstallationType * _Nullable)installationType __attribute__((swift_name("installationType()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<OnoUpdateFile *> *files __attribute__((swift_name("files")));
@property (readonly) NSString *installation __attribute__((swift_name("installation")));
@property (readonly) NSString * _Nullable message __attribute__((swift_name("message")));
@property (readonly) OnoInt * _Nullable totalUpdates __attribute__((swift_name("totalUpdates")));
@property (readonly) OnoInt * _Nullable updateIndex __attribute__((swift_name("updateIndex")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Terminal")))
@interface OnoTerminal : OnoBase
- (instancetype)initWithAddress:(NSString *)address serialNumber:(NSString *)serialNumber manufacturer:(OnoManufacturer *)manufacturer model:(OnoTerminalModel *)model state:(OnoTerminalState *)state connectionType:(OnoConnectionType *)connectionType connectionError:(NSString * _Nullable)connectionError statusMessage:(NSString * _Nullable)statusMessage terminalInfo:(OnoTerminalInfo * _Nullable)terminalInfo isPaired:(BOOL)isPaired connectionAttempted:(BOOL)connectionAttempted availableUpdates:(NSArray<OnoAvailableUpdate *> * _Nullable)availableUpdates currentlyApplyingUpdate:(OnoAvailableUpdate * _Nullable)currentlyApplyingUpdate __attribute__((swift_name("init(address:serialNumber:manufacturer:model:state:connectionType:connectionError:statusMessage:terminalInfo:isPaired:connectionAttempted:availableUpdates:currentlyApplyingUpdate:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (BOOL)component10 __attribute__((swift_name("component10()")));
- (BOOL)component11 __attribute__((swift_name("component11()")));
- (NSArray<OnoAvailableUpdate *> * _Nullable)component12 __attribute__((swift_name("component12()")));
- (OnoAvailableUpdate * _Nullable)component13 __attribute__((swift_name("component13()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (OnoManufacturer *)component3 __attribute__((swift_name("component3()")));
- (OnoTerminalModel *)component4 __attribute__((swift_name("component4()")));
- (OnoTerminalState *)component5 __attribute__((swift_name("component5()")));
- (OnoConnectionType *)component6 __attribute__((swift_name("component6()")));
- (NSString * _Nullable)component7 __attribute__((swift_name("component7()")));
- (NSString * _Nullable)component8 __attribute__((swift_name("component8()")));
- (OnoTerminalInfo * _Nullable)component9 __attribute__((swift_name("component9()")));
- (OnoTerminal *)doCopyAddress:(NSString *)address serialNumber:(NSString *)serialNumber manufacturer:(OnoManufacturer *)manufacturer model:(OnoTerminalModel *)model state:(OnoTerminalState *)state connectionType:(OnoConnectionType *)connectionType connectionError:(NSString * _Nullable)connectionError statusMessage:(NSString * _Nullable)statusMessage terminalInfo:(OnoTerminalInfo * _Nullable)terminalInfo isPaired:(BOOL)isPaired connectionAttempted:(BOOL)connectionAttempted availableUpdates:(NSArray<OnoAvailableUpdate *> * _Nullable)availableUpdates currentlyApplyingUpdate:(OnoAvailableUpdate * _Nullable)currentlyApplyingUpdate __attribute__((swift_name("doCopy(address:serialNumber:manufacturer:model:state:connectionType:connectionError:statusMessage:terminalInfo:isPaired:connectionAttempted:availableUpdates:currentlyApplyingUpdate:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSDictionary<NSString *, id> *)getTerminalDebugInfo __attribute__((swift_name("getTerminalDebugInfo()")));
- (NSDictionary<NSString *, id> *)getTerminalInfo __attribute__((swift_name("getTerminalInfo()")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (BOOL)isBusy __attribute__((swift_name("isBusy()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *address __attribute__((swift_name("address")));
@property (readonly) NSArray<OnoAvailableUpdate *> * _Nullable availableUpdates __attribute__((swift_name("availableUpdates")));
@property (readonly) BOOL connectionAttempted __attribute__((swift_name("connectionAttempted")));
@property (readonly) NSString * _Nullable connectionError __attribute__((swift_name("connectionError")));
@property (readonly) OnoConnectionType *connectionType __attribute__((swift_name("connectionType")));
@property (readonly) OnoAvailableUpdate * _Nullable currentlyApplyingUpdate __attribute__((swift_name("currentlyApplyingUpdate")));
@property (readonly) BOOL isPaired __attribute__((swift_name("isPaired")));
@property (readonly) OnoManufacturer *manufacturer __attribute__((swift_name("manufacturer")));
@property (readonly) OnoTerminalModel *model __attribute__((swift_name("model")));
@property (readonly) NSString *serialNumber __attribute__((swift_name("serialNumber")));
@property (readonly) OnoTerminalState *state __attribute__((swift_name("state")));
@property (readonly) NSString * _Nullable statusMessage __attribute__((swift_name("statusMessage")));
@property (readonly) OnoTerminalInfo * _Nullable terminalInfo __attribute__((swift_name("terminalInfo")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalConfig")))
@interface OnoTerminalConfig : OnoBase
- (instancetype)initWithTransactionTimeoutInSeconds:(int32_t)transactionTimeoutInSeconds enableSound:(BOOL)enableSound checkChipOnNFC:(BOOL)checkChipOnNFC statusCheckDelay:(int64_t)statusCheckDelay stayConnectedLength:(int64_t)stayConnectedLength shutdownTimeInSeconds:(int32_t)shutdownTimeInSeconds connectionAttempts:(int32_t)connectionAttempts fallbackOn05:(BOOL)fallbackOn05 keyIndex:(int32_t)keyIndex nfcLimitInCents:(OnoLong * _Nullable)nfcLimitInCents customConfiguration:(NSDictionary<NSString *, id> * _Nullable)customConfiguration __attribute__((swift_name("init(transactionTimeoutInSeconds:enableSound:checkChipOnNFC:statusCheckDelay:stayConnectedLength:shutdownTimeInSeconds:connectionAttempts:fallbackOn05:keyIndex:nfcLimitInCents:customConfiguration:)"))) __attribute__((objc_designated_initializer));
- (int32_t)component1 __attribute__((swift_name("component1()")));
- (OnoLong * _Nullable)component10 __attribute__((swift_name("component10()")));
- (NSDictionary<NSString *, id> * _Nullable)component11 __attribute__((swift_name("component11()")));
- (BOOL)component2 __attribute__((swift_name("component2()")));
- (BOOL)component3 __attribute__((swift_name("component3()")));
- (int64_t)component4 __attribute__((swift_name("component4()")));
- (int64_t)component5 __attribute__((swift_name("component5()")));
- (int32_t)component6 __attribute__((swift_name("component6()")));
- (int32_t)component7 __attribute__((swift_name("component7()")));
- (BOOL)component8 __attribute__((swift_name("component8()")));
- (int32_t)component9 __attribute__((swift_name("component9()")));
- (OnoTerminalConfig *)doCopyTransactionTimeoutInSeconds:(int32_t)transactionTimeoutInSeconds enableSound:(BOOL)enableSound checkChipOnNFC:(BOOL)checkChipOnNFC statusCheckDelay:(int64_t)statusCheckDelay stayConnectedLength:(int64_t)stayConnectedLength shutdownTimeInSeconds:(int32_t)shutdownTimeInSeconds connectionAttempts:(int32_t)connectionAttempts fallbackOn05:(BOOL)fallbackOn05 keyIndex:(int32_t)keyIndex nfcLimitInCents:(OnoLong * _Nullable)nfcLimitInCents customConfiguration:(NSDictionary<NSString *, id> * _Nullable)customConfiguration __attribute__((swift_name("doCopy(transactionTimeoutInSeconds:enableSound:checkChipOnNFC:statusCheckDelay:stayConnectedLength:shutdownTimeInSeconds:connectionAttempts:fallbackOn05:keyIndex:nfcLimitInCents:customConfiguration:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSDictionary<NSString *, id> *)toMap __attribute__((swift_name("toMap()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL checkChipOnNFC __attribute__((swift_name("checkChipOnNFC")));
@property (readonly) int32_t connectionAttempts __attribute__((swift_name("connectionAttempts")));
@property (readonly) NSDictionary<NSString *, id> * _Nullable customConfiguration __attribute__((swift_name("customConfiguration")));
@property (readonly) BOOL enableSound __attribute__((swift_name("enableSound")));
@property (readonly) BOOL fallbackOn05 __attribute__((swift_name("fallbackOn05")));
@property (readonly) int32_t keyIndex __attribute__((swift_name("keyIndex")));
@property (readonly) OnoLong * _Nullable nfcLimitInCents __attribute__((swift_name("nfcLimitInCents")));
@property (readonly) int32_t shutdownTimeInSeconds __attribute__((swift_name("shutdownTimeInSeconds")));
@property (readonly) int64_t statusCheckDelay __attribute__((swift_name("statusCheckDelay")));
@property (readonly) int64_t stayConnectedLength __attribute__((swift_name("stayConnectedLength")));
@property (readonly) int32_t transactionTimeoutInSeconds __attribute__((swift_name("transactionTimeoutInSeconds")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalInfo")))
@interface OnoTerminalInfo : OnoBase
- (instancetype)initWithModel:(NSString * _Nullable)model firmwareVersion:(NSString * _Nullable)firmwareVersion hardwareVersion:(NSString * _Nullable)hardwareVersion isCharging:(OnoBoolean * _Nullable)isCharging batteryPercentage:(OnoInt * _Nullable)batteryPercentage sdkVersion:(NSString * _Nullable)sdkVersion bootloaderVersion:(NSString * _Nullable)bootloaderVersion emvConfigVersion:(NSString * _Nullable)emvConfigVersion versions:(NSDictionary<NSString *, id> * _Nullable)versions additionalInfo:(NSDictionary<NSString *, id> * _Nullable)additionalInfo forceUpgradeCheck:(BOOL)forceUpgradeCheck __attribute__((swift_name("init(model:firmwareVersion:hardwareVersion:isCharging:batteryPercentage:sdkVersion:bootloaderVersion:emvConfigVersion:versions:additionalInfo:forceUpgradeCheck:)"))) __attribute__((objc_designated_initializer));
- (NSString * _Nullable)component1 __attribute__((swift_name("component1()")));
- (NSDictionary<NSString *, id> * _Nullable)component10 __attribute__((swift_name("component10()")));
- (BOOL)component11 __attribute__((swift_name("component11()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (NSString * _Nullable)component3 __attribute__((swift_name("component3()")));
- (OnoBoolean * _Nullable)component4 __attribute__((swift_name("component4()")));
- (OnoInt * _Nullable)component5 __attribute__((swift_name("component5()")));
- (NSString * _Nullable)component6 __attribute__((swift_name("component6()")));
- (NSString * _Nullable)component7 __attribute__((swift_name("component7()")));
- (NSString * _Nullable)component8 __attribute__((swift_name("component8()")));
- (NSDictionary<NSString *, id> * _Nullable)component9 __attribute__((swift_name("component9()")));
- (OnoTerminalInfo *)doCopyModel:(NSString * _Nullable)model firmwareVersion:(NSString * _Nullable)firmwareVersion hardwareVersion:(NSString * _Nullable)hardwareVersion isCharging:(OnoBoolean * _Nullable)isCharging batteryPercentage:(OnoInt * _Nullable)batteryPercentage sdkVersion:(NSString * _Nullable)sdkVersion bootloaderVersion:(NSString * _Nullable)bootloaderVersion emvConfigVersion:(NSString * _Nullable)emvConfigVersion versions:(NSDictionary<NSString *, id> * _Nullable)versions additionalInfo:(NSDictionary<NSString *, id> * _Nullable)additionalInfo forceUpgradeCheck:(BOOL)forceUpgradeCheck __attribute__((swift_name("doCopy(model:firmwareVersion:hardwareVersion:isCharging:batteryPercentage:sdkVersion:bootloaderVersion:emvConfigVersion:versions:additionalInfo:forceUpgradeCheck:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSDictionary<NSString *, id> * _Nullable additionalInfo __attribute__((swift_name("additionalInfo")));
@property (readonly) OnoInt * _Nullable batteryPercentage __attribute__((swift_name("batteryPercentage")));
@property (readonly) NSString * _Nullable bootloaderVersion __attribute__((swift_name("bootloaderVersion")));
@property (readonly) NSString * _Nullable emvConfigVersion __attribute__((swift_name("emvConfigVersion")));
@property (readonly) NSString * _Nullable firmwareVersion __attribute__((swift_name("firmwareVersion")));
@property (readonly) BOOL forceUpgradeCheck __attribute__((swift_name("forceUpgradeCheck")));
@property (readonly) NSString * _Nullable hardwareVersion __attribute__((swift_name("hardwareVersion")));
@property (readonly) OnoBoolean * _Nullable isCharging __attribute__((swift_name("isCharging")));
@property (readonly) NSString * _Nullable model __attribute__((swift_name("model")));
@property (readonly) NSString * _Nullable sdkVersion __attribute__((swift_name("sdkVersion")));
@property (readonly) NSDictionary<NSString *, id> * _Nullable versions __attribute__((swift_name("versions")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionInitializationRequest")))
@interface OnoTransactionInitializationRequest : OnoBase
- (instancetype)initWithCurrency:(NSString *)currency amount:(int64_t)amount clientTransactionUUID:(NSString *)clientTransactionUUID initiatedBy:(OnoTransactionInitiator *)initiatedBy tipData:(OnoTransactionInitializationRequestTipData *)tipData __attribute__((swift_name("init(currency:amount:clientTransactionUUID:initiatedBy:tipData:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (int64_t)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (OnoTransactionInitiator *)component4 __attribute__((swift_name("component4()")));
- (OnoTransactionInitializationRequestTipData *)component5 __attribute__((swift_name("component5()")));
- (OnoTransactionInitializationRequest *)doCopyCurrency:(NSString *)currency amount:(int64_t)amount clientTransactionUUID:(NSString *)clientTransactionUUID initiatedBy:(OnoTransactionInitiator *)initiatedBy tipData:(OnoTransactionInitializationRequestTipData *)tipData __attribute__((swift_name("doCopy(currency:amount:clientTransactionUUID:initiatedBy:tipData:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int64_t amount __attribute__((swift_name("amount")));
@property (readonly) NSString *clientTransactionUUID __attribute__((swift_name("clientTransactionUUID")));
@property (readonly) NSString *currency __attribute__((swift_name("currency")));
@property (readonly) OnoTransactionInitiator *initiatedBy __attribute__((swift_name("initiatedBy")));
@property (readonly) OnoTransactionInitializationRequestTipData *tipData __attribute__((swift_name("tipData")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionInitializationRequest.TipData")))
@interface OnoTransactionInitializationRequestTipData : OnoBase
- (instancetype)initWithAskForTipFirst:(BOOL)askForTipFirst enteredTipAmount:(OnoInt * _Nullable)enteredTipAmount isAbortTipping:(BOOL)isAbortTipping __attribute__((swift_name("init(askForTipFirst:enteredTipAmount:isAbortTipping:)"))) __attribute__((objc_designated_initializer));
- (BOOL)component1 __attribute__((swift_name("component1()")));
- (OnoInt * _Nullable)component2 __attribute__((swift_name("component2()")));
- (BOOL)component3 __attribute__((swift_name("component3()")));
- (OnoTransactionInitializationRequestTipData *)doCopyAskForTipFirst:(BOOL)askForTipFirst enteredTipAmount:(OnoInt * _Nullable)enteredTipAmount isAbortTipping:(BOOL)isAbortTipping __attribute__((swift_name("doCopy(askForTipFirst:enteredTipAmount:isAbortTipping:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL askForTipFirst __attribute__((swift_name("askForTipFirst")));
@property (readonly) OnoInt * _Nullable enteredTipAmount __attribute__((swift_name("enteredTipAmount")));
@property (readonly) BOOL isAbortTipping __attribute__((swift_name("isAbortTipping")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UpdateFile")))
@interface OnoUpdateFile : OnoBase
- (instancetype)initWithUrl:(NSString *)url md5sum:(NSString *)md5sum size:(int32_t)size type:(NSString *)type version:(NSString *)version __attribute__((swift_name("init(url:md5sum:size:type:version:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (int32_t)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (NSString *)component5 __attribute__((swift_name("component5()")));
- (OnoUpdateFile *)doCopyUrl:(NSString *)url md5sum:(NSString *)md5sum size:(int32_t)size type:(NSString *)type version:(NSString *)version __attribute__((swift_name("doCopy(url:md5sum:size:type:version:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *md5sum __attribute__((swift_name("md5sum")));
@property (readonly) int32_t size __attribute__((swift_name("size")));
@property (readonly) NSString *type __attribute__((swift_name("type")));
@property (readonly) NSString *url __attribute__((swift_name("url")));
@property (readonly) NSString *version __attribute__((swift_name("version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConnectionType")))
@interface OnoConnectionType : OnoKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) OnoConnectionType *bluetooth __attribute__((swift_name("bluetooth")));
- (int32_t)compareToOther:(OnoConnectionType *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Manufacturer")))
@interface OnoManufacturer : OnoKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) OnoManufacturer *dspread __attribute__((swift_name("dspread")));
@property (class, readonly) OnoManufacturer *miura __attribute__((swift_name("miura")));
@property (class, readonly) OnoManufacturer *pax __attribute__((swift_name("pax")));
- (int32_t)compareToOther:(OnoManufacturer *)other __attribute__((swift_name("compareTo(other:)")));
@property (readonly) BOOL requirePairing __attribute__((swift_name("requirePairing")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Manufacturer.Companion")))
@interface OnoManufacturerCompanion : OnoBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (OnoManufacturer * _Nullable)valueOrNullManufacturer:(NSString *)manufacturer __attribute__((swift_name("valueOrNull(manufacturer:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("RkiMessages")))
@interface OnoRkiMessages : OnoBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)rkiMessages __attribute__((swift_name("init()")));
- (NSString *)CONNECTED_DIFFERENT_TERMINALExpectedTerminal:(OnoTerminal *)expectedTerminal actualTerminal:(OnoTerminal * _Nullable)actualTerminal __attribute__((swift_name("CONNECTED_DIFFERENT_TERMINAL(expectedTerminal:actualTerminal:)")));
- (NSString *)KEY_INJECTION_FAILEDReason:(NSString *)reason __attribute__((swift_name("KEY_INJECTION_FAILED(reason:)")));
- (NSString *)TERMINAL_IS_BUSYBusyTerminal:(OnoTerminal *)busyTerminal __attribute__((swift_name("TERMINAL_IS_BUSY(busyTerminal:)")));
@property (readonly) NSString *INJECTING_KEYS __attribute__((swift_name("INJECTING_KEYS")));
@property (readonly) NSString *KEY_INJECTION_SUCCESSFUL __attribute__((swift_name("KEY_INJECTION_SUCCESSFUL")));
@property (readonly) NSString *RKI_DISABLED_OR_UNSUPPORTED __attribute__((swift_name("RKI_DISABLED_OR_UNSUPPORTED")));
@property (readonly) NSString *RKI_INITIALIZING __attribute__((swift_name("RKI_INITIALIZING")));
@property (readonly) NSString *RKI_SERVER_REQUEST __attribute__((swift_name("RKI_SERVER_REQUEST")));
@property (readonly) NSString *TERMINAL_DISCONNECTED __attribute__((swift_name("TERMINAL_DISCONNECTED")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalModel")))
@interface OnoTerminalModel : OnoKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) OnoTerminalModel *qposMini __attribute__((swift_name("qposMini")));
@property (class, readonly) OnoTerminalModel *qpos __attribute__((swift_name("qpos")));
@property (class, readonly) OnoTerminalModel *m010 __attribute__((swift_name("m010")));
@property (class, readonly) OnoTerminalModel *shuttle __attribute__((swift_name("shuttle")));
@property (class, readonly) OnoTerminalModel *unknown __attribute__((swift_name("unknown")));
- (int32_t)compareToOther:(OnoTerminalModel *)other __attribute__((swift_name("compareTo(other:)")));
@property (readonly) BOOL rkiDisabled __attribute__((swift_name("rkiDisabled")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalModel.Companion")))
@interface OnoTerminalModelCompanion : OnoBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (NSString *)describeModel:(OnoTerminalModel *)model __attribute__((swift_name("describe(model:)")));
- (BOOL)requireArpcForNfcModel:(OnoTerminalModel *)model __attribute__((swift_name("requireArpcForNfc(model:)")));
- (OnoTerminalModel *)tryValueOfValueStr:(NSString *)valueStr __attribute__((swift_name("tryValueOf(valueStr:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalState")))
@interface OnoTerminalState : OnoKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) OnoTerminalState *connecting __attribute__((swift_name("connecting")));
@property (class, readonly) OnoTerminalState *connected __attribute__((swift_name("connected")));
@property (class, readonly) OnoTerminalState *updating __attribute__((swift_name("updating")));
@property (class, readonly) OnoTerminalState *disconnecting __attribute__((swift_name("disconnecting")));
@property (class, readonly) OnoTerminalState *disconnected __attribute__((swift_name("disconnected")));
@property (class, readonly) OnoTerminalState *unavailable __attribute__((swift_name("unavailable")));
@property (class, readonly) OnoTerminalState *unknown __attribute__((swift_name("unknown")));
@property (class, readonly) OnoTerminalState *busy __attribute__((swift_name("busy")));
@property (class, readonly) OnoTerminalState *busyTipEntry __attribute__((swift_name("busyTipEntry")));
@property (class, readonly) OnoTerminalState *pairing __attribute__((swift_name("pairing")));
@property (class, readonly) OnoTerminalState *paired __attribute__((swift_name("paired")));
- (int32_t)compareToOther:(OnoTerminalState *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalStatusMessage")))
@interface OnoTerminalStatusMessage : OnoBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)terminalStatusMessage __attribute__((swift_name("init()")));
@property (readonly) NSString *CONNECTED __attribute__((swift_name("CONNECTED")));
@property (readonly) NSString *DISCONNECTED __attribute__((swift_name("DISCONNECTED")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalStatusMessage.Busy")))
@interface OnoTerminalStatusMessageBusy : OnoBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)busy __attribute__((swift_name("init()")));
@property (readonly) NSString *PROCESSING_TRANSACTION __attribute__((swift_name("PROCESSING_TRANSACTION")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalStatusMessage.UpdateMessages")))
@interface OnoTerminalStatusMessageUpdateMessages : OnoBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)updateMessages __attribute__((swift_name("init()")));
- (NSString *)getUpdatingMessageMessage:(NSString *)message availableUpdate:(OnoAvailableUpdate *)availableUpdate __attribute__((swift_name("getUpdatingMessage(message:availableUpdate:)")));
@property (readonly) NSString *COPYING_UPDATES __attribute__((swift_name("COPYING_UPDATES")));
@property (readonly) NSString *DOWNLOADING_UPDATES __attribute__((swift_name("DOWNLOADING_UPDATES")));
@property (readonly) NSString *INSTALLING_UPDATES __attribute__((swift_name("INSTALLING_UPDATES")));
@property (readonly) NSString *PROCESSING_UPDATES __attribute__((swift_name("PROCESSING_UPDATES")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ActivityTimeline")))
@interface OnoActivityTimeline : OnoBase <OnoStoreData>
- (instancetype)initWithTasks:(NSArray<OnoActivityTask *> *)tasks events:(NSArray<OnoActivityEvent *> *)events __attribute__((swift_name("init(tasks:events:)"))) __attribute__((objc_designated_initializer));
- (NSArray<OnoActivityTask *> *)component1 __attribute__((swift_name("component1()")));
- (NSArray<OnoActivityEvent *> *)component2 __attribute__((swift_name("component2()")));
- (OnoActivityTimeline *)doCopyTasks:(NSArray<OnoActivityTask *> *)tasks events:(NSArray<OnoActivityEvent *> *)events __attribute__((swift_name("doCopy(tasks:events:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSDictionary<NSString *, id> *)getTimelineDebugInfo __attribute__((swift_name("getTimelineDebugInfo()")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSDictionary<NSString *, OnoDouble *> *)summarizedTiming __attribute__((swift_name("summarizedTiming()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<OnoActivityEvent *> *events __attribute__((swift_name("events")));
@property (readonly) NSArray<OnoActivityTask *> *tasks __attribute__((swift_name("tasks")));
@end;

__attribute__((swift_name("ActivityTimelineActions")))
@interface OnoActivityTimelineActions : OnoBase
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ActivityTimelineActions.ClearAllActivityTiming")))
@interface OnoActivityTimelineActionsClearAllActivityTiming : OnoAction
- (instancetype)initWithMessage:(NSString *)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoActivityTimelineActionsClearAllActivityTiming *)doCopyMessage:(NSString *)message __attribute__((swift_name("doCopy(message:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *message __attribute__((swift_name("message")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ActivityTimelineActions.EndAllRunningTasks")))
@interface OnoActivityTimelineActionsEndAllRunningTasks : OnoAction
- (instancetype)initWithErrorTimeMillis:(int64_t)errorTimeMillis __attribute__((swift_name("init(errorTimeMillis:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (int64_t)component1 __attribute__((swift_name("component1()")));
- (OnoActivityTimelineActionsEndAllRunningTasks *)doCopyErrorTimeMillis:(int64_t)errorTimeMillis __attribute__((swift_name("doCopy(errorTimeMillis:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int64_t errorTimeMillis __attribute__((swift_name("errorTimeMillis")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ActivityTimelineActions.EventOccurred")))
@interface OnoActivityTimelineActionsEventOccurred : OnoAction
- (instancetype)initWithEvent:(OnoActivityEvent *)event __attribute__((swift_name("init(event:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoActivityEvent *)component1 __attribute__((swift_name("component1()")));
- (OnoActivityTimelineActionsEventOccurred *)doCopyEvent:(OnoActivityEvent *)event __attribute__((swift_name("doCopy(event:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoActivityEvent *event __attribute__((swift_name("event")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ActivityTimelineActions.GetCompletedTransactionDebugData")))
@interface OnoActivityTimelineActionsGetCompletedTransactionDebugData : OnoAction
- (instancetype)initWithTransaction:(OnoATransaction *)transaction terminal:(OnoTerminal * _Nullable)terminal __attribute__((swift_name("init(transaction:terminal:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoATransaction *)component1 __attribute__((swift_name("component1()")));
- (OnoTerminal * _Nullable)component2 __attribute__((swift_name("component2()")));
- (OnoActivityTimelineActionsGetCompletedTransactionDebugData *)doCopyTransaction:(OnoATransaction *)transaction terminal:(OnoTerminal * _Nullable)terminal __attribute__((swift_name("doCopy(transaction:terminal:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoTerminal * _Nullable terminal __attribute__((swift_name("terminal")));
@property (readonly) OnoATransaction *transaction __attribute__((swift_name("transaction")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ActivityTimelineActions.TaskBegan")))
@interface OnoActivityTimelineActionsTaskBegan : OnoAction
- (instancetype)initWithTaskName:(OnoActivityTaskName *)taskName startTimeMillis:(int64_t)startTimeMillis __attribute__((swift_name("init(taskName:startTimeMillis:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoActivityTaskName *)component1 __attribute__((swift_name("component1()")));
- (int64_t)component2 __attribute__((swift_name("component2()")));
- (OnoActivityTimelineActionsTaskBegan *)doCopyTaskName:(OnoActivityTaskName *)taskName startTimeMillis:(int64_t)startTimeMillis __attribute__((swift_name("doCopy(taskName:startTimeMillis:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int64_t startTimeMillis __attribute__((swift_name("startTimeMillis")));
@property (readonly) OnoActivityTaskName *taskName __attribute__((swift_name("taskName")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ActivityTimelineActions.TaskEnded")))
@interface OnoActivityTimelineActionsTaskEnded : OnoAction
- (instancetype)initWithTaskName:(OnoActivityTaskName *)taskName finishTimeMillis:(int64_t)finishTimeMillis __attribute__((swift_name("init(taskName:finishTimeMillis:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoActivityTaskName *)component1 __attribute__((swift_name("component1()")));
- (int64_t)component2 __attribute__((swift_name("component2()")));
- (OnoActivityTimelineActionsTaskEnded *)doCopyTaskName:(OnoActivityTaskName *)taskName finishTimeMillis:(int64_t)finishTimeMillis __attribute__((swift_name("doCopy(taskName:finishTimeMillis:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int64_t finishTimeMillis __attribute__((swift_name("finishTimeMillis")));
@property (readonly) OnoActivityTaskName *taskName __attribute__((swift_name("taskName")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ActivityTimelineStateMachine")))
@interface OnoActivityTimelineStateMachine : OnoBase <OnoStateMachine>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)activityTimelineStateMachine __attribute__((swift_name("init()")));
- (OnoActivityTimeline *)castStoreDataStoreData:(id<OnoStoreData>)storeData __attribute__((swift_name("castStoreData(storeData:)")));
- (OnoStateMachineResult *)consumeActionStoreData:(id<OnoStoreData>)storeData action:(OnoAction *)action __attribute__((swift_name("consumeAction(storeData:action:)")));
- (OnoActivityTimeline *)getCurrentStateStore:(OnoStore *)store __attribute__((swift_name("getCurrentState(store:)")));
- (OnoActivityTimeline *)getCurrentStateState:(NSDictionary<NSString *, id<OnoStoreData>> *)state __attribute__((swift_name("getCurrentState(state:)")));
- (OnoActivityTimeline *)getStoreDataState:(NSDictionary<NSString *, id<OnoStoreData>> *)state __attribute__((swift_name("getStoreData(state:)")));
@property (readonly) OnoActivityTimeline *initialState __attribute__((swift_name("initialState")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ActivityEventColors")))
@interface OnoActivityEventColors : OnoBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)activityEventColors __attribute__((swift_name("init()")));
@property (readonly) NSString *GREEN __attribute__((swift_name("GREEN")));
@property (readonly) NSString *RED __attribute__((swift_name("RED")));
@property (readonly) NSString *YELLOW __attribute__((swift_name("YELLOW")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ActivityEventName")))
@interface OnoActivityEventName : OnoKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) OnoActivityEventName *qposErrorOccurred __attribute__((swift_name("qposErrorOccurred")));
@property (class, readonly) OnoActivityEventName *terminalDisconnected __attribute__((swift_name("terminalDisconnected")));
@property (class, readonly) OnoActivityEventName *terminalConnectionError __attribute__((swift_name("terminalConnectionError")));
@property (class, readonly) OnoActivityEventName *transactionInWrongState __attribute__((swift_name("transactionInWrongState")));
@property (class, readonly) OnoActivityEventName *onoSdkInitialized __attribute__((swift_name("onoSdkInitialized")));
@property (class, readonly) OnoActivityEventName *sdkAlreadyInstantiated __attribute__((swift_name("sdkAlreadyInstantiated")));
@property (class, readonly) OnoActivityEventName *terminalRkiFailed __attribute__((swift_name("terminalRkiFailed")));
@property (class, readonly) OnoActivityEventName *tipRequestFailed __attribute__((swift_name("tipRequestFailed")));
@property (class, readonly) OnoActivityEventName *terminalSettingsApplied __attribute__((swift_name("terminalSettingsApplied")));
@property (class, readonly) OnoActivityEventName *miuraIgnoredAction __attribute__((swift_name("miuraIgnoredAction")));
@property (class, readonly) OnoActivityEventName *continueTranAfterTipEntry __attribute__((swift_name("continueTranAfterTipEntry")));
- (int32_t)compareToOther:(OnoActivityEventName *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ActivityTaskName")))
@interface OnoActivityTaskName : OnoKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) OnoActivityTaskName *awaitingPermissions __attribute__((swift_name("awaitingPermissions")));
@property (class, readonly) OnoActivityTaskName *awaitingIdentification __attribute__((swift_name("awaitingIdentification")));
@property (class, readonly) OnoActivityTaskName *connectTerminal __attribute__((swift_name("connectTerminal")));
@property (class, readonly) OnoActivityTaskName *awaitingTerminalConnection __attribute__((swift_name("awaitingTerminalConnection")));
@property (class, readonly) OnoActivityTaskName *updateTerminal __attribute__((swift_name("updateTerminal")));
@property (class, readonly) OnoActivityTaskName *transaction __attribute__((swift_name("transaction")));
@property (class, readonly) OnoActivityTaskName *awaitingCard __attribute__((swift_name("awaitingCard")));
@property (class, readonly) OnoActivityTaskName *awaitingPin __attribute__((swift_name("awaitingPin")));
@property (class, readonly) OnoActivityTaskName *awaitingSignature __attribute__((swift_name("awaitingSignature")));
@property (class, readonly) OnoActivityTaskName *awaitingTip __attribute__((swift_name("awaitingTip")));
@property (class, readonly) OnoActivityTaskName *awaitingEmvAppSelection __attribute__((swift_name("awaitingEmvAppSelection")));
@property (class, readonly) OnoActivityTaskName *postingSignature __attribute__((swift_name("postingSignature")));
@property (class, readonly) OnoActivityTaskName *arpcHandling __attribute__((swift_name("arpcHandling")));
@property (class, readonly) OnoActivityTaskName *authorizing __attribute__((swift_name("authorizing")));
@property (class, readonly) OnoActivityTaskName *finalizing __attribute__((swift_name("finalizing")));
@property (class, readonly) OnoActivityTaskName *terminalRki __attribute__((swift_name("terminalRki")));
@property (class, readonly) OnoActivityTaskName *terminalRkiStartKeyInjection __attribute__((swift_name("terminalRkiStartKeyInjection")));
@property (class, readonly) OnoActivityTaskName *terminalRkiServerRequest __attribute__((swift_name("terminalRkiServerRequest")));
@property (class, readonly) OnoActivityTaskName *terminalRkiGetData __attribute__((swift_name("terminalRkiGetData")));
- (int32_t)compareToOther:(OnoActivityTaskName *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ActivityTaskName.Companion")))
@interface OnoActivityTaskNameCompanion : OnoBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (NSSet<OnoActivityTaskName *> *)processingActivityNames __attribute__((swift_name("processingActivityNames()")));
- (NSSet<OnoActivityTaskName *> *)userInputActivityNames __attribute__((swift_name("userInputActivityNames()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ActivityEvent")))
@interface OnoActivityEvent : OnoBase
- (instancetype)initWithTitle:(OnoActivityEventName *)title description:(NSString *)description color:(NSString *)color atMillis:(int64_t)atMillis __attribute__((swift_name("init(title:description:color:atMillis:)"))) __attribute__((objc_designated_initializer));
- (OnoActivityEventName *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (int64_t)component4 __attribute__((swift_name("component4()")));
- (OnoActivityEvent *)doCopyTitle:(OnoActivityEventName *)title description:(NSString *)description color:(NSString *)color atMillis:(int64_t)atMillis __attribute__((swift_name("doCopy(title:description:color:atMillis:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSDictionary<NSString *, id> *)toDebuggingDataMap __attribute__((swift_name("toDebuggingDataMap()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int64_t atMillis __attribute__((swift_name("atMillis")));
@property (readonly) NSString *color __attribute__((swift_name("color")));
@property (readonly, getter=description_) NSString *description __attribute__((swift_name("description")));
@property (readonly) OnoActivityEventName *title __attribute__((swift_name("title")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ActivityTask")))
@interface OnoActivityTask : OnoBase
- (instancetype)initWithTaskName:(OnoActivityTaskName *)taskName startMillis:(int64_t)startMillis endMillis:(OnoLong * _Nullable)endMillis __attribute__((swift_name("init(taskName:startMillis:endMillis:)"))) __attribute__((objc_designated_initializer));
- (OnoActivityTaskName *)component1 __attribute__((swift_name("component1()")));
- (int64_t)component2 __attribute__((swift_name("component2()")));
- (OnoLong * _Nullable)component3 __attribute__((swift_name("component3()")));
- (OnoActivityTask *)doCopyTaskName:(OnoActivityTaskName *)taskName startMillis:(int64_t)startMillis endMillis:(OnoLong * _Nullable)endMillis __attribute__((swift_name("doCopy(taskName:startMillis:endMillis:)")));
- (OnoLong * _Nullable)durationMillis __attribute__((swift_name("durationMillis()")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSDictionary<NSString *, id> *)toDebuggingDataMap __attribute__((swift_name("toDebuggingDataMap()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoLong * _Nullable endMillis __attribute__((swift_name("endMillis")));
@property (readonly) int64_t startMillis __attribute__((swift_name("startMillis")));
@property (readonly) OnoActivityTaskName *taskName __attribute__((swift_name("taskName")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Events")))
@interface OnoEvents : OnoBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)events __attribute__((swift_name("init()")));
@property (readonly) NSString *QPOS_HANDLER_EXECUTION_TIMEOUT __attribute__((swift_name("QPOS_HANDLER_EXECUTION_TIMEOUT")));
@property (readonly) NSString *QPOS_HANDLER_SERVICE_BUSY __attribute__((swift_name("QPOS_HANDLER_SERVICE_BUSY")));
@property (readonly) NSString *QPOS_HANDLER_START_TIMEOUT __attribute__((swift_name("QPOS_HANDLER_START_TIMEOUT")));
@property (readonly) NSString *QPOS_SERVICE_APP_SELECTION_TIMEOUT __attribute__((swift_name("QPOS_SERVICE_APP_SELECTION_TIMEOUT")));
@property (readonly) NSString *QPOS_SERVICE_DEVICE_BUSY __attribute__((swift_name("QPOS_SERVICE_DEVICE_BUSY")));
@property (readonly) NSString *QPOS_SERVICE_DEVICE_RESET __attribute__((swift_name("QPOS_SERVICE_DEVICE_RESET")));
@property (readonly) NSString *QPOS_SERVICE_TIMEOUT __attribute__((swift_name("QPOS_SERVICE_TIMEOUT")));
@property (readonly) NSString *QPOS_SERVICE_UNKWOWN __attribute__((swift_name("QPOS_SERVICE_UNKWOWN")));
@property (readonly) NSString *TERMINAL_CONNECTED __attribute__((swift_name("TERMINAL_CONNECTED")));
@property (readonly) NSString *TERMINAL_CONNECT_ANY_AVAILABLE_FAILED __attribute__((swift_name("TERMINAL_CONNECT_ANY_AVAILABLE_FAILED")));
@property (readonly) NSString *TERMINAL_CONNECT_FAILED __attribute__((swift_name("TERMINAL_CONNECT_FAILED")));
@property (readonly) NSString *TERMINAL_RKI_GET_DATA __attribute__((swift_name("TERMINAL_RKI_GET_DATA")));
@property (readonly) NSString *TERMINAL_RKI_INIT_FAILED __attribute__((swift_name("TERMINAL_RKI_INIT_FAILED")));
@property (readonly) NSString *TERMINAL_RKI_KEY_INJECTION_FAILED __attribute__((swift_name("TERMINAL_RKI_KEY_INJECTION_FAILED")));
@property (readonly) NSString *TERMINAL_RKI_KEY_INJECTION_SUCCESSFUL __attribute__((swift_name("TERMINAL_RKI_KEY_INJECTION_SUCCESSFUL")));
@property (readonly) NSString *TERMINAL_RKI_NOT_SUPPORTED __attribute__((swift_name("TERMINAL_RKI_NOT_SUPPORTED")));
@property (readonly) NSString *TERMINAL_RKI_SERVER_REQUEST __attribute__((swift_name("TERMINAL_RKI_SERVER_REQUEST")));
@property (readonly) NSString *TERMINAL_RKI_START_KEY_INJECTION __attribute__((swift_name("TERMINAL_RKI_START_KEY_INJECTION")));
@property (readonly) NSString *TERMINAL_UPDATE_FAILED __attribute__((swift_name("TERMINAL_UPDATE_FAILED")));
@property (readonly) NSString *TERMINAL_UPDATE_FINISHED __attribute__((swift_name("TERMINAL_UPDATE_FINISHED")));
@property (readonly) NSString *TRANSACTION_CHIP_FALLBACK __attribute__((swift_name("TRANSACTION_CHIP_FALLBACK")));
@property (readonly) NSString *TRANSACTION_FALLBACK __attribute__((swift_name("TRANSACTION_FALLBACK")));
@property (readonly) NSString *TRANSACTION_NFC_FALLBACK __attribute__((swift_name("TRANSACTION_NFC_FALLBACK")));
@end;

__attribute__((swift_name("ISegmentService")))
@protocol OnoISegmentService
@required
- (void)trackEventEventName:(NSString *)eventName data:(NSDictionary<NSString *, NSString *> *)data __attribute__((swift_name("trackEvent(eventName:data:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MockSegmentService")))
@interface OnoMockSegmentService : OnoBase <OnoISegmentService>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)trackEventEventName:(NSString *)eventName data:(NSDictionary<NSString *, NSString *> *)data __attribute__((swift_name("trackEvent(eventName:data:)")));
@property (readonly) NSDictionary<NSString *, NSString *> * _Nullable lastDataTracked __attribute__((swift_name("lastDataTracked")));
@property (readonly) NSString * _Nullable lastEventNameTracked __attribute__((swift_name("lastEventNameTracked")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SegmentCommandDriver")))
@interface OnoSegmentCommandDriver : OnoCommandDriver
- (instancetype)initWithSegmentService:(id<OnoISegmentService>)segmentService __attribute__((swift_name("init(segmentService:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (void)receiveCommandCommand:(OnoCommand *)command __attribute__((swift_name("receiveCommand(command:)")));
@property (readonly) id<OnoISegmentService> segmentService __attribute__((swift_name("segmentService")));
@end;

__attribute__((swift_name("SegmentCommands")))
@interface OnoSegmentCommands : OnoBase
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SegmentCommands.TrackEvent")))
@interface OnoSegmentCommandsTrackEvent : OnoCommand
- (instancetype)initWithEventName:(NSString *)eventName data:(NSDictionary<NSString *, NSString *> *)data __attribute__((swift_name("init(eventName:data:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSDictionary<NSString *, NSString *> *)component2 __attribute__((swift_name("component2()")));
- (OnoSegmentCommandsTrackEvent *)doCopyEventName:(NSString *)eventName data:(NSDictionary<NSString *, NSString *> *)data __attribute__((swift_name("doCopy(eventName:data:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSDictionary<NSString *, NSString *> *data __attribute__((swift_name("data")));
@property (readonly) NSString *eventName __attribute__((swift_name("eventName")));
@end;

__attribute__((swift_name("CoreActions")))
@interface OnoCoreActions : OnoBase
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CoreActions.AssociateThisDeviceWithBusiness")))
@interface OnoCoreActionsAssociateThisDeviceWithBusiness : OnoAction
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CoreActions.AuthenticationUpdated")))
@interface OnoCoreActionsAuthenticationUpdated : OnoAction
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CoreActions.ClearAuthentication")))
@interface OnoCoreActionsClearAuthentication : OnoAction
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CoreActions.DeviceAndBusinessAssociatedSuccessfully")))
@interface OnoCoreActionsDeviceAndBusinessAssociatedSuccessfully : OnoAction
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CoreActions.DeviceAndBusinessAssociationFailed")))
@interface OnoCoreActionsDeviceAndBusinessAssociationFailed : OnoAction
- (instancetype)initWithCoreErrorDetails:(OnoHttp *)coreErrorDetails __attribute__((swift_name("init(coreErrorDetails:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoHttp *)component1 __attribute__((swift_name("component1()")));
- (OnoCoreActionsDeviceAndBusinessAssociationFailed *)doCopyCoreErrorDetails:(OnoHttp *)coreErrorDetails __attribute__((swift_name("doCopy(coreErrorDetails:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoHttp *coreErrorDetails __attribute__((swift_name("coreErrorDetails")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CoreActions.FetchOnoAuthenticationToken")))
@interface OnoCoreActionsFetchOnoAuthenticationToken : OnoAction
- (instancetype)initWithOnoMerchantId:(NSString *)onoMerchantId __attribute__((swift_name("init(onoMerchantId:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoCoreActionsFetchOnoAuthenticationToken *)doCopyOnoMerchantId:(NSString *)onoMerchantId __attribute__((swift_name("doCopy(onoMerchantId:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *onoMerchantId __attribute__((swift_name("onoMerchantId")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CoreActions.NullApiInterface")))
@interface OnoCoreActionsNullApiInterface : OnoAction
- (instancetype)initWithUserClass:(id<OnoKotlinKClass>)userClass __attribute__((swift_name("init(userClass:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (id<OnoKotlinKClass>)component1 __attribute__((swift_name("component1()")));
- (OnoCoreActionsNullApiInterface *)doCopyUserClass:(id<OnoKotlinKClass>)userClass __attribute__((swift_name("doCopy(userClass:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) id<OnoKotlinKClass> userClass __attribute__((swift_name("userClass")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CoreActions.PushTransactionCompleteData")))
@interface OnoCoreActionsPushTransactionCompleteData : OnoAction
- (instancetype)initWithTransaction:(OnoATransaction *)transaction terminal:(OnoTerminal * _Nullable)terminal activityTimeline:(OnoActivityTimeline *)activityTimeline summarizedTiming:(NSDictionary<NSString *, OnoDouble *> *)summarizedTiming __attribute__((swift_name("init(transaction:terminal:activityTimeline:summarizedTiming:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoATransaction *)component1 __attribute__((swift_name("component1()")));
- (OnoTerminal * _Nullable)component2 __attribute__((swift_name("component2()")));
- (OnoActivityTimeline *)component3 __attribute__((swift_name("component3()")));
- (NSDictionary<NSString *, OnoDouble *> *)component4 __attribute__((swift_name("component4()")));
- (OnoCoreActionsPushTransactionCompleteData *)doCopyTransaction:(OnoATransaction *)transaction terminal:(OnoTerminal * _Nullable)terminal activityTimeline:(OnoActivityTimeline *)activityTimeline summarizedTiming:(NSDictionary<NSString *, OnoDouble *> *)summarizedTiming __attribute__((swift_name("doCopy(transaction:terminal:activityTimeline:summarizedTiming:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoActivityTimeline *activityTimeline __attribute__((swift_name("activityTimeline")));
@property (readonly) NSDictionary<NSString *, OnoDouble *> *summarizedTiming __attribute__((swift_name("summarizedTiming")));
@property (readonly) OnoTerminal * _Nullable terminal __attribute__((swift_name("terminal")));
@property (readonly) OnoATransaction *transaction __attribute__((swift_name("transaction")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CoreActions.RetryFetchOnoAuthToken")))
@interface OnoCoreActionsRetryFetchOnoAuthToken : OnoAction
- (instancetype)initWithOnoMerchantId:(NSString *)onoMerchantId errorDetails:(OnoErrorDetails *)errorDetails __attribute__((swift_name("init(onoMerchantId:errorDetails:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoErrorDetails *)component2 __attribute__((swift_name("component2()")));
- (OnoCoreActionsRetryFetchOnoAuthToken *)doCopyOnoMerchantId:(NSString *)onoMerchantId errorDetails:(OnoErrorDetails *)errorDetails __attribute__((swift_name("doCopy(onoMerchantId:errorDetails:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoErrorDetails *errorDetails __attribute__((swift_name("errorDetails")));
@property (readonly) NSString *onoMerchantId __attribute__((swift_name("onoMerchantId")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CoreActions.RkiSuccessReported")))
@interface OnoCoreActionsRkiSuccessReported : OnoAction
- (instancetype)initWithSerialNumber:(NSString *)serialNumber coreErrorDetails:(OnoHttp * _Nullable)coreErrorDetails __attribute__((swift_name("init(serialNumber:coreErrorDetails:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoHttp * _Nullable)component2 __attribute__((swift_name("component2()")));
- (OnoCoreActionsRkiSuccessReported *)doCopySerialNumber:(NSString *)serialNumber coreErrorDetails:(OnoHttp * _Nullable)coreErrorDetails __attribute__((swift_name("doCopy(serialNumber:coreErrorDetails:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoHttp * _Nullable coreErrorDetails __attribute__((swift_name("coreErrorDetails")));
@property (readonly) NSString *serialNumber __attribute__((swift_name("serialNumber")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CoreActions.SetAuthentication")))
@interface OnoCoreActionsSetAuthentication : OnoAction
- (instancetype)initWithToken:(OnoSensitiveString * _Nullable)token integrationAuthentication:(OnoIntegrationAuthentication * _Nullable)integrationAuthentication __attribute__((swift_name("init(token:integrationAuthentication:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoSensitiveString * _Nullable)component1 __attribute__((swift_name("component1()")));
- (OnoIntegrationAuthentication * _Nullable)component2 __attribute__((swift_name("component2()")));
- (OnoCoreActionsSetAuthentication *)doCopyToken:(OnoSensitiveString * _Nullable)token integrationAuthentication:(OnoIntegrationAuthentication * _Nullable)integrationAuthentication __attribute__((swift_name("doCopy(token:integrationAuthentication:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoIntegrationAuthentication * _Nullable integrationAuthentication __attribute__((swift_name("integrationAuthentication")));
@property (readonly) OnoSensitiveString * _Nullable token __attribute__((swift_name("token")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CoreActions.SetBaseCoreURL")))
@interface OnoCoreActionsSetBaseCoreURL : OnoAction
- (instancetype)initWithUrl:(NSString *)url __attribute__((swift_name("init(url:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoCoreActionsSetBaseCoreURL *)doCopyUrl:(NSString *)url __attribute__((swift_name("doCopy(url:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *url __attribute__((swift_name("url")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CoreActions.TerminalAndBusinessAssociatedSuccessfully")))
@interface OnoCoreActionsTerminalAndBusinessAssociatedSuccessfully : OnoAction
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CoreActions.TerminalAndDeviceAssociationFailed")))
@interface OnoCoreActionsTerminalAndDeviceAssociationFailed : OnoAction
- (instancetype)initWithCoreErrorDetails:(OnoHttp *)coreErrorDetails __attribute__((swift_name("init(coreErrorDetails:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoHttp *)component1 __attribute__((swift_name("component1()")));
- (OnoCoreActionsTerminalAndDeviceAssociationFailed *)doCopyCoreErrorDetails:(OnoHttp *)coreErrorDetails __attribute__((swift_name("doCopy(coreErrorDetails:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoHttp *coreErrorDetails __attribute__((swift_name("coreErrorDetails")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CoreCommandDriver")))
@interface OnoCoreCommandDriver : OnoCommandDriver
- (instancetype)initWithCoreApiService:(OnoCoreApiServiceProtocol *)coreApiService __attribute__((swift_name("init(coreApiService:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (void)receiveCommandCommand:(OnoCommand *)command __attribute__((swift_name("receiveCommand(command:)")));
@end;

__attribute__((swift_name("CoreCommands")))
@interface OnoCoreCommands : OnoBase
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CoreCommands.AssociateTerminalWithThisDevice")))
@interface OnoCoreCommandsAssociateTerminalWithThisDevice : OnoCommand
- (instancetype)initWithDeviceIdentifier:(NSString *)deviceIdentifier terminal:(OnoTerminal *)terminal __attribute__((swift_name("init(deviceIdentifier:terminal:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithDeviceIdentifierAnfTerminal:(NSDictionary<NSString *, NSString *> *)deviceIdentifierAnfTerminal __attribute__((swift_name("init(deviceIdentifierAnfTerminal:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSDictionary<NSString *, NSString *> *)component1 __attribute__((swift_name("component1()")));
- (OnoCoreCommandsAssociateTerminalWithThisDevice *)doCopyDeviceIdentifierAnfTerminal:(NSDictionary<NSString *, NSString *> *)deviceIdentifierAnfTerminal __attribute__((swift_name("doCopy(deviceIdentifierAnfTerminal:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSDictionary<NSString *, NSString *> *deviceIdentifierAnfTerminal __attribute__((swift_name("deviceIdentifierAnfTerminal")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CoreCommands.AssociateThisDeviceWithBusiness")))
@interface OnoCoreCommandsAssociateThisDeviceWithBusiness : OnoCommand
- (instancetype)initWithDeviceData:(NSDictionary<NSString *, NSString *> *)deviceData __attribute__((swift_name("init(deviceData:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSDictionary<NSString *, NSString *> *)component1 __attribute__((swift_name("component1()")));
- (OnoCoreCommandsAssociateThisDeviceWithBusiness *)doCopyDeviceData:(NSDictionary<NSString *, NSString *> *)deviceData __attribute__((swift_name("doCopy(deviceData:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSDictionary<NSString *, NSString *> *deviceData __attribute__((swift_name("deviceData")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CoreCommands.ClearAuthentication")))
@interface OnoCoreCommandsClearAuthentication : OnoCommand
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CoreCommands.ClearCachedTransactionToken")))
@interface OnoCoreCommandsClearCachedTransactionToken : OnoCommand
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CoreCommands.FetchOnoAuthenticationToken")))
@interface OnoCoreCommandsFetchOnoAuthenticationToken : OnoCommand
- (instancetype)initWithOnoMerchantId:(NSString *)onoMerchantId authenticationToken:(OnoSensitiveString * _Nullable)authenticationToken integrationAuthentication:(OnoIntegrationAuthentication * _Nullable)integrationAuthentication __attribute__((swift_name("init(onoMerchantId:authenticationToken:integrationAuthentication:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoSensitiveString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (OnoIntegrationAuthentication * _Nullable)component3 __attribute__((swift_name("component3()")));
- (OnoCoreCommandsFetchOnoAuthenticationToken *)doCopyOnoMerchantId:(NSString *)onoMerchantId authenticationToken:(OnoSensitiveString * _Nullable)authenticationToken integrationAuthentication:(OnoIntegrationAuthentication * _Nullable)integrationAuthentication __attribute__((swift_name("doCopy(onoMerchantId:authenticationToken:integrationAuthentication:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoSensitiveString * _Nullable authenticationToken __attribute__((swift_name("authenticationToken")));
@property (readonly) OnoIntegrationAuthentication * _Nullable integrationAuthentication __attribute__((swift_name("integrationAuthentication")));
@property (readonly) NSString *onoMerchantId __attribute__((swift_name("onoMerchantId")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CoreCommands.TransactionApproved")))
@interface OnoCoreCommandsTransactionApproved : OnoCommand
- (instancetype)initWithClientTransactionId:(NSString *)clientTransactionId debuggingInfo:(OnoDebuggingInfo *)debuggingInfo __attribute__((swift_name("init(clientTransactionId:debuggingInfo:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoDebuggingInfo *)component2 __attribute__((swift_name("component2()")));
- (OnoCoreCommandsTransactionApproved *)doCopyClientTransactionId:(NSString *)clientTransactionId debuggingInfo:(OnoDebuggingInfo *)debuggingInfo __attribute__((swift_name("doCopy(clientTransactionId:debuggingInfo:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *clientTransactionId __attribute__((swift_name("clientTransactionId")));
@property (readonly) OnoDebuggingInfo *debuggingInfo __attribute__((swift_name("debuggingInfo")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CoreCommands.TransactionFailed")))
@interface OnoCoreCommandsTransactionFailed : OnoCommand
- (instancetype)initWithClientTransactionId:(NSString *)clientTransactionId debuggingInfo:(OnoDebuggingInfo *)debuggingInfo __attribute__((swift_name("init(clientTransactionId:debuggingInfo:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoDebuggingInfo *)component2 __attribute__((swift_name("component2()")));
- (OnoCoreCommandsTransactionFailed *)doCopyClientTransactionId:(NSString *)clientTransactionId debuggingInfo:(OnoDebuggingInfo *)debuggingInfo __attribute__((swift_name("doCopy(clientTransactionId:debuggingInfo:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *clientTransactionId __attribute__((swift_name("clientTransactionId")));
@property (readonly) OnoDebuggingInfo *debuggingInfo __attribute__((swift_name("debuggingInfo")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CoreCommands.UpdateAuthentication")))
@interface OnoCoreCommandsUpdateAuthentication : OnoCommand
- (instancetype)initWithAuthenticationToken:(OnoSensitiveString * _Nullable)authenticationToken integrationAuthentication:(OnoIntegrationAuthentication * _Nullable)integrationAuthentication __attribute__((swift_name("init(authenticationToken:integrationAuthentication:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoSensitiveString * _Nullable)component1 __attribute__((swift_name("component1()")));
- (OnoIntegrationAuthentication * _Nullable)component2 __attribute__((swift_name("component2()")));
- (OnoCoreCommandsUpdateAuthentication *)doCopyAuthenticationToken:(OnoSensitiveString * _Nullable)authenticationToken integrationAuthentication:(OnoIntegrationAuthentication * _Nullable)integrationAuthentication __attribute__((swift_name("doCopy(authenticationToken:integrationAuthentication:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoSensitiveString * _Nullable authenticationToken __attribute__((swift_name("authenticationToken")));
@property (readonly) OnoIntegrationAuthentication * _Nullable integrationAuthentication __attribute__((swift_name("integrationAuthentication")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CoreCommands.UpdateBaseURL")))
@interface OnoCoreCommandsUpdateBaseURL : OnoCommand
- (instancetype)initWithBaseURL:(NSString *)baseURL __attribute__((swift_name("init(baseURL:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoCoreCommandsUpdateBaseURL *)doCopyBaseURL:(NSString *)baseURL __attribute__((swift_name("doCopy(baseURL:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *baseURL __attribute__((swift_name("baseURL")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CoreStateData")))
@interface OnoCoreStateData : OnoBase <OnoStoreData>
- (instancetype)initWithBaseUrl:(NSString * _Nullable)baseUrl authenticationToken:(OnoSensitiveString * _Nullable)authenticationToken integrationAuthentication:(OnoIntegrationAuthentication * _Nullable)integrationAuthentication onoAuthTokenFetchAttempts:(int32_t)onoAuthTokenFetchAttempts maxAttemptsToFetchOnoAuthToken:(int32_t)maxAttemptsToFetchOnoAuthToken terminalConfigs:(NSDictionary<OnoManufacturer *, OnoTerminalConfig *> *)terminalConfigs unhandledApiTriesBeforeBaseUrlConfig:(int32_t)unhandledApiTriesBeforeBaseUrlConfig __attribute__((swift_name("init(baseUrl:authenticationToken:integrationAuthentication:onoAuthTokenFetchAttempts:maxAttemptsToFetchOnoAuthToken:terminalConfigs:unhandledApiTriesBeforeBaseUrlConfig:)"))) __attribute__((objc_designated_initializer));
- (NSString * _Nullable)component1 __attribute__((swift_name("component1()")));
- (OnoSensitiveString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (OnoIntegrationAuthentication * _Nullable)component3 __attribute__((swift_name("component3()")));
- (int32_t)component4 __attribute__((swift_name("component4()")));
- (int32_t)component5 __attribute__((swift_name("component5()")));
- (NSDictionary<OnoManufacturer *, OnoTerminalConfig *> *)component6 __attribute__((swift_name("component6()")));
- (int32_t)component7 __attribute__((swift_name("component7()")));
- (OnoCoreStateData *)doCopyBaseUrl:(NSString * _Nullable)baseUrl authenticationToken:(OnoSensitiveString * _Nullable)authenticationToken integrationAuthentication:(OnoIntegrationAuthentication * _Nullable)integrationAuthentication onoAuthTokenFetchAttempts:(int32_t)onoAuthTokenFetchAttempts maxAttemptsToFetchOnoAuthToken:(int32_t)maxAttemptsToFetchOnoAuthToken terminalConfigs:(NSDictionary<OnoManufacturer *, OnoTerminalConfig *> *)terminalConfigs unhandledApiTriesBeforeBaseUrlConfig:(int32_t)unhandledApiTriesBeforeBaseUrlConfig __attribute__((swift_name("doCopy(baseUrl:authenticationToken:integrationAuthentication:onoAuthTokenFetchAttempts:maxAttemptsToFetchOnoAuthToken:terminalConfigs:unhandledApiTriesBeforeBaseUrlConfig:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoSensitiveString * _Nullable authenticationToken __attribute__((swift_name("authenticationToken")));
@property (readonly) NSString * _Nullable baseUrl __attribute__((swift_name("baseUrl")));
@property (readonly) BOOL hasAuthentication __attribute__((swift_name("hasAuthentication")));
@property (readonly) OnoIntegrationAuthentication * _Nullable integrationAuthentication __attribute__((swift_name("integrationAuthentication")));
@property (readonly) int32_t maxAttemptsToFetchOnoAuthToken __attribute__((swift_name("maxAttemptsToFetchOnoAuthToken")));
@property int32_t onoAuthTokenFetchAttempts __attribute__((swift_name("onoAuthTokenFetchAttempts")));
@property (readonly) NSDictionary<OnoManufacturer *, OnoTerminalConfig *> *terminalConfigs __attribute__((swift_name("terminalConfigs")));
@property (readonly) int32_t unhandledApiTriesBeforeBaseUrlConfig __attribute__((swift_name("unhandledApiTriesBeforeBaseUrlConfig")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CoreStateMachine")))
@interface OnoCoreStateMachine : OnoBase <OnoStateMachine>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)coreStateMachine __attribute__((swift_name("init()")));
- (OnoCoreStateData *)castStoreDataStoreData:(id<OnoStoreData>)storeData __attribute__((swift_name("castStoreData(storeData:)")));
- (OnoStateMachineResult *)consumeActionStoreData:(id<OnoStoreData>)storeData action:(OnoAction *)action __attribute__((swift_name("consumeAction(storeData:action:)")));
- (OnoCoreStateData *)getCurrentStateStore:(OnoStore *)store __attribute__((swift_name("getCurrentState(store:)")));
- (OnoCoreStateData *)getCurrentStateState:(NSDictionary<NSString *, id<OnoStoreData>> *)state __attribute__((swift_name("getCurrentState(state:)")));
- (OnoCoreStateData *)getStoreDataState:(NSDictionary<NSString *, id<OnoStoreData>> *)state __attribute__((swift_name("getStoreData(state:)")));
@property (readonly) OnoCoreStateData *initialState __attribute__((swift_name("initialState")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DebuggingInfo")))
@interface OnoDebuggingInfo : OnoBase
- (instancetype)initWithTimeline:(OnoActivityTimeline *)timeline paymentProcessor:(OnoPaymentProcessor *)paymentProcessor transaction:(OnoATransaction *)transaction terminal:(OnoTerminal * _Nullable)terminal summarizedTiming:(NSDictionary<NSString *, OnoDouble *> *)summarizedTiming terminalConfig:(OnoTerminalConfig * _Nullable)terminalConfig transactionError:(OnoTransactionError * _Nullable)transactionError __attribute__((swift_name("init(timeline:paymentProcessor:transaction:terminal:summarizedTiming:terminalConfig:transactionError:)"))) __attribute__((objc_designated_initializer));
- (OnoActivityTimeline *)component1 __attribute__((swift_name("component1()")));
- (OnoPaymentProcessor *)component2 __attribute__((swift_name("component2()")));
- (OnoATransaction *)component3 __attribute__((swift_name("component3()")));
- (OnoTerminal * _Nullable)component4 __attribute__((swift_name("component4()")));
- (NSDictionary<NSString *, OnoDouble *> *)component5 __attribute__((swift_name("component5()")));
- (OnoTerminalConfig * _Nullable)component6 __attribute__((swift_name("component6()")));
- (OnoTransactionError * _Nullable)component7 __attribute__((swift_name("component7()")));
- (OnoDebuggingInfo *)doCopyTimeline:(OnoActivityTimeline *)timeline paymentProcessor:(OnoPaymentProcessor *)paymentProcessor transaction:(OnoATransaction *)transaction terminal:(OnoTerminal * _Nullable)terminal summarizedTiming:(NSDictionary<NSString *, OnoDouble *> *)summarizedTiming terminalConfig:(OnoTerminalConfig * _Nullable)terminalConfig transactionError:(OnoTransactionError * _Nullable)transactionError __attribute__((swift_name("doCopy(timeline:paymentProcessor:transaction:terminal:summarizedTiming:terminalConfig:transactionError:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSDictionary<NSString *, id> *)getMappedDebugData __attribute__((swift_name("getMappedDebugData()")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoPaymentProcessor *paymentProcessor __attribute__((swift_name("paymentProcessor")));
@property (readonly) NSDictionary<NSString *, OnoDouble *> *summarizedTiming __attribute__((swift_name("summarizedTiming")));
@property (readonly) OnoTerminal * _Nullable terminal __attribute__((swift_name("terminal")));
@property (readonly) OnoTerminalConfig * _Nullable terminalConfig __attribute__((swift_name("terminalConfig")));
@property (readonly) OnoActivityTimeline *timeline __attribute__((swift_name("timeline")));
@property (readonly) OnoATransaction *transaction __attribute__((swift_name("transaction")));
@property (readonly) OnoTransactionError * _Nullable transactionError __attribute__((swift_name("transactionError")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("IntegrationAuthentication")))
@interface OnoIntegrationAuthentication : OnoBase
- (instancetype)initWithSecret:(OnoSensitiveString *)secret token:(OnoSensitiveString *)token __attribute__((swift_name("init(secret:token:)"))) __attribute__((objc_designated_initializer));
- (OnoSensitiveString *)component1 __attribute__((swift_name("component1()")));
- (OnoSensitiveString *)component2 __attribute__((swift_name("component2()")));
- (OnoIntegrationAuthentication *)doCopySecret:(OnoSensitiveString *)secret token:(OnoSensitiveString *)token __attribute__((swift_name("doCopy(secret:token:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoSensitiveString *secret __attribute__((swift_name("secret")));
@property (readonly) OnoSensitiveString *token __attribute__((swift_name("token")));
@end;

__attribute__((swift_name("CoreApiServiceProtocol")))
@interface OnoCoreApiServiceProtocol : OnoDispatcher
- (instancetype)initWithDispatch:(void (^)(OnoAction *))dispatch __attribute__((swift_name("init(dispatch:)"))) __attribute__((objc_designated_initializer));
- (void)authenticationUpdatedAuthenticationToken:(OnoSensitiveString * _Nullable)authenticationToken integrationAuthentication:(OnoIntegrationAuthentication * _Nullable)integrationAuthentication __attribute__((swift_name("authenticationUpdated(authenticationToken:integrationAuthentication:)")));
- (void)baseURLUpdatedBaseUrl:(NSString *)baseUrl __attribute__((swift_name("baseURLUpdated(baseUrl:)")));
- (void)clearAuthentication __attribute__((swift_name("clearAuthentication()")));
- (void)clearCachedTransactionToken __attribute__((swift_name("clearCachedTransactionToken()")));
- (void)fetchOnoAuthenticationTokenOnoMerchantId:(NSString *)onoMerchantId coreUserAuthenticationToken:(OnoSensitiveString * _Nullable)coreUserAuthenticationToken coreIntegrationAuthentication:(OnoIntegrationAuthentication * _Nullable)coreIntegrationAuthentication __attribute__((swift_name("fetchOnoAuthenticationToken(onoMerchantId:coreUserAuthenticationToken:coreIntegrationAuthentication:)")));
- (void)linkThisDeviceWithBusinessDeviceMap:(NSDictionary<NSString *, NSString *> *)deviceMap __attribute__((swift_name("linkThisDeviceWithBusiness(deviceMap:)")));
- (void)linkThisDeviceWithTerminalDeviceTerminalMap:(NSDictionary<NSString *, NSString *> *)deviceTerminalMap __attribute__((swift_name("linkThisDeviceWithTerminal(deviceTerminalMap:)")));
- (void)rkiSuccessfulSerialNumber:(NSString *)serialNumber __attribute__((swift_name("rkiSuccessful(serialNumber:)")));
- (void)transactionApprovedClientTransactionId:(NSString *)clientTransactionId debuggingInfo:(OnoDebuggingInfo *)debuggingInfo __attribute__((swift_name("transactionApproved(clientTransactionId:debuggingInfo:)")));
- (void)transactionFailedClientTransactionId:(NSString *)clientTransactionId debuggingInfo:(OnoDebuggingInfo *)debuggingInfo __attribute__((swift_name("transactionFailed(clientTransactionId:debuggingInfo:)")));
- (void)updateClientTransactionIdClientTransactionId:(NSString * _Nullable)clientTransactionId __attribute__((swift_name("updateClientTransactionId(clientTransactionId:)")));
@end;

__attribute__((swift_name("MockCoreApiService")))
@interface OnoMockCoreApiService : OnoCoreApiServiceProtocol
- (instancetype)initWithDispatch:(void (^)(OnoAction *))dispatch __attribute__((swift_name("init(dispatch:)"))) __attribute__((objc_designated_initializer));
- (void)authenticationUpdatedAuthenticationToken:(OnoSensitiveString * _Nullable)authenticationToken integrationAuthentication:(OnoIntegrationAuthentication * _Nullable)integrationAuthentication __attribute__((swift_name("authenticationUpdated(authenticationToken:integrationAuthentication:)")));
- (void)baseURLUpdatedBaseUrl:(NSString *)baseUrl __attribute__((swift_name("baseURLUpdated(baseUrl:)")));
- (void)clearAuthentication __attribute__((swift_name("clearAuthentication()")));
- (void)clearCachedTransactionToken __attribute__((swift_name("clearCachedTransactionToken()")));
- (void)fetchOnoAuthenticationTokenOnoMerchantId:(NSString *)onoMerchantId coreUserAuthenticationToken:(OnoSensitiveString * _Nullable)coreUserAuthenticationToken coreIntegrationAuthentication:(OnoIntegrationAuthentication * _Nullable)coreIntegrationAuthentication __attribute__((swift_name("fetchOnoAuthenticationToken(onoMerchantId:coreUserAuthenticationToken:coreIntegrationAuthentication:)")));
- (void)linkThisDeviceWithBusinessDeviceMap:(NSDictionary<NSString *, NSString *> *)device __attribute__((swift_name("linkThisDeviceWithBusiness(deviceMap:)")));
- (void)linkThisDeviceWithTerminalDeviceTerminalMap:(NSDictionary<NSString *, NSString *> *)deviceAndTerminal __attribute__((swift_name("linkThisDeviceWithTerminal(deviceTerminalMap:)")));
- (void)rkiSuccessfulSerialNumber:(NSString *)serialNumber __attribute__((swift_name("rkiSuccessful(serialNumber:)")));
- (void)transactionApprovedClientTransactionId:(NSString *)clientTransactionId debuggingInfo:(OnoDebuggingInfo *)debuggingInfo __attribute__((swift_name("transactionApproved(clientTransactionId:debuggingInfo:)")));
- (void)transactionFailedClientTransactionId:(NSString *)clientTransactionId debuggingInfo:(OnoDebuggingInfo *)debuggingInfo __attribute__((swift_name("transactionFailed(clientTransactionId:debuggingInfo:)")));
- (void)updateClientTransactionIdClientTransactionId:(NSString * _Nullable)clientTransactionId __attribute__((swift_name("updateClientTransactionId(clientTransactionId:)")));
@property (readonly) OnoKotlinPair * _Nullable approvedTransactionData __attribute__((swift_name("approvedTransactionData")));
@property (readonly) OnoSensitiveString * _Nullable authenticationToken __attribute__((swift_name("authenticationToken")));
@property (readonly) NSString * _Nullable baseUrl __attribute__((swift_name("baseUrl")));
@property (readonly) NSString * _Nullable clientTransactionId __attribute__((swift_name("clientTransactionId")));
@property (readonly) OnoKotlinPair * _Nullable failedTransactionData __attribute__((swift_name("failedTransactionData")));
@property (readonly) OnoIntegrationAuthentication * _Nullable integrationAuthentication __attribute__((swift_name("integrationAuthentication")));
@property OnoKotlinPair * _Nullable linkBusinessAndDeviceError __attribute__((swift_name("linkBusinessAndDeviceError")));
@property OnoKotlinPair * _Nullable linkTerminalAndDeviceError __attribute__((swift_name("linkTerminalAndDeviceError")));
@property (readonly) OnoSensitiveString * _Nullable onoAuthenticationToken __attribute__((swift_name("onoAuthenticationToken")));
@property BOOL shouldFailFetchOnoAuthToken __attribute__((swift_name("shouldFailFetchOnoAuthToken")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LoggingCommandDriver")))
@interface OnoLoggingCommandDriver : OnoCommandDriver
- (instancetype)initWithLoggingService:(id<OnoILogger>)loggingService __attribute__((swift_name("init(loggingService:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (void)receiveCommandCommand:(OnoCommand *)command __attribute__((swift_name("receiveCommand(command:)")));
@end;

__attribute__((swift_name("LoggingCommands")))
@interface OnoLoggingCommands : OnoBase
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LoggingCommands.SetLogUUID")))
@interface OnoLoggingCommandsSetLogUUID : OnoCommand
- (instancetype)initWithLogUUID:(NSString *)logUUID __attribute__((swift_name("init(logUUID:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoLoggingCommandsSetLogUUID *)doCopyLogUUID:(NSString *)logUUID __attribute__((swift_name("doCopy(logUUID:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *logUUID __attribute__((swift_name("logUUID")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LoggingCommands.UploadLogs")))
@interface OnoLoggingCommandsUploadLogs : OnoCommand
- (instancetype)initWithLogUUID:(NSString *)logUUID logLevel:(OnoLogLevel *)logLevel __attribute__((swift_name("init(logUUID:logLevel:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoLogLevel *)component2 __attribute__((swift_name("component2()")));
- (OnoLoggingCommandsUploadLogs *)doCopyLogUUID:(NSString *)logUUID logLevel:(OnoLogLevel *)logLevel __attribute__((swift_name("doCopy(logUUID:logLevel:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoLogLevel *logLevel __attribute__((swift_name("logLevel")));
@property (readonly) NSString *logUUID __attribute__((swift_name("logUUID")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LoggingCommands.UploadLogsInRange")))
@interface OnoLoggingCommandsUploadLogsInRange : OnoCommand
- (instancetype)initWithStartDate:(int64_t)startDate endDate:(int64_t)endDate logLevel:(OnoLogLevel *)logLevel __attribute__((swift_name("init(startDate:endDate:logLevel:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (int64_t)component1 __attribute__((swift_name("component1()")));
- (int64_t)component2 __attribute__((swift_name("component2()")));
- (OnoLogLevel *)component3 __attribute__((swift_name("component3()")));
- (OnoLoggingCommandsUploadLogsInRange *)doCopyStartDate:(int64_t)startDate endDate:(int64_t)endDate logLevel:(OnoLogLevel *)logLevel __attribute__((swift_name("doCopy(startDate:endDate:logLevel:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int64_t endDate __attribute__((swift_name("endDate")));
@property (readonly) OnoLogLevel *logLevel __attribute__((swift_name("logLevel")));
@property (readonly) int64_t startDate __attribute__((swift_name("startDate")));
@end;

__attribute__((swift_name("TLVDecoderServiceProtocol")))
@interface OnoTLVDecoderServiceProtocol : OnoBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (NSArray<NSString *> * _Nullable)extractBinaryStringListFromTLVTlv:(NSString *)tlv tag:(NSString *)tag __attribute__((swift_name("extractBinaryStringListFromTLV(tlv:tag:)")));
- (NSString * _Nullable)extractHexStringFromTLVTlv:(NSString *)tlv tag:(NSString *)tag __attribute__((swift_name("extractHexStringFromTLV(tlv:tag:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MockTlvDecoderService")))
@interface OnoMockTlvDecoderService : OnoTLVDecoderServiceProtocol
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (NSArray<NSString *> * _Nullable)extractBinaryStringListFromTLVTlv:(NSString *)tlv tag:(NSString *)tag __attribute__((swift_name("extractBinaryStringListFromTLV(tlv:tag:)")));
- (NSString * _Nullable)extractHexStringFromTLVTlv:(NSString *)tlv tag:(NSString *)tag __attribute__((swift_name("extractHexStringFromTLV(tlv:tag:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TLVUtils")))
@interface OnoTLVUtils : OnoBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)tLVUtils __attribute__((swift_name("init()")));
- (OnoBoolean * _Nullable)getBitAtPositionBytes:(NSArray<NSString *> *)bytes byteIndex:(int32_t)byteIndex bitIndex:(int32_t)bitIndex __attribute__((swift_name("getBitAtPosition(bytes:byteIndex:bitIndex:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TLV_TAGS")))
@interface OnoTLV_TAGS : OnoKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) OnoTLV_TAGS *tvr __attribute__((swift_name("tvr")));
- (int32_t)compareToOther:(OnoTLV_TAGS *)other __attribute__((swift_name("compareTo(other:)")));
@property (readonly) NSString *hexValue __attribute__((swift_name("hexValue")));
@end;

__attribute__((swift_name("IOnoUpdateService")))
@protocol OnoIOnoUpdateService
@required
- (void)downloadAndVerifyUpdateAvailableUpdate:(OnoAvailableUpdate *)availableUpdate onComplete:(void (^)(NSDictionary<NSString *, OnoKotlinByteArray *> *))onComplete onFailure:(void (^)(NSString *))onFailure __attribute__((swift_name("downloadAndVerifyUpdate(availableUpdate:onComplete:onFailure:)")));
@end;

__attribute__((swift_name("OnoActions")))
@interface OnoOnoActions : OnoBase
@end;

__attribute__((swift_name("OnoApiResultAction")))
@interface OnoOnoApiResultAction : OnoAction
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property (readonly) OnoInt * _Nullable httpStatusCode __attribute__((swift_name("httpStatusCode")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoActions.AuthorizationFailed")))
@interface OnoOnoActionsAuthorizationFailed : OnoOnoApiResultAction
- (instancetype)initWithHttpStatusCode:(OnoInt * _Nullable)httpStatusCode errorDetails:(OnoErrorDetails *)errorDetails maybeOnoTransactionId:(NSString * _Nullable)maybeOnoTransactionId __attribute__((swift_name("init(httpStatusCode:errorDetails:maybeOnoTransactionId:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoInt * _Nullable)component1 __attribute__((swift_name("component1()")));
- (OnoErrorDetails *)component2 __attribute__((swift_name("component2()")));
- (NSString * _Nullable)component3 __attribute__((swift_name("component3()")));
- (OnoOnoActionsAuthorizationFailed *)doCopyHttpStatusCode:(OnoInt * _Nullable)httpStatusCode errorDetails:(OnoErrorDetails *)errorDetails maybeOnoTransactionId:(NSString * _Nullable)maybeOnoTransactionId __attribute__((swift_name("doCopy(httpStatusCode:errorDetails:maybeOnoTransactionId:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoErrorDetails *errorDetails __attribute__((swift_name("errorDetails")));
@property (readonly) OnoInt * _Nullable httpStatusCode __attribute__((swift_name("httpStatusCode")));
@property (readonly) NSString * _Nullable maybeOnoTransactionId __attribute__((swift_name("maybeOnoTransactionId")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoActions.AuthorizationResponse")))
@interface OnoOnoActionsAuthorizationResponse : OnoOnoApiResultAction
- (instancetype)initWithClientTransactionId:(NSString *)clientTransactionId transactionId:(NSString *)transactionId chargeResult:(NSString *)chargeResult isoResponse:(NSString * _Nullable)isoResponse chargeResultMessage:(NSString * _Nullable)chargeResultMessage arpc:(OnoSensitiveString * _Nullable)arpc signatureRequired:(OnoBoolean * _Nullable)signatureRequired errorMessage:(NSString * _Nullable)errorMessage attempt:(OnoInt * _Nullable)attempt httpStatusCode:(OnoInt * _Nullable)httpStatusCode __attribute__((swift_name("init(clientTransactionId:transactionId:chargeResult:isoResponse:chargeResultMessage:arpc:signatureRequired:errorMessage:attempt:httpStatusCode:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoInt * _Nullable)component10 __attribute__((swift_name("component10()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (NSString * _Nullable)component4 __attribute__((swift_name("component4()")));
- (NSString * _Nullable)component5 __attribute__((swift_name("component5()")));
- (OnoSensitiveString * _Nullable)component6 __attribute__((swift_name("component6()")));
- (OnoBoolean * _Nullable)component7 __attribute__((swift_name("component7()")));
- (NSString * _Nullable)component8 __attribute__((swift_name("component8()")));
- (OnoInt * _Nullable)component9 __attribute__((swift_name("component9()")));
- (OnoOnoActionsAuthorizationResponse *)doCopyClientTransactionId:(NSString *)clientTransactionId transactionId:(NSString *)transactionId chargeResult:(NSString *)chargeResult isoResponse:(NSString * _Nullable)isoResponse chargeResultMessage:(NSString * _Nullable)chargeResultMessage arpc:(OnoSensitiveString * _Nullable)arpc signatureRequired:(OnoBoolean * _Nullable)signatureRequired errorMessage:(NSString * _Nullable)errorMessage attempt:(OnoInt * _Nullable)attempt httpStatusCode:(OnoInt * _Nullable)httpStatusCode __attribute__((swift_name("doCopy(clientTransactionId:transactionId:chargeResult:isoResponse:chargeResultMessage:arpc:signatureRequired:errorMessage:attempt:httpStatusCode:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoSensitiveString * _Nullable arpc __attribute__((swift_name("arpc")));
@property (readonly) OnoInt * _Nullable attempt __attribute__((swift_name("attempt")));
@property (readonly) NSString *chargeResult __attribute__((swift_name("chargeResult")));
@property (readonly) NSString * _Nullable chargeResultMessage __attribute__((swift_name("chargeResultMessage")));
@property (readonly) NSString *clientTransactionId __attribute__((swift_name("clientTransactionId")));
@property (readonly) NSString * _Nullable errorMessage __attribute__((swift_name("errorMessage")));
@property (readonly) OnoInt * _Nullable httpStatusCode __attribute__((swift_name("httpStatusCode")));
@property (readonly) NSString * _Nullable isoResponse __attribute__((swift_name("isoResponse")));
@property (readonly) OnoBoolean * _Nullable signatureRequired __attribute__((swift_name("signatureRequired")));
@property (readonly) NSString *transactionId __attribute__((swift_name("transactionId")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoActions.BackgroundRejectResult")))
@interface OnoOnoActionsBackgroundRejectResult : OnoOnoApiResultAction
- (instancetype)initWithTransaction:(OnoTransaction *)transaction attempt:(int32_t)attempt httpStatusCode:(OnoInt * _Nullable)httpStatusCode errorDetails:(OnoErrorDetails * _Nullable)errorDetails __attribute__((swift_name("init(transaction:attempt:httpStatusCode:errorDetails:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoTransaction *)component1 __attribute__((swift_name("component1()")));
- (int32_t)component2 __attribute__((swift_name("component2()")));
- (OnoInt * _Nullable)component3 __attribute__((swift_name("component3()")));
- (OnoErrorDetails * _Nullable)component4 __attribute__((swift_name("component4()")));
- (OnoOnoActionsBackgroundRejectResult *)doCopyTransaction:(OnoTransaction *)transaction attempt:(int32_t)attempt httpStatusCode:(OnoInt * _Nullable)httpStatusCode errorDetails:(OnoErrorDetails * _Nullable)errorDetails __attribute__((swift_name("doCopy(transaction:attempt:httpStatusCode:errorDetails:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t attempt __attribute__((swift_name("attempt")));
@property (readonly) OnoErrorDetails * _Nullable errorDetails __attribute__((swift_name("errorDetails")));
@property (readonly) OnoInt * _Nullable httpStatusCode __attribute__((swift_name("httpStatusCode")));
@property (readonly) OnoTransaction *transaction __attribute__((swift_name("transaction")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoActions.CheckForUpdatesFailed")))
@interface OnoOnoActionsCheckForUpdatesFailed : OnoOnoApiResultAction
- (instancetype)initWithErrorDetails:(OnoErrorDetails *)errorDetails httpStatusCode:(OnoInt * _Nullable)httpStatusCode __attribute__((swift_name("init(errorDetails:httpStatusCode:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoErrorDetails *)component1 __attribute__((swift_name("component1()")));
- (OnoInt * _Nullable)component2 __attribute__((swift_name("component2()")));
- (OnoOnoActionsCheckForUpdatesFailed *)doCopyErrorDetails:(OnoErrorDetails *)errorDetails httpStatusCode:(OnoInt * _Nullable)httpStatusCode __attribute__((swift_name("doCopy(errorDetails:httpStatusCode:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoErrorDetails *errorDetails __attribute__((swift_name("errorDetails")));
@property (readonly) OnoInt * _Nullable httpStatusCode __attribute__((swift_name("httpStatusCode")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoActions.ClearMerchant")))
@interface OnoOnoActionsClearMerchant : OnoAction
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoActions.ClearMerchantSecret")))
@interface OnoOnoActionsClearMerchantSecret : OnoAction
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoActions.FetchOnoAuthenticationTokenFailed")))
@interface OnoOnoActionsFetchOnoAuthenticationTokenFailed : OnoAction
- (instancetype)initWithOnoMerchantId:(NSString *)onoMerchantId errorDetails:(OnoErrorDetails *)errorDetails __attribute__((swift_name("init(onoMerchantId:errorDetails:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoErrorDetails *)component2 __attribute__((swift_name("component2()")));
- (OnoOnoActionsFetchOnoAuthenticationTokenFailed *)doCopyOnoMerchantId:(NSString *)onoMerchantId errorDetails:(OnoErrorDetails *)errorDetails __attribute__((swift_name("doCopy(onoMerchantId:errorDetails:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoErrorDetails *errorDetails __attribute__((swift_name("errorDetails")));
@property (readonly) NSString *onoMerchantId __attribute__((swift_name("onoMerchantId")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoActions.FetchedMerchantSecret")))
@interface OnoOnoActionsFetchedMerchantSecret : OnoAction
- (instancetype)initWithSecret:(OnoSensitiveString *)secret __attribute__((swift_name("init(secret:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoSensitiveString *)component1 __attribute__((swift_name("component1()")));
- (OnoOnoActionsFetchedMerchantSecret *)doCopySecret:(OnoSensitiveString *)secret __attribute__((swift_name("doCopy(secret:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoSensitiveString *secret __attribute__((swift_name("secret")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoActions.FinalizationFailed")))
@interface OnoOnoActionsFinalizationFailed : OnoOnoApiResultAction
- (instancetype)initWithClientTransactionID:(NSString *)clientTransactionID finalizeState:(OnoFinalizeState *)finalizeState finalizationBody:(OnoOnoFinalizationBody *)finalizationBody errorDetails:(OnoErrorDetails *)errorDetails httpStatusCode:(OnoInt * _Nullable)httpStatusCode __attribute__((swift_name("init(clientTransactionID:finalizeState:finalizationBody:errorDetails:httpStatusCode:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoFinalizeState *)component2 __attribute__((swift_name("component2()")));
- (OnoOnoFinalizationBody *)component3 __attribute__((swift_name("component3()")));
- (OnoErrorDetails *)component4 __attribute__((swift_name("component4()")));
- (OnoInt * _Nullable)component5 __attribute__((swift_name("component5()")));
- (OnoOnoActionsFinalizationFailed *)doCopyClientTransactionID:(NSString *)clientTransactionID finalizeState:(OnoFinalizeState *)finalizeState finalizationBody:(OnoOnoFinalizationBody *)finalizationBody errorDetails:(OnoErrorDetails *)errorDetails httpStatusCode:(OnoInt * _Nullable)httpStatusCode __attribute__((swift_name("doCopy(clientTransactionID:finalizeState:finalizationBody:errorDetails:httpStatusCode:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *clientTransactionID __attribute__((swift_name("clientTransactionID")));
@property (readonly) OnoErrorDetails *errorDetails __attribute__((swift_name("errorDetails")));
@property (readonly) OnoOnoFinalizationBody *finalizationBody __attribute__((swift_name("finalizationBody")));
@property (readonly) OnoFinalizeState *finalizeState __attribute__((swift_name("finalizeState")));
@property (readonly) OnoInt * _Nullable httpStatusCode __attribute__((swift_name("httpStatusCode")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoActions.FinalizationResponse")))
@interface OnoOnoActionsFinalizationResponse : OnoAction
- (instancetype)initWithTransactionState:(OnoTransactionState *)transactionState message:(NSString * _Nullable)message transactionId:(NSString *)transactionId __attribute__((swift_name("init(transactionState:message:transactionId:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoTransactionState *)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (OnoOnoActionsFinalizationResponse *)doCopyTransactionState:(OnoTransactionState *)transactionState message:(NSString * _Nullable)message transactionId:(NSString *)transactionId __attribute__((swift_name("doCopy(transactionState:message:transactionId:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable message __attribute__((swift_name("message")));
@property (readonly) NSString *transactionId __attribute__((swift_name("transactionId")));
@property (readonly) OnoTransactionState *transactionState __attribute__((swift_name("transactionState")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoActions.FinalizationTimeoutReached")))
@interface OnoOnoActionsFinalizationTimeoutReached : OnoAction
- (instancetype)initWithTransactionId:(NSString *)transactionId transactionState:(OnoTransactionState *)transactionState finalizeState:(OnoFinalizeState *)finalizeState finalizationBody:(OnoOnoFinalizationBody *)finalizationBody __attribute__((swift_name("init(transactionId:transactionState:finalizeState:finalizationBody:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoTransactionState *)component2 __attribute__((swift_name("component2()")));
- (OnoFinalizeState *)component3 __attribute__((swift_name("component3()")));
- (OnoOnoFinalizationBody *)component4 __attribute__((swift_name("component4()")));
- (OnoOnoActionsFinalizationTimeoutReached *)doCopyTransactionId:(NSString *)transactionId transactionState:(OnoTransactionState *)transactionState finalizeState:(OnoFinalizeState *)finalizeState finalizationBody:(OnoOnoFinalizationBody *)finalizationBody __attribute__((swift_name("doCopy(transactionId:transactionState:finalizeState:finalizationBody:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoOnoFinalizationBody *finalizationBody __attribute__((swift_name("finalizationBody")));
@property (readonly) OnoFinalizeState *finalizeState __attribute__((swift_name("finalizeState")));
@property (readonly) NSString *transactionId __attribute__((swift_name("transactionId")));
@property (readonly) OnoTransactionState *transactionState __attribute__((swift_name("transactionState")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoActions.GetServiceStatus")))
@interface OnoOnoActionsGetServiceStatus : OnoAction
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoActions.IdentifyMerchant")))
@interface OnoOnoActionsIdentifyMerchant : OnoAction
- (instancetype)initWithId:(NSString *)id __attribute__((swift_name("init(id:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoOnoActionsIdentifyMerchant *)doCopyId:(NSString *)id __attribute__((swift_name("doCopy(id:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *id __attribute__((swift_name("id")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoActions.NullApiInterface")))
@interface OnoOnoActionsNullApiInterface : OnoAction
- (instancetype)initWithUserClass:(id<OnoKotlinKClass>)userClass __attribute__((swift_name("init(userClass:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (id<OnoKotlinKClass>)component1 __attribute__((swift_name("component1()")));
- (OnoOnoActionsNullApiInterface *)doCopyUserClass:(id<OnoKotlinKClass>)userClass __attribute__((swift_name("doCopy(userClass:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) id<OnoKotlinKClass> userClass __attribute__((swift_name("userClass")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoActions.RkiServerRequestFailed")))
@interface OnoOnoActionsRkiServerRequestFailed : OnoOnoApiResultAction
- (instancetype)initWithTerminal:(OnoTerminal *)terminal rkiRequestMap:(OnoSensitiveMap *)rkiRequestMap errorDetails:(OnoErrorDetails *)errorDetails requestID:(int32_t)requestID attempt:(int32_t)attempt httpStatusCode:(OnoInt * _Nullable)httpStatusCode __attribute__((swift_name("init(terminal:rkiRequestMap:errorDetails:requestID:attempt:httpStatusCode:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoTerminal *)component1 __attribute__((swift_name("component1()")));
- (OnoSensitiveMap *)component2 __attribute__((swift_name("component2()")));
- (OnoErrorDetails *)component3 __attribute__((swift_name("component3()")));
- (int32_t)component4 __attribute__((swift_name("component4()")));
- (int32_t)component5 __attribute__((swift_name("component5()")));
- (OnoInt * _Nullable)component6 __attribute__((swift_name("component6()")));
- (OnoOnoActionsRkiServerRequestFailed *)doCopyTerminal:(OnoTerminal *)terminal rkiRequestMap:(OnoSensitiveMap *)rkiRequestMap errorDetails:(OnoErrorDetails *)errorDetails requestID:(int32_t)requestID attempt:(int32_t)attempt httpStatusCode:(OnoInt * _Nullable)httpStatusCode __attribute__((swift_name("doCopy(terminal:rkiRequestMap:errorDetails:requestID:attempt:httpStatusCode:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t attempt __attribute__((swift_name("attempt")));
@property (readonly) OnoErrorDetails *errorDetails __attribute__((swift_name("errorDetails")));
@property (readonly) OnoInt * _Nullable httpStatusCode __attribute__((swift_name("httpStatusCode")));
@property (readonly) int32_t requestID __attribute__((swift_name("requestID")));
@property (readonly) OnoSensitiveMap *rkiRequestMap __attribute__((swift_name("rkiRequestMap")));
@property (readonly) OnoTerminal *terminal __attribute__((swift_name("terminal")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoActions.RkiServerResponse")))
@interface OnoOnoActionsRkiServerResponse : OnoOnoApiResultAction
- (instancetype)initWithTerminal:(OnoTerminal *)terminal rkiResponseData:(OnoSensitiveMap *)rkiResponseData requestID:(int32_t)requestID httpStatusCode:(OnoInt * _Nullable)httpStatusCode __attribute__((swift_name("init(terminal:rkiResponseData:requestID:httpStatusCode:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoTerminal *)component1 __attribute__((swift_name("component1()")));
- (OnoSensitiveMap *)component2 __attribute__((swift_name("component2()")));
- (int32_t)component3 __attribute__((swift_name("component3()")));
- (OnoInt * _Nullable)component4 __attribute__((swift_name("component4()")));
- (OnoOnoActionsRkiServerResponse *)doCopyTerminal:(OnoTerminal *)terminal rkiResponseData:(OnoSensitiveMap *)rkiResponseData requestID:(int32_t)requestID httpStatusCode:(OnoInt * _Nullable)httpStatusCode __attribute__((swift_name("doCopy(terminal:rkiResponseData:requestID:httpStatusCode:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoInt * _Nullable httpStatusCode __attribute__((swift_name("httpStatusCode")));
@property (readonly) int32_t requestID __attribute__((swift_name("requestID")));
@property (readonly) OnoSensitiveMap *rkiResponseData __attribute__((swift_name("rkiResponseData")));
@property (readonly) OnoTerminal *terminal __attribute__((swift_name("terminal")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoActions.SetOnoURL")))
@interface OnoOnoActionsSetOnoURL : OnoAction
- (instancetype)initWithUrl:(NSString *)url __attribute__((swift_name("init(url:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoOnoActionsSetOnoURL *)doCopyUrl:(NSString *)url __attribute__((swift_name("doCopy(url:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *url __attribute__((swift_name("url")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoActions.SignatureSubmissionTimeoutReached")))
@interface OnoOnoActionsSignatureSubmissionTimeoutReached : OnoAction
- (instancetype)initWithTransactionId:(NSString *)transactionId signatureBody:(OnoOnoSignatureBody *)signatureBody __attribute__((swift_name("init(transactionId:signatureBody:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoOnoSignatureBody *)component2 __attribute__((swift_name("component2()")));
- (OnoOnoActionsSignatureSubmissionTimeoutReached *)doCopyTransactionId:(NSString *)transactionId signatureBody:(OnoOnoSignatureBody *)signatureBody __attribute__((swift_name("doCopy(transactionId:signatureBody:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoOnoSignatureBody *signatureBody __attribute__((swift_name("signatureBody")));
@property (readonly) NSString *transactionId __attribute__((swift_name("transactionId")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoActions.SignatureSubmittedSuccessfully")))
@interface OnoOnoActionsSignatureSubmittedSuccessfully : OnoAction
- (instancetype)initWithTransactionId:(NSString *)transactionId __attribute__((swift_name("init(transactionId:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoOnoActionsSignatureSubmittedSuccessfully *)doCopyTransactionId:(NSString *)transactionId __attribute__((swift_name("doCopy(transactionId:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *transactionId __attribute__((swift_name("transactionId")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoActions.SubmitSignatureFailed")))
@interface OnoOnoActionsSubmitSignatureFailed : OnoOnoApiResultAction
- (instancetype)initWithTransactionId:(NSString *)transactionId signatureBody:(OnoOnoSignatureBody *)signatureBody errorDetails:(OnoErrorDetails *)errorDetails httpStatusCode:(OnoInt * _Nullable)httpStatusCode __attribute__((swift_name("init(transactionId:signatureBody:errorDetails:httpStatusCode:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoOnoSignatureBody *)component2 __attribute__((swift_name("component2()")));
- (OnoErrorDetails *)component3 __attribute__((swift_name("component3()")));
- (OnoInt * _Nullable)component4 __attribute__((swift_name("component4()")));
- (OnoOnoActionsSubmitSignatureFailed *)doCopyTransactionId:(NSString *)transactionId signatureBody:(OnoOnoSignatureBody *)signatureBody errorDetails:(OnoErrorDetails *)errorDetails httpStatusCode:(OnoInt * _Nullable)httpStatusCode __attribute__((swift_name("doCopy(transactionId:signatureBody:errorDetails:httpStatusCode:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoErrorDetails *errorDetails __attribute__((swift_name("errorDetails")));
@property (readonly) OnoInt * _Nullable httpStatusCode __attribute__((swift_name("httpStatusCode")));
@property (readonly) OnoOnoSignatureBody *signatureBody __attribute__((swift_name("signatureBody")));
@property (readonly) NSString *transactionId __attribute__((swift_name("transactionId")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoCommandDriver")))
@interface OnoOnoCommandDriver : OnoCommandDriver
- (instancetype)initWithOnoApiService:(OnoOnoApiServiceProtocol *)onoApiService __attribute__((swift_name("init(onoApiService:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (void)receiveCommandCommand:(OnoCommand *)command __attribute__((swift_name("receiveCommand(command:)")));
@end;

__attribute__((swift_name("OnoCommands")))
@interface OnoOnoCommands : OnoBase
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoCommands.AuthorizeTransaction")))
@interface OnoOnoCommandsAuthorizeTransaction : OnoCommand
- (instancetype)initWithAuthorizationBody:(OnoOnoAuthorizationBody *)authorizationBody bin:(NSString * _Nullable)bin requestID:(int32_t)requestID __attribute__((swift_name("init(authorizationBody:bin:requestID:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoOnoAuthorizationBody *)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (int32_t)component3 __attribute__((swift_name("component3()")));
- (OnoOnoCommandsAuthorizeTransaction *)doCopyAuthorizationBody:(OnoOnoAuthorizationBody *)authorizationBody bin:(NSString * _Nullable)bin requestID:(int32_t)requestID __attribute__((swift_name("doCopy(authorizationBody:bin:requestID:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoOnoAuthorizationBody *authorizationBody __attribute__((swift_name("authorizationBody")));
@property (readonly) NSString * _Nullable bin __attribute__((swift_name("bin")));
@property (readonly) int32_t requestID __attribute__((swift_name("requestID")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoCommands.BackgroundRejectTransaction")))
@interface OnoOnoCommandsBackgroundRejectTransaction : OnoCommand
- (instancetype)initWithTransaction:(OnoTransaction *)transaction attempt:(int32_t)attempt __attribute__((swift_name("init(transaction:attempt:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoTransaction *)component1 __attribute__((swift_name("component1()")));
- (int32_t)component2 __attribute__((swift_name("component2()")));
- (OnoOnoCommandsBackgroundRejectTransaction *)doCopyTransaction:(OnoTransaction *)transaction attempt:(int32_t)attempt __attribute__((swift_name("doCopy(transaction:attempt:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t attempt __attribute__((swift_name("attempt")));
@property (readonly) OnoTransaction *transaction __attribute__((swift_name("transaction")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoCommands.ChargeTransaction")))
@interface OnoOnoCommandsChargeTransaction : OnoCommand
- (instancetype)initWithChargeTransactionBody:(OnoOnoChargeTransactionBody *)chargeTransactionBody requestID:(int32_t)requestID onoAuthToken:(OnoSensitiveString * _Nullable)onoAuthToken __attribute__((swift_name("init(chargeTransactionBody:requestID:onoAuthToken:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoOnoChargeTransactionBody *)component1 __attribute__((swift_name("component1()")));
- (int32_t)component2 __attribute__((swift_name("component2()")));
- (OnoSensitiveString * _Nullable)component3 __attribute__((swift_name("component3()")));
- (OnoOnoCommandsChargeTransaction *)doCopyChargeTransactionBody:(OnoOnoChargeTransactionBody *)chargeTransactionBody requestID:(int32_t)requestID onoAuthToken:(OnoSensitiveString * _Nullable)onoAuthToken __attribute__((swift_name("doCopy(chargeTransactionBody:requestID:onoAuthToken:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoOnoChargeTransactionBody *chargeTransactionBody __attribute__((swift_name("chargeTransactionBody")));
@property (readonly) OnoSensitiveString * _Nullable onoAuthToken __attribute__((swift_name("onoAuthToken")));
@property (readonly) int32_t requestID __attribute__((swift_name("requestID")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoCommands.CheckForUpdates")))
@interface OnoOnoCommandsCheckForUpdates : OnoCommand
- (instancetype)initWithTerminal:(OnoTerminal *)terminal requestID:(int32_t)requestID __attribute__((swift_name("init(terminal:requestID:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoTerminal *)component1 __attribute__((swift_name("component1()")));
- (int32_t)component2 __attribute__((swift_name("component2()")));
- (OnoOnoCommandsCheckForUpdates *)doCopyTerminal:(OnoTerminal *)terminal requestID:(int32_t)requestID __attribute__((swift_name("doCopy(terminal:requestID:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t requestID __attribute__((swift_name("requestID")));
@property (readonly) OnoTerminal *terminal __attribute__((swift_name("terminal")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoCommands.ClearMerchant")))
@interface OnoOnoCommandsClearMerchant : OnoCommand
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoCommands.ClearMerchantSecret")))
@interface OnoOnoCommandsClearMerchantSecret : OnoCommand
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoCommands.ConfirmTransaction")))
@interface OnoOnoCommandsConfirmTransaction : OnoCommand
- (instancetype)initWithTransactionId:(NSString *)transactionId confirmationBody:(OnoOnoConfirmationBody *)confirmationBody requestID:(int32_t)requestID __attribute__((swift_name("init(transactionId:confirmationBody:requestID:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoOnoConfirmationBody *)component2 __attribute__((swift_name("component2()")));
- (int32_t)component3 __attribute__((swift_name("component3()")));
- (OnoOnoCommandsConfirmTransaction *)doCopyTransactionId:(NSString *)transactionId confirmationBody:(OnoOnoConfirmationBody *)confirmationBody requestID:(int32_t)requestID __attribute__((swift_name("doCopy(transactionId:confirmationBody:requestID:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoOnoConfirmationBody *confirmationBody __attribute__((swift_name("confirmationBody")));
@property (readonly) int32_t requestID __attribute__((swift_name("requestID")));
@property (readonly) NSString *transactionId __attribute__((swift_name("transactionId")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoCommands.DelayBackgroundRejectTransaction")))
@interface OnoOnoCommandsDelayBackgroundRejectTransaction : OnoCommand
- (instancetype)initWithTransaction:(OnoTransaction *)transaction attempt:(int32_t)attempt __attribute__((swift_name("init(transaction:attempt:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoTransaction *)component1 __attribute__((swift_name("component1()")));
- (int32_t)component2 __attribute__((swift_name("component2()")));
- (OnoOnoCommandsDelayBackgroundRejectTransaction *)doCopyTransaction:(OnoTransaction *)transaction attempt:(int32_t)attempt __attribute__((swift_name("doCopy(transaction:attempt:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t attempt __attribute__((swift_name("attempt")));
@property (readonly) OnoTransaction *transaction __attribute__((swift_name("transaction")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoCommands.FinalizeTransaction")))
@interface OnoOnoCommandsFinalizeTransaction : OnoCommand
- (instancetype)initWithTransactionId:(NSString *)transactionId expectedFinalTransactionState:(OnoTransactionState *)expectedFinalTransactionState finalizeState:(OnoFinalizeState *)finalizeState finalizationBody:(OnoOnoFinalizationBody *)finalizationBody timeoutSeconds:(int32_t)timeoutSeconds requestID:(int32_t)requestID __attribute__((swift_name("init(transactionId:expectedFinalTransactionState:finalizeState:finalizationBody:timeoutSeconds:requestID:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoTransactionState *)component2 __attribute__((swift_name("component2()")));
- (OnoFinalizeState *)component3 __attribute__((swift_name("component3()")));
- (OnoOnoFinalizationBody *)component4 __attribute__((swift_name("component4()")));
- (int32_t)component5 __attribute__((swift_name("component5()")));
- (int32_t)component6 __attribute__((swift_name("component6()")));
- (OnoOnoCommandsFinalizeTransaction *)doCopyTransactionId:(NSString *)transactionId expectedFinalTransactionState:(OnoTransactionState *)expectedFinalTransactionState finalizeState:(OnoFinalizeState *)finalizeState finalizationBody:(OnoOnoFinalizationBody *)finalizationBody timeoutSeconds:(int32_t)timeoutSeconds requestID:(int32_t)requestID __attribute__((swift_name("doCopy(transactionId:expectedFinalTransactionState:finalizeState:finalizationBody:timeoutSeconds:requestID:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoTransactionState *expectedFinalTransactionState __attribute__((swift_name("expectedFinalTransactionState")));
@property (readonly) OnoOnoFinalizationBody *finalizationBody __attribute__((swift_name("finalizationBody")));
@property (readonly) OnoFinalizeState *finalizeState __attribute__((swift_name("finalizeState")));
@property (readonly) int32_t requestID __attribute__((swift_name("requestID")));
@property (readonly) int32_t timeoutSeconds __attribute__((swift_name("timeoutSeconds")));
@property (readonly) NSString *transactionId __attribute__((swift_name("transactionId")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoCommands.PollChargeStatus")))
@interface OnoOnoCommandsPollChargeStatus : OnoCommand
- (instancetype)initWithClientTransactionId:(NSString *)clientTransactionId requestID:(int32_t)requestID __attribute__((swift_name("init(clientTransactionId:requestID:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (int32_t)component2 __attribute__((swift_name("component2()")));
- (OnoOnoCommandsPollChargeStatus *)doCopyClientTransactionId:(NSString *)clientTransactionId requestID:(int32_t)requestID __attribute__((swift_name("doCopy(clientTransactionId:requestID:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *clientTransactionId __attribute__((swift_name("clientTransactionId")));
@property (readonly) int32_t requestID __attribute__((swift_name("requestID")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoCommands.ProcessRkiServerRequest")))
@interface OnoOnoCommandsProcessRkiServerRequest : OnoCommand
- (instancetype)initWithTerminal:(OnoTerminal *)terminal rkiRequestData:(OnoSensitiveMap *)rkiRequestData requestID:(int32_t)requestID __attribute__((swift_name("init(terminal:rkiRequestData:requestID:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoTerminal *)component1 __attribute__((swift_name("component1()")));
- (OnoSensitiveMap *)component2 __attribute__((swift_name("component2()")));
- (int32_t)component3 __attribute__((swift_name("component3()")));
- (OnoOnoCommandsProcessRkiServerRequest *)doCopyTerminal:(OnoTerminal *)terminal rkiRequestData:(OnoSensitiveMap *)rkiRequestData requestID:(int32_t)requestID __attribute__((swift_name("doCopy(terminal:rkiRequestData:requestID:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t requestID __attribute__((swift_name("requestID")));
@property (readonly) OnoSensitiveMap *rkiRequestData __attribute__((swift_name("rkiRequestData")));
@property (readonly) OnoTerminal *terminal __attribute__((swift_name("terminal")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoCommands.RejectTransaction")))
@interface OnoOnoCommandsRejectTransaction : OnoCommand
- (instancetype)initWithTransactionId:(NSString *)transactionId rejectionBody:(OnoOnoConfirmationBody *)rejectionBody requestID:(int32_t)requestID __attribute__((swift_name("init(transactionId:rejectionBody:requestID:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoOnoConfirmationBody *)component2 __attribute__((swift_name("component2()")));
- (int32_t)component3 __attribute__((swift_name("component3()")));
- (OnoOnoCommandsRejectTransaction *)doCopyTransactionId:(NSString *)transactionId rejectionBody:(OnoOnoConfirmationBody *)rejectionBody requestID:(int32_t)requestID __attribute__((swift_name("doCopy(transactionId:rejectionBody:requestID:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoOnoConfirmationBody *rejectionBody __attribute__((swift_name("rejectionBody")));
@property (readonly) int32_t requestID __attribute__((swift_name("requestID")));
@property (readonly) NSString *transactionId __attribute__((swift_name("transactionId")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoCommands.RejectTransactionByClientId")))
@interface OnoOnoCommandsRejectTransactionByClientId : OnoCommand
- (instancetype)initWithClientTransactionId:(NSString *)clientTransactionId rejectionBody:(OnoOnoRejectByClientIdBody *)rejectionBody requestID:(int32_t)requestID __attribute__((swift_name("init(clientTransactionId:rejectionBody:requestID:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoOnoRejectByClientIdBody *)component2 __attribute__((swift_name("component2()")));
- (int32_t)component3 __attribute__((swift_name("component3()")));
- (OnoOnoCommandsRejectTransactionByClientId *)doCopyClientTransactionId:(NSString *)clientTransactionId rejectionBody:(OnoOnoRejectByClientIdBody *)rejectionBody requestID:(int32_t)requestID __attribute__((swift_name("doCopy(clientTransactionId:rejectionBody:requestID:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *clientTransactionId __attribute__((swift_name("clientTransactionId")));
@property (readonly) OnoOnoRejectByClientIdBody *rejectionBody __attribute__((swift_name("rejectionBody")));
@property (readonly) int32_t requestID __attribute__((swift_name("requestID")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoCommands.RetryRkiServerRequest")))
@interface OnoOnoCommandsRetryRkiServerRequest : OnoCommand
- (instancetype)initWithTerminal:(OnoTerminal *)terminal rkiRequestData:(OnoSensitiveMap *)rkiRequestData requestID:(int32_t)requestID attempt:(int32_t)attempt __attribute__((swift_name("init(terminal:rkiRequestData:requestID:attempt:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoTerminal *)component1 __attribute__((swift_name("component1()")));
- (OnoSensitiveMap *)component2 __attribute__((swift_name("component2()")));
- (int32_t)component3 __attribute__((swift_name("component3()")));
- (int32_t)component4 __attribute__((swift_name("component4()")));
- (OnoOnoCommandsRetryRkiServerRequest *)doCopyTerminal:(OnoTerminal *)terminal rkiRequestData:(OnoSensitiveMap *)rkiRequestData requestID:(int32_t)requestID attempt:(int32_t)attempt __attribute__((swift_name("doCopy(terminal:rkiRequestData:requestID:attempt:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t attempt __attribute__((swift_name("attempt")));
@property (readonly) int32_t requestID __attribute__((swift_name("requestID")));
@property (readonly) OnoSensitiveMap *rkiRequestData __attribute__((swift_name("rkiRequestData")));
@property (readonly) OnoTerminal *terminal __attribute__((swift_name("terminal")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoCommands.RetryTransactionAuthorization")))
@interface OnoOnoCommandsRetryTransactionAuthorization : OnoCommand
- (instancetype)initWithClientTransactionId:(NSString *)clientTransactionId attempt:(int32_t)attempt requestID:(int32_t)requestID __attribute__((swift_name("init(clientTransactionId:attempt:requestID:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (int32_t)component2 __attribute__((swift_name("component2()")));
- (int32_t)component3 __attribute__((swift_name("component3()")));
- (OnoOnoCommandsRetryTransactionAuthorization *)doCopyClientTransactionId:(NSString *)clientTransactionId attempt:(int32_t)attempt requestID:(int32_t)requestID __attribute__((swift_name("doCopy(clientTransactionId:attempt:requestID:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t attempt __attribute__((swift_name("attempt")));
@property (readonly) NSString *clientTransactionId __attribute__((swift_name("clientTransactionId")));
@property (readonly) int32_t requestID __attribute__((swift_name("requestID")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoCommands.ServiceNotReady")))
@interface OnoOnoCommandsServiceNotReady : OnoCommand
- (instancetype)initWithErrorDetails:(OnoErrorDetails *)errorDetails __attribute__((swift_name("init(errorDetails:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoErrorDetails *)component1 __attribute__((swift_name("component1()")));
- (OnoOnoCommandsServiceNotReady *)doCopyErrorDetails:(OnoErrorDetails *)errorDetails __attribute__((swift_name("doCopy(errorDetails:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoErrorDetails *errorDetails __attribute__((swift_name("errorDetails")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoCommands.ServiceReady")))
@interface OnoOnoCommandsServiceReady : OnoCommand
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoCommands.SubmitSignature")))
@interface OnoOnoCommandsSubmitSignature : OnoCommand
- (instancetype)initWithTransactionId:(NSString *)transactionId signatureBody:(OnoOnoSignatureBody *)signatureBody timeoutSeconds:(int32_t)timeoutSeconds requestID:(int32_t)requestID __attribute__((swift_name("init(transactionId:signatureBody:timeoutSeconds:requestID:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoOnoSignatureBody *)component2 __attribute__((swift_name("component2()")));
- (int32_t)component3 __attribute__((swift_name("component3()")));
- (int32_t)component4 __attribute__((swift_name("component4()")));
- (OnoOnoCommandsSubmitSignature *)doCopyTransactionId:(NSString *)transactionId signatureBody:(OnoOnoSignatureBody *)signatureBody timeoutSeconds:(int32_t)timeoutSeconds requestID:(int32_t)requestID __attribute__((swift_name("doCopy(transactionId:signatureBody:timeoutSeconds:requestID:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t requestID __attribute__((swift_name("requestID")));
@property (readonly) OnoOnoSignatureBody *signatureBody __attribute__((swift_name("signatureBody")));
@property (readonly) int32_t timeoutSeconds __attribute__((swift_name("timeoutSeconds")));
@property (readonly) NSString *transactionId __attribute__((swift_name("transactionId")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoCommands.UpdateBaseURL")))
@interface OnoOnoCommandsUpdateBaseURL : OnoCommand
- (instancetype)initWithBaseURL:(NSString *)baseURL __attribute__((swift_name("init(baseURL:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoOnoCommandsUpdateBaseURL *)doCopyBaseURL:(NSString *)baseURL __attribute__((swift_name("doCopy(baseURL:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *baseURL __attribute__((swift_name("baseURL")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoCommands.UpdateMerchantIdentity")))
@interface OnoOnoCommandsUpdateMerchantIdentity : OnoCommand
- (instancetype)initWithId:(NSString *)id __attribute__((swift_name("init(id:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoOnoCommandsUpdateMerchantIdentity *)doCopyId:(NSString *)id __attribute__((swift_name("doCopy(id:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *id __attribute__((swift_name("id")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoCommands.UpdateMerchantSecret")))
@interface OnoOnoCommandsUpdateMerchantSecret : OnoCommand
- (instancetype)initWithSecret:(OnoSensitiveString *)secret __attribute__((swift_name("init(secret:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoSensitiveString *)component1 __attribute__((swift_name("component1()")));
- (OnoOnoCommandsUpdateMerchantSecret *)doCopySecret:(OnoSensitiveString *)secret __attribute__((swift_name("doCopy(secret:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoSensitiveString *secret __attribute__((swift_name("secret")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoModuleUtils")))
@interface OnoOnoModuleUtils : OnoBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)onoModuleUtils __attribute__((swift_name("init()")));
- (NSString * _Nullable)getBinFromMaskedPanMaskedPan:(NSString * _Nullable)maskedPan __attribute__((swift_name("getBinFromMaskedPan(maskedPan:)")));
- (NSString *)getNetworkRequestErrorMessageStatusCode:(OnoInt * _Nullable)statusCode errorMessage:(NSString * _Nullable)errorMessage __attribute__((swift_name("getNetworkRequestErrorMessage(statusCode:errorMessage:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoStateData")))
@interface OnoOnoStateData : OnoBase <OnoStoreData>
- (instancetype)initWithCurrentRequestID:(int32_t)currentRequestID baseURL:(NSString * _Nullable)baseURL merchantId:(NSString * _Nullable)merchantId merchantToken:(OnoSensitiveString * _Nullable)merchantToken networkPostRetrySchedule:(NSArray<OnoInt *> *)networkPostRetrySchedule busyFinalizingTransactionId:(NSString * _Nullable)busyFinalizingTransactionId busySubmittingSignatureForTransactionId:(NSString * _Nullable)busySubmittingSignatureForTransactionId unhandledApiTriesBeforeBaseUrlConfig:(int32_t)unhandledApiTriesBeforeBaseUrlConfig isFetchingToken:(BOOL)isFetchingToken __attribute__((swift_name("init(currentRequestID:baseURL:merchantId:merchantToken:networkPostRetrySchedule:busyFinalizingTransactionId:busySubmittingSignatureForTransactionId:unhandledApiTriesBeforeBaseUrlConfig:isFetchingToken:)"))) __attribute__((objc_designated_initializer));
- (int32_t)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (NSString * _Nullable)component3 __attribute__((swift_name("component3()")));
- (OnoSensitiveString * _Nullable)component4 __attribute__((swift_name("component4()")));
- (NSArray<OnoInt *> *)component5 __attribute__((swift_name("component5()")));
- (NSString * _Nullable)component6 __attribute__((swift_name("component6()")));
- (NSString * _Nullable)component7 __attribute__((swift_name("component7()")));
- (int32_t)component8 __attribute__((swift_name("component8()")));
- (BOOL)component9 __attribute__((swift_name("component9()")));
- (OnoOnoStateData *)doCopyCurrentRequestID:(int32_t)currentRequestID baseURL:(NSString * _Nullable)baseURL merchantId:(NSString * _Nullable)merchantId merchantToken:(OnoSensitiveString * _Nullable)merchantToken networkPostRetrySchedule:(NSArray<OnoInt *> *)networkPostRetrySchedule busyFinalizingTransactionId:(NSString * _Nullable)busyFinalizingTransactionId busySubmittingSignatureForTransactionId:(NSString * _Nullable)busySubmittingSignatureForTransactionId unhandledApiTriesBeforeBaseUrlConfig:(int32_t)unhandledApiTriesBeforeBaseUrlConfig isFetchingToken:(BOOL)isFetchingToken __attribute__((swift_name("doCopy(currentRequestID:baseURL:merchantId:merchantToken:networkPostRetrySchedule:busyFinalizingTransactionId:busySubmittingSignatureForTransactionId:unhandledApiTriesBeforeBaseUrlConfig:isFetchingToken:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable baseURL __attribute__((swift_name("baseURL")));
@property (readonly) NSString * _Nullable busyFinalizingTransactionId __attribute__((swift_name("busyFinalizingTransactionId")));
@property (readonly) NSString * _Nullable busySubmittingSignatureForTransactionId __attribute__((swift_name("busySubmittingSignatureForTransactionId")));
@property (readonly) int32_t currentRequestID __attribute__((swift_name("currentRequestID")));
@property (readonly) BOOL isFetchingToken __attribute__((swift_name("isFetchingToken")));
@property (readonly) NSString * _Nullable merchantId __attribute__((swift_name("merchantId")));
@property (readonly) OnoSensitiveString * _Nullable merchantToken __attribute__((swift_name("merchantToken")));
@property (readonly) NSArray<OnoInt *> *networkPostRetrySchedule __attribute__((swift_name("networkPostRetrySchedule")));
@property (readonly) int32_t unhandledApiTriesBeforeBaseUrlConfig __attribute__((swift_name("unhandledApiTriesBeforeBaseUrlConfig")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoStateMachine")))
@interface OnoOnoStateMachine : OnoBase <OnoStateMachine>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)onoStateMachine __attribute__((swift_name("init()")));
- (OnoOnoStateData *)castStoreDataStoreData:(id<OnoStoreData>)storeData __attribute__((swift_name("castStoreData(storeData:)")));
- (OnoStateMachineResult *)consumeActionStoreData:(id<OnoStoreData>)storeData action:(OnoAction *)action __attribute__((swift_name("consumeAction(storeData:action:)")));
- (OnoOnoStateData *)getCurrentStateStore:(OnoStore *)store __attribute__((swift_name("getCurrentState(store:)")));
- (OnoOnoStateData *)getCurrentStateState:(NSDictionary<NSString *, id<OnoStoreData>> *)state __attribute__((swift_name("getCurrentState(state:)")));
- (OnoOnoStateData *)getStoreDataState:(NSDictionary<NSString *, id<OnoStoreData>> *)state __attribute__((swift_name("getStoreData(state:)")));
@property (readonly) OnoOnoStateData *initialState __attribute__((swift_name("initialState")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoMetaDataKeys")))
@interface OnoOnoMetaDataKeys : OnoKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) OnoOnoMetaDataKeys *integrator __attribute__((swift_name("integrator")));
@property (class, readonly) OnoOnoMetaDataKeys *integratorUuid __attribute__((swift_name("integratorUuid")));
@property (class, readonly) OnoOnoMetaDataKeys *tipAmountInCents __attribute__((swift_name("tipAmountInCents")));
- (int32_t)compareToOther:(OnoOnoMetaDataKeys *)other __attribute__((swift_name("compareTo(other:)")));
@property (readonly) NSString *value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UpdateInstallationType")))
@interface OnoUpdateInstallationType : OnoKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) OnoUpdateInstallationType *automatic __attribute__((swift_name("automatic")));
@property (class, readonly) OnoUpdateInstallationType *required __attribute__((swift_name("required")));
@property (class, readonly) OnoUpdateInstallationType *optional __attribute__((swift_name("optional")));
- (int32_t)compareToOther:(OnoUpdateInstallationType *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((swift_name("OnoApiServiceProtocol")))
@interface OnoOnoApiServiceProtocol : OnoDispatcher
- (instancetype)initWithDispatch:(void (^)(OnoAction *))dispatch __attribute__((swift_name("init(dispatch:)"))) __attribute__((objc_designated_initializer));
- (void)authorizeTransactionAuthorizationBody:(OnoOnoAuthorizationBody *)authorizationBody bin:(NSString * _Nullable)bin requestID:(int32_t)requestID __attribute__((swift_name("authorizeTransaction(authorizationBody:bin:requestID:)")));
- (void)baseURLUpdatedBaseUrl:(NSString *)baseUrl __attribute__((swift_name("baseURLUpdated(baseUrl:)")));
- (void)chargeTransactionChargeTransactionBody:(OnoOnoChargeTransactionBody *)chargeTransactionBody requestID:(int32_t)requestID onoAuthToken:(OnoSensitiveString * _Nullable)onoAuthToken __attribute__((swift_name("chargeTransaction(chargeTransactionBody:requestID:onoAuthToken:)")));
- (void)checkForUpdatesTerminal:(OnoTerminal *)terminal requestID:(int32_t)requestID __attribute__((swift_name("checkForUpdates(terminal:requestID:)")));
- (void)clearMerchant __attribute__((swift_name("clearMerchant()")));
- (void)clearMerchantSecret __attribute__((swift_name("clearMerchantSecret()")));
- (void)confirmTransactionTransactionId:(NSString *)transactionId confirmationBody:(OnoOnoConfirmationBody *)confirmationBody requestID:(int32_t)requestID __attribute__((swift_name("confirmTransaction(transactionId:confirmationBody:requestID:)")));
- (void)delaySilentRejectCallTransaction:(OnoTransaction *)transaction attempt:(int32_t)attempt __attribute__((swift_name("delaySilentRejectCall(transaction:attempt:)")));
- (void)finalizeTransactionTransactionId:(NSString *)transactionId transactionState:(OnoTransactionState *)transactionState finalizeState:(OnoFinalizeState *)finalizeState finalizationBody:(OnoOnoFinalizationBody *)finalizationBody timeoutSeconds:(int32_t)timeoutSeconds requestID:(int32_t)requestID __attribute__((swift_name("finalizeTransaction(transactionId:transactionState:finalizeState:finalizationBody:timeoutSeconds:requestID:)")));
- (void)merchantIdentityUpdatedId:(NSString *)id __attribute__((swift_name("merchantIdentityUpdated(id:)")));
- (void)merchantSecretUpdatedSecret:(OnoSensitiveString *)secret __attribute__((swift_name("merchantSecretUpdated(secret:)")));
- (void)pollChargeStatusClientTransactionId:(NSString *)clientTransactionId requestID:(int32_t)requestID __attribute__((swift_name("pollChargeStatus(clientTransactionId:requestID:)")));
- (void)postRkiRequestTerminal:(OnoTerminal *)terminal rkiRequestMap:(OnoSensitiveMap *)rkiRequestMap requestID:(int32_t)requestID attempt:(int32_t)attempt __attribute__((swift_name("postRkiRequest(terminal:rkiRequestMap:requestID:attempt:)")));
- (void)rejectTransactionTransactionId:(NSString *)transactionId rejectionBody:(OnoOnoConfirmationBody *)rejectionBody requestID:(int32_t)requestID __attribute__((swift_name("rejectTransaction(transactionId:rejectionBody:requestID:)")));
- (void)rejectTransactionByClientTransactionIdClientTransactionId:(NSString *)clientTransactionId rejectionBody:(OnoOnoRejectByClientIdBody *)rejectionBody requestID:(int32_t)requestID __attribute__((swift_name("rejectTransactionByClientTransactionId(clientTransactionId:rejectionBody:requestID:)")));
- (void)retryRkiServerRequestTerminal:(OnoTerminal *)terminal rkiRequestMap:(OnoSensitiveMap *)rkiRequestMap requestID:(int32_t)requestID attempt:(int32_t)attempt __attribute__((swift_name("retryRkiServerRequest(terminal:rkiRequestMap:requestID:attempt:)")));
- (void)retryTransactionAuthorizationClientTransactionId:(NSString *)clientTransactionId attempt:(int32_t)attempt requestID:(int32_t)requestID __attribute__((swift_name("retryTransactionAuthorization(clientTransactionId:attempt:requestID:)")));
- (void)silentRejectTransactionTransaction:(OnoTransaction *)transaction attempt:(int32_t)attempt __attribute__((swift_name("silentRejectTransaction(transaction:attempt:)")));
- (void)submitTransactionSignatureTransactionId:(NSString *)transactionId signatureBody:(OnoOnoSignatureBody *)signatureBody timeoutSeconds:(int32_t)timeoutSeconds requestID:(int32_t)requestID __attribute__((swift_name("submitTransactionSignature(transactionId:signatureBody:timeoutSeconds:requestID:)")));
- (void)updateClientTransactionIdClientTransactionId:(NSString * _Nullable)clientTransactionId __attribute__((swift_name("updateClientTransactionId(clientTransactionId:)")));
@end;

__attribute__((swift_name("MockOnoApiService")))
@interface OnoMockOnoApiService : OnoOnoApiServiceProtocol
- (instancetype)initWithDispatch:(void (^)(OnoAction *))dispatch __attribute__((swift_name("init(dispatch:)"))) __attribute__((objc_designated_initializer));
- (void)authorizeTransactionAuthorizationBody:(OnoOnoAuthorizationBody *)authorizationBody bin:(NSString * _Nullable)bin requestID:(int32_t)requestID __attribute__((swift_name("authorizeTransaction(authorizationBody:bin:requestID:)")));
- (void)baseURLUpdatedBaseUrl:(NSString *)baseUrl __attribute__((swift_name("baseURLUpdated(baseUrl:)")));
- (void)chargeTransactionChargeTransactionBody:(OnoOnoChargeTransactionBody *)onoChargeTransactionBody requestID:(int32_t)requestID onoAuthToken:(OnoSensitiveString * _Nullable)onoAuthToken __attribute__((swift_name("chargeTransaction(chargeTransactionBody:requestID:onoAuthToken:)")));
- (void)checkForUpdatesTerminal:(OnoTerminal *)terminal requestID:(int32_t)requestID __attribute__((swift_name("checkForUpdates(terminal:requestID:)")));
- (void)clearMerchant __attribute__((swift_name("clearMerchant()")));
- (void)clearMerchantSecret __attribute__((swift_name("clearMerchantSecret()")));
- (void)confirmTransactionTransactionId:(NSString *)transactionId confirmationBody:(OnoOnoConfirmationBody *)confirmationBody requestID:(int32_t)requestID __attribute__((swift_name("confirmTransaction(transactionId:confirmationBody:requestID:)")));
- (void)delaySilentRejectCallTransaction:(OnoTransaction *)transaction attempt:(int32_t)attempt __attribute__((swift_name("delaySilentRejectCall(transaction:attempt:)")));
- (void)dequeueAllSuspendedActions __attribute__((swift_name("dequeueAllSuspendedActions()")));
- (void)dequeueFirst __attribute__((swift_name("dequeueFirst()")));
- (void)finalizeTransactionTransactionId:(NSString *)transactionId transactionState:(OnoTransactionState *)transactionState finalizeState:(OnoFinalizeState *)finalizeState finalizationBody:(OnoOnoFinalizationBody *)finalizationBody timeoutSeconds:(int32_t)timeoutSeconds requestID:(int32_t)requestID __attribute__((swift_name("finalizeTransaction(transactionId:transactionState:finalizeState:finalizationBody:timeoutSeconds:requestID:)")));
- (void)merchantIdentityUpdatedId:(NSString *)id __attribute__((swift_name("merchantIdentityUpdated(id:)")));
- (void)merchantSecretUpdatedSecret:(OnoSensitiveString *)secret __attribute__((swift_name("merchantSecretUpdated(secret:)")));
- (void)pollChargeStatusClientTransactionId:(NSString *)clientTransactionId requestID:(int32_t)requestID __attribute__((swift_name("pollChargeStatus(clientTransactionId:requestID:)")));
- (void)postRkiRequestTerminal:(OnoTerminal *)terminal rkiRequestMap:(OnoSensitiveMap *)rkiRequestMap requestID:(int32_t)requestID attempt:(int32_t)attempt __attribute__((swift_name("postRkiRequest(terminal:rkiRequestMap:requestID:attempt:)")));
- (void)rejectTransactionTransactionId:(NSString *)transactionId rejectionBody:(OnoOnoConfirmationBody *)rejectionBody requestID:(int32_t)requestID __attribute__((swift_name("rejectTransaction(transactionId:rejectionBody:requestID:)")));
- (void)rejectTransactionByClientTransactionIdClientTransactionId:(NSString *)clientTransactionId rejectionBody:(OnoOnoRejectByClientIdBody *)rejectionBody requestID:(int32_t)requestID __attribute__((swift_name("rejectTransactionByClientTransactionId(clientTransactionId:rejectionBody:requestID:)")));
- (void)retryRkiServerRequestTerminal:(OnoTerminal *)terminal rkiRequestMap:(OnoSensitiveMap *)rkiRequestMap requestID:(int32_t)requestID attempt:(int32_t)attempt __attribute__((swift_name("retryRkiServerRequest(terminal:rkiRequestMap:requestID:attempt:)")));
- (void)retryTransactionAuthorizationClientTransactionId:(NSString *)clientTransactionId attempt:(int32_t)attempt requestID:(int32_t)requestID __attribute__((swift_name("retryTransactionAuthorization(clientTransactionId:attempt:requestID:)")));
- (void)silentRejectTransactionTransaction:(OnoTransaction *)transaction attempt:(int32_t)attempt __attribute__((swift_name("silentRejectTransaction(transaction:attempt:)")));
- (void)submitTransactionSignatureTransactionId:(NSString *)transactionId signatureBody:(OnoOnoSignatureBody *)signatureBody timeoutSeconds:(int32_t)timeoutSeconds requestID:(int32_t)requestID __attribute__((swift_name("submitTransactionSignature(transactionId:signatureBody:timeoutSeconds:requestID:)")));
- (OnoTransactionActionsAuthorizationResponse *)successfulAuthorizationAction __attribute__((swift_name("successfulAuthorizationAction()")));
- (void)updateClientTransactionIdClientTransactionId:(NSString * _Nullable)clientTransactionId __attribute__((swift_name("updateClientTransactionId(clientTransactionId:)")));
@property BOOL authResponseExcludeAPRC __attribute__((swift_name("authResponseExcludeAPRC")));
@property OnoMockOnoApiServiceCompanionMockNetworkOrServerError * _Nullable authorizationSeverOrNetworkError __attribute__((swift_name("authorizationSeverOrNetworkError")));
@property NSArray<OnoAvailableUpdate *> *availableUpdates __attribute__((swift_name("availableUpdates")));
@property (readonly) NSString * _Nullable baseUrl __attribute__((swift_name("baseUrl")));
@property (readonly) NSString * _Nullable clientTransactionId __attribute__((swift_name("clientTransactionId")));
@property BOOL declineAuthorization __attribute__((swift_name("declineAuthorization")));
@property BOOL failCheckForUpdates __attribute__((swift_name("failCheckForUpdates")));
@property BOOL failFinalizeCalls __attribute__((swift_name("failFinalizeCalls")));
@property int32_t failNextXRejectCalls __attribute__((swift_name("failNextXRejectCalls")));
@property OnoInt * _Nullable failRejectResponseCode __attribute__((swift_name("failRejectResponseCode")));
@property BOOL failRetryTransactionAuthorization __attribute__((swift_name("failRetryTransactionAuthorization")));
@property BOOL failSignatureSubmission __attribute__((swift_name("failSignatureSubmission")));
@property int32_t finalizeResponseCode __attribute__((swift_name("finalizeResponseCode")));
@property BOOL keepRetryingTransactionAuthorization __attribute__((swift_name("keepRetryingTransactionAuthorization")));
@property NSString *limitedLifeAuthorizationResponseCode __attribute__((swift_name("limitedLifeAuthorizationResponseCode")));
@property (readonly) NSString * _Nullable merchantIdentity __attribute__((swift_name("merchantIdentity")));
@property (readonly) OnoSensitiveString * _Nullable merchantSecret __attribute__((swift_name("merchantSecret")));
@property BOOL requireSignature __attribute__((swift_name("requireSignature")));
@property int32_t retryNextXRkiServerRequestCalls __attribute__((swift_name("retryNextXRkiServerRequestCalls")));
@property BOOL retryTransactionAuthorization __attribute__((swift_name("retryTransactionAuthorization")));
@property OnoSensitiveMap * _Nullable rkiResponseData __attribute__((swift_name("rkiResponseData")));
@property int32_t signatureSubmissionResponseCode __attribute__((swift_name("signatureSubmissionResponseCode")));
@property BOOL suspendResponseDispatches __attribute__((swift_name("suspendResponseDispatches")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MockOnoApiService.Companion")))
@interface OnoMockOnoApiServiceCompanion : OnoBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (readonly) NSString *TRANSACTION_ID __attribute__((swift_name("TRANSACTION_ID")));
@property (readonly) OnoTransactionCommandsFinalizeTransaction *approvedFinalizationTransactionCommand __attribute__((swift_name("approvedFinalizationTransactionCommand")));
@property (readonly) OnoTransaction *chipTransaction __attribute__((swift_name("chipTransaction")));
@property (readonly) OnoTransactionActionsAuthorizationResponse *declinedAuthorizationAction __attribute__((swift_name("declinedAuthorizationAction")));
@property (readonly) OnoSignature *signature __attribute__((swift_name("signature")));
@property (readonly) OnoTransactionCommandsSubmitSignature *submitTransactionSignatureCommand __attribute__((swift_name("submitTransactionSignatureCommand")));
@property (readonly) OnoTransactionActionsAuthorizationResponse *successfulAuthorizationAction __attribute__((swift_name("successfulAuthorizationAction")));
@property (readonly) OnoTerminal *terminalWithUpdates __attribute__((swift_name("terminalWithUpdates")));
@property (readonly) OnoTransaction *transaction __attribute__((swift_name("transaction")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MockOnoApiService.CompanionMockNetworkOrServerError")))
@interface OnoMockOnoApiServiceCompanionMockNetworkOrServerError : OnoBase
- (instancetype)initWithMessage:(NSString *)message httpStatusCode:(OnoInt * _Nullable)httpStatusCode __attribute__((swift_name("init(message:httpStatusCode:)"))) __attribute__((objc_designated_initializer));
@property (readonly) OnoInt * _Nullable httpStatusCode __attribute__((swift_name("httpStatusCode")));
@property (readonly) NSString *message __attribute__((swift_name("message")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ChargeResult")))
@interface OnoChargeResult : OnoKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) OnoChargeResult *chargeError __attribute__((swift_name("chargeError")));
@property (class, readonly) OnoChargeResult *chargePending __attribute__((swift_name("chargePending")));
@property (class, readonly) OnoChargeResult *chargeApproved __attribute__((swift_name("chargeApproved")));
@property (class, readonly) OnoChargeResult *chargeDeclined __attribute__((swift_name("chargeDeclined")));
@property (class, readonly) OnoChargeResult *chargeCancelled __attribute__((swift_name("chargeCancelled")));
- (int32_t)compareToOther:(OnoChargeResult *)other __attribute__((swift_name("compareTo(other:)")));
@property (readonly) NSString *value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ChargeResult.Companion")))
@interface OnoChargeResultCompanion : OnoBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (OnoChargeResult * _Nullable)fromStringValueValue:(NSString *)value __attribute__((swift_name("fromStringValue(value:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoMagicValues")))
@interface OnoOnoMagicValues : OnoBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)onoMagicValues __attribute__((swift_name("init()")));
@property (readonly) NSString *TRANSACTION_ID __attribute__((swift_name("TRANSACTION_ID")));
@property (readonly) NSString *declinedTransactionARPC __attribute__((swift_name("declinedTransactionARPC")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoTransactionUtils")))
@interface OnoOnoTransactionUtils : OnoBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)onoTransactionUtils __attribute__((swift_name("init()")));
- (OnoCardPromptMode * _Nullable)getTerminalCompatibleCardPromptModeTerminalModel:(OnoTerminalModel *)terminalModel proposedCardPresentationMode:(OnoCardPromptMode *)proposedCardPresentationMode __attribute__((swift_name("getTerminalCompatibleCardPromptMode(terminalModel:proposedCardPresentationMode:)")));
- (BOOL)isArpcRequiredTerminalModel:(OnoTerminalModel * _Nullable)terminalModel transactionType:(OnoTransactionType * _Nullable)transactionType __attribute__((swift_name("isArpcRequired(terminalModel:transactionType:)")));
- (BOOL)shouldAskForPinServiceCode:(NSString *)serviceCode __attribute__((swift_name("shouldAskForPin(serviceCode:)")));
- (BOOL)shouldAttemptChipServiceCode:(NSString *)serviceCode __attribute__((swift_name("shouldAttemptChip(serviceCode:)")));
@end;

__attribute__((swift_name("TransactionActions")))
@interface OnoTransactionActions : OnoBase
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionActions.AbortTransaction")))
@interface OnoTransactionActionsAbortTransaction : OnoAction
- (instancetype)initWithMessage:(NSString *)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoTransactionActionsAbortTransaction *)doCopyMessage:(NSString *)message __attribute__((swift_name("doCopy(message:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *message __attribute__((swift_name("message")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionActions.AuthorizationData")))
@interface OnoTransactionActionsAuthorizationData : OnoAction
- (instancetype)initWithAuthorizationTLV:(OnoSensitiveString * _Nullable)authorizationTLV tradeData:(OnoSensitiveMap * _Nullable)tradeData maskedPan:(OnoSensitiveString * _Nullable)maskedPan __attribute__((swift_name("init(authorizationTLV:tradeData:maskedPan:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoSensitiveString * _Nullable)component1 __attribute__((swift_name("component1()")));
- (OnoSensitiveMap * _Nullable)component2 __attribute__((swift_name("component2()")));
- (OnoSensitiveString * _Nullable)component3 __attribute__((swift_name("component3()")));
- (OnoTransactionActionsAuthorizationData *)doCopyAuthorizationTLV:(OnoSensitiveString * _Nullable)authorizationTLV tradeData:(OnoSensitiveMap * _Nullable)tradeData maskedPan:(OnoSensitiveString * _Nullable)maskedPan __attribute__((swift_name("doCopy(authorizationTLV:tradeData:maskedPan:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoSensitiveString * _Nullable authorizationTLV __attribute__((swift_name("authorizationTLV")));
@property (readonly) OnoSensitiveString * _Nullable maskedPan __attribute__((swift_name("maskedPan")));
@property (readonly) OnoSensitiveMap * _Nullable tradeData __attribute__((swift_name("tradeData")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionActions.AuthorizationFailed")))
@interface OnoTransactionActionsAuthorizationFailed : OnoAction
- (instancetype)initWithErrorDetails:(OnoErrorDetails *)errorDetails maybeOnoTransactionId:(NSString * _Nullable)maybeOnoTransactionId __attribute__((swift_name("init(errorDetails:maybeOnoTransactionId:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoErrorDetails *)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (OnoTransactionActionsAuthorizationFailed *)doCopyErrorDetails:(OnoErrorDetails *)errorDetails maybeOnoTransactionId:(NSString * _Nullable)maybeOnoTransactionId __attribute__((swift_name("doCopy(errorDetails:maybeOnoTransactionId:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoErrorDetails *errorDetails __attribute__((swift_name("errorDetails")));
@property (readonly) NSString * _Nullable maybeOnoTransactionId __attribute__((swift_name("maybeOnoTransactionId")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionActions.AuthorizationResponse")))
@interface OnoTransactionActionsAuthorizationResponse : OnoAction
- (instancetype)initWithTransactionID:(NSString *)transactionID isoResponse:(NSString *)isoResponse chargeResult:(NSString *)chargeResult chargeResultMessage:(NSString * _Nullable)chargeResultMessage arpc:(OnoSensitiveString * _Nullable)arpc signatureRequired:(OnoBoolean * _Nullable)signatureRequired __attribute__((swift_name("init(transactionID:isoResponse:chargeResult:chargeResultMessage:arpc:signatureRequired:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (NSString * _Nullable)component4 __attribute__((swift_name("component4()")));
- (OnoSensitiveString * _Nullable)component5 __attribute__((swift_name("component5()")));
- (OnoBoolean * _Nullable)component6 __attribute__((swift_name("component6()")));
- (OnoTransactionActionsAuthorizationResponse *)doCopyTransactionID:(NSString *)transactionID isoResponse:(NSString *)isoResponse chargeResult:(NSString *)chargeResult chargeResultMessage:(NSString * _Nullable)chargeResultMessage arpc:(OnoSensitiveString * _Nullable)arpc signatureRequired:(OnoBoolean * _Nullable)signatureRequired __attribute__((swift_name("doCopy(transactionID:isoResponse:chargeResult:chargeResultMessage:arpc:signatureRequired:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (OnoAuthorizationResponseData * _Nullable)getResponseData __attribute__((swift_name("getResponseData()")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoSensitiveString * _Nullable arpc __attribute__((swift_name("arpc")));
@property (readonly) NSString *chargeResult __attribute__((swift_name("chargeResult")));
@property (readonly) NSString * _Nullable chargeResultMessage __attribute__((swift_name("chargeResultMessage")));
@property (readonly) NSString *isoResponse __attribute__((swift_name("isoResponse")));
@property (readonly) OnoBoolean * _Nullable signatureRequired __attribute__((swift_name("signatureRequired")));
@property (readonly) NSString *transactionID __attribute__((swift_name("transactionID")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionActions.AwaitingCard")))
@interface OnoTransactionActionsAwaitingCard : OnoAction
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
@property (readonly) NSString * _Nullable message __attribute__((swift_name("message")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionActions.AwaitingEMVSelection")))
@interface OnoTransactionActionsAwaitingEMVSelection : OnoAction
- (instancetype)initWithEmvApplicationsList:(NSMutableArray<NSString *> * _Nullable)emvApplicationsList __attribute__((swift_name("init(emvApplicationsList:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSMutableArray<NSString *> * _Nullable)component1 __attribute__((swift_name("component1()")));
- (OnoTransactionActionsAwaitingEMVSelection *)doCopyEmvApplicationsList:(NSMutableArray<NSString *> * _Nullable)emvApplicationsList __attribute__((swift_name("doCopy(emvApplicationsList:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSMutableArray<NSString *> * _Nullable emvApplicationsList __attribute__((swift_name("emvApplicationsList")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionActions.AwaitingPinEntry")))
@interface OnoTransactionActionsAwaitingPinEntry : OnoAction
- (instancetype)initWithIsLastPinEntry:(BOOL)isLastPinEntry message:(NSString * _Nullable)message __attribute__((swift_name("init(isLastPinEntry:message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (BOOL)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (OnoTransactionActionsAwaitingPinEntry *)doCopyIsLastPinEntry:(BOOL)isLastPinEntry message:(NSString * _Nullable)message __attribute__((swift_name("doCopy(isLastPinEntry:message:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL isLastPinEntry __attribute__((swift_name("isLastPinEntry")));
@property (readonly) NSString * _Nullable message __attribute__((swift_name("message")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionActions.CardPresented")))
@interface OnoTransactionActionsCardPresented : OnoAction
- (instancetype)initWithTransactionType:(OnoTransactionType *)transactionType message:(NSString * _Nullable)message __attribute__((swift_name("init(transactionType:message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoTransactionType *)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (OnoTransactionActionsCardPresented *)doCopyTransactionType:(OnoTransactionType *)transactionType message:(NSString * _Nullable)message __attribute__((swift_name("doCopy(transactionType:message:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable message __attribute__((swift_name("message")));
@property (readonly) OnoTransactionType *transactionType __attribute__((swift_name("transactionType")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionActions.CheckSignatureRequirement")))
@interface OnoTransactionActionsCheckSignatureRequirement : OnoAction
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionActions.CheckSwitchFallbackRequired")))
@interface OnoTransactionActionsCheckSwitchFallbackRequired : OnoAction
- (instancetype)initWithIsoResponseCode:(NSString *)isoResponseCode chargeResultMessage:(NSString * _Nullable)chargeResultMessage __attribute__((swift_name("init(isoResponseCode:chargeResultMessage:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (OnoTransactionActionsCheckSwitchFallbackRequired *)doCopyIsoResponseCode:(NSString *)isoResponseCode chargeResultMessage:(NSString * _Nullable)chargeResultMessage __attribute__((swift_name("doCopy(isoResponseCode:chargeResultMessage:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable chargeResultMessage __attribute__((swift_name("chargeResultMessage")));
@property (readonly) NSString *isoResponseCode __attribute__((swift_name("isoResponseCode")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionActions.ChipCardSwiped")))
@interface OnoTransactionActionsChipCardSwiped : OnoAction
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionActions.EMVAppSelectedOnApp")))
@interface OnoTransactionActionsEMVAppSelectedOnApp : OnoAction
- (instancetype)initWithEmpOption:(int32_t)empOption __attribute__((swift_name("init(empOption:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (int32_t)component1 __attribute__((swift_name("component1()")));
- (OnoTransactionActionsEMVAppSelectedOnApp *)doCopyEmpOption:(int32_t)empOption __attribute__((swift_name("doCopy(empOption:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t empOption __attribute__((swift_name("empOption")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionActions.EMVAppSelectedOnReader")))
@interface OnoTransactionActionsEMVAppSelectedOnReader : OnoAction
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionActions.FinalizationFailed")))
@interface OnoTransactionActionsFinalizationFailed : OnoAction
- (instancetype)initWithMessage:(NSString *)message retryAvailable:(BOOL)retryAvailable attempts:(int32_t)attempts __attribute__((swift_name("init(message:retryAvailable:attempts:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithErrorDetails:(OnoErrorDetails *)errorDetails retryAvailable:(BOOL)retryAvailable attempts:(int32_t)attempts __attribute__((swift_name("init(errorDetails:retryAvailable:attempts:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoErrorDetails *)component1 __attribute__((swift_name("component1()")));
- (BOOL)component2 __attribute__((swift_name("component2()")));
- (int32_t)component3 __attribute__((swift_name("component3()")));
- (OnoTransactionActionsFinalizationFailed *)doCopyErrorDetails:(OnoErrorDetails *)errorDetails retryAvailable:(BOOL)retryAvailable attempts:(int32_t)attempts __attribute__((swift_name("doCopy(errorDetails:retryAvailable:attempts:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t attempts __attribute__((swift_name("attempts")));
@property (readonly) OnoErrorDetails *errorDetails __attribute__((swift_name("errorDetails")));
@property (readonly) BOOL retryAvailable __attribute__((swift_name("retryAvailable")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionActions.FinalizationResponse")))
@interface OnoTransactionActionsFinalizationResponse : OnoAction
- (instancetype)initWithTransactionState:(OnoTransactionState *)transactionState message:(NSString * _Nullable)message __attribute__((swift_name("init(transactionState:message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoTransactionState *)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (OnoTransactionActionsFinalizationResponse *)doCopyTransactionState:(OnoTransactionState *)transactionState message:(NSString * _Nullable)message __attribute__((swift_name("doCopy(transactionState:message:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable message __attribute__((swift_name("message")));
@property (readonly) OnoTransactionState *transactionState __attribute__((swift_name("transactionState")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionActions.FinalizationTLV")))
@interface OnoTransactionActionsFinalizationTLV : OnoAction
- (instancetype)initWithFinalizationTLV:(OnoSensitiveString *)finalizationTLV isReversalTLV:(BOOL)isReversalTLV __attribute__((swift_name("init(finalizationTLV:isReversalTLV:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoSensitiveString *)component1 __attribute__((swift_name("component1()")));
- (BOOL)component2 __attribute__((swift_name("component2()")));
- (OnoTransactionActionsFinalizationTLV *)doCopyFinalizationTLV:(OnoSensitiveString *)finalizationTLV isReversalTLV:(BOOL)isReversalTLV __attribute__((swift_name("doCopy(finalizationTLV:isReversalTLV:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoSensitiveString *finalizationTLV __attribute__((swift_name("finalizationTLV")));
@property (readonly) BOOL isReversalTLV __attribute__((swift_name("isReversalTLV")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionActions.FinalizeTransaction")))
@interface OnoTransactionActionsFinalizeTransaction : OnoAction
- (instancetype)initWithTerminal:(OnoTerminal *)terminal __attribute__((swift_name("init(terminal:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoTerminal *)component1 __attribute__((swift_name("component1()")));
- (OnoTransactionActionsFinalizeTransaction *)doCopyTerminal:(OnoTerminal *)terminal __attribute__((swift_name("doCopy(terminal:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoTerminal *terminal __attribute__((swift_name("terminal")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionActions.InitializeTransaction")))
@interface OnoTransactionActionsInitializeTransaction : OnoAction
- (instancetype)initWithTerminal:(OnoTerminal *)terminal amountInCents:(int64_t)amountInCents clientTransactionUUID:(NSString *)clientTransactionUUID currency:(NSString *)currency fallbackHistory:(NSSet<OnoFallbackEntry *> * _Nullable)fallbackHistory switchFallbackResponseCode:(NSString * _Nullable)switchFallbackResponseCode cardPresentationMode:(OnoCardPromptMode *)cardPresentationMode sdkUniqueTransactionId:(NSString *)sdkUniqueTransactionId initiatedBy:(OnoTransactionInitiator *)initiatedBy tipAmountInCentsIncludedInAmount:(int32_t)tipAmountInCentsIncludedInAmount __attribute__((swift_name("init(terminal:amountInCents:clientTransactionUUID:currency:fallbackHistory:switchFallbackResponseCode:cardPresentationMode:sdkUniqueTransactionId:initiatedBy:tipAmountInCentsIncludedInAmount:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoTerminal *)component1 __attribute__((swift_name("component1()")));
- (int32_t)component10 __attribute__((swift_name("component10()")));
- (int64_t)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (NSSet<OnoFallbackEntry *> * _Nullable)component5 __attribute__((swift_name("component5()")));
- (NSString * _Nullable)component6 __attribute__((swift_name("component6()")));
- (OnoCardPromptMode *)component7 __attribute__((swift_name("component7()")));
- (NSString *)component8 __attribute__((swift_name("component8()")));
- (OnoTransactionInitiator *)component9 __attribute__((swift_name("component9()")));
- (OnoTransactionActionsInitializeTransaction *)doCopyTerminal:(OnoTerminal *)terminal amountInCents:(int64_t)amountInCents clientTransactionUUID:(NSString *)clientTransactionUUID currency:(NSString *)currency fallbackHistory:(NSSet<OnoFallbackEntry *> * _Nullable)fallbackHistory switchFallbackResponseCode:(NSString * _Nullable)switchFallbackResponseCode cardPresentationMode:(OnoCardPromptMode *)cardPresentationMode sdkUniqueTransactionId:(NSString *)sdkUniqueTransactionId initiatedBy:(OnoTransactionInitiator *)initiatedBy tipAmountInCentsIncludedInAmount:(int32_t)tipAmountInCentsIncludedInAmount __attribute__((swift_name("doCopy(terminal:amountInCents:clientTransactionUUID:currency:fallbackHistory:switchFallbackResponseCode:cardPresentationMode:sdkUniqueTransactionId:initiatedBy:tipAmountInCentsIncludedInAmount:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int64_t amountInCents __attribute__((swift_name("amountInCents")));
@property (readonly) OnoCardPromptMode *cardPresentationMode __attribute__((swift_name("cardPresentationMode")));
@property (readonly) NSString *clientTransactionUUID __attribute__((swift_name("clientTransactionUUID")));
@property (readonly) NSString *currency __attribute__((swift_name("currency")));
@property (readonly) NSSet<OnoFallbackEntry *> * _Nullable fallbackHistory __attribute__((swift_name("fallbackHistory")));
@property (readonly) OnoTransactionInitiator *initiatedBy __attribute__((swift_name("initiatedBy")));
@property (readonly) NSString *sdkUniqueTransactionId __attribute__((swift_name("sdkUniqueTransactionId")));
@property (readonly) NSString * _Nullable switchFallbackResponseCode __attribute__((swift_name("switchFallbackResponseCode")));
@property (readonly) OnoTerminal *terminal __attribute__((swift_name("terminal")));
@property (readonly) int32_t tipAmountInCentsIncludedInAmount __attribute__((swift_name("tipAmountInCentsIncludedInAmount")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionActions.Initialized")))
@interface OnoTransactionActionsInitialized : OnoAction
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
@property (readonly) NSString * _Nullable message __attribute__((swift_name("message")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionActions.PinDigitEntered")))
@interface OnoTransactionActionsPinDigitEntered : OnoAction
- (instancetype)initWithEnteredDigits:(int32_t)enteredDigits __attribute__((swift_name("init(enteredDigits:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (int32_t)component1 __attribute__((swift_name("component1()")));
- (OnoTransactionActionsPinDigitEntered *)doCopyEnteredDigits:(int32_t)enteredDigits __attribute__((swift_name("doCopy(enteredDigits:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t enteredDigits __attribute__((swift_name("enteredDigits")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionActions.ReadingCard")))
@interface OnoTransactionActionsReadingCard : OnoAction
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString * _Nullable)component1 __attribute__((swift_name("component1()")));
- (OnoTransactionActionsReadingCard *)doCopyMessage:(NSString * _Nullable)message __attribute__((swift_name("doCopy(message:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable message __attribute__((swift_name("message")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionActions.SignatureProvided")))
@interface OnoTransactionActionsSignatureProvided : OnoAction
- (instancetype)initWithSignature:(OnoSignature *)signature __attribute__((swift_name("init(signature:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoSignature *)component1 __attribute__((swift_name("component1()")));
- (OnoTransactionActionsSignatureProvided *)doCopySignature:(OnoSignature *)signature __attribute__((swift_name("doCopy(signature:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoSignature *signature __attribute__((swift_name("signature")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionActions.SignatureSubmissionFailed")))
@interface OnoTransactionActionsSignatureSubmissionFailed : OnoAction
- (instancetype)initWithErrorDetails:(OnoErrorDetails *)errorDetails canRetry:(OnoBoolean * _Nullable)canRetry __attribute__((swift_name("init(errorDetails:canRetry:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoErrorDetails *)component1 __attribute__((swift_name("component1()")));
- (OnoBoolean * _Nullable)component2 __attribute__((swift_name("component2()")));
- (OnoTransactionActionsSignatureSubmissionFailed *)doCopyErrorDetails:(OnoErrorDetails *)errorDetails canRetry:(OnoBoolean * _Nullable)canRetry __attribute__((swift_name("doCopy(errorDetails:canRetry:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoBoolean * _Nullable canRetry __attribute__((swift_name("canRetry")));
@property (readonly) OnoErrorDetails *errorDetails __attribute__((swift_name("errorDetails")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionActions.SignatureSubmitted")))
@interface OnoTransactionActionsSignatureSubmitted : OnoAction
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionActions.SubmitSignature")))
@interface OnoTransactionActionsSubmitSignature : OnoAction
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionActions.TransactionCancelled")))
@interface OnoTransactionActionsTransactionCancelled : OnoAction
- (instancetype)initWithTerminalManufacturer:(OnoManufacturer *)terminalManufacturer message:(NSString * _Nullable)message cancelledDetails:(OnoErrorDetails * _Nullable)cancelledDetails __attribute__((swift_name("init(terminalManufacturer:message:cancelledDetails:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoManufacturer *)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (OnoErrorDetails * _Nullable)component3 __attribute__((swift_name("component3()")));
- (OnoTransactionActionsTransactionCancelled *)doCopyTerminalManufacturer:(OnoManufacturer *)terminalManufacturer message:(NSString * _Nullable)message cancelledDetails:(OnoErrorDetails * _Nullable)cancelledDetails __attribute__((swift_name("doCopy(terminalManufacturer:message:cancelledDetails:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoErrorDetails * _Nullable cancelledDetails __attribute__((swift_name("cancelledDetails")));
@property (readonly) NSString * _Nullable message __attribute__((swift_name("message")));
@property (readonly) OnoManufacturer *terminalManufacturer __attribute__((swift_name("terminalManufacturer")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionActions.TransactionCompleted")))
@interface OnoTransactionActionsTransactionCompleted : OnoAction
- (instancetype)initWithMethodName:(NSString *)methodName __attribute__((swift_name("init(methodName:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoTransactionActionsTransactionCompleted *)doCopyMethodName:(NSString *)methodName __attribute__((swift_name("doCopy(methodName:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *methodName __attribute__((swift_name("methodName")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionActions.TransactionDeclinedBySwitch")))
@interface OnoTransactionActionsTransactionDeclinedBySwitch : OnoAction
- (instancetype)initWithIsoCode:(NSString *)isoCode chargeMessage:(NSString * _Nullable)chargeMessage __attribute__((swift_name("init(isoCode:chargeMessage:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (OnoTransactionActionsTransactionDeclinedBySwitch *)doCopyIsoCode:(NSString *)isoCode chargeMessage:(NSString * _Nullable)chargeMessage __attribute__((swift_name("doCopy(isoCode:chargeMessage:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable chargeMessage __attribute__((swift_name("chargeMessage")));
@property (readonly) NSString *isoCode __attribute__((swift_name("isoCode")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionActions.TransactionDeclinedByTerminalOrCard")))
@interface OnoTransactionActionsTransactionDeclinedByTerminalOrCard : OnoAction
- (instancetype)initWithMessage:(NSString * _Nullable)message debuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("init(message:debuggingInfo:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString * _Nullable)component1 __attribute__((swift_name("component1()")));
- (id _Nullable)component2 __attribute__((swift_name("component2()")));
- (OnoTransactionActionsTransactionDeclinedByTerminalOrCard *)doCopyMessage:(NSString * _Nullable)message debuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("doCopy(message:debuggingInfo:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) id _Nullable debuggingInfo __attribute__((swift_name("debuggingInfo")));
@property (readonly) NSString * _Nullable message __attribute__((swift_name("message")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionActions.TransactionErrorOccurred")))
@interface OnoTransactionActionsTransactionErrorOccurred : OnoAction
- (instancetype)initWithMessage:(NSString *)message terminalManufacturer:(OnoManufacturer * _Nullable)terminalManufacturer debuggingInfo:(id _Nullable)debuggingInfo shouldAbortTransaction:(BOOL)shouldAbortTransaction __attribute__((swift_name("init(message:terminalManufacturer:debuggingInfo:shouldAbortTransaction:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithErrorDetails:(OnoErrorDetails *)errorDetails terminalManufacturer:(OnoManufacturer * _Nullable)terminalManufacturer shouldAbortTransaction:(BOOL)shouldAbortTransaction __attribute__((swift_name("init(errorDetails:terminalManufacturer:shouldAbortTransaction:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoErrorDetails *)component1 __attribute__((swift_name("component1()")));
- (OnoManufacturer * _Nullable)component2 __attribute__((swift_name("component2()")));
- (BOOL)component3 __attribute__((swift_name("component3()")));
- (OnoTransactionActionsTransactionErrorOccurred *)doCopyErrorDetails:(OnoErrorDetails *)errorDetails terminalManufacturer:(OnoManufacturer * _Nullable)terminalManufacturer shouldAbortTransaction:(BOOL)shouldAbortTransaction __attribute__((swift_name("doCopy(errorDetails:terminalManufacturer:shouldAbortTransaction:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoErrorDetails *errorDetails __attribute__((swift_name("errorDetails")));
@property (readonly) BOOL shouldAbortTransaction __attribute__((swift_name("shouldAbortTransaction")));
@property (readonly) OnoManufacturer * _Nullable terminalManufacturer __attribute__((swift_name("terminalManufacturer")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionActions.TransactionFallback")))
@interface OnoTransactionActionsTransactionFallback : OnoAction
- (instancetype)initWithMessage:(NSString * _Nullable)message transactionType:(OnoTransactionType *)transactionType newCardPromptMode:(OnoCardPromptMode * _Nullable)newCardPromptMode startNewTransaction:(BOOL)startNewTransaction __attribute__((swift_name("init(message:transactionType:newCardPromptMode:startNewTransaction:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString * _Nullable)component1 __attribute__((swift_name("component1()")));
- (OnoTransactionType *)component2 __attribute__((swift_name("component2()")));
- (OnoCardPromptMode * _Nullable)component3 __attribute__((swift_name("component3()")));
- (BOOL)component4 __attribute__((swift_name("component4()")));
- (OnoTransactionActionsTransactionFallback *)doCopyMessage:(NSString * _Nullable)message transactionType:(OnoTransactionType *)transactionType newCardPromptMode:(OnoCardPromptMode * _Nullable)newCardPromptMode startNewTransaction:(BOOL)startNewTransaction __attribute__((swift_name("doCopy(message:transactionType:newCardPromptMode:startNewTransaction:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable message __attribute__((swift_name("message")));
@property (readonly, getter=doNewCardPromptMode) OnoCardPromptMode * _Nullable newCardPromptMode __attribute__((swift_name("newCardPromptMode")));
@property (readonly) BOOL startNewTransaction __attribute__((swift_name("startNewTransaction")));
@property (readonly) OnoTransactionType *transactionType __attribute__((swift_name("transactionType")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionActions.TransactionProcessing")))
@interface OnoTransactionActionsTransactionProcessing : OnoAction
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString * _Nullable)component1 __attribute__((swift_name("component1()")));
- (OnoTransactionActionsTransactionProcessing *)doCopyMessage:(NSString * _Nullable)message __attribute__((swift_name("doCopy(message:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable message __attribute__((swift_name("message")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionCommandDriver")))
@interface OnoTransactionCommandDriver : OnoCommandDriver
- (instancetype)initWithTransactionListener:(id<OnoTransactionListener>)transactionListener __attribute__((swift_name("init(transactionListener:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (void)receiveCommandCommand:(OnoCommand *)command __attribute__((swift_name("receiveCommand(command:)")));
@property (readonly) id<OnoTransactionListener> transactionListener __attribute__((swift_name("transactionListener")));
@end;

__attribute__((swift_name("TransactionCommands")))
@interface OnoTransactionCommands : OnoBase
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionCommands.AbortTransaction")))
@interface OnoTransactionCommandsAbortTransaction : OnoCommand
- (instancetype)initWithMessage:(NSString *)message alsoCancelTransaction:(BOOL)alsoCancelTransaction terminal:(OnoTerminal *)terminal __attribute__((swift_name("init(message:alsoCancelTransaction:terminal:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithErrorDetails:(OnoErrorDetails *)errorDetails alsoCancelTransaction:(BOOL)alsoCancelTransaction terminal:(OnoTerminal *)terminal __attribute__((swift_name("init(errorDetails:alsoCancelTransaction:terminal:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoErrorDetails *)component1 __attribute__((swift_name("component1()")));
- (BOOL)component2 __attribute__((swift_name("component2()")));
- (OnoTerminal *)component3 __attribute__((swift_name("component3()")));
- (OnoTransactionCommandsAbortTransaction *)doCopyErrorDetails:(OnoErrorDetails *)errorDetails alsoCancelTransaction:(BOOL)alsoCancelTransaction terminal:(OnoTerminal *)terminal __attribute__((swift_name("doCopy(errorDetails:alsoCancelTransaction:terminal:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL alsoCancelTransaction __attribute__((swift_name("alsoCancelTransaction")));
@property (readonly) OnoErrorDetails *errorDetails __attribute__((swift_name("errorDetails")));
@property (readonly) OnoTerminal *terminal __attribute__((swift_name("terminal")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionCommands.AuthorizeTransaction")))
@interface OnoTransactionCommandsAuthorizeTransaction : OnoCommand
- (instancetype)initWithTransaction:(OnoTransaction *)transaction terminal:(OnoTerminal *)terminal __attribute__((swift_name("init(transaction:terminal:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoTransaction *)component1 __attribute__((swift_name("component1()")));
- (OnoTerminal *)component2 __attribute__((swift_name("component2()")));
- (OnoTransactionCommandsAuthorizeTransaction *)doCopyTransaction:(OnoTransaction *)transaction terminal:(OnoTerminal *)terminal __attribute__((swift_name("doCopy(transaction:terminal:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoTerminal *terminal __attribute__((swift_name("terminal")));
@property (readonly) OnoTransaction *transaction __attribute__((swift_name("transaction")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionCommands.BackgroundRejectTransaction")))
@interface OnoTransactionCommandsBackgroundRejectTransaction : OnoCommand
- (instancetype)initWithTransaction:(OnoTransaction *)transaction attempt:(int32_t)attempt __attribute__((swift_name("init(transaction:attempt:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoTransaction *)component1 __attribute__((swift_name("component1()")));
- (int32_t)component2 __attribute__((swift_name("component2()")));
- (OnoTransactionCommandsBackgroundRejectTransaction *)doCopyTransaction:(OnoTransaction *)transaction attempt:(int32_t)attempt __attribute__((swift_name("doCopy(transaction:attempt:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t attempt __attribute__((swift_name("attempt")));
@property (readonly) OnoTransaction *transaction __attribute__((swift_name("transaction")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionCommands.FinalizeTransaction")))
@interface OnoTransactionCommandsFinalizeTransaction : OnoCommand
- (instancetype)initWithTransactionId:(NSString *)transactionId sdkTransactionId:(NSString *)sdkTransactionId expectedFinalTransactionState:(OnoTransactionState *)expectedFinalTransactionState finalizationData:(OnoFinalizationData *)finalizationData attempt:(int32_t)attempt __attribute__((swift_name("init(transactionId:sdkTransactionId:expectedFinalTransactionState:finalizationData:attempt:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (OnoTransactionState *)component3 __attribute__((swift_name("component3()")));
- (OnoFinalizationData *)component4 __attribute__((swift_name("component4()")));
- (int32_t)component5 __attribute__((swift_name("component5()")));
- (OnoTransactionCommandsFinalizeTransaction *)doCopyTransactionId:(NSString *)transactionId sdkTransactionId:(NSString *)sdkTransactionId expectedFinalTransactionState:(OnoTransactionState *)expectedFinalTransactionState finalizationData:(OnoFinalizationData *)finalizationData attempt:(int32_t)attempt __attribute__((swift_name("doCopy(transactionId:sdkTransactionId:expectedFinalTransactionState:finalizationData:attempt:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t attempt __attribute__((swift_name("attempt")));
@property (readonly) OnoTransactionState *expectedFinalTransactionState __attribute__((swift_name("expectedFinalTransactionState")));
@property (readonly) OnoFinalizationData *finalizationData __attribute__((swift_name("finalizationData")));
@property (readonly) NSString *sdkTransactionId __attribute__((swift_name("sdkTransactionId")));
@property (readonly) NSString *transactionId __attribute__((swift_name("transactionId")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionCommands.RetryTransactionAuthorization")))
@interface OnoTransactionCommandsRetryTransactionAuthorization : OnoCommand
- (instancetype)initWithClientTransactionId:(NSString *)clientTransactionId attempt:(int32_t)attempt __attribute__((swift_name("init(clientTransactionId:attempt:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (int32_t)component2 __attribute__((swift_name("component2()")));
- (OnoTransactionCommandsRetryTransactionAuthorization *)doCopyClientTransactionId:(NSString *)clientTransactionId attempt:(int32_t)attempt __attribute__((swift_name("doCopy(clientTransactionId:attempt:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t attempt __attribute__((swift_name("attempt")));
@property (readonly) NSString *clientTransactionId __attribute__((swift_name("clientTransactionId")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionCommands.SelectEMVApplication")))
@interface OnoTransactionCommandsSelectEMVApplication : OnoCommand
- (instancetype)initWithTransaction:(OnoTransaction *)transaction emvApplicationsList:(NSArray<NSString *> * _Nullable)emvApplicationsList __attribute__((swift_name("init(transaction:emvApplicationsList:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoTransaction *)component1 __attribute__((swift_name("component1()")));
- (NSArray<NSString *> * _Nullable)component2 __attribute__((swift_name("component2()")));
- (OnoTransactionCommandsSelectEMVApplication *)doCopyTransaction:(OnoTransaction *)transaction emvApplicationsList:(NSArray<NSString *> * _Nullable)emvApplicationsList __attribute__((swift_name("doCopy(transaction:emvApplicationsList:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<NSString *> * _Nullable emvApplicationsList __attribute__((swift_name("emvApplicationsList")));
@property (readonly) OnoTransaction *transaction __attribute__((swift_name("transaction")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionCommands.SignatureRequired")))
@interface OnoTransactionCommandsSignatureRequired : OnoCommand
- (instancetype)initWithTransaction:(OnoTransaction *)transaction terminal:(OnoTerminal *)terminal __attribute__((swift_name("init(transaction:terminal:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoTransaction *)component1 __attribute__((swift_name("component1()")));
- (OnoTerminal *)component2 __attribute__((swift_name("component2()")));
- (OnoTransactionCommandsSignatureRequired *)doCopyTransaction:(OnoTransaction *)transaction terminal:(OnoTerminal *)terminal __attribute__((swift_name("doCopy(transaction:terminal:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoTerminal *terminal __attribute__((swift_name("terminal")));
@property (readonly) OnoTransaction *transaction __attribute__((swift_name("transaction")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionCommands.StartTransaction")))
@interface OnoTransactionCommandsStartTransaction : OnoCommand
- (instancetype)initWithTransaction:(OnoTransaction *)transaction terminal:(OnoTerminal *)terminal cardPresentationMode:(OnoCardPromptMode *)cardPresentationMode __attribute__((swift_name("init(transaction:terminal:cardPresentationMode:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoTransaction *)component1 __attribute__((swift_name("component1()")));
- (OnoTerminal *)component2 __attribute__((swift_name("component2()")));
- (OnoCardPromptMode *)component3 __attribute__((swift_name("component3()")));
- (OnoTransactionCommandsStartTransaction *)doCopyTransaction:(OnoTransaction *)transaction terminal:(OnoTerminal *)terminal cardPresentationMode:(OnoCardPromptMode *)cardPresentationMode __attribute__((swift_name("doCopy(transaction:terminal:cardPresentationMode:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoCardPromptMode *cardPresentationMode __attribute__((swift_name("cardPresentationMode")));
@property (readonly) OnoTerminal *terminal __attribute__((swift_name("terminal")));
@property (readonly) OnoTransaction *transaction __attribute__((swift_name("transaction")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionCommands.StartTransactionFailed")))
@interface OnoTransactionCommandsStartTransactionFailed : OnoCommand
- (instancetype)initWithMessage:(NSString *)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoTransactionCommandsStartTransactionFailed *)doCopyMessage:(NSString *)message __attribute__((swift_name("doCopy(message:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *message __attribute__((swift_name("message")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionCommands.SubmitArpc")))
@interface OnoTransactionCommandsSubmitArpc : OnoCommand
- (instancetype)initWithAuthorizationARPC:(OnoSensitiveString *)authorizationARPC terminal:(OnoTerminal *)terminal __attribute__((swift_name("init(authorizationARPC:terminal:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoSensitiveString *)component1 __attribute__((swift_name("component1()")));
- (OnoTerminal *)component2 __attribute__((swift_name("component2()")));
- (OnoTransactionCommandsSubmitArpc *)doCopyAuthorizationARPC:(OnoSensitiveString *)authorizationARPC terminal:(OnoTerminal *)terminal __attribute__((swift_name("doCopy(authorizationARPC:terminal:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoSensitiveString *authorizationARPC __attribute__((swift_name("authorizationARPC")));
@property (readonly) OnoTerminal *terminal __attribute__((swift_name("terminal")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionCommands.SubmitEMVOption")))
@interface OnoTransactionCommandsSubmitEMVOption : OnoCommand
- (instancetype)initWithEmvOption:(int32_t)emvOption terminal:(OnoTerminal *)terminal __attribute__((swift_name("init(emvOption:terminal:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (int32_t)component1 __attribute__((swift_name("component1()")));
- (OnoTerminal *)component2 __attribute__((swift_name("component2()")));
- (OnoTransactionCommandsSubmitEMVOption *)doCopyEmvOption:(int32_t)emvOption terminal:(OnoTerminal *)terminal __attribute__((swift_name("doCopy(emvOption:terminal:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t emvOption __attribute__((swift_name("emvOption")));
@property (readonly) OnoTerminal *terminal __attribute__((swift_name("terminal")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionCommands.SubmitSignature")))
@interface OnoTransactionCommandsSubmitSignature : OnoCommand
- (instancetype)initWithTransactionId:(NSString *)transactionId sdkTransactionId:(NSString *)sdkTransactionId signature:(OnoSignature *)signature terminal:(OnoTerminal * _Nullable)terminal attempt:(int32_t)attempt __attribute__((swift_name("init(transactionId:sdkTransactionId:signature:terminal:attempt:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (OnoSignature *)component3 __attribute__((swift_name("component3()")));
- (OnoTerminal * _Nullable)component4 __attribute__((swift_name("component4()")));
- (int32_t)component5 __attribute__((swift_name("component5()")));
- (OnoTransactionCommandsSubmitSignature *)doCopyTransactionId:(NSString *)transactionId sdkTransactionId:(NSString *)sdkTransactionId signature:(OnoSignature *)signature terminal:(OnoTerminal * _Nullable)terminal attempt:(int32_t)attempt __attribute__((swift_name("doCopy(transactionId:sdkTransactionId:signature:terminal:attempt:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t attempt __attribute__((swift_name("attempt")));
@property (readonly) NSString *sdkTransactionId __attribute__((swift_name("sdkTransactionId")));
@property (readonly) OnoSignature *signature __attribute__((swift_name("signature")));
@property (readonly) OnoTerminal * _Nullable terminal __attribute__((swift_name("terminal")));
@property (readonly) NSString *transactionId __attribute__((swift_name("transactionId")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionCommands.TransactionCompleted")))
@interface OnoTransactionCommandsTransactionCompleted : OnoCommand
- (instancetype)initWithTransaction:(OnoTransaction *)transaction terminal:(OnoTerminal * _Nullable)terminal __attribute__((swift_name("init(transaction:terminal:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoTransaction *)component1 __attribute__((swift_name("component1()")));
- (OnoTerminal * _Nullable)component2 __attribute__((swift_name("component2()")));
- (OnoTransactionCommandsTransactionCompleted *)doCopyTransaction:(OnoTransaction *)transaction terminal:(OnoTerminal * _Nullable)terminal __attribute__((swift_name("doCopy(transaction:terminal:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoTerminal * _Nullable terminal __attribute__((swift_name("terminal")));
@property (readonly) OnoTransaction *transaction __attribute__((swift_name("transaction")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionCommands.TransactionError")))
@interface OnoTransactionCommandsTransactionError : OnoCommand
- (instancetype)initWithErrorDetails:(OnoErrorDetails *)errorDetails terminal:(OnoTerminal * _Nullable)terminal __attribute__((swift_name("init(errorDetails:terminal:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoErrorDetails *)component1 __attribute__((swift_name("component1()")));
- (OnoTerminal * _Nullable)component2 __attribute__((swift_name("component2()")));
- (OnoTransactionCommandsTransactionError *)doCopyErrorDetails:(OnoErrorDetails *)errorDetails terminal:(OnoTerminal * _Nullable)terminal __attribute__((swift_name("doCopy(errorDetails:terminal:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoErrorDetails *errorDetails __attribute__((swift_name("errorDetails")));
@property (readonly) OnoTerminal * _Nullable terminal __attribute__((swift_name("terminal")));
@end;

__attribute__((swift_name("TransactionListener")))
@protocol OnoTransactionListener
@required
- (void)startTransactionFailedMessage:(NSString *)message __attribute__((swift_name("startTransactionFailed(message:)")));
- (void)transactionCompletedTransaction:(OnoTransaction *)transaction __attribute__((swift_name("transactionCompleted(transaction:)")));
- (void)transactionRequiresEMVSelectionTransaction:(OnoTransaction * _Nullable)transaction emvApplicationsList:(NSArray<NSString *> * _Nullable)emvApplicationsList __attribute__((swift_name("transactionRequiresEMVSelection(transaction:emvApplicationsList:)")));
- (void)transactionRequiresSignatureTransaction:(OnoTransaction *)transaction __attribute__((swift_name("transactionRequiresSignature(transaction:)")));
- (void)transactionUpdatedTransaction:(OnoTransaction * _Nullable)transaction __attribute__((swift_name("transactionUpdated(transaction:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionListenerHandler")))
@interface OnoTransactionListenerHandler : OnoBase <OnoTransactionListener>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)startTransactionFailedMessage:(NSString *)message __attribute__((swift_name("startTransactionFailed(message:)")));
- (void)transactionCompletedTransaction:(OnoTransaction *)transaction __attribute__((swift_name("transactionCompleted(transaction:)")));
- (void)transactionRequiresEMVSelectionTransaction:(OnoTransaction * _Nullable)transaction emvApplicationsList:(NSArray<NSString *> * _Nullable)emvApplicationsList __attribute__((swift_name("transactionRequiresEMVSelection(transaction:emvApplicationsList:)")));
- (void)transactionRequiresSignatureTransaction:(OnoTransaction *)transaction __attribute__((swift_name("transactionRequiresSignature(transaction:)")));
- (void)transactionUpdatedTransaction:(OnoTransaction * _Nullable)transaction __attribute__((swift_name("transactionUpdated(transaction:)")));
@property (readonly) NSString *TAG __attribute__((swift_name("TAG")));
@property id<OnoTransactionListener> _Nullable listener __attribute__((swift_name("listener")));
@property (readonly) NSArray<OnoStoreListener *> *storeListeners __attribute__((swift_name("storeListeners")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionStateData")))
@interface OnoTransactionStateData : OnoBase <OnoStoreData>
- (instancetype)initWithTransaction:(OnoTransaction * _Nullable)transaction terminal:(OnoTerminal * _Nullable)terminal terminalConfig:(OnoTerminalConfig *)terminalConfig __attribute__((swift_name("init(transaction:terminal:terminalConfig:)"))) __attribute__((objc_designated_initializer));
- (OnoTransaction * _Nullable)component1 __attribute__((swift_name("component1()")));
- (OnoTerminal * _Nullable)component2 __attribute__((swift_name("component2()")));
- (OnoTerminalConfig *)component3 __attribute__((swift_name("component3()")));
- (OnoTransactionStateData *)doCopyTransaction:(OnoTransaction * _Nullable)transaction terminal:(OnoTerminal * _Nullable)terminal terminalConfig:(OnoTerminalConfig *)terminalConfig __attribute__((swift_name("doCopy(transaction:terminal:terminalConfig:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoTerminal * _Nullable terminal __attribute__((swift_name("terminal")));
@property (readonly) OnoTerminalConfig *terminalConfig __attribute__((swift_name("terminalConfig")));
@property (readonly) OnoTransaction * _Nullable transaction __attribute__((swift_name("transaction")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionStateMachine")))
@interface OnoTransactionStateMachine : OnoBase <OnoStateMachine>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)transactionStateMachine __attribute__((swift_name("init()")));
- (OnoTransactionStateData *)castStoreDataStoreData:(id<OnoStoreData>)storeData __attribute__((swift_name("castStoreData(storeData:)")));
- (OnoStateMachineResult *)consumeActionStoreData:(id<OnoStoreData>)storeData action:(OnoAction *)action __attribute__((swift_name("consumeAction(storeData:action:)")));
- (OnoTransactionStateData *)getCurrentStateStore:(OnoStore *)store __attribute__((swift_name("getCurrentState(store:)")));
- (OnoTransactionStateData *)getCurrentStateState:(NSDictionary<NSString *, id<OnoStoreData>> *)state __attribute__((swift_name("getCurrentState(state:)")));
- (OnoTransactionStateData *)getStoreDataState:(NSDictionary<NSString *, id<OnoStoreData>> *)state __attribute__((swift_name("getStoreData(state:)")));
- (OnoTransaction * _Nullable)getTransactionState:(NSDictionary<NSString *, id<OnoStoreData>> *)state __attribute__((swift_name("getTransaction(state:)")));
- (OnoStateMachineResult *)terminalConfigUpdatedState:(OnoTransactionStateData *)state action:(OnoTerminalActionsSetTerminalConfig *)action __attribute__((swift_name("terminalConfigUpdated(state:action:)")));
@property (readonly) NSString *TAG __attribute__((swift_name("TAG")));
@property (readonly) OnoTransactionStateData *initialState __attribute__((swift_name("initialState")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TLV")))
@interface OnoTLV : OnoBase
- (instancetype)initWithTag:(NSString *)tag length:(NSString *)length value:(NSString *)value isNested:(BOOL)isNested __attribute__((swift_name("init(tag:length:value:isNested:)"))) __attribute__((objc_designated_initializer));
@property (readonly) BOOL isNested __attribute__((swift_name("isNested")));
@property (readonly) NSString *length __attribute__((swift_name("length")));
@property (readonly) NSString *tag __attribute__((swift_name("tag")));
@property NSArray<OnoTLV *> * _Nullable tlvList __attribute__((swift_name("tlvList")));
@property (readonly) NSString *value __attribute__((swift_name("value")));
@end;

__attribute__((swift_name("ATransaction")))
@interface OnoATransaction : OnoBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (NSString *)getCoreClientTransactionId __attribute__((swift_name("getCoreClientTransactionId()")));
- (NSDictionary<NSString *, id> *)getPaymentTypeSpecificDebugInfo __attribute__((swift_name("getPaymentTypeSpecificDebugInfo()")));
- (NSDictionary<NSString *, id> *)getTransactionDebugInfo __attribute__((swift_name("getTransactionDebugInfo()")));
- (NSString * _Nullable)internalDescription __attribute__((swift_name("internalDescription()")));
- (BOOL)isAbortable __attribute__((swift_name("isAbortable()")));
- (BOOL)isBusy __attribute__((swift_name("isBusy()")));
- (BOOL)isCompleted __attribute__((swift_name("isCompleted()")));
- (NSDictionary<NSString *, id> *)onoMetaData __attribute__((swift_name("onoMetaData()")));
- (NSString * _Nullable)userDescription __attribute__((swift_name("userDescription()")));
@property (readonly) int64_t amountInCents __attribute__((swift_name("amountInCents")));
@property (readonly) NSString * _Nullable chargeResult __attribute__((swift_name("chargeResult")));
@property (readonly) NSString *clientTransactionId __attribute__((swift_name("clientTransactionId")));
@property (readonly) OnoCurrency *currency __attribute__((swift_name("currency")));
@property (readonly) OnoTransactionInitiator *initiatedBy __attribute__((swift_name("initiatedBy")));
@property (readonly) OnoPaymentType *paymentType __attribute__((swift_name("paymentType")));
@property (readonly) NSString *sdkTransactionId __attribute__((swift_name("sdkTransactionId")));
@property (readonly) id<OnoATransactionState> state __attribute__((swift_name("state")));
@property (readonly) NSString * _Nullable switchTransactionId __attribute__((swift_name("switchTransactionId")));
@property (readonly) OnoTransactionError * _Nullable transactionError __attribute__((swift_name("transactionError")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AuthorizationResponseData")))
@interface OnoAuthorizationResponseData : OnoBase
- (instancetype)initWithTransactionId:(NSString *)transactionId isoResponseCode:(NSString *)isoResponseCode chargeResult:(OnoTransactionState *)chargeResult chargeResultMessage:(NSString * _Nullable)chargeResultMessage arpc:(OnoSensitiveString * _Nullable)arpc signatureRequired:(OnoBoolean * _Nullable)signatureRequired __attribute__((swift_name("init(transactionId:isoResponseCode:chargeResult:chargeResultMessage:arpc:signatureRequired:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (OnoTransactionState *)component3 __attribute__((swift_name("component3()")));
- (NSString * _Nullable)component4 __attribute__((swift_name("component4()")));
- (OnoSensitiveString * _Nullable)component5 __attribute__((swift_name("component5()")));
- (OnoBoolean * _Nullable)component6 __attribute__((swift_name("component6()")));
- (OnoAuthorizationResponseData *)doCopyTransactionId:(NSString *)transactionId isoResponseCode:(NSString *)isoResponseCode chargeResult:(OnoTransactionState *)chargeResult chargeResultMessage:(NSString * _Nullable)chargeResultMessage arpc:(OnoSensitiveString * _Nullable)arpc signatureRequired:(OnoBoolean * _Nullable)signatureRequired __attribute__((swift_name("doCopy(transactionId:isoResponseCode:chargeResult:chargeResultMessage:arpc:signatureRequired:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSDictionary<NSString *, id> *)getMappedInfo __attribute__((swift_name("getMappedInfo()")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoSensitiveString * _Nullable arpc __attribute__((swift_name("arpc")));
@property (readonly) OnoTransactionState *chargeResult __attribute__((swift_name("chargeResult")));
@property (readonly) NSString * _Nullable chargeResultMessage __attribute__((swift_name("chargeResultMessage")));
@property (readonly) NSString *isoResponseCode __attribute__((swift_name("isoResponseCode")));
@property (readonly) OnoBoolean * _Nullable signatureRequired __attribute__((swift_name("signatureRequired")));
@property (readonly) NSString *transactionId __attribute__((swift_name("transactionId")));
@end;

__attribute__((swift_name("ErrorDetails")))
@interface OnoErrorDetails : OnoBase
- (instancetype)initWithUserDescription:(NSString *)userDescription debuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("init(userDescription:debuggingInfo:)"))) __attribute__((objc_designated_initializer));
- (NSDictionary<NSString *, id> *)toMap __attribute__((swift_name("toMap()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) id _Nullable debuggingInfo __attribute__((swift_name("debuggingInfo")));
@property (readonly) NSString *userDescription __attribute__((swift_name("userDescription")));
@end;

__attribute__((swift_name("Cancelled")))
@interface OnoCancelled : OnoErrorDetails
- (instancetype)initWithUserDescription:(NSString *)userDescription debuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("init(userDescription:debuggingInfo:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Cancelled.ByApp")))
@interface OnoCancelledByApp : OnoCancelled
- (instancetype)initWithDebuggingInfo:(NSString * _Nullable)debuggingInfo __attribute__((swift_name("init(debuggingInfo:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Cancelled.BySDK")))
@interface OnoCancelledBySDK : OnoCancelled
- (instancetype)initWithCancellingComponentName:(NSString *)cancellingComponentName cancelReason:(NSString * _Nullable)cancelReason __attribute__((swift_name("init(cancellingComponentName:cancelReason:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Cancelled.ByTerminal")))
@interface OnoCancelledByTerminal : OnoCancelled
- (instancetype)initWithDebuggingInfo:(NSString * _Nullable)debuggingInfo __attribute__((swift_name("init(debuggingInfo:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("FallbackEntry")))
@interface OnoFallbackEntry : OnoBase
- (instancetype)initWithFallbackFrom:(OnoTransactionType *)fallbackFrom fallbackReason:(NSString * _Nullable)fallbackReason wasServerTransactionId:(NSString * _Nullable)wasServerTransactionId __attribute__((swift_name("init(fallbackFrom:fallbackReason:wasServerTransactionId:)"))) __attribute__((objc_designated_initializer));
- (OnoTransactionType *)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (NSString * _Nullable)component3 __attribute__((swift_name("component3()")));
- (OnoFallbackEntry *)doCopyFallbackFrom:(OnoTransactionType *)fallbackFrom fallbackReason:(NSString * _Nullable)fallbackReason wasServerTransactionId:(NSString * _Nullable)wasServerTransactionId __attribute__((swift_name("doCopy(fallbackFrom:fallbackReason:wasServerTransactionId:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSDictionary<NSString *, id> *)getMappedInfo __attribute__((swift_name("getMappedInfo()")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoTransactionType *fallbackFrom __attribute__((swift_name("fallbackFrom")));
@property (readonly) NSString * _Nullable fallbackReason __attribute__((swift_name("fallbackReason")));
@property (readonly) NSString * _Nullable wasServerTransactionId __attribute__((swift_name("wasServerTransactionId")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("FinalizationData")))
@interface OnoFinalizationData : OnoBase
- (instancetype)initWithTlv:(OnoSensitiveString * _Nullable)tlv finalizeState:(OnoFinalizeState *)finalizeState __attribute__((swift_name("init(tlv:finalizeState:)"))) __attribute__((objc_designated_initializer));
- (OnoSensitiveString * _Nullable)component1 __attribute__((swift_name("component1()")));
- (OnoFinalizeState *)component2 __attribute__((swift_name("component2()")));
- (OnoFinalizationData *)doCopyTlv:(OnoSensitiveString * _Nullable)tlv finalizeState:(OnoFinalizeState *)finalizeState __attribute__((swift_name("doCopy(tlv:finalizeState:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoFinalizeState *finalizeState __attribute__((swift_name("finalizeState")));
@property (readonly) OnoSensitiveString * _Nullable tlv __attribute__((swift_name("tlv")));
@end;

__attribute__((swift_name("Http")))
@interface OnoHttp : OnoErrorDetails
- (instancetype)initWithUserDescription:(NSString *)userDescription debuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("init(userDescription:debuggingInfo:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Http.Companion")))
@interface OnoHttpCompanion : OnoBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (OnoHttp *)otherNetworkErrorStatusCode:(OnoInt * _Nullable)statusCode message:(NSString * _Nullable)message __attribute__((swift_name("otherNetworkError(statusCode:message:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Http.ConnectionTimeout")))
@interface OnoHttpConnectionTimeout : OnoHttp
- (instancetype)initWithStatusCode:(OnoInt * _Nullable)statusCode message:(NSString * _Nullable)message __attribute__((swift_name("init(statusCode:message:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Http.InternalServerError")))
@interface OnoHttpInternalServerError : OnoHttp
- (instancetype)initWithStatusCode:(OnoInt * _Nullable)statusCode message:(NSString * _Nullable)message __attribute__((swift_name("init(statusCode:message:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Http.NoConnection")))
@interface OnoHttpNoConnection : OnoHttp
- (instancetype)initWithStatusCode:(OnoInt * _Nullable)statusCode message:(NSString * _Nullable)message __attribute__((swift_name("init(statusCode:message:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Http.NoMerchantId")))
@interface OnoHttpNoMerchantId : OnoHttp
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Http.NotWhitelisted")))
@interface OnoHttpNotWhitelisted : OnoHttp
- (instancetype)initWithUserDescription:(NSString *)userDescription errorMessage:(NSString *)errorMessage statusCode:(OnoInt * _Nullable)statusCode __attribute__((swift_name("init(userDescription:errorMessage:statusCode:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Http.NullOnoAuthenticationToken")))
@interface OnoHttpNullOnoAuthenticationToken : OnoHttp
- (instancetype)initWithStatusCode:(OnoInt * _Nullable)statusCode __attribute__((swift_name("init(statusCode:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Http.Unauthorized")))
@interface OnoHttpUnauthorized : OnoHttp
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Http.Unknown")))
@interface OnoHttpUnknown : OnoHttp
- (instancetype)initWithStatusCode:(OnoInt * _Nullable)statusCode message:(NSString * _Nullable)message __attribute__((swift_name("init(statusCode:message:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((swift_name("PinEnforcedBy")))
@interface OnoPinEnforcedBy : OnoBase
@property (readonly, getter=description_) NSString *description __attribute__((swift_name("description")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("PinEnforcedBy.CardServiceCode")))
@interface OnoPinEnforcedByCardServiceCode : OnoPinEnforcedBy
- (instancetype)initWithServiceCode:(NSString *)serviceCode __attribute__((swift_name("init(serviceCode:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoPinEnforcedByCardServiceCode *)doCopyServiceCode:(NSString *)serviceCode __attribute__((swift_name("doCopy(serviceCode:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *serviceCode __attribute__((swift_name("serviceCode")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("PinEnforcedBy.OnoCardPromptMode")))
@interface OnoPinEnforcedByOnoCardPromptMode : OnoPinEnforcedBy
- (instancetype)initWithCardPromptMode:(OnoCardPromptMode *)cardPromptMode __attribute__((swift_name("init(cardPromptMode:)"))) __attribute__((objc_designated_initializer));
- (OnoCardPromptMode *)component1 __attribute__((swift_name("component1()")));
- (OnoPinEnforcedByOnoCardPromptMode *)doCopyCardPromptMode:(OnoCardPromptMode *)cardPromptMode __attribute__((swift_name("doCopy(cardPromptMode:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoCardPromptMode *cardPromptMode __attribute__((swift_name("cardPromptMode")));
@end;

__attribute__((swift_name("RKIError")))
@interface OnoRKIError : OnoErrorDetails
- (instancetype)initWithUserDescription:(NSString *)userDescription debuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("init(userDescription:debuggingInfo:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("RKIError.Unknown")))
@interface OnoRKIErrorUnknown : OnoRKIError
- (instancetype)initWithUserDescription:(NSString *)userDescription debuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("init(userDescription:debuggingInfo:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Signature")))
@interface OnoSignature : OnoBase
- (instancetype)initWithData:(NSString *)data encoding:(NSString *)encoding mediaType:(NSString *)mediaType __attribute__((swift_name("init(data:encoding:mediaType:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (OnoSignature *)doCopyData:(NSString *)data encoding:(NSString *)encoding mediaType:(NSString *)mediaType __attribute__((swift_name("doCopy(data:encoding:mediaType:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSString *)getSerializedString __attribute__((swift_name("getSerializedString()")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (BOOL)isBlank __attribute__((swift_name("isBlank()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *data __attribute__((swift_name("data")));
@property (readonly) NSString *encoding __attribute__((swift_name("encoding")));
@property (readonly) NSString *mediaType __attribute__((swift_name("mediaType")));
@end;

__attribute__((swift_name("SwitchReject")))
@interface OnoSwitchReject : OnoErrorDetails
- (instancetype)initWithUserDescription:(NSString *)userDescription debuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("init(userDescription:debuggingInfo:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (NSDictionary<NSString *, id> *)toMap __attribute__((swift_name("toMap()")));
@property (readonly) NSString * _Nullable isoResponseCode __attribute__((swift_name("isoResponseCode")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SwitchReject.ArpcMissing")))
@interface OnoSwitchRejectArpcMissing : OnoSwitchReject
- (instancetype)initWithIsoResponseCode:(NSString *)isoResponseCode debuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("init(isoResponseCode:debuggingInfo:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SwitchReject.AuthorizationFailed")))
@interface OnoSwitchRejectAuthorizationFailed : OnoSwitchReject
- (instancetype)initWithIsoResponseCode:(NSString *)isoResponseCode userDescription:(NSString *)userDescription __attribute__((swift_name("init(isoResponseCode:userDescription:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SwitchReject.FallbackFromChipForbidden")))
@interface OnoSwitchRejectFallbackFromChipForbidden : OnoSwitchReject
- (instancetype)initWithIsoResponseCode:(OnoIsoResponseCode *)isoResponseCode __attribute__((swift_name("init(isoResponseCode:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SwitchReject.IsoCodeAndChargeResultMismatch")))
@interface OnoSwitchRejectIsoCodeAndChargeResultMismatch : OnoSwitchReject
- (instancetype)initWithIsoResponseCode:(NSString *)isoResponseCode chargeResult:(OnoTransactionState * _Nullable)chargeResult __attribute__((swift_name("init(isoResponseCode:chargeResult:)"))) __attribute__((objc_designated_initializer));
@property (readonly) OnoTransactionState * _Nullable chargeResult __attribute__((swift_name("chargeResult")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SwitchReject.RepeatingFallbackCode")))
@interface OnoSwitchRejectRepeatingFallbackCode : OnoSwitchReject
- (instancetype)initWithIsoResponseCode:(OnoIsoResponseCode *)isoResponseCode userDescription:(NSString *)userDescription __attribute__((swift_name("init(isoResponseCode:userDescription:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((swift_name("TerminalError")))
@interface OnoTerminalError : OnoErrorDetails
- (instancetype)initWithUserDescription:(NSString *)userDescription debuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("init(userDescription:debuggingInfo:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalError.Busy")))
@interface OnoTerminalErrorBusy : OnoTerminalError
- (instancetype)initWithDebuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("init(debuggingInfo:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalError.CardRemoved")))
@interface OnoTerminalErrorCardRemoved : OnoTerminalError
- (instancetype)initWithDebuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("init(debuggingInfo:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalError.DeviceReset")))
@interface OnoTerminalErrorDeviceReset : OnoTerminalError
- (instancetype)initWithDebuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("init(debuggingInfo:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalError.Disconnected")))
@interface OnoTerminalErrorDisconnected : OnoTerminalError
- (instancetype)initWithDebuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("init(debuggingInfo:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalError.InvalidTLV")))
@interface OnoTerminalErrorInvalidTLV : OnoTerminalError
- (instancetype)initWithDebuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("init(debuggingInfo:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((swift_name("TerminalError.Queue")))
@interface OnoTerminalErrorQueue : OnoTerminalError
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalError.QueueExecutionTimeout")))
@interface OnoTerminalErrorQueueExecutionTimeout : OnoTerminalErrorQueue
- (instancetype)initWithQueueCommand:(NSString *)queueCommand __attribute__((swift_name("init(queueCommand:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalError.QueueServiceBusy")))
@interface OnoTerminalErrorQueueServiceBusy : OnoTerminalErrorQueue
- (instancetype)initWithQueueCommand:(NSString *)queueCommand __attribute__((swift_name("init(queueCommand:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalError.QueueStartTimeout")))
@interface OnoTerminalErrorQueueStartTimeout : OnoTerminalErrorQueue
- (instancetype)initWithQueueCommand:(NSString *)queueCommand __attribute__((swift_name("init(queueCommand:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalError.ReleasedConnection")))
@interface OnoTerminalErrorReleasedConnection : OnoTerminalError
- (instancetype)initWithDebuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("init(debuggingInfo:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalError.Timeout")))
@interface OnoTerminalErrorTimeout : OnoTerminalError
- (instancetype)initWithUserDescription:(NSString *)userDescription debuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("init(userDescription:debuggingInfo:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalError.UnhandledAction")))
@interface OnoTerminalErrorUnhandledAction : OnoTerminalError
- (instancetype)initWithErrorMessage:(NSString *)errorMessage action:(OnoAction *)action debuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("init(errorMessage:action:debuggingInfo:)"))) __attribute__((objc_designated_initializer));
- (NSDictionary<NSString *, id> *)toMap __attribute__((swift_name("toMap()")));
@property (readonly) OnoAction *action __attribute__((swift_name("action")));
@property (readonly) NSString *errorMessage __attribute__((swift_name("errorMessage")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TerminalError.UpdateFailed")))
@interface OnoTerminalErrorUpdateFailed : OnoTerminalError
- (instancetype)initWithInstallation:(NSString *)installation userDescription:(NSString * _Nullable)userDescription debuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("init(installation:userDescription:debuggingInfo:)"))) __attribute__((objc_designated_initializer));
- (NSDictionary<NSString *, id> *)toMap __attribute__((swift_name("toMap()")));
@property (readonly) NSString *installation __attribute__((swift_name("installation")));
@end;

__attribute__((swift_name("TimedOut")))
@interface OnoTimedOut : OnoErrorDetails
- (instancetype)initWithUserDescription:(NSString *)userDescription debuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("init(userDescription:debuggingInfo:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TimedOut.AwaitingCard")))
@interface OnoTimedOutAwaitingCard : OnoTimedOut
- (instancetype)initWithDebuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("init(debuggingInfo:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TimedOut.DuringApplicationSelection")))
@interface OnoTimedOutDuringApplicationSelection : OnoTimedOut
- (instancetype)initWithDebuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("init(debuggingInfo:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Transaction")))
@interface OnoTransaction : OnoATransaction
- (instancetype)initWithAmountInCents:(int64_t)amountInCents state:(OnoTransactionState *)state clientTransactionId:(NSString *)clientTransactionId currency:(OnoCurrency *)currency initiatedBy:(OnoTransactionInitiator *)initiatedBy switchTransactionId:(NSString * _Nullable)switchTransactionId type:(OnoTransactionType * _Nullable)type cardPresentationMode:(OnoCardPromptMode * _Nullable)cardPresentationMode authorizationTLV:(OnoSensitiveString * _Nullable)authorizationTLV authorizationResponseData:(OnoAuthorizationResponseData * _Nullable)authorizationResponseData finalizationData:(OnoFinalizationData * _Nullable)finalizationData internalDescription:(NSString * _Nullable)internalDescription emvApplications:(NSArray<NSString *> * _Nullable)emvApplications transactionData:(OnoSensitiveMap * _Nullable)transactionData signature:(OnoSignature * _Nullable)signature fallbackHistory:(NSSet<OnoFallbackEntry *> * _Nullable)fallbackHistory switchFallbackResponseCode:(NSString * _Nullable)switchFallbackResponseCode maskedPan:(OnoSensitiveString * _Nullable)maskedPan sdkTransactionId:(NSString *)sdkTransactionId transactionError:(OnoTransactionError * _Nullable)transactionError tipAmountInCentsIncludedInAmount:(int32_t)tipAmountInCentsIncludedInAmount paymentType:(OnoPaymentType *)paymentType chargeResult:(NSString * _Nullable)chargeResult hasCompletedCommandDispatched:(BOOL)hasCompletedCommandDispatched __attribute__((swift_name("init(amountInCents:state:clientTransactionId:currency:initiatedBy:switchTransactionId:type:cardPresentationMode:authorizationTLV:authorizationResponseData:finalizationData:internalDescription:emvApplications:transactionData:signature:fallbackHistory:switchFallbackResponseCode:maskedPan:sdkTransactionId:transactionError:tipAmountInCentsIncludedInAmount:paymentType:chargeResult:hasCompletedCommandDispatched:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (int64_t)component1 __attribute__((swift_name("component1()")));
- (OnoAuthorizationResponseData * _Nullable)component10 __attribute__((swift_name("component10()")));
- (OnoFinalizationData * _Nullable)component11 __attribute__((swift_name("component11()")));
- (NSString * _Nullable)component12 __attribute__((swift_name("component12()")));
- (NSArray<NSString *> * _Nullable)component13 __attribute__((swift_name("component13()")));
- (OnoSensitiveMap * _Nullable)component14 __attribute__((swift_name("component14()")));
- (OnoSignature * _Nullable)component15 __attribute__((swift_name("component15()")));
- (NSSet<OnoFallbackEntry *> * _Nullable)component16 __attribute__((swift_name("component16()")));
- (NSString * _Nullable)component17 __attribute__((swift_name("component17()")));
- (OnoSensitiveString * _Nullable)component18 __attribute__((swift_name("component18()")));
- (NSString *)component19 __attribute__((swift_name("component19()")));
- (OnoTransactionState *)component2 __attribute__((swift_name("component2()")));
- (OnoTransactionError * _Nullable)component20 __attribute__((swift_name("component20()")));
- (int32_t)component21 __attribute__((swift_name("component21()")));
- (OnoPaymentType *)component22 __attribute__((swift_name("component22()")));
- (NSString * _Nullable)component23 __attribute__((swift_name("component23()")));
- (BOOL)component24 __attribute__((swift_name("component24()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (OnoCurrency *)component4 __attribute__((swift_name("component4()")));
- (OnoTransactionInitiator *)component5 __attribute__((swift_name("component5()")));
- (NSString * _Nullable)component6 __attribute__((swift_name("component6()")));
- (OnoTransactionType * _Nullable)component7 __attribute__((swift_name("component7()")));
- (OnoCardPromptMode * _Nullable)component8 __attribute__((swift_name("component8()")));
- (OnoSensitiveString * _Nullable)component9 __attribute__((swift_name("component9()")));
- (OnoTransaction *)doCopyAmountInCents:(int64_t)amountInCents state:(OnoTransactionState *)state clientTransactionId:(NSString *)clientTransactionId currency:(OnoCurrency *)currency initiatedBy:(OnoTransactionInitiator *)initiatedBy switchTransactionId:(NSString * _Nullable)switchTransactionId type:(OnoTransactionType * _Nullable)type cardPresentationMode:(OnoCardPromptMode * _Nullable)cardPresentationMode authorizationTLV:(OnoSensitiveString * _Nullable)authorizationTLV authorizationResponseData:(OnoAuthorizationResponseData * _Nullable)authorizationResponseData finalizationData:(OnoFinalizationData * _Nullable)finalizationData internalDescription:(NSString * _Nullable)internalDescription emvApplications:(NSArray<NSString *> * _Nullable)emvApplications transactionData:(OnoSensitiveMap * _Nullable)transactionData signature:(OnoSignature * _Nullable)signature fallbackHistory:(NSSet<OnoFallbackEntry *> * _Nullable)fallbackHistory switchFallbackResponseCode:(NSString * _Nullable)switchFallbackResponseCode maskedPan:(OnoSensitiveString * _Nullable)maskedPan sdkTransactionId:(NSString *)sdkTransactionId transactionError:(OnoTransactionError * _Nullable)transactionError tipAmountInCentsIncludedInAmount:(int32_t)tipAmountInCentsIncludedInAmount paymentType:(OnoPaymentType *)paymentType chargeResult:(NSString * _Nullable)chargeResult hasCompletedCommandDispatched:(BOOL)hasCompletedCommandDispatched __attribute__((swift_name("doCopy(amountInCents:state:clientTransactionId:currency:initiatedBy:switchTransactionId:type:cardPresentationMode:authorizationTLV:authorizationResponseData:finalizationData:internalDescription:emvApplications:transactionData:signature:fallbackHistory:switchFallbackResponseCode:maskedPan:sdkTransactionId:transactionError:tipAmountInCentsIncludedInAmount:paymentType:chargeResult:hasCompletedCommandDispatched:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSString *)getCoreClientTransactionId __attribute__((swift_name("getCoreClientTransactionId()")));
- (NSDictionary<NSString *, id> *)getPaymentTypeSpecificDebugInfo __attribute__((swift_name("getPaymentTypeSpecificDebugInfo()")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)heading __attribute__((swift_name("heading()")));
- (NSString * _Nullable)internalDescription __attribute__((swift_name("internalDescription()")));
- (BOOL)isAbortable __attribute__((swift_name("isAbortable()")));
- (BOOL)isBusy __attribute__((swift_name("isBusy()")));
- (BOOL)isCompleted __attribute__((swift_name("isCompleted()")));
- (BOOL)isFallbackOrCompleted __attribute__((swift_name("isFallbackOrCompleted()")));
- (BOOL)isSignatureRequired __attribute__((swift_name("isSignatureRequired()")));
- (NSDictionary<NSString *, id> *)onoMetaData __attribute__((swift_name("onoMetaData()")));
- (OnoTransaction *)sanitizeTransaction __attribute__((swift_name("sanitizeTransaction()")));
- (NSString *)description __attribute__((swift_name("description()")));
- (NSString * _Nullable)userDescription __attribute__((swift_name("userDescription()")));
@property (readonly) int64_t amountInCents __attribute__((swift_name("amountInCents")));
@property (readonly) OnoAuthorizationResponseData * _Nullable authorizationResponseData __attribute__((swift_name("authorizationResponseData")));
@property (readonly) OnoSensitiveString * _Nullable authorizationTLV __attribute__((swift_name("authorizationTLV")));
@property (readonly) OnoCardPromptMode * _Nullable cardPresentationMode __attribute__((swift_name("cardPresentationMode")));
@property (readonly) NSString * _Nullable chargeResult __attribute__((swift_name("chargeResult")));
@property (readonly) NSString *clientTransactionId __attribute__((swift_name("clientTransactionId")));
@property (readonly) OnoCurrency *currency __attribute__((swift_name("currency")));
@property (readonly) NSArray<NSString *> * _Nullable emvApplications __attribute__((swift_name("emvApplications")));
@property (readonly) NSSet<OnoFallbackEntry *> * _Nullable fallbackHistory __attribute__((swift_name("fallbackHistory")));
@property (readonly) OnoFinalizationData * _Nullable finalizationData __attribute__((swift_name("finalizationData")));
@property (readonly) BOOL hasCompletedCommandDispatched __attribute__((swift_name("hasCompletedCommandDispatched")));
@property (readonly) OnoTransactionInitiator *initiatedBy __attribute__((swift_name("initiatedBy")));
@property (readonly, getter=internalDescription_) NSString * _Nullable internalDescription __attribute__((swift_name("internalDescription")));
@property (readonly) OnoSensitiveString * _Nullable maskedPan __attribute__((swift_name("maskedPan")));
@property (readonly) OnoPaymentType *paymentType __attribute__((swift_name("paymentType")));
@property (readonly) NSString *sdkTransactionId __attribute__((swift_name("sdkTransactionId")));
@property (readonly) OnoSignature * _Nullable signature __attribute__((swift_name("signature")));
@property (readonly) OnoTransactionState *state __attribute__((swift_name("state")));
@property (readonly) NSString * _Nullable switchFallbackResponseCode __attribute__((swift_name("switchFallbackResponseCode")));
@property (readonly) NSString * _Nullable switchTransactionId __attribute__((swift_name("switchTransactionId")));
@property (readonly) int32_t tipAmountInCentsIncludedInAmount __attribute__((swift_name("tipAmountInCentsIncludedInAmount")));
@property (readonly) OnoSensitiveMap * _Nullable transactionData __attribute__((swift_name("transactionData")));
@property (readonly) OnoTransactionError * _Nullable transactionError __attribute__((swift_name("transactionError")));
@property (readonly) OnoTransactionType * _Nullable type __attribute__((swift_name("type")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Transaction.Companion")))
@interface OnoTransactionCompanion : OnoBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (readonly) NSSet<NSString *> *EXCLUDED_FIELDS_FOR_PCI_COMPLIANCE __attribute__((swift_name("EXCLUDED_FIELDS_FOR_PCI_COMPLIANCE")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Transaction.UserDescriptions")))
@interface OnoTransactionUserDescriptions : OnoBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)userDescriptions __attribute__((swift_name("init()")));
- (NSString * _Nullable)presentCardCardPresentationMode:(OnoCardPromptMode * _Nullable)cardPresentationMode __attribute__((swift_name("presentCard(cardPresentationMode:)")));
@property (readonly) NSString *EnterPin __attribute__((swift_name("EnterPin")));
@property (readonly) NSString *PleaseWait __attribute__((swift_name("PleaseWait")));
@property (readonly) NSString *SelectApplicationOnCardMachine __attribute__((swift_name("SelectApplicationOnCardMachine")));
@property (readonly) NSString *SelectApplicationOnPosScreen __attribute__((swift_name("SelectApplicationOnPosScreen")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionError")))
@interface OnoTransactionError : OnoBase
- (instancetype)initWithStateWhenOccurred:(id<OnoATransactionState>)stateWhenOccurred errorDetails:(OnoErrorDetails *)errorDetails __attribute__((swift_name("init(stateWhenOccurred:errorDetails:)"))) __attribute__((objc_designated_initializer));
- (NSString *)getErrorName __attribute__((swift_name("getErrorName()")));
- (NSDictionary<NSString *, id> *)toMap __attribute__((swift_name("toMap()")));
- (NSString *)description __attribute__((swift_name("description()")));
- (NSString *)userDescription __attribute__((swift_name("userDescription()")));
@property (readonly) OnoErrorDetails *errorDetails __attribute__((swift_name("errorDetails")));
@property (readonly) id<OnoATransactionState> stateWhenOccurred __attribute__((swift_name("stateWhenOccurred")));
@end;

__attribute__((swift_name("TransactionErrorDetails")))
@interface OnoTransactionErrorDetails : OnoErrorDetails
- (instancetype)initWithUserDescription:(NSString *)userDescription debuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("init(userDescription:debuggingInfo:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionErrorDetails.CancellingQrTransaction")))
@interface OnoTransactionErrorDetailsCancellingQrTransaction : OnoErrorDetails
- (instancetype)initWithDebuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("init(debuggingInfo:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithUserDescription:(NSString *)userDescription debuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("init(userDescription:debuggingInfo:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionErrorDetails.ChargeCancelled")))
@interface OnoTransactionErrorDetailsChargeCancelled : OnoErrorDetails
- (instancetype)initWithDebuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("init(debuggingInfo:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithUserDescription:(NSString *)userDescription debuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("init(userDescription:debuggingInfo:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionErrorDetails.ChargeDeclined")))
@interface OnoTransactionErrorDetailsChargeDeclined : OnoErrorDetails
- (instancetype)initWithChargeResultMessage:(NSString * _Nullable)chargeResultMessage __attribute__((swift_name("init(chargeResultMessage:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithUserDescription:(NSString *)userDescription debuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("init(userDescription:debuggingInfo:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionErrorDetails.ChargeError")))
@interface OnoTransactionErrorDetailsChargeError : OnoErrorDetails
- (instancetype)initWithChargeResultMessage:(NSString * _Nullable)chargeResultMessage __attribute__((swift_name("init(chargeResultMessage:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithUserDescription:(NSString *)userDescription debuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("init(userDescription:debuggingInfo:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionErrorDetails.ConfirmedUnApprovedCharge")))
@interface OnoTransactionErrorDetailsConfirmedUnApprovedCharge : OnoErrorDetails
- (instancetype)initWithChargeResult:(NSString * _Nullable)chargeResult __attribute__((swift_name("init(chargeResult:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithUserDescription:(NSString *)userDescription debuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("init(userDescription:debuggingInfo:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (readonly) NSString * _Nullable chargeResult __attribute__((swift_name("chargeResult")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionErrorDetails.DeclinedByTerminalOrCard")))
@interface OnoTransactionErrorDetailsDeclinedByTerminalOrCard : OnoTransactionErrorDetails
- (instancetype)initWithDebuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("init(debuggingInfo:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionErrorDetails.DeclinedWithNonFinalState")))
@interface OnoTransactionErrorDetailsDeclinedWithNonFinalState : OnoTransactionErrorDetails
- (instancetype)initWithNonFinalState:(OnoTransactionState * _Nullable)nonFinalState originalError:(OnoErrorDetails *)originalError debuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("init(nonFinalState:originalError:debuggingInfo:)"))) __attribute__((objc_designated_initializer));
- (NSDictionary<NSString *, id> *)toMap __attribute__((swift_name("toMap()")));
@property (readonly) OnoTransactionState * _Nullable nonFinalState __attribute__((swift_name("nonFinalState")));
@property (readonly) OnoErrorDetails *originalError __attribute__((swift_name("originalError")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionErrorDetails.InvalidAmountValue")))
@interface OnoTransactionErrorDetailsInvalidAmountValue : OnoErrorDetails
- (instancetype)initWithAmountInCents:(int64_t)amountInCents __attribute__((swift_name("init(amountInCents:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithUserDescription:(NSString *)userDescription debuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("init(userDescription:debuggingInfo:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (readonly) int64_t amountInCents __attribute__((swift_name("amountInCents")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionErrorDetails.InvalidClientTransactionId")))
@interface OnoTransactionErrorDetailsInvalidClientTransactionId : OnoErrorDetails
- (instancetype)initWithClientTransactionUUID:(NSString *)clientTransactionUUID __attribute__((swift_name("init(clientTransactionUUID:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithUserDescription:(NSString *)userDescription debuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("init(userDescription:debuggingInfo:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (readonly) NSString *clientTransactionUUID __attribute__((swift_name("clientTransactionUUID")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionErrorDetails.InvalidCurrencyString")))
@interface OnoTransactionErrorDetailsInvalidCurrencyString : OnoErrorDetails
- (instancetype)initWithCurrency:(NSString *)currency __attribute__((swift_name("init(currency:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithUserDescription:(NSString *)userDescription debuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("init(userDescription:debuggingInfo:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (readonly) NSString *currency __attribute__((swift_name("currency")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionErrorDetails.NotInExpectedState")))
@interface OnoTransactionErrorDetailsNotInExpectedState : OnoErrorDetails
- (instancetype)initWithCurrentState:(id<OnoATransactionState> _Nullable)currentState expectedStates:(NSSet<id<OnoATransactionState>> *)expectedStates causeAction:(OnoAction *)causeAction debuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("init(currentState:expectedStates:causeAction:debuggingInfo:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithUserDescription:(NSString *)userDescription debuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("init(userDescription:debuggingInfo:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (NSDictionary<NSString *, id> *)toMap __attribute__((swift_name("toMap()")));
@property (readonly) OnoAction *causeAction __attribute__((swift_name("causeAction")));
@property (readonly) id<OnoATransactionState> _Nullable currentState __attribute__((swift_name("currentState")));
@property (readonly) NSSet<id<OnoATransactionState>> *expectedStates __attribute__((swift_name("expectedStates")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionErrorDetails.NullTerminalMidTran")))
@interface OnoTransactionErrorDetailsNullTerminalMidTran : OnoErrorDetails
- (instancetype)initWithState:(id<OnoATransactionState> _Nullable)state __attribute__((swift_name("init(state:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithUserDescription:(NSString *)userDescription debuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("init(userDescription:debuggingInfo:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionErrorDetails.QrTransactionRejected")))
@interface OnoTransactionErrorDetailsQrTransactionRejected : OnoErrorDetails
- (instancetype)initWithDebuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("init(debuggingInfo:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithUserDescription:(NSString *)userDescription debuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("init(userDescription:debuggingInfo:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionErrorDetails.TransactionInflight")))
@interface OnoTransactionErrorDetailsTransactionInflight : OnoErrorDetails
- (instancetype)initWithTransaction:(OnoATransaction *)transaction __attribute__((swift_name("init(transaction:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithUserDescription:(NSString *)userDescription debuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("init(userDescription:debuggingInfo:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (readonly) OnoATransaction *transaction __attribute__((swift_name("transaction")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionErrorDetails.TransactionInfoMissing")))
@interface OnoTransactionErrorDetailsTransactionInfoMissing : OnoTransactionErrorDetails
- (instancetype)initWithDebuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("init(debuggingInfo:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionErrorDetails.UnableToConfirm")))
@interface OnoTransactionErrorDetailsUnableToConfirm : OnoErrorDetails
- (instancetype)initWithDebuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("init(debuggingInfo:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithUserDescription:(NSString *)userDescription debuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("init(userDescription:debuggingInfo:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionErrorDetails.UnableToReject")))
@interface OnoTransactionErrorDetailsUnableToReject : OnoErrorDetails
- (instancetype)initWithDebuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("init(debuggingInfo:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithUserDescription:(NSString *)userDescription debuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("init(userDescription:debuggingInfo:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionErrorDetails.UnhandledChargeResult")))
@interface OnoTransactionErrorDetailsUnhandledChargeResult : OnoErrorDetails
- (instancetype)initWithChargeResult:(NSString *)chargeResult chargeResultMessage:(NSString * _Nullable)chargeResultMessage __attribute__((swift_name("init(chargeResult:chargeResultMessage:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithUserDescription:(NSString *)userDescription debuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("init(userDescription:debuggingInfo:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@end;

__attribute__((swift_name("Unclassified")))
@interface OnoUnclassified : OnoErrorDetails
- (instancetype)initWithUserDescription:(NSString *)userDescription debuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("init(userDescription:debuggingInfo:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Unclassified.NullApiInterface")))
@interface OnoUnclassifiedNullApiInterface : OnoUnclassified
- (instancetype)initWithTargetInterface:(NSString *)targetInterface __attribute__((swift_name("init(targetInterface:)"))) __attribute__((objc_designated_initializer));
@property (readonly) NSString *targetInterface __attribute__((swift_name("targetInterface")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Unclassified.Unknown")))
@interface OnoUnclassifiedUnknown : OnoUnclassified
- (instancetype)initWithUserDescription:(NSString *)userDescription debuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("init(userDescription:debuggingInfo:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((swift_name("ATransactionState")))
@protocol OnoATransactionState
@required
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly) BOOL postToCoreApprovedApi __attribute__((swift_name("postToCoreApprovedApi")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CardPromptMode")))
@interface OnoCardPromptMode : OnoKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) OnoCardPromptMode *tapInsertSwipe __attribute__((swift_name("tapInsertSwipe")));
@property (class, readonly) OnoCardPromptMode *tapInsertSwipeWithForcedPin __attribute__((swift_name("tapInsertSwipeWithForcedPin")));
@property (class, readonly) OnoCardPromptMode *insertOrSwipeWithForcedPin __attribute__((swift_name("insertOrSwipeWithForcedPin")));
@property (class, readonly) OnoCardPromptMode *insertOrSwipe __attribute__((swift_name("insertOrSwipe")));
@property (class, readonly) OnoCardPromptMode *insertOnly __attribute__((swift_name("insertOnly")));
@property (class, readonly) OnoCardPromptMode *swipeOnly __attribute__((swift_name("swipeOnly")));
@property (class, readonly) OnoCardPromptMode *swipeOnlyWithForcedPin __attribute__((swift_name("swipeOnlyWithForcedPin")));
- (BOOL)canInsertCard __attribute__((swift_name("canInsertCard()")));
- (BOOL)canSwipeCard __attribute__((swift_name("canSwipeCard()")));
- (BOOL)canTapCard __attribute__((swift_name("canTapCard()")));
- (int32_t)compareToOther:(OnoCardPromptMode *)other __attribute__((swift_name("compareTo(other:)")));
- (BOOL)mustForceAskForPinOnSwipe __attribute__((swift_name("mustForceAskForPinOnSwipe()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CardPromptMode.Companion")))
@interface OnoCardPromptModeCompanion : OnoBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (NSString *)describeCardPromptMode:(OnoCardPromptMode *)cardPromptMode __attribute__((swift_name("describe(cardPromptMode:)")));
- (OnoCardPromptMode *)fallbackCardPromptModeTransactionType:(OnoTransactionType *)transactionType __attribute__((swift_name("fallbackCardPromptMode(transactionType:)")));
- (OnoCardPromptMode * _Nullable)noTapEquivalentCardPromptModeProposedPromptMode:(OnoCardPromptMode *)proposedPromptMode __attribute__((swift_name("noTapEquivalentCardPromptMode(proposedPromptMode:)")));
@property (readonly) NSSet<OnoCardPromptMode *> *forcePinWhenSwipedModes __attribute__((swift_name("forcePinWhenSwipedModes")));
@property (readonly) NSSet<OnoCardPromptMode *> *insertEnabledPromptModes __attribute__((swift_name("insertEnabledPromptModes")));
@property (readonly) NSSet<OnoCardPromptMode *> *swipeEnabledPromptModes __attribute__((swift_name("swipeEnabledPromptModes")));
@property (readonly) NSSet<OnoCardPromptMode *> *tapEnabledPromptModes __attribute__((swift_name("tapEnabledPromptModes")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Currency")))
@interface OnoCurrency : OnoKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) OnoCurrency *zar __attribute__((swift_name("zar")));
@property (class, readonly) OnoCurrency *mur __attribute__((swift_name("mur")));
- (int32_t)compareToOther:(OnoCurrency *)other __attribute__((swift_name("compareTo(other:)")));
@property (readonly) int32_t code __attribute__((swift_name("code")));
@property (readonly) int64_t nfcLimitInCents __attribute__((swift_name("nfcLimitInCents")));
@property (readonly) NSString *symbol __attribute__((swift_name("symbol")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Currency.Companion")))
@interface OnoCurrencyCompanion : OnoBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (OnoCurrency * _Nullable)valueOrNullCurrency:(NSString *)currency __attribute__((swift_name("valueOrNull(currency:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("FinalizeState")))
@interface OnoFinalizeState : OnoKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) OnoFinalizeState *confirm __attribute__((swift_name("confirm")));
@property (class, readonly) OnoFinalizeState *reject __attribute__((swift_name("reject")));
- (int32_t)compareToOther:(OnoFinalizeState *)other __attribute__((swift_name("compareTo(other:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("IsoResponseCode")))
@interface OnoIsoResponseCode : OnoKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) OnoIsoResponseCode *_00SuccessfulAuthorization __attribute__((swift_name("_00SuccessfulAuthorization")));
@property (class, readonly) OnoIsoResponseCode *_05DoNotHonor __attribute__((swift_name("_05DoNotHonor")));
@property (class, readonly) OnoIsoResponseCode *_55PinFailed __attribute__((swift_name("_55PinFailed")));
@property (class, readonly) OnoIsoResponseCode *_65AccountLimitExceeded __attribute__((swift_name("_65AccountLimitExceeded")));
@property (class, readonly) OnoIsoResponseCode *n0InsertChipCard __attribute__((swift_name("n0InsertChipCard")));
@property (class, readonly) OnoIsoResponseCode *n2SwipeCard __attribute__((swift_name("n2SwipeCard")));
@property (class, readonly) OnoIsoResponseCode *n5SwipeCardAndEnterPin __attribute__((swift_name("n5SwipeCardAndEnterPin")));
@property (class, readonly) OnoIsoResponseCode *n6TapOrInsertOrSwipeAndEnterPin __attribute__((swift_name("n6TapOrInsertOrSwipeAndEnterPin")));
@property (class, readonly) OnoIsoResponseCode *n7TryAnotherCard __attribute__((swift_name("n7TryAnotherCard")));
@property (class, readonly) OnoIsoResponseCode *p0AmountBelowMinimum __attribute__((swift_name("p0AmountBelowMinimum")));
@property (class, readonly) OnoIsoResponseCode *p1AmountAboveMaximum __attribute__((swift_name("p1AmountAboveMaximum")));
@property (class, readonly) OnoIsoResponseCode *p2RestartTransaction __attribute__((swift_name("p2RestartTransaction")));
- (int32_t)compareToOther:(OnoIsoResponseCode *)other __attribute__((swift_name("compareTo(other:)")));
- (BOOL)isNotApprovedAuthorization __attribute__((swift_name("isNotApprovedAuthorization()")));
- (BOOL)isOnoSwitchFallbackCode __attribute__((swift_name("isOnoSwitchFallbackCode()")));
- (BOOL)shouldFallbackFromTapMustFallbackOn05:(BOOL)mustFallbackOn05 __attribute__((swift_name("shouldFallbackFromTap(mustFallbackOn05:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *code __attribute__((swift_name("code")));
@property (readonly) OnoCardPromptMode * _Nullable fallbackCardPromptMode __attribute__((swift_name("fallbackCardPromptMode")));
@property (readonly) NSString * _Nullable fallbackMessage __attribute__((swift_name("fallbackMessage")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("IsoResponseCode.Companion")))
@interface OnoIsoResponseCodeCompanion : OnoBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (OnoIsoResponseCode * _Nullable)maybeIsoResponseCodeFromCodeCode:(NSString *)code __attribute__((swift_name("maybeIsoResponseCodeFromCode(code:)")));
@property (readonly) NSArray<OnoIsoResponseCode *> *fallbackNcodes __attribute__((swift_name("fallbackNcodes")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("PaymentProcessor")))
@interface OnoPaymentProcessor : OnoKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) OnoPaymentProcessor *payworks __attribute__((swift_name("payworks")));
@property (class, readonly) OnoPaymentProcessor *ono __attribute__((swift_name("ono")));
- (int32_t)compareToOther:(OnoPaymentProcessor *)other __attribute__((swift_name("compareTo(other:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("PaymentType")))
@interface OnoPaymentType : OnoKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) OnoPaymentType *card __attribute__((swift_name("card")));
@property (class, readonly) OnoPaymentType *masterpass __attribute__((swift_name("masterpass")));
- (int32_t)compareToOther:(OnoPaymentType *)other __attribute__((swift_name("compareTo(other:)")));
@property (readonly) NSString *onoSwitchName __attribute__((swift_name("onoSwitchName")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionState")))
@interface OnoTransactionState : OnoKotlinEnum <OnoATransactionState>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) OnoTransactionState *getOnoStatus __attribute__((swift_name("getOnoStatus")));
@property (class, readonly) OnoTransactionState *initialized __attribute__((swift_name("initialized")));
@property (class, readonly) OnoTransactionState *initializing __attribute__((swift_name("initializing")));
@property (class, readonly) OnoTransactionState *authorizing __attribute__((swift_name("authorizing")));
@property (class, readonly) OnoTransactionState *processingArpc __attribute__((swift_name("processingArpc")));
@property (class, readonly) OnoTransactionState *finalizing __attribute__((swift_name("finalizing")));
@property (class, readonly) OnoTransactionState *presentCard __attribute__((swift_name("presentCard")));
@property (class, readonly) OnoTransactionState *cardPresented __attribute__((swift_name("cardPresented")));
@property (class, readonly) OnoTransactionState *readingCard __attribute__((swift_name("readingCard")));
@property (class, readonly) OnoTransactionState *selectEmvApp __attribute__((swift_name("selectEmvApp")));
@property (class, readonly) OnoTransactionState *emvAppSelected __attribute__((swift_name("emvAppSelected")));
@property (class, readonly) OnoTransactionState *enterPin __attribute__((swift_name("enterPin")));
@property (class, readonly) OnoTransactionState *enterPinFinal __attribute__((swift_name("enterPinFinal")));
@property (class, readonly) OnoTransactionState *processing __attribute__((swift_name("processing")));
@property (class, readonly) OnoTransactionState *checkSwitchFallback __attribute__((swift_name("checkSwitchFallback")));
@property (class, readonly) OnoTransactionState *fallback __attribute__((swift_name("fallback")));
@property (class, readonly) OnoTransactionState *aborting __attribute__((swift_name("aborting")));
@property (class, readonly) OnoTransactionState *aborted __attribute__((swift_name("aborted")));
@property (class, readonly) OnoTransactionState *error __attribute__((swift_name("error")));
@property (class, readonly) OnoTransactionState *declined __attribute__((swift_name("declined")));
@property (class, readonly) OnoTransactionState *approved __attribute__((swift_name("approved")));
@property (class, readonly) OnoTransactionState *signatureRequired __attribute__((swift_name("signatureRequired")));
@property (class, readonly) OnoTransactionState *signatureProvided __attribute__((swift_name("signatureProvided")));
@property (class, readonly) OnoTransactionState *submittingSignature __attribute__((swift_name("submittingSignature")));
@property (class, readonly) OnoTransactionState *signatureSubmitted __attribute__((swift_name("signatureSubmitted")));
- (int32_t)compareToOther:(OnoTransactionState *)other __attribute__((swift_name("compareTo(other:)")));
@property (readonly) BOOL postToCoreApprovedApi __attribute__((swift_name("postToCoreApprovedApi")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionState.Companion")))
@interface OnoTransactionStateCompanion : OnoBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (readonly) NSSet<OnoTransactionState *> *abortableStates __attribute__((swift_name("abortableStates")));
@property (readonly) NSSet<OnoTransactionState *> *awaitingUserInput __attribute__((swift_name("awaitingUserInput")));
@property (readonly) NSSet<OnoTransactionState *> *busyStates __attribute__((swift_name("busyStates")));
@property (readonly) NSSet<OnoTransactionState *> *completedStates __attribute__((swift_name("completedStates")));
@property (readonly) NSSet<OnoTransactionState *> *declinedStates __attribute__((swift_name("declinedStates")));
@property (readonly) NSSet<OnoTransactionState *> *fallbackEnabledStates __attribute__((swift_name("fallbackEnabledStates")));
@property (readonly) NSSet<OnoTransactionState *> *finalizingStates __attribute__((swift_name("finalizingStates")));
@property (readonly) NSSet<OnoTransactionState *> *nonAbortableStates __attribute__((swift_name("nonAbortableStates")));
@property (readonly) NSSet<OnoTransactionState *> *pinEntryStates __attribute__((swift_name("pinEntryStates")));
@property (readonly) NSSet<OnoTransactionState *> *preAuthorizationSteps __attribute__((swift_name("preAuthorizationSteps")));
@property (readonly) NSSet<OnoTransactionState *> *statesRequiringBackgroundReject __attribute__((swift_name("statesRequiringBackgroundReject")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionType")))
@interface OnoTransactionType : OnoKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) OnoTransactionType *nfc __attribute__((swift_name("nfc")));
@property (class, readonly) OnoTransactionType *chip __attribute__((swift_name("chip")));
@property (class, readonly) OnoTransactionType *swipe __attribute__((swift_name("swipe")));
@property (class, readonly) OnoTransactionType *chipFallbackToSwipe __attribute__((swift_name("chipFallbackToSwipe")));
@property (class, readonly) OnoTransactionType *nfcFallbackToChip __attribute__((swift_name("nfcFallbackToChip")));
- (int32_t)compareToOther:(OnoTransactionType *)other __attribute__((swift_name("compareTo(other:)")));
- (BOOL)isChip __attribute__((swift_name("isChip()")));
@end;

__attribute__((swift_name("QRTransactionActions")))
@interface OnoQRTransactionActions : OnoBase
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("QRTransactionActions.ChargeTransactionFailed")))
@interface OnoQRTransactionActionsChargeTransactionFailed : OnoAction
- (instancetype)initWithErrorDetails:(OnoErrorDetails *)errorDetails maybeOnoTransactionId:(NSString * _Nullable)maybeOnoTransactionId __attribute__((swift_name("init(errorDetails:maybeOnoTransactionId:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoErrorDetails *)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (OnoQRTransactionActionsChargeTransactionFailed *)doCopyErrorDetails:(OnoErrorDetails *)errorDetails maybeOnoTransactionId:(NSString * _Nullable)maybeOnoTransactionId __attribute__((swift_name("doCopy(errorDetails:maybeOnoTransactionId:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoErrorDetails *errorDetails __attribute__((swift_name("errorDetails")));
@property (readonly) NSString * _Nullable maybeOnoTransactionId __attribute__((swift_name("maybeOnoTransactionId")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("QRTransactionActions.ChargeTransactionResponse")))
@interface OnoQRTransactionActionsChargeTransactionResponse : OnoAction
- (instancetype)initWithSwitchTransactionId:(NSString *)switchTransactionId chargeResult:(NSString *)chargeResult qrCode:(NSString *)qrCode __attribute__((swift_name("init(switchTransactionId:chargeResult:qrCode:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (OnoQRTransactionActionsChargeTransactionResponse *)doCopySwitchTransactionId:(NSString *)switchTransactionId chargeResult:(NSString *)chargeResult qrCode:(NSString *)qrCode __attribute__((swift_name("doCopy(switchTransactionId:chargeResult:qrCode:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (OnoQRPaymentInfo *)toQrPaymentInto __attribute__((swift_name("toQrPaymentInto()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *chargeResult __attribute__((swift_name("chargeResult")));
@property (readonly) NSString *qrCode __attribute__((swift_name("qrCode")));
@property (readonly) NSString *switchTransactionId __attribute__((swift_name("switchTransactionId")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("QRTransactionActions.FailTransaction")))
@interface OnoQRTransactionActionsFailTransaction : OnoAction
- (instancetype)initWithErrorDetails:(OnoErrorDetails *)errorDetails __attribute__((swift_name("init(errorDetails:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoErrorDetails *)component1 __attribute__((swift_name("component1()")));
- (OnoQRTransactionActionsFailTransaction *)doCopyErrorDetails:(OnoErrorDetails *)errorDetails __attribute__((swift_name("doCopy(errorDetails:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoErrorDetails *errorDetails __attribute__((swift_name("errorDetails")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("QRTransactionActions.PollTransactionFailed")))
@interface OnoQRTransactionActionsPollTransactionFailed : OnoAction
- (instancetype)initWithErrorDetails:(OnoErrorDetails *)errorDetails maybeOnoTransactionId:(NSString * _Nullable)maybeOnoTransactionId __attribute__((swift_name("init(errorDetails:maybeOnoTransactionId:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoErrorDetails *)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (OnoQRTransactionActionsPollTransactionFailed *)doCopyErrorDetails:(OnoErrorDetails *)errorDetails maybeOnoTransactionId:(NSString * _Nullable)maybeOnoTransactionId __attribute__((swift_name("doCopy(errorDetails:maybeOnoTransactionId:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoErrorDetails *errorDetails __attribute__((swift_name("errorDetails")));
@property (readonly) NSString * _Nullable maybeOnoTransactionId __attribute__((swift_name("maybeOnoTransactionId")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("QRTransactionActions.PollTransactionResponse")))
@interface OnoQRTransactionActionsPollTransactionResponse : OnoAction
- (instancetype)initWithSwitchTransactionId:(NSString *)switchTransactionId chargeResult:(NSString *)chargeResult qrCode:(NSString *)qrCode isoResponse:(NSString * _Nullable)isoResponse chargeResultMessage:(NSString * _Nullable)chargeResultMessage rrn:(NSString * _Nullable)rrn authorizationId:(NSString * _Nullable)authorizationId msisdn:(NSString * _Nullable)msisdn accountType:(NSString * _Nullable)accountType cardType:(NSString * _Nullable)cardType binNumber:(NSString * _Nullable)binNumber panLast4Digits:(NSString * _Nullable)panLast4Digits cardHolderName:(NSString * _Nullable)cardHolderName wallet:(NSString * _Nullable)wallet __attribute__((swift_name("init(switchTransactionId:chargeResult:qrCode:isoResponse:chargeResultMessage:rrn:authorizationId:msisdn:accountType:cardType:binNumber:panLast4Digits:cardHolderName:wallet:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component10 __attribute__((swift_name("component10()")));
- (NSString * _Nullable)component11 __attribute__((swift_name("component11()")));
- (NSString * _Nullable)component12 __attribute__((swift_name("component12()")));
- (NSString * _Nullable)component13 __attribute__((swift_name("component13()")));
- (NSString * _Nullable)component14 __attribute__((swift_name("component14()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (NSString * _Nullable)component4 __attribute__((swift_name("component4()")));
- (NSString * _Nullable)component5 __attribute__((swift_name("component5()")));
- (NSString * _Nullable)component6 __attribute__((swift_name("component6()")));
- (NSString * _Nullable)component7 __attribute__((swift_name("component7()")));
- (NSString * _Nullable)component8 __attribute__((swift_name("component8()")));
- (NSString * _Nullable)component9 __attribute__((swift_name("component9()")));
- (OnoQRTransactionActionsPollTransactionResponse *)doCopySwitchTransactionId:(NSString *)switchTransactionId chargeResult:(NSString *)chargeResult qrCode:(NSString *)qrCode isoResponse:(NSString * _Nullable)isoResponse chargeResultMessage:(NSString * _Nullable)chargeResultMessage rrn:(NSString * _Nullable)rrn authorizationId:(NSString * _Nullable)authorizationId msisdn:(NSString * _Nullable)msisdn accountType:(NSString * _Nullable)accountType cardType:(NSString * _Nullable)cardType binNumber:(NSString * _Nullable)binNumber panLast4Digits:(NSString * _Nullable)panLast4Digits cardHolderName:(NSString * _Nullable)cardHolderName wallet:(NSString * _Nullable)wallet __attribute__((swift_name("doCopy(switchTransactionId:chargeResult:qrCode:isoResponse:chargeResultMessage:rrn:authorizationId:msisdn:accountType:cardType:binNumber:panLast4Digits:cardHolderName:wallet:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (OnoQRPaymentInfo *)toPaymentInfo __attribute__((swift_name("toPaymentInfo()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable accountType __attribute__((swift_name("accountType")));
@property (readonly) NSString * _Nullable authorizationId __attribute__((swift_name("authorizationId")));
@property (readonly) NSString * _Nullable binNumber __attribute__((swift_name("binNumber")));
@property (readonly) NSString * _Nullable cardHolderName __attribute__((swift_name("cardHolderName")));
@property (readonly) NSString * _Nullable cardType __attribute__((swift_name("cardType")));
@property (readonly) NSString *chargeResult __attribute__((swift_name("chargeResult")));
@property (readonly) NSString * _Nullable chargeResultMessage __attribute__((swift_name("chargeResultMessage")));
@property (readonly) NSString * _Nullable isoResponse __attribute__((swift_name("isoResponse")));
@property (readonly) NSString * _Nullable msisdn __attribute__((swift_name("msisdn")));
@property (readonly) NSString * _Nullable panLast4Digits __attribute__((swift_name("panLast4Digits")));
@property (readonly) NSString *qrCode __attribute__((swift_name("qrCode")));
@property (readonly) NSString * _Nullable rrn __attribute__((swift_name("rrn")));
@property (readonly) NSString *switchTransactionId __attribute__((swift_name("switchTransactionId")));
@property (readonly) NSString * _Nullable wallet __attribute__((swift_name("wallet")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("QRTransactionActions.QrTransactionConfirmed")))
@interface OnoQRTransactionActionsQrTransactionConfirmed : OnoAction
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("QRTransactionActions.QrTransactionRejected")))
@interface OnoQRTransactionActionsQrTransactionRejected : OnoAction
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("QRTransactionActions.StartTransaction")))
@interface OnoQRTransactionActionsStartTransaction : OnoAction
- (instancetype)initWithClientTransactionUUID:(NSString *)clientTransactionUUID amountInCents:(int64_t)amountInCents currency:(NSString *)currency initiatedBy:(OnoTransactionInitiator *)initiatedBy __attribute__((swift_name("init(clientTransactionUUID:amountInCents:currency:initiatedBy:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (int64_t)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (OnoTransactionInitiator *)component4 __attribute__((swift_name("component4()")));
- (OnoQRTransactionActionsStartTransaction *)doCopyClientTransactionUUID:(NSString *)clientTransactionUUID amountInCents:(int64_t)amountInCents currency:(NSString *)currency initiatedBy:(OnoTransactionInitiator *)initiatedBy __attribute__((swift_name("doCopy(clientTransactionUUID:amountInCents:currency:initiatedBy:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int64_t amountInCents __attribute__((swift_name("amountInCents")));
@property (readonly) NSString *clientTransactionUUID __attribute__((swift_name("clientTransactionUUID")));
@property (readonly) NSString *currency __attribute__((swift_name("currency")));
@property (readonly) OnoTransactionInitiator *initiatedBy __attribute__((swift_name("initiatedBy")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("QRTransactionActions.TransactionCompleted")))
@interface OnoQRTransactionActionsTransactionCompleted : OnoAction
- (instancetype)initWithMethodName:(NSString *)methodName __attribute__((swift_name("init(methodName:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
@property (readonly) NSString *methodName __attribute__((swift_name("methodName")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("QRTransactionActions.TransactionConfirmationFailed")))
@interface OnoQRTransactionActionsTransactionConfirmationFailed : OnoAction
- (instancetype)initWithErrorDetails:(OnoErrorDetails *)errorDetails statusCode:(OnoInt * _Nullable)statusCode __attribute__((swift_name("init(errorDetails:statusCode:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoErrorDetails *)component1 __attribute__((swift_name("component1()")));
- (OnoInt * _Nullable)component2 __attribute__((swift_name("component2()")));
- (OnoQRTransactionActionsTransactionConfirmationFailed *)doCopyErrorDetails:(OnoErrorDetails *)errorDetails statusCode:(OnoInt * _Nullable)statusCode __attribute__((swift_name("doCopy(errorDetails:statusCode:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoErrorDetails *errorDetails __attribute__((swift_name("errorDetails")));
@property (readonly) OnoInt * _Nullable statusCode __attribute__((swift_name("statusCode")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("QRTransactionActions.TransactionRejectionByIdFailed")))
@interface OnoQRTransactionActionsTransactionRejectionByIdFailed : OnoAction
- (instancetype)initWithErrorDetails:(OnoErrorDetails *)errorDetails statusCode:(OnoInt * _Nullable)statusCode __attribute__((swift_name("init(errorDetails:statusCode:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoErrorDetails *)component1 __attribute__((swift_name("component1()")));
- (OnoInt * _Nullable)component2 __attribute__((swift_name("component2()")));
- (OnoQRTransactionActionsTransactionRejectionByIdFailed *)doCopyErrorDetails:(OnoErrorDetails *)errorDetails statusCode:(OnoInt * _Nullable)statusCode __attribute__((swift_name("doCopy(errorDetails:statusCode:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoErrorDetails *errorDetails __attribute__((swift_name("errorDetails")));
@property (readonly) OnoInt * _Nullable statusCode __attribute__((swift_name("statusCode")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("QRTransactionActions.TransactionRejectionFailed")))
@interface OnoQRTransactionActionsTransactionRejectionFailed : OnoAction
- (instancetype)initWithErrorDetails:(OnoErrorDetails *)errorDetails statusCode:(OnoInt * _Nullable)statusCode __attribute__((swift_name("init(errorDetails:statusCode:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoErrorDetails *)component1 __attribute__((swift_name("component1()")));
- (OnoInt * _Nullable)component2 __attribute__((swift_name("component2()")));
- (OnoQRTransactionActionsTransactionRejectionFailed *)doCopyErrorDetails:(OnoErrorDetails *)errorDetails statusCode:(OnoInt * _Nullable)statusCode __attribute__((swift_name("doCopy(errorDetails:statusCode:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoErrorDetails *errorDetails __attribute__((swift_name("errorDetails")));
@property (readonly) OnoInt * _Nullable statusCode __attribute__((swift_name("statusCode")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("QRTransactionCommandDriver")))
@interface OnoQRTransactionCommandDriver : OnoCommandDriver
- (instancetype)initWithQrTransactionListener:(id<OnoQRTransactionListener>)qrTransactionListener __attribute__((swift_name("init(qrTransactionListener:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (void)receiveCommandCommand:(OnoCommand *)command __attribute__((swift_name("receiveCommand(command:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("QRTransactionCommands")))
@interface OnoQRTransactionCommands : OnoBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("QRTransactionCommands.ChargeTransaction")))
@interface OnoQRTransactionCommandsChargeTransaction : OnoCommand
- (instancetype)initWithTransaction:(OnoQRTransaction *)transaction __attribute__((swift_name("init(transaction:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoQRTransaction *)component1 __attribute__((swift_name("component1()")));
- (OnoQRTransactionCommandsChargeTransaction *)doCopyTransaction:(OnoQRTransaction *)transaction __attribute__((swift_name("doCopy(transaction:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoQRTransaction *transaction __attribute__((swift_name("transaction")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("QRTransactionCommands.ConfirmQrTransaction")))
@interface OnoQRTransactionCommandsConfirmQrTransaction : OnoCommand
- (instancetype)initWithSwitchTransactionId:(NSString *)switchTransactionId sdkUniqueId:(NSString *)sdkUniqueId qrPaymentInfo:(OnoQRPaymentInfo *)qrPaymentInfo __attribute__((swift_name("init(switchTransactionId:sdkUniqueId:qrPaymentInfo:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (OnoQRPaymentInfo *)component3 __attribute__((swift_name("component3()")));
- (OnoQRTransactionCommandsConfirmQrTransaction *)doCopySwitchTransactionId:(NSString *)switchTransactionId sdkUniqueId:(NSString *)sdkUniqueId qrPaymentInfo:(OnoQRPaymentInfo *)qrPaymentInfo __attribute__((swift_name("doCopy(switchTransactionId:sdkUniqueId:qrPaymentInfo:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoQRPaymentInfo *qrPaymentInfo __attribute__((swift_name("qrPaymentInfo")));
@property (readonly) NSString *sdkUniqueId __attribute__((swift_name("sdkUniqueId")));
@property (readonly) NSString *switchTransactionId __attribute__((swift_name("switchTransactionId")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("QRTransactionCommands.PollChargeStatus")))
@interface OnoQRTransactionCommandsPollChargeStatus : OnoCommand
- (instancetype)initWithSwitchTransactionId:(NSString *)switchTransactionId __attribute__((swift_name("init(switchTransactionId:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoQRTransactionCommandsPollChargeStatus *)doCopySwitchTransactionId:(NSString *)switchTransactionId __attribute__((swift_name("doCopy(switchTransactionId:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *switchTransactionId __attribute__((swift_name("switchTransactionId")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("QRTransactionCommands.PresentQrCode")))
@interface OnoQRTransactionCommandsPresentQrCode : OnoCommand
- (instancetype)initWithQrCode:(NSString *)qrCode qrTransaction:(OnoQRTransaction *)qrTransaction __attribute__((swift_name("init(qrCode:qrTransaction:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoQRTransaction *)component2 __attribute__((swift_name("component2()")));
- (OnoQRTransactionCommandsPresentQrCode *)doCopyQrCode:(NSString *)qrCode qrTransaction:(OnoQRTransaction *)qrTransaction __attribute__((swift_name("doCopy(qrCode:qrTransaction:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *qrCode __attribute__((swift_name("qrCode")));
@property (readonly) OnoQRTransaction *qrTransaction __attribute__((swift_name("qrTransaction")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("QRTransactionCommands.RejectQrTransaction")))
@interface OnoQRTransactionCommandsRejectQrTransaction : OnoCommand
- (instancetype)initWithSwitchTransactionId:(NSString *)switchTransactionId sdkUniqueId:(NSString * _Nullable)sdkUniqueId qrPaymentInfo:(OnoQRPaymentInfo *)qrPaymentInfo rejectReason:(NSString *)rejectReason __attribute__((swift_name("init(switchTransactionId:sdkUniqueId:qrPaymentInfo:rejectReason:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (OnoQRPaymentInfo *)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (OnoQRTransactionCommandsRejectQrTransaction *)doCopySwitchTransactionId:(NSString *)switchTransactionId sdkUniqueId:(NSString * _Nullable)sdkUniqueId qrPaymentInfo:(OnoQRPaymentInfo *)qrPaymentInfo rejectReason:(NSString *)rejectReason __attribute__((swift_name("doCopy(switchTransactionId:sdkUniqueId:qrPaymentInfo:rejectReason:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoQRPaymentInfo *qrPaymentInfo __attribute__((swift_name("qrPaymentInfo")));
@property (readonly) NSString *rejectReason __attribute__((swift_name("rejectReason")));
@property (readonly) NSString * _Nullable sdkUniqueId __attribute__((swift_name("sdkUniqueId")));
@property (readonly) NSString *switchTransactionId __attribute__((swift_name("switchTransactionId")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("QRTransactionCommands.RejectQrTransactionByClientId")))
@interface OnoQRTransactionCommandsRejectQrTransactionByClientId : OnoCommand
- (instancetype)initWithClientTransactionId:(NSString *)clientTransactionId sdkUniqueId:(NSString *)sdkUniqueId rejectReason:(NSString *)rejectReason __attribute__((swift_name("init(clientTransactionId:sdkUniqueId:rejectReason:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (OnoQRTransactionCommandsRejectQrTransactionByClientId *)doCopyClientTransactionId:(NSString *)clientTransactionId sdkUniqueId:(NSString *)sdkUniqueId rejectReason:(NSString *)rejectReason __attribute__((swift_name("doCopy(clientTransactionId:sdkUniqueId:rejectReason:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *clientTransactionId __attribute__((swift_name("clientTransactionId")));
@property (readonly) NSString *rejectReason __attribute__((swift_name("rejectReason")));
@property (readonly) NSString *sdkUniqueId __attribute__((swift_name("sdkUniqueId")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("QRTransactionCommands.TransactionCompleted")))
@interface OnoQRTransactionCommandsTransactionCompleted : OnoCommand
- (instancetype)initWithQrTransaction:(OnoQRTransaction *)qrTransaction __attribute__((swift_name("init(qrTransaction:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoQRTransaction *)component1 __attribute__((swift_name("component1()")));
- (OnoQRTransactionCommandsTransactionCompleted *)doCopyQrTransaction:(OnoQRTransaction *)qrTransaction __attribute__((swift_name("doCopy(qrTransaction:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoQRTransaction *qrTransaction __attribute__((swift_name("qrTransaction")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("QRTransactionCommands.TransactionStartFailed")))
@interface OnoQRTransactionCommandsTransactionStartFailed : OnoCommand
- (instancetype)initWithErrorDetails:(OnoErrorDetails *)errorDetails __attribute__((swift_name("init(errorDetails:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoErrorDetails *)component1 __attribute__((swift_name("component1()")));
- (OnoQRTransactionCommandsTransactionStartFailed *)doCopyErrorDetails:(OnoErrorDetails *)errorDetails __attribute__((swift_name("doCopy(errorDetails:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoErrorDetails *errorDetails __attribute__((swift_name("errorDetails")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("QRTransactionData")))
@interface OnoQRTransactionData : OnoBase <OnoStoreData>
- (instancetype)initWithTransaction:(OnoQRTransaction * _Nullable)transaction __attribute__((swift_name("init(transaction:)"))) __attribute__((objc_designated_initializer));
- (OnoQRTransaction * _Nullable)component1 __attribute__((swift_name("component1()")));
- (OnoQRTransactionData *)doCopyTransaction:(OnoQRTransaction * _Nullable)transaction __attribute__((swift_name("doCopy(transaction:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoQRTransaction * _Nullable transaction __attribute__((swift_name("transaction")));
@end;

__attribute__((swift_name("QRTransactionListener")))
@protocol OnoQRTransactionListener
@required
- (void)presentQrCodeQrCode:(NSString *)qrCode transaction:(OnoQRTransaction *)transaction __attribute__((swift_name("presentQrCode(qrCode:transaction:)")));
- (void)qrTransactionCompletedTransaction:(OnoQRTransaction *)transaction __attribute__((swift_name("qrTransactionCompleted(transaction:)")));
- (void)qrTransactionStartFailedError:(OnoErrorDetails *)error __attribute__((swift_name("qrTransactionStartFailed(error:)")));
- (void)qrTransactionUpdatedTransaction:(OnoQRTransaction * _Nullable)transaction __attribute__((swift_name("qrTransactionUpdated(transaction:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("QRTransactionListenerHandler")))
@interface OnoQRTransactionListenerHandler : OnoBase <OnoQRTransactionListener>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)presentQrCodeQrCode:(NSString *)qrCode transaction:(OnoQRTransaction *)transaction __attribute__((swift_name("presentQrCode(qrCode:transaction:)")));
- (void)qrTransactionCompletedTransaction:(OnoQRTransaction *)transaction __attribute__((swift_name("qrTransactionCompleted(transaction:)")));
- (void)qrTransactionStartFailedError:(OnoErrorDetails *)error __attribute__((swift_name("qrTransactionStartFailed(error:)")));
- (void)qrTransactionUpdatedTransaction:(OnoQRTransaction * _Nullable)transaction __attribute__((swift_name("qrTransactionUpdated(transaction:)")));
@property (readonly) NSString *TAG __attribute__((swift_name("TAG")));
@property id<OnoQRTransactionListener> _Nullable listener __attribute__((swift_name("listener")));
@property (readonly) NSArray<OnoStoreListener *> *storeListeners __attribute__((swift_name("storeListeners")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("QRTransactionStateMachine")))
@interface OnoQRTransactionStateMachine : OnoBase <OnoStateMachine>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)qRTransactionStateMachine __attribute__((swift_name("init()")));
- (OnoQRTransactionData *)castStoreDataStoreData:(id<OnoStoreData>)storeData __attribute__((swift_name("castStoreData(storeData:)")));
- (OnoStateMachineResult *)consumeActionStoreData:(id<OnoStoreData>)storeData action:(OnoAction *)action __attribute__((swift_name("consumeAction(storeData:action:)")));
- (OnoQRTransactionData *)getCurrentStateStore:(OnoStore *)store __attribute__((swift_name("getCurrentState(store:)")));
- (OnoQRTransactionData *)getCurrentStateState:(NSDictionary<NSString *, id<OnoStoreData>> *)state __attribute__((swift_name("getCurrentState(state:)")));
- (OnoQRTransactionData *)getStoreDataState:(NSDictionary<NSString *, id<OnoStoreData>> *)state __attribute__((swift_name("getStoreData(state:)")));
- (OnoQRTransaction * _Nullable)getTransactionState:(NSDictionary<NSString *, id<OnoStoreData>> *)state __attribute__((swift_name("getTransaction(state:)")));
@property (readonly) OnoQRTransactionData *initialState __attribute__((swift_name("initialState")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("QRPaymentInfo")))
@interface OnoQRPaymentInfo : OnoBase
- (instancetype)initWithCode:(NSString *)code rrn:(NSString * _Nullable)rrn authorizationId:(NSString * _Nullable)authorizationId msisdn:(NSString * _Nullable)msisdn accountType:(NSString * _Nullable)accountType cardType:(NSString * _Nullable)cardType binNumber:(NSString * _Nullable)binNumber panLast4Digits:(NSString * _Nullable)panLast4Digits cardHolderName:(NSString * _Nullable)cardHolderName wallet:(NSString * _Nullable)wallet __attribute__((swift_name("init(code:rrn:authorizationId:msisdn:accountType:cardType:binNumber:panLast4Digits:cardHolderName:wallet:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component10 __attribute__((swift_name("component10()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (NSString * _Nullable)component3 __attribute__((swift_name("component3()")));
- (NSString * _Nullable)component4 __attribute__((swift_name("component4()")));
- (NSString * _Nullable)component5 __attribute__((swift_name("component5()")));
- (NSString * _Nullable)component6 __attribute__((swift_name("component6()")));
- (NSString * _Nullable)component7 __attribute__((swift_name("component7()")));
- (NSString * _Nullable)component8 __attribute__((swift_name("component8()")));
- (NSString * _Nullable)component9 __attribute__((swift_name("component9()")));
- (OnoQRPaymentInfo *)doCopyCode:(NSString *)code rrn:(NSString * _Nullable)rrn authorizationId:(NSString * _Nullable)authorizationId msisdn:(NSString * _Nullable)msisdn accountType:(NSString * _Nullable)accountType cardType:(NSString * _Nullable)cardType binNumber:(NSString * _Nullable)binNumber panLast4Digits:(NSString * _Nullable)panLast4Digits cardHolderName:(NSString * _Nullable)cardHolderName wallet:(NSString * _Nullable)wallet __attribute__((swift_name("doCopy(code:rrn:authorizationId:msisdn:accountType:cardType:binNumber:panLast4Digits:cardHolderName:wallet:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSDictionary<NSString *, id> *)toMap __attribute__((swift_name("toMap()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable accountType __attribute__((swift_name("accountType")));
@property (readonly) NSString * _Nullable authorizationId __attribute__((swift_name("authorizationId")));
@property (readonly) NSString * _Nullable binNumber __attribute__((swift_name("binNumber")));
@property (readonly) NSString * _Nullable cardHolderName __attribute__((swift_name("cardHolderName")));
@property (readonly) NSString * _Nullable cardType __attribute__((swift_name("cardType")));
@property (readonly) NSString *code __attribute__((swift_name("code")));
@property (readonly) NSString * _Nullable msisdn __attribute__((swift_name("msisdn")));
@property (readonly) NSString * _Nullable panLast4Digits __attribute__((swift_name("panLast4Digits")));
@property (readonly) NSString * _Nullable rrn __attribute__((swift_name("rrn")));
@property (readonly) NSString * _Nullable wallet __attribute__((swift_name("wallet")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("QRTransaction")))
@interface OnoQRTransaction : OnoATransaction
- (instancetype)initWithClientTransactionId:(NSString *)clientTransactionId amountInCents:(int64_t)amountInCents currency:(OnoCurrency *)currency initiatedBy:(OnoTransactionInitiator *)initiatedBy state:(OnoQRTransactionState *)state switchTransactionId:(NSString * _Nullable)switchTransactionId paymentInfo:(OnoQRPaymentInfo * _Nullable)paymentInfo transactionError:(OnoTransactionError * _Nullable)transactionError sdkTransactionId:(NSString *)sdkTransactionId chargeResult:(NSString * _Nullable)chargeResult paymentType:(OnoPaymentType *)paymentType confirmationRetries:(int32_t)confirmationRetries __attribute__((swift_name("init(clientTransactionId:amountInCents:currency:initiatedBy:state:switchTransactionId:paymentInfo:transactionError:sdkTransactionId:chargeResult:paymentType:confirmationRetries:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component10 __attribute__((swift_name("component10()")));
- (OnoPaymentType *)component11 __attribute__((swift_name("component11()")));
- (int32_t)component12 __attribute__((swift_name("component12()")));
- (int64_t)component2 __attribute__((swift_name("component2()")));
- (OnoCurrency *)component3 __attribute__((swift_name("component3()")));
- (OnoTransactionInitiator *)component4 __attribute__((swift_name("component4()")));
- (OnoQRTransactionState *)component5 __attribute__((swift_name("component5()")));
- (NSString * _Nullable)component6 __attribute__((swift_name("component6()")));
- (OnoQRPaymentInfo * _Nullable)component7 __attribute__((swift_name("component7()")));
- (OnoTransactionError * _Nullable)component8 __attribute__((swift_name("component8()")));
- (NSString *)component9 __attribute__((swift_name("component9()")));
- (OnoQRTransaction *)doCopyClientTransactionId:(NSString *)clientTransactionId amountInCents:(int64_t)amountInCents currency:(OnoCurrency *)currency initiatedBy:(OnoTransactionInitiator *)initiatedBy state:(OnoQRTransactionState *)state switchTransactionId:(NSString * _Nullable)switchTransactionId paymentInfo:(OnoQRPaymentInfo * _Nullable)paymentInfo transactionError:(OnoTransactionError * _Nullable)transactionError sdkTransactionId:(NSString *)sdkTransactionId chargeResult:(NSString * _Nullable)chargeResult paymentType:(OnoPaymentType *)paymentType confirmationRetries:(int32_t)confirmationRetries __attribute__((swift_name("doCopy(clientTransactionId:amountInCents:currency:initiatedBy:state:switchTransactionId:paymentInfo:transactionError:sdkTransactionId:chargeResult:paymentType:confirmationRetries:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSString *)getCoreClientTransactionId __attribute__((swift_name("getCoreClientTransactionId()")));
- (NSDictionary<NSString *, id> *)getPaymentTypeSpecificDebugInfo __attribute__((swift_name("getPaymentTypeSpecificDebugInfo()")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString * _Nullable)internalDescription __attribute__((swift_name("internalDescription()")));
- (BOOL)isAbortable __attribute__((swift_name("isAbortable()")));
- (BOOL)isBusy __attribute__((swift_name("isBusy()")));
- (BOOL)isCompleted __attribute__((swift_name("isCompleted()")));
- (BOOL)isFinalizing __attribute__((swift_name("isFinalizing()")));
- (NSString *)description __attribute__((swift_name("description()")));
- (NSString * _Nullable)userDescription __attribute__((swift_name("userDescription()")));
@property (readonly) int64_t amountInCents __attribute__((swift_name("amountInCents")));
@property (readonly) NSString * _Nullable chargeResult __attribute__((swift_name("chargeResult")));
@property (readonly) NSString *clientTransactionId __attribute__((swift_name("clientTransactionId")));
@property (readonly) int32_t confirmationRetries __attribute__((swift_name("confirmationRetries")));
@property (readonly) OnoCurrency *currency __attribute__((swift_name("currency")));
@property (readonly) OnoTransactionInitiator *initiatedBy __attribute__((swift_name("initiatedBy")));
@property (readonly) OnoQRPaymentInfo * _Nullable paymentInfo __attribute__((swift_name("paymentInfo")));
@property (readonly) OnoPaymentType *paymentType __attribute__((swift_name("paymentType")));
@property (readonly) NSString *sdkTransactionId __attribute__((swift_name("sdkTransactionId")));
@property (readonly) OnoQRTransactionState *state __attribute__((swift_name("state")));
@property (readonly) NSString * _Nullable switchTransactionId __attribute__((swift_name("switchTransactionId")));
@property (readonly) OnoTransactionError * _Nullable transactionError __attribute__((swift_name("transactionError")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("QRTransactionState")))
@interface OnoQRTransactionState : OnoKotlinEnum <OnoATransactionState>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) OnoQRTransactionState *getOnoStatus __attribute__((swift_name("getOnoStatus")));
@property (class, readonly) OnoQRTransactionState *fetching __attribute__((swift_name("fetching")));
@property (class, readonly) OnoQRTransactionState *pollingStatus __attribute__((swift_name("pollingStatus")));
@property (class, readonly) OnoQRTransactionState *approved __attribute__((swift_name("approved")));
@property (class, readonly) OnoQRTransactionState *declined __attribute__((swift_name("declined")));
@property (class, readonly) OnoQRTransactionState *cancelling __attribute__((swift_name("cancelling")));
@property (class, readonly) OnoQRTransactionState *cancelled __attribute__((swift_name("cancelled")));
@property (class, readonly) OnoQRTransactionState *failed __attribute__((swift_name("failed")));
@property (class, readonly) OnoQRTransactionState *confirming __attribute__((swift_name("confirming")));
@property (class, readonly) OnoQRTransactionState *rejecting __attribute__((swift_name("rejecting")));
@property (class, readonly) OnoQRTransactionState *rejected __attribute__((swift_name("rejected")));
- (int32_t)compareToOther:(OnoQRTransactionState *)other __attribute__((swift_name("compareTo(other:)")));
@property (readonly) BOOL postToCoreApprovedApi __attribute__((swift_name("postToCoreApprovedApi")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("QRTransactionState.Companion")))
@interface OnoQRTransactionStateCompanion : OnoBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (readonly) NSSet<OnoQRTransactionState *> *abortableStates __attribute__((swift_name("abortableStates")));
@property (readonly) NSSet<OnoQRTransactionState *> *busyStates __attribute__((swift_name("busyStates")));
@property (readonly) NSSet<OnoQRTransactionState *> *completedStates __attribute__((swift_name("completedStates")));
@property (readonly) NSSet<OnoQRTransactionState *> *finalizingStates __attribute__((swift_name("finalizingStates")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadModule")))
@interface OnoDspreadModule : OnoBase <OnoOnoSDKModule>
- (instancetype)initWithDspreadService:(OnoDspreadServiceProtocol *)dspreadService __attribute__((swift_name("init(dspreadService:)"))) __attribute__((objc_designated_initializer));
@property (readonly) NSArray<OnoCommandDriver *> *commandDrivers __attribute__((swift_name("commandDrivers")));
@property (readonly) NSArray<id<OnoStateMachine>> *stateMachines __attribute__((swift_name("stateMachines")));
@property (readonly) NSArray<OnoStoreListener *> *storeListeners __attribute__((swift_name("storeListeners")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoDspreadUtils")))
@interface OnoOnoDspreadUtils : OnoBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)onoDspreadUtils __attribute__((swift_name("init()")));
- (BOOL)checkDeviceIsDspreadBluetoothDevice:(OnoBluetoothDevice *)bluetoothDevice __attribute__((swift_name("checkDeviceIsDspread(bluetoothDevice:)")));
- (OnoTransactionType *)convertTradeResultToTransactionTypeTradeResult:(OnoOnoQposDoTradeResult *)tradeResult __attribute__((swift_name("convertTradeResultToTransactionType(tradeResult:)")));
- (NSString *)currencyCodeCurrency:(OnoCurrency *)currency __attribute__((swift_name("currencyCode(currency:)")));
- (OnoSensitiveString * _Nullable)getMaskedPanSensitiveTradeData:(OnoSensitiveMap * _Nullable)sensitiveTradeData __attribute__((swift_name("getMaskedPan(sensitiveTradeData:)")));
- (OnoPinEnforcedBy * _Nullable)getPinEnforcerTradeResult:(OnoSensitiveMap * _Nullable)tradeResult cardPromptMode:(OnoCardPromptMode *)cardPromptMode __attribute__((swift_name("getPinEnforcer(tradeResult:cardPromptMode:)")));
- (NSString * _Nullable)getServiceCodeSensitiveTradeData:(OnoSensitiveMap * _Nullable)sensitiveTradeData __attribute__((swift_name("getServiceCode(sensitiveTradeData:)")));
- (BOOL)hasPinDataTradeResult:(OnoSensitiveMap * _Nullable)tradeResult __attribute__((swift_name("hasPinData(tradeResult:)")));
- (NSString *)messageForCompletedTransactionTransaction:(OnoTransaction *)transaction isCardStillInserted:(BOOL)isCardStillInserted __attribute__((swift_name("messageForCompletedTransaction(transaction:isCardStillInserted:)")));
- (OnoTerminalInfo *)qposResponseToTerminalInfoInfo:(NSDictionary<NSString *, NSString *> *)info sdkVersion:(NSString * _Nullable)sdkVersion __attribute__((swift_name("qposResponseToTerminalInfo(info:sdkVersion:)")));
- (NSString *)serialNumberFromDeviceBluetoothDevice:(OnoBluetoothDevice *)bluetoothDevice __attribute__((swift_name("serialNumberFromDevice(bluetoothDevice:)")));
- (NSString * _Nullable)serialNumberFromIDResponseInfo:(NSDictionary<NSString *, NSString *> *)info __attribute__((swift_name("serialNumberFromIDResponse(info:)")));
- (NSString *)terminalDisplayMessageTransactionState:(OnoTransactionState * _Nullable)transactionState isCardStillInserted:(BOOL)isCardStillInserted __attribute__((swift_name("terminalDisplayMessage(transactionState:isCardStillInserted:)")));
@property (readonly) NSString *INVALID_SERIAL_NUMBER __attribute__((swift_name("INVALID_SERIAL_NUMBER")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadConstants")))
@interface OnoDspreadConstants : OnoBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)dspreadConstants __attribute__((swift_name("init()")));
@property (readonly) NSString *BATTERY_PERCENTAGE __attribute__((swift_name("BATTERY_PERCENTAGE")));
@property (readonly) NSString *BOOTLOADER_VERSION __attribute__((swift_name("BOOTLOADER_VERSION")));
@property (readonly) NSString *FIRMWARE_VERSION __attribute__((swift_name("FIRMWARE_VERSION")));
@property (readonly) NSString *HARDWARE_VERSION __attribute__((swift_name("HARDWARE_VERSION")));
@property (readonly) NSString *IS_CHARGING __attribute__((swift_name("IS_CHARGING")));
@property (readonly) NSString *QPOS_ID __attribute__((swift_name("QPOS_ID")));
@property (readonly) NSString *QPOS_MODEL __attribute__((swift_name("QPOS_MODEL")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadConstants.TradeDataKeys")))
@interface OnoDspreadConstantsTradeDataKeys : OnoBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)tradeDataKeys __attribute__((swift_name("init()")));
@property (readonly) NSString *MASKED_PAN __attribute__((swift_name("MASKED_PAN")));
@property (readonly) NSString *PIN_BLOCK __attribute__((swift_name("PIN_BLOCK")));
@property (readonly) NSString *PIN_KSN __attribute__((swift_name("PIN_KSN")));
@property (readonly) NSString *SERVICE_CODE __attribute__((swift_name("SERVICE_CODE")));
@end;

__attribute__((swift_name("DspreadServiceProtocol")))
@interface OnoDspreadServiceProtocol : OnoDispatcher
- (instancetype)initWithDispatch:(void (^)(OnoAction *))dispatch __attribute__((swift_name("init(dispatch:)"))) __attribute__((objc_designated_initializer));
- (void)cancelOnEmvAppSelection __attribute__((swift_name("cancelOnEmvAppSelection()")));
- (void)cancelTrade __attribute__((swift_name("cancelTrade()")));
- (void)checkCardExistsDelay:(OnoLong * _Nullable)delay __attribute__((swift_name("checkCardExists(delay:)")));
- (void)checkTerminalStatus __attribute__((swift_name("checkTerminalStatus()")));
- (void)clearAllQposCommands __attribute__((swift_name("clearAllQposCommands()")));
- (void)clearMessage __attribute__((swift_name("clearMessage()")));
- (void)clearQposCommandQueueCommand:(OnoQueueCommand *)queueCommand __attribute__((swift_name("clearQposCommand(queueCommand:)")));
- (void)connectTerminalAddress:(NSString *)address __attribute__((swift_name("connectTerminal(address:)")));
- (void)delayCheckTerminalStatusDelay:(int64_t)delay statusCheckId:(int32_t)statusCheckId __attribute__((swift_name("delayCheckTerminalStatus(delay:statusCheckId:)")));
- (void)discardNFCBatchDataMessage:(NSString *)message __attribute__((swift_name("discardNFCBatchData(message:)")));
- (void)disconnect __attribute__((swift_name("disconnect()")));
- (void)doEMVTransactionType:(OnoTransactionType *)transactionType __attribute__((swift_name("doEMV(transactionType:)")));
- (void)doTradeTransaction:(OnoTransaction *)transaction transactionConfig:(OnoTransactionConfig *)transactionConfig cardPromptMode:(OnoCardPromptMode *)cardPromptMode keyIndex:(int32_t)keyIndex __attribute__((swift_name("doTrade(transaction:transactionConfig:cardPromptMode:keyIndex:)")));
- (void)fetchNFCBatchData __attribute__((swift_name("fetchNFCBatchData()")));
- (void)getTerminalId __attribute__((swift_name("getTerminalId()")));
- (void)getTerminalInfo __attribute__((swift_name("getTerminalInfo()")));
- (void)performUpdateAvailableUpdate:(OnoAvailableUpdate *)availableUpdate __attribute__((swift_name("performUpdate(availableUpdate:)")));
- (void)quickReset __attribute__((swift_name("quickReset()")));
- (void)reconnectAfterDelayDelay:(int64_t)delay terminal:(OnoTerminal *)terminal __attribute__((swift_name("reconnectAfterDelay(delay:terminal:)")));
- (void)requestPinMaskedPan:(OnoSensitiveString *)maskedPan timeout:(int32_t)timeout keyIndex:(int32_t)keyIndex __attribute__((swift_name("requestPin(maskedPan:timeout:keyIndex:)")));
- (void)resetQpos __attribute__((swift_name("resetQpos()")));
- (void)retrieveEmvConfigVersion __attribute__((swift_name("retrieveEmvConfigVersion()")));
- (void)setAmountAmountInCents:(int64_t)amountInCents currency:(OnoCurrency *)currency __attribute__((swift_name("setAmount(amountInCents:currency:)")));
- (void)setBuzzerStatusEnableSound:(BOOL)enableSound __attribute__((swift_name("setBuzzerStatus(enableSound:)")));
- (void)setShutdownTimeShutdownTime:(int32_t)shutdownTime __attribute__((swift_name("setShutdownTime(shutdownTime:)")));
- (void)setTime __attribute__((swift_name("setTime()")));
- (void)showMessageMessage:(NSString *)message timeoutInSeconds:(OnoInt * _Nullable)timeoutInSeconds maximumMessageTimeout:(int32_t)maximumMessageTimeout timeoutDelay:(int32_t)timeoutDelay __attribute__((swift_name("showMessage(message:timeoutInSeconds:maximumMessageTimeout:timeoutDelay:)")));
- (void)submitAPRCArpc:(OnoSensitiveString *)arpc __attribute__((swift_name("submitAPRC(arpc:)")));
- (void)submitEMVOptionEmvOption:(int32_t)emvOption __attribute__((swift_name("submitEMVOption(emvOption:)")));
- (void)updateEMVEmvApp:(NSString *)emvApp emvCapk:(NSString *)emvCapk __attribute__((swift_name("updateEMV(emvApp:emvCapk:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadService")))
@interface OnoDspreadService : OnoDspreadServiceProtocol
- (instancetype)initWithQposCommandQueue:(OnoTerminalCommandQueue *)qposCommandQueue updateService:(id<OnoIOnoUpdateService>)updateService dispatch:(void (^)(OnoAction *))dispatch __attribute__((swift_name("init(qposCommandQueue:updateService:dispatch:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithDispatch:(void (^)(OnoAction *))dispatch __attribute__((swift_name("init(dispatch:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (void)cancelOnEmvAppSelection __attribute__((swift_name("cancelOnEmvAppSelection()")));
- (void)cancelTrade __attribute__((swift_name("cancelTrade()")));
- (void)checkCardExistsDelay:(OnoLong * _Nullable)delay __attribute__((swift_name("checkCardExists(delay:)")));
- (void)checkTerminalStatus __attribute__((swift_name("checkTerminalStatus()")));
- (void)clearAllQposCommands __attribute__((swift_name("clearAllQposCommands()")));
- (void)clearMessage __attribute__((swift_name("clearMessage()")));
- (void)clearQposCommandQueueCommand:(OnoQueueCommand *)queueCommand __attribute__((swift_name("clearQposCommand(queueCommand:)")));
- (void)connectTerminalAddress:(NSString *)address __attribute__((swift_name("connectTerminal(address:)")));
- (void)delayCheckTerminalStatusDelay:(int64_t)delay statusCheckId:(int32_t)statusCheckId __attribute__((swift_name("delayCheckTerminalStatus(delay:statusCheckId:)")));
- (void)discardNFCBatchDataMessage:(NSString *)message __attribute__((swift_name("discardNFCBatchData(message:)")));
- (void)disconnect __attribute__((swift_name("disconnect()")));
- (void)doEMVTransactionType:(OnoTransactionType *)transactionType __attribute__((swift_name("doEMV(transactionType:)")));
- (void)doTradeTransaction:(OnoTransaction *)transaction transactionConfig:(OnoTransactionConfig *)transactionConfig cardPromptMode:(OnoCardPromptMode *)cardPromptMode keyIndex:(int32_t)keyIndex __attribute__((swift_name("doTrade(transaction:transactionConfig:cardPromptMode:keyIndex:)")));
- (void)fetchNFCBatchData __attribute__((swift_name("fetchNFCBatchData()")));
- (void)getTerminalId __attribute__((swift_name("getTerminalId()")));
- (void)getTerminalInfo __attribute__((swift_name("getTerminalInfo()")));
- (void)performUpdateAvailableUpdate:(OnoAvailableUpdate *)availableUpdate __attribute__((swift_name("performUpdate(availableUpdate:)")));
- (void)quickReset __attribute__((swift_name("quickReset()")));
- (void)reconnectAfterDelayDelay:(int64_t)delay terminal:(OnoTerminal *)terminal __attribute__((swift_name("reconnectAfterDelay(delay:terminal:)")));
- (void)requestPinMaskedPan:(OnoSensitiveString *)maskedPan timeout:(int32_t)timeout keyIndex:(int32_t)keyIndex __attribute__((swift_name("requestPin(maskedPan:timeout:keyIndex:)")));
- (void)resetQpos __attribute__((swift_name("resetQpos()")));
- (void)retrieveEmvConfigVersion __attribute__((swift_name("retrieveEmvConfigVersion()")));
- (void)setAmountAmountInCents:(int64_t)amountInCents currency:(OnoCurrency *)currency __attribute__((swift_name("setAmount(amountInCents:currency:)")));
- (void)setBuzzerStatusEnableSound:(BOOL)enableSound __attribute__((swift_name("setBuzzerStatus(enableSound:)")));
- (void)setShutdownTimeShutdownTime:(int32_t)shutdownTime __attribute__((swift_name("setShutdownTime(shutdownTime:)")));
- (void)setTime __attribute__((swift_name("setTime()")));
- (void)showMessageMessage:(NSString *)message timeoutInSeconds:(OnoInt * _Nullable)timeoutInSeconds maximumMessageTimeout:(int32_t)maximumMessageTimeout timeoutDelay:(int32_t)timeoutDelay __attribute__((swift_name("showMessage(message:timeoutInSeconds:maximumMessageTimeout:timeoutDelay:)")));
- (void)submitAPRCArpc:(OnoSensitiveString *)arpc __attribute__((swift_name("submitAPRC(arpc:)")));
- (void)submitEMVOptionEmvOption:(int32_t)emvOption __attribute__((swift_name("submitEMVOption(emvOption:)")));
- (void)updateEMVEmvApp:(NSString *)emvApp emvCapk:(NSString *)emvCapk __attribute__((swift_name("updateEMV(emvApp:emvCapk:)")));
@property (readonly) NSString *TAG __attribute__((swift_name("TAG")));
@end;

__attribute__((swift_name("MockDspreadService")))
@interface OnoMockDspreadService : OnoDspreadServiceProtocol
- (instancetype)initWithDispatch:(void (^)(OnoAction *))dispatch isUnitTesting:(BOOL)isUnitTesting dateTimeString:(NSString *(^ _Nullable)(NSString *))dateTimeString __attribute__((swift_name("init(dispatch:isUnitTesting:dateTimeString:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithDispatch:(void (^)(OnoAction *))dispatch __attribute__((swift_name("init(dispatch:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (void)cancelOnEmvAppSelection __attribute__((swift_name("cancelOnEmvAppSelection()")));
- (void)cancelTrade __attribute__((swift_name("cancelTrade()")));
- (void)checkCardExistsDelay:(OnoLong * _Nullable)delay __attribute__((swift_name("checkCardExists(delay:)")));
- (void)checkTerminalStatus __attribute__((swift_name("checkTerminalStatus()")));
- (void)clearAllQposCommands __attribute__((swift_name("clearAllQposCommands()")));
- (void)clearMessage __attribute__((swift_name("clearMessage()")));
- (void)clearQposCommandQueueCommand:(OnoQueueCommand *)queueCommand __attribute__((swift_name("clearQposCommand(queueCommand:)")));
- (void)connectTerminalAddress:(NSString *)address __attribute__((swift_name("connectTerminal(address:)")));
- (void)delayCheckTerminalStatusDelay:(int64_t)delay statusCheckId:(int32_t)statusCheckId __attribute__((swift_name("delayCheckTerminalStatus(delay:statusCheckId:)")));
- (void)discardNFCBatchDataMessage:(NSString *)message __attribute__((swift_name("discardNFCBatchData(message:)")));
- (void)disconnect __attribute__((swift_name("disconnect()")));
- (void)doEMVTransactionType:(OnoTransactionType *)transactionType __attribute__((swift_name("doEMV(transactionType:)")));
- (void)doTradeTransaction:(OnoTransaction *)transaction transactionConfig:(OnoTransactionConfig *)transactionConfig cardPromptMode:(OnoCardPromptMode *)cardPromptMode keyIndex:(int32_t)keyIndex __attribute__((swift_name("doTrade(transaction:transactionConfig:cardPromptMode:keyIndex:)")));
- (void)fetchNFCBatchData __attribute__((swift_name("fetchNFCBatchData()")));
- (void)getTerminalId __attribute__((swift_name("getTerminalId()")));
- (void)getTerminalInfo __attribute__((swift_name("getTerminalInfo()")));
- (void)performUpdateAvailableUpdate:(OnoAvailableUpdate *)availableUpdate __attribute__((swift_name("performUpdate(availableUpdate:)")));
- (void)quickReset __attribute__((swift_name("quickReset()")));
- (void)reconnectAfterDelayDelay:(int64_t)delay terminal:(OnoTerminal *)terminal __attribute__((swift_name("reconnectAfterDelay(delay:terminal:)")));
- (void)requestPinMaskedPan:(OnoSensitiveString *)maskedPan timeout:(int32_t)timeout keyIndex:(int32_t)keyIndex __attribute__((swift_name("requestPin(maskedPan:timeout:keyIndex:)")));
- (void)resetQpos __attribute__((swift_name("resetQpos()")));
- (void)retrieveEmvConfigVersion __attribute__((swift_name("retrieveEmvConfigVersion()")));
- (void)setAmountAmountInCents:(int64_t)amountInCents currency:(OnoCurrency *)currency __attribute__((swift_name("setAmount(amountInCents:currency:)")));
- (void)setBuzzerStatusEnableSound:(BOOL)enableSound __attribute__((swift_name("setBuzzerStatus(enableSound:)")));
- (void)setShutdownTimeShutdownTime:(int32_t)shutdownTime __attribute__((swift_name("setShutdownTime(shutdownTime:)")));
- (void)setTime __attribute__((swift_name("setTime()")));
- (void)showMessageMessage:(NSString *)message timeoutInSeconds:(OnoInt * _Nullable)timeoutInSeconds maximumMessageTimeout:(int32_t)maximumMessageTimeout timeoutDelay:(int32_t)timeoutDelay __attribute__((swift_name("showMessage(message:timeoutInSeconds:maximumMessageTimeout:timeoutDelay:)")));
- (void)submitAPRCArpc:(OnoSensitiveString *)arpc __attribute__((swift_name("submitAPRC(arpc:)")));
- (void)submitEMVOptionEmvOption:(int32_t)emvOption __attribute__((swift_name("submitEMVOption(emvOption:)")));
- (void)updateEMVEmvApp:(NSString *)emvApp emvCapk:(NSString *)emvCapk __attribute__((swift_name("updateEMV(emvApp:emvCapk:)")));
@property NSString * _Nullable appliedEMVConfigVersion __attribute__((swift_name("appliedEMVConfigVersion")));
@property BOOL askForSignature __attribute__((swift_name("askForSignature")));
@property OnoCardPromptMode *cardPromptMode __attribute__((swift_name("cardPromptMode")));
@property BOOL chipFallback __attribute__((swift_name("chipFallback")));
@property BOOL declineARPC __attribute__((swift_name("declineARPC")));
@property BOOL emvAppCardRemoved __attribute__((swift_name("emvAppCardRemoved")));
@property BOOL emvAppSelectionTimeout __attribute__((swift_name("emvAppSelectionTimeout")));
@property NSMutableArray<NSString *> *emvApplications __attribute__((swift_name("emvApplications")));
@property BOOL failConnect __attribute__((swift_name("failConnect")));
@property BOOL nfcDeclined __attribute__((swift_name("nfcDeclined")));
@property BOOL nfcFallback __attribute__((swift_name("nfcFallback")));
@property (readonly) OnoSensitiveString *nfcTLV __attribute__((swift_name("nfcTLV")));
@property (readonly) OnoSensitiveMap *nfcTradeData __attribute__((swift_name("nfcTradeData")));
@property OnoMockDspreadServicePinPrompt *promptForPin __attribute__((swift_name("promptForPin")));
@property BOOL returnCheckTerminalStatus __attribute__((swift_name("returnCheckTerminalStatus")));
@property BOOL shouldAutomaticallyApplyUpdate __attribute__((swift_name("shouldAutomaticallyApplyUpdate")));
@property int32_t shouldCardExist __attribute__((swift_name("shouldCardExist")));
@property (readonly) OnoSensitiveMap *swipeTradeData __attribute__((swift_name("swipeTradeData")));
@property OnoSensitiveString *tlv __attribute__((swift_name("tlv")));
@property OnoSensitiveMap *tradeData __attribute__((swift_name("tradeData")));
@property OnoTransactionType *tradeResult __attribute__((swift_name("tradeResult")));
@property OnoKotlinNothing * _Nullable tvr __attribute__((swift_name("tvr")));
@property BOOL unknownErrorOnCancelTrade __attribute__((swift_name("unknownErrorOnCancelTrade")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MockDspreadService.Companion")))
@interface OnoMockDspreadServiceCompanion : OnoBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (readonly) NSMutableArray<NSString *> *appSelectionOptions __attribute__((swift_name("appSelectionOptions")));
@property (readonly) NSString *connectionFailedMessage __attribute__((swift_name("connectionFailedMessage")));
@property (readonly) NSDictionary<NSString *, NSString *> *idInfo __attribute__((swift_name("idInfo")));
@property (readonly) NSString *mockVersion __attribute__((swift_name("mockVersion")));
@property (readonly) OnoTerminal *terminal __attribute__((swift_name("terminal")));
@property (readonly) NSDictionary<NSString *, NSString *> *terminalInfo __attribute__((swift_name("terminalInfo")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MockDspreadService.PinPrompt")))
@interface OnoMockDspreadServicePinPrompt : OnoBase
- (instancetype)initWithAskForPin:(int32_t)askForPin askForPinFinal:(BOOL)askForPinFinal __attribute__((swift_name("init(askForPin:askForPinFinal:)"))) __attribute__((objc_designated_initializer));
- (int32_t)component1 __attribute__((swift_name("component1()")));
- (BOOL)component2 __attribute__((swift_name("component2()")));
- (OnoMockDspreadServicePinPrompt *)doCopyAskForPin:(int32_t)askForPin askForPinFinal:(BOOL)askForPinFinal __attribute__((swift_name("doCopy(askForPin:askForPinFinal:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t askForPin __attribute__((swift_name("askForPin")));
@property (readonly) BOOL askForPinFinal __attribute__((swift_name("askForPinFinal")));
@end;

__attribute__((swift_name("IOnoQposService")))
@protocol OnoIOnoQposService
@required
- (void)cancelSelectEmvApp __attribute__((swift_name("cancelSelectEmvApp()")));
- (void)cancelTrade __attribute__((swift_name("cancelTrade()")));
- (void)connectBTAddress:(NSString *)address __attribute__((swift_name("connectBT(address:)")));
- (void)disconnectBT __attribute__((swift_name("disconnectBT()")));
- (void)doEmvAppEmvOption:(OnoOnoQposEmvOption *)emvOption __attribute__((swift_name("doEmvApp(emvOption:)")));
- (void)doTradeTransaction:(OnoTransaction *)transaction transactionConfig:(OnoTransactionConfig *)transactionConfig cardPromptMode:(OnoCardPromptMode *)cardPromptMode keyIndex:(int32_t)keyIndex __attribute__((swift_name("doTrade(transaction:transactionConfig:cardPromptMode:keyIndex:)")));
- (void)getQposId __attribute__((swift_name("getQposId()")));
- (void)getQposInfo __attribute__((swift_name("getQposInfo()")));
- (NSString *)getSdkVersion __attribute__((swift_name("getSdkVersion()")));
- (void)isCardExistTimeout:(int32_t)timeout __attribute__((swift_name("isCardExist(timeout:)")));
- (void)lcdShowCloseDisplay __attribute__((swift_name("lcdShowCloseDisplay()")));
- (void)lcdShowCustomDisplayLcdModeAlign:(OnoOnoQposLcdModeAlign *)lcdModeAlign customDisplayString:(NSString *)customDisplayString timeout:(int32_t)timeout __attribute__((swift_name("lcdShowCustomDisplay(lcdModeAlign:customDisplayString:timeout:)")));
- (NSDictionary<NSString *, NSString *> *)nfcBatchData __attribute__((swift_name("nfcBatchData()")));
- (void)requestPinMaskedPan:(OnoSensitiveString *)maskedPan timeout:(int32_t)timeout keyIndex:(int32_t)keyIndex __attribute__((swift_name("requestPin(maskedPan:timeout:keyIndex:)")));
- (void)resetPosStatus __attribute__((swift_name("resetPosStatus()")));
- (void)selectEmvAppEmvOption:(int32_t)emvOption __attribute__((swift_name("selectEmvApp(emvOption:)")));
- (void)sendOnlineProcessResultArpc:(NSString *)arpc __attribute__((swift_name("sendOnlineProcessResult(arpc:)")));
- (void)sendTime __attribute__((swift_name("sendTime()")));
- (void)setAmountAmount:(NSString *)amount cashbackAmount:(NSString *)cashbackAmount currencyCode:(NSString *)currencyCode transactionType:(OnoOnoQposTransactionType *)transactionType shouldPosDisplayAmount:(BOOL)shouldPosDisplayAmount __attribute__((swift_name("setAmount(amount:cashbackAmount:currencyCode:transactionType:shouldPosDisplayAmount:)")));
- (void)setAmountIconSymbol:(NSString *)symbol __attribute__((swift_name("setAmountIcon(symbol:)")));
- (void)setBuzzerStatusBuzzerStatus:(int32_t)buzzerStatus __attribute__((swift_name("setBuzzerStatus(buzzerStatus:)")));
- (void)setShutdownTimeShutdownTime:(int32_t)shutdownTime __attribute__((swift_name("setShutdownTime(shutdownTime:)")));
- (void)updateEmvConfigEmvApp:(NSString *)emvApp emvCapk:(NSString *)emvCapk __attribute__((swift_name("updateEmvConfig(emvApp:emvCapk:)")));
@end;

__attribute__((swift_name("IQposSDKService")))
@protocol OnoIQposSDKService <OnoISDKService>
@required
- (void)cancelSelectEmvApp __attribute__((swift_name("cancelSelectEmvApp()")));
- (void)cancelTrade __attribute__((swift_name("cancelTrade()")));
- (void)connectBTAddress:(NSString *)address __attribute__((swift_name("connectBT(address:)")));
- (void)disconnectBT __attribute__((swift_name("disconnectBT()")));
- (void)doEmvAppEmvOption:(OnoOnoQposEmvOption *)emvOption __attribute__((swift_name("doEmvApp(emvOption:)")));
- (void)doTradeTransaction:(OnoTransaction *)transaction transactionConfig:(OnoTransactionConfig *)transactionConfig cardPromptMode:(OnoCardPromptMode *)cardPromptMode keyIndex:(int32_t)keyIndex __attribute__((swift_name("doTrade(transaction:transactionConfig:cardPromptMode:keyIndex:)")));
- (void)getQposId __attribute__((swift_name("getQposId()")));
- (void)getQposInfo __attribute__((swift_name("getQposInfo()")));
- (NSString *)getSdkVersion __attribute__((swift_name("getSdkVersion()")));
- (void)isCardExistTimeout:(int32_t)timeout __attribute__((swift_name("isCardExist(timeout:)")));
- (void)lcdShowCloseDisplay __attribute__((swift_name("lcdShowCloseDisplay()")));
- (void)lcdShowCustomDisplayLcdModeAlign:(OnoOnoQposLcdModeAlign *)lcdModeAlign customDisplayString:(NSString *)customDisplayString timeout:(int32_t)timeout __attribute__((swift_name("lcdShowCustomDisplay(lcdModeAlign:customDisplayString:timeout:)")));
- (NSDictionary<NSString *, NSString *> *)nfcBatchData __attribute__((swift_name("nfcBatchData()")));
- (void)quickReset __attribute__((swift_name("quickReset()")));
- (void)requestPinMaskedPan:(OnoSensitiveString *)maskedPan timeout:(int32_t)timeout keyIndex:(int32_t)keyIndex __attribute__((swift_name("requestPin(maskedPan:timeout:keyIndex:)")));
- (void)resetPosStatus __attribute__((swift_name("resetPosStatus()")));
- (void)retrieveEMVConfigTlv:(NSString *)tlv __attribute__((swift_name("retrieveEMVConfig(tlv:)")));
- (void)selectEmvAppEmvOption:(int32_t)emvOption __attribute__((swift_name("selectEmvApp(emvOption:)")));
- (void)sendOnlineProcessResultArpc:(NSString *)arpc __attribute__((swift_name("sendOnlineProcessResult(arpc:)")));
- (void)sendTime __attribute__((swift_name("sendTime()")));
- (void)setAmountAmount:(NSString *)amount cashbackAmount:(NSString *)cashbackAmount currencyCode:(NSString *)currencyCode transactionType:(OnoOnoQposTransactionType *)transactionType shouldPosDisplayAmount:(BOOL)shouldPosDisplayAmount __attribute__((swift_name("setAmount(amount:cashbackAmount:currencyCode:transactionType:shouldPosDisplayAmount:)")));
- (void)setAmountIconSymbol:(NSString *)symbol __attribute__((swift_name("setAmountIcon(symbol:)")));
- (void)setBuzzerStatusBuzzerStatus:(int32_t)buzzerStatus __attribute__((swift_name("setBuzzerStatus(buzzerStatus:)")));
- (void)setShutdownTimeShutdownTime:(int32_t)shutdownTime __attribute__((swift_name("setShutdownTime(shutdownTime:)")));
- (void)updateEmvConfigEmvApp:(NSString *)emvApp emvCapk:(NSString *)emvCapk __attribute__((swift_name("updateEmvConfig(emvApp:emvCapk:)")));
@end;

__attribute__((swift_name("OnoQposListenerProtocol")))
@interface OnoOnoQposListenerProtocol : OnoDispatcher
- (instancetype)initWithDispatch:(void (^)(OnoAction *))dispatch __attribute__((swift_name("init(dispatch:)"))) __attribute__((objc_designated_initializer));
- (void)getMifareCardVersionP0:(NSDictionary<NSString *, NSString *> * _Nullable)p0 __attribute__((swift_name("getMifareCardVersion(p0:)")));
- (void)getMifareFastReadDataP0:(NSDictionary<NSString *, NSString *> * _Nullable)p0 __attribute__((swift_name("getMifareFastReadData(p0:)")));
- (void)getMifareReadDataP0:(NSDictionary<NSString *, NSString *> * _Nullable)p0 __attribute__((swift_name("getMifareReadData(p0:)")));
- (void)onAddKeyP0:(BOOL)p0 __attribute__((swift_name("onAddKey(p0:)")));
- (void)onBatchReadMifareCardResultP0:(NSString * _Nullable)p0 p1:(NSDictionary<NSString *, NSMutableArray<NSString *> *> * _Nullable)p1 __attribute__((swift_name("onBatchReadMifareCardResult(p0:p1:)")));
- (void)onBatchWriteMifareCardResultP0:(NSString * _Nullable)p0 p1:(NSDictionary<NSString *, NSMutableArray<NSString *> *> * _Nullable)p1 __attribute__((swift_name("onBatchWriteMifareCardResult(p0:p1:)")));
- (void)onBluetoothBoardStateResultP0:(BOOL)p0 __attribute__((swift_name("onBluetoothBoardStateResult(p0:)")));
- (void)onBluetoothBondFailed __attribute__((swift_name("onBluetoothBondFailed()")));
- (void)onBluetoothBondTimeout __attribute__((swift_name("onBluetoothBondTimeout()")));
- (void)onBluetoothBonded __attribute__((swift_name("onBluetoothBonded()")));
- (void)onBluetoothBonding __attribute__((swift_name("onBluetoothBonding()")));
- (void)onCbcMacResultP0:(NSString * _Nullable)p0 __attribute__((swift_name("onCbcMacResult(p0:)")));
- (void)onConfirmAmountResultP0:(BOOL)p0 __attribute__((swift_name("onConfirmAmountResult(p0:)")));
- (void)onDeviceFoundP0:(OnoBluetoothDevice * _Nullable)p0 __attribute__((swift_name("onDeviceFound(p0:)")));
- (void)onDoTradeResultCardReadMode:(OnoOnoQposDoTradeResult * _Nullable)cardReadMode tradeData:(NSDictionary<NSString *, NSString *> * _Nullable)tradeData __attribute__((swift_name("onDoTradeResult(cardReadMode:tradeData:)")));
- (void)onEmvICCExceptionDataIccExceptionTlv:(NSString * _Nullable)iccExceptionTlv __attribute__((swift_name("onEmvICCExceptionData(iccExceptionTlv:)")));
- (void)onEncryptDataP0:(NSString * _Nullable)p0 __attribute__((swift_name("onEncryptData(p0:)")));
- (void)onErrorError:(OnoOnoQposError * _Nullable)error __attribute__((swift_name("onError(error:)")));
- (void)onFinishMifareCardResultP0:(BOOL)p0 __attribute__((swift_name("onFinishMifareCardResult(p0:)")));
- (void)onGetBuzzerStatusResultP0:(NSString * _Nullable)p0 __attribute__((swift_name("onGetBuzzerStatusResult(p0:)")));
- (void)onGetCardNoResultP0:(NSString * _Nullable)p0 __attribute__((swift_name("onGetCardNoResult(p0:)")));
- (void)onGetDevicePubKeyKey:(NSString * _Nullable)key __attribute__((swift_name("onGetDevicePubKey(key:)")));
- (void)onGetInputAmountResultIsInputEntered:(BOOL)isInputEntered inputAmount:(NSString * _Nullable)inputAmount __attribute__((swift_name("onGetInputAmountResult(isInputEntered:inputAmount:)")));
- (void)onGetKeyCheckValueP0:(NSMutableArray<NSString *> * _Nullable)p0 __attribute__((swift_name("onGetKeyCheckValue(p0:)")));
- (void)onGetPosCommMod:(int32_t)mod amount:(NSString * _Nullable)amount posid:(NSString * _Nullable)posid __attribute__((swift_name("onGetPosComm(mod:amount:posid:)")));
- (void)onGetShutDownTimeP0:(NSString * _Nullable)p0 __attribute__((swift_name("onGetShutDownTime(p0:)")));
- (void)onGetSleepModeTimeP0:(NSString * _Nullable)p0 __attribute__((swift_name("onGetSleepModeTime(p0:)")));
- (void)onLcdShowCustomDisplayIsMessageDisplayed:(BOOL)isMessageDisplayed __attribute__((swift_name("onLcdShowCustomDisplay(isMessageDisplayed:)")));
- (void)onOperateMifareCardResultP0:(NSDictionary<NSString *, NSString *> * _Nullable)p0 __attribute__((swift_name("onOperateMifareCardResult(p0:)")));
- (void)onPinKey_TDES_ResultP0:(NSString * _Nullable)p0 __attribute__((swift_name("onPinKey_TDES_Result(p0:)")));
- (void)onQposDoGetTradeLogP0:(NSString * _Nullable)p0 p1:(NSString * _Nullable)p1 __attribute__((swift_name("onQposDoGetTradeLog(p0:p1:)")));
- (void)onQposDoGetTradeLogNumP0:(NSString * _Nullable)p0 __attribute__((swift_name("onQposDoGetTradeLogNum(p0:)")));
- (void)onQposDoSetRsaPublicKeyP0:(BOOL)p0 __attribute__((swift_name("onQposDoSetRsaPublicKey(p0:)")));
- (void)onQposDoTradeLogP0:(BOOL)p0 __attribute__((swift_name("onQposDoTradeLog(p0:)")));
- (void)onQposGenerateSessionKeysResultP0:(NSDictionary<NSString *, NSString *> * _Nullable)p0 __attribute__((swift_name("onQposGenerateSessionKeysResult(p0:)")));
- (void)onQposIdResultTerminalData:(NSDictionary<NSString *, NSString *> * _Nullable)terminalData __attribute__((swift_name("onQposIdResult(terminalData:)")));
- (void)onQposInfoResultTerminalInfo:(NSDictionary<NSString *, NSString *> * _Nullable)terminalInfo __attribute__((swift_name("onQposInfoResult(terminalInfo:)")));
- (void)onQposIsCardExistExists:(BOOL)exists __attribute__((swift_name("onQposIsCardExist(exists:)")));
- (void)onQposKsnResultP0:(NSDictionary<NSString *, NSString *> * _Nullable)p0 __attribute__((swift_name("onQposKsnResult(p0:)")));
- (void)onReadBusinessCardResultP0:(BOOL)p0 p1:(NSString * _Nullable)p1 __attribute__((swift_name("onReadBusinessCardResult(p0:p1:)")));
- (void)onReadMifareCardResultP0:(NSDictionary<NSString *, NSString *> * _Nullable)p0 __attribute__((swift_name("onReadMifareCardResult(p0:)")));
- (void)onRequestBatchDataAcceptedTLV:(NSString * _Nullable)acceptedTLV __attribute__((swift_name("onRequestBatchData(acceptedTLV:)")));
- (void)onRequestCalculateMacP0:(NSString * _Nullable)p0 __attribute__((swift_name("onRequestCalculateMac(p0:)")));
- (void)onRequestDevice __attribute__((swift_name("onRequestDevice()")));
- (void)onRequestDeviceScanFinished __attribute__((swift_name("onRequestDeviceScanFinished()")));
- (void)onRequestDisplayDisplay:(OnoOnoQposDisplay * _Nullable)display __attribute__((swift_name("onRequestDisplay(display:)")));
- (void)onRequestFinalConfirm __attribute__((swift_name("onRequestFinalConfirm()")));
- (void)onRequestIsServerConnected __attribute__((swift_name("onRequestIsServerConnected()")));
- (void)onRequestNoQposDetected __attribute__((swift_name("onRequestNoQposDetected()")));
- (void)onRequestNoQposDetectedUnbond __attribute__((swift_name("onRequestNoQposDetectedUnbond()")));
- (void)onRequestOnlineProcessAuthorizationTLV:(NSString * _Nullable)authorizationTLV __attribute__((swift_name("onRequestOnlineProcess(authorizationTLV:)")));
- (void)onRequestQposConnected __attribute__((swift_name("onRequestQposConnected()")));
- (void)onRequestQposDisconnected __attribute__((swift_name("onRequestQposDisconnected()")));
- (void)onRequestSelectEmvAppEmvApplicationsList:(NSMutableArray<NSString *> * _Nullable)emvApplicationsList __attribute__((swift_name("onRequestSelectEmvApp(emvApplicationsList:)")));
- (void)onRequestSetAmount __attribute__((swift_name("onRequestSetAmount()")));
- (void)onRequestSetPin __attribute__((swift_name("onRequestSetPin()")));
- (void)onRequestSignatureResultP0:(OnoKotlinByteArray * _Nullable)p0 __attribute__((swift_name("onRequestSignatureResult(p0:)")));
- (void)onRequestTime __attribute__((swift_name("onRequestTime()")));
- (void)onRequestTransactionLogP0:(NSString * _Nullable)p0 __attribute__((swift_name("onRequestTransactionLog(p0:)")));
- (void)onRequestTransactionResultTransactionResult:(OnoOnoQposTransactionResult * _Nullable)transactionResult __attribute__((swift_name("onRequestTransactionResult(transactionResult:)")));
- (void)onRequestUpdateKeyP0:(NSString * _Nullable)p0 __attribute__((swift_name("onRequestUpdateKey(p0:)")));
- (void)onRequestUpdateWorkKeyResultP0:(OnoOnoQposUpdateInformationResult * _Nullable)p0 __attribute__((swift_name("onRequestUpdateWorkKeyResult(p0:)")));
- (void)onRequestWaitingUser __attribute__((swift_name("onRequestWaitingUser()")));
- (void)onReturnApduResultP0:(BOOL)p0 p1:(NSString * _Nullable)p1 p2:(int32_t)p2 __attribute__((swift_name("onReturnApduResult(p0:p1:p2:)")));
- (void)onReturnBatchSendAPDUResultP0:(OnoMutableDictionary<OnoInt *, NSString *> * _Nullable)p0 __attribute__((swift_name("onReturnBatchSendAPDUResult(p0:)")));
- (void)onReturnCustomConfigResultWasSuccessful:(BOOL)wasSuccessful result:(NSString * _Nullable)result __attribute__((swift_name("onReturnCustomConfigResult(wasSuccessful:result:)")));
- (void)onReturnDownloadRsaPublicKeyP0:(OnoMutableDictionary<NSString *, NSString *> * _Nullable)p0 __attribute__((swift_name("onReturnDownloadRsaPublicKey(p0:)")));
- (void)onReturnGetEMVListResultP0:(NSString * _Nullable)p0 __attribute__((swift_name("onReturnGetEMVListResult(p0:)")));
- (void)onReturnGetPinResultP0:(NSDictionary<NSString *, NSString *> * _Nullable)p0 __attribute__((swift_name("onReturnGetPinResult(p0:)")));
- (void)onReturnGetQuickEmvResultP0:(BOOL)p0 __attribute__((swift_name("onReturnGetQuickEmvResult(p0:)")));
- (void)onReturnNFCApduResultP0:(BOOL)p0 p1:(NSString * _Nullable)p1 p2:(int32_t)p2 __attribute__((swift_name("onReturnNFCApduResult(p0:p1:p2:)")));
- (void)onReturnPowerOffIccResultP0:(BOOL)p0 __attribute__((swift_name("onReturnPowerOffIccResult(p0:)")));
- (void)onReturnPowerOffNFCResultP0:(BOOL)p0 __attribute__((swift_name("onReturnPowerOffNFCResult(p0:)")));
- (void)onReturnPowerOnIccResultP0:(BOOL)p0 p1:(NSString * _Nullable)p1 p2:(NSString * _Nullable)p2 p3:(int32_t)p3 __attribute__((swift_name("onReturnPowerOnIccResult(p0:p1:p2:p3:)")));
- (void)onReturnPowerOnNFCResultP0:(BOOL)p0 p1:(NSString * _Nullable)p1 p2:(NSString * _Nullable)p2 p3:(int32_t)p3 __attribute__((swift_name("onReturnPowerOnNFCResult(p0:p1:p2:p3:)")));
- (void)onReturnRSAResultP0:(NSString * _Nullable)p0 __attribute__((swift_name("onReturnRSAResult(p0:)")));
- (void)onReturnReversalDataDeclinedTLV:(NSString * _Nullable)declinedTLV __attribute__((swift_name("onReturnReversalData(declinedTLV:)")));
- (void)onReturnSetMasterKeyResultP0:(BOOL)p0 __attribute__((swift_name("onReturnSetMasterKeyResult(p0:)")));
- (void)onReturnSetSleepTimeResultP0:(BOOL)p0 __attribute__((swift_name("onReturnSetSleepTimeResult(p0:)")));
- (void)onReturnUpdateEMVRIDResultP0:(BOOL)p0 __attribute__((swift_name("onReturnUpdateEMVRIDResult(p0:)")));
- (void)onReturnUpdateEMVResultWasSuccessful:(BOOL)wasSuccessful __attribute__((swift_name("onReturnUpdateEMVResult(wasSuccessful:)")));
- (void)onReturnUpdateIPEKResultP0:(BOOL)p0 __attribute__((swift_name("onReturnUpdateIPEKResult(p0:)")));
- (void)onReturniccCashBackP0:(NSDictionary<NSString *, NSString *> * _Nullable)p0 __attribute__((swift_name("onReturniccCashBack(p0:)")));
- (void)onSearchMifareCardResultP0:(NSDictionary<NSString *, NSString *> * _Nullable)p0 __attribute__((swift_name("onSearchMifareCardResult(p0:)")));
- (void)onSetBuzzerResultP0:(BOOL)p0 __attribute__((swift_name("onSetBuzzerResult(p0:)")));
- (void)onSetBuzzerStatusResultStatus:(BOOL)status __attribute__((swift_name("onSetBuzzerStatusResult(status:)")));
- (void)onSetBuzzerTimeResultStatus:(BOOL)status __attribute__((swift_name("onSetBuzzerTimeResult(status:)")));
- (void)onSetManagementKeyP0:(BOOL)p0 __attribute__((swift_name("onSetManagementKey(p0:)")));
- (void)onSetParamsResultP0:(BOOL)p0 p1:(NSDictionary<NSString *, id> * _Nullable)p1 __attribute__((swift_name("onSetParamsResult(p0:p1:)")));
- (void)onSetPosBlePinCodeP0:(BOOL)p0 __attribute__((swift_name("onSetPosBlePinCode(p0:)")));
- (void)onSetSleepModeTimeP0:(BOOL)p0 __attribute__((swift_name("onSetSleepModeTime(p0:)")));
- (void)onTradeCancelled __attribute__((swift_name("onTradeCancelled()")));
- (void)onUpdateMasterKeyResultP0:(BOOL)p0 p1:(NSDictionary<NSString *, NSString *> * _Nullable)p1 __attribute__((swift_name("onUpdateMasterKeyResult(p0:p1:)")));
- (void)onUpdatePosFirmwareResultP0:(OnoOnoQposUpdateInformationResult * _Nullable)p0 __attribute__((swift_name("onUpdatePosFirmwareResult(p0:)")));
- (void)onVerifyMifareCardResultP0:(BOOL)p0 __attribute__((swift_name("onVerifyMifareCardResult(p0:)")));
- (void)onWaitingforDataP0:(NSString * _Nullable)p0 __attribute__((swift_name("onWaitingforData(p0:)")));
- (void)onWriteBusinessCardResultP0:(BOOL)p0 __attribute__((swift_name("onWriteBusinessCardResult(p0:)")));
- (void)onWriteMifareCardResultP0:(BOOL)p0 __attribute__((swift_name("onWriteMifareCardResult(p0:)")));
- (void)transferMifareDataP0:(NSString * _Nullable)p0 __attribute__((swift_name("transferMifareData(p0:)")));
- (void)verifyMifareULDataP0:(NSDictionary<NSString *, NSString *> * _Nullable)p0 __attribute__((swift_name("verifyMifareULData(p0:)")));
- (void)writeMifareULDataP0:(NSString * _Nullable)p0 __attribute__((swift_name("writeMifareULData(p0:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoQposListener")))
@interface OnoOnoQposListener : OnoOnoQposListenerProtocol
- (instancetype)initWithDispatch:(void (^)(OnoAction *))dispatch __attribute__((swift_name("init(dispatch:)"))) __attribute__((objc_designated_initializer));
- (void)getMifareCardVersionP0:(NSDictionary<NSString *, NSString *> * _Nullable)p0 __attribute__((swift_name("getMifareCardVersion(p0:)")));
- (void)getMifareFastReadDataP0:(NSDictionary<NSString *, NSString *> * _Nullable)p0 __attribute__((swift_name("getMifareFastReadData(p0:)")));
- (void)getMifareReadDataP0:(NSDictionary<NSString *, NSString *> * _Nullable)p0 __attribute__((swift_name("getMifareReadData(p0:)")));
- (void)onAddKeyP0:(BOOL)p0 __attribute__((swift_name("onAddKey(p0:)")));
- (void)onBatchReadMifareCardResultP0:(NSString * _Nullable)p0 p1:(NSDictionary<NSString *, NSMutableArray<NSString *> *> * _Nullable)p1 __attribute__((swift_name("onBatchReadMifareCardResult(p0:p1:)")));
- (void)onBatchWriteMifareCardResultP0:(NSString * _Nullable)p0 p1:(NSDictionary<NSString *, NSMutableArray<NSString *> *> * _Nullable)p1 __attribute__((swift_name("onBatchWriteMifareCardResult(p0:p1:)")));
- (void)onBluetoothBoardStateResultP0:(BOOL)p0 __attribute__((swift_name("onBluetoothBoardStateResult(p0:)")));
- (void)onBluetoothBondFailed __attribute__((swift_name("onBluetoothBondFailed()")));
- (void)onBluetoothBondTimeout __attribute__((swift_name("onBluetoothBondTimeout()")));
- (void)onBluetoothBonded __attribute__((swift_name("onBluetoothBonded()")));
- (void)onBluetoothBonding __attribute__((swift_name("onBluetoothBonding()")));
- (void)onCbcMacResultP0:(NSString * _Nullable)p0 __attribute__((swift_name("onCbcMacResult(p0:)")));
- (void)onConfirmAmountResultP0:(BOOL)p0 __attribute__((swift_name("onConfirmAmountResult(p0:)")));
- (void)onDeviceFoundP0:(OnoBluetoothDevice * _Nullable)p0 __attribute__((swift_name("onDeviceFound(p0:)")));
- (void)onDoTradeResultCardReadMode:(OnoOnoQposDoTradeResult * _Nullable)cardReadMode tradeData:(NSDictionary<NSString *, NSString *> * _Nullable)tradeData __attribute__((swift_name("onDoTradeResult(cardReadMode:tradeData:)")));
- (void)onEmvICCExceptionDataIccExceptionTlv:(NSString * _Nullable)iccExceptionTlv __attribute__((swift_name("onEmvICCExceptionData(iccExceptionTlv:)")));
- (void)onEncryptDataP0:(NSString * _Nullable)p0 __attribute__((swift_name("onEncryptData(p0:)")));
- (void)onErrorError:(OnoOnoQposError * _Nullable)error __attribute__((swift_name("onError(error:)")));
- (void)onFinishMifareCardResultP0:(BOOL)p0 __attribute__((swift_name("onFinishMifareCardResult(p0:)")));
- (void)onGetBuzzerStatusResultP0:(NSString * _Nullable)p0 __attribute__((swift_name("onGetBuzzerStatusResult(p0:)")));
- (void)onGetCardNoResultP0:(NSString * _Nullable)p0 __attribute__((swift_name("onGetCardNoResult(p0:)")));
- (void)onGetDevicePubKeyKey:(NSString * _Nullable)key __attribute__((swift_name("onGetDevicePubKey(key:)")));
- (void)onGetInputAmountResultIsInputEntered:(BOOL)isInputEntered inputAmount:(NSString * _Nullable)inputAmount __attribute__((swift_name("onGetInputAmountResult(isInputEntered:inputAmount:)")));
- (void)onGetKeyCheckValueP0:(NSMutableArray<NSString *> * _Nullable)p0 __attribute__((swift_name("onGetKeyCheckValue(p0:)")));
- (void)onGetPosCommMod:(int32_t)mod amount:(NSString * _Nullable)amount posid:(NSString * _Nullable)posid __attribute__((swift_name("onGetPosComm(mod:amount:posid:)")));
- (void)onGetShutDownTimeP0:(NSString * _Nullable)p0 __attribute__((swift_name("onGetShutDownTime(p0:)")));
- (void)onGetSleepModeTimeP0:(NSString * _Nullable)p0 __attribute__((swift_name("onGetSleepModeTime(p0:)")));
- (void)onLcdShowCustomDisplayIsMessageDisplayed:(BOOL)isMessageDisplayed __attribute__((swift_name("onLcdShowCustomDisplay(isMessageDisplayed:)")));
- (void)onOperateMifareCardResultP0:(NSDictionary<NSString *, NSString *> * _Nullable)p0 __attribute__((swift_name("onOperateMifareCardResult(p0:)")));
- (void)onPinKey_TDES_ResultP0:(NSString * _Nullable)p0 __attribute__((swift_name("onPinKey_TDES_Result(p0:)")));
- (void)onQposDoGetTradeLogP0:(NSString * _Nullable)p0 p1:(NSString * _Nullable)p1 __attribute__((swift_name("onQposDoGetTradeLog(p0:p1:)")));
- (void)onQposDoGetTradeLogNumP0:(NSString * _Nullable)p0 __attribute__((swift_name("onQposDoGetTradeLogNum(p0:)")));
- (void)onQposDoSetRsaPublicKeyP0:(BOOL)p0 __attribute__((swift_name("onQposDoSetRsaPublicKey(p0:)")));
- (void)onQposDoTradeLogP0:(BOOL)p0 __attribute__((swift_name("onQposDoTradeLog(p0:)")));
- (void)onQposGenerateSessionKeysResultP0:(NSDictionary<NSString *, NSString *> * _Nullable)p0 __attribute__((swift_name("onQposGenerateSessionKeysResult(p0:)")));
- (void)onQposIdResultTerminalData:(NSDictionary<NSString *, NSString *> * _Nullable)terminalData __attribute__((swift_name("onQposIdResult(terminalData:)")));
- (void)onQposInfoResultTerminalInfo:(NSDictionary<NSString *, NSString *> * _Nullable)terminalInfo __attribute__((swift_name("onQposInfoResult(terminalInfo:)")));
- (void)onQposIsCardExistExists:(BOOL)exists __attribute__((swift_name("onQposIsCardExist(exists:)")));
- (void)onQposKsnResultP0:(NSDictionary<NSString *, NSString *> * _Nullable)p0 __attribute__((swift_name("onQposKsnResult(p0:)")));
- (void)onReadBusinessCardResultP0:(BOOL)p0 p1:(NSString * _Nullable)p1 __attribute__((swift_name("onReadBusinessCardResult(p0:p1:)")));
- (void)onReadMifareCardResultP0:(NSDictionary<NSString *, NSString *> * _Nullable)p0 __attribute__((swift_name("onReadMifareCardResult(p0:)")));
- (void)onRequestBatchDataAcceptedTLV:(NSString * _Nullable)acceptedTLV __attribute__((swift_name("onRequestBatchData(acceptedTLV:)")));
- (void)onRequestCalculateMacP0:(NSString * _Nullable)p0 __attribute__((swift_name("onRequestCalculateMac(p0:)")));
- (void)onRequestDevice __attribute__((swift_name("onRequestDevice()")));
- (void)onRequestDeviceScanFinished __attribute__((swift_name("onRequestDeviceScanFinished()")));
- (void)onRequestDisplayDisplay:(OnoOnoQposDisplay * _Nullable)display __attribute__((swift_name("onRequestDisplay(display:)")));
- (void)onRequestFinalConfirm __attribute__((swift_name("onRequestFinalConfirm()")));
- (void)onRequestIsServerConnected __attribute__((swift_name("onRequestIsServerConnected()")));
- (void)onRequestNoQposDetected __attribute__((swift_name("onRequestNoQposDetected()")));
- (void)onRequestNoQposDetectedUnbond __attribute__((swift_name("onRequestNoQposDetectedUnbond()")));
- (void)onRequestOnlineProcessAuthorizationTLV:(NSString * _Nullable)authorizationTLV __attribute__((swift_name("onRequestOnlineProcess(authorizationTLV:)")));
- (void)onRequestQposConnected __attribute__((swift_name("onRequestQposConnected()")));
- (void)onRequestQposDisconnected __attribute__((swift_name("onRequestQposDisconnected()")));
- (void)onRequestSelectEmvAppEmvApplicationsList:(NSMutableArray<NSString *> * _Nullable)emvApplicationsList __attribute__((swift_name("onRequestSelectEmvApp(emvApplicationsList:)")));
- (void)onRequestSetAmount __attribute__((swift_name("onRequestSetAmount()")));
- (void)onRequestSetPin __attribute__((swift_name("onRequestSetPin()")));
- (void)onRequestSignatureResultP0:(OnoKotlinByteArray * _Nullable)p0 __attribute__((swift_name("onRequestSignatureResult(p0:)")));
- (void)onRequestTime __attribute__((swift_name("onRequestTime()")));
- (void)onRequestTransactionLogP0:(NSString * _Nullable)p0 __attribute__((swift_name("onRequestTransactionLog(p0:)")));
- (void)onRequestTransactionResultTransactionResult:(OnoOnoQposTransactionResult * _Nullable)transactionResult __attribute__((swift_name("onRequestTransactionResult(transactionResult:)")));
- (void)onRequestUpdateKeyP0:(NSString * _Nullable)p0 __attribute__((swift_name("onRequestUpdateKey(p0:)")));
- (void)onRequestUpdateWorkKeyResultP0:(OnoOnoQposUpdateInformationResult * _Nullable)p0 __attribute__((swift_name("onRequestUpdateWorkKeyResult(p0:)")));
- (void)onRequestWaitingUser __attribute__((swift_name("onRequestWaitingUser()")));
- (void)onReturnApduResultP0:(BOOL)p0 p1:(NSString * _Nullable)p1 p2:(int32_t)p2 __attribute__((swift_name("onReturnApduResult(p0:p1:p2:)")));
- (void)onReturnBatchSendAPDUResultP0:(OnoMutableDictionary<OnoInt *, NSString *> * _Nullable)p0 __attribute__((swift_name("onReturnBatchSendAPDUResult(p0:)")));
- (void)onReturnCustomConfigResultWasSuccessful:(BOOL)wasSuccessful result:(NSString * _Nullable)result __attribute__((swift_name("onReturnCustomConfigResult(wasSuccessful:result:)")));
- (void)onReturnDownloadRsaPublicKeyP0:(OnoMutableDictionary<NSString *, NSString *> * _Nullable)p0 __attribute__((swift_name("onReturnDownloadRsaPublicKey(p0:)")));
- (void)onReturnGetEMVListResultP0:(NSString * _Nullable)p0 __attribute__((swift_name("onReturnGetEMVListResult(p0:)")));
- (void)onReturnGetPinResultP0:(NSDictionary<NSString *, NSString *> * _Nullable)p0 __attribute__((swift_name("onReturnGetPinResult(p0:)")));
- (void)onReturnGetQuickEmvResultP0:(BOOL)p0 __attribute__((swift_name("onReturnGetQuickEmvResult(p0:)")));
- (void)onReturnNFCApduResultP0:(BOOL)p0 p1:(NSString * _Nullable)p1 p2:(int32_t)p2 __attribute__((swift_name("onReturnNFCApduResult(p0:p1:p2:)")));
- (void)onReturnPowerOffIccResultP0:(BOOL)p0 __attribute__((swift_name("onReturnPowerOffIccResult(p0:)")));
- (void)onReturnPowerOffNFCResultP0:(BOOL)p0 __attribute__((swift_name("onReturnPowerOffNFCResult(p0:)")));
- (void)onReturnPowerOnIccResultP0:(BOOL)p0 p1:(NSString * _Nullable)p1 p2:(NSString * _Nullable)p2 p3:(int32_t)p3 __attribute__((swift_name("onReturnPowerOnIccResult(p0:p1:p2:p3:)")));
- (void)onReturnPowerOnNFCResultP0:(BOOL)p0 p1:(NSString * _Nullable)p1 p2:(NSString * _Nullable)p2 p3:(int32_t)p3 __attribute__((swift_name("onReturnPowerOnNFCResult(p0:p1:p2:p3:)")));
- (void)onReturnRSAResultP0:(NSString * _Nullable)p0 __attribute__((swift_name("onReturnRSAResult(p0:)")));
- (void)onReturnReversalDataDeclinedTLV:(NSString * _Nullable)declinedTLV __attribute__((swift_name("onReturnReversalData(declinedTLV:)")));
- (void)onReturnSetMasterKeyResultP0:(BOOL)p0 __attribute__((swift_name("onReturnSetMasterKeyResult(p0:)")));
- (void)onReturnSetSleepTimeResultP0:(BOOL)p0 __attribute__((swift_name("onReturnSetSleepTimeResult(p0:)")));
- (void)onReturnUpdateEMVRIDResultP0:(BOOL)p0 __attribute__((swift_name("onReturnUpdateEMVRIDResult(p0:)")));
- (void)onReturnUpdateEMVResultWasSuccessful:(BOOL)wasSuccessful __attribute__((swift_name("onReturnUpdateEMVResult(wasSuccessful:)")));
- (void)onReturnUpdateIPEKResultP0:(BOOL)p0 __attribute__((swift_name("onReturnUpdateIPEKResult(p0:)")));
- (void)onReturniccCashBackP0:(NSDictionary<NSString *, NSString *> * _Nullable)p0 __attribute__((swift_name("onReturniccCashBack(p0:)")));
- (void)onSearchMifareCardResultP0:(NSDictionary<NSString *, NSString *> * _Nullable)p0 __attribute__((swift_name("onSearchMifareCardResult(p0:)")));
- (void)onSetBuzzerResultP0:(BOOL)p0 __attribute__((swift_name("onSetBuzzerResult(p0:)")));
- (void)onSetBuzzerStatusResultStatus:(BOOL)status __attribute__((swift_name("onSetBuzzerStatusResult(status:)")));
- (void)onSetBuzzerTimeResultStatus:(BOOL)status __attribute__((swift_name("onSetBuzzerTimeResult(status:)")));
- (void)onSetManagementKeyP0:(BOOL)p0 __attribute__((swift_name("onSetManagementKey(p0:)")));
- (void)onSetParamsResultP0:(BOOL)p0 p1:(NSDictionary<NSString *, id> * _Nullable)p1 __attribute__((swift_name("onSetParamsResult(p0:p1:)")));
- (void)onSetPosBlePinCodeP0:(BOOL)p0 __attribute__((swift_name("onSetPosBlePinCode(p0:)")));
- (void)onSetSleepModeTimeP0:(BOOL)p0 __attribute__((swift_name("onSetSleepModeTime(p0:)")));
- (void)onTradeCancelled __attribute__((swift_name("onTradeCancelled()")));
- (void)onUpdateMasterKeyResultP0:(BOOL)p0 p1:(NSDictionary<NSString *, NSString *> * _Nullable)p1 __attribute__((swift_name("onUpdateMasterKeyResult(p0:p1:)")));
- (void)onUpdatePosFirmwareResultP0:(OnoOnoQposUpdateInformationResult * _Nullable)p0 __attribute__((swift_name("onUpdatePosFirmwareResult(p0:)")));
- (void)onVerifyMifareCardResultP0:(BOOL)p0 __attribute__((swift_name("onVerifyMifareCardResult(p0:)")));
- (void)onWaitingforDataP0:(NSString * _Nullable)p0 __attribute__((swift_name("onWaitingforData(p0:)")));
- (void)onWriteBusinessCardResultP0:(BOOL)p0 __attribute__((swift_name("onWriteBusinessCardResult(p0:)")));
- (void)onWriteMifareCardResultP0:(BOOL)p0 __attribute__((swift_name("onWriteMifareCardResult(p0:)")));
- (void)transferMifareDataP0:(NSString * _Nullable)p0 __attribute__((swift_name("transferMifareData(p0:)")));
- (void)verifyMifareULDataP0:(NSDictionary<NSString *, NSString *> * _Nullable)p0 __attribute__((swift_name("verifyMifareULData(p0:)")));
- (void)writeMifareULDataP0:(NSString * _Nullable)p0 __attribute__((swift_name("writeMifareULData(p0:)")));
@property (readonly) NSString *TAG __attribute__((swift_name("TAG")));
@property OnoTerminalCommandQueue * _Nullable qposCommandQueue __attribute__((swift_name("qposCommandQueue")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("QposCommandType")))
@interface OnoQposCommandType : OnoKotlinEnum <OnoIQueueCommandType>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) OnoQposCommandType *connectTerminal __attribute__((swift_name("connectTerminal")));
@property (class, readonly) OnoQposCommandType *disconnectTerminal __attribute__((swift_name("disconnectTerminal")));
@property (class, readonly) OnoQposCommandType *setBuzzerStatus __attribute__((swift_name("setBuzzerStatus")));
@property (class, readonly) OnoQposCommandType *setShutdownTime __attribute__((swift_name("setShutdownTime")));
@property (class, readonly) OnoQposCommandType *setSleepTime __attribute__((swift_name("setSleepTime")));
@property (class, readonly) OnoQposCommandType *checkTerminalState __attribute__((swift_name("checkTerminalState")));
@property (class, readonly) OnoQposCommandType *doTrade __attribute__((swift_name("doTrade")));
@property (class, readonly) OnoQposCommandType *askForTip __attribute__((swift_name("askForTip")));
@property (class, readonly) OnoQposCommandType *requestPin __attribute__((swift_name("requestPin")));
@property (class, readonly) OnoQposCommandType *setTradeAmount __attribute__((swift_name("setTradeAmount")));
@property (class, readonly) OnoQposCommandType *doEmvApp __attribute__((swift_name("doEmvApp")));
@property (class, readonly) OnoQposCommandType *getTerminalTime __attribute__((swift_name("getTerminalTime")));
@property (class, readonly) OnoQposCommandType *emvAppSelected __attribute__((swift_name("emvAppSelected")));
@property (class, readonly) OnoQposCommandType *cancelEmvAppSelection __attribute__((swift_name("cancelEmvAppSelection")));
@property (class, readonly) OnoQposCommandType *cancelAskingForCard __attribute__((swift_name("cancelAskingForCard")));
@property (class, readonly) OnoQposCommandType *onoTradeResult __attribute__((swift_name("onoTradeResult")));
@property (class, readonly) OnoQposCommandType *getTerminalInfo __attribute__((swift_name("getTerminalInfo")));
@property (class, readonly) OnoQposCommandType *getTerminalStatus __attribute__((swift_name("getTerminalStatus")));
@property (class, readonly) OnoQposCommandType *getQposId __attribute__((swift_name("getQposId")));
@property (class, readonly) OnoQposCommandType *getNfcBatchData __attribute__((swift_name("getNfcBatchData")));
@property (class, readonly) OnoQposCommandType *displayMessage __attribute__((swift_name("displayMessage")));
@property (class, readonly) OnoQposCommandType *clearMessage __attribute__((swift_name("clearMessage")));
@property (class, readonly) OnoQposCommandType *updateEmvConfig __attribute__((swift_name("updateEmvConfig")));
@property (class, readonly) OnoQposCommandType *retrieveEmvConfig __attribute__((swift_name("retrieveEmvConfig")));
@property (class, readonly) OnoQposCommandType *checkIfCardExists __attribute__((swift_name("checkIfCardExists")));
@property (class, readonly) OnoQposCommandType *resetTerminal __attribute__((swift_name("resetTerminal")));
@property (class, readonly) OnoQposCommandType *submitArpc __attribute__((swift_name("submitArpc")));
@property (class, readonly) OnoQposCommandType *cancelTrade __attribute__((swift_name("cancelTrade")));
@property (class, readonly) OnoQposCommandType *quickReset __attribute__((swift_name("quickReset")));
- (int32_t)compareToOther:(OnoQposCommandType *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("QposConstants")))
@interface OnoQposConstants : OnoBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)qposConstants __attribute__((swift_name("init()")));
@property (readonly) NSString *APP_SELECT_TIMEOUT __attribute__((swift_name("APP_SELECT_TIMEOUT")));
@property (readonly) NSString *CMD_TIMEOUT __attribute__((swift_name("CMD_TIMEOUT")));
@property (readonly) NSString *DEVICE_BUSY __attribute__((swift_name("DEVICE_BUSY")));
@property (readonly) NSString *DEVICE_RESET __attribute__((swift_name("DEVICE_RESET")));
@property (readonly) NSString *QPOS_EXECUTION_TIMEOUT __attribute__((swift_name("QPOS_EXECUTION_TIMEOUT")));
@property (readonly) NSString *QPOS_SERVICE_BUSY __attribute__((swift_name("QPOS_SERVICE_BUSY")));
@property (readonly) NSString *QPOS_START_TIMEOUT __attribute__((swift_name("QPOS_START_TIMEOUT")));
@property (readonly) NSString *TIMEOUT __attribute__((swift_name("TIMEOUT")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoQposDisplay")))
@interface OnoOnoQposDisplay : OnoKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) OnoOnoQposDisplay *tryAnotherInterface __attribute__((swift_name("tryAnotherInterface")));
@property (class, readonly) OnoOnoQposDisplay *pleaseWait __attribute__((swift_name("pleaseWait")));
@property (class, readonly) OnoOnoQposDisplay *removeCard __attribute__((swift_name("removeCard")));
@property (class, readonly) OnoOnoQposDisplay *clearDisplayMsg __attribute__((swift_name("clearDisplayMsg")));
@property (class, readonly) OnoOnoQposDisplay *processing __attribute__((swift_name("processing")));
@property (class, readonly) OnoOnoQposDisplay *pinOk __attribute__((swift_name("pinOk")));
@property (class, readonly) OnoOnoQposDisplay *transactionTerminated __attribute__((swift_name("transactionTerminated")));
@property (class, readonly) OnoOnoQposDisplay *inputPinIng __attribute__((swift_name("inputPinIng")));
@property (class, readonly) OnoOnoQposDisplay *magToIccTrade __attribute__((swift_name("magToIccTrade")));
@property (class, readonly) OnoOnoQposDisplay *inputOfflinePinOnly __attribute__((swift_name("inputOfflinePinOnly")));
@property (class, readonly) OnoOnoQposDisplay *cardRemoved __attribute__((swift_name("cardRemoved")));
@property (class, readonly) OnoOnoQposDisplay *inputLastOfflinePin __attribute__((swift_name("inputLastOfflinePin")));
@property (class, readonly) OnoOnoQposDisplay *msrDataReady __attribute__((swift_name("msrDataReady")));
- (int32_t)compareToOther:(OnoOnoQposDisplay *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoQposDoTradeResult")))
@interface OnoOnoQposDoTradeResult : OnoKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) OnoOnoQposDoTradeResult *none __attribute__((swift_name("none")));
@property (class, readonly) OnoOnoQposDoTradeResult *mcr __attribute__((swift_name("mcr")));
@property (class, readonly) OnoOnoQposDoTradeResult *icc __attribute__((swift_name("icc")));
@property (class, readonly) OnoOnoQposDoTradeResult *notIcc __attribute__((swift_name("notIcc")));
@property (class, readonly) OnoOnoQposDoTradeResult *badSwipe __attribute__((swift_name("badSwipe")));
@property (class, readonly) OnoOnoQposDoTradeResult *noResponse __attribute__((swift_name("noResponse")));
@property (class, readonly) OnoOnoQposDoTradeResult *noUpdateWorkKey __attribute__((swift_name("noUpdateWorkKey")));
@property (class, readonly) OnoOnoQposDoTradeResult *nfcOnline __attribute__((swift_name("nfcOnline")));
@property (class, readonly) OnoOnoQposDoTradeResult *nfcOffline __attribute__((swift_name("nfcOffline")));
@property (class, readonly) OnoOnoQposDoTradeResult *nfcDeclined __attribute__((swift_name("nfcDeclined")));
- (int32_t)compareToOther:(OnoOnoQposDoTradeResult *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoQposEmvOption")))
@interface OnoOnoQposEmvOption : OnoKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) OnoOnoQposEmvOption *startWithForceOnline __attribute__((swift_name("startWithForceOnline")));
- (int32_t)compareToOther:(OnoOnoQposEmvOption *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoQposError")))
@interface OnoOnoQposError : OnoKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) OnoOnoQposError *timeout __attribute__((swift_name("timeout")));
@property (class, readonly) OnoOnoQposError *macError __attribute__((swift_name("macError")));
@property (class, readonly) OnoOnoQposError *cmdTimeout __attribute__((swift_name("cmdTimeout")));
@property (class, readonly) OnoOnoQposError *cmdNotAvailable __attribute__((swift_name("cmdNotAvailable")));
@property (class, readonly) OnoOnoQposError *deviceReset __attribute__((swift_name("deviceReset")));
@property (class, readonly) OnoOnoQposError *unknown __attribute__((swift_name("unknown")));
@property (class, readonly) OnoOnoQposError *deviceBusy __attribute__((swift_name("deviceBusy")));
@property (class, readonly) OnoOnoQposError *inputOutOfRange __attribute__((swift_name("inputOutOfRange")));
@property (class, readonly) OnoOnoQposError *inputInvalidFormat __attribute__((swift_name("inputInvalidFormat")));
@property (class, readonly) OnoOnoQposError *inputZeroValues __attribute__((swift_name("inputZeroValues")));
@property (class, readonly) OnoOnoQposError *inputInvalid __attribute__((swift_name("inputInvalid")));
@property (class, readonly) OnoOnoQposError *cashbackNotSupported __attribute__((swift_name("cashbackNotSupported")));
@property (class, readonly) OnoOnoQposError *crcError __attribute__((swift_name("crcError")));
@property (class, readonly) OnoOnoQposError *commError __attribute__((swift_name("commError")));
@property (class, readonly) OnoOnoQposError *wrDataError __attribute__((swift_name("wrDataError")));
@property (class, readonly) OnoOnoQposError *emvAppCfgError __attribute__((swift_name("emvAppCfgError")));
@property (class, readonly) OnoOnoQposError *emvCapkCfgError __attribute__((swift_name("emvCapkCfgError")));
@property (class, readonly) OnoOnoQposError *apduError __attribute__((swift_name("apduError")));
@property (class, readonly) OnoOnoQposError *appSelectTimeout __attribute__((swift_name("appSelectTimeout")));
@property (class, readonly) OnoOnoQposError *iccOnlineTimeout __attribute__((swift_name("iccOnlineTimeout")));
@property (class, readonly) OnoOnoQposError *amountOutOfLimit __attribute__((swift_name("amountOutOfLimit")));
- (int32_t)compareToOther:(OnoOnoQposError *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoQposLcdModeAlign")))
@interface OnoOnoQposLcdModeAlign : OnoKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) OnoOnoQposLcdModeAlign *lcdModeAligncenter __attribute__((swift_name("lcdModeAligncenter")));
- (int32_t)compareToOther:(OnoOnoQposLcdModeAlign *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoQposTransactionResult")))
@interface OnoOnoQposTransactionResult : OnoKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) OnoOnoQposTransactionResult *approved __attribute__((swift_name("approved")));
@property (class, readonly) OnoOnoQposTransactionResult *terminated __attribute__((swift_name("terminated")));
@property (class, readonly) OnoOnoQposTransactionResult *declined __attribute__((swift_name("declined")));
@property (class, readonly) OnoOnoQposTransactionResult *cancel __attribute__((swift_name("cancel")));
@property (class, readonly) OnoOnoQposTransactionResult *capkFail __attribute__((swift_name("capkFail")));
@property (class, readonly) OnoOnoQposTransactionResult *notIcc __attribute__((swift_name("notIcc")));
@property (class, readonly) OnoOnoQposTransactionResult *selectAppFail __attribute__((swift_name("selectAppFail")));
@property (class, readonly) OnoOnoQposTransactionResult *deviceError __attribute__((swift_name("deviceError")));
@property (class, readonly) OnoOnoQposTransactionResult *cardNotSupported __attribute__((swift_name("cardNotSupported")));
@property (class, readonly) OnoOnoQposTransactionResult *missingMandatoryData __attribute__((swift_name("missingMandatoryData")));
@property (class, readonly) OnoOnoQposTransactionResult *cardBlockedOrNoEmvApps __attribute__((swift_name("cardBlockedOrNoEmvApps")));
@property (class, readonly) OnoOnoQposTransactionResult *invalidIccData __attribute__((swift_name("invalidIccData")));
@property (class, readonly) OnoOnoQposTransactionResult *fallback __attribute__((swift_name("fallback")));
@property (class, readonly) OnoOnoQposTransactionResult *nfcTerminated __attribute__((swift_name("nfcTerminated")));
@property (class, readonly) OnoOnoQposTransactionResult *cardRemoved __attribute__((swift_name("cardRemoved")));
@property (class, readonly) OnoOnoQposTransactionResult *tradeLogFull __attribute__((swift_name("tradeLogFull")));
- (int32_t)compareToOther:(OnoOnoQposTransactionResult *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoQposTransactionType")))
@interface OnoOnoQposTransactionType : OnoKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) OnoOnoQposTransactionType *goods __attribute__((swift_name("goods")));
- (int32_t)compareToOther:(OnoOnoQposTransactionType *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoQposUpdateInformationResult")))
@interface OnoOnoQposUpdateInformationResult : OnoKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) OnoOnoQposUpdateInformationResult *updateSuccess __attribute__((swift_name("updateSuccess")));
@property (class, readonly) OnoOnoQposUpdateInformationResult *updateFail __attribute__((swift_name("updateFail")));
@property (class, readonly) OnoOnoQposUpdateInformationResult *updatePacketVefiryError __attribute__((swift_name("updatePacketVefiryError")));
@property (class, readonly) OnoOnoQposUpdateInformationResult *updatePacketLenError __attribute__((swift_name("updatePacketLenError")));
@property (class, readonly) OnoOnoQposUpdateInformationResult *updateLowpower __attribute__((swift_name("updateLowpower")));
@property (class, readonly) OnoOnoQposUpdateInformationResult *updating __attribute__((swift_name("updating")));
@property (class, readonly) OnoOnoQposUpdateInformationResult *usbReconnecting __attribute__((swift_name("usbReconnecting")));
- (int32_t)compareToOther:(OnoOnoQposUpdateInformationResult *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConnectionStatus")))
@interface OnoConnectionStatus : OnoKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) OnoConnectionStatus *initializing __attribute__((swift_name("initializing")));
@property (class, readonly) OnoConnectionStatus *settingShutdown __attribute__((swift_name("settingShutdown")));
@property (class, readonly) OnoConnectionStatus *gettingId __attribute__((swift_name("gettingId")));
@property (class, readonly) OnoConnectionStatus *gettingInfo __attribute__((swift_name("gettingInfo")));
@property (class, readonly) OnoConnectionStatus *quickReset __attribute__((swift_name("quickReset")));
@property (class, readonly) OnoConnectionStatus *gettingEmvConfigVersion __attribute__((swift_name("gettingEmvConfigVersion")));
@property (class, readonly) OnoConnectionStatus *disconnecting __attribute__((swift_name("disconnecting")));
@property (class, readonly) OnoConnectionStatus *settingBuzzerStatus __attribute__((swift_name("settingBuzzerStatus")));
- (int32_t)compareToOther:(OnoConnectionStatus *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TradeMode")))
@interface OnoTradeMode : OnoKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) OnoTradeMode *nfc __attribute__((swift_name("nfc")));
@property (class, readonly) OnoTradeMode *insert __attribute__((swift_name("insert")));
@property (class, readonly) OnoTradeMode *swipe __attribute__((swift_name("swipe")));
- (int32_t)compareToOther:(OnoTradeMode *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionStep")))
@interface OnoTransactionStep : OnoKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) OnoTransactionStep *doTrade __attribute__((swift_name("doTrade")));
@property (class, readonly) OnoTransactionStep *setAmount __attribute__((swift_name("setAmount")));
@property (class, readonly) OnoTransactionStep *awaitingCard __attribute__((swift_name("awaitingCard")));
@property (class, readonly) OnoTransactionStep *readingCard __attribute__((swift_name("readingCard")));
@property (class, readonly) OnoTransactionStep *tradeResult __attribute__((swift_name("tradeResult")));
@property (class, readonly) OnoTransactionStep *requestTime __attribute__((swift_name("requestTime")));
@property (class, readonly) OnoTransactionStep *requestEmvApplication __attribute__((swift_name("requestEmvApplication")));
@property (class, readonly) OnoTransactionStep *emvApplicationSelected __attribute__((swift_name("emvApplicationSelected")));
@property (class, readonly) OnoTransactionStep *enterPin __attribute__((swift_name("enterPin")));
@property (class, readonly) OnoTransactionStep *processing __attribute__((swift_name("processing")));
@property (class, readonly) OnoTransactionStep *authorizeTlv __attribute__((swift_name("authorizeTlv")));
@property (class, readonly) OnoTransactionStep *submitArpc __attribute__((swift_name("submitArpc")));
@property (class, readonly) OnoTransactionStep *finalizeTlv __attribute__((swift_name("finalizeTlv")));
@property (class, readonly) OnoTransactionStep *completed __attribute__((swift_name("completed")));
@property (class, readonly) OnoTransactionStep *cancelling __attribute__((swift_name("cancelling")));
- (int32_t)compareToOther:(OnoTransactionStep *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionStep.Companion")))
@interface OnoTransactionStepCompanion : OnoBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (readonly) NSSet<OnoTransactionStep *> *finishedStep __attribute__((swift_name("finishedStep")));
@property (readonly) NSSet<OnoTransactionStep *> *processingStep __attribute__((swift_name("processingStep")));
@end;

__attribute__((swift_name("DspreadActions")))
@interface OnoDspreadActions : OnoBase
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadActions.AuthorizationTLV")))
@interface OnoDspreadActionsAuthorizationTLV : OnoAction
- (instancetype)initWithAuthorizationTLV:(OnoSensitiveString *)authorizationTLV maskedPan:(OnoSensitiveString * _Nullable)maskedPan __attribute__((swift_name("init(authorizationTLV:maskedPan:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoSensitiveString *)component1 __attribute__((swift_name("component1()")));
- (OnoSensitiveString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (OnoDspreadActionsAuthorizationTLV *)doCopyAuthorizationTLV:(OnoSensitiveString *)authorizationTLV maskedPan:(OnoSensitiveString * _Nullable)maskedPan __attribute__((swift_name("doCopy(authorizationTLV:maskedPan:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoSensitiveString *authorizationTLV __attribute__((swift_name("authorizationTLV")));
@property (readonly) OnoSensitiveString * _Nullable maskedPan __attribute__((swift_name("maskedPan")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadActions.AwaitingCard")))
@interface OnoDspreadActionsAwaitingCard : OnoAction
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadActions.BuzzerStatusSet")))
@interface OnoDspreadActionsBuzzerStatusSet : OnoAction
- (instancetype)initWithStatus:(BOOL)status __attribute__((swift_name("init(status:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (BOOL)component1 __attribute__((swift_name("component1()")));
- (OnoDspreadActionsBuzzerStatusSet *)doCopyStatus:(BOOL)status __attribute__((swift_name("doCopy(status:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL status __attribute__((swift_name("status")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadActions.CardPresented")))
@interface OnoDspreadActionsCardPresented : OnoAction
- (instancetype)initWithTransactionType:(OnoTransactionType *)transactionType tradeData:(OnoSensitiveMap * _Nullable)tradeData __attribute__((swift_name("init(transactionType:tradeData:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoTransactionType *)component1 __attribute__((swift_name("component1()")));
- (OnoSensitiveMap * _Nullable)component2 __attribute__((swift_name("component2()")));
- (OnoDspreadActionsCardPresented *)doCopyTransactionType:(OnoTransactionType *)transactionType tradeData:(OnoSensitiveMap * _Nullable)tradeData __attribute__((swift_name("doCopy(transactionType:tradeData:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoSensitiveMap * _Nullable tradeData __attribute__((swift_name("tradeData")));
@property (readonly) OnoTransactionType *transactionType __attribute__((swift_name("transactionType")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadActions.CardStillExists")))
@interface OnoDspreadActionsCardStillExists : OnoAction
- (instancetype)initWithCardStillPresent:(BOOL)cardStillPresent __attribute__((swift_name("init(cardStillPresent:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (BOOL)component1 __attribute__((swift_name("component1()")));
- (OnoDspreadActionsCardStillExists *)doCopyCardStillPresent:(BOOL)cardStillPresent __attribute__((swift_name("doCopy(cardStillPresent:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL cardStillPresent __attribute__((swift_name("cardStillPresent")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadActions.CheckTerminalStatus")))
@interface OnoDspreadActionsCheckTerminalStatus : OnoAction
- (instancetype)initWithStatusCheckId:(int32_t)statusCheckId __attribute__((swift_name("init(statusCheckId:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (int32_t)component1 __attribute__((swift_name("component1()")));
- (OnoDspreadActionsCheckTerminalStatus *)doCopyStatusCheckId:(int32_t)statusCheckId __attribute__((swift_name("doCopy(statusCheckId:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t statusCheckId __attribute__((swift_name("statusCheckId")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadActions.ChipFallback")))
@interface OnoDspreadActionsChipFallback : OnoAction
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString * _Nullable)component1 __attribute__((swift_name("component1()")));
- (OnoDspreadActionsChipFallback *)doCopyMessage:(NSString * _Nullable)message __attribute__((swift_name("doCopy(message:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable message __attribute__((swift_name("message")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadActions.ConnectionInitialized")))
@interface OnoDspreadActionsConnectionInitialized : OnoAction
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadActions.EmvIccExceptionData")))
@interface OnoDspreadActionsEmvIccExceptionData : OnoAction
- (instancetype)initWithIccExceptionTlv:(OnoSensitiveString * _Nullable)iccExceptionTlv tvr:(NSArray<NSString *> * _Nullable)tvr __attribute__((swift_name("init(iccExceptionTlv:tvr:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoSensitiveString * _Nullable)component1 __attribute__((swift_name("component1()")));
- (NSArray<NSString *> * _Nullable)component2 __attribute__((swift_name("component2()")));
- (OnoDspreadActionsEmvIccExceptionData *)doCopyIccExceptionTlv:(OnoSensitiveString * _Nullable)iccExceptionTlv tvr:(NSArray<NSString *> * _Nullable)tvr __attribute__((swift_name("doCopy(iccExceptionTlv:tvr:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoSensitiveString * _Nullable iccExceptionTlv __attribute__((swift_name("iccExceptionTlv")));
@property (readonly) NSArray<NSString *> * _Nullable tvr __attribute__((swift_name("tvr")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadActions.EnterPin")))
@interface OnoDspreadActionsEnterPin : OnoAction
- (instancetype)initWithIsLastPintEntry:(BOOL)isLastPintEntry __attribute__((swift_name("init(isLastPintEntry:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (BOOL)component1 __attribute__((swift_name("component1()")));
- (OnoDspreadActionsEnterPin *)doCopyIsLastPintEntry:(BOOL)isLastPintEntry __attribute__((swift_name("doCopy(isLastPintEntry:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL isLastPintEntry __attribute__((swift_name("isLastPintEntry")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadActions.FinalizationTLV")))
@interface OnoDspreadActionsFinalizationTLV : OnoAction
- (instancetype)initWithFinalizeTLV:(OnoSensitiveString *)finalizeTLV isReversalTLV:(BOOL)isReversalTLV __attribute__((swift_name("init(finalizeTLV:isReversalTLV:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoSensitiveString *)component1 __attribute__((swift_name("component1()")));
- (BOOL)component2 __attribute__((swift_name("component2()")));
- (OnoDspreadActionsFinalizationTLV *)doCopyFinalizeTLV:(OnoSensitiveString *)finalizeTLV isReversalTLV:(BOOL)isReversalTLV __attribute__((swift_name("doCopy(finalizeTLV:isReversalTLV:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoSensitiveString *finalizeTLV __attribute__((swift_name("finalizeTLV")));
@property (readonly) BOOL isReversalTLV __attribute__((swift_name("isReversalTLV")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadActions.GotQposComm")))
@interface OnoDspreadActionsGotQposComm : OnoAction
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadActions.MessageShown")))
@interface OnoDspreadActionsMessageShown : OnoAction
- (instancetype)initWithIsMessageShowing:(BOOL)isMessageShowing __attribute__((swift_name("init(isMessageShowing:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (BOOL)component1 __attribute__((swift_name("component1()")));
- (OnoDspreadActionsMessageShown *)doCopyIsMessageShowing:(BOOL)isMessageShowing __attribute__((swift_name("doCopy(isMessageShowing:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL isMessageShowing __attribute__((swift_name("isMessageShowing")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadActions.MessageTimeout")))
@interface OnoDspreadActionsMessageTimeout : OnoAction
- (instancetype)initWithMessage:(NSString *)message timeoutInSeconds:(OnoInt * _Nullable)timeoutInSeconds __attribute__((swift_name("init(message:timeoutInSeconds:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoInt * _Nullable)component2 __attribute__((swift_name("component2()")));
- (OnoDspreadActionsMessageTimeout *)doCopyMessage:(NSString *)message timeoutInSeconds:(OnoInt * _Nullable)timeoutInSeconds __attribute__((swift_name("doCopy(message:timeoutInSeconds:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *message __attribute__((swift_name("message")));
@property (readonly) OnoInt * _Nullable timeoutInSeconds __attribute__((swift_name("timeoutInSeconds")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadActions.NfcFallback")))
@interface OnoDspreadActionsNfcFallback : OnoAction
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString * _Nullable)component1 __attribute__((swift_name("component1()")));
- (OnoDspreadActionsNfcFallback *)doCopyMessage:(NSString * _Nullable)message __attribute__((swift_name("doCopy(message:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable message __attribute__((swift_name("message")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadActions.QposDisconnected")))
@interface OnoDspreadActionsQposDisconnected : OnoAction
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadActions.QposError")))
@interface OnoDspreadActionsQposError : OnoAction
- (instancetype)initWithError:(NSString * _Nullable)error currentlyBusyAction:(OnoQposCommandType * _Nullable)currentlyBusyAction queueError:(OnoTerminalErrorQueue * _Nullable)queueError __attribute__((swift_name("init(error:currentlyBusyAction:queueError:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString * _Nullable)component1 __attribute__((swift_name("component1()")));
- (OnoQposCommandType * _Nullable)component2 __attribute__((swift_name("component2()")));
- (OnoTerminalErrorQueue * _Nullable)component3 __attribute__((swift_name("component3()")));
- (OnoDspreadActionsQposError *)doCopyError:(NSString * _Nullable)error currentlyBusyAction:(OnoQposCommandType * _Nullable)currentlyBusyAction queueError:(OnoTerminalErrorQueue * _Nullable)queueError __attribute__((swift_name("doCopy(error:currentlyBusyAction:queueError:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoQposCommandType * _Nullable currentlyBusyAction __attribute__((swift_name("currentlyBusyAction")));
@property (readonly) NSString * _Nullable error __attribute__((swift_name("error")));
@property (readonly) OnoTerminalErrorQueue * _Nullable queueError __attribute__((swift_name("queueError")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadActions.QposExecutionTimeout")))
@interface OnoDspreadActionsQposExecutionTimeout : OnoAction
- (instancetype)initWithCommand:(OnoQposCommandType *)command __attribute__((swift_name("init(command:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoQposCommandType *)component1 __attribute__((swift_name("component1()")));
- (OnoDspreadActionsQposExecutionTimeout *)doCopyCommand:(OnoQposCommandType *)command __attribute__((swift_name("doCopy(command:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoQposCommandType *command __attribute__((swift_name("command")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadActions.QposIdRetrieved")))
@interface OnoDspreadActionsQposIdRetrieved : OnoAction
- (instancetype)initWithInfo:(NSDictionary<NSString *, NSString *> *)info __attribute__((swift_name("init(info:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSDictionary<NSString *, NSString *> *)component1 __attribute__((swift_name("component1()")));
- (OnoDspreadActionsQposIdRetrieved *)doCopyInfo:(NSDictionary<NSString *, NSString *> *)info __attribute__((swift_name("doCopy(info:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSDictionary<NSString *, NSString *> *info __attribute__((swift_name("info")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadActions.QposInfoRetrieved")))
@interface OnoDspreadActionsQposInfoRetrieved : OnoAction
- (instancetype)initWithInfo:(NSDictionary<NSString *, NSString *> *)info sdkVersion:(NSString * _Nullable)sdkVersion __attribute__((swift_name("init(info:sdkVersion:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSDictionary<NSString *, NSString *> *)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (OnoDspreadActionsQposInfoRetrieved *)doCopyInfo:(NSDictionary<NSString *, NSString *> *)info sdkVersion:(NSString * _Nullable)sdkVersion __attribute__((swift_name("doCopy(info:sdkVersion:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSDictionary<NSString *, NSString *> *info __attribute__((swift_name("info")));
@property (readonly) NSString * _Nullable sdkVersion __attribute__((swift_name("sdkVersion")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadActions.QposServiceBusy")))
@interface OnoDspreadActionsQposServiceBusy : OnoAction
- (instancetype)initWithAttemptedCommand:(OnoQposCommandType *)attemptedCommand currentCommand:(OnoQposCommandType *)currentCommand __attribute__((swift_name("init(attemptedCommand:currentCommand:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoQposCommandType *)component1 __attribute__((swift_name("component1()")));
- (OnoQposCommandType *)component2 __attribute__((swift_name("component2()")));
- (OnoDspreadActionsQposServiceBusy *)doCopyAttemptedCommand:(OnoQposCommandType *)attemptedCommand currentCommand:(OnoQposCommandType *)currentCommand __attribute__((swift_name("doCopy(attemptedCommand:currentCommand:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoQposCommandType *attemptedCommand __attribute__((swift_name("attemptedCommand")));
@property (readonly) OnoQposCommandType *currentCommand __attribute__((swift_name("currentCommand")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadActions.QposStartTimeout")))
@interface OnoDspreadActionsQposStartTimeout : OnoAction
- (instancetype)initWithCommand:(OnoQposCommandType *)command __attribute__((swift_name("init(command:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoQposCommandType *)component1 __attribute__((swift_name("component1()")));
- (OnoDspreadActionsQposStartTimeout *)doCopyCommand:(OnoQposCommandType *)command __attribute__((swift_name("doCopy(command:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoQposCommandType *command __attribute__((swift_name("command")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadActions.QposTerminalInfoRetrieved")))
@interface OnoDspreadActionsQposTerminalInfoRetrieved : OnoAction
- (instancetype)initWithInfo:(NSDictionary<NSString *, NSString *> *)info sdkVersion:(NSString * _Nullable)sdkVersion __attribute__((swift_name("init(info:sdkVersion:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSDictionary<NSString *, NSString *> *)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (OnoDspreadActionsQposTerminalInfoRetrieved *)doCopyInfo:(NSDictionary<NSString *, NSString *> *)info sdkVersion:(NSString * _Nullable)sdkVersion __attribute__((swift_name("doCopy(info:sdkVersion:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSDictionary<NSString *, NSString *> *info __attribute__((swift_name("info")));
@property (readonly) NSString * _Nullable sdkVersion __attribute__((swift_name("sdkVersion")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadActions.QposTransactionResult")))
@interface OnoDspreadActionsQposTransactionResult : OnoAction
- (instancetype)initWithTransactionResult:(OnoOnoQposTransactionResult * _Nullable)transactionResult __attribute__((swift_name("init(transactionResult:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoOnoQposTransactionResult * _Nullable)component1 __attribute__((swift_name("component1()")));
- (OnoDspreadActionsQposTransactionResult *)doCopyTransactionResult:(OnoOnoQposTransactionResult * _Nullable)transactionResult __attribute__((swift_name("doCopy(transactionResult:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoOnoQposTransactionResult * _Nullable transactionResult __attribute__((swift_name("transactionResult")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadActions.QuickResetComplete")))
@interface OnoDspreadActionsQuickResetComplete : OnoAction
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadActions.RequestEMVApplication")))
@interface OnoDspreadActionsRequestEMVApplication : OnoAction
- (instancetype)initWithEmvApplicationsList:(NSMutableArray<NSString *> * _Nullable)emvApplicationsList __attribute__((swift_name("init(emvApplicationsList:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSMutableArray<NSString *> * _Nullable)component1 __attribute__((swift_name("component1()")));
- (OnoDspreadActionsRequestEMVApplication *)doCopyEmvApplicationsList:(NSMutableArray<NSString *> * _Nullable)emvApplicationsList __attribute__((swift_name("doCopy(emvApplicationsList:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSMutableArray<NSString *> * _Nullable emvApplicationsList __attribute__((swift_name("emvApplicationsList")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadActions.RequestTime")))
@interface OnoDspreadActionsRequestTime : OnoAction
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadActions.SetAmountRequested")))
@interface OnoDspreadActionsSetAmountRequested : OnoAction
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadActions.ShutdownTimeSet")))
@interface OnoDspreadActionsShutdownTimeSet : OnoAction
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadActions.SwipeRequestPin")))
@interface OnoDspreadActionsSwipeRequestPin : OnoAction
- (instancetype)initWithTradeData:(OnoSensitiveMap * _Nullable)tradeData maskedPan:(OnoSensitiveString *)maskedPan pinEnforcedBy:(OnoPinEnforcedBy *)pinEnforcedBy __attribute__((swift_name("init(tradeData:maskedPan:pinEnforcedBy:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoSensitiveMap * _Nullable)component1 __attribute__((swift_name("component1()")));
- (OnoSensitiveString *)component2 __attribute__((swift_name("component2()")));
- (OnoPinEnforcedBy *)component3 __attribute__((swift_name("component3()")));
- (OnoDspreadActionsSwipeRequestPin *)doCopyTradeData:(OnoSensitiveMap * _Nullable)tradeData maskedPan:(OnoSensitiveString *)maskedPan pinEnforcedBy:(OnoPinEnforcedBy *)pinEnforcedBy __attribute__((swift_name("doCopy(tradeData:maskedPan:pinEnforcedBy:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoSensitiveString *maskedPan __attribute__((swift_name("maskedPan")));
@property (readonly) OnoPinEnforcedBy *pinEnforcedBy __attribute__((swift_name("pinEnforcedBy")));
@property (readonly) OnoSensitiveMap * _Nullable tradeData __attribute__((swift_name("tradeData")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadActions.TransactionProcessing")))
@interface OnoDspreadActionsTransactionProcessing : OnoAction
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadActions.UpdateFinished")))
@interface OnoDspreadActionsUpdateFinished : OnoAction
- (instancetype)initWithWasSuccessful:(BOOL)wasSuccessful error:(NSString * _Nullable)error __attribute__((swift_name("init(wasSuccessful:error:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (BOOL)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (OnoDspreadActionsUpdateFinished *)doCopyWasSuccessful:(BOOL)wasSuccessful error:(NSString * _Nullable)error __attribute__((swift_name("doCopy(wasSuccessful:error:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable error __attribute__((swift_name("error")));
@property (readonly) BOOL wasSuccessful __attribute__((swift_name("wasSuccessful")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadCommandDriver")))
@interface OnoDspreadCommandDriver : OnoCommandDriver
- (instancetype)initWithDspreadService:(OnoDspreadServiceProtocol *)dspreadService __attribute__((swift_name("init(dspreadService:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (void)receiveCommandCommand:(OnoCommand *)command __attribute__((swift_name("receiveCommand(command:)")));
@end;

__attribute__((swift_name("DspreadCommands")))
@interface OnoDspreadCommands : OnoBase
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadCommands.CancelEmvAppSelection")))
@interface OnoDspreadCommandsCancelEmvAppSelection : OnoCommand
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadCommands.CancelTrade")))
@interface OnoDspreadCommandsCancelTrade : OnoCommand
- (instancetype)initWithErrorDetails:(OnoErrorDetails *)errorDetails __attribute__((swift_name("init(errorDetails:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoErrorDetails *)component1 __attribute__((swift_name("component1()")));
- (OnoDspreadCommandsCancelTrade *)doCopyErrorDetails:(OnoErrorDetails *)errorDetails __attribute__((swift_name("doCopy(errorDetails:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoErrorDetails *errorDetails __attribute__((swift_name("errorDetails")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadCommands.CheckCardExists")))
@interface OnoDspreadCommandsCheckCardExists : OnoCommand
- (instancetype)initWithDelay:(OnoLong * _Nullable)delay __attribute__((swift_name("init(delay:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoLong * _Nullable)component1 __attribute__((swift_name("component1()")));
- (OnoDspreadCommandsCheckCardExists *)doCopyDelay:(OnoLong * _Nullable)delay __attribute__((swift_name("doCopy(delay:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoLong * _Nullable delay __attribute__((swift_name("delay")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadCommands.CheckTerminalStatus")))
@interface OnoDspreadCommandsCheckTerminalStatus : OnoCommand
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadCommands.ClearAllQposCommands")))
@interface OnoDspreadCommandsClearAllQposCommands : OnoCommand
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadCommands.ClearMessage")))
@interface OnoDspreadCommandsClearMessage : OnoCommand
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadCommands.ClearQposCommand")))
@interface OnoDspreadCommandsClearQposCommand : OnoCommand
- (instancetype)initWithCommand:(OnoQueueCommand *)command __attribute__((swift_name("init(command:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoQueueCommand *)component1 __attribute__((swift_name("component1()")));
- (OnoDspreadCommandsClearQposCommand *)doCopyCommand:(OnoQueueCommand *)command __attribute__((swift_name("doCopy(command:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoQueueCommand *command __attribute__((swift_name("command")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadCommands.ConnectQpos")))
@interface OnoDspreadCommandsConnectQpos : OnoCommand
- (instancetype)initWithAddress:(NSString *)address __attribute__((swift_name("init(address:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoDspreadCommandsConnectQpos *)doCopyAddress:(NSString *)address __attribute__((swift_name("doCopy(address:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *address __attribute__((swift_name("address")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadCommands.DelayCheckTerminalStatus")))
@interface OnoDspreadCommandsDelayCheckTerminalStatus : OnoCommand
- (instancetype)initWithDelay:(int64_t)delay statusCheckId:(int32_t)statusCheckId __attribute__((swift_name("init(delay:statusCheckId:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (int64_t)component1 __attribute__((swift_name("component1()")));
- (int32_t)component2 __attribute__((swift_name("component2()")));
- (OnoDspreadCommandsDelayCheckTerminalStatus *)doCopyDelay:(int64_t)delay statusCheckId:(int32_t)statusCheckId __attribute__((swift_name("doCopy(delay:statusCheckId:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int64_t delay __attribute__((swift_name("delay")));
@property (readonly) int32_t statusCheckId __attribute__((swift_name("statusCheckId")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadCommands.DiscardNFCBatchData")))
@interface OnoDspreadCommandsDiscardNFCBatchData : OnoCommand
- (instancetype)initWithMessage:(NSString *)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoDspreadCommandsDiscardNFCBatchData *)doCopyMessage:(NSString *)message __attribute__((swift_name("doCopy(message:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *message __attribute__((swift_name("message")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadCommands.Disconnect")))
@interface OnoDspreadCommandsDisconnect : OnoCommand
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadCommands.DoEMV")))
@interface OnoDspreadCommandsDoEMV : OnoCommand
- (instancetype)initWithTransactionType:(OnoTransactionType *)transactionType __attribute__((swift_name("init(transactionType:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoTransactionType *)component1 __attribute__((swift_name("component1()")));
- (OnoDspreadCommandsDoEMV *)doCopyTransactionType:(OnoTransactionType *)transactionType __attribute__((swift_name("doCopy(transactionType:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoTransactionType *transactionType __attribute__((swift_name("transactionType")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadCommands.DoTrade")))
@interface OnoDspreadCommandsDoTrade : OnoCommand
- (instancetype)initWithTransaction:(OnoTransaction *)transaction transactionConfig:(OnoTransactionConfig *)transactionConfig cardPresentationMode:(OnoCardPromptMode *)cardPresentationMode keyIndex:(int32_t)keyIndex __attribute__((swift_name("init(transaction:transactionConfig:cardPresentationMode:keyIndex:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoTransaction *)component1 __attribute__((swift_name("component1()")));
- (OnoTransactionConfig *)component2 __attribute__((swift_name("component2()")));
- (OnoCardPromptMode *)component3 __attribute__((swift_name("component3()")));
- (int32_t)component4 __attribute__((swift_name("component4()")));
- (OnoDspreadCommandsDoTrade *)doCopyTransaction:(OnoTransaction *)transaction transactionConfig:(OnoTransactionConfig *)transactionConfig cardPresentationMode:(OnoCardPromptMode *)cardPresentationMode keyIndex:(int32_t)keyIndex __attribute__((swift_name("doCopy(transaction:transactionConfig:cardPresentationMode:keyIndex:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoCardPromptMode *cardPresentationMode __attribute__((swift_name("cardPresentationMode")));
@property (readonly) int32_t keyIndex __attribute__((swift_name("keyIndex")));
@property (readonly) OnoTransaction *transaction __attribute__((swift_name("transaction")));
@property (readonly) OnoTransactionConfig *transactionConfig __attribute__((swift_name("transactionConfig")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadCommands.FetchNFCBatchData")))
@interface OnoDspreadCommandsFetchNFCBatchData : OnoCommand
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadCommands.GetQposId")))
@interface OnoDspreadCommandsGetQposId : OnoCommand
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadCommands.GetQposInfo")))
@interface OnoDspreadCommandsGetQposInfo : OnoCommand
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadCommands.PerformEMVAppUpdate")))
@interface OnoDspreadCommandsPerformEMVAppUpdate : OnoCommand
- (instancetype)initWithEmvAppString:(NSString *)emvAppString emvCapkString:(NSString *)emvCapkString availableUpdate:(OnoAvailableUpdate *)availableUpdate __attribute__((swift_name("init(emvAppString:emvCapkString:availableUpdate:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (OnoAvailableUpdate *)component3 __attribute__((swift_name("component3()")));
- (OnoDspreadCommandsPerformEMVAppUpdate *)doCopyEmvAppString:(NSString *)emvAppString emvCapkString:(NSString *)emvCapkString availableUpdate:(OnoAvailableUpdate *)availableUpdate __attribute__((swift_name("doCopy(emvAppString:emvCapkString:availableUpdate:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoAvailableUpdate *availableUpdate __attribute__((swift_name("availableUpdate")));
@property (readonly) NSString *emvAppString __attribute__((swift_name("emvAppString")));
@property (readonly) NSString *emvCapkString __attribute__((swift_name("emvCapkString")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadCommands.PerformUpdate")))
@interface OnoDspreadCommandsPerformUpdate : OnoCommand
- (instancetype)initWithAvailableUpdate:(OnoAvailableUpdate *)availableUpdate __attribute__((swift_name("init(availableUpdate:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoAvailableUpdate *)component1 __attribute__((swift_name("component1()")));
- (OnoDspreadCommandsPerformUpdate *)doCopyAvailableUpdate:(OnoAvailableUpdate *)availableUpdate __attribute__((swift_name("doCopy(availableUpdate:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoAvailableUpdate *availableUpdate __attribute__((swift_name("availableUpdate")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadCommands.QuickResetQpos")))
@interface OnoDspreadCommandsQuickResetQpos : OnoCommand
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadCommands.ReconnectAfterDelay")))
@interface OnoDspreadCommandsReconnectAfterDelay : OnoCommand
- (instancetype)initWithDelay:(int64_t)delay terminal:(OnoTerminal *)terminal __attribute__((swift_name("init(delay:terminal:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (int64_t)component1 __attribute__((swift_name("component1()")));
- (OnoTerminal *)component2 __attribute__((swift_name("component2()")));
- (OnoDspreadCommandsReconnectAfterDelay *)doCopyDelay:(int64_t)delay terminal:(OnoTerminal *)terminal __attribute__((swift_name("doCopy(delay:terminal:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int64_t delay __attribute__((swift_name("delay")));
@property (readonly) OnoTerminal *terminal __attribute__((swift_name("terminal")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadCommands.RequestPin")))
@interface OnoDspreadCommandsRequestPin : OnoCommand
- (instancetype)initWithMaskedPan:(OnoSensitiveString *)maskedPan timeout:(int32_t)timeout keyIndex:(int32_t)keyIndex __attribute__((swift_name("init(maskedPan:timeout:keyIndex:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoSensitiveString *)component1 __attribute__((swift_name("component1()")));
- (int32_t)component2 __attribute__((swift_name("component2()")));
- (int32_t)component3 __attribute__((swift_name("component3()")));
- (OnoDspreadCommandsRequestPin *)doCopyMaskedPan:(OnoSensitiveString *)maskedPan timeout:(int32_t)timeout keyIndex:(int32_t)keyIndex __attribute__((swift_name("doCopy(maskedPan:timeout:keyIndex:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t keyIndex __attribute__((swift_name("keyIndex")));
@property (readonly) OnoSensitiveString *maskedPan __attribute__((swift_name("maskedPan")));
@property (readonly) int32_t timeout __attribute__((swift_name("timeout")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadCommands.ResetQpos")))
@interface OnoDspreadCommandsResetQpos : OnoCommand
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString * _Nullable)component1 __attribute__((swift_name("component1()")));
- (OnoDspreadCommandsResetQpos *)doCopyMessage:(NSString * _Nullable)message __attribute__((swift_name("doCopy(message:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable message __attribute__((swift_name("message")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadCommands.RetrieveLatestEMVUpdatePerformed")))
@interface OnoDspreadCommandsRetrieveLatestEMVUpdatePerformed : OnoCommand
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadCommands.SetAmount")))
@interface OnoDspreadCommandsSetAmount : OnoCommand
- (instancetype)initWithTransactionAmountInCents:(int64_t)transactionAmountInCents currency:(OnoCurrency *)currency __attribute__((swift_name("init(transactionAmountInCents:currency:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (int64_t)component1 __attribute__((swift_name("component1()")));
- (OnoCurrency *)component2 __attribute__((swift_name("component2()")));
- (OnoDspreadCommandsSetAmount *)doCopyTransactionAmountInCents:(int64_t)transactionAmountInCents currency:(OnoCurrency *)currency __attribute__((swift_name("doCopy(transactionAmountInCents:currency:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoCurrency *currency __attribute__((swift_name("currency")));
@property (readonly) int64_t transactionAmountInCents __attribute__((swift_name("transactionAmountInCents")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadCommands.SetBuzzerStatus")))
@interface OnoDspreadCommandsSetBuzzerStatus : OnoCommand
- (instancetype)initWithEnableSound:(BOOL)enableSound __attribute__((swift_name("init(enableSound:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (BOOL)component1 __attribute__((swift_name("component1()")));
- (OnoDspreadCommandsSetBuzzerStatus *)doCopyEnableSound:(BOOL)enableSound __attribute__((swift_name("doCopy(enableSound:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL enableSound __attribute__((swift_name("enableSound")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadCommands.SetShutdownTime")))
@interface OnoDspreadCommandsSetShutdownTime : OnoCommand
- (instancetype)initWithShutdownTime:(int32_t)shutdownTime __attribute__((swift_name("init(shutdownTime:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (int32_t)component1 __attribute__((swift_name("component1()")));
- (OnoDspreadCommandsSetShutdownTime *)doCopyShutdownTime:(int32_t)shutdownTime __attribute__((swift_name("doCopy(shutdownTime:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t shutdownTime __attribute__((swift_name("shutdownTime")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadCommands.SetTime")))
@interface OnoDspreadCommandsSetTime : OnoCommand
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadCommands.ShowMessage")))
@interface OnoDspreadCommandsShowMessage : OnoCommand
- (instancetype)initWithMessage:(NSString *)message timeoutInSeconds:(OnoInt * _Nullable)timeoutInSeconds maximumMessageTimeout:(int32_t)maximumMessageTimeout timeoutDelay:(int32_t)timeoutDelay __attribute__((swift_name("init(message:timeoutInSeconds:maximumMessageTimeout:timeoutDelay:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoInt * _Nullable)component2 __attribute__((swift_name("component2()")));
- (int32_t)component3 __attribute__((swift_name("component3()")));
- (int32_t)component4 __attribute__((swift_name("component4()")));
- (OnoDspreadCommandsShowMessage *)doCopyMessage:(NSString *)message timeoutInSeconds:(OnoInt * _Nullable)timeoutInSeconds maximumMessageTimeout:(int32_t)maximumMessageTimeout timeoutDelay:(int32_t)timeoutDelay __attribute__((swift_name("doCopy(message:timeoutInSeconds:maximumMessageTimeout:timeoutDelay:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t maximumMessageTimeout __attribute__((swift_name("maximumMessageTimeout")));
@property (readonly) NSString *message __attribute__((swift_name("message")));
@property (readonly) int32_t timeoutDelay __attribute__((swift_name("timeoutDelay")));
@property (readonly) OnoInt * _Nullable timeoutInSeconds __attribute__((swift_name("timeoutInSeconds")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadCommands.SubmitARPC")))
@interface OnoDspreadCommandsSubmitARPC : OnoCommand
- (instancetype)initWithArpc:(OnoSensitiveString *)arpc __attribute__((swift_name("init(arpc:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoSensitiveString *)component1 __attribute__((swift_name("component1()")));
- (OnoDspreadCommandsSubmitARPC *)doCopyArpc:(OnoSensitiveString *)arpc __attribute__((swift_name("doCopy(arpc:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoSensitiveString *arpc __attribute__((swift_name("arpc")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadCommands.SubmitEMVOption")))
@interface OnoDspreadCommandsSubmitEMVOption : OnoCommand
- (instancetype)initWithEmvOption:(int32_t)emvOption __attribute__((swift_name("init(emvOption:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (int32_t)component1 __attribute__((swift_name("component1()")));
- (OnoDspreadCommandsSubmitEMVOption *)doCopyEmvOption:(int32_t)emvOption __attribute__((swift_name("doCopy(emvOption:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t emvOption __attribute__((swift_name("emvOption")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadData")))
@interface OnoDspreadData : OnoBase <OnoStoreData>
- (instancetype)initWithConnectionStatus:(OnoConnectionStatus * _Nullable)connectionStatus transactionStep:(OnoTransactionStep * _Nullable)transactionStep transactionAmountInCents:(OnoLong * _Nullable)transactionAmountInCents transactionCurrency:(OnoCurrency * _Nullable)transactionCurrency transactionConfig:(OnoTransactionConfig *)transactionConfig tradeResult:(OnoTransactionType *)tradeResult tradeData:(OnoSensitiveMap * _Nullable)tradeData shownMessage:(NSString * _Nullable)shownMessage maximumMessageTimeout:(int32_t)maximumMessageTimeout defaultDelayTimeout:(int32_t)defaultDelayTimeout cardExistsDelayMillis:(int64_t)cardExistsDelayMillis completedTransactionState:(OnoTransactionState * _Nullable)completedTransactionState terminalConfig:(OnoTerminalConfig *)terminalConfig terminalKeptAwakeLength:(OnoLong * _Nullable)terminalKeptAwakeLength connectionsAttempted:(int32_t)connectionsAttempted currentlyConnectingTerminal:(OnoTerminal * _Nullable)currentlyConnectingTerminal isCurrentTerminalUpdating:(BOOL)isCurrentTerminalUpdating statusCheckId:(int32_t)statusCheckId busyApplyingUpdate:(OnoAvailableUpdate * _Nullable)busyApplyingUpdate cardPresentationMode:(OnoCardPromptMode *)cardPresentationMode __attribute__((swift_name("init(connectionStatus:transactionStep:transactionAmountInCents:transactionCurrency:transactionConfig:tradeResult:tradeData:shownMessage:maximumMessageTimeout:defaultDelayTimeout:cardExistsDelayMillis:completedTransactionState:terminalConfig:terminalKeptAwakeLength:connectionsAttempted:currentlyConnectingTerminal:isCurrentTerminalUpdating:statusCheckId:busyApplyingUpdate:cardPresentationMode:)"))) __attribute__((objc_designated_initializer));
- (OnoConnectionStatus * _Nullable)component1 __attribute__((swift_name("component1()")));
- (int32_t)component10 __attribute__((swift_name("component10()")));
- (int64_t)component11 __attribute__((swift_name("component11()")));
- (OnoTransactionState * _Nullable)component12 __attribute__((swift_name("component12()")));
- (OnoTerminalConfig *)component13 __attribute__((swift_name("component13()")));
- (OnoLong * _Nullable)component14 __attribute__((swift_name("component14()")));
- (int32_t)component15 __attribute__((swift_name("component15()")));
- (OnoTerminal * _Nullable)component16 __attribute__((swift_name("component16()")));
- (BOOL)component17 __attribute__((swift_name("component17()")));
- (int32_t)component18 __attribute__((swift_name("component18()")));
- (OnoAvailableUpdate * _Nullable)component19 __attribute__((swift_name("component19()")));
- (OnoTransactionStep * _Nullable)component2 __attribute__((swift_name("component2()")));
- (OnoCardPromptMode *)component20 __attribute__((swift_name("component20()")));
- (OnoLong * _Nullable)component3 __attribute__((swift_name("component3()")));
- (OnoCurrency * _Nullable)component4 __attribute__((swift_name("component4()")));
- (OnoTransactionConfig *)component5 __attribute__((swift_name("component5()")));
- (OnoTransactionType *)component6 __attribute__((swift_name("component6()")));
- (OnoSensitiveMap * _Nullable)component7 __attribute__((swift_name("component7()")));
- (NSString * _Nullable)component8 __attribute__((swift_name("component8()")));
- (int32_t)component9 __attribute__((swift_name("component9()")));
- (OnoDspreadData *)doCopyConnectionStatus:(OnoConnectionStatus * _Nullable)connectionStatus transactionStep:(OnoTransactionStep * _Nullable)transactionStep transactionAmountInCents:(OnoLong * _Nullable)transactionAmountInCents transactionCurrency:(OnoCurrency * _Nullable)transactionCurrency transactionConfig:(OnoTransactionConfig *)transactionConfig tradeResult:(OnoTransactionType *)tradeResult tradeData:(OnoSensitiveMap * _Nullable)tradeData shownMessage:(NSString * _Nullable)shownMessage maximumMessageTimeout:(int32_t)maximumMessageTimeout defaultDelayTimeout:(int32_t)defaultDelayTimeout cardExistsDelayMillis:(int64_t)cardExistsDelayMillis completedTransactionState:(OnoTransactionState * _Nullable)completedTransactionState terminalConfig:(OnoTerminalConfig *)terminalConfig terminalKeptAwakeLength:(OnoLong * _Nullable)terminalKeptAwakeLength connectionsAttempted:(int32_t)connectionsAttempted currentlyConnectingTerminal:(OnoTerminal * _Nullable)currentlyConnectingTerminal isCurrentTerminalUpdating:(BOOL)isCurrentTerminalUpdating statusCheckId:(int32_t)statusCheckId busyApplyingUpdate:(OnoAvailableUpdate * _Nullable)busyApplyingUpdate cardPresentationMode:(OnoCardPromptMode *)cardPresentationMode __attribute__((swift_name("doCopy(connectionStatus:transactionStep:transactionAmountInCents:transactionCurrency:transactionConfig:tradeResult:tradeData:shownMessage:maximumMessageTimeout:defaultDelayTimeout:cardExistsDelayMillis:completedTransactionState:terminalConfig:terminalKeptAwakeLength:connectionsAttempted:currentlyConnectingTerminal:isCurrentTerminalUpdating:statusCheckId:busyApplyingUpdate:cardPresentationMode:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoAvailableUpdate * _Nullable busyApplyingUpdate __attribute__((swift_name("busyApplyingUpdate")));
@property (readonly) int64_t cardExistsDelayMillis __attribute__((swift_name("cardExistsDelayMillis")));
@property (readonly) OnoCardPromptMode *cardPresentationMode __attribute__((swift_name("cardPresentationMode")));
@property (readonly) OnoTransactionState * _Nullable completedTransactionState __attribute__((swift_name("completedTransactionState")));
@property (readonly) OnoConnectionStatus * _Nullable connectionStatus __attribute__((swift_name("connectionStatus")));
@property (readonly) int32_t connectionsAttempted __attribute__((swift_name("connectionsAttempted")));
@property (readonly) OnoTerminal * _Nullable currentlyConnectingTerminal __attribute__((swift_name("currentlyConnectingTerminal")));
@property (readonly) int32_t defaultDelayTimeout __attribute__((swift_name("defaultDelayTimeout")));
@property (readonly) BOOL isCurrentTerminalUpdating __attribute__((swift_name("isCurrentTerminalUpdating")));
@property (readonly) int32_t maximumMessageTimeout __attribute__((swift_name("maximumMessageTimeout")));
@property (readonly) NSString * _Nullable shownMessage __attribute__((swift_name("shownMessage")));
@property (readonly) int32_t statusCheckId __attribute__((swift_name("statusCheckId")));
@property (readonly) OnoTerminalConfig *terminalConfig __attribute__((swift_name("terminalConfig")));
@property (readonly) OnoLong * _Nullable terminalKeptAwakeLength __attribute__((swift_name("terminalKeptAwakeLength")));
@property (readonly) OnoSensitiveMap * _Nullable tradeData __attribute__((swift_name("tradeData")));
@property (readonly) OnoTransactionType *tradeResult __attribute__((swift_name("tradeResult")));
@property (readonly) OnoLong * _Nullable transactionAmountInCents __attribute__((swift_name("transactionAmountInCents")));
@property (readonly) OnoTransactionConfig *transactionConfig __attribute__((swift_name("transactionConfig")));
@property (readonly) OnoCurrency * _Nullable transactionCurrency __attribute__((swift_name("transactionCurrency")));
@property (readonly) OnoTransactionStep * _Nullable transactionStep __attribute__((swift_name("transactionStep")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DspreadStateMachine")))
@interface OnoDspreadStateMachine : OnoBase <OnoStateMachine>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)dspreadStateMachine __attribute__((swift_name("init()")));
- (OnoDspreadData *)castStoreDataStoreData:(id<OnoStoreData>)storeData __attribute__((swift_name("castStoreData(storeData:)")));
- (OnoStateMachineResult *)consumeActionStoreData:(id<OnoStoreData>)storeData action:(OnoAction *)action __attribute__((swift_name("consumeAction(storeData:action:)")));
- (OnoDspreadData *)getCurrentStateStore:(OnoStore *)store __attribute__((swift_name("getCurrentState(store:)")));
- (OnoDspreadData *)getCurrentStateState:(NSDictionary<NSString *, id<OnoStoreData>> *)state __attribute__((swift_name("getCurrentState(state:)")));
- (OnoDspreadData *)getStoreDataState:(NSDictionary<NSString *, id<OnoStoreData>> *)state __attribute__((swift_name("getStoreData(state:)")));
@property (readonly) NSString *TAG __attribute__((swift_name("TAG")));
@property (readonly) OnoDspreadData *initialState __attribute__((swift_name("initialState")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TransactionConfig")))
@interface OnoTransactionConfig : OnoBase
- (instancetype)initWithTransactionTimeout:(int32_t)transactionTimeout transactionFinalMessageLinger:(int32_t)transactionFinalMessageLinger __attribute__((swift_name("init(transactionTimeout:transactionFinalMessageLinger:)"))) __attribute__((objc_designated_initializer));
- (int32_t)component1 __attribute__((swift_name("component1()")));
- (int32_t)component2 __attribute__((swift_name("component2()")));
- (OnoTransactionConfig *)doCopyTransactionTimeout:(int32_t)transactionTimeout transactionFinalMessageLinger:(int32_t)transactionFinalMessageLinger __attribute__((swift_name("doCopy(transactionTimeout:transactionFinalMessageLinger:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t transactionFinalMessageLinger __attribute__((swift_name("transactionFinalMessageLinger")));
@property (readonly) int32_t transactionTimeout __attribute__((swift_name("transactionTimeout")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("BluetoothModule")))
@interface OnoBluetoothModule : OnoBase <OnoOnoSDKModule>
- (instancetype)initWithBluetoothService:(OnoBluetoothServiceProtocol *)bluetoothService __attribute__((swift_name("init(bluetoothService:)"))) __attribute__((objc_designated_initializer));
@property (readonly) NSArray<OnoCommandDriver *> *commandDrivers __attribute__((swift_name("commandDrivers")));
@property (readonly) NSArray<id<OnoStateMachine>> *stateMachines __attribute__((swift_name("stateMachines")));
@property (readonly) NSArray<OnoStoreListener *> *storeListeners __attribute__((swift_name("storeListeners")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OnoSDKBluetooth")))
@interface OnoOnoSDKBluetooth : OnoBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)onoSDKBluetooth __attribute__((swift_name("init()")));
- (void)enableBluetooth __attribute__((swift_name("enableBluetooth()")));
- (void)setListenerBluetoothListener:(id<OnoBluetoothListener>)bluetoothListener __attribute__((swift_name("setListener(bluetoothListener:)")));
@property (readonly) OnoBluetoothListenerHelper *listener __attribute__((swift_name("listener")));
@end;

__attribute__((swift_name("BluetoothServiceProtocol")))
@interface OnoBluetoothServiceProtocol : OnoDispatcher
- (instancetype)initWithDispatch:(void (^)(OnoAction *))dispatch __attribute__((swift_name("init(dispatch:)"))) __attribute__((objc_designated_initializer));
- (void)cancelPairingAddress:(NSString *)address __attribute__((swift_name("cancelPairing(address:)")));
- (void)enableBluetooth __attribute__((swift_name("enableBluetooth()")));
- (void)fetchPairedDevices __attribute__((swift_name("fetchPairedDevices()")));
- (void)doInitState __attribute__((swift_name("doInitState()")));
- (void)pairDeviceAddress:(NSString *)address __attribute__((swift_name("pairDevice(address:)")));
- (void)showPairingDialog __attribute__((swift_name("showPairingDialog()")));
- (void)startSearching __attribute__((swift_name("startSearching()")));
- (void)stopSearching __attribute__((swift_name("stopSearching()")));
- (void)unpairDeviceAddress:(NSString *)address __attribute__((swift_name("unpairDevice(address:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MockBluetoothService")))
@interface OnoMockBluetoothService : OnoBluetoothServiceProtocol
- (instancetype)initWithDispatch:(void (^)(OnoAction *))dispatch __attribute__((swift_name("init(dispatch:)"))) __attribute__((objc_designated_initializer));
- (void)cancelPairingAddress:(NSString *)address __attribute__((swift_name("cancelPairing(address:)")));
- (void)enableBluetooth __attribute__((swift_name("enableBluetooth()")));
- (void)fetchPairedDevices __attribute__((swift_name("fetchPairedDevices()")));
- (void)doInitState __attribute__((swift_name("doInitState()")));
- (void)pairDeviceAddress:(NSString *)address __attribute__((swift_name("pairDevice(address:)")));
- (void)showPairingDialog __attribute__((swift_name("showPairingDialog()")));
- (void)startSearching __attribute__((swift_name("startSearching()")));
- (void)stopSearching __attribute__((swift_name("stopSearching()")));
- (void)unpairDeviceAddress:(NSString *)address __attribute__((swift_name("unpairDevice(address:)")));
@property NSString * _Nullable failPairingReason __attribute__((swift_name("failPairingReason")));
@property (readonly) NSString *tag __attribute__((swift_name("tag")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MockBluetoothService.Companion")))
@interface OnoMockBluetoothServiceCompanion : OnoBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (readonly) NSArray<OnoBluetoothDevice *> *bluetoothDevices __attribute__((swift_name("bluetoothDevices")));
@end;

__attribute__((swift_name("BluetoothActions")))
@interface OnoBluetoothActions : OnoBase
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("BluetoothActions.DeviceConnected")))
@interface OnoBluetoothActionsDeviceConnected : OnoAction
- (instancetype)initWithDevice:(OnoBluetoothDevice *)device __attribute__((swift_name("init(device:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoBluetoothDevice *)component1 __attribute__((swift_name("component1()")));
- (OnoBluetoothActionsDeviceConnected *)doCopyDevice:(OnoBluetoothDevice *)device __attribute__((swift_name("doCopy(device:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoBluetoothDevice *device __attribute__((swift_name("device")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("BluetoothActions.DeviceDiscovered")))
@interface OnoBluetoothActionsDeviceDiscovered : OnoAction
- (instancetype)initWithDiscoveredDevice:(OnoBluetoothDevice *)discoveredDevice __attribute__((swift_name("init(discoveredDevice:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoBluetoothDevice *)component1 __attribute__((swift_name("component1()")));
- (OnoBluetoothActionsDeviceDiscovered *)doCopyDiscoveredDevice:(OnoBluetoothDevice *)discoveredDevice __attribute__((swift_name("doCopy(discoveredDevice:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoBluetoothDevice *discoveredDevice __attribute__((swift_name("discoveredDevice")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("BluetoothActions.DeviceIsPairing")))
@interface OnoBluetoothActionsDeviceIsPairing : OnoAction
- (instancetype)initWithDevice:(OnoBluetoothDevice *)device __attribute__((swift_name("init(device:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoBluetoothDevice *)component1 __attribute__((swift_name("component1()")));
- (OnoBluetoothActionsDeviceIsPairing *)doCopyDevice:(OnoBluetoothDevice *)device __attribute__((swift_name("doCopy(device:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoBluetoothDevice *device __attribute__((swift_name("device")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("BluetoothActions.DevicePaired")))
@interface OnoBluetoothActionsDevicePaired : OnoAction
- (instancetype)initWithPairedDevice:(OnoBluetoothDevice *)pairedDevice __attribute__((swift_name("init(pairedDevice:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoBluetoothDevice *)component1 __attribute__((swift_name("component1()")));
- (OnoBluetoothActionsDevicePaired *)doCopyPairedDevice:(OnoBluetoothDevice *)pairedDevice __attribute__((swift_name("doCopy(pairedDevice:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoBluetoothDevice *pairedDevice __attribute__((swift_name("pairedDevice")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("BluetoothActions.DevicePairingFailed")))
@interface OnoBluetoothActionsDevicePairingFailed : OnoAction
- (instancetype)initWithUnpairedDevice:(OnoBluetoothDevice *)unpairedDevice reason:(NSString *)reason __attribute__((swift_name("init(unpairedDevice:reason:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoBluetoothDevice *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (OnoBluetoothActionsDevicePairingFailed *)doCopyUnpairedDevice:(OnoBluetoothDevice *)unpairedDevice reason:(NSString *)reason __attribute__((swift_name("doCopy(unpairedDevice:reason:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *reason __attribute__((swift_name("reason")));
@property (readonly) OnoBluetoothDevice *unpairedDevice __attribute__((swift_name("unpairedDevice")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("BluetoothActions.DiscoveryFinished")))
@interface OnoBluetoothActionsDiscoveryFinished : OnoAction
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("BluetoothActions.DiscoveryStarted")))
@interface OnoBluetoothActionsDiscoveryStarted : OnoAction
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("BluetoothActions.StateChanged")))
@interface OnoBluetoothActionsStateChanged : OnoAction
- (instancetype)initWithState:(OnoBluetoothState *)state __attribute__((swift_name("init(state:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (OnoBluetoothState *)component1 __attribute__((swift_name("component1()")));
- (OnoBluetoothActionsStateChanged *)doCopyState:(OnoBluetoothState *)state __attribute__((swift_name("doCopy(state:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoBluetoothState *state __attribute__((swift_name("state")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("BluetoothActions.UnpairingDevice")))
@interface OnoBluetoothActionsUnpairingDevice : OnoAction
- (instancetype)initWithDeviceAddress:(NSString *)deviceAddress __attribute__((swift_name("init(deviceAddress:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoBluetoothActionsUnpairingDevice *)doCopyDeviceAddress:(NSString *)deviceAddress __attribute__((swift_name("doCopy(deviceAddress:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *deviceAddress __attribute__((swift_name("deviceAddress")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("BluetoothCommandDriver")))
@interface OnoBluetoothCommandDriver : OnoCommandDriver
- (instancetype)initWithBluetoothService:(OnoBluetoothServiceProtocol *)bluetoothService __attribute__((swift_name("init(bluetoothService:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (void)receiveCommandCommand:(OnoCommand *)command __attribute__((swift_name("receiveCommand(command:)")));
@end;

__attribute__((swift_name("BluetoothCommands")))
@interface OnoBluetoothCommands : OnoBase
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("BluetoothCommands.AbortDevicePairing")))
@interface OnoBluetoothCommandsAbortDevicePairing : OnoCommand
- (instancetype)initWithAddress:(NSString *)address __attribute__((swift_name("init(address:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoBluetoothCommandsAbortDevicePairing *)doCopyAddress:(NSString *)address __attribute__((swift_name("doCopy(address:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *address __attribute__((swift_name("address")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("BluetoothCommands.EnableBluetooth")))
@interface OnoBluetoothCommandsEnableBluetooth : OnoCommand
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("BluetoothCommands.FetchInitialState")))
@interface OnoBluetoothCommandsFetchInitialState : OnoCommand
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("BluetoothCommands.FetchPairedDevices")))
@interface OnoBluetoothCommandsFetchPairedDevices : OnoCommand
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("BluetoothCommands.PairDevice")))
@interface OnoBluetoothCommandsPairDevice : OnoCommand
- (instancetype)initWithAddress:(NSString *)address __attribute__((swift_name("init(address:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoBluetoothCommandsPairDevice *)doCopyAddress:(NSString *)address __attribute__((swift_name("doCopy(address:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *address __attribute__((swift_name("address")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("BluetoothCommands.StartDiscovering")))
@interface OnoBluetoothCommandsStartDiscovering : OnoCommand
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("BluetoothCommands.StopDiscovering")))
@interface OnoBluetoothCommandsStopDiscovering : OnoCommand
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("BluetoothCommands.UnpairDevice")))
@interface OnoBluetoothCommandsUnpairDevice : OnoCommand
- (instancetype)initWithAddress:(NSString *)address __attribute__((swift_name("init(address:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (OnoBluetoothCommandsUnpairDevice *)doCopyAddress:(NSString *)address __attribute__((swift_name("doCopy(address:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *address __attribute__((swift_name("address")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("BluetoothStateData")))
@interface OnoBluetoothStateData : OnoBase <OnoStoreData>
- (instancetype)initWithBluetoothState:(OnoBluetoothState *)bluetoothState shouldSearch:(BOOL)shouldSearch discoveredDevices:(NSArray<OnoBluetoothDevice *> *)discoveredDevices isSearching:(BOOL)isSearching currentSearchTimeout:(int32_t)currentSearchTimeout timesToSearch:(int32_t)timesToSearch __attribute__((swift_name("init(bluetoothState:shouldSearch:discoveredDevices:isSearching:currentSearchTimeout:timesToSearch:)"))) __attribute__((objc_designated_initializer));
- (OnoBluetoothState *)component1 __attribute__((swift_name("component1()")));
- (BOOL)component2 __attribute__((swift_name("component2()")));
- (NSArray<OnoBluetoothDevice *> *)component3 __attribute__((swift_name("component3()")));
- (BOOL)component4 __attribute__((swift_name("component4()")));
- (int32_t)component5 __attribute__((swift_name("component5()")));
- (int32_t)component6 __attribute__((swift_name("component6()")));
- (OnoBluetoothStateData *)doCopyBluetoothState:(OnoBluetoothState *)bluetoothState shouldSearch:(BOOL)shouldSearch discoveredDevices:(NSArray<OnoBluetoothDevice *> *)discoveredDevices isSearching:(BOOL)isSearching currentSearchTimeout:(int32_t)currentSearchTimeout timesToSearch:(int32_t)timesToSearch __attribute__((swift_name("doCopy(bluetoothState:shouldSearch:discoveredDevices:isSearching:currentSearchTimeout:timesToSearch:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoBluetoothState *bluetoothState __attribute__((swift_name("bluetoothState")));
@property (readonly) int32_t currentSearchTimeout __attribute__((swift_name("currentSearchTimeout")));
@property (readonly) NSArray<OnoBluetoothDevice *> *discoveredDevices __attribute__((swift_name("discoveredDevices")));
@property (readonly) BOOL isSearching __attribute__((swift_name("isSearching")));
@property (readonly) BOOL shouldSearch __attribute__((swift_name("shouldSearch")));
@property (readonly) int32_t timesToSearch __attribute__((swift_name("timesToSearch")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("BluetoothStateMachine")))
@interface OnoBluetoothStateMachine : OnoBase <OnoStateMachine>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)bluetoothStateMachine __attribute__((swift_name("init()")));
- (OnoBluetoothStateData *)castStoreDataStoreData:(id<OnoStoreData>)storeData __attribute__((swift_name("castStoreData(storeData:)")));
- (OnoStateMachineResult *)consumeActionStoreData:(id<OnoStoreData>)storeData action:(OnoAction *)action __attribute__((swift_name("consumeAction(storeData:action:)")));
- (OnoBluetoothStatus *)getBluetoothStateState:(NSDictionary<NSString *, id<OnoStoreData>> *)state __attribute__((swift_name("getBluetoothState(state:)")));
- (OnoBluetoothStateData *)getCurrentStateStore:(OnoStore *)store __attribute__((swift_name("getCurrentState(store:)")));
- (OnoBluetoothStateData *)getCurrentStateState:(NSDictionary<NSString *, id<OnoStoreData>> *)state __attribute__((swift_name("getCurrentState(state:)")));
- (OnoBluetoothStateData *)getStoreDataState:(NSDictionary<NSString *, id<OnoStoreData>> *)state __attribute__((swift_name("getStoreData(state:)")));
@property (readonly) NSString *TAG __attribute__((swift_name("TAG")));
@property (readonly) OnoBluetoothStateData *initialState __attribute__((swift_name("initialState")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((swift_name("BluetoothListener")))
@protocol OnoBluetoothListener
@required
- (void)bluetoothStatusUpdatedBluetoothStatus:(OnoBluetoothStatus *)bluetoothStatus __attribute__((swift_name("bluetoothStatusUpdated(bluetoothStatus:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("BluetoothListenerHelper")))
@interface OnoBluetoothListenerHelper : OnoBase <OnoBluetoothListener>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)bluetoothStatusUpdatedBluetoothStatus:(OnoBluetoothStatus *)bluetoothStatus __attribute__((swift_name("bluetoothStatusUpdated(bluetoothStatus:)")));
@property id<OnoBluetoothListener> _Nullable listener __attribute__((swift_name("listener")));
@property (readonly) NSArray<OnoStoreListener *> *storeListeners __attribute__((swift_name("storeListeners")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("BluetoothStatus")))
@interface OnoBluetoothStatus : OnoBase
- (instancetype)initWithState:(OnoBluetoothState *)state isSearching:(BOOL)isSearching __attribute__((swift_name("init(state:isSearching:)"))) __attribute__((objc_designated_initializer));
- (OnoBluetoothState *)component1 __attribute__((swift_name("component1()")));
- (BOOL)component2 __attribute__((swift_name("component2()")));
- (OnoBluetoothStatus *)doCopyState:(OnoBluetoothState *)state isSearching:(BOOL)isSearching __attribute__((swift_name("doCopy(state:isSearching:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL isSearching __attribute__((swift_name("isSearching")));
@property (readonly) OnoBluetoothState *state __attribute__((swift_name("state")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("BluetoothDevice")))
@interface OnoBluetoothDevice : OnoBase
- (instancetype)initWithAddress:(NSString *)address name:(NSString * _Nullable)name bondState:(OnoBluetoothBondState *)bondState __attribute__((swift_name("init(address:name:bondState:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (OnoBluetoothBondState *)component3 __attribute__((swift_name("component3()")));
- (OnoBluetoothDevice *)doCopyAddress:(NSString *)address name:(NSString * _Nullable)name bondState:(OnoBluetoothBondState *)bondState __attribute__((swift_name("doCopy(address:name:bondState:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *address __attribute__((swift_name("address")));
@property (readonly) OnoBluetoothBondState *bondState __attribute__((swift_name("bondState")));
@property (readonly) NSString * _Nullable name __attribute__((swift_name("name")));
@end;

__attribute__((swift_name("BluetoothErrorDetails")))
@interface OnoBluetoothErrorDetails : OnoErrorDetails
- (instancetype)initWithUserDescription:(NSString *)userDescription debuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("init(userDescription:debuggingInfo:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("BluetoothErrorDetails.BluetoothDisabled")))
@interface OnoBluetoothErrorDetailsBluetoothDisabled : OnoBluetoothErrorDetails
- (instancetype)initWithDebuggingInfo:(id _Nullable)debuggingInfo __attribute__((swift_name("init(debuggingInfo:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("BluetoothBondState")))
@interface OnoBluetoothBondState : OnoKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) OnoBluetoothBondState *stateNotPaired __attribute__((swift_name("stateNotPaired")));
@property (class, readonly) OnoBluetoothBondState *statePairing __attribute__((swift_name("statePairing")));
@property (class, readonly) OnoBluetoothBondState *statePaired __attribute__((swift_name("statePaired")));
- (int32_t)compareToOther:(OnoBluetoothBondState *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("BluetoothState")))
@interface OnoBluetoothState : OnoKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) OnoBluetoothState *stateOff __attribute__((swift_name("stateOff")));
@property (class, readonly) OnoBluetoothState *stateTurningOn __attribute__((swift_name("stateTurningOn")));
@property (class, readonly) OnoBluetoothState *stateOn __attribute__((swift_name("stateOn")));
@property (class, readonly) OnoBluetoothState *stateTurningOff __attribute__((swift_name("stateTurningOff")));
@property (class, readonly) OnoBluetoothState *unknown __attribute__((swift_name("unknown")));
- (int32_t)compareToOther:(OnoBluetoothState *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MainDispatcherKt")))
@interface OnoMainDispatcherKt : OnoBase
+ (void)dispatchOnMainBlock:(void (^)(void))block __attribute__((swift_name("dispatchOnMain(block:)")));
+ (NSString *)formattedAmountAmount:(float)amount numFractals:(int32_t)numFractals __attribute__((swift_name("formattedAmount(amount:numFractals:)")));
+ (void)runAsyncAsyncExceptionHandler:(id<OnoKotlinx_coroutines_coreCoroutineExceptionHandler>)asyncExceptionHandler codeBlock:(void (^)(void))codeBlock __attribute__((swift_name("runAsync(asyncExceptionHandler:codeBlock:)")));
+ (void)runWithLockMutex:(id<OnoKotlinx_coroutines_coreMutex>)mutex lockOwner:(NSString *)lockOwner onError:(void (^)(OnoKotlinThrowable *))onError block:(void (^)(void))block __attribute__((swift_name("runWithLock(mutex:lockOwner:onError:block:)")));
+ (NSString *)uuidString __attribute__((swift_name("uuidString()")));
@end;

__attribute__((swift_name("KotlinCoroutineContext")))
@protocol OnoKotlinCoroutineContext
@required
- (id _Nullable)foldInitial:(id _Nullable)initial operation:(id _Nullable (^)(id _Nullable, id<OnoKotlinCoroutineContextElement>))operation __attribute__((swift_name("fold(initial:operation:)")));
- (id<OnoKotlinCoroutineContextElement> _Nullable)getKey:(id<OnoKotlinCoroutineContextKey>)key __attribute__((swift_name("get(key:)")));
- (id<OnoKotlinCoroutineContext>)minusKeyKey:(id<OnoKotlinCoroutineContextKey>)key __attribute__((swift_name("minusKey(key:)")));
- (id<OnoKotlinCoroutineContext>)plusContext:(id<OnoKotlinCoroutineContext>)context __attribute__((swift_name("plus(context:)")));
@end;

__attribute__((swift_name("KotlinCoroutineContextElement")))
@protocol OnoKotlinCoroutineContextElement <OnoKotlinCoroutineContext>
@required
@property (readonly) id<OnoKotlinCoroutineContextKey> key __attribute__((swift_name("key")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreCoroutineExceptionHandler")))
@protocol OnoKotlinx_coroutines_coreCoroutineExceptionHandler <OnoKotlinCoroutineContextElement>
@required
- (void)handleExceptionContext:(id<OnoKotlinCoroutineContext>)context exception:(OnoKotlinThrowable *)exception __attribute__((swift_name("handleException(context:exception:)")));
@end;

__attribute__((swift_name("KotlinThrowable")))
@interface OnoKotlinThrowable : OnoBase
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(OnoKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(OnoKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (OnoKotlinArray *)getStackTrace __attribute__((swift_name("getStackTrace()")));
- (void)printStackTrace __attribute__((swift_name("printStackTrace()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoKotlinThrowable * _Nullable cause __attribute__((swift_name("cause")));
@property (readonly) NSString * _Nullable message __attribute__((swift_name("message")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinByteArray")))
@interface OnoKotlinByteArray : OnoBase
+ (instancetype)arrayWithSize:(int32_t)size __attribute__((swift_name("init(size:)")));
+ (instancetype)arrayWithSize:(int32_t)size init:(OnoByte *(^)(OnoInt *))init __attribute__((swift_name("init(size:init:)")));
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (int8_t)getIndex:(int32_t)index __attribute__((swift_name("get(index:)")));
- (OnoKotlinByteIterator *)iterator __attribute__((swift_name("iterator()")));
- (void)setIndex:(int32_t)index value:(int8_t)value __attribute__((swift_name("set(index:value:)")));
@property (readonly) int32_t size __attribute__((swift_name("size")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinArray")))
@interface OnoKotlinArray : OnoBase
+ (instancetype)arrayWithSize:(int32_t)size init:(id _Nullable (^)(OnoInt *))init __attribute__((swift_name("init(size:init:)")));
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (id _Nullable)getIndex:(int32_t)index __attribute__((swift_name("get(index:)")));
- (id<OnoKotlinIterator>)iterator __attribute__((swift_name("iterator()")));
- (void)setIndex:(int32_t)index value:(id _Nullable)value __attribute__((swift_name("set(index:value:)")));
@property (readonly) int32_t size __attribute__((swift_name("size")));
@end;

__attribute__((swift_name("KotlinKDeclarationContainer")))
@protocol OnoKotlinKDeclarationContainer
@required
@end;

__attribute__((swift_name("KotlinKAnnotatedElement")))
@protocol OnoKotlinKAnnotatedElement
@required
@end;

__attribute__((swift_name("KotlinKClassifier")))
@protocol OnoKotlinKClassifier
@required
@end;

__attribute__((swift_name("KotlinKClass")))
@protocol OnoKotlinKClass <OnoKotlinKDeclarationContainer, OnoKotlinKAnnotatedElement, OnoKotlinKClassifier>
@required
- (BOOL)isInstanceValue:(id _Nullable)value __attribute__((swift_name("isInstance(value:)")));
@property (readonly) NSString * _Nullable qualifiedName __attribute__((swift_name("qualifiedName")));
@property (readonly) NSString * _Nullable simpleName __attribute__((swift_name("simpleName")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinPair")))
@interface OnoKotlinPair : OnoBase
- (instancetype)initWithFirst:(id _Nullable)first second:(id _Nullable)second __attribute__((swift_name("init(first:second:)"))) __attribute__((objc_designated_initializer));
- (id _Nullable)component1 __attribute__((swift_name("component1()")));
- (id _Nullable)component2 __attribute__((swift_name("component2()")));
- (OnoKotlinPair *)doCopyFirst:(id _Nullable)first second:(id _Nullable)second __attribute__((swift_name("doCopy(first:second:)")));
- (BOOL)equalsOther:(id _Nullable)other __attribute__((swift_name("equals(other:)")));
- (int32_t)hashCode __attribute__((swift_name("hashCode()")));
- (NSString *)toString __attribute__((swift_name("toString()")));
@property (readonly) id _Nullable first __attribute__((swift_name("first")));
@property (readonly) id _Nullable second __attribute__((swift_name("second")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinNothing")))
@interface OnoKotlinNothing : OnoBase
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreMutex")))
@protocol OnoKotlinx_coroutines_coreMutex
@required
- (BOOL)holdsLockOwner:(id)owner __attribute__((swift_name("holdsLock(owner:)")));
- (BOOL)tryLockOwner:(id _Nullable)owner __attribute__((swift_name("tryLock(owner:)")));
- (void)unlockOwner:(id _Nullable)owner __attribute__((swift_name("unlock(owner:)")));
@property (readonly) BOOL isLocked __attribute__((swift_name("isLocked")));
@property (readonly) id<OnoKotlinx_coroutines_coreSelectClause2> onLock __attribute__((swift_name("onLock")));
@end;

__attribute__((swift_name("KotlinCoroutineContextKey")))
@protocol OnoKotlinCoroutineContextKey
@required
@end;

__attribute__((swift_name("KotlinIterator")))
@protocol OnoKotlinIterator
@required
- (BOOL)hasNext __attribute__((swift_name("hasNext()")));
- (id _Nullable)next __attribute__((swift_name("next()")));
@end;

__attribute__((swift_name("KotlinByteIterator")))
@interface OnoKotlinByteIterator : OnoBase <OnoKotlinIterator>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (OnoByte *)next __attribute__((swift_name("next()")));
- (int8_t)nextByte __attribute__((swift_name("nextByte()")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreSelectClause2")))
@protocol OnoKotlinx_coroutines_coreSelectClause2
@required
- (void)registerSelectClause2Select:(id<OnoKotlinx_coroutines_coreSelectInstance>)select param:(id _Nullable)param block:(id<OnoKotlinSuspendFunction1>)block __attribute__((swift_name("registerSelectClause2(select:param:block:)")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreSelectInstance")))
@protocol OnoKotlinx_coroutines_coreSelectInstance
@required
- (void)disposeOnSelectHandle:(id<OnoKotlinx_coroutines_coreDisposableHandle>)handle __attribute__((swift_name("disposeOnSelect(handle:)")));
- (id _Nullable)performAtomicTrySelectDesc:(OnoKotlinx_coroutines_coreAtomicDesc *)desc __attribute__((swift_name("performAtomicTrySelect(desc:)")));
- (void)resumeSelectWithExceptionException:(OnoKotlinThrowable *)exception __attribute__((swift_name("resumeSelectWithException(exception:)")));
- (BOOL)trySelect __attribute__((swift_name("trySelect()")));
- (id _Nullable)trySelectOtherOtherOp:(OnoKotlinx_coroutines_corePrepareOp * _Nullable)otherOp __attribute__((swift_name("trySelectOther(otherOp:)")));
@property (readonly) id<OnoKotlinContinuation> completion __attribute__((swift_name("completion")));
@property (readonly) BOOL isSelected __attribute__((swift_name("isSelected")));
@end;

__attribute__((swift_name("KotlinFunction")))
@protocol OnoKotlinFunction
@required
@end;

__attribute__((swift_name("KotlinSuspendFunction1")))
@protocol OnoKotlinSuspendFunction1 <OnoKotlinFunction>
@required
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreDisposableHandle")))
@protocol OnoKotlinx_coroutines_coreDisposableHandle
@required
- (void)dispose __attribute__((swift_name("dispose()")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreAtomicDesc")))
@interface OnoKotlinx_coroutines_coreAtomicDesc : OnoBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)completeOp:(OnoKotlinx_coroutines_coreAtomicOp *)op failure:(id _Nullable)failure __attribute__((swift_name("complete(op:failure:)")));
- (id _Nullable)prepareOp:(OnoKotlinx_coroutines_coreAtomicOp *)op __attribute__((swift_name("prepare(op:)")));
@property OnoKotlinx_coroutines_coreAtomicOp *atomicOp __attribute__((swift_name("atomicOp")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreOpDescriptor")))
@interface OnoKotlinx_coroutines_coreOpDescriptor : OnoBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (BOOL)isEarlierThanThat:(OnoKotlinx_coroutines_coreOpDescriptor *)that __attribute__((swift_name("isEarlierThan(that:)")));
- (id _Nullable)performAffected:(id _Nullable)affected __attribute__((swift_name("perform(affected:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) OnoKotlinx_coroutines_coreAtomicOp * _Nullable atomicOp __attribute__((swift_name("atomicOp")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Kotlinx_coroutines_corePrepareOp")))
@interface OnoKotlinx_coroutines_corePrepareOp : OnoKotlinx_coroutines_coreOpDescriptor
- (instancetype)initWithAffected:(OnoKotlinx_coroutines_coreLinkedListNode *)affected desc:(OnoKotlinx_coroutines_coreAbstractAtomicDesc *)desc atomicOp:(OnoKotlinx_coroutines_coreAtomicOp *)atomicOp __attribute__((swift_name("init(affected:desc:atomicOp:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (void)finishPrepare __attribute__((swift_name("finishPrepare()")));
- (id _Nullable)performAffected:(id _Nullable)affected __attribute__((swift_name("perform(affected:)")));
@property (readonly) OnoKotlinx_coroutines_coreLinkedListNode *affected __attribute__((swift_name("affected")));
@property (readonly) OnoKotlinx_coroutines_coreAtomicOp *atomicOp __attribute__((swift_name("atomicOp")));
@property (readonly) OnoKotlinx_coroutines_coreAbstractAtomicDesc *desc __attribute__((swift_name("desc")));
@end;

__attribute__((swift_name("KotlinContinuation")))
@protocol OnoKotlinContinuation
@required
- (void)resumeWithResult:(id _Nullable)result __attribute__((swift_name("resumeWith(result:)")));
@property (readonly) id<OnoKotlinCoroutineContext> context __attribute__((swift_name("context")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreAtomicOp")))
@interface OnoKotlinx_coroutines_coreAtomicOp : OnoKotlinx_coroutines_coreOpDescriptor
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)completeAffected:(id _Nullable)affected failure:(id _Nullable)failure __attribute__((swift_name("complete(affected:failure:)")));
- (id _Nullable)decideDecision:(id _Nullable)decision __attribute__((swift_name("decide(decision:)")));
- (id _Nullable)performAffected:(id _Nullable)affected __attribute__((swift_name("perform(affected:)")));
- (id _Nullable)prepareAffected:(id _Nullable)affected __attribute__((swift_name("prepare(affected:)")));
@property (readonly) OnoKotlinx_coroutines_coreAtomicOp *atomicOp __attribute__((swift_name("atomicOp")));
@property (readonly) BOOL isDecided __attribute__((swift_name("isDecided")));
@property (readonly) int64_t opSequence __attribute__((swift_name("opSequence")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreLinkedListNode")))
@interface OnoKotlinx_coroutines_coreLinkedListNode : OnoBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)addLastNode:(OnoKotlinx_coroutines_coreLinkedListNode *)node __attribute__((swift_name("addLast(node:)")));
- (BOOL)addLastIfNode:(OnoKotlinx_coroutines_coreLinkedListNode *)node condition:(OnoBoolean *(^)(void))condition __attribute__((swift_name("addLastIf(node:condition:)")));
- (BOOL)addLastIfPrevNode:(OnoKotlinx_coroutines_coreLinkedListNode *)node predicate:(OnoBoolean *(^)(OnoKotlinx_coroutines_coreLinkedListNode *))predicate __attribute__((swift_name("addLastIfPrev(node:predicate:)")));
- (BOOL)addLastIfPrevAndIfNode:(OnoKotlinx_coroutines_coreLinkedListNode *)node predicate:(OnoBoolean *(^)(OnoKotlinx_coroutines_coreLinkedListNode *))predicate condition:(OnoBoolean *(^)(void))condition __attribute__((swift_name("addLastIfPrevAndIf(node:predicate:condition:)")));
- (BOOL)addOneIfEmptyNode:(OnoKotlinx_coroutines_coreLinkedListNode *)node __attribute__((swift_name("addOneIfEmpty(node:)")));
- (void)helpRemove __attribute__((swift_name("helpRemove()")));
- (BOOL)remove __attribute__((swift_name("remove()")));
- (id _Nullable)removeFirstIfIsInstanceOfOrPeekIfPredicate:(OnoBoolean *(^)(id _Nullable))predicate __attribute__((swift_name("removeFirstIfIsInstanceOfOrPeekIf(predicate:)")));
- (OnoKotlinx_coroutines_coreLinkedListNode * _Nullable)removeFirstOrNull __attribute__((swift_name("removeFirstOrNull()")));
@property (readonly) BOOL isRemoved __attribute__((swift_name("isRemoved")));
@property (readonly) OnoKotlinx_coroutines_coreLinkedListNode *nextNode __attribute__((swift_name("nextNode")));
@property (readonly) OnoKotlinx_coroutines_coreLinkedListNode *prevNode __attribute__((swift_name("prevNode")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreAbstractAtomicDesc")))
@interface OnoKotlinx_coroutines_coreAbstractAtomicDesc : OnoKotlinx_coroutines_coreAtomicDesc
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)completeOp:(OnoKotlinx_coroutines_coreAtomicOp *)op failure:(id _Nullable)failure __attribute__((swift_name("complete(op:failure:)")));
- (id _Nullable)failureAffected:(OnoKotlinx_coroutines_coreLinkedListNode *)affected __attribute__((swift_name("failure(affected:)")));
- (void)finishOnSuccessAffected:(OnoKotlinx_coroutines_coreLinkedListNode *)affected next:(OnoKotlinx_coroutines_coreLinkedListNode *)next __attribute__((swift_name("finishOnSuccess(affected:next:)")));
- (void)finishPreparePrepareOp:(OnoKotlinx_coroutines_corePrepareOp *)prepareOp __attribute__((swift_name("finishPrepare(prepareOp:)")));
- (void)onComplete __attribute__((swift_name("onComplete()")));
- (id _Nullable)onPreparePrepareOp:(OnoKotlinx_coroutines_corePrepareOp *)prepareOp __attribute__((swift_name("onPrepare(prepareOp:)")));
- (id _Nullable)prepareOp:(OnoKotlinx_coroutines_coreAtomicOp *)op __attribute__((swift_name("prepare(op:)")));
- (BOOL)retryAffected:(OnoKotlinx_coroutines_coreLinkedListNode *)affected next:(id)next __attribute__((swift_name("retry(affected:next:)")));
@property (readonly) OnoKotlinx_coroutines_coreLinkedListNode *affectedNode __attribute__((swift_name("affectedNode")));
@end;

#pragma clang diagnostic pop
NS_ASSUME_NONNULL_END
