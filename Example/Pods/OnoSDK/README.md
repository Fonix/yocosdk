# OnoSDK

[![CI Status](https://img.shields.io/travis/Jacques Questiaux/OnoSDK.svg?style=flat)](https://travis-ci.org/Jacques Questiaux/OnoSDK)
[![Version](https://img.shields.io/cocoapods/v/OnoSDK.svg?style=flat)](https://cocoapods.org/pods/OnoSDK)
[![License](https://img.shields.io/cocoapods/l/OnoSDK.svg?style=flat)](https://cocoapods.org/pods/OnoSDK)
[![Platform](https://img.shields.io/cocoapods/p/OnoSDK.svg?style=flat)](https://cocoapods.org/pods/OnoSDK)

## Example

To run the example project

- Clone the repo
- run `git submodule update`
- run `pod install` from the Example directory
- Open OnoSDK.xcworkspace in Xcode

## Making Changes

When adding files/editing the Pod, you probably want to open the example project and edit the files in

`Pods -> Development Pods -> OnoSDK`

If you add any files to the Pod, make sure Xcode is not adding them to any Targets, and it's correctly putting the files
in the `OnoSDK/Classes` folder (not the Example folder), once you have added files, you need to run `pod install` again,
if you only update files you don't need to run `pod install`.

## Working with KotlinNative submodule

The Ono.framework found in `OnoSDK/Libraries`, is built from the submodule `yoco-thrive-android`. Running `pod install`
rebuilds this framework and copys it to the Libraries folder. This file must be checked in, as that is the version that
is used when installing the Pod through CocoaPods. The script that builds this is found in `Example/buildFramework.sh`

## Installation

OnoSDK is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'OnoSDK'
```

## Installing using the checked out source code

If you have the source code, and want to make test changes rapidly you can instead reference the source code from your
projects Podfile like so:

```ruby
pod 'OnoSDK', :path => '../../payment-sdk/ono-sdk-ios'
```

The above example assumes the following folder structure:
```
Development
|- yoco-register
|  |- ios # location of Podfile above
|- payment-sdk
   |- ono-sdk-ios # Where OnoSDK was checked out
```

## Publishing OnoSDK Pod

To release a new version you simply have to update the OnoSDK.podspec s.version, and tag the branch with that version.
Please only do this on master, and never delete a tag, if you published something bad, simply fix it and publish a new fix

## Author

Jacques Questiaux, jacques@yoco.com

## License

OnoSDK is available under the MIT license. See the LICENSE file for more info.
