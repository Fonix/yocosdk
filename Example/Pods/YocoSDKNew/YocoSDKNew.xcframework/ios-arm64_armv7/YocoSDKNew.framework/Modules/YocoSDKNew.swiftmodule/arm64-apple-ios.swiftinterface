// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.3.1 (swiftlang-1200.0.41 clang-1200.0.32.8)
// swift-module-flags: -target arm64-apple-ios9.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name YocoSDKNew
import Alamofire
import CoreBluetooth
import CoreLocation
import Gloss
import Lottie
import Ono
import OnoSDK
import ReSwift
import Swift
import SwiftSignatureView
import UIKit
@_exported import YocoSDKNew
@objc public enum SupportedCurrency : Swift.Int, Swift.RawRepresentable, Swift.CaseIterable {
  case mur
  case zar
  public var code: Swift.String {
    get
  }
  public var symbol: Swift.String {
    get
  }
  public var debugDescription: Swift.String {
    get
  }
  public typealias RawValue = Swift.Int
  public init?(rawValue: Swift.Int)
  public var rawValue: Swift.Int {
    get
  }
  public typealias AllCases = [YocoSDKNew.SupportedCurrency]
  public static var allCases: [YocoSDKNew.SupportedCurrency] {
    get
  }
}
@objc public enum PaymentType : Swift.Int, Swift.RawRepresentable, Swift.CaseIterable {
  case card
  case cash
  case qr
  public var debugDescription: Swift.String {
    get
  }
  public typealias RawValue = Swift.Int
  public init?(rawValue: Swift.Int)
  public var rawValue: Swift.Int {
    get
  }
  public typealias AllCases = [YocoSDKNew.PaymentType]
  public static var allCases: [YocoSDKNew.PaymentType] {
    get
  }
}
@objc public enum Environment : Swift.Int, Swift.RawRepresentable, Swift.CaseIterable {
  case production
  case staging
  case camel🐫
  case cow🐄
  case dingo🐕
  case fish🐟
  case hippo🦛
  case llama🦙
  case panda🐼
  case racoon🦝
  case rat🐀
  case raven🦅
  case wolf🐺
  public typealias RawValue = Swift.Int
  public init?(rawValue: Swift.Int)
  public var rawValue: Swift.Int {
    get
  }
  public typealias AllCases = [YocoSDKNew.Environment]
  public static var allCases: [YocoSDKNew.Environment] {
    get
  }
}
public struct PaymentParameters {
  public var transactionID: Swift.String?
  public init(autoTransition: Swift.Bool = false, receiptDelegate: YocoSDKNew.YocoReceiptDelegate? = nil, userInfo: [Swift.AnyHashable : Any]? = nil, metaData: [Swift.String : Swift.String]? = nil, staffMember: YocoSDKNew.YocoStaff? = nil, adaptiveTheme: Swift.Bool = false, transactionCompleteNotifier: YocoSDKNew.YocoCompletionHandler? = nil)
}
@objc @_hasMissingDesignatedInitializers @objcMembers public class PaymentResult : ObjectiveC.NSObject {
  @objc final public let result: YocoSDKNew.ResultCode
  @objc final public let amount: Swift.UInt64
  @objc final public let currency: YocoSDKNew.SupportedCurrency
  final public let paymentType: YocoSDKNew.PaymentType?
  @objc final public let transactionID: Swift.String
  @objc final public let staffMember: YocoSDKNew.YocoStaff?
  @objc final public let userInfo: [Swift.AnyHashable : Any]?
  @objc final public let metaData: [Swift.String : Swift.String]?
  @objc deinit
  @objc override dynamic public init()
}
@objc public enum ResultCode : Swift.Int, Swift.RawRepresentable, Swift.CustomDebugStringConvertible, Swift.CaseIterable {
  case success
  case inProgress
  case failure
  case cancelled
  case permissionDenied
  case noConnectivity
  case invalidToken
  case bluetoothDisabled
  case cardMachineError
  public var debugDescription: Swift.String {
    get
  }
  public typealias RawValue = Swift.Int
  public init?(rawValue: Swift.Int)
  public var rawValue: Swift.Int {
    get
  }
  public typealias AllCases = [YocoSDKNew.ResultCode]
  public static var allCases: [YocoSDKNew.ResultCode] {
    get
  }
}
@objc @objcMembers public class YocoStaff : ObjectiveC.NSObject {
  @objc final public let name: Swift.String
  @objc final public let staffNumber: Swift.String
  @objc public init(staffNumber: Swift.String, name: Swift.String)
  @objc deinit
  @objc override dynamic public init()
}
public enum SupportedReceiptType : Swift.String, Swift.CaseIterable {
  case sms
  case email
  case print
  public typealias RawValue = Swift.String
  public init?(rawValue: Swift.String)
  public var rawValue: Swift.String {
    get
  }
  public typealias AllCases = [YocoSDKNew.SupportedReceiptType]
  public static var allCases: [YocoSDKNew.SupportedReceiptType] {
    get
  }
}
public enum ReceiptState {
  case initial
  case inProgress(message: Swift.String? = nil)
  case error(message: Swift.String)
  case complete(message: Swift.String? = nil)
}
public protocol YocoReceiptDelegate : AnyObject {
  func supportedReceiptTypes() -> [YocoSDKNew.SupportedReceiptType]
  func sendSMSReceipt(phoneNumber: Swift.String, paymentResult: YocoSDKNew.PaymentResult, progress: @escaping YocoSDKNew.UpdateReceiptProgress)
  func sendEmailReceipt(address: Swift.String, paymentResult: YocoSDKNew.PaymentResult, progress: @escaping YocoSDKNew.UpdateReceiptProgress)
  func printReceipt(paymentResult: YocoSDKNew.PaymentResult, progress: @escaping YocoSDKNew.UpdateReceiptProgress)
}
extension YocoReceiptDelegate {
  public func sendSMSReceipt(phoneNumber: Swift.String, paymentResult: YocoSDKNew.PaymentResult, progress: @escaping YocoSDKNew.UpdateReceiptProgress)
  public func sendEmailReceipt(address: Swift.String, paymentResult: YocoSDKNew.PaymentResult, progress: @escaping YocoSDKNew.UpdateReceiptProgress)
  public func printReceipt(paymentResult: YocoSDKNew.PaymentResult, progress: @escaping YocoSDKNew.UpdateReceiptProgress)
}
public typealias UpdateReceiptProgress = (YocoSDKNew.ReceiptState) -> Swift.Void
@_hasMissingDesignatedInitializers public class Yoco {
  public static func initialise()
  public static func configure(secret: Swift.String, loggingEnabled: Swift.Bool = false, environment: YocoSDKNew.Environment = .production)
  @available(*, deprecated, message: "This function is only to maintain backwards compatibility")
  public static func configure(secret: Swift.String, apiToken: Swift.String? = nil, loggingEnabled: Swift.Bool = false, environment: YocoSDKNew.Environment = .production)
  @discardableResult
  public static func charge(_ cents: Swift.UInt64, paymentType: YocoSDKNew.PaymentType = .card, currency: YocoSDKNew.SupportedCurrency = .zar, askForTip: Swift.Bool = false, parameters: YocoSDKNew.PaymentParameters? = nil, completionHandler: YocoSDKNew.YocoCompletionHandler? = nil) -> Swift.String
  public static func pairTerminal()
  public static func refund(transactionID: Swift.String, parameters: YocoSDKNew.PaymentParameters? = nil, completionHandler: YocoSDKNew.YocoCompletionHandler? = nil)
  public static func logout()
  public static func getTransactionStatus(transactionID: Swift.String, completionHandler: @escaping (YocoSDKNew.PaymentResult?, Swift.Error?) -> Swift.Void)
  public static func applicationWillTerminate()
  public static func configure(token: Swift.String, onoMerchantID: Swift.String, loggingEnabled: Swift.Bool = false, environment: YocoSDKNew.Environment = .production)
  public static func passSavedTerminalInfo(address: Swift.String, manufacturer: Swift.String, model: Swift.String) -> Swift.Bool
  @objc deinit
}
public typealias YocoCompletionHandler = (YocoSDKNew.PaymentResult) -> Swift.Void
extension YocoSDKNew.SupportedCurrency : Swift.Equatable {}
extension YocoSDKNew.SupportedCurrency : Swift.Hashable {}
extension YocoSDKNew.PaymentType : Swift.Equatable {}
extension YocoSDKNew.PaymentType : Swift.Hashable {}
extension YocoSDKNew.Environment : Swift.Equatable {}
extension YocoSDKNew.Environment : Swift.Hashable {}
extension YocoSDKNew.ResultCode : Swift.Equatable {}
extension YocoSDKNew.ResultCode : Swift.Hashable {}
extension YocoSDKNew.SupportedReceiptType : Swift.Equatable {}
extension YocoSDKNew.SupportedReceiptType : Swift.Hashable {}
extension YocoSDKNew.SupportedReceiptType : Swift.RawRepresentable {}
