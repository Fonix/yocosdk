import UIKit
import YocoSDKNew

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  
  var window: UIWindow?
  
  internal func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    
    Yoco.initialise()
//    Yoco.configure(token: "1602586260241-eace6ba1-add5-470d-b74a-2dcda4e3e19f|177287a655b7984c55f4b4edb681513c4e21e546713a64ab67542eb2622eb4f", onoMerchantID: "3b0a8650-551b-4b34-af7e-5fe7c8aad746", loggingEnabled: true, environment: .fish🐟)
    Yoco.configure(secret: "s-yoco-secret", loggingEnabled: true, environment: .fish🐟)
//    Yoco.configure(secret: "s-ecret", loggingEnabled: true, environment: .production)
    
    return true
  }
  
  func applicationWillResignActive(_ application: UIApplication) {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
  }
  
  func applicationDidEnterBackground(_ application: UIApplication) {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
  }
  
  func applicationWillEnterForeground(_ application: UIApplication) {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
  }
  
  func applicationDidBecomeActive(_ application: UIApplication) {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
  }
  
  func applicationWillTerminate(_ application: UIApplication) {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    Yoco.applicationWillTerminate()
  }
  
}
