import UIKit
import YocoSDKNew

class ViewController: UIViewController {
  
  @IBOutlet weak var amountTextField: UITextField!
  
  var parameters: PaymentParameters!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    parameters = PaymentParameters(receiptDelegate: self,
                                   userInfo: [
                                    "user" : "info"
                                   ],
                                   metaData: [
                                    "meta" : "data"
                                   ],
                                   staffMember: YocoStaff(staffNumber: "1234",
                                                          name: "Joe Bloggs"),
                                   adaptiveTheme: true,
                                   transactionCompleteNotifier: { paymentResult in
                                    // Get notified immediately when the transaction is complete.
                                   })
    
    // If something happened during a transaction, attempt to get the transaction status and display the result to the user.
//    if let transactionID = UserDefaults.standard.value(forKey: "transactionInProgress") as? String {
//      Yoco.getPaymentResult(transactionID: transactionID) { paymentResult, error in
//        if let paymentResult = paymentResult {
//          Yoco.showPaymentResult(paymentResult, paymentParameters: self.parameters) { paymentResult in
//            UserDefaults.standard.set(nil, forKey: "transactionInProgress")
//          }
//        }
//      }
//    }
  }
  
  // MARK: IBActions
  
  @IBAction func startCardPayment(_ sender: UIButton) {
    startTransaction(type: .card)
  }
  
  @IBAction func startQRPayment(_ sender: UIButton) {
    startTransaction(type: .qr)
  }
  
  @IBAction func refundPressed(_ sender: UIButton) {
    if let transactionID = UserDefaults.standard.value(forKey: "lastTransactionID") as? String {
      Yoco.refund(transactionID: transactionID, parameters: parameters)
    } else {
      let alert = UIAlertController(title: "Error", message: "No transaction has been done yet to refund, please do a transaction first", preferredStyle: .alert)
      alert.addAction(UIAlertAction(title: "OK", style: .default) { action in
        alert.dismiss(animated: true)
      })

      present(alert, animated: true)
    }
  }
  
  @IBAction func pairButtonPressed(_ sender: UIButton) {
    Yoco.pairTerminal()
  }
  
  @IBAction func logoutButtonPressed(_ sender: UIButton) {
    Yoco.logout()
    
    let alert = UIAlertController(title: "Success", message: "You have logged out successfully.", preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "OK", style: .default) { action in
      alert.dismiss(animated: true)
    })
    
    present(alert, animated: true)
  }
  
  // MARK: Transaction functions
  
  func startTransaction(type: PaymentType) {
    let cents = Double(amountTextField.text!)! * 100
    let money = UInt64(cents.rounded())
    
    let transactionID = Yoco.charge(money,
                                    paymentType: type,
                                    currency: .zar,
                                    askForTip: true,
                                    parameters: parameters) { paymentResult in
      // Do something once the payment is complete and the YocoSDK has dismissed
      UserDefaults.standard.set(nil, forKey: "transactionInProgress")
    }
    
    UserDefaults.standard.set(transactionID, forKey: "transactionInProgress")
    UserDefaults.standard.set(transactionID, forKey: "lastTransactionID")
  }
  
  // MARK: Receipt functions
  
  func validate(phoneNumber: String?) -> Bool {
    
    if let number = phoneNumber, number.count > 0 {
      return true
    }
    
    return false
  }
  
  func validate(emailAddress: String?) -> Bool {
    
    if let email = emailAddress, email.count > 0 {
      return true
    }
    
    return false
  }
}

extension ViewController: YocoReceiptDelegate {
  
  func supportedReceiptTypes() -> [SupportedReceiptType] {
    return [.email, .sms, .print]
  }
  
  // MARK: - optional delegate methods
  func sendSMSReceipt(phoneNumber: String, paymentResult: PaymentResult, progress: @escaping UpdateReceiptProgress) {
    if validate(phoneNumber: phoneNumber) {
      progress(.complete())
    } else {
      progress(.error(message: "Phone number is invalid"))
    }
  }
  
  func sendEmailReceipt(address: String, paymentResult: PaymentResult, progress: @escaping UpdateReceiptProgress) {
    progress(.inProgress())
    
    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
      
      if self.validate(emailAddress: address) {
        progress(.complete())
      } else {
        progress(.error(message: "Email is invalid"))
      }
    }
  }
  
  func printReceipt(paymentResult: PaymentResult, progress: @escaping UpdateReceiptProgress) {
    progress(.inProgress(message: "Initialising..."))
    
    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
      
      progress(.inProgress(message: "Printing..."))
      
      DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
        
        progress(.complete())
      }
    }
  }
}
