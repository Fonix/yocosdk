// https://github.com/Quick/Quick

@testable import YocoSDKNew
import Quick
import Nimble

class LoginTest: QuickSpec {
  
  override func spec() {
    
    Nimble.AsyncDefaults.Timeout = 5
    
    Webservice.dataSource = FileDataSource(stubFileMapping: [EndPoints.Auth.login: "LoginSuccess",
                                                             EndPoints.Analytics.event: "AnalyticsEvent"])
    
    // To simulate first time run of the app these would not be set
    
    Storage.write(nil, for: .secret)
    Storage.write(nil, for: .token)
    
    describe("attempting login") {
      
      beforeEach {
        Yoco.setup()
        actionHistory = [] // removes some of the setup actions that are fired that muddy the tests
      }
      
      afterEach {
        Yoco.cleanUp()
      }
      
      it("should login successfully") {
        
        let fakeTrxnID = "xxxx"
        let amount: Int64 = 1000
        let vc = LoginViewController()
        
        dispatch(Actions.Transaction.Start(amount: amount, paymentType: .card, currency: .zar, transactionID: fakeTrxnID, askForTip: false, completionHandler: nil))
        
        dispatch(Actions.Auth.Login(password: "Silver123", email: "test@yoco.com"))
        
        let expectedActions: [Action] = [
          Actions.Transaction.Start(amount: amount, paymentType: .card, currency: .zar, transactionID: fakeTrxnID, askForTip: false, completionHandler: nil),
          Actions.App.Segue(storyboard: .login(.initial), clearNavigationStack: false, allowDuplicate: false),
          Actions.App.SeguePushQueue(segue: Actions.App.Segue(storyboard: .login(.initial), clearNavigationStack: false, allowDuplicate: false)),
          Actions.App.TopViewController(viewController: vc),
          Actions.Auth.Login(password: "Silver123", email: "test@yoco.com", phoneNumber: nil),
          Actions.Auth.SetToken(token: "abcde"),
          Actions.Auth.SetOnoMerchantID(onoMerchantID: "zyxwv"),
          Actions.Auth.SetIntegration(integrator: nil, integratorUUID: nil),
          Actions.Auth.LoginSuccess(),
          Actions.Transaction.Start(amount: amount, paymentType: .card, currency: .zar, transactionID: fakeTrxnID, askForTip: false, completionHandler: nil),
          Actions.PaymentProcessor.SetIntegrationAuth(secret: "s-yoco-secret", token: "abcde"),
          Actions.PaymentProcessor.StartCardTransaction(amount: amount, currencyCode: "ZAR", transactionID: fakeTrxnID, onoMerchantID: "zyxwv", askForTip: false, integrator: "PaymentSDK", integratorUUID: nil),
          Actions.Terminal.DidFindTerminals(terminals: [:]),
          Actions.Terminal.Updated(terminal: nil),
          Actions.Terminal.StartPairingProcess(),
          Actions.App.Segue(storyboard: .choose(.initial), clearNavigationStack: false, allowDuplicate: false),
          Actions.App.SeguePushQueue(segue: Actions.App.Segue(storyboard: .choose(.initial), clearNavigationStack: false, allowDuplicate: false)),
          Actions.Analytics.Event(name: "transaction_start", properties: nil)
        ]

        expect(actionHistory.debugDescription).to(equal(expectedActions.debugDescription))
        expect(state.auth.secret).to(equal("s-yoco-secret"))
        expect(state.auth.token).to(equal("abcde"))
        expect(state.transaction.transactionID).to(equal(fakeTrxnID))
        expect(state.transaction.amount).to(equal(amount))
        expect(state.transaction.currency).to(equal(.zar))
      }
    }
  }
}
