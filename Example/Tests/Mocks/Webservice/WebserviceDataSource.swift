import Gloss
import Alamofire
@testable import YocoSDKNew

class FileDataSource: DataSource {

  let jsonSerilizer = GlossJSONSerializer()
  let stubFileMapping: [String: String] // Maps an end point url path to a stub json file
  
  required init (stubFileMapping: [String: String]) {
    
    self.stubFileMapping = stubFileMapping
  }

  func get<T: JSONDecodable, E: ServerError>(path: String,
                                             parameters: [String: String]? = nil,
                                             errorType: E.Type,
                                             cachePolicy: URLRequest.CachePolicy = .reloadRevalidatingCacheData,
                                             timeout: TimeInterval = 30,
                                             completionHandler: WebserviceCompletionHandler<T>?) {

    doRequest(path, errorType: errorType, completionHandler: completionHandler)
  }

  func post<T: JSONDecodable, E: ServerError>(path: String,
                                              parameters: [String: Any],
                                              errorType: E.Type,
                                              cachePolicy: URLRequest.CachePolicy = .reloadRevalidatingCacheData,
                                              timeout: TimeInterval = 30,
                                              completionHandler: WebserviceCompletionHandler<T>?) {

    doRequest(path, errorType: errorType, completionHandler: completionHandler)
  }

  func doRequest<T: JSONDecodable, E: ServerError>(_ path: String,
                                                   errorType: E.Type,
                                                   completionHandler: WebserviceCompletionHandler<T>?) {

    
    
    if let fileName = stubFileMapping[path], let path = Bundle.main.path(forResource: fileName, ofType: "json") {
      do {
        let fileUrl = URL(fileURLWithPath: path)
        // Getting data from JSON file using the file URL
        let data = try Data(contentsOf: fileUrl, options: .mappedIfSafe)
        
        let jsonResponse = try JSONSerialization.jsonObject(with: data, options: [.allowFragments])
        log("Response: \(fileName).json: \n\(jsonResponse)") //Response result

        if let response = T(data: data, serializer: self.jsonSerilizer) {
          completionHandler?(200, response, nil)
        } else if let serverError = E(data: data, serializer: self.jsonSerilizer) {
          completionHandler?(400, nil, YocoError(kind: .webservice(.serverError(serverError))))
        } else {
          completionHandler?(0, nil, YocoError(kind: .webservice(.jsonParsingError))) //Return JsonParsingError if the response object was not formed
        }
        
      } catch let error as NSError {
        assert(false, "Failed to load: \(error.localizedDescription)")
      }
    } else {
      assert(false, "Couldnt find stubfile")
    }
  }
}
